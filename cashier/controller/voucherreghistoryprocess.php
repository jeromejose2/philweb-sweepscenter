<?php

/*
 * Author :             Noel Antonio
 * Date Created:        September 21, 2012
 */

require_once('../init.inc.php');

$stylesheets[] = "css/datepicker.css";
$javascripts[] = "jscripts/datepicker.js";
$javascripts[] = "jscripts/checking.js";

App::LoadModuleClass("SweepsCenter", "SCCV_PromotionalCoupons");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");

$cvpromotionalcoupons = new SCCV_PromotionalCoupons();

$fproc = new FormsProcessor();

$txtDateFr = new TextBox("txtDateFr","txtDateFr","Date");
$txtDateFr->Length = 10;
$txtDateFr->Args = 'onkeypress="javascript: return isNumberKey(event);" style="text-align:center;"';
$txtDateFr->Text = date("m/d/Y");
$txtDateFr->ReadOnly = true;

$btnSearch = new Button("btnSearch","btnSearch","Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";

$fproc->AddControl($txtDateFr);
$fproc->AddControl($btnSearch);
$fproc->ProcessForms();

/*PAGING*/
$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
/*PAGING*/

$records = $cvpromotionalcoupons->SelectPromotionalCouponsHistory($txtDateFr->Text);
$count = count($records);
$pgcon->Initialize($itemsperpage, $count);
$pgTransactionHistory = $pgcon->PreRender();
$start = ($count > 0) ? $pgcon->SelectedItemFrom : 0;
$end = (($start - 1) + $itemsperpage);
$totalpages = ceil(($count / $itemsperpage));

$pcinfo = $cvpromotionalcoupons->SelectPromotionalCouponsHistoryWithLimit($txtDateFr->Text, ($pgcon->SelectedItemFrom - 1), $itemsperpage);
$pclist = new ArrayList();
$pclist->AddArray($pcinfo);

if ($fproc->IsPostBack)
{
    if($btnSearch->SubmittedValue == "Search")
    {
        $pgcon->SelectedPage = 1;
    }
    
    $records = $cvpromotionalcoupons->SelectPromotionalCouponsHistory($txtDateFr->SubmittedValue);
    $count = count($records);
    $pgcon->Initialize($itemsperpage, $count);
    $pgTransactionHistory = $pgcon->PreRender();
    $start = ($count > 0) ? $pgcon->SelectedItemFrom : 0;
    $end = (($start - 1) + $itemsperpage);
    $totalpages = ceil(($count / $itemsperpage));
    
    $pcinfo = $cvpromotionalcoupons->SelectPromotionalCouponsHistoryWithLimit($txtDateFr->SubmittedValue, ($pgcon->SelectedItemFrom - 1), $itemsperpage);
    $pclist = new ArrayList();
    $pclist->AddArray($pcinfo);
}
?>
