<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 01, 2012
 */
require_once('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessionDetails");
App::LoadModuleClass("SweepsCenter", "SCCC_PointsConversionInfo");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");

$cterminals = new SCC_Terminals();
$cterminalsessiondtls = new SCC_TerminalSessionDetails();
$ccpointconversioninfo = new SCCC_PointsConversionInfo();
$stylesheets[] = "css/datepicker.css";
$javascripts[] = "jscripts/datepicker.js";
$javascripts[] = "jscripts/checking.js";
$pagename = "transhistconversion";

$transhistconversion_form = new FormsProcessor();

$txtDateFr = new TextBox("txtDateFr","txtDateFr","Date From");
$txtDateFr->Length = 10;
$txtDateFr->Args = 'onkeypress="javascript: return isNumberKey(event);" style="text-align:center;"';
$txtDateFr->Text = date("m/d/Y");
$txtDateFr->ReadOnly = true;

$txtDateTo = new TextBox("txtDateTo","txtDateTo","Date To");
$txtDateTo->Length = 10;
$txtDateTo->Args = 'onkeypress="javascript: return isNumberKey(event);" style="text-align:center;"';
$txtDateTo->Text = date("m/d/Y");
$txtDateTo->ReadOnly = true;

$terminals = $cterminals->SelectTerminalsForTransHistory($_SESSION['siteidcashier']);
$terminal_list = new ArrayList();
$terminal_list->AddArray($terminals);
$ddlTerminal = new ComboBox("ddlTerminal","ddlTerminal","Terminal");
$options = null;
$options[] = new ListItem("-----","0",true);
$ddlTerminal->Items = $options;
$ddlTerminal->DataSource = $terminal_list;
$ddlTerminal->DataSourceText = "Name";
$ddlTerminal->DataSourceValue = "ID";
$ddlTerminal->DataBind();

$btnSearch = new Button("btnSearch","btnSearch","Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = 'onclick="javascript: return checkinput();"';

$transhistconversion_form->AddControl($txtDateFr);
$transhistconversion_form->AddControl($txtDateTo);
$transhistconversion_form->AddControl($ddlTerminal);
$transhistconversion_form->AddControl($btnSearch);

$transhistconversion_form->ProcessForms();
/*PAGING*/
$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
/*PAGING*/
if($transhistconversion_form->IsPostBack)
{
    if($btnSearch->SubmittedValue == "Search")
    {
        $pgcon->SelectedPage = 1;
    }

    $siteid = $_SESSION['siteidcashier'];
    $dateFrom = $txtDateFr->SubmittedValue;
    $dateFrom = date_create($dateFrom);
    $dateFrom = date_format($dateFrom, "Y-m-d 09:59:59.99999");
    $dateTo = $txtDateTo->SubmittedValue;
    /*$dateinterval = new DateInterval('P1D');
    $dateTo = date_create($dateTo);
    $dateTo = date_add($dateTo, $dateinterval);
    $dateTo = date_format($dateTo, "Y-m-d 10:00:00.00000");*/
	$dateTo = strtotime ( '+1 day' , strtotime ( $dateTo ) ) ;
	$dateTo = date ( 'Y-m-d 10:00:00.00000' , $dateTo );
    $terminalid = $ddlTerminal->SubmittedValue;

    $displayrecords = "ok";
    $transhistconversion = $ccpointconversioninfo->SelectConversionHistory($terminalid, $siteid, $dateFrom, $dateTo);
    $transhistcount = count($transhistconversion);
    $pgcon->Initialize($itemsperpage, $transhistcount);
    $pgTransactionHistory = $pgcon->PreRender();
    $start = ($transhistcount > 0 ) ? $pgcon->SelectedItemFrom : 0;
    $end = (($start - 1) + $itemsperpage);
    $selectedpage = $pgcon->SelectedPage;
    $totalpages = ceil(($transhistcount / $itemsperpage));
    $transhistconversiondtls = $ccpointconversioninfo->SelectConversionHistoryWithLimit($terminalid, $siteid, $dateFrom, $dateTo,($pgcon->SelectedItemFrom - 1),$itemsperpage);
}
?>
