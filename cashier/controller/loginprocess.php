<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 21, 2012
 */

error_reporting(E_ALL);
require_once('../init.inc.php');
session_regenerate_id();
App::LoadModuleClass("SweepsCenter", "SCC_Accounts");
App::LoadModuleClass("SweepsCenter", "SCC_AccountSessions");
App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");

App::LoadCore("PHPMailer.class.php");

App::LoadControl("TextBox");
App::LoadControl("Button");

$caccounts = new SCC_Accounts();
$cacctsessions = new SCC_AccountSessions();
$caudittrail = new SCC_AuditTrail();

$login_form = new FormsProcessor();

$txtUsername = new TextBox("txtUsername","txtUsername","Username");
$txtUsername->Length = 20;
$txtUsername->Args="autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtPassword = new TextBox("txtPassword","txtPassword","Password");
$txtPassword->Password = true;
$txtPassword->Args="autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$btnSubmit = new Button("btnSubmit","btnSubmit","LOG-IN");
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "labelbold2";
$btnSubmit->Args = 'onclick="javascript: return checkLogin();"';

$txtEmail = new TextBox("txtEmail","txtEmail","Email");
$txtEmail->Style = "width: 350px";
$txtEmail->Args="onkeypress='javascript: return verifyEmail(event);'";

$btnReset = new Button("btnReset","btnReset","RESET");
$btnReset->CssClass = "labelbold2";
$btnReset->IsSubmit = true;
$btnReset->Args = 'onclick="javascript: return redirect();"';

$login_form->AddControl($txtUsername);
$login_form->AddControl($txtPassword);
$login_form->AddControl($btnSubmit);
$login_form->AddControl($txtEmail);
$login_form->AddControl($btnReset);

$login_form->ProcessForms();

if ($login_form->IsPostBack)
{
    if($btnSubmit->SubmittedValue == "LOG-IN")
    {
        $username = $txtUsername->SubmittedValue;
        $password = $txtPassword->SubmittedValue;
        $userdtls = $caccounts->SelectAccountDetails($username,MD5($password));
        if(count($userdtls) > 0)
        {
            if($userdtls[0]["Password"] == MD5($password))
            {
                $session = session_id();
                $cacctsessions->StartTransaction();
                $arraccountsessions["SessionID"] = $session;
                $arraccountsessions["AccountID"] = $userdtls[0]["ID"];
                $arraccountsessions["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $arraccountsessions["DateCreated"] = 'now_usec()';
                $arraccountsessions["DateStart"] = 'now_usec()';
                $cacctsessions->Insert($arraccountsessions);
                if($cacctsessions->HasError)
                {
                    $cacctsessions->RollBackTransaction();
                    $error_title = "INVALID LOG IN";
                    $error_msg = "Error has occured: " . $cacctsessions->getError();
                }
                else
                {
                    $cacctsessions->CommitTransaction();
                }
               
                $caudittrail->StartTransaction();
                $arraudittrail["SessionID"] = $session;
                $arraudittrail["AccountID"] = $userdtls[0]["ID"];
                $arraudittrail["TransDetails"] = 'Login: ' . $username;
                $arraudittrail["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $arraudittrail["TransDateTime"] = 'now_usec()';
                $caudittrail->Insert($arraudittrail);
                if($caudittrail->HasError)
                {
                    $caudittrail->RollBackTransaction();
                    $error_title = "INVALID LOG IN";
                    $error_msg = "Error has occured: " . $caudittrail->getError();
                }
                else
                {
                    $caudittrail->CommitTransaction();
                    $_SESSION['sid'] = $session;
                    $_SESSION['accttype'] = $userdtls[0]["AccountType"];
                    $_SESSION['acctid'] = $userdtls[0]["ID"];
                    $_SESSION['siteidcashier'] = $userdtls[0]["SiteID"];
                    $_SESSION['projectid'] = '2';
                    echo "<script>window.location = 'activesession.php';</script>";
                    //header("location: activesession.php");echo "here";exit();
                }
            }
            else
            {
                $error_title = "INVALID LOG IN";
                $error_msg = "You have entered an incorrect account information.";
            }
        }
        else
        {
            $error_title = "INVALID LOG IN";
            $error_msg = "You have entered an incorrect account information.";
        }
    }

    if($btnReset->SubmittedValue == "RESET")
    {
        $email = $txtEmail->SubmittedValue;
        $useremaildtls = $caccounts->SelectAccountEmail($email);
        if(count($useremaildtls) > 0)
        {
            $newpass = substr(session_id(),0,8);
            $caccounts->StartTransaction();
            $caccounts->UpdateAccountPassword($newpass,$useremaildtls[0]["ID"],$email,$useremaildtls[0]["Username"]);
            if($caccounts->HasError)
            {
                $caccounts->RollBackTransaction();
                $error_title = "RESET PASSWORD";
                $error_msg = "Error updating the accounts table.";
            }
            else
            {
                $caccounts->CommitTransaction();
            }

            $caudittrail->StartTransaction();
            $arraudittrail["AccountID"] = $useremaildtls[0]["ID"];
            $arraudittrail["TransDetails"] = 'Change password for: ' . $useremaildtls[0]["Username"];
            $arraudittrail["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $arraudittrail["TransDateTime"] = 'now_usec()';
            $caudittrail->Insert($arraudittrail);
            if($caudittrail->HasError)
            {
                $caudittrail->RollBackTransaction();
                $error_title = "INVALID LOG IN";
                $error_msg = "Error inserting in audit trail.";
            }
            else
            {
                $caudittrail->CommitTransaction();

                $pm = new PHPMailer();
                $pm->AddAddress($email, $useremaildtls[0]["Username"]);
                $pageURL = 'http';
                if (!empty($_SERVER['HTTPS'])) {$pageURL .= "s";}
                $pageURL .= "://";
                $folder = $_SERVER["REQUEST_URI"];
                $folder = substr($folder,0,strrpos($folder,'/') + 1);
                if ($_SERVER["SERVER_PORT"] != "80")
                {
                  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
                }
                else
                {
                  $pageURL .= $_SERVER["SERVER_NAME"].$folder;
                }
                $date = date("m/d/Y");
                $time = date("h:i:s A");
                $acctname = $useremaildtls[0]["FirstName"] . " " . $useremaildtls[0]["MiddleName"] . " " . $useremaildtls[0]["LastName"];
                $pm->IsHTML(true);
                $pm->Body = "<div>Dear $acctname,</div><br />
                            This is to inform you that your password has been reset on this date $date and time $time. Here are your new Account Details:
                            <br /><br />
                                <div style=\"padding-left: 3%\">Username: <b>" . $useremaildtls[0]["Username"] . "</b></div>
                                <div style=\"padding-left: 3%\">Password: <b>" . $newpass . "</b></div>
                            <br /><br />
                            For security purposes, please change this system-generated password upon log in.
                            <br /><br />
                            If you didn’t perform this procedure, please e-mail this to our Customer Service at support@thesweepscenter.com.
                            <br /><br />
                            Regards,
                            <br /><br />
                            The Sweeps Center Team";

                $pm->From = "support@thesweepscenter.com";
                $pm->FromName = "The Sweeps Center";
                $pm->Host = "localhost";
                $pm->Subject = "THE SWEEPS CENTER NOTIFICATION FOR NEW PASSWORD";
                $email_sent = $pm->Send();
                if($email_sent)
                {
                    $error_title = "RESET PASSWORD";
                    $error_msg = "Your new password has been sent to your e-mail account. For security purpose, please change this system-generated password upon log in.";
                }
                else
                {
                    $error_msg = "An error occurred while sending the email to your email address";
                    $error_title = "ERROR!";
                }
            }
        }
        else
        {
            $error_title = "RESET PASSWORD";
            $error_msg = "You have entered an invalid <br />e-mail address. Please try again.";
        }
    }
}
?>
