<?php

/*
 * Author :             Noel Antonio
 * Date Created:        September 20, 2012
 */
require_once('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCCV_PromotionalCoupons");
App::LoadModuleClass("SweepsCenter", "SCCV_VoucherInfo");
App::LoadModuleClass("SweepsCenter", "SCCV_AuditLog");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");

App::LoadCore("PHPMailer.class.php");

$cvpromotionalcoupons = new SCCV_PromotionalCoupons();
$cvvoucherinfo = new SCCV_VoucherInfo();
$cvauditlog = new SCCV_AuditLog();

$txtFullName = new TextBox("txtFullName", "txtFullName");
$txtFullName->Length = 75;
$txtFullName->Args="autocomplete = 'off' onkeypress='javascript: return AlphaAndSpaces(event);'";

$txtBday = new TextBox("txtBday", "txtBday");
$txtBday->Style = "display: none";

$txtAddress = new TextBox("txtAddress", "txtAddress");
$txtAddress->Length = 300;
$txtAddress->Args="autocomplete = 'off' size='34px' onkeypress='javascript: return AlphaNumericAndSpaces(event);'";

$txtContact = new TextBox("txtContact", "txtContact");
$txtContact->Args = "autocomplete = 'off' onkeypress='javascript: return isNumberKey(event);'";
$txtContact->Length = 20;

$txtEmail = new TextBox("txtEmail", "txtEmail");
$txtEmail->Length = 100;
$txtEmail->Args = "autocomplete = 'off' onkeypress='javascript: return verifyEmail(event);'";

$txtCN = new TextBox("txtCN", "txtCN");
$txtCN->Length = 20;
$txtCN->Args = "autocomplete = 'off' onblur='javascript: return validatecoupon(event);'";

$btnSubmit = new Button("btnSubmit", "btnSubmit", "SUBMIT");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Args = "onclick='javascript: return checkvoucherinput();'";

$btnOkay = new Button("btnOkay", "btnOkay", " ");
$btnOkay->CssClass = "inputBoxEffectPopup";
$btnOkay->IsSubmit = true;

$hiddencn = new Hidden("hiddencn", "hiddencn", "hiddencn");
$hiddencn->Text = '0';

$fproc = new FormsProcessor();
$fproc->AddControl($txtFullName);
$fproc->AddControl($txtBday);
$fproc->AddControl($txtAddress);
$fproc->AddControl($txtContact);
$fproc->AddControl($txtEmail);
$fproc->AddControl($txtCN);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnOkay);
$fproc->AddControl($hiddencn);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    $ds = new DateSelector("now");
    $currentdate = $ds->CurrentDate;
    $txtBday->SubmittedValue = $_POST['txtBday_year'] . '-' . $_POST['txtBday_month'] . '-' . $_POST['txtBday_day'];
    $month = date("m");
    $day = date("d");
    $year = date("Y") - 18;
    $date = $year.'-'.$month.'-'.$day;

    
    if ($txtBday->SubmittedValue  <= $date)        
    {
            $fullname = $txtFullName->SubmittedValue;
            $gender = $_POST['rdogen'];
            $bday = $txtBday->SubmittedValue;
            $address = $txtAddress->SubmittedValue;
            $email = $txtEmail->SubmittedValue;
            $contact = $txtContact->SubmittedValue;
            $cn = $txtCN->SubmittedValue;

            $playerexist = $cvpromotionalcoupons->SelectExistingPlayer($fullname, $email, $currentdate);
            $couponexist = $cvpromotionalcoupons->SelectCouponExist($cn);
       if (count($couponexist) == 0)
       {
            if (count($playerexist) == 0)
            {
                $cvpromotionalcoupons->StartTransaction();
                $cvvoucherinfo->StartTransaction();

                $voucherinfo = $cvvoucherinfo->SelectAvailableVoucher();
                if (count($voucherinfo) > 0)
                {
                    $id = $voucherinfo[0]["ID"];
                    $voucherno = $voucherinfo[0]["VoucherNo"];

                    $cvvoucherinfo->UpdateAvailableVoucher($id);
                    if ($cvvoucherinfo->HasError)
                    {
                        $cvvoucherinfo->RollBackTransaction();
                        $error_title = "ERROR!";
                        $error_msg = $cvvoucherinfo->getErrors();
                    }
                    else
                    {   
                        $cvvoucherinfo->CommitTransaction();

                        $pcinfo["FullName"] = $fullname;
                        $pcinfo["Gender"] = $gender;
                        $pcinfo["Birthday"] = $bday;
                        $pcinfo["MailingAddress"] = $address;
                        $pcinfo["ContactNumber"] = $contact;
                        $pcinfo["EmailAddress"] = $email;
                        $pcinfo["CouponNo"] = $cn;
                        $pcinfo["VoucherNo"] = $voucherno;
                        $pcinfo["DateRegistered"] = 'now_usec()';
                        $pcinfo["Status"] = 0;
                        $cvpromotionalcoupons->Insert($pcinfo);
                        if ($cvpromotionalcoupons->HasError)
                        {
                            $cvpromotionalcoupons->RollBackTransaction();
                            $error_title = "ERROR!";
                            $error_msg = $cvpromotionalcoupons->getErrors();
                        }
                        else
                        {
                            $cvpromotionalcoupons->CommitTransaction();

                            $error_title = "SUCCESSFUL!";
                            $error_msg = 'Coupon Number: ' . $cn . '<br/>' . ' Voucher Code: ' . $voucherno . '';
                    // Sending of Email
                            $date = date("m/d/Y");
                            $time = date("h:i:s A");
                            $pm = new PHPMailer();
                            $pm->AddAddress($email, $fullname);

                            $pageURL = 'http';
                            if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
                            $pageURL .= "://";
                            $folder = $_SERVER["REQUEST_URI"];
                            $folder = substr($folder,0,strrpos($folder,'/') + 1);
                            if ($_SERVER["SERVER_PORT"] != "80") 
                            {
                            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
                            } 
                            else 
                            {
                            $pageURL .= $_SERVER["SERVER_NAME"].$folder;
                            }

                            $pm->IsHTML(true);

                            $pm->Body = "<HTML><BODY>						
                                        Dear Mr./Ms. $fullname,
                            <br /><br />
                            Good Day!
                            <br /><br />
                                        We have received your registration for Sweeps Center's Free Entry promo on this date $date and time $time. 
                            Your Coupon Number: <b>$cn</b> and Voucher Code: <b>$voucherno</b>.
                            <br /><br />
                                        To claim your free sweepstakes entry, visit the nearest Sweeps Center and enter this code on the allocated Free Entry terminal.
                            <br /><br />
                                        Please be informed that this Sweeps code will expire within three days from the time of registration. 
                            You can only register for a Sweeps Free Entry once a day. 
                            <br /><br />
                                        For further inquiries regarding the Sweeps Center, contact us at our toll free Customer Service hotline at 023385599 or email us at support.gu@philwebasiapacific.com.
                            <br /><br />
                            Regards,
                            <br /><br />
                            Customer Support<br />
                            The Sweeps Center
                            </BODY></HTML>";

                            $pm->From = "support@thesweepscenter.com";
                            $pm->FromName = "The Sweeps Center";
                            $pm->Host = "localhost";
                            $pm->Subject = "The Sweeps Center Registration for Free Entry Service";
                            $email_sent = $pm->Send();
                            if($email_sent)
                            {
                            //$msg = "You have successfully sent your registration for the Sweeps Center Free Entry. A Free Entry Code was sent to your e-mail address. Please visit any Sweeps Center branch and enter this code at allocated terminals to avail your one Free Entry Sweepstakes.";
                            }
                            else
                            {
                                $error_title = "Error";
                                $error_msg = "An error occurred while sending the email to your email address";
                            }                            
                        }
                    }
                }
                else
                {
                    $error_title = "ERROR!";
                    $error_msg = "No voucher available.";
                }
            }
            else
            {
                $error_title = "ERROR!";
                $error_msg = "The player already played today.";
            }
            
       }else
       {
           $error_title = "ERROR!";
           $error_msg = "Coupon number has already been used.";
       }    
    }
    else
    {
                $error_title = "AGE LIMIT";
                $error_msg = "The Sweeps Center prohibits participation of individuals below 18 years of age.";
    }
     
}//end
?>
