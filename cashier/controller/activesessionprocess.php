<?php

/*
 * Created By: Arlene R. Salazar
 * Created On: May 21, 2012
 */
require_once("../init.inc.php");

$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$pagename = "activesession";

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCC_ref_Services");
App::LoadSettings("swcsettings.inc.php");
//include("../config.php");
$cterminals = new SCC_Terminals();
$refservices = new SCC_ref_Services();

$selectactive = $refservices->SelectActiveService();
$provider = $selectactive[0]['ID'];
$activesessions = $cterminals->SelectActiveSessions($_SESSION['siteidcashier']);

switch ($provider)
{
    case 1:
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");

        $rtg_url = App::getParam("rtg_url");
        $certFilePath = App::getParam("certFilePath");
        $keyFilePath = App::getParam("keyFilePath");

        $capiwrapper = new RealtimeGamingAPIWrapper($rtg_url, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);

        //$activesessions = $cterminals->SelectActiveSessions($_SESSION['siteidcashier']);
        $activesessions = $cterminals->SelectActiveSessionIsplayable($_SESSION['siteidcashier']);

        if (count($activesessions) > 0)
        {

            for ($i = 0; $i < count($activesessions); $i++)
            {

                $param = array('delimitedAccountNumbers' => $activesessions[$i]["Name"]);
                $rtgbalance = $capiwrapper->GetBalance($activesessions[$i]["Name"]);
                if ($rtgbalance['IsSucceed'] == 1 || $rtgbalance['IsSucceed'] == TRUE)
                {
                    $balance = $rtgbalance['BalanceInfo']['Balance'];

                    $cterminals->StartTransaction();
                    $cterminals->UpdateTerminalBalance($balance, $activesessions[$i]["ID"]);

                    if ($cterminals->HasError)
                    {
                        $cterminals->RollBackTransaction();
                    } else
                    {
                        $cterminals->CommitTransaction();
                    }
                } else
                {
                    break;
                }
            }
        }
        break;


    case 3:



        App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
        App::LoadLibrary("MicrogamingAPI.class.php");

        $csmagentsessions = new SCCSM_AgentSessions();

        //$activesessions = $cterminals->SelectActiveSessions($_SESSION['siteidcashier']);
        $activesessions = $cterminals->SelectActiveSessionIsplayable($_SESSION['siteidcashier']);
        $mg_url = App::getParam("mg_url");
        $sessionguid = $csmagentsessions->SelectSessionGUID();
        $sessionGUID_MG = $sessionguid[0]["SessionGUID"];
        $ip_add_MG = '172.20.60.15';
        $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>" .
                "<SessionGUID>" . $sessionGUID_MG . "</SessionGUID>" .
                "<IPAddress>" . $ip_add_MG . "</IPAddress>" .
                "<ErrorCode>0</ErrorCode>" .
                "<IsLengthenSession>true</IsLengthenSession>" .
                "</AgentSession>";
        $client = new nusoap_client($mg_url, 'wsdl');
        $client->setHeaders($headers);

        if (count($activesessions) > 0)
        {
            for ($i = 0; $i < count($activesessions); $i++)
            {
                $param = array('delimitedAccountNumbers' => $activesessions[$i]["Name"]);
                /* $filename = "../../APILogs/logs.txt";
                  $fp = fopen($filename , "a");

                  fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GetAccountBalance || ".$activesessions[$i]["Name"]." || ".serialize($param)."\r\n");
                  fclose($fp);
                 */
                $a = 1;
                while ($a < 4)
                {
                    $result = $client->call('GetAccountBalance', $param);

                    if ($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
                    {
                        $a++;
                    } else
                    {
                        break;
                    }
                }

                $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];

                $cterminals->StartTransaction();
                $cterminals->UpdateTerminalBalance($balance, $activesessions[$i]["ID"]);
                //$cterminals->UpdateTerminalBalance('1.00', $activesessions[$i]["ID"]);
                if ($cterminals->HasError)
                {
                    $cterminals->RollBackTransaction();
                } else
                {
                    $cterminals->CommitTransaction();
                }
            }
        }
        break;
}
?>
