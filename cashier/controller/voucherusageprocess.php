<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 06, 2012
 */
require_once('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCCV_VoucherInfo");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("PagingControl2");

$cvvoucherinfo = new SCCV_VoucherInfo();
$stylesheets[] = "css/datepicker.css";
$javascripts[] = "jscripts/datepicker.js";
$javascripts[] = "jscripts/checking.js";
$javascripts[] = "jscripts/checkinputs.js";
$pagename = "voucherusage";

$voucherusage_form = new FormsProcessor();

$txtDateFr = new TextBox("txtDateFr","txtDateFr","Date From");
$txtDateFr->Length = 10;
$txtDateFr->Args = 'onkeypress="javascript: return isNumberKey(event);" style="text-align:center;"';
$txtDateFr->Text = date("m/d/Y");
$txtDateFr->ReadOnly = true;

$txtDateTo = new TextBox("txtDateTo","txtDateTo","Date To");
$txtDateTo->Length = 10;
$txtDateTo->Args = 'onkeypress="javascript: return isNumberKey(event);" style="text-align:center;"';
$txtDateTo->Text = date("m/d/Y");
$txtDateTo->ReadOnly = true;

$txtVoucherCode = new TextBox("txtVoucherCode","txtVoucherCode","Voucher Code");
$txtVoucherCode ->Args = 'onkeypress="javascript: return AlphaNumericOnly(event);"';
$txtVoucherCode->Length = 12;

$btnSearch = new Button("btnSearch","btnSearch","Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checkinput();' ";

$voucherusage_form->AddControl($txtDateFr);
$voucherusage_form->AddControl($txtDateTo);
$voucherusage_form->AddControl($txtVoucherCode);
$voucherusage_form->AddControl($btnSearch);

$voucherusage_form->ProcessForms();
/*PAGING*/
$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
/*PAGING*/
if($voucherusage_form->IsPostBack)
{
    if($btnSearch->SubmittedValue == "Search")
    {
        $pgcon->SelectedPage = 1;
    }

    $siteid = $_SESSION['siteidcashier'];
    $dateFrom = $txtDateFr->SubmittedValue;
    $dateFrom = date_create($dateFrom);
    $dateFrom = date_format($dateFrom, "Y-m-d 09:59:59.99999");
    $dateTo = $txtDateTo->SubmittedValue;
        $dateTo = strtotime ( '+1 day' , strtotime ( $dateTo ) ) ;
	$dateTo = date ( 'Y-m-d 10:00:00.00000' , $dateTo );
    $vouchercode = $txtVoucherCode->SubmittedValue;

    $displayrecords = "ok";
    $transhist = $cvvoucherinfo->SelectVoucherHistory($siteid,$dateFrom,$dateTo,$vouchercode,0);
    $transhistcount = count($transhist);
    $pgcon->Initialize($itemsperpage, $transhistcount);
    $pgTransactionHistory = $pgcon->PreRender();
    $start = ($transhistcount > 0) ? $pgcon->SelectedItemFrom : 0;
    $end = (($start - 1) + $itemsperpage);
    $selectedpage = $pgcon->SelectedPage;
    $totalpages = ceil(($transhistcount / $itemsperpage));
    $voucherusagesdtls = $cvvoucherinfo->SelectVoucherHistoryWithLimit($siteid,$dateFrom,$dateTo,$vouchercode,0,($pgcon->SelectedItemFrom - 1),$itemsperpage);
}
?>
