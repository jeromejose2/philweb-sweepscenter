<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 21, 2012
 * Updated By: Tere Calderon
 * Updated On: September 26, 2012
 * Purpose:    Update for RTG API
 */
require_once("../init.inc.php");
//include("../config.php");
$pagename = "printvoucher";

App::LoadModuleClass("SweepsCenter","SCC_Terminals");
App::LoadModuleClass("SweepsCenter","SCC_TransactionLogs");
App::LoadModuleClass("SweepsCenter","SCC_AccountSessions");
App::LoadModuleClass("SweepsCenter","SCC_TerminalSessions");
App::LoadModuleClass("SweepsCenter","SCC_Sites");
App::LoadModuleClass("SweepsCenter","SCC_TerminalSessionDetails");
App::LoadModuleClass("SweepsCenter","SCC_AuditTrail");
App::LoadLibrary("microseconds.php");

App::LoadModuleClass("CasinoAPI","RealtimeGamingAPIWrapper");
App::LoadModuleClass("CasinoAPI","RealtimeGamingCashierAPI");
App::LoadSettings("swcsettings.inc.php");
App::LoadModuleClass("SweepsCenter","SCC_ref_Services");



$mg_url       = App::getParam("mg_url");
$rtg_url      = App::getParam("rtg_url");
$certFilePath = App::getParam("certFilePath");
$keyFilePath  = App::getParam("keyFilePath");

$refservices = new SCC_ref_Services();

$selectactive = $refservices->SelectActiveService();
$provider     = $selectactive[0]['ID'];

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$cterminals           = new SCC_Terminals();
$ctranslog            = new SCC_TransactionLogs();
$cacctsessions        = new SCC_AccountSessions();
$cterminalsessions    = new SCC_TerminalSessions();
$csites               = new SCC_Sites();
$cterminalsessiondtls = new SCC_TerminalSessionDetails();
$caudittrail          = new SCC_AuditTrail();

$capiwrapper = new RealtimeGamingAPIWrapper($rtg_url,RealtimeGamingAPIWrapper::CASHIER_API,$certFilePath,$keyFilePath);
$capicashier = new RealtimeGamingCashierAPI();

$startsession_form = new FormsProcessor();

$terminals                    = $cterminals->SelectTerminalsForStartSession($_SESSION['siteidcashier']);
$terminal_list                = new ArrayList();
$terminal_list->AddArray($terminals);
$ddlTerminal                  = new ComboBox("ddlTerminal","ddlTerminal","Gaming Terminal: ");
$options                      = null;
$options[]                    = new ListItem("----","0",true);
$ddlTerminal->Items           = $options;
$ddlTerminal->DataSource      = $terminal_list;
$ddlTerminal->DataSourceText  = "Name";
$ddlTerminal->DataSourceValue = "ID";
$ddlTerminal->DataBind();

$ddlAmount        = new ComboBox("ddlAmount","ddlAmount","Points:");
$options          = null;
$options[]        = new ListItem("----","0",true);
$options[]        = new ListItem("1","1");
$options[]        = new ListItem("5","5");
$options[]        = new ListItem("10","10");
$options[]        = new ListItem("20","20");
$options[]        = new ListItem("50","50");
$options[]        = new ListItem("100","100");
$ddlAmount->Items = $options;
$ddlAmount->Args  = "onchange='javascript: return disableText();'";

$txtAmount         = new TextBox("txtAmount","txtAmount","Amount");
$txtAmount->Length = 10;
$txtAmount->Args   = "maxlength=\"10\"  style=\"width:100px;\" onkeypress='javascript: return isNumberKey(event);'";
//onblur='javascript: return onchange_amounttxtbox();'

$btnStartSession           = new Button("btnStartSession","btnStartSession","START SESSION");
$btnStartSession->CssClass = "labelbutton2";
$btnStartSession->Args     = 'onclick="javascript: return checkinput();"';

$btnOkay           = new Button("btnOkay","btnOkay"," ");
$btnOkay->IsSubmit = true;
$btnOkay->CssClass = "inputBoxEffectPopup";

$startsession_form->AddControl($ddlTerminal);
$startsession_form->AddControl($ddlAmount);
$startsession_form->AddControl($txtAmount);
$startsession_form->AddControl($btnStartSession);
$startsession_form->AddControl($btnOkay);

$startsession_form->ProcessForms();

if ( $startsession_form->IsPostBack )
{

    if ( $btnOkay->SubmittedValue == " " )
    {
        switch ($provider)
        {
            Case 3:
                App::LoadModuleClass("SweepsCenter","SCCSM_AgentSessions");
                App::LoadLibrary("MicrogamingAPI.class.php");
                $csmagentsessions = new SCCSM_AgentSessions();

                $terminalsessionid = udate('YmdHisu');
                $terminal          = $ddlTerminal->SubmittedValue;
                if ( $ddlAmount->SubmittedValue != 0 )
                {
                    $amount = $ddlAmount->SubmittedValue;
                }
                else
                {
                    $amount = $txtAmount->SubmittedValue;
                }
                $arrterminalname = $cterminals->SelectTerminalName($terminal);

                $terminalname = $arrterminalname[0]["Name"];

                $sessionguid    = $csmagentsessions->SelectSessionGUID();
                $sessionGUID_MG = $sessionguid[0]["SessionGUID"];

                $ip_add_MG = '172.20.60.15';
                $headers   = "<AgentSession xmlns='https://entservices.totalegame.net'>" .
                          "<SessionGUID>" . $sessionGUID_MG . "</SessionGUID>" .
                          "<IPAddress>" . $ip_add_MG . "</IPAddress>" .
                          "<ErrorCode>0</ErrorCode>" .
                          "<IsLengthenSession>true</IsLengthenSession>" .
                          "</AgentSession>";
                $client    = new nusoap_client($mg_url,'wsdl');
                $client->setHeaders($headers);
                $param     = array('delimitedAccountNumbers' => $terminalname);

                //insert to API logs
                $filename = "../../APILogs/logs.txt";
                $fp       = fopen($filename,"a");
                fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GetAccountBalance || " . $terminalname . " || " . serialize($param) . "\r\n");
                fclose($fp);

                $a = 1;
                while ($a < 4)
                {
                    $result = $client->call('GetAccountBalance',$param);

                    if ( $result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false' )
                    {
                        $a++;
                    }
                    else
                    {
                        break;
                    }
                }
                $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];

                //insert to API logs
                $string   = serialize($result);
                $filename = "../../APILogs/logs.txt";
                $fp       = fopen($filename,"a");
                fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GET TERMINAL BALANCE(DEPOSIT) || " . $terminalname . " || " . $string . "\r\n");
                fclose($fp);

                //start session
                $sessiondtls = $cacctsessions->SelectSessionDetails($_SESSION['sid'],$_SESSION['siteidcashier']);
                if ( count($sessiondtls) > 0 )
                {
                    $pendingdtails = $ctranslog->CheckPendingTransactionIfExists($terminal);
                    if ( count($pendingdtails) > 0 )
                    {
                        $error_title = "ERROR";
                        $error_msg   = "You cannot load points at the moment. There is still a pending transaction for this terminal. Please try again later.";
                    }
                    else
                    {


                        if ( $balance > 0 )
                        {
                            $error_title = "INVALID DEPOSIT";
                            $error_msg   = "Balance must be zero.";
                        }
                        else
                        {
                            $param    = array('accountNumber' => $terminalname,'amount'        => $amount,'currency'      => 1);
                            //insert to API logs
                            $filename = "../../APILogs/logs.txt";
                            $fp       = fopen($filename,"a");
                            fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: Deposit || " . $terminalname . " || " . serialize($param) . "\r\n");
                            fclose($fp);
                            $result   = $client->call('Deposit',$param);

                            $filename = "../../APILogs/logs.txt";
                            $fp       = fopen($filename,"a");
                            fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || " . serialize($result) . "\r\n");
                            fclose($fp);

                            //start add 01182013 mtcc
                            $filename = "../../XMLLogs/apixml.txt";
                            $fp       = fopen($filename,"a");
                            fwrite($fp,"DEPOSIT REQUEST: " . date("Y-m-d H:i:s") . " || " . $terminalname . " || " . $client->request . "\r\n");
                            fwrite($fp,"DEPOSIT RESPONSE: " . date("Y-m-d H:i:s") . " || " . $terminalname . " || " . $client->response . "\r\n");
                            fclose($fp);
                            //end add 01182013 mtcc

                            $mgtransid     = $result['DepositResult']['TransactionId'];
                            $mgtransstatus = $result['DepositResult']['IsSucceed'];

                            //insert to transaction logs
                            $ctranslog->StartTransaction();
                            $arrTransactionLog["TerminalSessionID"] = $terminalsessionid;
                            $arrTransactionLog["TerminalID"]        = $terminal;
                            $arrTransactionLog["ServiceID"]         = 4;
                            $arrTransactionLog["DateCreated"]       = 'now_usec()';
                            $arrTransactionLog["DateUpdated"]       = 'now_usec()';
                            $arrTransactionLog["TransactionType"]   = 'D';
                            $arrTransactionLog["Amount"]            = $amount;
                            $arrTransactionLog["LaunchPad"]         = 0;
                            $ctranslog->Insert($arrTransactionLog);
                            if ( $ctranslog->HasError )
                            {
                                $ctranslog->RollBackTransaction();
                                $error_title = "ERROR";
                                $error_msg   = "INSERT Failed - Failed to insert tbl_transactionlogs";
                            }
                            else
                            {
                                $ctranslog->CommitTransaction();

                                //insert to DB logs
                                $filename = "../../DBLogs/logs.txt";
                                $fp       = fopen($filename,"a");
                                fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || INSERT INTO tbl_transactionlogs: " . serialize($arrTransactionLog) . "\r\n");
                                fclose($fp);

                                if ( $mgtransstatus == 'true' )
                                {
                                    $mgtransstat = '1';

                                    //update transactionlog
                                    $ctranslog->StartTransaction();
                                    $ctranslog->UpdateTransactionLog($mgtransid,$mgtransstatus,$mgtransstat,$terminalsessionid);
                                    if ( $ctranslog->HasError )
                                    {
                                        $ctranslog->RollBackTransaction();
                                        $error_title = "ERROR";
                                        $error_msg   = "Update Failed - Failed to update tbl_transactionlogs";
                                    }
                                    else
                                    {
                                        $ctranslog->CommitTransaction();
                                        //insert to DB logs
                                        $filename = "../../DBLogs/logs.txt";
                                        $fp       = fopen($filename,"a");
                                        fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || UPDATE tbl_transactionlogs: " . $mgtransid . ';' . $mgtransstatus . ';' . $mgtransstat . ';' . $terminalsessionid . "\r\n");
                                        fclose($fp);

                                        $bal             = $_SESSION['Bal'] - $amount;
                                        $_SESSION['Bal'] = $bal;

                                        $terminaldtls = $cterminals->SelectTerminalDetails($terminal);
                                        if ( count($terminaldtls) > 0 )
                                        {
                                            $error_title = "ERROR";
                                            $error_msg   = "Terminal is already in use.";
                                        }
                                        else
                                        {
                                            $terminalsessiondtls = $cterminalsessions->SelectTerminalSessionDetails($terminal);
                                            if ( count($terminalsessiondtls) > 0 )
                                            {
                                                $error_title = "ERROR";
                                                $error_msg   = "Terminal session already exists.";
                                            }
                                            else
                                            {
                                                if ( ($amount == 0) && ($isFreeEntry != 1) )
                                                {
                                                    $error_title = "ERROR";
                                                    $error_msg   = "Initial deposit amount must be greater than 0.";
                                                }
                                                else
                                                {
                                                    $terminalDetails = $cterminals->SelectTerminalName($terminal);
                                                    $isFreeEntry     = $terminalDetails[0]['IsFreeEntry'];
                                                    $siteid          = $sessiondtls[0]['SiteID'];
                                                    $acctid          = $sessiondtls[0]['ID'];
                                                    $sitedtls        = $csites->SelectSiteDetails($siteid);
                                                    $balance2        = $sitedtls[0]['Balance'];
                                                    if ( $balance2 < $amount )
                                                    {
                                                        $error_title = "ERROR";
                                                        $error_msg   = "Account balance is less than the deposit amount.";
                                                    }
                                                    else
                                                    {
                                                        if ( $isFreeEntry != 1 )
                                                        {
                                                            $newbalance = ($balance2 - $amount);
                                                            $csites->StartTransaction();
                                                            $csites->UpdateSiteBalance($newbalance,$siteid);
                                                            if ( $csites->HasError )
                                                            {
                                                                $csites->RollBackTransaction();
                                                                $error_title = "ERROR";
                                                                $error_msg   = "Error updating site balance.";
                                                            }
                                                            else
                                                            {
                                                                $csites->CommitTransaction();
                                                                //insert to DB logs
                                                                $filename = "../../DBLogs/logs.txt";
                                                                $fp       = fopen($filename,"a");
                                                                fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || UPDATE tbl_sites: " . $siteid . ';' . $newbalance . " \r\n");
                                                                fclose($fp);

                                                                $cterminals->StartTransaction();
                                                                $cterminals->UpdateTerminalBalanceServiceIDEquivalentPoints($amount,$terminal);
                                                                if ( $cterminals->HasError )
                                                                {
                                                                    $cterminals->RollBackTransaction();
                                                                    $error_title = "ERROR";
                                                                    $error_msg   = "Error updating terminal balance.";
                                                                }
                                                                else
                                                                {
                                                                    $cterminals->CommitTransaction();
                                                                    //insert to DB logs
                                                                    $filename = "../../DBLogs/logs.txt";
                                                                    $fp       = fopen($filename,"a");
                                                                    fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || UPDATE tbl_terminals: " . $terminal . ';' . $amount . " \r\n");
                                                                    fclose($fp);

                                                                    $cterminalsessions->StartTransaction();
                                                                    $arrTerminalSessions["TerminalID"]  = $terminal;
                                                                    $arrTerminalSessions["SiteID"]      = $_SESSION['siteidcashier'];
                                                                    $arrTerminalSessions["RemoteIP"]    = $_SERVER['REMOTE_ADDR'];
                                                                    $arrTerminalSessions["DateStart"]   = 'now_usec()';
                                                                    $arrTerminalSessions["DateCreated"] = 'now_usec()';
                                                                    $cterminalsessions->Insert($arrTerminalSessions);
                                                                    if ( $cterminalsessions->HasError )
                                                                    {
                                                                        $cterminalsessions->RollBackTransaction();
                                                                        $error_title = "INVALID START SESSION";
                                                                        $error_msg   = "Error inserting new terminal session.";
                                                                    }
                                                                    else
                                                                    {
                                                                        $cterminalsessions->CommitTransaction();
                                                                        $terminalsessionlastinsertid = $cterminalsessions->LastInsertID;
                                                                        //insert to DB logs
                                                                        $filename                    = "../../DBLogs/logs.txt";
                                                                        $fp                          = fopen($filename,"a");
                                                                        fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || INSERT INTO tbl_terminalsessions: " . serialize($arrTerminalSessions) . " \r\n");
                                                                        fclose($fp);

                                                                        $terminalsessiondetails = $cterminalsessiondtls->SelectMaxId();
                                                                        $lastTransId            = ($terminalsessiondetails[0]["max"] != null) ? $terminalsessiondetails[0]["max"] : 0;
                                                                        $lastTransId            = ($lastTransId + 1);

                                                                        $transRefId = 'D' . str_pad($terminalsessionlastinsertid,6,0,STR_PAD_LEFT) . str_pad($lastTransId,10,0,STR_PAD_LEFT);

                                                                        $cterminalsessiondtls->StartTransaction();
                                                                        $arrTerminalSessionDetails["TerminalSessionID"]    = $terminalsessionlastinsertid;
                                                                        $arrTerminalSessionDetails["TransactionType"]      = 'D';
                                                                        $arrTerminalSessionDetails["Amount"]               = $amount;
                                                                        $arrTerminalSessionDetails["ServiceID"]            = '1';
                                                                        $arrTerminalSessionDetails["ServiceTransactionID"] = $mgtransid;
                                                                        $arrTerminalSessionDetails["TransID"]              = $transRefId;
                                                                        $arrTerminalSessionDetails["AcctID"]               = $acctid;
                                                                        $arrTerminalSessionDetails["TransDate"]            = 'now_usec()';
                                                                        $arrTerminalSessionDetails["DateCreated"]          = 'now_usec()';
                                                                        $cterminalsessiondtls->Insert($arrTerminalSessionDetails);
                                                                        if ( $cterminalsessiondtls->HasError )
                                                                        {
                                                                            $cterminalsessiondtls->RollBackTransaction();
                                                                            $error_title = "ERROR";
                                                                            $error_msg   = "Error inserting new terminal session details.";
                                                                        }
                                                                        else
                                                                        {
                                                                            $cterminalsessiondtls->CommitTransaction();
                                                                            //insert to DB logs
                                                                            $filename = "../../DBLogs/logs.txt";
                                                                            $fp       = fopen($filename,"a");
                                                                            fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || INSERT INTO tbl_terminalsessiondetails: " . serialize($arrTerminalSessionDetails) . " \r\n");

                                                                            fclose($fp);

                                                                            $caudittrail->StartTransaction();
                                                                            $arrAuditTrail["SessionID"]     = $_SESSION['sid'];
                                                                            $arrAuditTrail["AccountID"]     = $acctid;
                                                                            $arrAuditTrail["TransDetails"]  = 'Session start for terminalid: ' . $terminal;
                                                                            $arrAuditTrail["RemoteIP"]      = $_SERVER['REMOTE_ADDR'];
                                                                            $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                                                            $caudittrail->Insert($arrAuditTrail);
                                                                            if ( $caudittrail->HasError )
                                                                            {
                                                                                $caudittrail->RollBackTransaction();
                                                                                $error_title = "ERROR";
                                                                                $error_msg   = "Error inserting in audit trail.";
                                                                            }
                                                                            else
                                                                            {
                                                                                $caudittrail->CommitTransaction();
                                                                                //insert to DB logs
                                                                                $filename = "../../DBLogs/logs.txt";
                                                                                $fp       = fopen($filename,"a");
                                                                                fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || INSERT INTO tbl_audittrail: " . serialize($arrAuditTrail) . " \r\n");
                                                                                fclose($fp);

                                                                                //$error_title = "SUCCESS";
                                                                                //$error_msg = "Session successfully started.";

                                                                                $transdate = date("Y-m-d h:i:s A");
                                                                                $url       = "\"print.php?points=" . $amount . "&terminal=" . $terminalname . "&refnum=" . $transRefId . "&transdate=" . $transdate . "\"";
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    $mgtransstat = '2';

                                    //update transactionlog
                                    $ctranslog->StartTransaction();
                                    $ctranslog->UpdateTransactionLog($mgtransid,$mgtransstatus,$mgtransstat,$terminalsessionid);
                                    if ( $ctranslog->HasError )
                                    {
                                        $ctranslog->RollBackTransaction();
                                        $error_title = "RESET PASSWORD";
                                        $error_msg   = "Update Failed - Failed to update tbl_transactionlogs";
                                    }
                                    else
                                    {
                                        $ctranslog->CommitTransaction();
                                        //insert to logs
                                        $filename    = "../../DBLogs/logs.txt";
                                        $fp          = fopen($filename,"a");
                                        fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || UPDATE tbl_transactionlogs: " . $mgtransid . ';' . $mgtransstatus . ';' . $mgtransstat . ';' . $terminalsessionid . "\r\n");
                                        fclose($fp);
                                        $error_title = "INVALID DEPOSIT";
                                        $error_msg   = "An Error Occured. Please try again. API Deposit Error.";
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    $error_title = "ERROR";
                    $error_msg   = "Cashier account session does not exist or has expired. Kindly log-in again.";
                }
                break;


            Case 1:
                //check if cashier session exists        
                $sessionsdtls = $cacctsessions->SelectSessionDetails($_SESSION['sid'],$_SESSION['siteidcashier']);
                if ( count($sessionsdtls) > 0 )
                {
                    $terminalsessionid = udate('YmdHisu');

                    $terminal = $ddlTerminal->SubmittedValue;
                    if ( $ddlAmount->SubmittedValue != 0 )
                    {
                        $amount = $ddlAmount->SubmittedValue;
                    }
                    else
                    {
                        $amount = $txtAmount->SubmittedValue;
                    }
                    //check the terminal's session
                    $terminalsessiondtls = $cterminalsessions->SelectTerminalSessionDetails($terminal); // 
                    if ( count($terminalsessiondtls) > 0 )
                    {
                        $error_title = "ERROR";
                        $error_msg   = "Terminal session already exists.";
                    }
                    else
                    {
                        //check the terminal's balance
                        $terminaldtls = $cterminals->SelectTerminalDetails($terminal);

                        if ( count($terminaldtls) > 0 )
                        {
                            $error_title = "ERROR";
                            $error_msg   = "Terminal is already in use.";
                        }
                        else
                        {

                            $isFreeEntrydtls = $cterminals->SelectTerminalName($terminal);
                            $isFreeEntry     = $isFreeEntrydtls[0]['IsFreeEntry']; //0
                            $siteid          = $sessionsdtls[0]['SiteID']; //1
                            $acctid          = $sessionsdtls[0]['ID'];
                            //check if the deposit amount exceeds the site's SCF
                            $sitedtls        = $csites->SelectSiteDetails($siteid);
                            $sitebalance     = $sitedtls[0]['Balance'];

                            if ( $sitebalance < $amount )
                            {
                                $error_title = "ERROR";
                                $error_msg   = "Insufficient SCF.";
                            }
                            else
                            {
                                //check the deposit amount
                                if ( ($amount == 0) && ($isFreeEntry != 1) )
                                {
                                    $error_title = "ERROR";
                                    $error_msg   = "Initial deposit amount must be greater than 0.";
                                }
                                else
                                {
                                    //get the terminal's name
                                    $arrterminalname = $cterminals->SelectTerminalName($terminal);

                                    $terminalname = $arrterminalname[0]["Name"];
                                    $param        = array('delimitedAccountNumbers' => $terminalname);
                                    //insert to logs before the get balance api call
                                    $filename     = "../../APILogs/logs.txt";
                                    $fp           = fopen($filename,"a");
                                    fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GetAccountBalance || " . $terminalname . " || " . serialize($param) . "\r\n");
                                    fclose($fp);

                                    $a = 1;
                                    while ($a < 4)
                                    {
                                        //call the get balance api
                                        $rtgbalance = $capiwrapper->GetBalance($terminalname);
                                        if ( $rtgbalance['IsSucceed'] == 1 )
                                        {
                                            $balance = $rtgbalance['BalanceInfo']['Balance'];
                                            break;
                                        }
                                        else
                                        {
                                            $a++;
                                        }
                                        break;
                                    }
                                      
                                    //insert to logs after the get balance api call
                                    $filename = "../../APILogs/logs.txt";
                                    $fp       = fopen($filename,"a");
                                    fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GET TERMINAL BALANCE(DEPOSIT) || " . $terminalname . " || " . serialize($rtgbalance) . "\r\n");
                                    fclose($fp);
                                    //check if balance is greater than zero (otherwise, ther terminal must not be allowed to deposit)
                                    if ( $balance > 0 )
                                    {
                                        $error_title = "INVALID DEPOSIT";
                                        $error_msg   = "Casino balance must be zero.";
                                    }
                                    else
                                    {
                                        //insert to tbl_transactionlogs table
                                        $ctranslog->StartTransaction();
                                        $arrTransactionLog["TerminalSessionID"] = $terminalsessionid;
                                        $arrTransactionLog["TerminalID"]        = $terminal;
                                        $arrTransactionLog["ServiceID"]         = 4;
                                        $arrTransactionLog["DateCreated"]       = 'now_usec()';
                                        $arrTransactionLog["DateUpdated"]       = 'now_usec()';
                                        $arrTransactionLog["TransactionType"]   = 'D';
                                        $arrTransactionLog["Amount"]            = $amount;
                                        $arrTransactionLog["LaunchPad"]         = 0;
                                        $ctranslog->Insert($arrTransactionLog);

                                        if ( $ctranslog->HasError )
                                        {
                                            $ctranslog->RollBackTransaction();
                                            $error_title = "ERROR";
                                            $error_msg   = "INSERT Failed - Failed to insert in tbl_transactionlogs";
                                        }
                                        else
                                        {
                                            $ctranslog->CommitTransaction();
                                            //insert to logs
                                            $param    = array('delimitedAccountNumbers' => $terminalname);
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp       = fopen($filename,"a");
                                            fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || INSERT INTO tbl_transactionlogs: " . serialize($arrTransactionLog) . "\r\n");
                                            fclose($fp);

                                            //insert to logs before the deposit api call
                                            $param    = array('accountNumber' => $terminalname,'amount'        => $amount,'currency'      => 1);
                                            $filename = "../../APILogs/logs.txt";
                                            $fp       = fopen($filename,"a");
                                            fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: Deposit || " . $terminalname . " || " . serialize($param) . "\r\n");
                                            fclose($fp);

                                            $rtgdeposit = $capiwrapper->Deposit($terminalname, $amount, $tracking1 = '', $tracking2 = '', $tracking3 = '', $tracking4 = '');
                                            $rtgtransstatus = $rtgdeposit['TransactionInfo']['DepositGenericResult']['transactionStatus'];
                                            $rtgtransid     = $rtgdeposit['TransactionInfo']['DepositGenericResult']['transactionID'];
                                            if ( $rtgdeposit['TransactionInfo']['DepositGenericResult']['transactionStatus'] == 'TRANSACTIONSTATUS_APPROVED' )
                                            {
                                                $rtgtransstat = '1';
                                                //insert to logs after the reload api call
                                                $filename     = "../../APILogs/logs.txt";
                                                $fp           = fopen($filename,"a");
                                                fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || " . serialize($rtgdeposit) . "\r\n");
                                                fclose($fp);
                                                //update the tbl_transactionlogs table
                                                $ctranslog->StartTransaction();
                                                $ctranslog->UpdateTransactionLog($rtgtransid,$rtgtransstatus,$rtgtransstat,$terminalsessionid);
                                                if ( $ctranslog->HasError )
                                                {
                                                    $ctranslog->RollBackTransaction();
                                                    $error_title = "ERROR";
                                                    $error_msg   = "Update Failed - Failed to update tbl_transactionlogs";
                                                }
                                                else
                                                {
                                                    $ctranslog->CommitTransaction();
                                                    //insert to db logs the updating of tbl_transactionlogs
                                                    $filename = "../../DBLogs/logs.txt";
                                                    $fp       = fopen($filename,"a");
                                                    fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || UPDATE tbl_transactionlogs: " . $rtgtransid . ';' . $rtgtransstatus . ';' . $rtgtransstat . ';' . $terminalsessionid . "\r\n");
                                                    fclose($fp);
                                                    if ( $isFreeEntry != 1 )
                                                    {
                                                        //update the site's balance
                                                        $newbalance = ($sitebalance - $amount);
                                                        $csites->StartTransaction();
                                                        $csites->UpdateSiteBalance($newbalance,$siteid);
                                                        if ( $csites->HasError )
                                                        {
                                                            $csites->RollBackTransaction();
                                                            $error_title = "ERROR";
                                                            $error_msg   = "Error updating site balance.";
                                                        }
                                                        else
                                                        {
                                                            $csites->CommitTransaction();
                                                            //insert to db logs
                                                            $filename = "../../DBLogs/logs.txt";
                                                            $fp       = fopen($filename,"a");
                                                            fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || UPDATE tbl_sites: " . $siteid . ';' . $newbalance . " \r\n");
                                                            fclose($fp);
                                                            //update the terminal's balance
                                                            $cterminals->StartTransaction();
                                                            $cterminals->UpdateTerminalBalanceServiceIDEquivalentPoints($amount,$terminal);
                                                            if ( $cterminals->HasError )
                                                            {
                                                                $cterminals->RollBackTransaction();
                                                                $error_title = "ERROR";
                                                                $error_msg   = "Error updating terminal balance.";
                                                            }
                                                            else
                                                            {
                                                                $cterminals->CommitTransaction();
                                                                //insert to db logs
                                                                $filename                           = "../../DBLogs/logs.txt";
                                                                $fp                                 = fopen($filename,"a");
                                                                fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || UPDATE tbl_terminals: " . $terminal . ';' . $amount . " \r\n");
                                                                fclose($fp);
                                                                //insert into the terminal sessions table
                                                                $cterminalsessions->StartTransaction();
                                                                $arrTerminalSessions["TerminalID"]  = $terminal;
                                                                $arrTerminalSessions["SiteID"]      = $siteid;
                                                                $arrTerminalSessions["RemoteIP"]    = $_SERVER['REMOTE_ADDR'];
                                                                $arrTerminalSessions["DateStart"]   = 'now_usec()';
                                                                $arrTerminalSessions["DateCreated"] = 'now_usec()';
                                                                $cterminalsessions->Insert($arrTerminalSessions);
                                                                if ( $cterminalsessions->HasError )
                                                                {
                                                                    $cterminalsessions->RollBackTransaction();
                                                                    $error_title = "INVALID START SESSION";
                                                                    $error_msg   = "Error inserting new terminal session.";
                                                                }
                                                                else
                                                                {
                                                                    $cterminalsessions->CommitTransaction();
                                                                    $terminalsessionlastinsertid = $cterminalsessions->LastInsertID;
                                                                    //insert to db logs
                                                                    $filename                    = "../../DBLogs/logs.txt";
                                                                    $fp                          = fopen($filename,"a");
                                                                    fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || INSERT INTO tbl_terminalsessions: " . serialize($arrTerminalSessions) . " \r\n");

                                                                    fclose($fp);
                                                                    //formulate SWC deposit reference id
                                                                    $terminalsessiondetails                            = $cterminalsessiondtls->SelectMaxId();
                                                                    $lastTransId                                       = ($terminalsessiondetails[0]["max"] != null) ? $terminalsessiondetails[0]["max"] : 0;
                                                                    $lastTransId                                       = ($lastTransId + 1);
                                                                    $transRefId                                        = 'D' . str_pad($terminalsessionlastinsertid,6,0,STR_PAD_LEFT) . str_pad($lastTransId,10,0,STR_PAD_LEFT);
                                                                    //insert the terminal session details
                                                                    $cterminalsessiondtls->StartTransaction();
                                                                    $arrTerminalSessionDetails["TerminalSessionID"]    = $terminalsessionlastinsertid;
                                                                    $arrTerminalSessionDetails["TransactionType"]      = 'D';
                                                                    $arrTerminalSessionDetails["Amount"]               = $amount;
                                                                    $arrTerminalSessionDetails["ServiceID"]            = '1';
                                                                    $arrTerminalSessionDetails["ServiceTransactionID"] = $rtgtransid;
                                                                    $arrTerminalSessionDetails["TransID"]              = $transRefId;
                                                                    $arrTerminalSessionDetails["AcctID"]               = $acctid;
                                                                    $arrTerminalSessionDetails["TransDate"]            = 'now_usec()';
                                                                    $arrTerminalSessionDetails["DateCreated"]          = 'now_usec()';
                                                                    $cterminalsessiondtls->Insert($arrTerminalSessionDetails);
                                                                    if ( $cterminalsessiondtls->HasError )
                                                                    {
                                                                        $cterminalsessiondtls->RollBackTransaction();
                                                                        $error_title = "ERROR";
                                                                        $error_msg   = "Error inserting new terminal session details.";
                                                                    }
                                                                    else
                                                                    {
                                                                        $cterminalsessiondtls->CommitTransaction();
                                                                        //insert to db logs
                                                                        $filename                       = "../../DBLogs/logs.txt";
                                                                        $fp                             = fopen($filename,"a");
                                                                        fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || INSERT INTO tbl_terminalsessiondetails: " . serialize($arrTerminalSessionDetails) . " \r\n");
                                                                        fclose($fp);
                                                                        //insert in the audit trail table
                                                                        $caudittrail->StartTransaction();
                                                                        $arrAuditTrail["SessionID"]     = $_SESSION['sid'];
                                                                        $arrAuditTrail["AccountID"]     = $acctid;
                                                                        $arrAuditTrail["TransDetails"]  = 'Session start for terminalid: ' . $terminal;
                                                                        $arrAuditTrail["RemoteIP"]      = $_SERVER['REMOTE_ADDR'];
                                                                        $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                                                        $caudittrail->Insert($arrAuditTrail);
                                                                        if ( $caudittrail->HasError )
                                                                        {
                                                                            $caudittrail->RollBackTransaction();
                                                                            $error_title = "ERROR";
                                                                            $error_msg   = "Error inserting in audit trail.";
                                                                        }
                                                                        else
                                                                        {
                                                                            $caudittrail->CommitTransaction();
                                                                            //insert to db logs
                                                                            $filename  = "../../DBLogs/logs.txt";
                                                                            $fp        = fopen($filename,"a");
                                                                            fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || INSERT INTO tbl_audittrail: " . serialize($arrAuditTrail) . " \r\n");
                                                                            fclose($fp);
                                                                            $transdate = date("Y-m-d h:i:s A");
                                                                            $url       = "\"print.php?points=" . $amount . "&terminal=" . $terminalname . "&refnum=" . $transRefId . "&transdate=" . $transdate . "\"";
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        $error_title = "ERROR";
                                                        $error_msg   = "Terminal is not valid for loading.";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                $rtgtransstat = '2';
                                                $ctranslog->StartTransaction();
                                                $ctranslog->UpdateTransactionLog($rtgtransid,$rtgtransstatus,$rtgtransstat,$terminalsessionid);
                                                if ( $ctranslog->HasError )
                                                {
                                                    $ctranslog->RollBackTransaction();
                                                    $error_title = "ERROR";
                                                    $error_msg   = "Update Failed - Failed to update tbl_transactionlogs";
                                                }
                                                else
                                                {
                                                    $ctranslog->CommitTransaction();
                                                    //insert to logs
                                                    $filename    = "../../DBLogs/logs.txt";
                                                    $fp          = fopen($filename,"a");
                                                    fwrite($fp,date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || " . $terminalname . " || UPDATE tbl_transactionlogs : " . $rtgtransid . ';' . $rtgtransstatus . ';' . $rtgtransstat . ';' . $terminalsessionid . " \r\n");
                                                    fclose($fp);
                                                    $error_title = "ERROR";
                                                    $error_msg   = "Transaction denied. Please try again later.";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    $error_title = "ERROR";
                    $error_msg   = "Cashier account session does not exist or has expired. Kindly log-in again.";
                }
                break;
        }
    }
}
?>
