<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 05, 2012
 */
require_once('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCCC_TransactionSummary");
App::LoadModuleClass("SweepsCenter", "SCCC_DeckInfo");

App::LoadControl("ComboBox");
App::LoadControl("Button");

$cterminals = new SCC_Terminals();
$cctranssummary = new SCCC_TransactionSummary();
$ccdeckinfo = new SCCC_DeckInfo();
$javascripts[] = "jscripts/checking.js";
$pagename = "transhistwinnings";

$transhistwinnings_form = new FormsProcessor();

$terminals = $cterminals->SelectTerminalsForCardDisplay($_SESSION['siteidcashier']);
$terminal_list = new ArrayList();
$terminal_list->AddArray($terminals);
$ddlternme = new ComboBox("ddlternme","ddlternme","Terminal");
$options = null;
$options[] = new ListItem("----","-1",true);
$ddlternme->Items = $options;
$ddlternme->DataSource = $terminal_list;
$ddlternme->DataSourceText = "Name";
$ddlternme->DataSourceValue = "ID";
$ddlternme->DataBind();
$ddlternme->Args = 'onchange="gettransnum();"';

$ddltransno = new ComboBox("ddltransno","ddltransno","Transaction Reference ID");
$ddltransno->Enabled = false;
$options = null;
$options[] = new ListItem("Select One","-1",true);
$ddltransno->Items = $options;

$btnSearch = new Button("btnSearch","btnSearch","Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = 'onclick="javascript: return checkinput2();"';

$transhistwinnings_form->AddControl($ddlternme);
$transhistwinnings_form->AddControl($ddltransno);
$transhistwinnings_form->AddControl($btnSearch);

$transhistwinnings_form->ProcessForms();
if($transhistwinnings_form->IsPostBack)
{
    if($btnSearch->SubmittedValue == "Search")
    {
        $terminalid = $ddlternme->SubmittedValue;
        $transsummnum = $ddltransno->SubmittedValue;
        $ddltransno->Enabled = true;
        $transref_array = array();
        $transactionreferences = $cctranssummary->SelectTransactionSummaryPerTerminal($terminalid);
        for($i = 0 ; $i < count($transactionreferences) ; $i++)
        {
            $transref = array("TransactionSummID" => $transactionreferences[$i]["TransactionSummaryID"] , "TransactionSummaryID" => str_pad($transactionreferences[$i]["TransactionSummaryID"],14,"0",STR_PAD_LEFT));
            array_push($transref_array , $transref);
        }
        $transactionreferences_list = new ArrayList();
        $transactionreferences_list->AddArray($transref_array);
        $ddltransno->DataSource = $transactionreferences_list;
        $ddltransno->DataSourceText = "TransactionSummaryID";
        $ddltransno->DataSourceValue = "TransactionSummID";
        $ddltransno->DataBind();
        $ddltransno->SetSelectedValue($transsummnum);

        $deckdtls = $ccdeckinfo->SelectLiveDeckNumber();
        $deckid = $deckdtls[0]["DeckID"];

        $winningsdtls = $cctranssummary->SelectWinningsSummary($terminalid,$transsummnum);
        $transsummid = $winningsdtls[0]['TransactionSummaryID'];
        
                $lastdig = strpos($winningsdtls[0]["Winnings"],"h");
                $diglenth =  strlen($winningsdtls[0]["Winnings"]);
               if($lastdig + 1 == $diglenth)
              { 
                 if(strpos($winningsdtls[0]["Winnings"],'$') == 0)
                       $winnings = $winningsdtls[0]["Winnings"];
                else if (strpos($winningsdtls[0]["Winnings"],',') == 0)
                      $winnings = "$".substr($winningsdtls[0]["Winnings"],1); 
              }else
              {
                if(strpos($winningsdtls[0]["Winnings"],'$') == 0)
                     $winnings = $winningsdtls[0]["Winnings"];
                else if(strpos($winningsdtls[0]["Winnings"],'(') == 1 || strpos($winningsdtls[0]["Winnings"],'(') == 2)
                       $winnings = substr($winningsdtls[0]["Winnings"],1); 
                else if (strpos($winningsdtls[0]["Winnings"],',') == 0)
                       $winnings = substr($winningsdtls[0]["Winnings"],0); 
              } 
        if(count($deckdtls) > 0)
        {
            $displayrecords = "ok";
            $carddtls = $ccdeckinfo->SelectCardsFromLiveDeck($deckid,$transsummid);
        }
        else
        {
            $winningdtls = "Deck Id empty";
        }
    }
}
?>