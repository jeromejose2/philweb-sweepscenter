<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 22, 2012
 */
require_once('../init.inc.php');
App::LoadModuleClass("SweepsCenter", "SCCC_TransactionSummary");
App::LoadModuleClass("SweepsCenter", "SCCC_Winners");
App::LoadModuleClass("SweepsCenter", "SCCC_DeckInfo");
App::LoadModuleClass("SweepsCenter", "SCC_AccountSessions");
App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");

App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("PagingControl2");

$cctranssummary = new SCCC_TransactionSummary();
$ccwinners = new SCCC_Winners();
$ccdeckinfo = new SCCC_DeckInfo();
$cacctsessions = new SCC_AccountSessions();
$caudittrail = new SCC_AuditTrail();
$redemption_form = new FormsProcessor();
$pagename = "redemption";

$hidTransID = new Hidden("hidTransID","hidTransID","Hidden Transaction ID");
$hidTransRef = new Hidden("hidTransRef","hidTransRef","Hidden Transaction Reference");
$hidRowID = new Hidden("hidRowID","hidRowID","Hidden Row ID");
$hidWinnings = new Hidden("hidWinnings","hidWinnings","Hidden Winnings");
$hidTerminal = new Hidden("hidTerminal","hidTerminal","Hidden Terminal");

$btnOkay = new Button("btnOkay","btnOkay"," ");
$btnOkay->IsSubmit = true;
$btnOkay->CssClass = "inputBoxEffectPopup";

$redemption_form->AddControl($hidTransID);
$redemption_form->AddControl($hidTransRef);
$redemption_form->AddControl($hidRowID);
$redemption_form->AddControl($hidWinnings);
$redemption_form->AddControl($hidTerminal);
$redemption_form->AddControl($btnOkay);

$redemption_form->ProcessForms();

/*PAGING*/
$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
/*PAGING*/

$redeemablewinnings = $cctranssummary->SelectRedeemableWinningsByBatch($_SESSION['siteidcashier']);
$redeemablewinningcount = count($redeemablewinnings);
$pgcon->Initialize($itemsperpage, $redeemablewinningcount);
$pgTransactionHistory = $pgcon->PreRender();
$redeemablewinnings_list = $cctranssummary->SelectRedeemableWinningsByBatchWithLimit($_SESSION['siteidcashier'],($pgcon->SelectedItemFrom - 1),$itemsperpage);

if ($redemption_form->IsPostBack)
{
    if($btnOkay->SubmittedValue == " ")
    {
        $transactionid = $hidTransID->SubmittedValue;
        $transactionref = $hidTransRef->SubmittedValue;
        $rowid = $hidRowID->SubmittedValue;
        $winnings = $hidWinnings->SubmittedValue;
        $terminal = $hidTerminal->SubmittedValue;
        
        $sessiondtls = $cacctsessions->SelectSessionDetails($_SESSION['sid'], $_SESSION['siteidcashier']);
        if(count($sessiondtls) > 0)
        {
            $acctid = $sessiondtls[0]["ID"];
            $ccwinners->StartTransaction();
            $ccwinners->UpdateWinningStatus($acctid, $transactionid);
            if($ccwinners->HasError)
            {
                $ccwinners->RollBackTransaction();
                $error_title = "ERROR";
                $error_msg = "Error updating winners table.";
            }
            else
            {
                $ccwinners->CommitTransaction();

                $cctranssummary->StartTransaction();
                $cctranssummary->UpdateIsClaimed($transactionid);
                if($cctranssummary->HasError)
                {
                    $cctranssummary->RollBackTransaction();
                    $error_title = "ERROR";
                    $error_msg = "Error updating transaction summary table.";
                }
                else
                {
                    $cctranssummary->CommitTransaction();

                    $deckdtls = $ccwinners->SelectDeckId($transactionid);
                    $deckid = $deckdtls[0]["DeckID"];

                    $usedcardcountdtls = $ccdeckinfo->SelectRedeemedCardCount($deckid);
                    $usedcardcount = ($usedcardcountdtls[0]["UsedWinningCardCount"] != null ) ? $usedcardcountdtls[0]["UsedWinningCardCount"] : 0;
                    $redeemedcardcount = ($usedcardcount + $rowid);

                    $ccdeckinfo->StartTransaction();
                    $ccdeckinfo->UpdateDeckNumber($deckid,$acctid,$transactionid);
                    if($ccdeckinfo->HasError)
                    {
                        $ccdeckinfo->RollBackTransaction();
                        $error_title = "ERROR";
                        $error_msg = "Error updating deck_" . $deckid;
                    }
                    else
                    {
                        $ccdeckinfo->CommitTransaction();

                        $ccdeckinfo->StartTransaction();
                        $ccdeckinfo->UpdateUsedWinningCardCount($redeemedcardcount,$deckid);
                        if($ccdeckinfo->HasError)
                        {
                            $ccdeckinfo->RollBackTransaction();
                            $error_title = "ERROR";
                            $error_msg = "Error updating deck info.";
                        }
                        else
                        {
                            $ccdeckinfo->CommitTransaction();

                            $caudittrail->StartTransaction();
                            $arrAuditTrail["SessionID"] = $_SESSION['sid'];
                            $arrAuditTrail["AccountID"] = $acctid;
                            $arrAuditTrail["TransDetails"] = "Redeemed Transaction: " . $transactionid;
                            $arrAuditTrail["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                            $arrAuditTrail["TransDateTime"] = 'now_usec()';
                            $caudittrail->Insert($arrAuditTrail);
                            if($caudittrail->HasError)
                            {
                                $caudittrail->RollBackTransaction();
                                $error_title = "ERROR";
                                $error_msg = "Error inserting in audit trail.";
                            }
                            else
                            {
                                $caudittrail->CommitTransaction();
                                $error_title = "REDEMPTION";
                                $error_msg = "You have successfully redeemed " . $hidWinnings->SubmittedValue . " for " . $hidTerminal->SubmittedValue . " with Transaction Reference ID of " . str_pad($hidTransID->SubmittedValue, 10, '0', STR_PAD_LEFT) . ".";
                            }
                        }
                    }
                }
            }
        }
        else
        {
            $error_title = "ERROR";
            $error_msg = "Account session does not exist.";
        }
    }
}
?>
