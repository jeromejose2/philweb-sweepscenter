<?php
/* * *****************
 * Author: ANGC
 * Date Created: 2013-08-24
 * Description: Get List of Terminals by cashier
 * ***************** */
require_once('../init.inc.php');

$cashiersiteid = $_SESSION['siteidcashier'];
$etype = $_GET['etype'];

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");

$cterminals = new SCC_Terminals();

    $terminals = $cterminals->SelectTerminalsForTransHistoryPayoutWithEtype($cashiersiteid,$etype);

    $options = "<option value = ''>- - - - - - - - - - - - - -</option>";
    
    if(count($terminals) > 0)
    {
        for($i = 0 ; $i < count($terminals) ; $i++)
        {
            $options .= "<option value = '" . $terminals[$i]["ID"] . "'>" . $terminals[$i]["Name"] . "</option>";
        }
    }
    if($etype == NULL || $etype == '')
    {
        $options = "";
        $options = "<option value = ''>- - - - - - - - - - - - - -</option>";
    }

echo $options;
?>