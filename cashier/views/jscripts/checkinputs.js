/*

Added by: Angelo Niño G. Cubos
Date created: December 14, 2012 
 
 */

function AlphaNumericOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;  
    if(/*DisableWhiteSpaces*/(charCode == 32) ||/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))      
    return false;

    return true;
}
function AlphaNumericAndSpaces(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;  
    if(/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))      
    return false;

    return true;
}
function AlphaOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;  
    if(/*DisableWhiteSpaces*/(charCode == 32) ||/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))      
    return false;
        var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
        // returns false if a numeric character has been entered
        return (chCode < 48 /* '0' */ || chCode > 57 /* '9' */ );
        return (chCode==32);

    return true;
}
function AlphaAndSpaces(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;  
    if(/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))      
    return false;
        var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
        // returns false if a numeric character has been entered
        return (chCode < 48 /* '0' */ || chCode > 57 /* '9' */ );
        return (chCode==32);

    return true;
}
function OnlyNumbers(event)
{
    var e = event || evt; // for trans-browser compatibility
    var charCode = e.which || e.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;

}
function verifyEmail(event){
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode == 64)||(charCode == 95)||(charCode == 46))/* @=64, _=95, .=46 characters are allowed*/
  {  
    return true;
    return true;
  }else{
  if(/*DisableWhiteSpaces*/(charCode == 32) ||/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128)) 
    return false;
    return true;
  }
}
function DisableSpacesOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if(/*DisableSpaces*/(charCode == 32))
       
        return false;

    return true;
}