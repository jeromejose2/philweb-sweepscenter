  function checkinput()
  {
  	var sDateFrom = document.getElementById("txtDateFr");
	var sDateTo = document.getElementById("txtDateTo");
        var sAccountId = document.getElementById("txtVoucherCode").value;
	var re=/^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;
	//var sortby = document.getElementById('ddlSortBy').options[document.getElementById('ddlSortBy').selectedIndex].value;
    if (sDateFrom.value.length == 0)
    {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Please enter the start date.";
        sDateFrom.focus();
        return false;
    }
    if (re.test(sDateFrom.value ) == false)
    {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Date must be mm/dd/yyyy format.";
        sDateFrom.focus();
        return false;
    }
    if (sDateTo.value.length == 0)
    {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Please enter the end date.";
        sDateTo.focus();
        return false;
    }
    if (re.test(sDateTo.value ) == false)
    {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Date must be mm/dd/yyyy format.";
        sDateTo.focus();
        return false;
    }
    var from = sDateFrom.value;
    var to = sDateTo.value;

     from = from.split('/', 3);
     from = from[2] + from[0] + from[1] ;

     to = to.split('/', 3);
     to = to[2] + to[0] + to[1] ;

    //if (sDateTo.value < sDateFrom.value)
    if (to < from)
    {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Invalid date range.";	    
        sDateFrom.focus();
        return false;
    }
    if(sAccountId == "0")
    {

        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Insufficient Data";	    
        sDateFrom.focus();
       
      
	return false;
    }
  return true;
  }
  function checkinput_reg()
  {
  	var sDateFrom = document.getElementById("txtDateFr");
	var sDateTo = document.getElementById("txtDateTo");
        //var sAccountId = document.getElementById("ddlTerminal").value;
	var re=/^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;
	//var sortby = document.getElementById('ddlSortBy').options[document.getElementById('ddlSortBy').selectedIndex].value;
  
    if (sDateFrom.value.length == 0)
    {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Please enter the start date.";
        sDateFrom.focus();
        return false;
    }
    if (re.test(sDateFrom.value ) == false)
    {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Date must be mm/dd/yyyy format.";
        sDateFrom.focus();
        return false;
    }
    if (sDateTo.value.length == 0)
    {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Please enter the end date.";
        sDateTo.focus();
        return false;
    }
    if (re.test(sDateTo.value ) == false)
    {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Date must be mm/dd/yyyy format.";
        sDateTo.focus();
        return false;
    }
    var from = sDateFrom.value;
    var to = sDateTo.value;

     from = from.split('/', 3);
     from = from[2] + from[0] + from[1] ;

     to = to.split('/', 3);
     to = to[2] + to[0] + to[1] ;

    //if (sDateTo.value < sDateFrom.value)
    if (to < from)
    {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Invalid date range.";	    
        sDateFrom.focus();
        return false;
    }
    if(sAccountId == "0")
    {

        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Insufficient Data";	    
        sDateFrom.focus();
       
      
	return false;
    }
  return true;
  }
  function checkinput2()
  {
      if(document.getElementById("ddlternme").value == -1)
      {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Please enter terminal name.";
        document.getElementById("ddlternme").focus();
        return false;
      }
      else if(document.getElementById("ddltransno").value == -1)
      {
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Please enter the terminal reference id.";
        document.getElementById("ddltransno").focus();
        return false;
      }
      else
      {
        return true;
      }
  }


  function num_only()
  {
        if (event.keyCode < 47 || event.keyCode > 57)
            return false;

        return true;
  }
  
  function isNumberKey(evt)
  {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
  }

  function disableSpace(evt)
  {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 32)
        return false;

    return true;
  }
  
  function checkemail(email)
  {
     var str=email.value
     var filter=/^.+@.+\..{2,3}$/

     if (filter.test(str))
        return true;
     else 
     {
        alert("Please input a valid email address.");
        document.getElementById("txtEmail").focus();
        return false;
      }
     return true;
  }
  
  function checkvoucherinput()
    {
        var fullname = document.getElementById("txtFullName").value;
        var bday_mos = document.getElementById("txtBday_month").value;
        var bday_day = document.getElementById("txtBday_day").value;
        var bday_year =  document.getElementById("txtBday_year").value;
        var bday = document.getElementById("txtBday").value; 
        var address = document.getElementById("txtAddress").value;
        var contact = document.getElementById("txtContact").value;
        var email = document.getElementById("txtEmail").value;
        var coupon = document.getElementById("txtCN").value;
        var hiddencn = document.getElementById("hiddencn").value;

        fullname = fullname.replace(/^\s+|\s+$/, '');
        address = address.replace(/^\s+|\s+$/, '');
        email = email.replace(/^\s+|\s+$/, '');
        contact = contact.replace(/^\s+|\s+$/, '');
        coupon = coupon.replace(/^\s+|\s+$/, '');
        
        addOrRemoveErrorFieldClass("lblfullname","txtFullName");
        addClass(elem('lblbday'), "errorField");
        //addOrRemoveErrorFieldClass("lblbday","txtBday");
        addOrRemoveErrorFieldClass("lblcont","txtContact");
        addOrRemoveErrorFieldClass("lblcn","txtCN");
        addOrRemoveErrorFieldClass("lbladdr","txtAddress");
        addOrRemoveErrorFieldClass("lblemail","txtEmail");

        if ((fullname.length == 0) || (fullname = ""))
        {
            if ((bday_year != '0000') && (bday_mos != '00') && (bday_day != '00'))
            {
                removeClass(elem('lblbday'), "errorField");
            }
            document.getElementById('title').innerHTML = "INVALID INPUT";
            document.getElementById('msg').innerHTML = "Please enter your full name.";
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
	if ((bday_year == '0000') || (bday_mos == '00') || (bday_day == '00'))
        {
            document.getElementById('title').innerHTML = "INVALID INPUT";
            document.getElementById('msg').innerHTML = "Please enter your complete birthdate.";
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
        else
        {
            removeClass(elem('lblbday'), "errorField");
        }
        
        var today = new Date();
        bday = bday_year + '-' + bday_mos + '-' + bday_day;
        var d = bday;
        if (!/\d{4}\-\d{2}\-\d{2}/.test(d))
        { // check valid format
        return false;
        }

        d = d.split("-");
        var byr = parseInt(d[0]);
        var nowyear = today.getFullYear();
        if (byr >= nowyear || byr < 1900)
        { // check valid year
            return false;
        }
        var bmth = parseInt(d[1],10)-1; // radix 10!
        if (bmth <0 || bmth >11)
        { // check valid month 0-11
            return false;
        }
        var bdy = parseInt(d[2],10); // radix 10!
        var dim = daysInMonth(bmth+1,byr);
        if (bdy <1 || bdy > dim)
        { // check valid date according to month
            return false;
        }

        var age = nowyear - byr;
        var nowmonth = today.getMonth();
        var nowday = today.getDate();
        var age_month = nowmonth - bmth;
        var age_day = nowday - bdy;
                        
        if (age < 18 )
        {
            document.getElementById('title').innerHTML = "AGE LIMIT";
            document.getElementById('msg').innerHTML = "The Sweeps Center prohibits participation of individuals below 18 years of age.";
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
        else if (age == 18 && age_month <= 0 && age_day <0)
        {
            document.getElementById('title').innerHTML = "AGE LIMIT";
            document.getElementById('msg').innerHTML = "The Sweeps Center prohibits participation of individuals below 18 years of age.";
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
        
        if ((address.length == 0) || (address = ""))
        {
            document.getElementById('title').innerHTML = "INVALID INPUT";
            document.getElementById('msg').innerHTML = "Please enter your complete address.";
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
        if ((contact.length == 0) || (contact = ""))
        {
            document.getElementById('title').innerHTML = "INVALID INPUT";
            document.getElementById('msg').innerHTML = "Please enter your contact number.";
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
        if ((email.length == 0) || (email = ""))
        {
            document.getElementById('title').innerHTML = "INVALID INPUT";
            document.getElementById('msg').innerHTML = "Please enter your email address.";
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
        else
        {
            if (echeck(document.getElementById("txtEmail").value) == false)
            {
                document.getElementById('title').innerHTML = "INVALID INPUT";
                document.getElementById('msg').innerHTML = "Please enter a valid email address.";
                document.getElementById('light1').style.display='block';
                document.getElementById('fade').style.display='block';
                return false;
            }
        }
        
        if ((coupon.length == 0) || (coupon = ""))
        {
            document.getElementById('title').innerHTML = "INVALID INPUT";
            document.getElementById('msg').innerHTML = "Please enter the coupon number.";
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
        if(hiddencn>0)
        {
            document.getElementById('title').innerHTML = "ERROR!";
            document.getElementById('msg').innerHTML = "Coupon number has already been used.";
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;            
        }
        document.getElementById('title2').innerHTML = "CONFIRMATION";
        document.getElementById('msg2').innerHTML = "Are you sure you want to register the following voucher information?";
        document.getElementById('light2').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }
    
    function echeck(str)
    {
            var at="@"
            var dot="."
            var lat=str.indexOf(at)
            var lstr=str.length

            if (str.indexOf(at)==-1){
                return false;
            }

            if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
                return false;
            }

            if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
                return false;
            }

            if (str.indexOf(at,(lat+1))!=-1){
                return false;
            }

            if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
                return false;
            }

            if (str.indexOf(dot,(lat+2))==-1){
                return false;
            }

            if (str.indexOf(" ")!=-1){
                return false;
            }

        return true;
    }
   
    function elem(id)
    {
        return document.getElementById(id);
    }

    function hasClass(ele,cls)
    {
        return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
    }

    function removeClass(ele,cls)
    {
        if (hasClass(ele,cls)) {
            var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
            ele.className=ele.className.replace(reg,' ');
        }
    }

    function addClass(ele,cls) {
        if (!this.hasClass(ele,cls)) ele.className += " "+cls;
    }

    function addOrRemoveErrorFieldClass(lblId,inputId)
    {
        var lblElem = elem(lblId);
        var inputElem = elem(inputId);

        if(inputElem.value.replace(/^\s+|\s+$/, '') == '') {
            addClass(lblElem,"errorField");
        } else {
            removeClass(lblElem,"errorField");
        }
    }
    
    function daysInMonth(month, year){
	var days = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        var no_of_days = 31;
        return no_of_days;
	//return (month == 2 && is_leapYear(year)) ? 29 : days[month-1];
    }
 

