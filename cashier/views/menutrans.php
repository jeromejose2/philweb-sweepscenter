<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 31, 2012
 */
?>
<div style="height:55px; width:100%; text-align:center;">
    <table>
        <tr>
            <td>
                <?php if ($pagename == 'transhistdeposit'):?>
                    <div class="labelbutton2"><a href="transhistdeposit.php" style="text-decoration:none; color:#000;">Sales</a></div>
                <?php else: ?>
                    <div class="labelbutton"><a href="transhistdeposit.php" style="text-decoration:none; color:#000;">Sales</a></div>
                <?php endif;?>
            </td>
            <td>
                <?php if ($pagename == 'transhistconversion'):?>
                    <div class="labelbutton2"><a href="transhistconversion.php" style="text-decoration:none; color:#000;">Points Conversion</a></div>
                <?php else: ?>
                    <div class="labelbutton"><a href="transhistconversion.php" style="text-decoration:none; color:#000;">Points Conversion</a></div>
                <?php endif;?>
            </td>
            <td>
                <?php if ($pagename == 'transhistpayout'):?>
                    <div class="labelbutton2"><a href="transhistpayout.php" style="text-decoration:none; color:#000;">Redemptions</a></div>
                <?php else: ?>
                    <div class="labelbutton"><a href="transhistpayout.php" style="text-decoration:none; color:#000;">Redemptions</a></div>
                <?php endif;?>
            </td>
            <td>
                <?php if ($pagename == 'transhistgrosshold'):?>
                    <div class="labelbutton2"><a href="transhistgrosshold.php" style="text-decoration:none; color:#000;">Cash On Hand</a></div>
                <?php else: ?>
                    <div class="labelbutton"><a href="transhistgrosshold.php" style="text-decoration:none; color:#000;">Cash On Hand</a></div>
                <?php endif;?>
            </td>
            <td>
                <?php if ($pagename == 'transhistregistrants'):?>
                    <div class="labelbutton2"><a href="transhistregistrants.php" style="text-decoration:none; color:#000;">Registrations</a></div>
                <?php else: ?>
                    <div class="labelbutton"><a href="transhistregistrants.php" style="text-decoration:none; color:#000;">Registrations</a></div>
                <?php endif;?>
            </td>
            <td>
                <?php if ($pagename == 'transhistwinnings'):?>
                    <div class="labelbutton2"><a href="transhistwinnings.php" style="text-decoration:none; color:#000;">Card Display</a></div>
                <?php else: ?>
                    <div class="labelbutton"><a href="transhistwinnings.php" style="text-decoration:none; color:#000;">Card Display</a></div>
                <?php endif;?>
            </td>
        </tr>
    </table>
</div>
