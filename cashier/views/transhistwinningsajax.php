<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 05, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCCC_TransactionSummary");

$cctranssummary = new SCCC_TransactionSummary();
$terminal_id = $_GET['tid'];

$options = "<option value='-1'>Select One</option>";
$transactionreferences = $cctranssummary->SelectTransactionSummaryPerTerminal($terminal_id);
if(count($transactionreferences) > 0)
{
    for($i = 0 ; $i < count($transactionreferences) ; $i++)
    {
        $options .= "<option value=" . $transactionreferences[$i]["TransactionSummaryID"] . ">" . str_pad($transactionreferences[$i]["TransactionSummaryID"],14,"0",STR_PAD_LEFT) . "</option>";
    }
}
echo $options;
?>