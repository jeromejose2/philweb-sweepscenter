<?php

/*
 * Created By: Jerome F. Jose
 * Created On: August 27, 2013
 * Purpose : Get Balance via Casino
 */
require_once '../init.inc.php';

App::LoadModuleClass("SweepsCenter", "SCC_ref_Services");
App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadSettings("swcsettings.inc.php");

$refservices = new SCC_ref_Services();
$cterminals = new SCC_Terminals();
$selectactive = $refservices->SelectActiveService();
$provider = $selectactive[0]['ID'];


switch ($provider)
{
    Case 1:

        App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");

        $rtg_url = App::getParam("rtg_url");
        $certFilePath = App::getParam("certFilePath");
        $keyFilePath = App::getParam("keyFilePath");

        $capiwrapper = new RealtimeGamingAPIWrapper($rtg_url, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);

//        $activesessions1 = $cterminals->SelectActiveSessions($_SESSION['siteidcashier']);
        $activesessions1 = $cterminals->SelectActiveSessionIsplayable($_SESSION['siteidcashier']);



        if (count($activesessions1) > 0)
        {

            for ($i = 0; $i < count($activesessions1); $i++)
            {
                $param = array('delimitedAccountNumbers' => $activesessions1[$i]["Name"]);
                $rtgbalance = $capiwrapper->GetBalance($activesessions1[$i]["Name"]);

                if ($rtgbalance['IsSucceed'] == 1 || $rtgbalance['IsSucceed'] == TRUE)
                {
                    $balance = $rtgbalance['BalanceInfo']['Balance'];

                    $cterminals->StartTransaction();
                    $cterminals->UpdateTerminalBalance($balance, $activesessions1[$i]["ID"]);
                    if ($cterminals->HasError)
                    {
                        $cterminals->RollBackTransaction();
                    } else
                    {
                        $cterminals->CommitTransaction();
                    }
                }
            }
        }

        break;


    Case 3:
        App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
        App::LoadLibrary("MicrogamingAPI.class.php");

        $csmagentsessions = new SCCSM_AgentSessions();

        $sessionguid = $csmagentsessions->SelectSessionGUID();
        $sessionGUID_MG = $sessionguid[0]["SessionGUID"];
        $ip_add_MG = $_SERVER['REMOTE_ADDR'];

        $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>" .
                "<SessionGUID>" . $sessionGUID_MG . "</SessionGUID>" .
                "<IPAddress>" . $ip_add_MG . "</IPAddress>" .
                "<ErrorCode>0</ErrorCode>" .
                "<IsLengthenSession>true</IsLengthenSession>" .
                "</AgentSession>";
        $mg_url = App::getParam("mg_url");

        $client = new nusoap_client($mg_url, 'wsdl');

        $client->setHeaders($headers);

        $arrterminal = $cterminals->GetTerminalLimit1($_SESSION['siteidcashier']);
        $terminalname = $arrterminal[0]["Name"];
        $param = array('delimitedAccountNumbers' => $terminalname);
        /* $filename = "../APILogs/logs.txt";
          $fp = fopen($filename , "a");
          fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GetAccountBalance || ".'SWCJBP1'." || ".serialize($param)."\r\n");
          fclose($fp); */
        $result = $client->call('GetAccountBalance', $param);

        $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];
        break;
}
?>