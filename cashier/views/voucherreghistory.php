<?php

/*
 * Author :             Noel Antonio
 * Date Created:        September 21, 2012
 */

include('../controller/voucherreghistoryprocess.php');
?>
<?php include('header.php'); ?>
<script type="text/javascript" language="Javascript">
function ChangePage(pagenum)
{
    selectedindex = document.getElementById("pgSelectedPage");
    selectedindex.value = pagenum;
    document.forms[0].submit();
}
</script>

<div id="page">
        <table style="margin: auto;">
          <tr>
            <td class="labelbold2">Date:&nbsp;
                <?php echo $txtDateFr;?>
                <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
                <?php echo $btnSearch;?>
            </td>
          </tr>
        </table>
        <br />
        <table style="overflow-x:scroll; width:100%;" >
            <tr><th colspan ="9" style="height:30px; background-color: #5C5858;color:#FFFFFF;">VOUCHER REGISTRATION HISTORY</th></tr>
            <tr>
                <th class="th">Full Name</th>
                <th class="th">Gender</th>
                <th class="th">Birthday</th>
                <th class="th">Mailing Address</th>
                <th class="th">Contact Number</th>
                <th class="th">Email</th>
                <th class="th">Coupon Number</th>
                <th class="th">Free Entry Code</th>
                <th class="th">Winnings</th>
            </tr>
            <?php if (isset($pclist) || count($pclist) > 0) { ?>
            <p class="paging"><b>Displaying <?php echo $start;?>-<?php echo ($end > $count) ? $count : $end;?> of <?php echo $count;?></b></p>
            <?php
                    for ($i=0; $i < count($pclist); $i++)
                    {
                        ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow";
                        $row = $pclist[$i];
                        if($row[1]==1)
                        {
                            echo "<tr class=\"$class\" style=\"height:30px;\"><td class=\"td\">".$row[0]."</td><td class=\"td\">".Male."</td><td class=\"td\">".$row[2]."</td><td class=\"td\">".$row[3]."</td><td class=\"td\">".$row[4]."</td><td class=\"td\">".$row[5]."</td><td class=\"td\">".$row[6]."</td><td class=\"td\">".$row[7]."</td><td class=\"td\">".$row[8]."</td></tr>";
                        }else{
                            echo "<tr class=\"$class\" style=\"height:30px;\"><td class=\"td\">".$row[0]."</td><td class=\"td\">".Female."</td><td class=\"td\">".$row[2]."</td><td class=\"td\">".$row[3]."</td><td class=\"td\">".$row[4]."</td><td class=\"td\">".$row[5]."</td><td class=\"td\">".$row[6]."</td><td class=\"td\">".$row[7]."</td><td class=\"td\">".$row[8]."</td></tr>";
                        }
                        
                    }
                }
            ?>
            <tr>
                <td colspan="9" align="center"><?php echo $pgTransactionHistory;?></td>
            </tr>
        </table>
</div>
<?php include('footer.php'); ?>