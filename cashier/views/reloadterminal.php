<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 29, 2012
 */
include('../controller/reloadterminalprocess.php');
?>
<?php include('header.php'); ?>
<script type="text/javascript">
    function onchange_amount()
    {
        if (document.getElementById("ddlAmount").value == 0)
        {
            document.getElementById("txtAmount").disabled = false;
        }
        else
        {
            document.getElementById("txtAmount").disabled = true;
        }
    }

    function onSubmit()
    {
        var bcf = "<?php echo $_SESSION['Bal']; ?>";
        var title = "INVALID RELOAD REQUEST";
        var msg = "Please indicate the gaming terminal to be reloaded.";
        var terminal = document.getElementById('ddlTerminal').options[document.getElementById('ddlTerminal').selectedIndex].value;
        var terminalname = document.getElementById("ddlTerminal").options[document.getElementById("ddlTerminal").selectedIndex].text;
        var ddlamount = document.getElementById('ddlAmount').options[document.getElementById('ddlAmount').selectedIndex].value;
        var txtamount = document.getElementById('txtAmount').value;
        if (ddlamount != 0)
        {
            var amount = ddlamount;
        }
        else
        {
            var amount = txtamount;
        }

        if (terminal == 0 && ddlamount == 0)
        {
            document.getElementById('title1').innerHTML = title;
            document.getElementById('msg1').innerHTML = msg;
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }

        if (terminal == 0)
        {
            document.getElementById('title1').innerHTML = title;
            document.getElementById('msg1').innerHTML = msg;
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
        if (ddlamount == 0 && txtamount == "")
        {
            msg = "Please indicate the amount of points to be reloaded.";
            document.getElementById('title1').innerHTML = title;
            document.getElementById('msg1').innerHTML = msg;
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
        if (ddlamount == 0 && txtamount > 500)
        {
            msg = "Initial deposit must not exceed $500. Please input excess value as a reload.";
            document.getElementById('title1').innerHTML = title;
            document.getElementById('msg1').innerHTML = msg;
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
        bcf = bcf.replace(/,/g, '');
        if (eval(amount) > eval(bcf))
        {
            title = "INSUFFICIENT SCF BALANCE";
            msg = "You do not have sufficient SCF on your account. Please contact Customer Service to replenish SCF.";
            document.getElementById('title1').innerHTML = title;
            document.getElementById('msg1').innerHTML = msg;
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }

        var title = "RELOAD CONFIRMATION";
        var msg = "Are you sure you want to reload " + terminalname + " with " + amount + " points?";
        document.getElementById('title2').innerHTML = title;
        document.getElementById('msg2').innerHTML = msg;
        document.getElementById('light2').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return true;
    }

    function loading()
    {
//        document.getElementById('light3').style.display='block';document.getElementById('fade').style.display='block';
//        setTimeout(function(){
//            document.getElementById('light2').style.display='none';
//            document.getElementById('light3').style.display='none';
//            document.getElementById('fade').style.display='none';   
        document.getElementById('btnOkay').disabled = 'true';
        document.getElementById('btnCancel').disabled = 'true';     
        document.forms[0].submit();

//        }, 10000);
    }


    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function printpage(url)
    {
        child = window.open(url, "", "height=1, width=1");
        window.focus();
        window.location = 'activesession.php';
    }
</script>
<div id="page">
    <div id="innerpage">
        <table style="margin-left:auto;margin-right:auto;text-align:left;">
            <tr>
                <td class="labelbold2" align="right">Gaming Terminal:&nbsp;</td>
                <td><?php echo $ddlTerminal; ?></td>
            </tr>
            <tr>
                <td class="labelbold2" align="right">Points:&nbsp;</td>
                <td><?php echo $ddlAmount; ?>&nbsp;&nbsp;<?php echo $txtAmount; ?></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><?php echo $btnReloadSession; ?></td>
            </tr>
        </table>
    </div>
    <div id="light1" class="white_content">
        <div class="light-title" id="title1"></div>
        <div class="light-msg" id="msg1"></div>
        <div class="inputBoxEffectPopup" style="margin-left:auto;margin-right:auto;" onclick ="document.getElementById('light1').style.display = 'none';
        document.getElementById('fade').style.display = 'none';
        javascript: location.reload(true);"></div>
    </div>
    <div id="light2" class="white_content">
        <div class="light-title" id="title2"></div>
        <div class="light-msg" id="msg2"></div>
        <?php echo $btnOkay; ?>
        <input id="btnCancel" type="button" value="" class="inputBoxEffectPopupCancel" onclick="document.getElementById('fade').style.display = 'none';document.getElementById('light2').style.display = 'none';">
    </div>
    <div id="light3" class="white_content">
        <div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 10px;" />
        </div>
    </div>
</div>
<div id="fade" class="black_overlay"></div>
<?php if (isset($error_msg)): ?>
    <script>
        document.getElementById('title1').innerHTML = "<?php echo $error_title ?>";
        document.getElementById('msg1').innerHTML = "<?php echo $error_msg ?>";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
    </script>
<?php endif; ?>
<?php if (isset($url)): ?>
    <script>
        printpage(<?php echo $url ?>);
    </script>
<?php endif; ?>
<?php include('footer.php'); ?>