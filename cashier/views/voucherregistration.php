<?php

/*
 * Author :             Noel Antonio
 * Date Created:        September 20, 2012
 */

include('../controller/tc_calendar.php');
include('../controller/voucherregistrationprocess.php');
?>
<?php include('header.php');?>
<script language="Javascript" src="jscripts/checkinputs.js" ></script>
<script type="text/javascript">
function validatecoupon()
{   
    var cn = document.getElementById('txtCN').value;
    $.ajax
    ({
        url: '../controller/verify_coupon.php?cn='+cn,
        type : 'post',
        success : function(data)
        { 
            if(data > 0)
            {
                document.getElementById('hiddencn').value = 1;            
            }else{
                document.getElementById('hiddencn').value = 0;
            }
        },
        error: function(e)
        {
            alert("Error");
        }
    });
}
</script>
<?php echo $hiddencn?>
<div id="page">
    <div id="innerpage">
        <table style="margin-left:auto; margin-right:auto; text-align:left;">
            <tr>
                <td id="lblfullname" class="labelbold2" align="right">Full Name:</td>
                <td><?php echo $txtFullName; ?></td>
            </tr>
            <tr>
                <td id="lblgen" class="labelbold2" align="right">Gender:</td>
                <td>
                    <input type="radio" checked="checked" name="rdogen" value="1" />Male
                    <input type="radio" name="rdogen" value="2" />Female
                </td>
            </tr>
            <tr>
                <td id="lblbday" class="labelbold2" align="right">Birthday:</td>
                <td>
                    <?php echo $txtBday; ?>
                    <?php
                        $year = '0000';
                        $month = '00';
                        $day = '00';
                        $myCalendar = new tc_calendar("txtBday", true);
                        $myCalendar->setIcon("images/iconCalendar.gif");
                        $myCalendar->setDate($day, $month, $year);
                        $myCalendar->setPath("./");
                        $myCalendar->setYearInterval(1900, (date('Y') - 18));
                        $myCalendar->dateAllow('1900-01-01', '$year-$month-$day');
                        $myCalendar->disabledDay("Sat");
                        $myCalendar->disabledDay("sun");
                        $myCalendar->writeScript();
                    ?>
                </td>
            </tr>
            <tr>
                <td id="lbladdr" class="labelbold2" align="right">Mailing Address:</td>
                <td><?php echo $txtAddress; ?></td>
            </tr>
            <tr>
                <td id="lblcont" class="labelbold2" align="right">Contact Number:</td>
                <td><?php echo $txtContact; ?></td>
            </tr>
            <tr>
                <td id="lblemail" class="labelbold2" align="right">Email Address:</td>
                <td><?php echo $txtEmail; ?></td>
            </tr>
            <tr>
                <td id="lblcn" class="labelbold2" align="right">Coupon Number:</td>
                <td><?php echo $txtCN; ?></td>
            </tr>
            <tr><td colspan="2" style="color: red; font-style: italic; font-size: 0.9em">*All fields are required.</td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td colspan="2" align="center"><?php echo $btnSubmit; ?></td>
            </tr>
        </table>
    </div>
</div>
<div id="fade" class="black_overlay"></div>
<div id="light1" class="white_content">
    <div class="light-title" id="title"></div><br/>
    <div class="light-msg" id="msg"></div><br/>
    <div type="button" class="inputBoxEffectPopup" id="button1" style="margin-left:auto;margin-right:auto;"  onclick ="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';"></div>
    <br/>
</div>
<div id="light2" class="white_content">
    <div class="light-title" id="title2"></div><br/>
    <div class="light-msg" id="msg2"></div><br/>
    <?php echo $btnOkay; ?>
    <input type="button" class="inputBoxEffectPopupCancel" id="button1" onclick ="document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" />
    <br/>
</div>
<?php if (isset($error_msg)): ?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $error_title?>";
    document.getElementById('msg').innerHTML = "<?php echo $error_msg?>";
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
</script>
<?php endif; ?>
<?php include('footer.php');?>