<?php
/***************************************************************
* Author : Noel Antonio                                        *
* Date   : October 3, 2012                                     *
* Purpose: Points Deduction for Browsing Internet              *
****************************************************************/

session_start();
include( 'microseconds.php' );
include( 'mysql_config.php' );
include( 'connstr.php' );

$query = "SELECT * FROM cafino.ref_services WHERE Status = 1";
$result = mysqli_query($dbConn, $query);
$row = mysqli_fetch_row($result);
$activeservice = $row[0];

mysqli_next_result($dbConn);
mysqli_free_result($result);

require_once("../../init.inc.php");

$id = $_SESSION['id'];
$login = $_SESSION['user'];
if ($activeservice == 1) // RTG
{
        App::LoadSettings("swcsettings.inc.php");
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
        $rtg_url       = App::getParam("rtg_url");
        $certFilePath = App::getParam("certFilePath");
        $keyFilePath  = App::getParam("keyFilePath");
        $rtgwrapper = new RealtimeGamingAPIWrapper($rtg_url, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);

    $filename = "APILogs.txt";
    $fp = fopen($filename , "a");
    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".$login."\r\n");
    fclose($fp);
    $a = 1;
    
    while($a < 4)
    {                
            $result = $rtgwrapper->GetBalance($login);
            if ($result && is_array($result))
            {
                    $isSucceed = $result['IsSucceed'];
                    if (array_key_exists('faultcode', $result))
                    {
                            $a++;
                    }
                    else
                    {
                            if ($isSucceed == 'true')
                            {
                                    $string = serialize($result);
                                    break;
                            }
                            else
                            {
                                    $a++;
                            }
                    }
            }
            else
            {
                    $filename = "APILogs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || " . $login . " || " . $string . "\r\n");
                    fclose($fp);
                    $a++;
            }
    }
    
    if ($isSucceed)
    {
        $balance = $result['BalanceInfo']['Balance'];
    }
   
}


//else if ($activeservice == 3) // MG
//{
//    include ( 'nusoap/nusoap.php' );
//    include ( 'MicrogamingAPI.class.php' );
//
//    $ip_add_MG = $_SERVER['REMOTE_ADDR'];
//
//    $query = "SELECT * FROM cafino_servicemapping.tbl_agentsessions where AgentID = 2;";
//    $result = mysqli_query($dbConn,$query);
//    $row = mysqli_fetch_array($result);
//
//    $sessionGUID_MG = $row['SessionGUID'];
//
//    mysqli_next_result($dbConn);
//    mysqli_free_result($result);
//    
//
//    $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
//                "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
//                "<IPAddress>".$ip_add_MG."</IPAddress>".
//                "<ErrorCode>0</ErrorCode>".
//                "<IsLengthenSession>true</IsLengthenSession>".
//                "</AgentSession>";
//
//
//    $client = new nusoap_client($mg_url, 'wsdl');
//    $client->setHeaders($headers);
//    $param = array('delimitedAccountNumbers' => $login);
//    
//    $filename = "APILogs.txt";
//    $fp = fopen($filename , "a");
//    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".serialize($param)."\r\n");
//    fclose($fp);
//    $a = 1;
//    
//    while($a < 4)
//    {
//            $result = $client->call('GetAccountBalance', $param);
//            $isSucceed = false;
//            if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
//            {
//                    $a++;
//            }
//            else
//            {
//                    $isSucceed = true;
//                    break;
//            }
//    }
//    
//    if ($isSucceed)
//    {
//        $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];
//    }
//}

if ($balance >= 5)
{
        //INSERT TO LOGS
        $datestamp = udate("YmdHisu");

        $query = "CALL proc_inserttotransactionlog('$datestamp','$id',4,1,'W',5.00,1,@retval)";
        $result = mysqli_query($dbConn,$query);
        $row = mysqli_fetch_array($result);
 
        $return = $row['ReturnID'];
        $returnmsg = $row['ReturnMsg'];

        mysqli_next_result($dbConn);
        mysqli_free_result($result);
        
        //insert to DBLogs.txt
        $filename = "DBLogs.txt";
        $fp = fopen($filename , "a");

        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: BROWSE INTERNET || ".$login." || proc_inserttotransactionlog('$datestamp','$id',4,1,'W',5.00,1,@retval) || ".$return." || ".$returnmsg."\r\n");

        fclose($fp);

        if($return == 0)
        {
                $filename = "APILogs.txt";
                $fp = fopen($filename , "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Withdrawal || ".$login." || ".$return." || ".$returnmsg."\r\n");
                fclose($fp);
                
                if ($activeservice == 1)
                {
                        //$rtgwrapper->SetWithdrawalMethodId(502);
                        $result_withdraw = $rtgwrapper->Withdraw($login, 5, $tracking1 = '', $tracking2 = '', $tracking3 = '', $tracking4 = '');
                        $filename = "apixml.txt";
                        $fp = fopen($filename , "a");
                        fwrite($fp, "WITHDRAW REQUEST: " . date("Y-m-d H:i:s") . " || " . $login . " || REQUEST" . "\r\n");   
                        fwrite($fp, "WITHDRAW RESPONSE: " . date("Y-m-d H:i:s") . " || " . $login . " || RESPONSE" . "\r\n"); 
                        fclose($fp);
                        
                        $string = serialize($result_withdraw);
                        $errormsg = $result_withdraw["IsSucceed"];
                        $externaltransid = $result_withdraw["TransactionInfo"]["WithdrawGenericResult"]["transactionID"];
                }
                
                else if ($activeservice == 3)
                {
                        $param_withdraw = array('accountNumber' => $login, 'amount' => 5.00, 'currency' => 1);
                        $filename = "APILogs.txt";
                        $fp = fopen($filename , "a");
                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Withdrawal || ".$login." || ".serialize($param_withdraw)."\r\n");
                        fclose($fp);
                        $result_withdraw = $client->call('Withdrawal', $param_withdraw);

                        $errormsg = $result_withdraw["WithdrawalResult"]["IsSucceed"];
                        $externaltransid = $result_withdraw["WithdrawalResult"]["TransactionId"];
                }

                if($errormsg == 'true' || ($errormsg) || $errormsg == 1)
                {
                        $string = serialize($result_withdraw);

                        //insert to logs
                        $filename = "APILogs.txt";
                        $fp = fopen($filename , "a");

                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(BROWSE INTERNET) || ".$login." || ".$string."\r\n");

                        fclose($fp);

                        //UPDATE LOGS
                        $query = "CALL proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',1,1,@retval)";
                        $result = mysqli_query($dbConn,$query);
                        $row = mysqli_fetch_array($result);

                        $return = $row['ReturnID'];
                        $returnmsg = $row['ReturnMsg'];

                        mysqli_next_result($dbConn);
                        mysqli_free_result($result);

                        //insert to logs
                        $filename = "DBLogs.txt";
                        $fp = fopen($filename , "a");

                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: BROWSE INTERNET || ".$login." || proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',1,1,@retval) || ".$return." || ".$returnmsg."\r\n");

                        fclose($fp);

                        if($return == 0)
                        {
                                $query1 = "SELECT Balance FROM tbl_terminals where ID = '$id' ";
                                $result1 = mysqli_query($dbConn,$query1);
                                $row = mysqli_fetch_array($result1);
                                $updatedBalance = $row['Balance'] - 5;
                                mysqli_next_result($dbConn);
                                mysqli_free_result($result);

                                $query = "Update tbl_terminals set ServiceID = 4 , Balance = '$updatedBalance' where ID = '$id';";
                                $sql_result = mysqli_query($dbConn,$query);

                                mysqli_next_result($dbConn);


                                $query = "Delete from tbl_terminalbrowsing where TerminalID = '$id'";
                                $sql_result = mysqli_query($dbConn,$query);

                                mysqli_next_result($dbConn);


                                $query = "INSERT INTO tbl_terminalbrowsing (TerminalID,RecCreOn,Flag) VALUES ('$id',now(), 'Y');";
                                $sql_result = mysqli_query($dbConn,$query);

                                mysqli_close($dbConn);

                                if($sql_result == 'true')
                                {
                                        $status = 1;
                                }
                                else
                                {
                                        //rollback withdraw API (deposit API amt=5.00)
                                        $status = 2;
                                }	
                        }
                        else
                        {
                                //rollback withdraw API (deposit API amt=5.00)
                                $status = 3;
                        }
                }
                else
                {
                        $string = serialize($result_withdraw);

                        //insert to logs
                        $filename = "APILogs.txt";
                        $fp = fopen($filename , "a");

                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(BROWSE INTERNET) || ".$login." || ".$string."\r\n");

                        fclose($fp);

                        //UPDATE LOGS
                        $query = "CALL proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',2,1,@retval)";
                        $result = mysqli_query($dbConn,$query);
                        $row = mysqli_fetch_array($result);

                        $return = $row['ReturnID'];
                        $returnmsg = $row['ReturnMsg'];

                        mysqli_next_result($dbConn);
                        mysqli_free_result($result);

                        mysqli_close($dbConn);

                        //insert to logs
                        $filename = "DBLogs.txt";
                        $fp = fopen($filename , "a");

                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: BROWSE INTERNET || ".$login." || proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',2,1,@retval) || ".$return." || ".$returnmsg."\r\n");

                        fclose($fp);
                        $status = 4;
                }
        }
        else
        {
                $status = 5;
        }
}
else
{
        $status = 6;
}

echo $status;
?>