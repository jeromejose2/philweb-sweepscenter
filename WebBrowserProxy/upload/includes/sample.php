<?php
/***************************************************************
* Author : Noel Antonio                                        *
* Date   : October 3, 2012                                     *
* Purpose: Points Deduction for Browsing Internet              *
****************************************************************/

require 'includes/microseconds.php';
require 'includes/mysql_config.php';
require 'includes/connstr.php';
require_once("../init.inc.php");

$query = "SELECT * FROM cafino.ref_services WHERE Status = 1";
$result = mysqli_query($dbConn, $query);
$row = mysqli_fetch_row($result);
$activeservice = $row[0];

mysqli_next_result($dbConn);
mysqli_free_result($result);

$id = $_SESSION['id'];
$login = $_SESSION['user'];

if ($activeservice == 1) // RTG
{
        App::LoadSettings("swcsettings.inc.php");
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
        $rtg_url       = App::getParam("rtg_url");
        $certFilePath = App::getParam("certFilePath");
        $keyFilePath  = App::getParam("keyFilePath");
    
    $rtgwrapper = new RealtimeGamingAPIWrapper($rtg_url, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);

    $filename = $_SERVER['DOCUMENT_ROOT']."/APILogs/logs.txt";
    $fp = fopen($filename , "a");
    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".$login."\r\n");
    fclose($fp);
    $a = 1;
    
    while($a < 4)
    {                
            $result = $rtgwrapper->GetBalance($login);
            if ($result && is_array($result))
            {
                    $isSucceed = $result['IsSucceed'];
                    if (array_key_exists('faultcode', $result))
                    {
                            $a++;
                    }
                    else
                    {
                            if ($isSucceed == 'true')
                            {
                                    $string = serialize($result);
                                    break;
                            }
                            else
                            {
                                    $a++;
                            }
                    }
            }
            else
            {
                    $filename = $_SERVER['DOCUMENT_ROOT']."/APILogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || " . $login . " || " . $string . "\r\n");
                    fclose($fp);
                    $a++;
            }
    }
    
    if ($isSucceed)
    {
        $balance = $result['BalanceInfo']['Balance'];
    }
    
  
}


else if ($activeservice == 3) // MG
{

    require 'includes/nusoap/nusoap.php';
    require 'includes/MicrogamingAPI.class.php';

    $ip_add_MG = $_SERVER['REMOTE_ADDR'];

    $query = "SELECT * FROM cafino_servicemapping.tbl_agentsessions where AgentID = 2;";
    $result = mysqli_query($dbConn,$query);
    $row = mysqli_fetch_array($result);

    $sessionGUID_MG = $row['SessionGUID'];

    mysqli_next_result($dbConn);
    mysqli_free_result($result);
    

    $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
                "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
                "<IPAddress>".$ip_add_MG."</IPAddress>".
                "<ErrorCode>0</ErrorCode>".
                "<IsLengthenSession>true</IsLengthenSession>".
                "</AgentSession>";


    $client = new nusoap_client($mg_url, 'wsdl');
    $client->setHeaders($headers);
    $param = array('delimitedAccountNumbers' => $login);
    
    $filename = $_SERVER['DOCUMENT_ROOT']."/APILogs/logs.txt";

    $fp = fopen($filename , "a");
    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".serialize($param)."\r\n");
    fclose($fp);
    $a = 1;
    
    while($a < 4)
    {
            $result = $client->call('GetAccountBalance', $param);
            $isSucceed = false;
            if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
            {
                    $a++;
            }
            else
            {
                    $isSucceed = true;
                    break;
            }
    }
    
    if ($isSucceed)
    {
        $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];
    }
}

if ($balance >= 5)
{
error_reporting(0);


App::LoadModuleClass("SweepsCenter", "SCC_TransactionLogs");

$sctrans = new SCC_TransactionLogs();    
//        $query = "CALL proc_inserttotransactionlog('$datestamp','$id',4,1,'W',5.00,1,@retval)";
        $datestamp = udate("YmdHisu");        

        $sctrans->StartTransaction();        
        $arrtranslog["TerminalSessionID"] = $datestamp;
        $arrtranslog["TerminalID"] = $id;
        $arrtranslog["ServiceID"] = 4;
        $arrtranslog["DateCreated"] = 'now_usec()';
        $arrtranslog["DateUpdated"] = 'now_usec()';
        $arrtranslog["TransactionType"] = W;
        $arrtranslog["Amount"] = 5.00;
        $arrtranslog["LaunchPad"] = 1;        
        $sctrans->Insert($arrtranslog);
    
        if($sctrans->HasError)
        {
            $returnmsg = "INSERT Failed - Failed to insert tbl_transactionlogs";
            $sctrans->RollBackTransaction();
            $return = 1;
        }
        else
        {
            $sctrans->CommitTransaction();
            $returnmsg = "Transaction Successful";
            $return = 0;
        }
        
//        $result = mysqli_query($dbConn,$query);
//        $row = mysqli_fetch_array($result);
//
//        $return = $row['ReturnID'];//0
//        $returnmsg = $row['ReturnMsg'];//Transaction Successful
//
//        mysqli_next_result($dbConn);
//        mysqli_free_result($result);

        //insert to DBLogs.txt
        $filename = $_SERVER['DOCUMENT_ROOT']."/DBLogs/logs.txt";
        $fp = fopen($filename , "a");

        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: BROWSE INTERNET || ".$login." || insert to tbl_transactionlogs('$datestamp','$id',4,1,'W',5.00,1,@retval) || ".$return." || ".$returnmsg."\r\n");

        fclose($fp);
       
        if($return == 0)
        {
                $filename = $_SERVER['DOCUMENT_ROOT']."/APILogs/logs.txt";
                $fp = fopen($filename , "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Withdrawal || ".$login." || ".$return." || ".$returnmsg."\r\n");
                fclose($fp);
                
                if ($activeservice == 1)
                {
                        //$rtgwrapper->SetWithdrawalMethodId(503);
                        $result_withdraw = $rtgwrapper->Withdraw($login, 5.00);
 
                        $string = serialize($result_withdraw);
                        $errormsg = $result_withdraw["IsSucceed"];
                        $externaltransid = $result_withdraw["TransactionInfo"]["WithdrawGenericResult"]["transactionID"];
                }
                
                else if ($activeservice == 3)
                {
                        $param_withdraw = array('accountNumber' => $login, 'amount' => 5.00, 'currency' => 1);
                        $result_withdraw = $client->call('Withdrawal', $param_withdraw);
                        $errormsg = $result_withdraw["WithdrawalResult"]["IsSucceed"];
                        $externaltransid = $result_withdraw["WithdrawalResult"]["TransactionId"];
                        
                        $filename = $_SERVER['DOCUMENT_ROOT']."XMLLogs/apixml.txt";
                        $fp = fopen($filename , "a");
                        fwrite($fp, "WITHDRAW REQUEST: " . date("Y-m-d H:i:s") . " || " . $login . " || " . $client->request . "\r\n");  
                        fwrite($fp, "WITHDRAW RESPONSE: " . date("Y-m-d H:i:s") . " || " . $login . " || " .$client->response . "\r\n");
                        fclose($fp);
                }

                if($errormsg == 'true' || ($errormsg))
                {
                        $string = serialize($result_withdraw);

                        //insert to logs
                        $filename = $_SERVER['DOCUMENT_ROOT']."/APILogs/logs.txt";
                        $fp = fopen($filename , "a");

                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(BROWSE INTERNET) || ".$login." || ".$string."\r\n");

                        fclose($fp);

                        //UPDATE LOGS
//                        $query = "CALL proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',1,1,@retval)";
//                        $result = mysqli_query($dbConn,$query);
//                        $row = mysqli_fetch_array($result);
//
//                        $return = $row['ReturnID'];
//                        $returnmsg = $row['ReturnMsg'];
//
//                        mysqli_next_result($dbConn);
//                        mysqli_free_result($result);

                        $sctrans->StartTransaction();
                        $arrupdatetranslog["TerminalSessionID"] = $datestamp;
                        $arrupdatetranslog["ServiceTransactionID"] = $externaltransid;
                        $arrupdatetranslog["TransactionStatusDescription"] = $errormsg;
                        $arrupdatetranslog["DateUpdated"] = 'now_usec()';
                        $arrupdatetranslog["Status"] = 1;
                        $sctrans->UpdateByArray($arrupdatetranslog);

                        if($sctrans->HasError)
                        {
                            $returnmsg = "Update Failed - Failed to update tbl_transactionlogs";
                            $sctrans->RollBackTransaction();
                            $return = 1;
                        }
                        else
                        {
                            $sctrans->CommitTransaction();
                            $returnmsg = "Transaction Successful";
                            $return = 0;
                        }                        
                        
                        //insert to logs
                        $filename = $_SERVER['DOCUMENT_ROOT']."/DBLogs/logs.txt";
                        $fp = fopen($filename , "a");

                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: BROWSE INTERNET || ".$login." || updatetransactionlog('$datestamp','$externaltransid','$errormsg',1,1,@retval) || ".$return." || ".$returnmsg."\r\n");

                        fclose($fp);

                        if($return == 0)
                        {
						
									$query1 = "SELECT (Balance - 5) as Balance FROM tbl_terminals where ID = '$id' ";
									$result1 = mysqli_query($dbConn,$query1);
									$row = mysqli_fetch_array($result1);
									$updatedBalance = $row['Balance'];
									mysqli_next_result($dbConn);
									mysqli_free_result($result);

                                $query = "Update tbl_terminals set ServiceID = 4 , Balance = '$updatedBalance' where ID = '$id';";
                                $sql_result = mysqli_query($dbConn,$query);

                                mysqli_next_result($dbConn);


                                $query = "Delete from tbl_terminalbrowsing where TerminalID = '$id'";
                                $sql_result = mysqli_query($dbConn,$query);

                                mysqli_next_result($dbConn);


                                $query = "INSERT INTO tbl_terminalbrowsing (TerminalID,RecCreOn,Flag) VALUES ('$id',now(), 'Y');";
                                $sql_result = mysqli_query($dbConn,$query);

                                mysqli_close($dbConn);

                                if($sql_result == 'true')
                                {
                                        $proceedInternetBrowsing = "ok";
                                }
                                else
                                {
                                        //rollback withdraw API (deposit API amt=5.00)
                                        $error_msg = "An Error Occured. Please Try Again.(Update SeviceID=2 error)";
                                }	
                        }
                        else
                        {
                                //rollback withdraw API (deposit API amt=5.00)
                                $error_msg = "An Error Occured. Please Try Again.(Update Failed - Failed to update tbl_transactionlogs)";
                        }
                }
                else
                {
                        $string = serialize($result_withdraw);

                        //insert to logs
                        $filename = $_SERVER['DOCUMENT_ROOT']."/APILogs/logs.txt";
                        $fp = fopen($filename , "a");

                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(BROWSE INTERNET) || ".$login." || ".$string."\r\n");

                        fclose($fp);

                        //UPDATE LOGS
//                        $query = "CALL proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',2,1,@retval)";
//                        $result = mysqli_query($dbConn,$query);
//                        $row = mysqli_fetch_array($result);
//
//                        $return = $row['ReturnID'];
//                        $returnmsg = $row['ReturnMsg'];
//
//                        mysqli_next_result($dbConn);
//                        mysqli_free_result($result);
//
//                        mysqli_close($dbConn);

                        $sctrans->StartTransaction();
                        $arrupdatetranslog["TerminalSessionID"] = $datestamp;
                        $arrupdatetranslog["ServiceTransactionID"] = $externaltransid;
                        $arrupdatetranslog["TransactionStatusDescription"] = $errormsg;
                        $arrupdatetranslog["DateUpdated"] = 'now_usec()';
                        $arrupdatetranslog["Status"] = 2;
                        $sctrans->UpdateByArray($arrupdatetranslog);

                        if($sctrans->HasError)
                        {
                            $returnmsg = "Update Failed - Failed to update tbl_transactionlogs";
                            $sctrans->RollBackTransaction();
                            $return = 1;
                        }
                        else
                        {
                            $sctrans->CommitTransaction();
                            $returnmsg = "Transaction Successful";
                            $return = 0;
                        }
                        
                        //insert to logs
                        $filename = $_SERVER['DOCUMENT_ROOT']."/DBLogs/logs.txt";
                        $fp = fopen($filename , "a");

                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: BROWSE INTERNET || ".$login." || updatetransactionlog('$datestamp','$externaltransid','$errormsg',2,1,@retval) || ".$return." || ".$returnmsg."\r\n");

                        fclose($fp);
                        $error_msg = "An Error Occured. Please Try Again.(Withdrawal API error)";
                }
        }
        else
        {
                $error_msg = "An Error Occured. Please Try Again.(INSERT Failed - Failed to insert tbl_transactionlogs)";
        }
}
else
{
        $insufficient_balance = "ok";
}
?>