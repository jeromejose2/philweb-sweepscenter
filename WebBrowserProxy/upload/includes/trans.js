function check_browsing_terminals()
{
    $.ajax({
         url: 'includes/GetBrowsingTerminals.php',
         type : 'post',
         success : function(data)
         {
            $('#convert_img').html(data)
         },
         error: function(e)
         {
            alert("Error");
         }
      });
}

function deduct_balance()
{
    document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';
    //document.getElementById('light3').style.display='block';document.getElementById('fade').style.display='block';
    show_loading();

    $.ajax({
         url: 'includes/DeductBalance.php',
         type : 'post',
         success : function(data)
         {
            $('#convert_img').html(data)
         },
         error: function(e)
         {
            alert("Error");
         }
      });
}

function deduct_balance2()
{
    document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';
    document.getElementById('light3').style.display='block';document.getElementById('fade').style.display='block';

    $.ajax({
         url: 'includes/DeductBalance.php',
         type : 'post',
         success : function(data)
         {
            $('#convert_img2').html(data)
         },
         error: function(e)
         {
            alert("Error");
         }
      });
}

function back_to_lp()
{
    $.ajax({
         url: 'includes/BacktoLP.php',
         type : 'post',
         success : function(data)
         {
            $('#convert_img2').html(data)
         },
         error: function(e)
         {
            alert("Error");	     
         }
      });
}


function show_loading()
{
    document.getElementById('light3').style.display='block';document.getElementById('fade').style.display='block';
}

function hide_loading()
{
    document.getElementById('light3').style.display='none';document.getElementById('fade').style.display='none';
}

setInterval ("check_browsing_terminals()", 60000);
//setTimeout("check_browsing_terminals()", 5000);
