<?php
session_start();
include('../../../conn/connstr.php');
include('../../../conn/config.php');
include('nusoap/nusoap.php');
include('microseconds.php');

$login = $_SESSION['user'];
$id = $_SESSION['id'];

$query = "SELECT * FROM cafino_servicemapping.tbl_agentsessions where AgentID = 2";
$result = mysqli_query($dbConn,$query);
$row = mysqli_fetch_array($result);

$sessionGUID_MG = $row['SessionGUID'];

mysqli_next_result($dbConn);
mysqli_free_result($result);


$headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
           "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
           "<IPAddress>".$ip_add_MG."</IPAddress>".
           "<ErrorCode>0</ErrorCode>".
           "<IsLengthenSession>true</IsLengthenSession>".
           "</AgentSession>";


$client = new nusoap_client($mg_url, 'wsdl');
$client->setHeaders($headers);
$param = array('delimitedAccountNumbers' => $login);
$filename = "includes/APILogs.txt";
$fp = fopen($filename , "a");
fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".serialize($param)."\r\n");
fclose($fp);
$a = 1;
while($a < 4)
{
	$result = $client->call('GetAccountBalance', $param);
	if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
	{
		$a++;
	}
	else
	{
		break;
	}
}

$balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];

if ($balance >= 5)
{
    //INSERT TO LOGS
    $datestamp = udate("YmdHisu");

    $query = "CALL proc_inserttotransactionlog('$datestamp','$id',4,1,'W',5.00,1,@retval)";
    $result = mysqli_query($dbConn,$query);
    $row = mysqli_fetch_array($result);

    $return = $row['ReturnID'];
    $returnmsg = $row['ReturnMsg'];

    mysqli_next_result($dbConn);
    mysqli_free_result($result);

    if($return == 0)
    {
        $param_withdraw = array('accountNumber' => $login, 'amount' => 5.00, 'currency' => 1);
		$filename = "includes/APILogs.txt";
		$fp = fopen($filename , "a");
		fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Withdrawal || ".$login." || ".serialize($param_withdraw)."\r\n");
		fclose($fp);
        $result_withdraw = $client->call('Withdrawal', $param_withdraw);

        $errormsg = $result_withdraw["WithdrawalResult"]["IsSucceed"];
        $externaltransid = $result_withdraw["WithdrawalResult"]["TransactionId"];
        $transaction_amt = $result_withdraw["WithdrawalResult"]["TransactionAmount"];
        $transaction_credit_amt = $result_withdraw["WithdrawalResult"]["TransactionCreditAmount"];
        $credit_bal = $result_withdraw["WithdrawalResult"]["CreditBalance"];
        $balance_mg = $result_withdraw["WithdrawalResult"]["Balance"];

        if($errormsg == 'true')
        {
            //UPDATE LOGS
            $query = "CALL proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',1,1,@retval)";
            $result = mysqli_query($dbConn,$query);
            $row = mysqli_fetch_array($result);

            $return = $row['ReturnID'];
            $returnmsg = $row['ReturnMsg'];

            mysqli_next_result($dbConn);
            mysqli_free_result($result);

            if($return == 0)
            {
                //DEDUCTION SUCCESSFUL!
                //delete from tbl_terminalbrowsing
                $query = "Delete from tbl_terminalbrowsing where TerminalID = '$id'";
                $sql_result = mysqli_query($dbConn,$query);

                mysqli_close($dbConn);

                echo "<script>document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';</script>";
                echo "<script>hide_loading();</script>";
            }
            else
            {
                mysqli_close($dbConn);
                //echo "<script>document.getElementById('light3').style.display='none';document.getElementById('fade').style.display='none';</script>";
                //echo "<script>document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block';</script>";
                echo "<script>hide_loading();</script>";
                echo "<script>document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block';</script>";
                echo "An Error Occured. Please Try Again.(Error proc_updatetransactionlog)";
            }
        }
        else
        {
            mysqli_close($dbConn);
            //echo "<script>document.getElementById('light3').style.display='none';document.getElementById('fade').style.display='none';</script>";
            //echo "<script>document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block';</script>";
            echo "<script>hide_loading();</script>";
            echo "<script>document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block';</script>";
            echo "An Error Occured. Please Try Again.(Withdrawal API error)";
        }
    }
    else
    {
        mysqli_close($dbConn);
        //echo "<script>document.getElementById('light3').style.display='none';document.getElementById('fade').style.display='none';</script>";
        //echo "<script>document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block';</script>";
        echo "<script>hide_loading();</script>";
        echo "<script>document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block';</script>";
        echo "An Error Occured. Please Try Again.(Error proc_inserttotransactionlog)";
    }
}
else
{
    mysqli_close($dbConn);
    //echo "<script>document.getElementById('light3').style.display='none';document.getElementById('fade').style.display='none';</script>";
    //echo "<script>document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block';</script>";
    echo "<script>hide_loading();</script>";
    echo "<script>document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block';</script>";
    echo "You have insufficient points in your balance. Please reload from the cashier and click OKAY to continue this Internet session. Otherwise, click CANCEL to return";
}

?>
