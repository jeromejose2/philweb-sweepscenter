<?php 
//ini_set('display_errors', 1);
//ini_set('log_errors', 1);
$user_agent = $_SERVER['HTTP_USER_AGENT'];

$div = "";
$div2 = "";
if (preg_match('/MSIE/i', $user_agent))
{//"Internet Explorer";
    $div = '<div id="light" class="white_content" style="left: 30%;height: 35%;">';
    $div2 = '<div id="light4" class="white_content" style="left:30%;height: 30%;">';
    $div3 = '<div id="light2" class="white_content" style="left:30%;height: 30%;">';
}
else if (preg_match('/Epiphany/i', $user_agent))
{//"Epiphany";
    $div = '<div id="light" class="white_content" style="left: 25%;height: 35%;">';
    $div2 = '<div id="light4" class="white_content" style="left:25%;height: 30%;">';
    $div3 = '<div id="light2" class="white_content" style="left:25%;height: 30%;">';
}
else
{//"Non-IE Browser";
    $div = '<div id="light" class="white_content" style="left: 30%;height: 35%;">';
    $div2 = '<div id="light4" class="white_content" style="left:35%;height: 30%;">';
    $div3 = '<div id="light2" class="white_content" style="left:25%;height: 30%;">';
}

$_SESSION['id'] = $_GET['id'];
$_SESSION['user'] = $_GET['user'];

if (isset($_POST['u']))
    $_SESSION['url'] = $_POST['u'];
else
    $_SESSION['url'] = $_GET['url'];


if (isset($_POST['btnGo']))
{
    $_SESSION['btnGo'] = $_POST['btnGo'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<html>

<head>
<title>Launch Pad</title>
<meta name="description" content="<!--[meta_description]-->">
<meta name="keywords" content="<!--[meta_keywords]-->">
<style type="text/css">
/* GLOBAL ELEMENTS */
html,body {
   margin: 0; padding: 0;
   background: #0b1933;
   font-size: 100%;
   font-family: "trebuchet ms", arial, verdana;
	color: #444;
   text-align: center;
}
a {
   color: #EF7B0A;
   text-decoration:none; 
}
a:hover {
   text-decoration: underline;
}
p {
   color: #444;
   line-height: 170%; margin: 5px 0;
}
p, td, th, ul {
   font-size: 80%;
}

/* LAYOUT */
#wrapper {
   width: 700px;
   margin: 0 auto 0 auto;
   text-align: left;
}

#content {
   background: #fff;
   border-top: 3px solid #ce6c1c;
   border-bottom: 3px solid #ce6c1c;
   padding: 20px;
}

/* ELEMENTS */
h1 {
   font: 250% "trebuchet ms";
   color: #fff;
   padding: 40px 0 10px 10px;
   margin: 0;
}
h1 span { color: #6BAD42; }
h1 a { color: #FFF; }
h1 a:hover { color: #6BAD42;  text-decoration: none;}
h1 a:hover span { color: #FFF;}
h2 {
   font: bold 100% arial, verdana, sans-serif;
   color: #3B578B;
   border-bottom: 1px solid #ccc;
   padding-bottom: 3px;
   margin: 25px 0 10px 0;
}
p+p { padding-top: 1em; }
form.form { font-size: 80%; background-color: #f5f5f5; padding: 0px;}
/*form.form { font-size: 80%; background-color: #f5f5f5; padding: 10px;}*/
#options {
   list-style-type: none;
   width: 500px;
   margin: 10px; padding: 0;
}
#options li { 
   float: left;
   width: 240px;
   border-left: 5px solid #ccc;
}
#footer {
   margin: 10px 0 0 0; 
   font-size: 80%;
   color: #ccc;
}
#nav {
   text-align: right;
   list-style-type: none;
   font-size: 80%;
   border-top: 1px solid #ccc;
   margin: 20px 0 0 0;
   padding: 0;
}
#nav li {
   padding: 0 5px 0 5px;
   display: inline;
   border-left: 1px solid #ccc;
}
.left {
   float: left;
}

/* STYLES */
.first { margin-top: 0 }
input.textbox { width: 500px; font: 120% arial, verdana, sans-serif; }
input.button {font: 120% arial, verdana, sans-serif; margin-top: 10px;}
label { font-weight: light; }
#error {
   border: 1px solid red;
   border-left: 5px solid red;
   padding: 2px;
   margin: 5px 0 15px 0;
   background: #eee;
}

/* TABLES USED IN COOKIE MANAGEMENT / EDIT BROWSER PAGES */
table {
   border-color: #666;
   border-width: 0 0 1px 1px;
   border-style: solid;
   width: 50%;
}
th {
   font-size: normal;
   background: #ccc;
   border-width: 2px;
}
td, th {
   border-color: #666;
   border-width: 1px 1px 0 0;
   border-style: solid;
   padding: 2px 10px 2px 10px;
}
td {background: #eee;}
.full-width {
   width: 98%;
}
.large-table { 
   width: 75%; 
   margin-top: 15px;
}
.large-table td, .large-table th {
   padding: 5px;
}
td.small-note {
   font-size: 60%;
   padding: 2px;
   text-align: right;
}
.placeholder
{
    width:600px;
    height:150px;
    background:white;
    padding:5px;
    text-align:center;
    margin:0 auto;
}
.suggestedbrowsers
{
    float:left;
    padding: 10px;
    margin: auto;
    
}
.suggestedbrowsers:hover img
{
    cursor:pointer;
    color: #FF00FF;
    height: 110px;
}
.addressbar
{
    background:#FFFFFF url('images/search.png') no-repeat 4px 4px;
    padding:4px 4px 4px 22px;
    border:1px solid #CCCCCC;
    width:450px;
    height:20px;
    margin-top: 50px;
}

.white_content {
display: none;
	position: absolute;
	top: 35%;
        left: 265px;
	width: 500px;
	height: auto;
	padding-bottom: 0px;
	border: solid #000000;
	background-color: white;
	z-index:1002;
	overflow: auto;
	font-family:Helvetica; 
	font-size: 20px;
      
}

.light-title
{
	border-bottom-style: solid; 
	border-color:#1FC4A9; 
	background-color: #139E9E; 
	color: white; height: 40px;
}

.light-message
{
	margin-left:30px; 
	margin-top:10px; 
	width:460px; 
	height:auto; 
	font-weight:bold;
}

.light-button
{
	/*margin-top: 5px; */
	margin-left: 80px; 
	float: left;
}
.black_overlay{
    display: none;
    position: absolute;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: black;
    z-index:1001;
    -moz-opacity: 0.8;
    opacity:.80;
    filter: alpha(opacity=80);
}

.white_content2 {
	display: none;
	position: absolute;
	top: 35%;
	left: 30%;
	width: 35%;
	height: 26%;
	background-color: white;
	z-index:1002;
	border:5px solid #0B4C3F;
}
/* TOOLTIP HOVER EFFECT */
label { font-weight: bold; line-height: 20px; cursor: help; }
#tooltip{ width:20em; color: #fff; background: #555; font-size: 12px; font-weight: normal; padding: 5px; border: 3px solid #333; text-align: left; }
</style>
<?php echo injectionJS(); /* Javascript for redirection direct to proxified pages */ ?>
<script type="text/javascript">

window.addDomReadyFunc(function() {
   document.getElementById('options').style.display = 'none';
   document.getElementById('input').focus();
});
disableOverride();
</script>
<script type="text/javascript">
function redirect(location)
{
        document.getElementById('u').value = location;
        confirmInternetCharge();
}

function confirmInternetCharge()
{
	document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
}

function continueInternetBrowsing()
{
	document.forms[0].submit();
}

function cancelInternetBrowsing()
{
	
}

function show_loading()
{
	document.getElementById('light').style.display='none';
        document.getElementById('light3').style.display='block';
	document.getElementById('fade').style.display='block';
}

function hide_loading()
{
    document.getElementById('light3').style.display='none';
    document.getElementById('fade').style.display='none';
}
</script>

</head>
<body>
<div id="fade" class="black_overlay"></div>
	
		
   <div id="wrapper">
		
      <div id="content">
		
      <!-- CONTENT START --> 
         <!-- <form action="includes/process.php?action=update" method="post" onsubmit="return updateLocation(this);" class="form"> -->
		 
			<form action="" method="post" class="form" name="browser">
			<div class="placeholder">
		        <div>
		            <div class="suggestedbrowsers">
		                <a onclick="javascript: return redirect('http://www.google.com');">
		                    <img src="images/google.png" height="100" width="100" alt="Google"/>
		                </a>
		            </div>
		            <div class="suggestedbrowsers">
		                <a onclick="javascript: return redirect('http://www.yahoo.com');">
		                    <img src="images/yahoo.png" height="100" width="100" alt="Yahoo"/>
		                </a>
		            </div>
		            <div class="suggestedbrowsers">
		                <a onclick="javascript: return redirect('http://www.gmail.com');">
		                    <img src="images/gmail.png" height="100" width="100" alt="GMail"/>
		                </a>
		            </div>
		            <div class="suggestedbrowsers">
		                <a onclick="javascript: return redirect('http://www.mail.yahoo.com');">
		                    <img src="images/yahoomail.png" height="100" width="100" alt="Yahoo Mail"/>
		                </a>
		            </div>
		            <div class="suggestedbrowsers">
		                <a onclick="javascript: return redirect('http://www.youtube.com');">
		                    <img src="images/youtube.png" height="100" width="100" alt="YouTube"/>
		                </a>
		            </div>
		        </div>
		    </div>
		    <div class="placeholder">
		        <div>
		            <input type="text" class="addressbar" id="u" name="u" value="<?php echo (isset($_SESSION['url'])) ? $_SESSION['url'] : 'http://www.'; ?>"/>
                            <input type="submit" id="btnGo" name="btnGo" value="Go" class="button">
		        </div>
				<div>
					<input type="submit" name="btnCancel" id="btnCancel" value="" style="background: url('images/backLaunchPad.png');background-position:center;background-repeat:no-repeat;height: 40px; margin-left: 60%;margin-bottom:0;  cursor: pointer;width:241px;border:none;margin-top:30px"/>
				</div>
		    </div>
            </ul>
            <br style="clear: both;">
                  
         </ul>
      </div>
	</div>
	<!-- <p class="center">&copy; 2012 <a href="index.php"><!--[site_name]--></a> : Powered by <a href="http://www.glype.com/">glype</a> <!--[version]--></p> -->
	<!-- POP UP FOR CONFIRMATION MESSAGES -->
	<?php echo $div; ?>
		<div align="center" class="light-title" style="padding-top: 2%"><b>INTERNET BROWSING CONFIRMATION</b></div>
		<div id="msg" class="light-message">
			You have selected BROWSE INTERNET. Kindly select OKAY if you wish to continue. Five(5) points will be deducted from your point balance. Otherwise, select CANCEL to go back to the Launch Pad. Thank you.
		</div>
	    <div id="button" class="light-button">
			<input type="submit" name="btnConfirmBrowsing" id="btnConfirmBrowsing" value="" style="background: url('images/OK Button.png');background-position:center;background-repeat:no-repeat;width:139px;height:38px;border:none;margin-top: 5px; margin-left: 30px; float: left;" />
			<input type="submit" name="btnCancel" id="btnCancel" value="" style="background: url('images/cancelbutton.png');background-position:center;background-repeat:no-repeat;width:139px;height:38px;border:none;margin-top: 5px; margin-left: 0px; float: left;"/>
	    </div>
	</div>
	<!-- POP UP FOR CONFIRMATION MESSAGES -->
	<!-- POP UP FOR INSUFFICIENT BALANCE MESSAGES -->
    <?php echo $div2; ?>	
		<div align="center" class="light-title" style="padding-top: 2%;"><b>INSUFFICIENT BALANCE</b></div>
		<div id="msg" class="light-message">
			You have insufficient points in your balance. Please reload to enjoy continuous Internet browsing time.
		</div>
	    <div id="button" class="light-button">
			<input type="submit" name="btnCancel" id="btnCancel" value="" style="background: url('images/OK Button.png');background-position:center;background-repeat:no-repeat;width:139px;height:38px;border:none;margin-top: 5px; margin-left: 80px; float: left;"/>
	    </div>
	</div>
	<!-- POP UP FOR CONFIRMATION MESSAGES -->
	<!-- POP UP FOR ERROR MESSAGES -->
    <?php echo $div3; ?>	
		<div align="center" class="light-title"><b>ERROR</b></div>
		<div id="msg2" class="light-message">
			Invalid web address.
		</div>
	    <div id="button">
			<img src="images/OK Button.png" alt="" onclick="document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" style="cursor:pointer;"/>
	    </div>
	</div>
	<!-- POP UP FOR ERROR MESSAGES -->
	<div id="light3" class="white_content2" style="left:30%;height: 30%;">
		<div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div>
	</div>
</div>
</form>
<?php if (isset($_POST['btnGo'])): ?>
<script>
    document.getElementById('u').value = "<?php echo $_SESSION['url']; ?>";
    show_loading();
    document.browser.action = "includes/process.php?action=update";
    document.browser.onload = "return updateLocation(this);";
    document.browser.submit();
</script>
<?php endif; ?>
         
         
<?php if (isset($_POST['btnConfirmBrowsing'])): ?>
         <?php 
            unset($_SESSION['btnGo']);
            if (isset($_POST['u']))
                $_SESSION['url'] = $_POST['u'];
            else
                $_SESSION['url'] = $_GET['url'];
         ?>
        <script>
                document.getElementById('u').value = "<?php echo (isset($_POST['u'])) ? $_POST['u'] : $_SESSION['url'];?>";
        </script>

        <?php include 'includes/sample.php'; ?>

        <?php if(isset($insufficient_balance)):?>
                <script>
                        document.getElementById('light3').style.display = 'none';
                        document.getElementById('light4').style.display = 'block';
                        document.getElementById('fade').style.display = 'block';
                </script>
        <?php endif;?>
        <?php if(isset($error_msg)):?>
                <script>
                        document.getElementById('light3').style.display = 'none';
                        document.getElementById('msg2').innerHTML = "<?php echo $error_msg;?>";
                        document.getElementById('light2').style.display = 'block';
                        document.getElementById('fade').style.display = 'block';
                </script>
        <?php endif;?> 
        <?php if(isset($proceedInternetBrowsing)):?>
                <script>
                        document.browser.action = "includes/process.php?action=update";
                        document.browser.onload = "return updateLocation(this);";
                        document.browser.submit();
                </script>
        <?php endif; ?>
<?php endif; ?>
         
<?php if($_GET['e'] == "curl_error" || $_GET['e'] == "invalid_url"): ?>
        <script>	
                document.getElementById('light2').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
        </script>
<?php endif; ?> 
         
<?php if($_GET['e'] == "success"): ?>
        <script>	
                confirmInternetCharge();
        </script>
<?php endif; ?> 
         
  
<?php if(isset($_POST['btnCancel'])):?>
        <?php 
                require 'includes/connstr.php';
                $id = $_SESSION['id'];
                $query = "Update tbl_terminals set ServiceID = 3 where ID = '$id';";
                $sql_result = mysqli_query($dbConn,$query);


                $query = "Delete from tbl_terminalbrowsing where TerminalID = '$id'";
                $sql_result = mysqli_query($dbConn,$query);

                mysqli_close($dbConn);

                echo "<script>window.close();</script>";
        ?>
        <script>
                window.close();
        </script>
<?php endif;?>
          
</body>
</html>