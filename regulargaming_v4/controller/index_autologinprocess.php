<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 08, 2012
 */
require_once('../init.inc.php');
App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
App::LoadLibrary("class.rc4crypt.php");

$caudittrail = new SCC_AuditTrail();
$cterminals = new SCC_Terminals();
$cterminalsessions = new SCC_TerminalSessions();
$os = $_GET['os'];
$ipadd = get_ip_address();

if($os == 'Windows')
{
    $salt = $os;
    $id = $ipadd;
    $c1 = hash("crc32", $id.$salt);
    $encrpt = md5($c1);
    $e = rc4crypt::encrypt($ipadd, $encrpt);
    $hashfromIP1 = bin2hex($e);
//     var_dump($hashfromIP);
    $hashfromIP = substr($hashfromIP1, 0,20);
    $checkhash = $cterminals->SelectByhashbyipaddress($hashfromIP);
    $username = $checkhash[0]['Name'];
    $password = $checkhash[0]['Password'];
    $userdtls = $cterminals->SelectTerminalUserDtlsforWindows($username,$password);
                 $terminalid = $userdtls[0]['ID'];
                 if(count($userdtls) > 0)
                 {
                     $caudittrail->StartTransaction();
                     $arrAuditTrail["SessionID"] = "";
                     $arrAuditTrail["AccountID"] = 0;
                     $arrAuditTrail["TransDetails"] = 'Login: ' . $username;
                     $arrAuditTrail["RemoteIP"] = $_SERVER['REMOTE_ADDR'];;
                     $arrAuditTrail["TransDateTime"] = 'now_usec()';
                     $caudittrail->Insert($arrAuditTrail);
                     if($caudittrail->HasError)
                     {
                         $caudittrail->RollBackTransaction();
                         $error_msg = "Error inserting in audit trail.";
                         $succes = $error_msg;
                     }
                     else
                     {
                         $caudittrail->CommitTransaction();

                         $cterminals->StartTransaction();
                         $cterminals->UpdateIsPlayableStatus($terminalid,0);
                         $cterminals->UpdateServID($terminalid, 3);
                         if($cterminals->HasError)
                         {
                             $cterminals->RollBackTransaction();
                             $error_title = "ERROR";
                             $error_msg = "Error updating terminal\'s Isplayable status.";
                             $succes = $error_msg;
                         }
                         else
                         {
                             $cterminals->CommitTransaction();

                             $terminalsessionsdtls = $cterminalsessions->SelectTerminalSessionDetails($terminalid);
                             $_SESSION['user'] = $username;
                             $_SESSION['id'] = $terminalid;
                             $_SESSION['siteid'] = $terminalsessionsdtls[0]['SiteID'];
                             $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
                             $_SESSION['pass'] = $password;
                             $_SESSION['tsi'] = $terminalsessionsdtls[0]['ID'];
                           $succes = 'success';
                         }   

                     }
                 }
                 else
                 {
                     $error_msg = "You have entered an invalid account information1.";
                     $succes = $error_msg;
                 }
echo $succes;
    /* JFJ 03-04-2013
     * for windows server and windows client
    ob_start();
    system("ls"); 
    $cominfo = ob_get_contents(); 
    ob_clean(); 
    $search = "Physical";
    $primarymac = strpos($cominfo, $search);
    $mac = substr($cominfo,($primarymac+36),17); //MAC Address
    */
    
    

}
else
{
    echo 'linux';
}
        

function get_ip_address()
    {
        $client_addr = filter_var((!empty($_SERVER['HTTP_CLIENT_IP']))? $_SERVER['HTTP_CLIENT_IP']:
    (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))? $_SERVER['HTTP_X_FORWARDED_FOR']:
    (!empty($_SERVER['REMOTE_ADDR']))? $_SERVER['REMOTE_ADDR']:'UNKNOWN', FILTER_SANITIZE_STRING);
        return $client_addr;
    }

?>
