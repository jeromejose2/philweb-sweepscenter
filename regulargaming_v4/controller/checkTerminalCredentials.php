<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 19, 2012
 */
require_once('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");
App::LoadModuleClass("SweepsCenter", "SCC_ManualLogin");

$cterminals = new SCC_Terminals();
$cwinningsdump = new SCC_WinningsDump();
$cmanuallogin = new SCC_ManualLogin();

$mcadd = $_GET['mcadd'];

$macadddtls = $cterminals->SelectByMacAddress($mcadd);

if(count($macadddtls) > 0)
{
    for($i = 0 ; $i < count($macadddtls) ; $i++)
    {
        $buttonsdtls = $cmanuallogin->SelectButtons($macadddtls[$i]['SiteID'], $macadddtls[$i]['ID']);
        if(count($buttonsdtls) == 0)
        {
            echo json_encode(array("manuallogin" => 1 , "register" => 1));
        }
        else
        {
            for($i = 0 ; $i < count($buttonsdtls) ; $i++)
            {
                echo json_encode(array("manuallogin" => $buttonsdtls[$i]['IsMBEnabled'] , "register" => $buttonsdtls[$i]['IsRBEnabled']));
            }
        }
    }
}
?>