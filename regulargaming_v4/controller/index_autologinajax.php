<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 19, 2012
 */
require_once('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
App::LoadLibrary("class.rc4crypt.php");

$cterminals = new SCC_Terminals();
$caudittrail = new SCC_AuditTrail();
$cterminalsessions = new SCC_TerminalSessions();
$mac_address = trim($_GET['macadd']);
$operatingsystem = $_GET['os'];


$ip = $_SERVER['REMOTE_ADDR'];

$macadddtls = $cterminals->SelectByMacAddress($mac_address);

$name = $macadddtls[0]['Name'];
$pass = $macadddtls[0]['Password'];
$hshedpass = $macadddtls[0]['HashedPassword'];
$oschecker = $macadddtls[0]['OperatingSystemID'];

//decrypt password
$pwd = '23780984';
$hshedpass = @pack('H*', $hshedpass);
$dcrptpass = rc4crypt::decrypt($pwd, $hshedpass);
//end decrypt password
if(!$macadddtls)
{
    echo json_encode(array("msg1" => "There is no mac address registered for this terminal.","result" => "0"));
}
else 
{
        if($operatingsystem != $oschecker)
        {
             echo json_encode(array("msg1" => "Incorrect Operating System for this terminal.","result" => "0"));
        }else 
        {


                        if($name == '')
                        {
                            echo json_encode(array("msg1" => "Incorrect login for this specific terminal.","result" => "0"));
                        }
                        else
                        {
                            $terminaldtls = $cterminals->SelectTerminalUserDtlsWithHashedPassword($name,$pass);

                            if(count($terminaldtls) > 0)
                            {
                                $terminalid = $terminaldtls[0]['ID'];
                                $siteid = $terminaldtls[0]['SiteID'];

                                $terminalsessionsdtls = $cterminalsessions->SelectTerminalSessionDetails($terminalid);

                                $caudittrail->StartTransaction();
                                $arrAuditTrail['SessionID'] = "";
                                $arrAuditTrail['AccountID'] = 0;
                                $arrAuditTrail['TransDetails'] = 'Auto Login: ' . $name;
                                $arrAuditTrail['RemoteIP'] = $ip;
                                $arrAuditTrail['TransDateTime'] = 'now_usec()';
                                $caudittrail->Insert($arrAuditTrail);
                                if($caudittrail->HasError)
                                {
                                    $caudittrail->RollBackTransaction();
                                    echo json_encode(array("msg1" => "Error inserting in audit trail.","result" => "0"));
                                }
                                else
                                {
                                    $caudittrail->CommitTransaction();
                                    $_SESSION['user'] = $name;
                                    $_SESSION['id'] = $terminalid;
                                    $_SESSION['siteid'] = $siteid;
                                    $_SESSION['ip'] = $ip;
                                    $_SESSION['pass'] = $dcrptpass;
                                    $_SESSION['terminal_session_id'] = $terminalsessionsdtls[0]['ID'];

                                    //insert to logs
                                    $filename = "../../DBLogs/logs.txt";
                                    $fp = fopen($filename , "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: AUTO LOGIN || ".$name." || INSERT INTO tbl_audittrail \r\n");

                                    echo json_encode(array("msg1" => "Terminal login successful.","result" => "1"));
                                }
                            }
                            else
                            {
                                echo json_encode(array("msg1" => "You have entered an invalid account information.","result" => "0"));
                            }
                        }
        }
}
?>