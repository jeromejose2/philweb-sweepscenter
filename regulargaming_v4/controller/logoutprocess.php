<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 20, 2012
 */
require_once('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");
App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");

$cterminalsession = new SCC_TerminalSessions();
$cwinningsdump = new SCC_WinningsDump();
$cterminals = new SCC_Terminals();
$id = $_SESSION['id'];
$siteid = $_SESSION['siteid'];

if($id != NULL && isset($id) && $id != 0)
{
    
            $cwinningsdump->StartTransaction();
            $cwinningsdump->UpdateIsFinalizedStatus($id);
            if($cwinningsdump->HasError)
            {
                $cwinningsdump->RollBackTransaction();
            }
            else
            {
                $cwinningsdump->CommitTransaction();
                $cterminals->StartTransaction();
                $cterminals->UpdateIsPlayableStatus($id,4);
                if($cterminals->HasError)
                {
                    $cterminals->RollBackTransaction();
                }
                else
                {
                            $cterminals->CommitTransaction();
            //                $confirmsession = $cterminalsession->getID($id);
            //                if($confirmsession)
            //                {
            //                $cterminalsession->StartTransaction();
            //                $cterminalsession->UpdateDateEndPerSiteID($id, $siteid);
            //                if($cterminalsession->HasError)
            //                {
            //                $cterminalsession->RollBackTransaction();
            //                }
            //                else
            //                {
            //                $cterminalsession->CommitTransaction();
            //                header("location: launchpad.php");
            //                }
            //                } 
            //                else
            //                {
                                    header("location: launchpad.php");
            //                }
                }
            }
}else
{
    //echo 'no session';
}
?>