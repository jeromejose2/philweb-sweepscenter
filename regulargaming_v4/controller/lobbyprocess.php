<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 19, 2012
 * 
 * Modified By: Noel Antonio
 * Date Modified : October 11, 2012
 */

require_once('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
App::LoadModuleClass("SweepsCenter", "SCC_ref_Services");

$cterminalsessions = new SCC_TerminalSessions();
$crefservice = new SCC_ref_Services();
$id = $_SESSION['id'];

$arrservice = $crefservice->SelectActiveService();
$activeservice = $arrservice[0]["ID"]; // To be used for displaying games based on active service provider.

$terminaldtls = $cterminalsessions->SelectTerminalSessionDetails($id);

if(count($terminaldtls) > 0)
{
    $conv_btn = "<div class=\"convertPoints\" onclick=\"popup('popUpDivLPConvert');\"></div>";
}
else
{
    $conv_btn = "<div class=\"convertPoints2\"></div>";
}
?>