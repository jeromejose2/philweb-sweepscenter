<?php
/**
 * lastupdatedby: Noel Antonio 03-08-2013
 * purpose: check macaddress duplication (continue)
 */

require_once('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadLibrary("class.rc4crypt.php");

$cterminals = new SCC_Terminals();
$user = $_GET['usr'];
$passwd = $_GET['pwd'];
$mac_address = trim($_GET['macadd']);
$operatingsystem = $_GET['os'];

$macadddtls = $cterminals->SelectByMacAddress($mac_address);
if(count($macadddtls) > 0 )
{
        $novalue = ' ';
        $name = $macadddtls[0]['Name'];
        $cterminals->StartTransaction();
        $cterminals->UpdateMacAddress($novalue,$novalue,$name,$novalue);
        if($cterminals->HasError)
        {
            $cterminals->RollBackTransaction();
            echo json_encode(array("msg1" => "Terminal updates failed.","result" => "0"));
        }
        else
        {
            $cterminals->CommitTransaction();
        }
}

$terminaldtls = $cterminals->SelectByNameAndPassword($user,$passwd);
if(count($terminaldtls) > 0)
{
        $pwd = '23780984';
        $e = rc4crypt::encrypt($pwd, $passwd);
        $encrpt = bin2hex($e);

        $cterminals->StartTransaction();
        $cterminals->UpdateMacAddress($mac_address,$encrpt,$user,$operatingsystem);
        if($cterminals->HasError)
        {
            $cterminals->RollBackTransaction();
            echo json_encode(array("msg1" => "Terminal registration failed.","result" => "0"));
        }
        else
        {
            $cterminals->CommitTransaction();
            echo json_encode(array("msg1" => "Terminal registration successful","result" => "1"));
        }
}
else
{
    echo json_encode(array("msg1" => "Username/Password is invalid. Please try again.","result" => "0"));
}
?>