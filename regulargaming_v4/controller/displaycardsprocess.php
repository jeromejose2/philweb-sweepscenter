<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 18, 2012
 * 
 * Modified By: Noel Antonio
 * Date Modified: October 10, 2012
 */

if (!isset($_SESSION))
{
    session_start();
}

require_once('../init.inc.php');
include('pagination.php');

App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");

$cwinningsdump = new SCC_WinningsDump();

$id = $_SESSION['id'];
$resultmsg = "";
$okbtn = "";

$allwinningsdtls = $cwinningsdump->SelectAllWinningsIsOpened($id);
$selectallOpenedCards = $cwinningsdump->SelectOpenCards($id);
$Allcards = $cwinningsdump->SelectAllWinnings($id);
//$alldetails = $cwinningsdump->SelectAllbyFK($id);

if (count($selectallOpenedCards) == count($Allcards))
{
    echo "<script>window.location = '../views/displaycards_quickpick.php';</script>";	
}
else 
{
        $user_agent = $_SERVER['HTTP_USER_AGENT']; 
            if (preg_match('/MSIE/i', $user_agent))
    {//"Internet Explorer";
        $resultmsg = "<div id='resultmsg' class='winsumm_ie' ></div> ";
    }else
    {//"Non-IE Browser";
        $resultmsg = "<div id='resultmsg' class='winsumm' ></div> ";
    }
//            $resultmsg = "<div id='resultmsg' class='winsumm'></div> ";
//             $okbtn = "<div id='okbtn' class=\"quick7\"></div>";

    if (preg_match('/MSIE/i', $user_agent))
    {//"Internet Explorer";
        $okbtn="<div id='okbtn' class='quick7_ie'></div>";
    }else
    {//"Non-IE Browser";
        $okbtn="<div id='okbtn' class='quick7'></div>";
    }
    
    
             $avail_cards = count($allwinningsdtls);
$html_content .=<<<firstcontent
 <div class="sweeps_quickContainer">
            <div class="cardCount">CARD COUNTER:</div>
            <div class="countBox_left"></div>
            <div id='avail_cards' class="countBox_body"><p>$avail_cards</p></div>
            <div class="countBox_right"></div>
            <div id='quick_pick_btn' class="quick" >
                <div class="sweeps_quick" onclick='location.href="displaycards_quickpick.php";' ></div>
            </div>
</div>
firstcontent;

if(!isset($issetcards) && $issetcards == NULL)//for out of reach of limit..sample 50 cards.page 500;
{
    if(count($alldetailsfrompagination) < 10)
    {
        $loopcount = 10 ;
    }
    else
    {
        $loopcount = count($alldetailsfrompagination);
    }

  for($x=0; $x < $loopcount; $x++)
  {
    $row = $alldetailsfrompagination[$x];
    if($x % 5 == 0)// separate for 5 columns
    {
        if($row['IsOpened'] == 'Y')//select if cards are flip or not
        {
$html_content .=<<<secondcontent
<div class="sweepsContainer2"><div id='.$x.' class="win_container"><p>$row[WinType]</p><span>$row[ECN]</span></div></div>
secondcontent;
        } 
        else
        {
$html_content .=<<<thirdcontent
<div class="sweepsContainer2"><div id='.$x.' onclick='reveal_cards($x);' class="win_container"><div class="flipCard"></div></div></div>
thirdcontent;
        }
    }
    else // separate for 5 columns
    {

        if($row['IsOpened'] == 'Y')//select if cards are flip or not
            {
                        $html_content .=<<<fourthcontent
                        <div class="sweepsContainer"><div id='.$x.' class="win_container"><p>$row[WinType]</p><span>$row[ECN]</span></div></div>
fourthcontent;
            } 
            else
            {
                        $html_content .=<<<fivecontent
                        <div class="sweepsContainer"><div id='.$x.' onclick='reveal_cards($x);' class="win_container"><div class="flipCard"></div></div></div>
fivecontent;
            }
    }
        
   
  }
}

if(isset($issetcards) && $issetcards == 1)//for out of reach of limit..sample 50 cards.page 500;
{
    for($i = 0; $i < 10; $i++)
    {
              if($i % 5 == 0)
                 {
                        $html_content .=<<<sixcontent
                        <div class="sweepsContainer2"><div id='.$i.' onclick='reveal_cards($i);' class="win_container"><div class="flipCard"></div></div></div>
sixcontent;
                 }
                 else
                 {
                        $html_content .=<<<sevencontent
                        <div class="sweepsContainer"><div id='.$i.' onclick='reveal_cards($i);' class="win_container"><div class="flipCard"></div></div></div>
sevencontent;
                 }
    }
}

if (preg_match('/MSIE/i', $user_agent))
    {//"Internet Explorer";
        $html_content .=<<<lastcontent
    <div class="sweeps_quickContainer">
        <div class="sweeps_links_ie" >
        <div id="view_summary" class="viewSum_ie" onclick='popup("popUpDivOpenedCards_ie"); view_opened_cards();' style='visibility: hidden; margin-top:5px; width: 200px;'>VIEW CARDS SUMMARY</div>
        </div>
        <div class="sweeps_links2_ie" >
            <div id="view_next_set" class="viewNext" style=""  onclick='javascript:location.replace(true);'>$paginate</div>
        </div>
    </div>
lastcontent;
    }else
    {//"Non-IE Browser";
        $html_content .=<<<lastcontent
    <div class="sweeps_quickContainer">
        <div class="sweeps_links" >
        <div id="view_summary" class="viewSum" onclick='popup("popUpDivOpenedCards"); view_opened_cards();' style='visibility: hidden; cursor: pointer; width: 250px;'>VIEW CARDS SUMMARY</div>
        </div>
        <div class="sweeps_links2" >
            <div id="view_next_set" class="viewNext" style=""  onclick='javascript:location.replace(true);'>$paginate</div>
        </div>
    </div>
lastcontent;
    }

//$html_content .=<<<lastcontent
//    <div class="sweeps_quickContainer">
//        <div class="sweeps_links" >
//        <div id="view_summary" class="viewSum" onclick='popup("popUpDivOpenedCards"); view_opened_cards();' style='visibility: hidden; width: 250px;'>VIEW CARDS SUMMARY</div>
//        </div>
//        <div class="sweeps_links2" >
//            <div id="view_next_set" class="viewNext" style=""  onclick='javascript:location.replace(true);'>$paginate</div>
//        </div>
//    </div>
//lastcontent;
           
 
}


?>