<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 18, 2012
 */
require_once('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");

App::LoadControl("PagingControl2");

$cwinningsdump = new SCC_WinningsDump();

$id = $_SESSION['id'];

$ip = $_SESSION['ip'];

/*PAGING*/
$itemsperpage = 5;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
/*PAGING*/

$cards = $cwinningsdump->SelectAllWinningsbyQuickDisplay($id);
$cardscount = count($cards);
$pgcon->Initialize($itemsperpage, $cardscount);
$pgTransactionHistory = $pgcon->PreRender();
$cards_list = $cwinningsdump->SelectAllWinningsWithLimitbyQuickDisplay($id,($pgcon->SelectedItemFrom - 1),$itemsperpage);
$start = ($cardscount > 0) ? $pgcon->SelectedItemFrom : 0;
$end = (($start - 1) + $itemsperpage);
$selectedpage = $pgcon->SelectedPage;
$totalpages = ceil(($cardscount / $itemsperpage));

$html_content = "<p style='font-size: 25px; font-weight: bold; margin-top: 10px; margin-bottom: 10px; margin-left: -500px;'><b>Page:</b> ";
$html_content .= $pgTransactionHistory;
$html_content .= "</p>";

$okbtn = "<div class=\"sweeps_quick123\" onclick=\"window.location.href='logout.php'\" ></div>";
$resultmsg = $_SESSION['resultmsg'];

$html_content .= "<div>";
if(count($cards_list) == 0)
{
   App::Pr("<script> window.location = 'launchpad.php'; </script>"); 
}
else 
{
  $updatecardtoY = $cwinningsdump->UpdateCardsStatus($id);
for($i = 0 ; $i < count($cards_list) ; $i++)
{
    $row = $cards_list[$i];
//     if($i % 5 == 0)
//    {
//          $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
//    }
//    else
//    {
//        $html_content .= "<div class=\"sweepsContainer\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
//    }
    if($i == 4)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 9)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 14)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 19)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 24)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 29)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 34)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 39)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 44)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 49)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 54)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 59)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 64)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 69)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 74)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 79)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 84)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 89)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 94)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 99)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 104)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 109)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 114)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 119)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 124)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 129)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 134)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 139)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 144)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 149)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 154)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 159)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 164)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 169)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 174)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 179)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 184)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 189)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 194)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 199)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 204)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 209)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 214)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 219)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 224)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 229)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 234)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 239)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 244)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 249)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 254)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 259)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 264)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 269)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 274)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 279)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 284)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 289)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 294)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 299)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 304)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 309)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 314)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 319)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 324)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 329)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 334)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 339)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 344)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 349)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 354)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 359)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 364)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 369)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 374)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 379)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 384)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 389)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 394)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 399)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 404)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 409)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 414)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 419)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 424)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 429)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 434)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 439)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 444)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 449)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 454)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 459)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 464)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 469)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 474)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 479)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 484)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 489)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 494)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else if($i == 499)
    {
        $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
    else
    {
        $html_content .= "<div class=\"sweepsContainer\"><div id='.$i.' class=\"win_container\"><p>".$row[2]."</p><span>".$row[3]."</span></div></div>";
    }
}
}
$html_content .= "</div>";
?>