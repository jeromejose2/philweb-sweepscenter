<?php

/*
 * Created By: Arlene R. Salazar
 * Created On: June 08, 2012
 *
 * Modified By: Noel Antonio
 * Date Modified: October 8, 2012
 * Purpose: To make launchpad process flexible for any active service provider (e.g RTG, MG).
 */

require_once('../init.inc.php');
include('xajax_toolkit/xajax_core/xajax.inc.php');
App::LoadControl("Hidden");
$xajax = new xajax();

$xajax->registerFunction("GetBalance");
$xajax->registerFunction("GetBalanceConv");
$xajax->registerFunction("HeartBeat");
$xajax->registerFunction("ConvertPoints");
$xajax->registerFunction("BrowseInternet");
$xajax->registerFunction("GetRegularGamingCards");
$xajax->registerFunction("RevealCards");
$xajax->registerFunction("ViewOpenedCards");
$xajax->registerFunction("CheckSession");
$xajax->registerFunction("CheckSessionSweepsCode");
$xajax->registerFunction("EnterSweepsCode");
$xajax->registerFunction("InterBrowsingLogs");
$xajax->registerFunction("TerminalLogout");
$xajax->registerFunction("endterminalsession");
$xajax->registerFunction("confrmConvertPoints");
$xajax->registerFunction("CheckCardStatus");
$xajax->registerFunction("checkPendingtrans");
$xajax->registerFunction("CheckSessionforBrowseInternet");
$launchform = new FormsProcessor();

$hiddenclicker = new Hidden('hiddenclicker', 'hiddenclicker');
$hiddenvoucher = new Hidden('hiddenvoucher', 'hiddenvoucher');
$launchform->AddControl($hiddenvoucher);
$launchform->AddControl($hiddenclicker);
$launchform->ProcessForms();

App::LoadModuleClass("SweepsCenter", "SCC_ref_Services");
App::LoadModuleClass("SweepsCenter", "SCC_CasinoGames");

$casinogames = new SCC_CasinoGames();
$crefservices = new SCC_ref_Services();

$arrservice = $crefservices->SelectActiveService();
$activeservice = $arrservice[0]["ID"];
if ($activeservice == 1) // RTG
{
    $ActiveGames = $casinogames->SelectActiveGames($activeservice);
    $newArray = array();

    for ($xa = 0; $xa < count($ActiveGames); $xa += 4)
    {
        $newArray[] = array_slice($ActiveGames, $xa, 4);
    }
    $getlist = count($newArray);
    $list1 = ($getlist - 1);
    $list2 = ($getlist - 2);
}
else if ($activeservice == 3) // MG
{
    $ActiveGames = $casinogames->SelectActiveGames($activeservice);
    $newArray = array();

    for ($xa = 0; $xa < count($ActiveGames); $xa += 4)
    {
        $newArray[] = array_slice($ActiveGames, $xa, 4);
    }
    $getlist = count($newArray);
    $list1 = ($getlist - 1);
    $list2 = ($getlist - 2);
}

function CheckSessionforBrowseInternet()
{
    
$id = $_SESSION['id'];
$objResponse = new xajaxResponse();
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");

$cterminalsessions = new SCC_TerminalSessions();

$sessiondtls = $cterminalsessions->SelectTerminalSessionDetails($id);
   
                if (count($sessiondtls) > 0)
                {
                        $objResponse->script("popup('popUpDivLPBrowse');");
                        $objResponse->script("popup('popUpDivLPBrowseConfirmed');");
                }
                else
                {
                        $objResponse->script("popup('popUpDivLPBrowse');");
                        $convert = "No Active Session.";
                        $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                        $objResponse->assign("msg", "innerHTML", $convert);
                }
    return $objResponse;
}

function CheckCardStatus()
{
    App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");

    $cwinningsdump = new SCC_WinningsDump();

    $objResponse = new xajaxResponse();
    $login = $_SESSION['user'];
    $arrStr = explode("/", $_SERVER['SCRIPT_NAME']);
    $arrStr = array_reverse($arrStr);
    $page = $arrStr[0];
    $id = $_SESSION['id'];
    
      $isFinalized = $cwinningsdump->CheckCardsifFinalize($id);
    if(count($isFinalized) > 0)
    {
            $objResponse->script("window.location.href='displaycards.php';");
    } 
    else 
    {
                    $cwinningsdump->StartTransaction();
                    $cwinningsdump->DeleteByTerminalID($id);
                    if($cwinningsdump->HasError)
                    {
                                    $cwinningsdump->RollBackTransaction();
                                    $convert = "An Error Occured. Please try again.(Error in deleting to tbl_WinningsDump)";
                                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";
                                    $objResponse->script("popup('popUpDivConvert');");
                                    $objResponse->assign("convert", "innerHTML", $convert);
                                    $objResponse->assign("okbtn", "innerHTML", $okbutton);
                    }
                    else
                    {
                        $cwinningsdump->CommitTransaction();
                    }

    }
    $objResponse->script("hide_loading();");
    return $objResponse;
}

function checkPendingtrans()
{
    App::LoadModuleClass("SweepsCenter", "SCC_TransactionLogs");
    $objResponse = new xajaxResponse();
    $id = $_SESSION['id'];
    $ctranslog = new SCC_TransactionLogs();
    
                            $pendingdtails = $ctranslog->CheckPendingTransactionIfExists($id);
                            if (count($pendingdtails) > 0)
                             {
                                 $convert = "You cannot convert your points because you still have a pending transaction.";
                                                $okbutton = "<img src='images/okbutton.png' onclick=\"window.location='launchpad.php';\" style='cursor: pointer;' />";
                                                $objResponse->script("popup('popUpDivConvert');");
                                                $objResponse->assign("convert", "innerHTML", $convert);
                                                $objResponse->assign("okbtn", "innerHTML", $okbutton);
                             }
return $objResponse;
    
}

function GetBalance()
{
    App::LoadModuleClass("SweepsCenter", "SCC_ref_Services");
    $crefservices = new SCC_ref_Services();

    $arrservice = $crefservices->SelectActiveService();
    $activeservice = $arrservice[0]["ID"];

    $objResponse = new xajaxResponse();
    $login = $_SESSION['user'];
    
    $arrStr = explode("/", $_SERVER['SCRIPT_NAME']);
    $arrStr = array_reverse($arrStr);
    $page = $arrStr[0];
    
    if ($activeservice == 1) // RTG
    {
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
        App::LoadSettings("swcsettings.inc.php");

        $rtgurl = App::getParam("rtg_url");
        $certFilePath = App::getParam("certFilePath");
        $keyFilePath = App::getParam("keyFilePath");

        $capiwrapper = new RealtimeGamingAPIWrapper($rtgurl, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);

        if (isset($_SESSION['IsFreeEntry']))
        {
            $balance = '0.10';
            $balance = "Point Balance: " . $balance;
            $objResponse->assign("balance", "innerHTML", $balance);
            $objResponse->script("toggleBalanceLoading('balance');");
            $objResponse->script("hide_loading();");
        }
        else
        {
            $param = array('delimitedAccountNumbers' => $login);
            $rtgbalance = $capiwrapper->GetBalance($login);
//
//            $filename = "../../APILogs/logs.txt";
//            $fp = fopen($filename, "a");
//            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || " . $login . " || " . serialize($param) . "\r\n");
//            fclose($fp);
            $a = 1;
            while ($a < 4)
            {
                if ($rtgbalance['IsSucceed'] == 1)
                {
                    $balance = $rtgbalance['BalanceInfo']['Balance'];
                    break;
                }
                else
                {
                    $convert = "Error in getting balance.";
                    $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                    $objResponse->assign("msg", "innerHTML", $convert);
                    $objResponse->script("hide_loading();");
                    $a++;
                }
            }

            $balance = number_format($balance, 2, '.', '');
            $balance = "Point Balance: " . $balance;

            $objResponse->assign("balance", "innerHTML", $balance);
            $objResponse->script("hide_loading();");
            $objResponse->script("toggleBalanceLoading('balance');");
        }

        return $objResponse;
    }
    else if ($activeservice == 3) // MG
    {
        $ip_add_MG = $_SESSION['ip'];

        App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
        App::LoadLibrary("MicrogamingAPI.class.php");
        include("../config.php");

        $csmagentsessions = new SCCSM_AgentSessions();

        if (isset($_SESSION['IsFreeEntry']))
        {
            $balance = '0.10';

            $balance = "Point Balance: " . $balance;

            $objResponse->assign("balance", "innerHTML", $balance);
            $objResponse->script("toggleBalanceLoading('balance');");
            $objResponse->script("hide_loading();");
        }
        else
        {
            $sessionguid = $csmagentsessions->SelectSessionGUID();
            $sessionGUID_MG = $sessionguid[0]["SessionGUID"];

            $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>" .
                    "<SessionGUID>" . $sessionGUID_MG . "</SessionGUID>" .
                    "<IPAddress>" . $ip_add_MG . "</IPAddress>" .
                    "<ErrorCode>0</ErrorCode>" .
                    "<IsLengthenSession>true</IsLengthenSession>" .
                    "</AgentSession>";


            $client = new nusoap_client($mg_url, 'wsdl');
            $client->setHeaders($headers);
            $param = array('delimitedAccountNumbers' => $login);
//            $filename = "../../APILogs/logs.txt";
//            $fp = fopen($filename, "a");
//            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || " . $login . " || " . serialize($param) . "\r\n");
//            fclose($fp);
            $a = 1;
            while ($a < 4)
            {
                $result = $client->call('GetAccountBalance', $param);
                if (is_array($result))
                {
                    if (array_key_exists('faultcode', $result))
                    {
                        $convert = $result['faultstring'];
                        $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                        $objResponse->assign("msg", "innerHTML", $convert);
                        $objResponse->script("hide_loading();");
                        $a++;
                    }
                    else
                    {
                        if ($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
                        {
                            $a++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    $convert = "Error in getting balance.";
                    $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                    $objResponse->assign("msg", "innerHTML", $convert);
                    $objResponse->script("hide_loading();");
                    $a++;
                }
            }

            $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];

            $balance = number_format($balance, 2, '.', '');
            
            if($page == 'lobby.php')
            $balance = "Point Balance: " . $balance;
            else
            $balance = "Load Balance: " . $balance;
            
            $objResponse->assign("balance", "innerHTML", $balance);
            $objResponse->script("hide_loading();");
            $objResponse->script("toggleBalanceLoading('balance');");
            
            //after
        }

        return $objResponse;
    }
}

function GetBalanceConv()
{
    App::LoadModuleClass("SweepsCenter", "SCC_ref_Services");
    App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
    $cterminals = new SCC_Terminals();
    $crefservices = new SCC_ref_Services();

    $arrservice = $crefservices->SelectActiveService();
    $activeservice = $arrservice[0]["ID"];

    $objResponse = new xajaxResponse();
    $login = $_SESSION['user'];
$arr = $cterminals->CheckPlayableStatus($login);
if($arr[0][0] == 2 || $arr[0]['IsPlayable'] == 3)
{
    session_destroy();
    $objResponse->script("window.location = 'index.php'");
}else
{
    if ($activeservice == 1) // RTG
    {
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
        App::LoadSettings("swcsettings.inc.php");

        $rtgurl = App::getParam("rtg_url");
        $certFilePath = App::getParam("certFilePath");
        $keyFilePath = App::getParam("keyFilePath");

        $capiwrapper = new RealtimeGamingAPIWrapper($rtgurl, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);

        if (isset($_SESSION['IsFreeEntry']))
        {
            $balance = '0.10';

            $balance = "Point Balance: " . $balance;

            $objResponse->assign("balance", "innerHTML", $balance);
            $objResponse->script("toggleBalanceLoading('balance');");
            $objResponse->script("hide_loading();");
        }
        else
        {
            $param = array('delimitedAccountNumbers' => $login);
            $rtgbalance = $capiwrapper->GetBalance($login);
//            $filename = "../../APILogs/logs.txt";
//            $fp = fopen($filename, "a");
//            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance Convert Points || " . $login . " || " . serialize($param) . "\r\n");
//            fclose($fp);
            $a = 1;
            while ($a < 4)
            {
                if ($rtgbalance['IsSucceed'] == 1)
                {
                    $balance = $rtgbalance['BalanceInfo']['Balance'];
                    break;
                }
                else
                {
                    $a++;
                }
            }
                    $balance = number_format($balance, 2, '.', '');
                    unset($_SESSION['balancefromgetcov']);
                    $_SESSION['balancefromgetcov'] = $balance;
                    $objResponse->assign("convbal", "innerHTML", $balance);
                    $objResponse->script("hide_loading();");
                    $objResponse->script("popup('popUpDivLPConvert');");
        }

        return $objResponse;
    }
    else if ($activeservice == 3) // MG
    {
        $ip_add_MG = $_SESSION['ip'];

        App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
        App::LoadLibrary("MicrogamingAPI.class.php");
        include("../config.php");

        $csmagentsessions = new SCCSM_AgentSessions();

        if (isset($_SESSION['IsFreeEntry']))
        {
            $balance = '0.10';

            $balance = "Point Balance: " . $balance;

            $objResponse->assign("balance", "innerHTML", $balance);
            $objResponse->script("toggleBalanceLoading('balance');");
            $objResponse->script("hide_loading();");
        }
        else
        {
            $sessionguid = $csmagentsessions->SelectSessionGUID();
            $sessionGUID_MG = $sessionguid[0]["SessionGUID"];

            $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>" .
                    "<SessionGUID>" . $sessionGUID_MG . "</SessionGUID>" .
                    "<IPAddress>" . $ip_add_MG . "</IPAddress>" .
                    "<ErrorCode>0</ErrorCode>" .
                    "<IsLengthenSession>true</IsLengthenSession>" .
                    "</AgentSession>";


            $client = new nusoap_client($mg_url, 'wsdl');
            $client->setHeaders($headers);
            $param = array('delimitedAccountNumbers' => $login);
//            $filename = "../../APILogs/logs.txt";
//            $fp = fopen($filename, "a");
//            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || " . $login . " || " . serialize($param) . "\r\n");
//            fclose($fp);
            $a = 1;
            while ($a < 4)
            {
                $result = $client->call('GetAccountBalance', $param);
                if (array_key_exists('faultcode', $result))
                {
                    $convert = $result['faultstring'];
                    $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                    $objResponse->assign("msg", "innerHTML", $convert);
                    $objResponse->script("hide_loading();");
                    $a++;
                }
                else
                {
                    if ($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
                    {
                        $a++;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];
            $balance = number_format($balance, 2, '.', '');
            unset($_SESSION['balancefromgetcov']);
            $_SESSION['balancefromgetcov'] = $balance;
            $objResponse->assign("convbal", "innerHTML", $balance);
            $objResponse->script("hide_loading();");
            $objResponse->script("popup('popUpDivLPConvert');");
        }

        return $objResponse;
    }
}
 return $objResponse;
}

function HeartBeat()
{
    App::LoadModuleClass("SweepsCenter", "SCC_ref_Services");
    $crefservices = new SCC_ref_Services();

    $arrservice = $crefservices->SelectActiveService();
    $activeservice = $arrservice[0]["ID"];

    $objResponse = new xajaxResponse();
    $login = $_SESSION['user'];


    if ($activeservice == 1) // RTG
    {
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
        App::LoadSettings("swcsettings.inc.php");

        $rtgurl = App::getParam("rtg_url");
        $certFilePath = App::getParam("certFilePath");
        $keyFilePath = App::getParam("keyFilePath");

        $capiwrapper = new RealtimeGamingAPIWrapper($rtgurl, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);

        $param = array('delimitedAccountNumbers' => $login);
        $rtgbalance = $capiwrapper->GetBalance($login);
//        $filename = "../../APILogs/logs.txt";
//        $fp = fopen($filename, "a");
//        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance Heartbeat || " . $login . " || " . serialize($param) . "\r\n");
//        fclose($fp);
        $a = 1;
        while ($a < 4)
        {
            if ($rtgbalance['IsSucceed'] == 1)
            {
                break;
            }
            else
            {
                $a++;
            }
        }

        return $objResponse;
    }
    else if ($activeservice == 3) // MG
    {
        $ip_add_MG = $_SESSION['ip'];

        App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
        App::LoadLibrary("MicrogamingAPI.class.php");
        include("../config.php");

        $csmagentsessions = new SCCSM_AgentSessions();

        $sessionguid = $csmagentsessions->SelectSessionGUID();
        $sessionGUID_MG = $sessionguid[0]["SessionGUID"];

        $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>" .
                "<SessionGUID>" . $sessionGUID_MG . "</SessionGUID>" .
                "<IPAddress>" . $ip_add_MG . "</IPAddress>" .
                "<ErrorCode>0</ErrorCode>" .
                "<IsLengthenSession>true</IsLengthenSession>" .
                "</AgentSession>";

        $client = new nusoap_client($mg_url, 'wsdl');
        $client->setHeaders($headers);
        $param = array('delimitedAccountNumbers' => $login);
//        $filename = "../../APILogs/logs.txt";
//        $fp = fopen($filename, "a");
//        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || " . $login . " || " . serialize($param) . "\r\n");
//        fclose($fp);
        $a = 1;
        while ($a < 4)
        {
            $result = $client->call('GetAccountBalance', $param);
            if (array_key_exists('faultcode', $result))
            {
                $convert = $result['faultstring'];
                $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                $objResponse->assign("msg", "innerHTML", $convert);
                $objResponse->script("hide_loading();");
                $a++;
            }
            else
            {
                if ($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
                {
                    $a++;
                }
                else
                {
                    break;
                }
            }
        }

        return $objResponse;
    }
}

function CheckSession($game_id)
{
    $objResponse = new xajaxResponse();

    $id = $_SESSION['id'];
    $login = $_SESSION['user'];
    $passwrd = $_SESSION['pass'];

    App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
    App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
    App::LoadModuleClass("SweepsCenter", "SCC_ref_Services");

    $cterminalsessions = new SCC_TerminalSessions();
    $cterminals = new SCC_Terminals();
    $crefservices = new SCC_ref_Services();

    $sessiondtls = $cterminalsessions->SelectTerminalSessionDetails($id);

    if (count($sessiondtls) > 0)
    {
        if (isset($_SESSION['IsFreeEntry']))
        {
            $convert = "No Active Session.";
            $objResponse->script("popup('popUpDivLPCheckActiveSession');");
            $objResponse->assign("msg", "innerHTML", $convert);
        }
        else
        {
            $arrservice = $crefservices->SelectActiveService();
            $activeservice = $arrservice[0]["ID"];

            /* RTG Games */
            if ($activeservice == 1)
            {
                App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
                App::LoadModuleClass("CasinoAPI", "RealtimeGamingRemoteAuthAPI");
                App::LoadSettings("swcsettings.inc.php");

                $rtg_url_auth = App::getParam("rtg_url_auth");
                $certFilePath = App::getParam("certFilePath");
                $keyFilePath = App::getParam("keyFilePath");

                $capiwrapper = new RealtimeGamingAPIWrapper($rtg_url_auth, RealtimeGamingAPIWrapper::REMOTEAUTH_API, $certFilePath, $keyFilePath);
                $result = $capiwrapper->EncryptCredentials($login, $passwrd);
                $loginenc = $result['PID']['EncryptedLogin'];
                $passwrdenc = $result['PID']['EncryptedPassword'];

                if ($game_id == 'trex')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=18&mid=51&user=" . $loginenc . "&pass=" . $passwrdenc . "'");
                }
                else if ($game_id == 'aztecstreasure')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=18&mid=3&user=" . $loginenc . "&pass=" . $passwrdenc . "'");
                }
                else if ($game_id == 'crystalwaters')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=18&mid=26&user=" . $loginenc . "&pass=" . $passwrdenc . "'");
                }
                else if ($game_id == 'coyotecash')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=18&mid=43&user=" . $loginenc . "&pass=" . $passwrdenc . "'");
                }
                else if ($game_id == 'paydirt')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=18&mid=49&user=" . $loginenc . "&pass=" . $passwrdenc . "'");
                }
                else if ($game_id == 'aladdinswishes')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=18&mid=25&user=" . $loginenc . "&pass=" . $passwrdenc . "'");
                }
                else if ($game_id == 'cleopatrasgold')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=18&mid=2&user=" . $loginenc . "&pass=" . $passwrdenc . "'");
                }
                else if ($game_id == 'shoppingspree2')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=6&mid=162&user=" . $loginenc . "&pass=" . $passwrdenc . "'");
                }
                else if ($game_id == 'baccarat')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=1&mid=0&user=" . $loginenc . "&pass=" . $passwrdenc . "'");
                }
                else if ($game_id == 'blackjack')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=0&mid=0&user=" . $loginenc . "&pass=" . $passwrdenc . "'");
                }
                else if ($game_id == 'keno')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=13&mid=0&user=" . $loginenc . "&pass=" . $passwrdenc . "'");
                }
            }


            /* MG Games */
            else if ($activeservice == 3)
            {
                if ($game_id == 'keno')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=Keno&user=" . $login . "&pass=" . $passwrd . "'");
                }
                else if ($game_id == 'qhorse1')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=PremierRacing&user=" . $login . "&pass=" . $passwrd . "'");
                }
                else if ($game_id == 'qhorse2')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=RoyalDerby&user=" . $login . "&pass=" . $passwrd . "'");
                }
                else if ($game_id == 'dino')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=DinoMight&user=" . $login . "&pass=" . $passwrd . "'");
                }
                else if ($game_id == 'tomb')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=TombRaider&user=" . $login . "&pass=" . $passwrd . "'");
                }
                else if ($game_id == 'bj')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=bj&user=" . $login . "&pass=" . $passwrd . "'");
                }
                else if ($game_id == 'tomb2')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=TombRaiderII&user=" . $login . "&pass=" . $passwrd . "'");
                }
                else if ($game_id == 'wheel')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=WheelofWealth&user=" . $login . "&pass=" . $passwrd . "'");
                }
                else if ($game_id == 'bigkahuna')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=BigKahunaSnakesAndLadders&user=" . $login . "&pass=" . $passwrd . "'");
                }
                else if ($game_id == 'goldfactory')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=GoldFactory&user=" . $login . "&pass=" . $passwrd . "'");
                }
                else if ($game_id == 'karatepig')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=KaratePig&user=" . $login . "&pass=" . $passwrd . "'");
                }
                else if ($game_id == 'surewin')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=SureWin&user=" . $login . "&pass=" . $passwrd . "'");
                }
                else if ($game_id == 'thunderstruck2')
                {
                    $objResponse->script("window.location.href='launcher.php?gid=ThunderStruck2&user=" . $login . "&pass=" . $passwrd . "'");
                }
            }
        }

        $cterminals->StartTransaction();
        $cterminals->UpdateServID($id, 3);
        if ($cterminals->HasError)
        {
            $cterminals->RollBackTransaction();
        }
        else
        {
            $cterminals->CommitTransaction();
        }
    }
    else
    {
        $convert = "No Active Session.";
        $objResponse->script("popup('popUpDivLPCheckActiveSession');");
        $objResponse->assign("msg", "innerHTML", $convert);
    }

    return $objResponse;
}

function CheckSessionSweepsCode()
{
    $objResponse = new xajaxResponse();

    if (isset($_SESSION['IsFreeEntry']))
    {
        $convert = "<p>You have an existing free entry sweeps code.</p></br/> <p>Please convert this point first.</p>";
        $objResponse->script("popup('popUpDivLPCheckActiveSession');");
        $objResponse->assign("msg", "innerHTML", $convert);
    }
    else
    {
        $objResponse->script("location.href='entrycode-page.php';");
    }
    return $objResponse;
}

function BrowseInternet()
{
    $objResponse = new xajaxResponse();

    $pageURL = 'http';
    if (!empty($_SERVER['HTTPS']))
    {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    $folder = $_SERVER["REQUEST_URI"];
    $folder = substr($folder, 0, strrpos($folder, '/') + 1);
    if ($_SERVER["SERVER_PORT"] != "80")
    {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];
    }
    else
    {
        $pageURL .= $_SERVER["SERVER_NAME"];
    }
    $objResponse->script("openurl('" . $pageURL . "/WebBrowserProxy/upload/index.php?id=" . $_SESSION['id'] . "&user=" . $_SESSION['user'] . "'); location.href='launchpad.php';");
    return $objResponse;
}

function RevealCards($i)
{
    $objResponse = new xajaxResponse();

    $terminalid = $_SESSION['id'];
    $resultmsg = $_SESSION['resultmsg'];

    App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");

    $cwinningsdump = new SCC_WinningsDump();
    $winnings_count = $cwinningsdump->SelectAllWinningsIsOpened($terminalid);
    $count = count($winnings_count);
    $avail_cards = $count - 1;
    $avail_cards = "<div class=\"countBox_body\"><p>" . $avail_cards . "</p></div>";

    $cwinningsdump->StartTransaction();

    if ($count > 0)
    {
        $win_type = $winnings_count[0]['WinType'];
        $ecn = $winnings_count[0]['ECN'];
        $id = $winnings_count[0]['ID'];
        $cwinningsdump->UpdateIsOpenedStatus('Y', $id);
        if ($cwinningsdump->HasError)
        {
            $cwinningsdump->RollBackTransaction();
        }
        else
        {
            $cwinningsdump->CommitTransaction();

            $balance = "<div class=\"win_container2\"><p>" . $win_type . "</p><span>" . $ecn . "</span></div>";
            $objResponse->assign(".$i.", "innerHTML", $balance);
            $objResponse->assign("avail_cards", "innerHTML", $avail_cards);

            if ($count == 1)
            {
                $okbtn = "<div class=\"sweeps_quick7\" onclick=\"window.location.href='logout.php'\" ></div>";
                $quick_pick_btn = "<div class=\"sweeps_quick2\" ></div>";
                $objResponse->script("document.getElementById(\"view_summary\").style.visibility = \"visible\";");
                $objResponse->script("document.getElementById(\"view_next_set\").style.visibility = \"hidden\";");
                $objResponse->assign("quick_pick_btn", "innerHTML", $quick_pick_btn);
                $objResponse->assign("resultmsg", "innerHTML", $resultmsg);
                $objResponse->assign("okbtn", "innerHTML", $okbtn);
            }
        }
    }
    else
    {
        $balance = "<div class=\"win_container2\"><div class=\"flipCard\"></div></div>";
        $objResponse->assign(".$i.", "innerHTML", $balance);
        $objResponse->alert("You do not have available scratch card(s)");
    }

    return $objResponse;
}

function ViewOpenedCards()
{
    $objResponse = new xajaxResponse();
    $terminalid = $_SESSION['id'];
    App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");
    $cwinningsdump = new SCC_WinningsDump();
    $winningsdtls = $cwinningsdump->SelectOpenCards($terminalid);
    $opened_cards = "<div style='margin-left: 35px;'>";
    for ($i = 0; $i < count($winningsdtls); $i++)
    {
        $opened_cards .= "<div class=\"sweepsContainer\"><div class=\"win_container\"><p>" . $winningsdtls[$i]['WinType'] . "</p><span>" . $winningsdtls[$i]['ECN'] . "</span></div></div>";
    }
    $opened_cards .= "</div>";
    $objResponse->assign("opened_cards", "innerHTML", $opened_cards);
    return $objResponse;
}

function EnterSweepsCode($sweeps_code)
{
    $objResponse = new xajaxResponse();

    $id = $_SESSION['id'];
    $ip = $_SESSION['ip'];
    $_SESSION['sweeps_code'] = $sweeps_code;
    $login = $_SESSION['user'];

    App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
    App::LoadModuleClass("SweepsCenter", "SCCV_VoucherInfo");
    App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
    App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
    App::LoadModuleClass("SweepsCenter", "SCC_Accounts");
    App::LoadModuleClass("SweepsCenter", "SCC_Sites");
    App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessionDetails");
    App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
    App::LoadModuleClass("SweepsCenter", "SCC_ref_Services");
    App::LoadLibrary("microseconds.php");

    $csmagentsessions = new SCCSM_AgentSessions();
    $cvvoucherinfo = new SCCV_VoucherInfo();
    $ccterminalsessions = new SCC_TerminalSessions();
    $cterminals = new SCC_Terminals();
    $caccounts = new SCC_Accounts();
    $csites = new SCC_Sites();
    $cterminalsessiondtls = new SCC_TerminalSessionDetails();
    $caudittrail = new SCC_AuditTrail();
    $crefservices = new SCC_ref_Services();

    $voucherdtls = $cvvoucherinfo->SelectVoucherDtls($sweeps_code);
    if (count($voucherdtls) > 0)
    {
        $status = $voucherdtls[0]['Status'];
        $entrytype = $voucherdtls[0]['EntryType'];
        $denomination = $voucherdtls[0]['Description'];
    }
    else
    {
        $status = "";
        $entrytype = "";
        $denomination = "";
    }

    if ($status == 2)
    {
        $title = "USED CODE";
        $msg = "The Sweeps Code you have entered has already been used.";
        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
    }
    else if ($status == 3)
    {
        $title = "EXPIRED CODE";
        $msg = "The Sweeps Code you have entered has already expired.";
        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
    }
    else if ($status == 4) // Assigned
    {
        if ($entrytype == 3) // Regular Gaming
        {
            $arrservice = $crefservices->SelectActiveService();
            $activeservice = $arrservice[0]["ID"];

            if ($activeservice == 1)
            {
                App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
                App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
                App::LoadSettings("swcsettings.inc.php");

                $rtgurl = App::getParam("rtg_url");
                $certFilePath = App::getParam("certFilePath");
                $keyFilePath = App::getParam("keyFilePath");

                $capiwrapper = new RealtimeGamingAPIWrapper($rtgurl, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);
                $capiwrapper->SetDepositMethodId(503);

                $filename = "../../APILogs/logs.txt";
                $fp = fopen($filename, "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Deposit || " . $login . " || " . serialize($param) . "\r\n");
                fclose($fp);

                $result = $capiwrapper->Deposit($login, $denomination);

                $errormsg = $result['IsSucceed'];
                $transid_mg = $result["TransactionInfo"]["DepositGenericResult"]["transactionID"];
                $acctno_mg = '';
                $transamt_mg = '';
            }
            else if ($activeservice == 3)
            {
                App::LoadLibrary("MicrogamingAPI.class.php");
                include("../config.php");

                //deposit to MG
                $sessionguid = $csmagentsessions->SelectSessionGUID();
                $sessionGUID_MG = $sessionguid[0]["SessionGUID"];

                $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>" .
                        "<SessionGUID>" . $sessionGUID_MG . "</SessionGUID>" .
                        "<IPAddress>" . $ip . "</IPAddress>" .
                        "<ErrorCode>0</ErrorCode>" .
                        "<IsLengthenSession>true</IsLengthenSession>" .
                        "</AgentSession>";

                $client = new nusoap_client($mg_url, 'wsdl');
                $client->setHeaders($headers);
                $param = array('accountNumber' => $login, 'amount' => $denomination, 'currency' => 1);

                $filename = "../../APILogs/logs.txt";
                $fp = fopen($filename, "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Deposit || " . $login . " || " . serialize($param) . "\r\n");
                fclose($fp);
                $result = $client->call('Deposit', $param);

                $errormsg = $result['DepositResult']['IsSucceed'];
                $transid_mg = $result["DepositResult"]["TransactionId"];
                $acctno_mg = $result["DepositResult"]["AccountNumber"];
                $transamt_mg = $result["DepositResult"]["TransactionAmount"];
            }


            if ($errormsg == 'true' || ($errormsg) || $errormsg == 1)
            {
                $datestamp = udate("Y-m-d H:i:s.u");

                $string = serialize($result);

                //insert to logs
                $filename = "../../APILogs/logs.txt";
                $fp = fopen($filename, "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: DEPOSIT || " . $string . "\r\n");
                fclose($fp);

                $cvvoucherinfo->StartTransaction();
                $cvvoucherinfo->UpdateVoucherInfo($datestamp, $id, $sweeps_code);
                if ($cvvoucherinfo->HasError)
                {
                    $cvvoucherinfo->RollBackTransaction();
                }
                else
                {
                    $cvvoucherinfo->CommitTransaction();
                }

                $session_dtls = $ccterminalsessions->SelectTerminalSessionDetails($id);
                if (count($session_dtls) > 0)
                {
                    /*                     * * RELOAD ** */

                    $sitedtls = $cterminals->SelectTerminalName($id);
                    if (count($sitedtls) > 0)
                    {
                        $siteid = $sitedtls[0]['SiteID'];

                        $acctdtls = $caccounts->SelectAcctIDReloadNonCashier($siteid);
                        $acctid = $acctdtls[0]['ID'];

                        $balancedtls = $csites->SelectSiteDetails($siteid);
                        $balance = $balancedtls[0]['Balance'];

                        if ($balance < $denomination)
                        {
                            $title = "ERROR";
                            $msg = "An Error Occured. Please try again. " . 'Account balance is less than the deposit amount.';
                            $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                        }
                        else
                        {
                            $new_balance = ($balance - $denomination);
                            $csites->StartTransaction();
                            $csites->UpdateSiteBalance($new_balance, $siteid);
                            if ($csites->HasError)
                            {
                                $csites->RollBackTransaction();

                                $title = "ERROR";
                                $msg = "An Error Occured. Please try again. " . 'Error updating site balance.';
                                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                            }
                            else
                            {
                                $csites->CommitTransaction();

                                $cterminals->StartTransaction();
                                $cterminals->UpdateTerminalBalance($balance, $id);
                                if ($cterminals->HasError)
                                {
                                    $cterminals->RollBackTransaction();

                                    $title = "ERROR";
                                    $msg = "An Error Occured. Please try again. " . 'Error updating terminal balance.';
                                    $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                }
                                else
                                {
                                    $cterminals->CommitTransaction();

                                    $terminalsessiondtls = $ccterminalsessions->SelectTerminalSessionDetails($id);
                                    $terminalsessionid = $terminalsessiondtls[0]['ID'];

                                    $lasttransiddtls = $cterminalsessiondtls->SelectMaxId();
                                    $lasttransid = ($lasttransiddtls[0]['max'] == null) ? 0 : $lasttransiddtls[0]['max'];
                                    $lasttransid = ($lasttransid + 1);

                                    $transrefid = 'R' . str_pad($terminalsessionid, 6, 0, STR_PAD_LEFT) . str_pad($lasttransid, 10, 0, STR_PAD_LEFT);

                                    $cterminalsessiondtls->StartTransaction();
                                    $arrTerminalSessionDetails['AcctID'] = $acctid;
                                    $arrTerminalSessionDetails['TerminalSessionID'] = $terminalsessionid;
                                    $arrTerminalSessionDetails['TransactionType'] = 'R';
                                    $arrTerminalSessionDetails['Amount'] = $denomination;
                                    $arrTerminalSessionDetails['ServiceID'] = 1;
                                    $arrTerminalSessionDetails['TransID'] = $transrefid;
                                    $arrTerminalSessionDetails['ServiceTransactionID'] = $transid_mg;
                                    $arrTerminalSessionDetails['TransDate'] = 'now_usec()';
                                    $arrTerminalSessionDetails['DateCreated'] = 'now_usec()';
                                    $cterminalsessiondtls->Insert($arrTerminalSessionDetails);
                                    if ($cterminalsessiondtls->HasError)
                                    {
                                        $cterminalsessiondtls->RollBackTransaction();

                                        $title = "ERROR";
                                        $msg = "An Error Occured. Please try again. " . 'Error inserting new terminal session details.';
                                        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                    }
                                    else
                                    {
                                        $cterminalsessiondtls->CommitTransaction();

                                        $caudittrail->StartTransaction();
                                        $arrAuditTrail["SessionID"] = "";
                                        $arrAuditTrail["AccountID"] = $acctid;
                                        $arrAuditTrail["TransDetails"] = 'Non-cashier reload for terminalid: ' . $id;
                                        $arrAuditTrail["RemoteIP"] = $ip;
                                        $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                        $caudittrail->Insert($arrAuditTrail);
                                        if ($caudittrail->HasError)
                                        {
                                            $caudittrail->RollBackTransaction();

                                            $title = "ERROR";
                                            $msg = "An Error Occured. Please try again. " . 'Error inserting in audit trail..';
                                            $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                        }
                                        else
                                        {
                                            $caudittrail->CommitTransaction();

                                            $title = "SUCCESSFUL RELOAD";
                                            $msg = "You have successfully reloaded " . $denomination . " points using your voucher";
                                            $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"window.location.reload( true ); location.href='launchpad.php'\" style=\"cursor:pointer;\"/>";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        $title = "ERROR";
                        $msg = "An Error Occured. Please try again. " . 'Terminal ID does not exist.';
                        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                    }
                }
                else
                {
                    /*                     * * INITIAL DEPOSIT ** */

                    //start session
                    $terminaldtls = $cterminals->SelectTerminalDetails($id);
                    if (count($terminaldtls) > 0)
                    {
                        $title = "ERROR";
                        $msg = "An Error Occured. Please try again. " . 'Terminal is already in use.';
                        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                    }
                    else
                    {
                        $terminalsessiondtls = $ccterminalsessions->SelectTerminalSessionDetails($id);
                        if (count($terminalsessiondtls) > 0)
                        {
                            $title = "ERROR";
                            $msg = "An Error Occured. Please try again. " . 'Terminal session already exists.';
                            $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                        }
                        else
                        {
                            $terminalnamedtls = $cterminals->SelectTerminalName($id);
                            $isFreeEntry = $terminalnamedtls[0]['IsFreeEntry'];
                            $siteid = $terminalnamedtls[0]['SiteID'];

                            $accountdtls = $caccounts->SelectAcctIDStartSessionNonCashier($id);
                            $acctid = $accountdtls[0]['ID'];

                            if (($denomination == 0) && ($isFreeEntry != 1))
                            {
                                $title = "ERROR";
                                $msg = "An Error Occured. Please try again. " . 'Initial deposit amount must be greater than 0.';
                                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                            }
                            else
                            {
                                $balancedtls = $csites->SelectSiteDetails($siteid);
                                $balance = $balancedtls[0]['Balance'];
                                $new_balance = ($balance - $denomination);

                                if ($balance < $denomination)
                                {
                                    $title = "ERROR";
                                    $msg = "An Error Occured. Please try again. " . 'Account balance is less than the deposit amount.';
                                    $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                }
                                else
                                {
                                    if ($isFreeEntry != 1)
                                    {
                                        $csites->StartTransaction();
                                        $csites->UpdateSiteBalance($new_balance, $siteid);
                                        if ($csites->HasError)
                                        {
                                            $csites->RollBackTransaction();

                                            $title = "ERROR";
                                            $msg = "An Error Occured. Please try again. " . 'Error updating site balance.';
                                            $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                        }
                                        else
                                        {
                                            $csites->CommitTransaction();
                                        }
                                    }

                                    $cterminals->StartTransaction();
                                    $cterminals->UpdateTerminalBalanceServiceIDEquivalentPoints($denomination, $id);
                                    if ($cterminals->HasError)
                                    {
                                        $cterminals->RollBackTransaction();

                                        $title = "ERROR";
                                        $msg = "An Error Occured. Please try again. " . 'Error updating terminal balance.';
                                        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                    }
                                    else
                                    {
                                        $cterminals->CommitTransaction();

                                        $ccterminalsessions->StartTransaction();
                                        $arrTerminalSession['TerminalID'] = $id;
                                        $arrTerminalSession['SiteID'] = $siteid;
                                        $arrTerminalSession['RemoteIP'] = $ip;
                                        $arrTerminalSession['DateStart'] = 'now_usec()';
                                        $arrTerminalSession['DateCreated'] = 'now_usec()';
                                        $ccterminalsessions->Insert($arrTerminalSession);
                                        if ($ccterminalsessions->HasError)
                                        {
                                            $ccterminalsessions->RollBackTransaction();

                                            $title = "ERROR";
                                            $msg = "An Error Occured. Please try again. " . 'Error inserting new terminal session.';
                                            $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                        }
                                        else
                                        {
                                            $ccterminalsessions->CommitTransaction();

                                            $lastinsertid = $ccterminalsessions->LastInsertID;
                                            $maxiddtls = $cterminalsessiondtls->SelectMaxId();
                                            $lasttransid = $maxiddtls[0]['max'];

                                            $transrefid = 'D' . str_pad($lastinsertid, 6, 0, STR_PAD_LEFT) . str_pad($lasttransid, 10, 0, STR_PAD_LEFT);

                                            $cterminalsessiondtls->StartTransaction();
                                            $arrTerminalSessionDtls['TerminalSessionID'] = $lastinsertid;
                                            $arrTerminalSessionDtls['TransactionType'] = 'D';
                                            $arrTerminalSessionDtls['Amount'] = $denomination;
                                            $arrTerminalSessionDtls['ServiceID'] = 1;
                                            $arrTerminalSessionDtls['TransID'] = $transrefid;
                                            $arrTerminalSessionDtls['ServiceTransactionID'] = $transid_mg;
                                            $arrTerminalSessionDtls['AcctID'] = $acctid;
                                            $arrTerminalSessionDtls['TransDate'] = 'now_usec()';
                                            $arrTerminalSessionDtls['DateCreated'] = 'now_usec()';
                                            $cterminalsessiondtls->Insert($arrTerminalSessionDtls);
                                            if ($cterminalsessiondtls->HasError)
                                            {
                                                $cterminalsessiondtls->RollBackTransaction();

                                                $title = "ERROR";
                                                $msg = "An Error Occured. Please try again. " . 'Error inserting new terminal session details.';
                                                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                            }
                                            else
                                            {
                                                $cterminalsessiondtls->CommitTransaction();

                                                $caudittrail->StartTransaction();
                                                $arrAuditTrail["SessionID"] = "";
                                                $arrAuditTrail["AccountID"] = $acctid;
                                                $arrAuditTrail["TransDetails"] = 'Non-cashier session start for terminalid: ' . $id;
                                                $arrAuditTrail["RemoteIP"] = $ip;
                                                $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                                $caudittrail->Insert($arrAuditTrail);
                                                if ($caudittrail->HasError)
                                                {
                                                    $caudittrail->RollBackTransaction();

                                                    $title = "ERROR";
                                                    $msg = "An Error Occured. Please try again. " . 'Error inserting in audit trail.';
                                                    $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                                }
                                                else
                                                {
                                                    $caudittrail->CommitTransaction();

                                                    $title = "SUCCESSFUL INITIAL DEPOSIT";
                                                    $msg = "You have successfully loaded " . $denomination . " points using your voucher. You may use these points to Browse the Internet or Play Games";
                                                    $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"window.location.reload( true ); location.href='launchpad.php'\" style=\"cursor:pointer;\"/>";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                $string = serialize($result);

                //insert to logs
                $filename = "../../APILogs/logs.txt";
                $fp = fopen($filename, "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: DEPOSIT || " . $string . "\r\n");
                fclose($fp);

                $title = "ERROR";
                $msg = "An Error Occured. Please try again. API Deposit Error.";
                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
            }
        }
        else
        {
            //check if there is an existing session
            $sessiondtls = $ccterminalsessions->SelectTerminalSessionDetails($id);

            if (count($sessiondtls) > 0)
            {
                $title = "FREE ENTRY NOT ALLOWED";
                $msg = "Sorry, but you are not allowed to enter a Free Entry code with an ongoing session.";
                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
            }
            else
            {
                $title = "ERROR";
                $msg = "Please enter your Free Entry Sweeps Code on the designated Free Entry terminal.";
                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"location.href='launchpad.php'\" style=\"cursor:pointer;\"/>";
            }
        }
    }
    else
    {
        $title = "INVALID CODE";
        $msg = "You have entered an invalid sweeps code. Please check if you entered the code correctly.";
        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
    }

    $objResponse->script("hide_loading();");
    $objResponse->assign("title", "innerHTML", $title);
    $objResponse->assign("msg", "innerHTML", $msg);
    $objResponse->assign("okbtn", "innerHTML", $okbtn);
    $objResponse->script("popup('popUpDivEntryCode');");

    return $objResponse;
}

function InterBrowsingLogs($btn_name)
{
    $objResponse = new xajaxResponse();
    $login = $_SESSION['user'];
    if ($btn_name == 'browse_btn')
    {
        //insert to logs
        $filename = "../../BrowseInternetLogs/logs.txt";
        $fp = fopen($filename, "a");
        fwrite($fp, date("Y-m-d H:i:s") . " || " . $login . " || BUTTON CLICKED: INTERNET BROWSING BUTTON || \r\n");
        fclose($fp);
    }
    else if ($btn_name == 'cancel_btn')
    {
        //insert to logs
        $filename = "../../BrowseInternetLogs/logs.txt";
        $fp = fopen($filename, "a");
        fwrite($fp, date("Y-m-d H:i:s") . " || " . $login . " || BUTTON CLICKED: INTERNET BROWSING CANCEL BUTTON || \r\n");
        fclose($fp);
    }
    else
    {
        //insert to logs
        $filename = "../../BrowseInternetLogs/logs.txt";
        $fp = fopen($filename, "a");
        fwrite($fp, date("Y-m-d H:i:s") . " || " . $login . " || BUTTON CLICKED: INTERNET BROWSING CONFIRMATION BUTTON || \r\n");
        fclose($fp);
    }

    return $objResponse;
}

function checkPendingReloadTransactions()
{
    $objResponse = new xajaxResponse();

    App::LoadModuleClass("SweepsCenter", "SCC_TransactionLogs");

    $ctranslog = new SCC_TransactionLogs();

    $id = $_SESSION['id'];

    $pendingdtls = $ctranslog->SelectPendingReloadTransaction($id);
    if (count($pendingdtls) > 0)
    {
        $message = '<div class="convertPoints" style="cursor: default;" onclick="javascript: alert(\'You cannot convert your points because you still have a pending reload transaction.\');">
	                    <img src="images/convertPoints.png" height="33px" width="150px" />
	                </div>';
    }
    else
    {
        $message = '<div class="convertPoints" onclick="show_loading(); xajax_GetBalanceConv();">
	                    <img src="images/convertPoints.png" height="33px" width="150px" />
	                </div>';
    }
    $objResponse->assign("convertPointsContainer", "innerHTML", $message);

    return $objResponse;
}

function logtotxt($platform, $transtype, $login, $details)
{
    $filename = "../APILogs/logs.txt";
    $fp = fopen($filename, "a");
    fwrite($fp, date("Y-m-d H:i:s") . " || $platform || TRANSACTION TYPE: $transtype || " . $login . " || " . $details . "\r\n");
    fclose($fp);
}

function TerminalLogout($src)
{
    $objResponse = new xajaxResponse();

    App::LoadModuleClass("SweepsCenter", "SCC_ref_Services");
    $crefservice = new SCC_ref_Services();
    $arrservice = $crefservice->SelectActiveService();

    $activeservice = $arrservice[0]["ID"];
    $login = $_SESSION['user'];

    if ($activeservice == 1)
    {
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingPlayerAPI");
        App::LoadModuleClass("CasinoAPI", "RealtimeGamingRemoteAuthAPI");
        App::LoadSettings("swcsettings.inc.php");

        $rtg_url = App::getParam("rtg_url");
        $certFilePath = App::getParam("certFilePath");
        $keyFilePath = App::getParam("keyFilePath");

        $rtgwrapper = new RealtimeGamingAPIWrapper($rtg_url, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);
        $pidresult = $rtgwrapper->_GetPIDFromLogin($login);
        $pid = $pidresult["PID"];

        $rtgwrapper2 = new RealtimeGamingAPIWrapper($playerurl, RealtimeGamingAPIWrapper::PLAYER_API, $certFilePath, $keyFilePath);
        $logoutresult = $rtgwrapper2->TerminalForcedLogout($login, $pid);
    }
    else if ($activeservice == 3)
    {
        
    }

    $objResponse->script("document.getElementById('flashframe').contentWindow.location.reload();");

    return $objResponse;
}

function confrmConvertPoints()
{
            App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
            $cterminalsession = new SCC_TerminalSessions();
            $id = $_SESSION['id'];
            $objResponse = new xajaxResponse();
            
            $confirmsession = $cterminalsession->getID($id);
                    if($confirmsession)
                    {
                                $balance = number_format($_SESSION['balancefromgetcov'], 2);
                                $balance1=0;
                              
                              if($balance < 1)
                                  $balance1 = 1;
                              else 
                                  $balance1 = $balance;
                              
                              
                                  $convert = "<p>You have converted " . $balance . " point/s for " . number_format(floor($balance1)) . "</p> <p style=\"margin-top: 10px;\">number of sweepstakes card/s. Click PROCEED</p> <p style=\"margin-top: 10px;\">to reveal your cards.</p>";
                                  $okbutton = "<img src='images/ProceedButton.png' onclick=\"convertpoints2();popup('popUpDivConvert');\" style='cursor: pointer;' />";//endterminalsession();
                                  $objResponse->script("popup('popUpDivConvert');");
                                  $objResponse->script("hide_loading();");
                                  $objResponse->assign("convert", "innerHTML", $convert);
                                  $objResponse->assign("okbtn", "innerHTML", $okbutton);
                        
                    }else
                    {
                                    $convert = "No Active Session.";
                                    $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                                    $objResponse->assign("msg", "innerHTML", $convert);
                                    $objResponse->script("hide_loading();");
                    }
    return $objResponse;
}

function ConvertPoints()
{
    App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
    App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
    App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
    App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
    App::LoadModuleClass("SweepsCenter", "SCC_TransactionLogs");
    App::LoadModuleClass("SweepsCenter", "SCC_PointsConversion");
    App::LoadModuleClass("SweepsCenter", "SCC_ref_Services");
    App::LoadLibrary("microseconds.php");
    App::LoadModuleClass("SweepsCenter", "SCCC_TransactionSummary");
    App::LoadModuleClass("SweepsCenter", "SCCC_DeckInfo");
    App::LoadModuleClass("SweepsCenter", "SCCC_TemporaryDeckTable");
    App::LoadModuleClass("SweepsCenter", "SCCC_Winners");
    App::LoadModuleClass("SweepsCenter", "SCCC_TempPrizeList");
    App::LoadModuleClass("SweepsCenter", "SCCC_TempPrizesSummary");
    App::LoadModuleClass("SweepsCenter", "SCCC_TransactionDetails");
    App::LoadModuleClass("SweepsCenter", "SCCV_VoucherInfo");
    App::LoadModuleClass("SweepsCenter", "SCCC_PointsConversionInfo");
    App::LoadModuleClass("SweepsCenter", "SCC_FreeEntry");
    App::LoadModuleClass("SweepsCenter", "SCC_FreeEntryRegistrants");
    App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessionDetails");
    App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");
                                    
    $cwinningsdump = new SCC_WinningsDump();
    $cterminalsessiondtls = new SCC_TerminalSessionDetails();
    $cfreeentryregistrants = new SCC_FreeEntryRegistrants();
    $cfreeentry = new SCC_FreeEntry();
    $ccpointsconversion = new SCCC_PointsConversionInfo();
    $cvvoucherinfo = new SCCV_VoucherInfo();
    $cctransdetails = new SCCC_TransactionDetails();
    $cctempprizesumm = new SCCC_TempPrizesSummary();
    $cctempprizelist = new SCCC_TempPrizeList();
    $ccwinners = new SCCC_Winners();
    $cctempdeck = new SCCC_TemporaryDeckTable();
    $ccdeckinfo = new SCCC_DeckInfo();
    $cctranssumm = new SCCC_TransactionSummary();
    
    $cterminals = new SCC_Terminals();
    $csmagentsessions = new SCCSM_AgentSessions();
    $cterminalsession = new SCC_TerminalSessions();
    $caudittrail = new SCC_AuditTrail();
    $ctranslog = new SCC_TransactionLogs();
    $cpointsconversion = new SCC_PointsConversion();
    $crefservices = new SCC_ref_Services();

    $arrservice = $crefservices->SelectActiveService();
    $activeservice = $arrservice[0]["ID"];

    $objResponse = new xajaxResponse();
      
    $login = $_SESSION['user'];
    $arrStr = explode("/", $_SERVER['SCRIPT_NAME']);
    $arrStr = array_reverse($arrStr);
    $page = $arrStr[0];
    $id = $_SESSION['id'];
    $siteid = $_SESSION['siteid'];
    $ip_add_MG = $_SESSION['ip'];
    $terminal_session_id = $_SESSION['tsi'];
    $terminalbalance = 0;
    $login = $_SESSION['user'];
   
    if (isset($_SESSION['IsFreeEntry']))
    {
        $terminaldtls = $cterminals->SelectTerminalName($id);

        if (count($terminaldtls) > 0)
        {
            $isFreeEntry = $terminaldtls[0]['IsFreeEntry'];
            $cterminalsession->StartTransaction();
            $cterminalsession->UpdateDateEnd($id, $terminal_session_id);
            if ($cterminalsession->HasError)
            {
                $cterminalsession->RollBackTransaction();
                $convert = "An Error Occured. Please try again. (Error in updating tbl_terminalsessions)";
                $okbutton = "<img src='images/ProceedButton.png' onclick=\"popup('popUpDivConvert');\" style='cursor: pointer;' />";
            }
            else
            {
                $cterminalsession->CommitTransaction();
                //insert to logs1
                $filename = "../../DBLogs/logs.txt";
                $fp = fopen($filename, "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_terminalsessions: ". $id .';'. $terminal_session_id . "; \r\n");
                fclose($fp);

                if ($isFreeEntry == 0)
                {
                    $cterminals->StartTransaction();
                    $cterminals->UpdateTerminalStatus($id);
                    if ($cterminals->HasError)
                    {
                        $cterminals->RollBackTransaction();
                        $convert = "An Error Occured. Please try again. (Error in updating tbl_terminals)";
                        $okbutton = "<img src='images/ProceedButton.png' onclick=\"popup('popUpDivConvert');\" style='cursor: pointer;' />";
                    }
                    else
                    {
                        $cterminals->CommitTransaction();
                         //insert to logs2
                        $filename = "../../DBLogs/logs.txt";
                        $fp = fopen($filename, "a");
                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_terminals: ". $id . "; \r\n");
                        fclose($fp);
                    }
                }

                $caudittrail->StartTransaction();
                $arrAuditTrail["SessionID"] = "";
                $arrAuditTrail["AccountID"] = 0;
                $arrAuditTrail["TransDetails"] = "Session end for terminalid: " . $id;
                $arrAuditTrail["RemoteIP"] = "";
                $arrAuditTrail["TransDateTime"] = 'now_usec()';
                $caudittrail->Insert($arrAuditTrail);
                if ($caudittrail->HasError)
                {
                    $caudittrail->RollBackTransaction();
                    $convert = "An Error Occured. Please try again. (Error in inserting to tbl_audittrail)";
                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"popup('popUpDivConvert');\" style='cursor: pointer;' />";
                }
                else
                {
                    $caudittrail->CommitTransaction();
                    //insert to logs3
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tbl_audittrail: " . serialize($arrAuditTrail) . "; \r\n");
                    fclose($fp);
                    
                    $objResponse->script("GetRegularGamingCards();");
                    $convert = "You have converted 0.10 point/s for 1 number of sweepstakes card/s. Click PROCEED to reveal your cards.";
                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"display_loading_img(); location.href='displaycards.php';\" style='cursor: pointer;' />";
                }
            }
        }
        else
        {
            $convert = "An Error Occured. Please try again. " . $return_msg;
            $okbutton = "<img src='images/ProceedButton.png' onclick=\"popup('popUpDivConvert');\" style='cursor: pointer;' />";
        }

        $objResponse->script("popup('popUpDivConvert');");
        $objResponse->assign("convert", "innerHTML", $convert);
        $objResponse->assign("okbtn", "innerHTML", $okbutton);
    }
    else
    {
        $sessiondtls = $cterminalsession->SelectTerminalSessionDetails($id);
        if (count($sessiondtls) > 0)
        {
            $balancedtls = $cterminals->SelectBalance($id);
            if (count($balancedtls) > 0)
            {
                $terminalbalance = $balancedtls[0]['Balance'];
            }

            if ($activeservice == 1) // RTG
            {
                App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
                App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
                App::LoadSettings("swcsettings.inc.php");

                $rtgurl = App::getParam("rtg_url");
                $certFilePath = App::getParam("certFilePath");
                $keyFilePath = App::getParam("keyFilePath");

                $capiwrapper = new RealtimeGamingAPIWrapper($rtgurl, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);
                $param = array('delimitedAccountNumbers' => $login);
                $filename = "../../APILogs/logs.txt";
                $fp = fopen($filename, "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || " . $login . " || " . serialize($param) . "\r\n");
                fclose($fp);
                $a = 1;
                $isSucceed = false;
                $string = '';

                while ($a < 4)
                {
                    $result = $capiwrapper->GetBalance($login);
                    if ($result && is_array($result))
                    {
                        $isSucceed = $result['IsSucceed'];
                        if (array_key_exists('faultcode', $result))
                        {
                            $convert = $result['faultstring'];
                            $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                            $objResponse->assign("msg", "innerHTML", $convert);
                            $objResponse->script("hide_loading();");
                            $a++;
                        }
                        else
                        {
                            if ($isSucceed == 'true')
                            {
                                break;
                            }
                            else
                            {
                                $a++;
                            }
                        }
                    }
                    else
                    {
                        $a++;
                    }
                }

                if ($isSucceed == true)
                {
                    $balance = $result['BalanceInfo']['Balance'];
                }
                else
                {
                    $convert = "Error in serializing Get Balance.";
                    $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                    $objResponse->assign("msg", "innerHTML", $convert);
                    $objResponse->script("hide_loading();");
                }
            }
            else if ($activeservice == 3) // MG
            {
                App::LoadLibrary("MicrogamingAPI.class.php");
                include("../config.php");

                $sessionguid = $csmagentsessions->SelectSessionGUID();
                $sessionGUID_MG = $sessionguid[0]["SessionGUID"];

                $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>" .
                        "<SessionGUID>" . $sessionGUID_MG . "</SessionGUID>" .
                        "<IPAddress>" . $ip_add_MG . "</IPAddress>" .
                        "<ErrorCode>0</ErrorCode>" .
                        "<IsLengthenSession>true</IsLengthenSession>" .
                        "</AgentSession>";

                $client = new nusoap_client($mg_url, 'wsdl');
                $client->setHeaders($headers);
                $param = array('delimitedAccountNumbers' => $login);
                if (serialize($param) != "b:0")
                {
                    $filename = "../../APILogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || " . $login . " || " . serialize($param) . "\r\n");
                    fclose($fp);
                    $a = 1;
                    while ($a < 4)
                    {
                        $result = $client->call('GetAccountBalance', $param);

                        if (array_key_exists('faultcode', $result))
                        {
                            $convert = $result['faultstring'];
                            $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                            $objResponse->assign("msg", "innerHTML", $convert);
                            $objResponse->script("hide_loading();");
                            $a++;
                        }
                        else
                        {
                            if ($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
                            {
                                $a++;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    if (($string == "b:0") || (count($result) == 0) || (count($result["GetAccountBalanceResult"]["BalanceResult"]) != 8)) // Added by Arlene R. Salazar 04-29-2012
                    {
                        $convert = "Error in serializing Get Balance.";
                        $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                        $objResponse->assign("msg", "innerHTML", $convert);
                        $objResponse->script("hide_loading();");
                    }
                    else
                    {
                        $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];
                    }
                }
            }

            $filename = "../../APILogs/logs.txt";
            $fp = fopen($filename, "a");
            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GET TERMINAL BALANCE(CONVERT POINTS) || " . $login . " || " . serialize($result) . "\r\n");
            fclose($fp);

                //INSERT TO LOGS
                $datestamp = udate("YmdHisu");

                $ctranslog->StartTransaction();
                $arrTransactionLog["TerminalSessionID"] = $datestamp;
                $arrTransactionLog["TerminalID"] = $id;
                $arrTransactionLog["ServiceID"] = 3;
                $arrTransactionLog["DateCreated"] = 'now_usec()';
                $arrTransactionLog["DateUpdated"] = 'now_usec()';
                $arrTransactionLog["TransactionType"] = 'W';
                $arrTransactionLog["Amount"] = $balance;
                $arrTransactionLog["LaunchPad"] = 1;
                $ctranslog->Insert($arrTransactionLog);
                if ($ctranslog->HasError)
                {
                    $ctranslog->RollBackTransaction();

                    $convert = "An Error Occured. Please try again.(Error in inserting to tbl_transactionlogs)";
                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                    $objResponse->script("popup('popUpDivConvert');");
                    $objResponse->assign("convert", "innerHTML", $convert);
                    $objResponse->assign("okbtn", "innerHTML", $okbutton);
                }
                else
                {
                    $ctranslog->CommitTransaction();

                    //insert to logs
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tbl_transactionlogs: " . serialize($arrTransactionLog) . " \r\n");
                    fclose($fp);

                    $param_withdraw = array('accountNumber' => $login, 'amount' => $balance, 'currency' => 1);
                    $filename = "../../APILogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Withdrawal || " . $login . " || " . serialize($param_withdraw) . "\r\n");
                    fclose($fp);

            if ($balance > 0)
            {
                    if ($activeservice == 1)
                    {
                        $capiwrapper->SetWithdrawalMethodId(502);
                        $result_withdraw = $capiwrapper->Withdraw($login, $balance);
                        $string = serialize($result_withdraw);
                        $errormsg = $result_withdraw["IsSucceed"];
                        $externaltransid = $result_withdraw["TransactionInfo"]["WithdrawGenericResult"]["transactionID"]; //$result_withdraw["TransactionId"];
                    }
                    else if ($activeservice == 3)
                    {
                        $filename = "../../APILogs/logs.txt";
                        $fp = fopen($filename, "a");
                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Withdrawal || " . $login . " || " . serialize($param_withdraw) . "\r\n");
                        fclose($fp);

                        $result_withdraw = $client->call('Withdrawal', $param_withdraw);
                        $string = serialize($result_withdraw);

                        if (($string == "b:0") || (count($result_withdraw) == 0) || (count($result_withdraw["WithdrawalResult"]) != 8))
                        {
                            $convert = "Error in serializing Withdrawal.";
                            $objResponse->script("popup('popUpDivConvert');");
                            $objResponse->assign("convert", "innerHTML", $convert);
                            $objResponse->assign("okbtn", "innerHTML", $okbutton);
                        }
                        else
                        {
                            $errormsg = $result_withdraw["WithdrawalResult"]["IsSucceed"];
                            $externaltransid = $result_withdraw["WithdrawalResult"]["TransactionId"];
                            $transaction_amt = $result_withdraw["WithdrawalResult"]["TransactionAmount"];
                            $transaction_credit_amt = $result_withdraw["WithdrawalResult"]["TransactionCreditAmount"];
                            $credit_bal = $result_withdraw["WithdrawalResult"]["CreditBalance"];
                            $balance_mg = $result_withdraw["WithdrawalResult"]["Balance"];
                        }

                        //start add 01182013 mtcc
                        $filename = "../../XMLLogs/apixml.txt";
                        $fp = fopen($filename , "a");
                        fwrite($fp, "WITHDRAW REQUEST: " . date("Y-m-d H:i:s") . " || " . $login . " || " . $client->request . "\r\n");   
                        fwrite($fp, "WITHDRAW RESPONSE: " . date("Y-m-d H:i:s") . " || " . $login . " || " .$client->response . "\r\n");   
                        fclose($fp);
                        //end add 01182013 mtcc
                    }
            }else
            {
                $forbonusOnePoint = true;
            }
                    $filename = "../../APILogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(CONVERT POINTS) || " . $login . " || " . serialize($result_withdraw) . "\r\n");
                    fclose($fp);
                   
                    if($balance < 1)//if balance is like 0.84
                    {
                            $balance_orig = $balance;
                            $balance = 1;
                    }
                    if ($errormsg == 'true' || ($errormsg) || $errormsg == 1 || $forbonusOnePoint = true)
                    {
                            $terminaldtls = $cterminals->SelectTerminalName($id);
                            if (count($terminaldtls) > 0)
                            {
                                $processedby = $terminaldtls[0]['Name'];
                                $maxid_dtls = $cpointsconversion->SelectMaxID();
                                $lasttransid = ($maxid_dtls[0]['maxid'] != null) ? $maxid_dtls[0]['maxid'] : 0;
                                $lasttransid = ($lasttransid + 1);
                                $transid = 'C' . str_pad($id, 6, 0, STR_PAD_LEFT) . str_pad($lasttransid, 10, 0, STR_PAD_LEFT);
                                $balance = ($balance < 1) ? 1 : $balance;

                                $cpointsconversion->StartTransaction();
                                
                                $arrPointsConversion['SiteID'] = $siteid;
                                $arrPointsConversion['TerminalID'] = $id;
                                $arrPointsConversion['TransID'] = $transid;
                                $arrPointsConversion['Balance'] = $balance;
                                $arrPointsConversion['EquivalentPoints'] = floor($balance);
                                $arrPointsConversion['TransDate'] = 'now_usec()';
                                $arrPointsConversion['ProcessedBy'] = $processedby;
                                $cpointsconversion->Insert($arrPointsConversion);
                                if ($cpointsconversion->HasError)
                                {
                                    $cpointsconversion->RollBackTransaction();

                                    $convert = "An Error Occured. Please try again.(Error in inserting to tbl_pointsconversion)";
                                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                    $objResponse->script("popup('popUpDivConvert');");
                                    $objResponse->assign("convert", "innerHTML", $convert);
                                    $objResponse->assign("okbtn", "innerHTML", $okbutton);
                                }
                                else
                                {
                                    $cpointsconversion->CommitTransaction();
                                    //insert to logs4
                                    $filename = "../../DBLogs/logs.txt";
                                    $fp = fopen($filename, "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tbl_pointsconversion: " . serialize($arrPointsConversion) . "; \r\n");
                                    fclose($fp);
                                    
                                    if($balance_orig < 1 && $balance_orig != NULL)//if balance is like 0.84
                                    {
                                        $_SESSION['equivalentpoints'] = floor($balance);
                                        $_SESSION['convertedbalance'] = $balance_orig;

                                    }
                                    else
                                    {
                                        $_SESSION['equivalentpoints'] = floor($balance);
                                        $_SESSION['convertedbalance'] = $balance;
                                    }

                                    /*start
                                    * @author Jerome Jose
                                    * @date Feb 8 2013
                                    * @purpose to Combining Convert points and getregulargaming
                                    */
                                    
                                    $terminalid = $_SESSION['id'];
                                    $id = $_SESSION['id'];
                                    $login = $_SESSION['user'];
                                    $convertedpts = $_SESSION['equivalentpoints'];
                                    $convertedbalance = $_SESSION['convertedbalance'];
                                    $projectid = 2;
                                    $remainingcardcnt2 = 0;
                                    if(isset ($_SESSION['sweeps_code']))
                                    {
                                        $sweeps_code = $_SESSION['sweeps_code'];
                                    }
                                    else
                                    {
                                        $sweeps_code = '';
                                    }

                                    if(isset ($_SESSION['IsFreeEntry']))
                                    {
                                        $swps_cde = $_SESSION['sweeps_code'];
                                        $convertedpts = '1';
                                        $convertedbalance = 0.10;
                                        $option2 = 2;
                                    }
                                    else
                                    {
                                        $swps_cde = "";
                                        $option2 = 1;
                                    }
                                                                                                                       
                                    $cctranssumm->StartTransaction();
                                    $arrTransactionSummary['RequestedBy'] = $id;
                                    $arrTransactionSummary['TransactionDate'] = 'now_usec()';
                                    $arrTransactionSummary['CardCount'] = $convertedpts;
                                    $arrTransactionSummary['Status'] = 0;
                                    $arrTransactionSummary['Option1'] = $id;
                                    $cctranssumm->Insert($arrTransactionSummary);
                                    if($cctranssumm->HasError)
                                    {
                                        $cctranssumm->RollBackTransaction();
                                        echo "Error has occured: " . $cctranssumm->getError();
                                    }
                                    else
                                    {
                                        $cctranssumm->CommitTransaction();
                                            /*insert to logs15*/
                                        $filename = "../../DBLogs/logs.txt";
                                        $fp = fopen($filename, "a");
                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO transactionsummary: " . serialize($arrTransactionSummary) ."; \r\n");
                                        fclose($fp);

                                        $transsummaryid = $cctranssumm->LastInsertID;

                                        $deckdtls = $ccdeckinfo->SelectCurrentDeck($projectid, 1);
                                        $deckid = $deckdtls[0]['DeckID'];
                                        $decksize = $deckdtls[0]['CardCount'];
                                        $usedcardcount = ($deckdtls[0]['UsedCardCount'] != null) ? $deckdtls[0]['UsedCardCount'] : 0;
                                        $winningcardcount = ($deckdtls[0]['WinningCardCount'] != null) ? $deckdtls[0]['WinningCardCount'] : 0;
                                        $remainingcardcount = ($decksize - $usedcardcount);
                                                                
                                        if($convertedpts >= $remainingcardcount)
                                        {
                                            $ccdeckinfo->StartTransaction();
                                            $ccdeckinfo->UpdateDeckNStatus($deckid,$id,$option2,$transsummaryid,floor($remainingcardcount));
                                            if($ccdeckinfo->HasError)
                                            {
                                                $ccdeckinfo->RollBackTransaction();
                                                echo "Error has occured: " . $ccdeckinfo->getError();
                                            }
                                            else
                                            {
                                                $ccdeckinfo->CommitTransaction();

                                                /*insert to logs16*/
                                                $filename = "../../DBLogs/logs.txt";
                                                $fp = fopen($filename, "a");
                                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: ".$deckid.';'.$id.';'.$option2.';'.$transsummaryid.';'.floor($remainingcardcount)."; \r\n");
                                                fclose($fp);

                                                $cctempdeck->StartTransaction();
                                                $cctempdeck->InsertSelectedCards($deckid,$transsummaryid);
                                                if($cctempdeck->HasError)
                                                {
                                                    $cctempdeck->RollBackTransaction();
                                                    echo "Error has occured: " . $cctempdeck->getError();
                                                }
                                                else
                                                {
                                                    $cctempdeck->CommitTransaction();

                                                    /*insert to logs17*/
                                                    $filename = "../../DBLogs/logs.txt";
                                                    $fp = fopen($filename, "a");
                                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO temporarydecktable: ".$deckid.';'.$transsummaryid."; \r\n");
                                                    fclose($fp);

                                                    $usedcardcnt = ($usedcardcount + $remainingcardcount);
                                                    $ccdeckinfo->StartTransaction();
                                                    $ccdeckinfo->DeactivateDeck(2,$usedcardcnt, $deckid);
                                                    if($ccdeckinfo->HasError)
                                                    {
                                                        $ccdeckinfo->RollBackTransaction();
                                                        echo "Error has occured: " . $ccdeckinfo->getError();
                                                    }
                                                    else
                                                    {
                                                        $ccdeckinfo->CommitTransaction();

                                                        /*insert to logs18*/
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename, "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO deckinfo: ".$usedcardcnt.';'.$deckid."; \r\n");
                                                        fclose($fp);

                                                        $deckdtls = $ccdeckinfo->SelectCurrentDeck($projectid, 0);
                                                        $deckid = $deckdtls[0]['DeckID'];
                                                        $decksize = $deckdtls[0]['CardCount'];
                                                        $usedcardcount = ($deckdtls[0]['UsedCardCount'] != null) ? $deckdtls[0]['UsedCardCount'] : 0;
                                                        $winningcardcount = ($deckdtls[0]['WinningCardCount'] != null) ? $deckdtls[0]['WinningCardCount'] : 0;

                                                        $remainingcardcnt2 = ($convertedpts - $remainingcardcount);

                                                        $ccdeckinfo->StartTransaction();
                                                        $ccdeckinfo->ActivateQueuedDeck(1,$remainingcardcnt2,$deckid);
                                                        if($ccdeckinfo->HasError)
                                                        {
                                                            $ccdeckinfo->RollBackTransaction();
                                                            echo "Error has occured: " . $ccdeckinfo->getError();
                                                        }
                                                        else
                                                        {
                                                            $ccdeckinfo->CommitTransaction();

                                                            /*insert to logs19*/
                                                            $filename = "../../DBLogs/logs.txt";
                                                            $fp = fopen($filename, "a");
                                                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: ".$remainingcardcnt2.';'.$deckid."; \r\n");
                                                            fclose($fp);

                                                            $ccdeckinfo->StartTransaction();
                                                            $ccdeckinfo->UpdateDeckNStatus($deckid,$id,$option2,$transsummaryid,$remainingcardcnt2);
                                                            if($ccdeckinfo->HasError)
                                                            {
                                                                $ccdeckinfo->RollBackTransaction();
                                                                echo "Error has occured: " . $ccdeckinfo->getError();
                                                            }
                                                            else
                                                            {
                                                                $ccdeckinfo->CommitTransaction();

                                                                /*insert to logs20*/
                                                                $filename = "../../DBLogs/logs.txt";
                                                                $fp = fopen($filename, "a");
                                                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: ".$deckid.';'.$id.';'.$option2.';'.$transsummaryid.';'.$remainingcardcnt2."; \r\n");
                                                                fclose($fp);

                                                                $cctempdeck->StartTransaction();
                                                                $cctempdeck->InsertSelectedCards($deckid,$transsummaryid);
                                                                if($cctempdeck->HasError)
                                                                {
                                                                    $cctempdeck->RollBackTransaction();
                                                                    echo "Error has occured: " . $cctempdeck->getError();
                                                                }
                                                                else
                                                                {
                                                                    $cctempdeck->CommitTransaction();
                                                                    /*insert to logs21*/
                                                                    $filename = "../../DBLogs/logs.txt";
                                                                    $fp = fopen($filename, "a");
                                                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE temporarydecktable: ".$deckid.';'.$transsummaryid."; \r\n");
                                                                    fclose($fp);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $remainingcardcount = $convertedpts;

                                            $ccdeckinfo->StartTransaction();
                                            $ccdeckinfo->UpdateDeckNStatus($deckid,$id,$option2,$transsummaryid,floor($remainingcardcount));
                                            if($ccdeckinfo->HasError)
                                            {
                                                $ccdeckinfo->RollBackTransaction();
                                                echo "Error has occured: " . $ccdeckinfo->getError();
                                            }
                                            else
                                            {
                                                $ccdeckinfo->CommitTransaction();

                                                /*insert to logs22*/
                                                $filename = "../../DBLogs/logs.txt";
                                                $fp = fopen($filename, "a");
                                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deck_".$deckid.": ".$deckid.';'.$id.';'.$option2.';'.$transsummaryid.';'.floor($remainingcardcount)."; \r\n");
                                                fclose($fp);

                                                $cctempdeck->StartTransaction();
                                                $cctempdeck->InsertSelectedCards($deckid,$transsummaryid);
                                                if($cctempdeck->HasError)
                                                {
                                                    $cctempdeck->RollBackTransaction();
                                                    echo "Error has occured: " . $cctempdeck->getError();
                                                }
                                                else
                                                {
                                                    $cctempdeck->CommitTransaction();
                                                    /*insert to logs23*/
                                                    $filename = "../../DBLogs/logs.txt";
                                                    $fp = fopen($filename, "a");
                                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO temporarydecktable: ".$deckid.';'.$transsummaryid."; \r\n");
                                                    fclose($fp);
                                                }
                                            }
                                        }
                            
                                        $cctempdeck->StartTransaction();
                                        $cctempdeck->UpdatePrizeDescription($transsummaryid);
                                        if($cctempdeck->HasError)
                                        {
                                            $cctempdeck->RollBackTransaction();
                                            echo "Error has occured: " . $cctempdeck->getError();
                                        }
                                        else
                                        {
                                            $cctempdeck->CommitTransaction();

                                            /*insert to logs24*/
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp = fopen($filename, "a");
                                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO temporarydecktable: ".$transsummaryid."; \r\n");
                                            fclose($fp);

                                
                                            $insertintowinners =  $cctempdeck->SelectIsWinningCard($transsummaryid);
                                            if(count($insertintowinners) > 0)
                                            {
                                                $ccwinners->StartTransaction();
                                                $ccwinners->InsertDataFromTemporaryDeck($projectid,$deckid,$transsummaryid,$id);
                                                if($ccwinners->HasError)
                                                {
                                                    $ccwinners->RollBackTransaction();
                                                    echo "Error has occured: " . $ccwinners->getError();
                                                }
                                                else
                                                {
                                                    $ccwinners->CommitTransaction();
                                                    /*insert to logs25*/
                                                    $filename = "../../DBLogs/logs.txt";
                                                    $fp = fopen($filename, "a");
                                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO winners: ".$projectid.';'.$deckid.';'.$transsummaryid.';'.$id."; \r\n");
                                                    fclose($fp);
                                                }
                                            }
                    
                                            $totalcashwinningdtls = $cctempdeck->SelectTotalCashWinning($transsummaryid);
                                            $totalcashwinning = $totalcashwinningdtls[0]['TotalCash'];

                                            $winningscountdtls = $cctempdeck->SelectWinningCardCount($transsummaryid);
                                            $winningscount = $winningscountdtls[0]['cardcount'];

                                            if($winningscount > 0)
                                            {
                                                $cctempprizelist->StartTransaction();
                                                $cctempprizelist->InsertDetailsFromTemporaryDeck($transsummaryid);
                                                if($cctempprizelist->HasError)
                                                {
                                                    $cctempprizelist->RollBackTransaction();
                                                    echo "Error has occured: " . $cctempprizelist->getError();
                                                }
                                                else
                                                {
                                                    $cctempprizelist->CommitTransaction();
                                                    /*insert to logs26*/
                                                    $filename = "../../DBLogs/logs.txt";
                                                    $fp = fopen($filename, "a");
                                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tempprizelist: ".$transsummaryid."; \r\n");
                                                    fclose($fp);
                                                }

                                                $cctempprizesumm->StartTransaction();
                                                $cctempprizesumm->InsertRecordsFromTemporaryDeck($transsummaryid);
                                                if($cctempprizesumm->HasError)
                                                {
                                                    $cctempprizesumm->RollBackTransaction();
                                                    echo "Error has occured: " . $cctempprizelist->getError();
                                                }
                                                else
                                                {
                                                    $cctempprizesumm->CommitTransaction();
                                                    /*insert to logs27*/
                                                    $filename = "../../DBLogs/logs.txt";
                                                    $fp = fopen($filename, "a");
                                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tempprizesummary: ".$transsummaryid."; \r\n");
                                                    fclose($fp);
                                                }
                                            }

                                            $cctransdetails->StartTransaction();
                                            $arrTransactionDetails['TransactionSummaryID'] = $transsummaryid;
                                            $arrTransactionDetails['DeckID'] = $deckid;
                                            $arrTransactionDetails['CardCount'] = $convertedpts;
                                            $arrTransactionDetails['TransactionDate'] = 'now_usec()';
                                            $cctransdetails->Insert($arrTransactionDetails);
                                            if($cctransdetails->HasError)
                                            {
                                                $cctransdetails->RollBackTransaction();
                                                echo "Error has occured: " . $cctransdetails->getError();
                                            }
                                            else
                                            {
                                                $cctransdetails->CommitTransaction();
                                                /*insert to logs28*/
                                                $filename = "../../DBLogs/logs.txt";
                                                $fp = fopen($filename, "a");
                                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO transactiondetails: ".  serialize($arrTransactionDetails)."; \r\n");
                                                fclose($fp);

                                                $cctranssumm->StartTransaction();
                                                $cctranssumm->UpdateStatus(1,$transsummaryid);
                                                if($cctranssumm->HasError)
                                                {
                                                    $cctranssumm->RollBackTransaction();
                                                }
                                                else
                                                {
                                                    $cctranssumm->CommitTransaction();
                                                    /*insert to logs29*/
                                                    $filename = "../../DBLogs/logs.txt";
                                                    $fp = fopen($filename, "a");
                                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE transactionsummary: ".$transsummaryid."; \r\n");
                                                    fclose($fp);

                                                    $usedcardcount = ($usedcardcount + $convertedpts);
                                                    $winningcardcount = ($winningcardcount + $winningscount);

                                                    $ccdeckinfo->StartTransaction();
                                                    if($remainingcardcnt2 > 0)
                                                    {
                                                        $ccdeckinfo->UpdateWinningCardCount($winningcardcount,'',$deckid);
                                                    }
                                                    else
                                                    {
                                                        $ccdeckinfo->UpdateWinningCardCount($winningcardcount,$usedcardcount,$deckid);
                                                    }
                                                    if($ccdeckinfo->HasError)
                                                    {
                                                        $ccdeckinfo->RollBackTransaction();
                                                        echo "Error has occured: " . $cctransdetails->getError();
                                                    }
                                                    else
                                                    {
                                                        $ccdeckinfo->CommitTransaction();

                                                        /*insert to logs30*/
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename, "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: ".$winningcardcount.';'.$deckid.';'.$usedcardcount."; \r\n");
                                                        fclose($fp);

                                                        if($option2 == 1)
                                                        {
                                                            if($winningscount > 0)
                                                            {
                                                                $totalcashwinning = number_format($totalcashwinning,2);
                                                                $totalcash = "$" . $totalcashwinning . " Cash";

                                                                if($totalcashwinning > 0)
                                                                {
                                                                    $allPrizes = $totalcash;
                                                                }
                                                                else
                                                                {
                                                                    $allPrizes = "";
                                                                }

                                                                $prizecountdtls = $cctempprizesumm->SelectPrizeDescCount($transsummaryid);
                                                                $prizecount = $prizecountdtls[0]['prizedesccount'];

                                                                for($ctr3 = 1 ; $ctr3 <= $prizecount ; $ctr3++)
                                                                {
                                                                    $tempprizesummdtls = $cctempprizesumm->SelectTempPrizeSummaryDtls($transsummaryid);
                                                                    $vchr_Temp = $tempprizesummdtls[0]['PrizeDescription'];
                                                                    $int_TempCnt = $tempprizesummdtls[0]['PrizeCount'];
                                                                    $int_TempID = $tempprizesummdtls[0]['ID'];
                                                                    //$int_Denomination = $tempprizesummdtls[0]['PrizeDenomination'];
                                                                    $int_TempPrizeType = $tempprizesummdtls[0]['PrizeType'];

                                                                    $noncash = "(" . $int_TempCnt  . ") " . $vchr_Temp;
                                                                    $allPrizes .= "," . $noncash;

                                                                    $cctempprizesumm->StartTransaction();
                                                                    $cctempprizesumm->DeleteRecord($int_TempID);
                                                                    if($cctempprizesumm->HasError)
                                                                    {
                                                                        $cctempprizesumm->RollBackTransaction();
                                                                        echo "Error has occured: " . $cctransdetails->getError();
                                                                    }
                                                                    else
                                                                    {
                                                                        $cctempprizesumm->CommitTransaction();

                                                                    }
                                                                }
                                                                
                                                                /*insert to logs31*/
                                                                $filename = "../../DBLogs/logs.txt";
                                                                $fp = fopen($filename, "a");
                                                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || DELETE FROM tempprizesummary: ".$int_TempID."; \r\n");
                                                                fclose($fp);
                                                        
                                                                $respondmessage = "You've won the following prize/s: " . $allPrizes . ". Your Transaction Reference ID is " .  str_pad($transsummaryid,14,0,STR_PAD_LEFT) . ".  Please proceed to the cashier to claim your winnings.";
                                                            }
                                                            else
                                                            {
                                                                $respondmessage = "You did not get any winning card. Your Transaction Reference ID is " .  str_pad($transsummaryid,14,0,STR_PAD_LEFT) . ".  Thank you for playing.";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            $tempdeckdtls = $cctempdeck->SelectTempDeckDetailsByTransSummID($transsummaryid);
                                                            $allPrizes = $tempdeckdtls[0]['PrizeDescription'];
                                                            $vchr_ECN = $tempdeckdtls[0]['ECN'];
                                                            $int_TempPrizeType = $tempdeckdtls[0]['PrizeType'];

                                                            if($projectid == 2)
                                                            {
                                                                $vchr_ECN = str_pad($transsummaryid,14,0,STR_PAD_LEFT);
                                                            }

                                                            if($winningscount > 0)
                                                            {
                                                                $respondmessage = "You've won " .  $allPrizes . ". Your Transaction Reference ID is " . $vchr_ECN . ".<br />Please proceed to the cashier to claim your winnings.";
                                                            }
                                                            else
                                                            {
                                                                $respondmessage = "This is not a winning card. Your Transaction Reference ID is " . $vchr_ECN . ".<br /> Try our sweepstakes games and increase your chance of winning more and big prizes!<br />Thank you for playing.";
                                                            }
                                                        }


                                                        $cctranssumm->StartTransaction();
                                                        $cctranssumm->UpdateWinnings($winningcardcount,$allPrizes,$transsummaryid);
                                                        if($cctranssumm->HasError)
                                                        {
                                                            $cctranssumm->RollBackTransaction();
                                                            echo "Error has occured: " . $cctranssumm->getError();
                                                        }
                                                        else
                                                        {
                                                            $cctranssumm->CommitTransaction();

                                                            /*insert to logs32*/
                                                            $filename = "../../DBLogs/logs.txt";
                                                            $fp = fopen($filename, "a");
                                                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE transactionsummary: ".$winningcardcount.';'.$allPrizes.';'.$transsummaryid."; \r\n");
                                                            fclose($fp);
                                                            if($projectid == 2)
                                                            {
                                                                if($swps_cde != "")
                                                                {
                                                                    $denominationdtls = $cvvoucherinfo->SelectDenomination($swps_cde);
                                                                    $denomination = $denominationdtls[0]['Denomination'];
                                                                    $ccpointsconversion->StartTransaction();
                                                                    $arrPointsConversion['TransactionSummaryID'] = $transsummaryid;
                                                                    $arrPointsConversion['ProjectID'] = $projectid;
                                                                    $arrPointsConversion['TerminalID'] = $id;
                                                                    $arrPointsConversion['DateTimeTrans'] = 'now_usec()';
                                                                    $arrPointsConversion['EquivalentCards'] = $convertedpts;
                                                                    $arrPointsConversion['NoOfWinningCards'] = $winningscount;
                                                                    $arrPointsConversion['Winnings'] = $allPrizes;
                                                           
                                                                    if($denomination == 1)
                                                                    {
                                                                        $arrPointsConversion['ConvertedPoints'] = '0.10';
                                                                    }
                                                                    else
                                                                    {
                                                                        $arrPointsConversion['ConvertedPoints'] = $convertedbalance;
                                                                    }
                                                                    $ccpointsconversion->Insert($arrPointsConversion);
                                                                    if($ccpointsconversion->HasError)
                                                                    {
                                                                        $ccpointsconversion->RollBackTransaction();
                                                                        echo "Error has occured: " . $ccpointsconversion->getError();
                                                                    }
                                                                    else
                                                                    {
                                                                        $ccpointsconversion->CommitTransaction();
                                                                        /*insert to logs33*/
                                                                        $filename = "../../DBLogs/logs.txt";
                                                                        $fp = fopen($filename, "a");
                                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO pointsconversioninfo: ".  serialize($arrPointsConversion)."; \r\n");
                                                                        fclose($fp);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    $ccpointsconversion->StartTransaction();
                                                                    $arrPointsConversion09['TransactionSummaryID'] = $transsummaryid;
                                                                    $arrPointsConversion09['ProjectID'] = $projectid;
                                                                    $arrPointsConversion09['TerminalID'] = $id;
                                                                    $arrPointsConversion09['DateTimeTrans'] = "now_usec()";
                                                                    $arrPointsConversion09['EquivalentCards'] = $convertedpts;
                                                                    $arrPointsConversion09['NoOfWinningCards'] = $winningscount;
                                                                    $arrPointsConversion09['Winnings'] = $allPrizes;
                                                                    $arrPointsConversion09['ConvertedPoints'] = $convertedbalance;

                                                                    $truecheck = $ccpointsconversion->Insert($arrPointsConversion09);

                                                                    if($ccpointsconversion->HasError)
                                                                    {
                                                                        $ccpointsconversion->RollBackTransaction();
                                                                        echo "Error has occured: " . $ccpointsconversion->getError();
                                                                    }
                                                                    else
                                                                    {
                                                                        $ccpointsconversion->CommitTransaction();
                                                                
                                                                        /*insert to logs34*/
                                                                        $filename = "../../DBLogs/logs.txt";
                                                                        $fp = fopen($filename, "a");
                                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO pointsconversioninfo: ".  serialize($arrPointsConversion)."; \r\n");
                                                                        fclose($fp);
                                                                    }
                                                                }

                                                                if($swps_cde != "")
                                                                {
                                                                    $cvvoucherinfo->StartTransaction();
                                                                    $cvvoucherinfo->UpdateVoucherInfoByVoucherNumAndStatus(2,$id,$allPrizes,$swps_cde,4);
                                                                    if($cvvoucherinfo->HasError)
                                                                    {
                                                                        $cvvoucherinfo->RollBackTransaction();
                                                                        echo "Error has occured: " . $ccpointsconversion->getError();
                                                                    }
                                                                    else
                                                                    {
                                                                        $cvvoucherinfo->CommitTransaction();
                                                                        /*insert to logs35*/
                                                                        $filename = "../../DBLogs/logs.txt";
                                                                        $fp = fopen($filename, "a");
                                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_voucherinfo: 2;".$id.';'.$allPrizes.';'.$swps_cde.';'."4; \r\n");
                                                                        fclose($fp);
                                                                    }

                                                                    $freeentrydtls = $cfreeentry->SelectByVoucherCode($swps_cde);
                                                                    if(count($freeentrydtls) > 0)
                                                                    {
                                                                        $cfreeentry->StartTransaction();
                                                                        $cfreeentry->UpdateDatePlayed($swps_cde);
                                                                        if($cfreeentry->HasError)
                                                                        {
                                                                            $cfreeentry->RollBackTransaction();
                                                                            echo "Error has occured: " . $ccpointsconversion->getError();
                                                                        }
                                                                        else
                                                                        {
                                                                            $cfreeentry->CommitTransaction();
                                                                            /*insert to logs36*/
                                                                            $filename = "../../DBLogs/logs.txt";
                                                                            $fp = fopen($filename, "a");
                                                                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_freeentry: ".$swps_cde."; \r\n");
                                                                            fclose($fp);
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        $freeentryregistrants = $cfreeentryregistrants->SelectVoucherCode($swps_cde);
                                                                        if(count($freeentryregistrants) > 0)
                                                                        {
                                                                            $cfreeentryregistrants->StartTransaction();
                                                                            $cfreeentryregistrants->UpdateDatePlayedByVoucherCode($swps_cde);
                                                                            if($cfreeentryregistrants->HasError)
                                                                            {
                                                                                $cfreeentryregistrants->RollBackTransaction();
                                                                                echo "Error has occured: " . $cfreeentryregistrants->getError();
                                                                            }
                                                                            else
                                                                            {
                                                                                $cfreeentryregistrants->CommitTransaction();
                                                                                /*insert to logs37*/
                                                                                $filename = "../../DBLogs/logs.txt";
                                                                                $fp = fopen($filename, "a");
                                                                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_freeentryregistrants: ".$swps_cde."; \r\n");
                                                                                fclose($fp);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        $tempdeckdata = $cctempdeck->SelectTempDeckDetailsByTransSummID($transsummaryid);
                                                        $cctempdeck->StartTransaction();
                                                        $cctempdeck->DeleteByTransSummID($transsummaryid);
                                                        if($cctempdeck->HasError)
                                                        {
                                                            $cctempdeck->RollBackTransaction();
                                                            echo "Error has occured: " . $cctempdeck->getError();
                                                        }
                                                        else
                                                        {
                                                            $cctempdeck->CommitTransaction();
                                                            /*insert to logs38*/
                                                            $filename = "../../DBLogs/logs.txt";
                                                            $fp = fopen($filename, "a");
                                                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || DELETE FROM temporarydecktable: ".$transsummaryid."; \r\n");
                                                            fclose($fp);
                                                        }

                                                        $cctempprizelist->StartTransaction();
                                                        $cctempprizelist->DeleteByTransSummaryID($transsummaryid);
                                                        if($cctempprizelist->HasError)
                                                        {
                                                            $cctempprizelist->RollBackTransaction();
                                                            echo "Error has occured: " . $cctempprizelist->getError();
                                                        }
                                                        else
                                                        {
                                                            $cctempprizelist->CommitTransaction();
                                                            /*insert to logs39*/
                                                                $filename = "../../DBLogs/logs.txt";
                                                                $fp = fopen($filename, "a");
                                                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || DELETE FROM tempprizelist: ".$transsummaryid."; \r\n");
                                                                fclose($fp);
                                                        }
                                                    }
                                                }
                                            }
                                        //}
                                    }
                                }
                                
                                // $terminalsessionid = $_SESSION['tsi'];
                                $terminalsessionid = $sessiondtls[0]["ID"];
                                $maxiddtls = $cterminalsessiondtls->SelectMaxId();
                                $lastTransId = $maxiddtls[0]['max'];
                                $lastTransId = ($lastTransId != null) ? $lastTransId : 0;
                                $lastTransId = ($lastTransId + 1);
                                $transid = 'W' . str_pad($terminalsessionid,6,0,STR_PAD_LEFT) . str_pad($lastTransId,10,0,STR_PAD_LEFT);

                                $cterminalsessiondtls->StartTransaction();
                                $arrTerminalSessionDetails['TerminalSessionID'] = $terminalsessionid;
                                $arrTerminalSessionDetails['TransactionType'] = 'W';
                                $arrTerminalSessionDetails['Amount'] = $convertedbalance;
                                $arrTerminalSessionDetails['AcctID'] = $_SESSION['id'];
                                $arrTerminalSessionDetails['ServiceID'] = 1;
                                $arrTerminalSessionDetails['TransID'] = $transid;
                                $arrTerminalSessionDetails['ServiceTransactionID'] = $transsummaryid;
                                $arrTerminalSessionDetails['TransDate'] ='now_usec()' ;
                                $arrTerminalSessionDetails['DateCreated'] = 'now_usec()';
                                $cterminalsessiondtls->Insert($arrTerminalSessionDetails);
                                if($cterminalsessiondtls->HasError)
                                {
                                    $cterminalsessiondtls->RollBackTransaction();
                                    echo "Error has occured: " . $cterminalsessiondtls->getError();
                                }
                                else
                                {
                                    $cterminalsessiondtls->CommitTransaction();
                                    /*insert to logs40*/
                                    $filename = "../../DBLogs/logs.txt";
                                    $fp = fopen($filename, "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tbl_terminalsessiondetails: ".  serialize($arrTerminalSessionDetails)."; \r\n");
                                    fclose($fp);
                                }

                                if(isset($_SESSION['IsFreeEntry']))
                                {
                                    $cvvoucherinfo->StartTransaction();
                                    $cvvoucherinfo->UpdateWinnings($wintype,$sweeps_code);
                                    if($cvvoucherinfo->HasError)
                                    {
                                        $cvvoucherinfo->RollBackTransaction();
                                        echo "Error updating voucher info.: " . $cvvoucherinfo->getError();
                                    }
                                    else
                                    {
                                        $cvvoucherinfo->CommitTransaction();
                                        /*insert to logs41*/
                                        $filename = "../../DBLogs/logs.txt";
                                        $fp = fopen($filename, "a");
                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_voucherinfo: ". $wintype.';'.$sweeps_code."; \r\n");
                                        fclose($fp);
                                    }
                                    
                                    unset($_SESSION['IsFreeEntry']);
                                }                                                
                                //end terminal session                                                

                                $cwinningsdump->StartTransaction();
                                $bool = false;
                                for($i = 0 ; $i < count($tempdeckdata) ; $i++)
                                {
                                    if($tempdeckdata[$i]['PrizeDescription'] == "$0 Cash")
                                    {
                                        $prizedesc = "Try Again";
                                    }
                                    else
                                    {
                                        $prizedesc = $tempdeckdata[$i]['PrizeDescription'];
                                    }
                                    $ecnChecker = $cwinningsdump->ECNChecker($tempdeckdata[$i]['ECN']);//check if the ECN already inserted
                                    if(count($ecnChecker) == 0)
                                    {
                                        $arrWinningsDump['FK_TerminalID'] = $id;
                                        $arrWinningsDump['WinType'] = $prizedesc;
                                        $arrWinningsDump['ECN'] = $tempdeckdata[$i]['ECN'];
                                        $arrWinningsDump['IsOpened'] = 'N';
                                        $arrWinningsDump['IsFinalized'] = 0;
                                        $cwinningsdump->Insert($arrWinningsDump);
                                        if($cwinningsdump->HasError)
                                        {
                                            $bool = true;
                                        }
                                    } 
                                    else 
                                    {
                                        $bool = true;
                                    }
                                }

                                if ($bool)
                                {
                                    $cwinningsdump->RollBackTransaction();
                                    $error = "Error has occured: " . $cwinningsdump->getError();
                                }
                                else
                                {

                                    $cwinningsdump->CommitTransaction();
                                    /*insert to logs43*/
                                    $filename = "../../DBLogs/logs.txt";
                                    $fp = fopen($filename, "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tbl_winnings_dump: ".  serialize($arrWinningsDump)."; \r\n");
                                    fclose($fp);
                                }

                                //start terminal session
                                $_SESSION['resultmsg'] = $respondmessage;
                                $processedby = $_SESSION['$processedby'];
                                $confirmsession = $cterminalsession->getID($id);
                                if($confirmsession)
                                {                               
                                    $cterminalsession->StartTransaction();
                                    $cterminalsession->UpdateDateEndPerSiteID($id, $siteid);
                                    if ($cterminalsession->HasError)
                                    {
                                        $cterminalsession->RollBackTransaction();

                                        $convert = "An Error Occured. Please try again.(Error in updating tbl_terminalsessions)";
                                        $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                        $objResponse->script("popup('popUpDivConvert');");
                                        $objResponse->assign("convert", "innerHTML", $convert);
                                        $objResponse->assign("okbtn", "innerHTML", $okbutton);
                                    }
                                    else
                                    {
                                        $cterminalsession->CommitTransaction();

                                        //insert to logs5
                                        $filename = "../../DBLogs/logs.txt";
                                        $fp = fopen($filename, "a");
                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_terminalsessions: ". $id .';'. $siteid . "; \r\n");
                                        fclose($fp);
                                    
                                        $cterminals->StartTransaction();
                                        $cterminals->UpdateTerminalStatus($id);
                                        if ($cterminals->HasError)
                                        {
                                            $cterminals->RollBackTransaction();

                                            $convert = "An Error Occured. Please try again.(Error in updating tbl_terminals)";
                                            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                            $objResponse->script("popup('popUpDivConvert');");
                                            $objResponse->assign("convert", "innerHTML", $convert);
                                            $objResponse->assign("okbtn", "innerHTML", $okbutton);
                                        }
                                        else
                                        {
                                            $cterminals->CommitTransaction();
                                            
                                            //insert to logs6
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp = fopen($filename, "a");
                                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_terminals: ". $id . "; \r\n");
                                            fclose($fp);

                                            $caudittrail->StartTransaction();
                                            $arrAuditTrail["SessionID"] = "";
                                            $arrAuditTrail["AccountID"] = "";
                                            $arrAuditTrail["TransDetails"] = 'Convert points for : ' . $processedby;
                                            $arrAuditTrail["RemoteIP"] = "";
                                            $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                            $caudittrail->Insert($arrAuditTrail);
                                            if ($caudittrail->HasError)
                                            {
                                                $caudittrail->RollBackTransaction();

                                                $convert = "An Error Occured. Please try again.(Error in inserting to tbl_audittrail)";
                                                $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                                $objResponse->script("popup('popUpDivConvert');");
                                                $objResponse->assign("convert", "innerHTML", $convert);
                                                $objResponse->assign("okbtn", "innerHTML", $okbutton);
                                            }
                                            else
                                            {
                                                $caudittrail->CommitTransaction();
                                                
                                                //insert to logs7
                                                $filename = "../../DBLogs/logs.txt";
                                                $fp = fopen($filename, "a");
                                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tbl_audittrail: " . serialize($arrAuditTrail) . "; \r\n");
                                                fclose($fp);
                                           
                                                $cterminals->StartTransaction();
                                                $cterminals->UpdateServID($id, 0);
                                                if ($cterminals->HasError)
                                                {
                                                    $cterminals->RollBackTransaction();
                                                }
                                                else
                                                {
                                                    $cterminals->CommitTransaction();

                                                    /*insert to logs8*/
                                                    $filename = "../../DBLogs/logs.txt";
                                                    $fp = fopen($filename, "a");
                                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_terminals: " . $id . "; \r\n");
                                                    fclose($fp);
                                                        
                                                    //UPDATE LOGS
                                                    $ctranslog->StartTransaction();
                                                    $ctranslog->UpdateTransactionLog($externaltransid, $errormsg, 1, $datestamp);
                                                    if ($ctranslog->HasError)
                                                    {
                                                        $ctranslog->RollBackTransaction();

                                                        $convert = "An Error Occured. Please try again.(Error in updating tbl_transactionlogs)";
                                                        $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                                        $objResponse->script("popup('popUpDivConvert');");
                                                        $objResponse->assign("convert", "innerHTML", $convert);
                                                        $objResponse->assign("okbtn", "innerHTML", $okbutton);
                                                    }
                                                    else
                                                    {
                                                        $ctranslog->CommitTransaction();
                                                    }
                                                    //insert to logs
                                                    $filename = "../../DBLogs/logs.txt";
                                                    $fp = fopen($filename, "a");
                                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_transactionlogs: " . $externaltransid . ';' . $errormsg . ';1;' . $datestamp . "\r\n");
                                                    fclose($fp);
//                                                        $cterminals->StartTransaction();
//                                                        $cterminals->UpdateIsPlayableStatus($id,0);
//                                                        if($cterminals->HasError)
//                                                        {
//                                                            $cterminals->RollBackTransaction();
//                                                        }
//                                                        else
//                                                        {
//                                                                    $cterminals->CommitTransaction();
                                                                    $filename = "../../DBLogs/logs.txt";
                                                                    $fp = fopen($filename, "a");
                                                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_terminals: " .$id."0;" "\r\n");
                                                                    fclose($fp);

                                                                     $objResponse->script("window.location='displaycards.php';");
//                                                        }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                /*END
                                * @author Jerome Jose
                                * @date Feb 8 2013
                                * @purpose to Combining Convert points and getregulargaming
                                */
                                
                                /**
                                 * To unset all session variables after a successful points conversion.
                                 * @author Noel Antonio 04-03-2013
                                 */
                                 unset($_SESSION);
                            }
                        }
                        else
                        {
                            //rollback withdraw API (deposit API amt=$balance)
                            $convert = "An Error Occured. Please try again.(No terminal details.)";
                            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                            $objResponse->script("popup('popUpDivConvert');");
                            $objResponse->assign("convert", "innerHTML", $convert);
                            $objResponse->assign("okbtn", "innerHTML", $okbutton);
                        }
                    }
                    else
                    {
                        $string = serialize($result_withdraw);

                        //insert to logs
                        $filename = "../../APILogs/logs.txt";
                        $fp = fopen($filename, "a");
                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(CONVERT POINTS) || " . $login . " || " . $string . "\r\n");
                        fclose($fp);

                        $ctranslog->StartTransaction();
                        $ctranslog->UpdateTransactionLog($externaltransid, $errormsg, 2, $datestamp);
                        if ($ctranslog->HasError)
                        {
                            $ctranslog->RollBackTransaction();
                        }
                        else
                        {
                            $ctranslog->CommitTransaction();
                        }
                        //insert to logs
                        $filename = "../../DBLogs/logs.txt";
                        $fp = fopen($filename, "a");
                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_transactionlogs\r\n");
                        fclose($fp);

                        $convert = "An Error Occured. Please try again.(Withdrawal API error)";
                        $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                        $objResponse->script("popup('popUpDivConvert');");
                        $objResponse->assign("convert", "innerHTML", $convert);
                        $objResponse->assign("okbtn", "innerHTML", $okbutton);
                    }
                }

        }
        else
        {
            $convert = "No Active Session.";
            $objResponse->script("popup('popUpDivLPCheckActiveSession');");
            $objResponse->assign("msg", "innerHTML", $convert);
            $objResponse->script("hide_loading();");
        }
        
        unset($_SESSION);
    }

    return $objResponse;
}

function GetRegularGamingCards()
{
     $objResponse = new xajaxResponse();
                        App::LoadModuleClass("SweepsCenter", "SCCC_TransactionSummary");
                        App::LoadModuleClass("SweepsCenter", "SCCC_DeckInfo");
                        App::LoadModuleClass("SweepsCenter", "SCCC_TemporaryDeckTable");
                        App::LoadModuleClass("SweepsCenter", "SCCC_Winners");
                        App::LoadModuleClass("SweepsCenter", "SCCC_TempPrizeList");
                        App::LoadModuleClass("SweepsCenter", "SCCC_TempPrizesSummary");
                        App::LoadModuleClass("SweepsCenter", "SCCC_TransactionDetails");
                        App::LoadModuleClass("SweepsCenter", "SCCV_VoucherInfo");
                        App::LoadModuleClass("SweepsCenter", "SCCC_PointsConversionInfo");
                        App::LoadModuleClass("SweepsCenter", "SCC_FreeEntryRegistrants");
                        App::LoadModuleClass("SweepsCenter", "SCC_FreeEntry");
                        App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessionDetails");
                        App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");
                        App::LoadModuleClass("SweepsCenter", "SCC_Terminals");

                        $cctranssumm = new SCCC_TransactionSummary();
                        $ccdeckinfo = new SCCC_DeckInfo();
                        $cctempdeck = new SCCC_TemporaryDeckTable();
                        $ccwinners = new SCCC_Winners();
                        $cctempprizelist = new SCCC_TempPrizeList();
                        $cctempprizesumm = new SCCC_TempPrizesSummary();
                        $cctransdetails = new SCCC_TransactionDetails();
                        $cvvoucherinfo = new SCCV_VoucherInfo();
                        $ccpointsconversion = new SCCC_PointsConversionInfo();
                        $cfreeentryregistrants = new SCC_FreeEntryRegistrants();
                        $cfreeentry = new SCC_FreeEntry();
                        $cterminalsessiondtls = new SCC_TerminalSessionDetails();
                        $cwinningsdump = new SCC_WinningsDump();
                        $cterminals = new SCC_Terminals();
                        
                        $terminalid = $_SESSION['id'];
                        $id = $_SESSION['id'];
                        $login = $_SESSION['user'];
                        $convertedpts = $_SESSION['equivalentpoints'];
                        $convertedbalance = $_SESSION['convertedbalance'];
                        $projectid = 2;
                        $remainingcardcnt2 = 0;
                        if(isset ($_SESSION['sweeps_code']))
                        {
                            $sweeps_code = $_SESSION['sweeps_code'];
                        }
                        else
                        {
                            $sweeps_code = '';
                        }

                        if(isset ($_SESSION['IsFreeEntry']))
                        {
                            $swps_cde = $_SESSION['sweeps_code'];
                            $convertedpts = '1';
                            $convertedbalance = 0.10;
                            $option2 = 2;
                        }
                        else
                        {
                            $swps_cde = "";
                            $option2 = 1;
                        }

                        $cctranssumm->StartTransaction();
                        $arrTransactionSummary['RequestedBy'] = $id;
                        $arrTransactionSummary['TransactionDate'] = 'now_usec()';
                        $arrTransactionSummary['CardCount'] = $convertedpts;
                        $arrTransactionSummary['Status'] = 0;
                        $arrTransactionSummary['Option1'] = $id;
                        $cctranssumm->Insert($arrTransactionSummary);
                        if($cctranssumm->HasError)
                        {
                            $cctranssumm->RollBackTransaction();
                            echo "Error has occured: " . $cctranssumm->getError();
                        }
                        else
                        {
                            $cctranssumm->CommitTransaction();

                             /*insert to logs15*/
                            $filename = "../../DBLogs/logs.txt";
                            $fp = fopen($filename, "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO transactionsummary: " . serialize($arrTransactionSummary) ."; \r\n");
                            fclose($fp);

                            $transsummaryid = $cctranssumm->LastInsertID;

                            $deckdtls = $ccdeckinfo->SelectCurrentDeck($projectid, 1);
                            $deckid = $deckdtls[0]['DeckID'];
                            $decksize = $deckdtls[0]['CardCount'];
                            $usedcardcount = ($deckdtls[0]['UsedCardCount'] != null) ? $deckdtls[0]['UsedCardCount'] : 0;
                            $winningcardcount = ($deckdtls[0]['WinningCardCount'] != null) ? $deckdtls[0]['WinningCardCount'] : 0;
                            $remainingcardcount = ($decksize - $usedcardcount);

                            if($convertedpts >= $remainingcardcount)
                            {
                                $ccdeckinfo->StartTransaction();
                                $ccdeckinfo->UpdateDeckNStatus($deckid,$id,$option2,$transsummaryid,floor($remainingcardcount));
                                if($ccdeckinfo->HasError)
                                {
                                    $ccdeckinfo->RollBackTransaction();
                                    echo "Error has occured: " . $ccdeckinfo->getError();
                                }
                                else
                                {
                                    $ccdeckinfo->CommitTransaction();

                            /*insert to logs16*/
                            $filename = "../../DBLogs/logs.txt";
                            $fp = fopen($filename, "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: ".$deckid.';'.$id.';'.$option2.';'.$transsummaryid.';'.floor($remainingcardcount)."; \r\n");
                            fclose($fp);

                                    $cctempdeck->StartTransaction();
                                    $cctempdeck->InsertSelectedCards($deckid,$transsummaryid);
                                    if($cctempdeck->HasError)
                                    {
                                        $cctempdeck->RollBackTransaction();
                                        echo "Error has occured: " . $cctempdeck->getError();
                                    }
                                    else
                                    {
                                        $cctempdeck->CommitTransaction();

                                            /*insert to logs17*/
                            $filename = "../../DBLogs/logs.txt";
                            $fp = fopen($filename, "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO temporarydecktable: ".$deckid.';'.$transsummaryid."; \r\n");
                            fclose($fp);

                                        $usedcardcnt = ($usedcardcount + $remainingcardcount);
                                        $ccdeckinfo->StartTransaction();
                                        $ccdeckinfo->DeactivateDeck(2,$usedcardcnt, $deckid);
                                        if($ccdeckinfo->HasError)
                                        {
                                            $ccdeckinfo->RollBackTransaction();
                                            echo "Error has occured: " . $ccdeckinfo->getError();
                                        }
                                        else
                                        {
                                            $ccdeckinfo->CommitTransaction();

                                            /*insert to logs18*/
                                                $filename = "../../DBLogs/logs.txt";
                                                $fp = fopen($filename, "a");
                                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO deckinfo: ".$usedcardcnt.';'.$deckid."; \r\n");
                                                fclose($fp);

                                            $deckdtls = $ccdeckinfo->SelectCurrentDeck($projectid, 0);
                                            $deckid = $deckdtls[0]['DeckID'];
                                            $decksize = $deckdtls[0]['CardCount'];
                                            $usedcardcount = ($deckdtls[0]['UsedCardCount'] != null) ? $deckdtls[0]['UsedCardCount'] : 0;
                                            $winningcardcount = ($deckdtls[0]['WinningCardCount'] != null) ? $deckdtls[0]['WinningCardCount'] : 0;

                                            $remainingcardcnt2 = ($convertedpts - $remainingcardcount);

                                            $ccdeckinfo->StartTransaction();
                                            $ccdeckinfo->ActivateQueuedDeck(1,$remainingcardcnt2,$deckid);
                                            if($ccdeckinfo->HasError)
                                            {
                                                $ccdeckinfo->RollBackTransaction();
                                                echo "Error has occured: " . $ccdeckinfo->getError();
                                            }
                                            else
                                            {
                                                $ccdeckinfo->CommitTransaction();

                                                /*insert to logs19*/
                                                $filename = "../../DBLogs/logs.txt";
                                                $fp = fopen($filename, "a");
                                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: ".$remainingcardcnt2.';'.$deckid."; \r\n");
                                                fclose($fp);

                                                $ccdeckinfo->StartTransaction();
                                                $ccdeckinfo->UpdateDeckNStatus($deckid,$id,$option2,$transsummaryid,$remainingcardcnt2);
                                                if($ccdeckinfo->HasError)
                                                {
                                                    $ccdeckinfo->RollBackTransaction();
                                                    echo "Error has occured: " . $ccdeckinfo->getError();
                                                }
                                                else
                                                {
                                                    $ccdeckinfo->CommitTransaction();

                                                /*insert to logs20*/
                                                $filename = "../../DBLogs/logs.txt";
                                                $fp = fopen($filename, "a");
                                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: ".$deckid.';'.$id.';'.$option2.';'.$transsummaryid.';'.$remainingcardcnt2."; \r\n");
                                                fclose($fp);

                                                    $cctempdeck->StartTransaction();
                                                    $cctempdeck->InsertSelectedCards($deckid,$transsummaryid);
                                                    if($cctempdeck->HasError)
                                                    {
                                                        $cctempdeck->RollBackTransaction();
                                                        echo "Error has occured: " . $cctempdeck->getError();
                                                    }
                                                    else
                                                    {
                                                        $cctempdeck->CommitTransaction();
                                                /*insert to logs21*/
                                                $filename = "../../DBLogs/logs.txt";
                                                $fp = fopen($filename, "a");
                                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE temporarydecktable: ".$deckid.';'.$transsummaryid."; \r\n");
                                                fclose($fp);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                $remainingcardcount = $convertedpts;

                                $ccdeckinfo->StartTransaction();
                                $ccdeckinfo->UpdateDeckNStatus($deckid,$id,$option2,$transsummaryid,floor($remainingcardcount));
                                if($ccdeckinfo->HasError)
                                {
                                    $ccdeckinfo->RollBackTransaction();
                                    echo "Error has occured: " . $ccdeckinfo->getError();
                                }
                                else
                                {
                                    $ccdeckinfo->CommitTransaction();

                                    /*insert to logs22*/
                                    $filename = "../../DBLogs/logs.txt";
                                    $fp = fopen($filename, "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deck_".$deckid.": ".$deckid.';'.$id.';'.$option2.';'.$transsummaryid.';'.floor($remainingcardcount)."; \r\n");
                                    fclose($fp);

                                    $cctempdeck->StartTransaction();
                                    $cctempdeck->InsertSelectedCards($deckid,$transsummaryid);
                                    if($cctempdeck->HasError)
                                    {
                                        $cctempdeck->RollBackTransaction();
                                        echo "Error has occured: " . $cctempdeck->getError();
                                    }
                                    else
                                    {
                                        $cctempdeck->CommitTransaction();
                                /*insert to logs23*/
                                $filename = "../../DBLogs/logs.txt";
                                $fp = fopen($filename, "a");
                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO temporarydecktable: ".$deckid.';'.$transsummaryid."; \r\n");
                                fclose($fp);
                                    }
                                }
                            }

                            $cctempdeck->StartTransaction();
                            $cctempdeck->UpdatePrizeDescription($transsummaryid);
                            if($cctempdeck->HasError)
                            {
                                $cctempdeck->RollBackTransaction();
                                echo "Error has occured: " . $cctempdeck->getError();
                            }
                            else
                            {
                                $cctempdeck->CommitTransaction();

                                        /*insert to logs24*/
                                $filename = "../../DBLogs/logs.txt";
                                $fp = fopen($filename, "a");
                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO temporarydecktable: ".$transsummaryid."; \r\n");
                                fclose($fp);

                                $ccwinners->StartTransaction();
                                $ccwinners->InsertDataFromTemporaryDeck($projectid,$deckid,$transsummaryid,$id);
                                if($ccwinners->HasError)
                                {
                                    $ccwinners->RollBackTransaction();
                                    echo "Error has occured: " . $ccwinners->getError();
                                }
                                else
                                {
                                    $ccwinners->CommitTransaction();
                                /*insert to logs25*/
                                $filename = "../../DBLogs/logs.txt";
                                $fp = fopen($filename, "a");
                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO winners: ".$projectid.';'.$deckid.';'.$transsummaryid.';'.$id."; \r\n");
                                fclose($fp);

                                    $totalcashwinningdtls = $cctempdeck->SelectTotalCashWinning($transsummaryid);
                                    $totalcashwinning = $totalcashwinningdtls[0]['TotalCash'];

                                    $winningscountdtls = $cctempdeck->SelectWinningCardCount($transsummaryid);
                                    $winningscount = $winningscountdtls[0]['cardcount'];

                                    if($winningscount > 0)
                                    {
                                        $cctempprizelist->StartTransaction();
                                        $cctempprizelist->InsertDetailsFromTemporaryDeck($transsummaryid);
                                        if($cctempprizelist->HasError)
                                        {
                                            $cctempprizelist->RollBackTransaction();
                                            echo "Error has occured: " . $cctempprizelist->getError();
                                        }
                                        else
                                        {
                                            $cctempprizelist->CommitTransaction();
                                /*insert to logs26*/
                                $filename = "../../DBLogs/logs.txt";
                                $fp = fopen($filename, "a");
                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tempprizelist: ".$transsummaryid."; \r\n");
                                fclose($fp);
                                        }

                                        $cctempprizesumm->StartTransaction();
                                        $cctempprizesumm->InsertRecordsFromTemporaryDeck($transsummaryid);
                                        if($cctempprizesumm->HasError)
                                        {
                                            $cctempprizesumm->RollBackTransaction();
                                            echo "Error has occured: " . $cctempprizelist->getError();
                                        }
                                        else
                                        {
                                            $cctempprizesumm->CommitTransaction();
                                /*insert to logs27*/
                                $filename = "../../DBLogs/logs.txt";
                                $fp = fopen($filename, "a");
                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tempprizesummary: ".$transsummaryid."; \r\n");
                                fclose($fp);
                                        }
                                    }

                                    $cctransdetails->StartTransaction();
                                    $arrTransactionDetails['TransactionSummaryID'] = $transsummaryid;
                                    $arrTransactionDetails['DeckID'] = $deckid;
                                    $arrTransactionDetails['CardCount'] = $convertedpts;
                                    $arrTransactionDetails['TransactionDate'] = 'now_usec()';
                                    $cctransdetails->Insert($arrTransactionDetails);
                                    if($cctransdetails->HasError)
                                    {
                                        $cctransdetails->RollBackTransaction();
                                        echo "Error has occured: " . $cctransdetails->getError();
                                    }
                                    else
                                    {
                                        $cctransdetails->CommitTransaction();
                                                /*insert to logs28*/
                                $filename = "../../DBLogs/logs.txt";
                                $fp = fopen($filename, "a");
                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO transactiondetails: ".  serialize($arrTransactionDetails)."; \r\n");
                                fclose($fp);

                                        $cctranssumm->StartTransaction();
                                        $cctranssumm->UpdateStatus(1,$transsummaryid);
                                        if($cctranssumm->HasError)
                                        {
                                            $cctranssumm->RollBackTransaction();
                                        }
                                        else
                                        {
                                            $cctranssumm->CommitTransaction();
                                /*insert to logs29*/
                                $filename = "../../DBLogs/logs.txt";
                                $fp = fopen($filename, "a");
                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE transactionsummary: ".$transsummaryid."; \r\n");
                                fclose($fp);

                                            $usedcardcount = ($usedcardcount + $convertedpts);
                                            $winningcardcount = ($winningcardcount + $winningscount);

                                            $ccdeckinfo->StartTransaction();
                                            if($remainingcardcnt2 > 0)
                                            {
                                                $ccdeckinfo->UpdateWinningCardCount($winningcardcount,'',$deckid);
                                            }
                                            else
                                            {
                                                $ccdeckinfo->UpdateWinningCardCount($winningcardcount,$usedcardcount,$deckid);
                                            }
                                            if($ccdeckinfo->HasError)
                                            {
                                                $ccdeckinfo->RollBackTransaction();
                                                echo "Error has occured: " . $cctransdetails->getError();
                                            }
                                            else
                                            {
                                                $ccdeckinfo->CommitTransaction();

                                /*insert to logs30*/
                                $filename = "../../DBLogs/logs.txt";
                                $fp = fopen($filename, "a");
                                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: ".$winningcardcount.';'.$deckid.';'.$usedcardcount."; \r\n");
                                fclose($fp);

                                                if($option2 == 1)
                                                {
                                                    if($winningscount > 0)
                                                    {
                                                        $totalcashwinning = number_format($totalcashwinning,2);
                                                        $totalcash = "$" . $totalcashwinning . " Cash";

                                                        if($totalcashwinning > 0)
                                                        {
                                                            $allPrizes = $totalcash;
                                                        }
                                                        else
                                                        {
                                                            $allPrizes = "";
                                                        }

                                                        $prizecountdtls = $cctempprizesumm->SelectPrizeDescCount($transsummaryid);
                                                        $prizecount = $prizecountdtls[0]['prizedesccount'];

                                                        for($ctr3 = 1 ; $ctr3 <= $prizecount ; $ctr3++)
                                                        {
                                                            $tempprizesummdtls = $cctempprizesumm->SelectTempPrizeSummaryDtls($transsummaryid);
                                                            $vchr_Temp = $tempprizesummdtls[0]['PrizeDescription'];
                                                            $int_TempCnt = $tempprizesummdtls[0]['PrizeCount'];
                                                            $int_TempID = $tempprizesummdtls[0]['ID'];
                                                            //$int_Denomination = $tempprizesummdtls[0]['PrizeDenomination'];
                                                            $int_TempPrizeType = $tempprizesummdtls[0]['PrizeType'];

                                                            $noncash = "(" . $int_TempCnt  . ") " . $vchr_Temp;
                                                            $allPrizes .= "," . $noncash;

                                                            $cctempprizesumm->StartTransaction();
                                                            $cctempprizesumm->DeleteRecord($int_TempID);
                                                            if($cctempprizesumm->HasError)
                                                            {
                                                                $cctempprizesumm->RollBackTransaction();
                                                                echo "Error has occured: " . $cctransdetails->getError();
                                                            }
                                                            else
                                                            {
                                                                $cctempprizesumm->CommitTransaction();
                                                                        /*insert to logs31*/
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename, "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || DELETE FROM tempprizesummary: ".$int_TempID."; \r\n");
                                                        fclose($fp);
                                                            }
                                                        }
                                                        $respondmessage = "You've won the following prize/s: " . $allPrizes . ". Your Transaction Reference ID is " .  str_pad($transsummaryid,14,0,STR_PAD_LEFT) . ".  Please proceed to the cashier to claim your winnings.";
                                                    }
                                                    else
                                                    {
                                                        $respondmessage = "You did not get any winning card. Your Transaction Reference ID is " .  str_pad($transsummaryid,14,0,STR_PAD_LEFT) . ".  Thank you for playing.";
                                                    }
                                                }
                                                else
                                                {
                                                    $tempdeckdtls = $cctempdeck->SelectTempDeckDetailsByTransSummID($transsummaryid);
                                                    $allPrizes = $tempdeckdtls[0]['PrizeDescription'];
                                                    $vchr_ECN = $tempdeckdtls[0]['ECN'];
                                                    $int_TempPrizeType = $tempdeckdtls[0]['PrizeType'];
                                                    //$int_TempPrizeValue = $tempdeckdtls[0]['PrizeValue'];

                                                    if($projectid == 2)
                                                    {
                                                        $vchr_ECN = str_pad($transsummaryid,14,0,STR_PAD_LEFT);
                                                    }

                                                    if($winningscount > 0)
                                                    {
                                                        $respondmessage = "You've won " .  $allPrizes . ". Your Transaction Reference ID is " . $vchr_ECN . ".<br />Please proceed to the cashier to claim your winnings.";
                                                    }
                                                    else
                                                    {
                                                        $respondmessage = "This is not a winning card. Your Transaction Reference ID is " . $vchr_ECN . ".<br /> Try our sweepstakes games and increase your chance of winning more and big prizes!<br />Thank you for playing.";
                                                    }
                                                }

                                                $cctranssumm->StartTransaction();
                                                $cctranssumm->UpdateWinnings($winningcardcount,$allPrizes,$transsummaryid);
                                                if($cctranssumm->HasError)
                                                {
                                                    $cctranssumm->RollBackTransaction();
                                                    echo "Error has occured: " . $cctranssumm->getError();
                                                }
                                                else
                                                {
                                                    $cctranssumm->CommitTransaction();

                                                    /*insert to logs32*/
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename, "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE transactionsummary: ".$winningcardcount.';'.$allPrizes.';'.$transsummaryid."; \r\n");
                                                        fclose($fp);

                                                    if($projectid == 2)
                                                    {
                                                        if($swps_cde != "")
                                                        {
                                                            $denominationdtls = $cvvoucherinfo->SelectDenomination($swps_cde);
                                                            $denomination = $denominationdtls[0]['Denomination'];

                                                            $ccpointsconversion->StartTransaction();
                                                            $arrPointsConversion['TransactionSummaryID'] = $transsummaryid;
                                                            $arrPointsConversion['ProjectID'] = $projectid;
                                                            $arrPointsConversion['TerminalID'] = $id;
                                                            $arrPointsConversion['DateTimeTrans'] = 'now_usec()';
                                                            $arrPointsConversion['EquivalentCards'] = $convertedpts;
                                                            $arrPointsConversion['NoOfWinningCards'] = $winningscount;
                                                            $arrPointsConversion['Winnings'] = $allPrizes;
                                                            if($denomination == 1)
                                                            {
                                                                $arrPointsConversion['ConvertedPoints'] = '0.10';
                                                            }
                                                            else
                                                            {
                                                                $arrPointsConversion['ConvertedPoints'] = $convertedbalance;
                                                            }
                                                            $ccpointsconversion->Insert($arrPointsConversion);
                                                            if($ccpointsconversion->HasError)
                                                            {
                                                                $ccpointsconversion->RollBackTransaction();
                                                                echo "Error has occured: " . $ccpointsconversion->getError();
                                                            }
                                                            else
                                                            {
                                                                $ccpointsconversion->CommitTransaction();
                                                        /*insert to logs33*/
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename, "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO pointsconversioninfo: ".  serialize($arrPointsConversion)."; \r\n");
                                                        fclose($fp);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            $ccpointsconversion->StartTransaction();
                                                            $arrPointsConversion['TransactionSummaryID'] = $transsummaryid;
                                                            $arrPointsConversion['ProjectID'] = $projectid;
                                                            $arrPointsConversion['TerminalID'] = $id;
                                                            $arrPointsConversion['DateTimeTrans'] = 'now_usec()';
                                                            $arrPointsConversion['EquivalentCards'] = $convertedpts;
                                                            $arrPointsConversion['NoOfWinningCards'] = $winningscount;
                                                            $arrPointsConversion['Winnings'] = $allPrizes;
                                                            $arrPointsConversion['ConvertedPoints'] = $convertedbalance;
                                                            $ccpointsconversion->Insert($arrPointsConversion);
                                                            if($ccpointsconversion->HasError)
                                                            {
                                                                $ccpointsconversion->RollBackTransaction();
                                                                echo "Error has occured: " . $ccpointsconversion->getError();
                                                            }
                                                            else
                                                            {
                                                                $ccpointsconversion->CommitTransaction();
                                                        /*insert to logs34*/
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename, "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO pointsconversioninfo: ".  serialize($arrPointsConversion)."; \r\n");
                                                        fclose($fp);
                                                            }
                                                        }

                                                        if($swps_cde != "")
                                                        {
                                                            $cvvoucherinfo->StartTransaction();
                                                            $cvvoucherinfo->UpdateVoucherInfoByVoucherNumAndStatus(2,$id,$allPrizes,$swps_cde,4);
                                                            if($cvvoucherinfo->HasError)
                                                            {
                                                                $cvvoucherinfo->RollBackTransaction();
                                                                echo "Error has occured: " . $ccpointsconversion->getError();
                                                            }
                                                            else
                                                            {
                                                                $cvvoucherinfo->CommitTransaction();
                                                    /*insert to logs35*/
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename, "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_voucherinfo: 2;".$id.';'.$allPrizes.';'.$swps_cde.';'."4; \r\n");
                                                        fclose($fp);
                                                            }

                                                            $freeentrydtls = $cfreeentry->SelectByVoucherCode($swps_cde);
                                                            if(count($freeentrydtls) > 0)
                                                            {
                                                                $cfreeentry->StartTransaction();
                                                                $cfreeentry->UpdateDatePlayed($swps_cde);
                                                                if($cfreeentry->HasError)
                                                                {
                                                                    $cfreeentry->RollBackTransaction();
                                                                    echo "Error has occured: " . $ccpointsconversion->getError();
                                                                }
                                                                else
                                                                {
                                                                    $cfreeentry->CommitTransaction();
                                                        /*insert to logs36*/
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename, "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_freeentry: ".$swps_cde."; \r\n");
                                                        fclose($fp);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $freeentryregistrants = $cfreeentryregistrants->SelectVoucherCode($swps_cde);
                                                                if(count($freeentryregistrants) > 0)
                                                                {
                                                                    $cfreeentryregistrants->StartTransaction();
                                                                    $cfreeentryregistrants->UpdateDatePlayedByVoucherCode($swps_cde);
                                                                    if($cfreeentryregistrants->HasError)
                                                                    {
                                                                        $cfreeentryregistrants->RollBackTransaction();
                                                                        echo "Error has occured: " . $cfreeentryregistrants->getError();
                                                                    }
                                                                    else
                                                                    {
                                                                        $cfreeentryregistrants->CommitTransaction();
                                                        /*insert to logs37*/
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename, "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_freeentryregistrants: ".$swps_cde."; \r\n");
                                                        fclose($fp);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                $tempdeckdata = $cctempdeck->SelectTempDeckDetailsByTransSummID($transsummaryid);
                                                $cctempdeck->StartTransaction();
                                                $cctempdeck->DeleteByTransSummID($transsummaryid);
                                                if($cctempdeck->HasError)
                                                {
                                                    $cctempdeck->RollBackTransaction();
                                                    echo "Error has occured: " . $cctempdeck->getError();
                                                }
                                                else
                                                {
                                                    $cctempdeck->CommitTransaction();
                                                        /*insert to logs38*/
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename, "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || DELETE FROM temporarydecktable: ".$transsummaryid."; \r\n");
                                                        fclose($fp);
                                                }

                                                $cctempprizelist->StartTransaction();
                                                $cctempprizelist->DeleteByTransSummaryID($transsummaryid);
                                                if($cctempprizelist->HasError)
                                                {
                                                    $cctempprizelist->RollBackTransaction();
                                                    echo "Error has occured: " . $cctempprizelist->getError();
                                                }
                                                else
                                                {
                                                    $cctempprizelist->CommitTransaction();
                                                      /*insert to logs39*/
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename, "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || DELETE FROM tempprizelist: ".$transsummaryid."; \r\n");
                                                        fclose($fp);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $terminalsessionid = $_SESSION['tsi'];
                        $maxiddtls = $cterminalsessiondtls->SelectMaxId();
                        $lastTransId = $maxiddtls[0]['max'];
                        $lastTransId = ($lastTransId != null) ? $lastTransId : 0;
                        $lastTransId = ($lastTransId + 1);
                        $transid = 'W' . str_pad($terminalsessionid,6,0,STR_PAD_LEFT) . str_pad($lastTransId,10,0,STR_PAD_LEFT);

                        $cterminalsessiondtls->StartTransaction();
                        $arrTerminalSessionDetails['TerminalSessionID'] = $terminalsessionid;
                        $arrTerminalSessionDetails['TransactionType'] = 'W';
                        $arrTerminalSessionDetails['Amount'] = $convertedbalance;
                        $arrTerminalSessionDetails['AcctID'] = $_SESSION['id'];
                        $arrTerminalSessionDetails['ServiceID'] = 1;
                        $arrTerminalSessionDetails['TransID'] = $transid;
                        $arrTerminalSessionDetails['ServiceTransactionID'] = $transsummaryid;
                        $arrTerminalSessionDetails['TransDate'] ='now_usec()' ;
                        $arrTerminalSessionDetails['DateCreated'] = 'now_usec()';
                        $cterminalsessiondtls->Insert($arrTerminalSessionDetails);
                        if($cterminalsessiondtls->HasError)
                        {
                            $cterminalsessiondtls->RollBackTransaction();
                            echo "Error has occured: " . $cterminalsessiondtls->getError();
                        }
                        else
                        {
                            $cterminalsessiondtls->CommitTransaction();
                            /*insert to logs40*/
                          $filename = "../../DBLogs/logs.txt";
                          $fp = fopen($filename, "a");
                          fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tbl_terminalsessiondetails: ".  serialize($arrTerminalSessionDetails)."; \r\n");
                          fclose($fp);
                        }

                        if(isset($_SESSION['IsFreeEntry']))
                        {
                            $cvvoucherinfo->StartTransaction();
                            $cvvoucherinfo->UpdateWinnings($wintype,$sweeps_code);
                            if($cvvoucherinfo->HasError)
                            {
                                $cvvoucherinfo->RollBackTransaction();
                                echo "Error updating voucher info.: " . $cvvoucherinfo->getError();
                            }
                            else
                            {
                                $cvvoucherinfo->CommitTransaction();
                          /*insert to logs41*/
                          $filename = "../../DBLogs/logs.txt";
                          $fp = fopen($filename, "a");
                          fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_voucherinfo: ". $wintype.';'.$sweeps_code."; \r\n");
                          fclose($fp);
                            }

                            unset($_SESSION['IsFreeEntry']);
                        }

                        $cwinningsdump->StartTransaction();
                        $cwinningsdump->DeleteByTerminalID($id);
                        if($cwinningsdump->HasError)
                        {
                            $cwinningsdump->RollBackTransaction();
                            echo "Error has occured: " . $cwinningsdump->getError();
                        }
                        else
                        {
                            $cwinningsdump->CommitTransaction();
                        /*insert to logs42*/
                          $filename = "../../DBLogs/logs.txt";
                          $fp = fopen($filename, "a");
                          fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || DELETE FROM tbl_winnings_dump: ".$id."; \r\n");
                          fclose($fp);
                        }

                        $cwinningsdump->StartTransaction();
                        $bool = false;
                        for($i = 0 ; $i < count($tempdeckdata) ; $i++)
                        {
                            if($tempdeckdata[$i]['PrizeDescription'] == "$0 Cash")
                            {
                                $prizedesc = "Try Again";
                            }
                            else
                            {
                                $prizedesc = $tempdeckdata[$i]['PrizeDescription'];
                            }

                            $arrWinningsDump['FK_TerminalID'] = $id;
                            $arrWinningsDump['WinType'] = $prizedesc;
                            $arrWinningsDump['ECN'] = $tempdeckdata[$i]['ECN'];
                            $arrWinningsDump['IsOpened'] = 'N';
                            $cwinningsdump->Insert($arrWinningsDump);
                            if($cwinningsdump->HasError)
                            {
                                $bool = true;
                            }
                        }

                        if ($bool)
                        {
                            $cwinningsdump->RollBackTransaction();
                            $error = "Error has occured: " . $cwinningsdump->getError();
                        }
                        else
                        {
                            $cwinningsdump->CommitTransaction();
                            /*insert to logs43*/
                          $filename = "../../DBLogs/logs.txt";
                          $fp = fopen($filename, "a");
                          fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tbl_winnings_dump: ".  serialize($arrWinningsDump)."; \r\n");
                          fclose($fp);
                        }

                        //added 01/03/2012 mtcc
                        $cterminals->StartTransaction();
                        $cterminals->UpdateIsPlayableStatus($terminalid,1);
                        if($cterminals->HasError)
                        {
                            $cterminals->RollBackTransaction();
                            $error_title = "ERROR";
                            $error_msg = "Error updating terminal\'s Isplayable status.";
                        }
                        else
                        {
                            $cterminals->CommitTransaction();
                            /*insert to logs44*/
                          $filename = "../../DBLogs/logs.txt";
                          $fp = fopen($filename, "a");
                          fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_terminals: ". $terminalid.";1; \r\n");
                          fclose($fp);
                        }

                        $_SESSION['resultmsg'] = $respondmessage;
                        $objResponse->alert('here at endterminalsession');
                        return $objResponse;
//return true;
}

/*
 * @author Jerome Jose
 * @date Feb 8 2013
 * @purpose to End Terminal Session by clicking Proceed Button
 */

function endterminalsession()
{
    
                                    $objResponse = new xajaxResponse();

                                    App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
                                    App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
                                    App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");

                                    $cterminals = new SCC_Terminals();
                                    $cterminalsession = new SCC_TerminalSessions();
                                    $caudittrail = new SCC_AuditTrail();
$objResponse->alert('here at endterminalsession');
                                    $login = $_SESSION['user'];
                                    $id = $_SESSION['id'];
                                    $siteid = $_SESSION['siteid'];
                                    $arrStr = explode("/", $_SERVER['SCRIPT_NAME']);
                                    $arrStr = array_reverse($arrStr);
                                    $page = $arrStr[0];
                                    $processedby = $_SESSION['$processedby'];
    
                                    $cterminalsession->StartTransaction();

                                    $cterminalsession->UpdateDateEndPerSiteID($id, $siteid);

                                    if ($cterminalsession->HasError)
                                    {
                                        $cterminalsession->RollBackTransaction();

                                        $convert = "An Error Occured. Please try again.(Error in updating tbl_terminalsessions)";
                                        $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                        $objResponse->script("popup('popUpDivConvert');");
                                        $objResponse->assign("convert", "innerHTML", $convert);
                                        $objResponse->assign("okbtn", "innerHTML", $okbutton);
                                    }
                                    else
                                    {
                                        $cterminalsession->CommitTransaction();

                                        //insert to logs5
                                        $filename = "../../DBLogs/logs.txt";
                                        $fp = fopen($filename, "a");
                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_terminalsessions: ". $id .';'. $siteid . "; \r\n");
                                        fclose($fp);
                                    
                                        $cterminals->StartTransaction();
                                        $cterminals->UpdateTerminalStatus($id);
                                        if ($cterminals->HasError)
                                        {
                                            $cterminals->RollBackTransaction();

                                            $convert = "An Error Occured. Please try again.(Error in updating tbl_terminals)";
                                            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                            $objResponse->script("popup('popUpDivConvert');");
                                            $objResponse->assign("convert", "innerHTML", $convert);
                                            $objResponse->assign("okbtn", "innerHTML", $okbutton);
                                        }
                                        else
                                        {
                                            $cterminals->CommitTransaction();
                                            
                                            //insert to logs6
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp = fopen($filename, "a");
                                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_terminals: ". $id . "; \r\n");
                                            fclose($fp);

                                            $caudittrail->StartTransaction();
                                            $arrAuditTrail["SessionID"] = "";
                                            $arrAuditTrail["AccountID"] = "";
                                            $arrAuditTrail["TransDetails"] = 'Convert points for : ' . $processedby;
                                            $arrAuditTrail["RemoteIP"] = "";
                                            $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                            $caudittrail->Insert($arrAuditTrail);
                                            if ($caudittrail->HasError)
                                            {
                                                $caudittrail->RollBackTransaction();

                                                $convert = "An Error Occured. Please try again.(Error in inserting to tbl_audittrail)";
                                                $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                                $objResponse->script("popup('popUpDivConvert');");
                                                $objResponse->assign("convert", "innerHTML", $convert);
                                                $objResponse->assign("okbtn", "innerHTML", $okbutton);
                                            }
                                            else
                                            {
                                                $caudittrail->CommitTransaction();
                                                
                                            //insert to logs7
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp = fopen($filename, "a");
                                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tbl_audittrail: " . serialize($arrAuditTrail) . "; \r\n");
                                            fclose($fp);
                                           
                                                $cterminals->StartTransaction();
                                                $cterminals->UpdateServID($id, 0);
                                                if ($cterminals->HasError)
                                                {
                                                    $cterminals->RollBackTransaction();
                                                }
                                                else
                                                {
                                                    $cterminals->CommitTransaction();

                                                        /*insert to logs8*/
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename, "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_terminals: " . $id . "; \r\n");
                                                        fclose($fp);
                                                        $objResponse->script("window.location='displaycards.php';");
                                                }
                                            }
                                        }
                                    }
      $objResponse->alert('endthesession');
      return $objResponse;
   
}

$xajax->processRequest();
$xajax_js = $xajax->getJavascript("xajax_toolkit");
?>