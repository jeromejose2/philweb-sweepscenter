<?php
/* * *****************
 * Author: Jerome Jose
 * Date Created: 11-26-2012
 * ***************** */
             
$ctr = $_GET['ctr'];
$mode = $_GET['mod'];

require_once('../init.inc.php');

$modulename = 'SweepsCenter';
App::LoadModuleClass($modulename, "SCC_ref_Services");
App::LoadModuleClass($modulename,"SCC_CasinoGames");
$casinogames = new SCC_CasinoGames();
$crefservices = new SCC_ref_Services();
$arrservice = $crefservices->SelectActiveService();
$activeservice = $arrservice[0]["ID"];
    
         $activegamelist = $casinogames->SelectActiveGames($activeservice);
         $newgamelist= array();
         $html = "";
         
         for($xa=0;$xa<count($activegamelist);$xa += 4)
           {  
              $newgamelist[] = array_slice($activegamelist,$xa,4);
           }
           
      if($ctr > 0)
        {
                      // select specific 4 game
                      if($mode == 'up')
                      {
                          $gamelist = $newgamelist[$ctr];
                      }
                      if($mode == 'down')
                      {
                          $gamelist = $newgamelist[$ctr+1];
                      }

        }else
        {
                      if($mode == '0')
                      {
                          $gamelist = $newgamelist[$ctr];
                      }
                      if($mode == '1')
                      {
                          $gamelist = $newgamelist[$ctr+1];
                      }
        }
                $abspath = dirname(__FILE__) . "/";
               
                 //for game title
                 $array = $gamelist[0];
                 $array1 = $gamelist[1];
                 $array2 = $gamelist[2];
                 $array3 = $gamelist[3];
                //image path
                $gamepath =   ($array['ImagePath'] == NULL) ? 'Blank.png' : $array['ImagePath'];
                $gamepath1 =  ($array1['ImagePath'] == NULL) ? 'Blank.png' : $array1['ImagePath'];
                $gamepath2 =  ($array2['ImagePath'] == NULL) ? 'Blank.png' : $array2['ImagePath'];
                $gamepath3 =  ($array3['ImagePath'] == NULL) ? 'Blank.png' : $array3['ImagePath'];
                //game Name
                $gamename =   $array['GameName'];
                $gamename1 =  $array1['GameName'];
                $gamename2 =  $array2['GameName'];
                $gamename3 =  $array3['GameName'];
                //gameTitle and font
                $fontAndTitle = checker($array['GameTitle']);
                $fontAndTitle1 = checker($array1['GameTitle']);
                $fontAndTitle2 = checker($array2['GameTitle']);
                $fontAndTitle3 = checker($array3['GameTitle']);
                $gametitle = $fontAndTitle['forGameTitle'];
                $gametitle1 = $fontAndTitle1['forGameTitle'];
                $gametitle2 = $fontAndTitle2['forGameTitle'];
                $gametitle3 = $fontAndTitle3['forGameTitle'];
                //for font size
                $fontsize = $fontAndTitle['forFontSize'];
                $fontsize1 = $fontAndTitle1['forFontSize'];
                $fontsize2 = $fontAndTitle2['forFontSize'];
                $fontsize3 = $fontAndTitle3['forFontSize'];
                //for pointer
                $class  =  ($gamepath == 'Blank.png' )  ? " " : "horseRacing";
                $class1 =  ($gamepath1 == 'Blank.png' ) ? " " : "horseRacing";
                $class2 =  ($gamepath2 == 'Blank.png' ) ? " " : "horseRacing";
                $class3 =  ($gamepath3 == 'Blank.png' ) ? " " : "horseRacing";
                //onclick
                $onclick  = ($gamepath == 'Blank.png' )   ? " " : "onclick=\"check_session('$gamename')\";";
                $onclick1 = ($gamepath1 == 'Blank.png' )  ? " " : "onclick=\"check_session('$gamename1')\";";
                $onclick2 = ($gamepath2 == 'Blank.png' )  ? " " : "onclick=\"check_session('$gamename2')\";";
                $onclick3 = ($gamepath3 == 'Blank.png' )  ? " " : "onclick=\"check_session('$gamename3')\";";
                
$html .= <<<slider
   <li>
       
                    <div style="margin-left:5px;margin-top:10px;" class="$class" $onclick">
                        <img src="images/$gamepath" height="150px" width="180px"/>
                        <div class="gameTitles" style="$fontsize">$gametitle</div>
                    </div>
                    

                    <div style="margin-left: 195px;margin-top:-187px;" class="$class1" $onclick1">
                        <img src="images/$gamepath1" height="150px" width="180px"/>
                        <div class="gameTitles" style="$fontsize1">$gametitle1</div>
                    </div>
                    
                   <div style="margin-left:395px;margin-top:-187px;" class="$class2" $onclick2">
                        <img src="images/$gamepath2" height="150px" width="180px"/>
                        <div class="gameTitles" style="$fontsize2">$gametitle2</div>
                    </div>
                    
                    <div style="margin-left:595px;margin-top:-187px;" class="$class3" $onclick3">
                        <img src="images/$gamepath3" height="150px" width="180px"/>
                        <div class="gameTitles" style="$fontsize3"><p>$gametitle3</p></div>	
                    </div>
    </li>
slider;




echo $html;

  /** Checker for long game title*/
 function checker($chkr)
                {
                $TEST = $chkr;
                $namearray =  explode(' ', $TEST);
                $arr = array();
                   if(strlen($TEST) > 18 && count($namearray) >= 3)
                   {
                       foreach ($namearray as $key => $value) 
                       {
                           $arr[] = $value;
                       }
                       $font = "font-size: 11.5px;";
                       $TEST = implode(' ', $arr);
                   } 
                   else
                   {
                       $font = NULL;
                       $TEST = $chkr;
                   } 
                   $result = array("forGameTitle" => $TEST, "forFontSize" => $font);
                   return $result;
                }
   /** Checker*/
    
?>

  
