<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 08, 2012
 */
include('../controller/launchpadprocess.php');
include('../controller/displaycardsprocess.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/main.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
<link href="css/lightbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="jscripts/CSSPopUp3.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/trans.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/disable_f5.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/disable_rightclick.js"></script>
<!-- <script language="javascript" type="text/javascript" src="jscripts/quick_pick_button.js"></script> -->
<script src="jscripts/jquery-1.4.1.js" type="text/javascript"></script>
<script src="jscripts/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="http://fbug.googlecode.com/svn/lite/branches/firebug1.4/content/firebug-lite-dev.js"></script>-->
<script type="text/javascript">
        //Using document.ready causes issues with Safari when the page loads
        jQuery(window).load(function(){
                $("#contentContainer").backgroundScale({
                        imageSelector: "#gaBG",
                        centerAlign: true,
                        containerPadding: 0
                });
        });
</script>
<script type="text/javascript">
function heartbeat()
{
    xajax_HeartBeat();
}

setInterval ("heartbeat()", 600000);
</script>
<title>eSweeps</title>
<?php $xajax->printJavascript(); ?>
</head>

<body scroll="no">
<div id="blanket" style="display:none;"></div>
<?php
$user_agent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/MSIE/i', $user_agent))
    {//"Internet Explorer";
?>
    <div id="popUpDivOpenedCards_ie" align="center" style="display:none; width: 1020px; height: 750px; margin-left: 170px; border:solid; border-color:grey; font-family:Helvetica; font-size: 20px;">
<?php    }else
    {//"Non-IE Browser";
?>
    <div id="popUpDivOpenedCards" style="display:none; border:solid; border-color:grey; font-family:Helvetica; font-size: 20px;">
<?php    
    }
?>
<?php
    if (preg_match('/MSIE/i', $user_agent))
    {//"Internet Explorer";
?>
    <div align="center" style=" border-bottom-style: solid; border-color:grey; background-color: black; color: white; height: 40px;"><b><div style="margin-top: 8px; margin-left: -85px; position: absolute;">Opened Cards</div></b></div>
<?php    }else
    {//"Non-IE Browser";
?>
    <div align="center" style=" border-bottom-style: solid; border-color:grey; background-color: black; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 450px;">Opened Cards</div></b></div>
<?php    
    }
?>
<?php
    if (preg_match('/MSIE/i', $user_agent))
    {//"Internet Explorer";
?>
    <div id="popup_container_displaycards_ie">
        <div id="opened_cards" style="margin-left: 30px;"></div>
    </div>
 <?php }else
 {//"Non-IE Browser";  
 ?>
    <div id="popup_container_displaycards">
        <div id="opened_cards" style="margin-left: 40px;"></div>
    </div>
 <?php
 }
 ?>
    <div>
        <div class="quick8" align="center" >
          <div class="sweeps_quick8" onclick="window.location.href='logout.php'" style="visibility: hidden;"></div>
<?php
    if (preg_match('/MSIE/i', $user_agent))
    {//"Internet Explorer";
?>
          <div class="sweeps_back8" align="center" onclick="popup('popUpDivOpenedCards_ie');"></div>
 <?php }else
 {//"Non-IE Browser";  
 ?>
           <div class="sweeps_back8" align="center" onclick="popup('popUpDivOpenedCards');"></div>
 <?php
 }
 ?>
        </div>
    </div>
</div>
<div id="light" class="white_content"><?php include('mechanics.php') ?></div>
<div id="light2" class="white_content"><?php include('terms.php') ?></div>
<div id="fade" class="black_overlay"></div>

<!--  <div id="mainContainer">-->
  <div id="mainContainer" style="height: 100%;">      
    	<div id="banner2">
        	<div id="logo_landing2"> <img src="images/theSweepsLogo.png" alt="" height="130" width="230" /></div>
    	</div>
        
<?php
    $div="";
    
    if (preg_match('/MSIE/i', $user_agent))
    {//"Internet Explorer";
        $div='<div id="contentContainer" align="center" style="height: 100%; width: 100%; top: 132px;">';
    }else
    {//"Non-IE Browser";
        $div='<div id="contentContainer" align="center" style="height: 80%; width: 100%; top: 132px;">';
    }
    
    echo $div;
?>           
      
            <img id="gaBG" src="images/contentbg2.jpg" height="600px" alt="" />
            <div id="sweepsMainContainer">
            	<?php echo $html_content ?>
                <?php echo $resultmsg ?>
                <?php echo $okbtn ?>  
                
            </div>

<!--            <label class="winsumm">
                <?php //echo $resultmsg ?>
            </label>-->
            
            
<!--            <div class="quick7" >
                <div><?php //echo $okbtn ?></div>
            </div>-->

       

    </div>

    </div>

        <div id="footer" align="center">
        	<div id="footerBox">
            	<div class="footerBox_left"></div>
              	<div class="footerBox_body" >
                    <div class="under18"></div>
                    <div class="rules" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
                    <div class="terms" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
              	</div>
                <div class="footerBox_right"></div>
            </div>
         </div>
    </div>
</body>
</html>
