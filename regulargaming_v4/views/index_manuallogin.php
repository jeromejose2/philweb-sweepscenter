<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 08, 2012
 * Updated By: Tere Calderon 
 * Updated On: September 27, 2012
 * Purpose: Incorporated the updated css
 */
include('../controller/index_manualloginprocess.php');

$div="";
$user_agent = $_SERVER['HTTP_USER_AGENT']; 

if (preg_match('/MSIE/i', $user_agent))
{//"Internet Explorer";
    $divErrLogin='<div id="error1" style="display:none; font-family:Helvetica; font-size: 20px;margin-left: 31%;">';
}
else if (preg_match('/Epiphany/i', $user_agent))
{//"Epiphany";
    $divErrLogin='<div id="error1" style="display:none; font-family:Helvetica; font-size: 20px;margin-left: 25%;">';
}
else
{//"Non-IE Browser";
    $divErrLogin='<div id="error1" style="display:none; font-family:Helvetica; font-size: 20px;margin-left: 31%;">';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login : Launch Pad</title>
        <link rel="stylesheet" type="text/css" href="css/login.css" />
        <link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript" src="jscripts/CSSPopUp.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/form_validation.js"></script>
        <script type="text/javascript" src="jscripts/jquery.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                function browserTester(browserString)
                {
                    return navigator.userAgent.toLowerCase().indexOf(browserString) > -1;
                }

                if (browserTester('chrome')) {
                    cssfunctionChrome();
                }
                else if (browserTester('safari')) {
                    cssfunctionSafari();
                }
                else if (browserTester('firefox')) {
                    cssfunctionFirefox();
                }
            });
            function cssfunctionSafari()
            {
                $("#contentContainer").css("margin-left", "-30%");
            }
            function cssfunctionFirefox()
            {

            }
            function cssfunctionChrome()
            {
            }
        </script>
    </head>
    <body>
        <div id="mainContainer">
            <div align="center"><img src="images/login_bg.png" width="100%" height="100%" alt="" id="backgroundimage" />
                <div id="blanket" style="display:none;"></div>
                <div id="popUpDivLoginUname" style="display:none; font-family:Helvetica; font-size: 20px;">
                    <div align="center" style=" border-bottom-style: solid; border-color:#139E9E; background-color: #77A6A0; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 200px;">INVALID LOG IN</div></b></div>
                    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
                        <div id="convert" align="center"></div><div id="convert_img" align="center">Please enter your username.</div><!-- <div id="convert_img" align="center">Please enter a username.</div> -->
                        <div id="okbtn" align="center" style="margin-top: 20px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLoginUname');" style="cursor:pointer;"/></div>
                    </div>
                </div>
                <?php echo $divErrLogin; ?>
                    <div align="center" style=" border-bottom-style: solid; border-color:#139E9E; background-color: #77A6A0; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 200px;">INVALID LOG IN</div></b></div>
                     <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
                        <div id="convert" align="center"></div><div id="convert_img" align="center">You have entered an invalid account information.</div>
                        <div id="okbtn" align="center" style="margin-top: 20px;"><img src="images/OK Button.png" alt="" onclick="popup('error1');" style="cursor:pointer;"/></div>
                    </div>
                </div>
                <div id="popUpDivLoginPass" style="display:none; font-family:Helvetica; font-size: 20px;">
                    <div align="center" style=" border-bottom-style: solid; border-color:#139E9E; background-color: #77A6A0; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 200px;">INVALID LOG IN</div></b></div>
                    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
                        <div id="convert" align="center"></div><div id="convert_img" align="center">Please enter your password.</div><!-- <div id="convert_img" align="center">Please enter a password.</div> -->
                        <div id="okbtn" align="center" style="margin-top: 20px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLoginPass');" style="cursor:pointer;"/></div>
                    </div>
                </div>
                <div id="banner"></div>
                <div id="contentContainer" style="top : 50%;left: 50%;right: 50%;bottom: 50%;" >
                    <div id="Login_Cont" >
                        <div class="login_logo"></div>
                        <div class="login_body">
                            <form method="post" action="index_manuallogin.php" onsubmit="return checkform2(this);">
                                <div class="loginCont_body" >
                                    <div class="txtName">USERNAME</div>
                                    <div class="inputbox">
                                        <?php echo $txtuser; ?>
                                    </div>
                                    <div class="txtName">PASSWORD</div>
                                    <div class="inputbox">
                                        <?php echo $txtpass; ?>
                                    </div>
                                    <div class="loginCont_btn">
                                        <?php echo $btnSubmit; ?>
                                    </div>
                                </div>
                                <div class="login_bottom" style="margin:0 0 0 53px;">&nbsp;</div>

                            </form>

                            <div id="err_msg" align="center">
                                <?php if (isset($error_msg)): ?>
                                    <script>
                document.getElementById('error1').style.display = 'block';
                document.getElementById('blanket').style.display = 'block';
//                popup('error1');
//                form.txtpass.focus();
//                return false;
                                    </script>
                                    <?php // echo $error_msg; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>