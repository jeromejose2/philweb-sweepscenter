<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 20, 2012
 * 
 * Modified By: Noel Antonio
 * Date Modified : October 18, 2012
 */
include('../controller/launchpadprocess.php');
$login = $_SESSION['user'];
$gid = $_GET["gid"];
$mid = $_GET["mid"];
$user = $_GET["user"];
$pass = $_GET["pass"];

if($activeservice == 3)
{
   if(($gid == 'RoyalDerby') || ($gid == 'TombRaiderII') || ($gid == 'WheelofWealth'))
    {
        $src = "https://games.coffeebeansgames.com/aurora/?theme=goldentreecbg&gameid=".$gid."&extGameID=".$gid."&amp;AB=HELP&amp;sEXT1=".$user."&amp;sEXT2=".$pass."";
    }
    else
    {
        $src = "https://games.coffeebeansgames.com/goldentreecbgen/t3start.asp?gameid=".$gid."&amp;AB=HELP&amp;sEXT1=".$user."&amp;sEXT2=".$pass."";
    } 
}elseif($activeservice == 1)
{
    /* Added By NDA 2012-10-18 */
$src = "launchgame.php?user=" . $user . "&pass=" . $pass . "&gid=" . $gid . "&mid=" . $mid . "";

}



?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" type="text/css" href="css/launchpad.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
        <link href="css/lightbox.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript" src="jscripts/trans.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/CSSPopUp.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/lightbox.js"></script>
        <?php $xajax->printJavascript(); ?>
        <script type="text/javascript">
        
            var src = '<?php echo $src; ?>';
            function heartbeat()
            {
                xajax_HeartBeat();
            }

            function logoutterminal()
            {
                terminal_logout("<?php echo $src; ?>");
            }
           var activeservice = '<?php echo $activeservice ?>';
           
            if(activeservice == 1)
            {
                logoutterminal();
            }

            setInterval ("heartbeat()", 600000);

        </script>

        <title>Guam Sweepstakes Games</title>
        
    </head>
    <!--<body style="background-color: black;">-->    
    <body style="background-color: black;" scroll="no">
        <div id="blanket" style="display:none;"></div>
        <div id="popUpDivConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
            <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 180px;">CONVERT POINTS</div></b></div>
            <div id="popup_container_home">
                <div id="convert" align="center"></div><div id="convert_img" align="center" style="visibility:hidden;"><img src="images/load_bal.gif" height="20px" /></div>
                <div id="okbtn" align="center" style="margin-top: 0px;"></div><div id="okbtn_img" align="center" style="margin-top: -20px; visibility:hidden;">PROCESSING</div>
            </div>
        </div>
        <div id="popUpDivLPConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
            <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 100px;">CONVERT POINTS CONFIRMATION</div></b></div>
            <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
                <div id="convert" align="center"></div><div id="convert_img" align="center"><p>Are you sure you want to convert your <label id="convbal"></label></p><br/> <p>point/s and end your gaming session now?</p></div>
                <div id="okbtn" align="center" style="margin-top: 40px; margin-left: 80px; float: left;"><img src="images/OK Button.png" alt="" onclick="convert_points(); popup('popUpDivLPConvert');" style="cursor:pointer;"/></div><div style="margin-top: 35px;"><img src="images/cancelbutton.png" alt="" onclick="popup('popUpDivLPConvert');" style="cursor:pointer;"/></div>
            </div>
        </div>

        
        <div id="light3" class="white_content2"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>
        <div id="fade" class="black_overlay"></div>

        <div style="float: left; width: 70%;">
            <img src="images/back_hover.png" height="37px" width="120px" style="cursor: pointer;" onclick="window.location.href='lobby.php'" />
            <label style="color: white; margin-left: 40%;">
                <?php
                   $login = $_SESSION['user'];
                   $new_string = ereg_replace("[^0-9]", "", $login);

                   echo "Logged In As: Terminal ".$new_string;
                ?>
            </label>
        </div>

        <div align="right">
            <!--<div class="back_launchPad2" onclick="window.location.href='launchpad.php'"></div>-->
        </div>

        <div id="flashframe2"></div>
        <iframe  src="<?php echo $src;?>" id="flashframe" width="100%" height="93%" scrolling="no" frameBorder="0" style="border:none;" align="left"></iframe> 
        <!--<iframe  src="<?php echo $src;?>" id="flashframe" width="99%" height="93%" scrolling="no" style="border:none;" ></iframe>--> 
    </body>
</html>