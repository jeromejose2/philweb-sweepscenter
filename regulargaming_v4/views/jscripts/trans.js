function toggleBalanceLoading(display)
{
    loadingelem = document.getElementById('load_bal_img');
    balanceelem = document.getElementById('balance');

    if (display == 'load')
    {
        loadingelem.style.display = '';
        balanceelem.style.display = 'none';
    }
    else
    {
        loadingelem.style.display = 'none';
        balanceelem.style.display = '';
    }
}

function do_getbalance()
{
    toggleBalanceLoading('load');
    show_loading();
    xajax_GetBalance();
    xajax_CheckCardStatus();
    xajax_checkPendingtrans();
    xajax_updateDbBalance();
}
function do_getbalanceonly()
{
    show_loading();
    xajax_GetBalance();
}
function do_CheckCard()
{
    xajax_GetBalance();
    show_loading();
    xajax_CheckCardStatus();
    setInterval ("do_getbalanceonly()", 600000);
}


function check_session(gid)
{
    xajax_CheckSession(gid);
}



function browse_internet()
{
    show_loading();
    xajax_BrowseInternet();
}

function check_browsing_time()
{
    xajax_CheckBrowsingTime();
}

function reveal_cards(i)
{
    document.getElementById("."+i+".").style.backgroundImage = "";
    document.getElementById("."+i+".").onclick = "";
    xajax_RevealCards(i);
}

function display_loading_img()
{
    document.getElementById("convert_img").style.visibility = "visible";
    document.getElementById("convert").style.visibility = "hidden";
    document.getElementById("okbtn").style.visibility = "hidden";
    document.getElementById("okbtn_img").style.visibility = "visible";
}

function view_opened_cards()
{
    xajax_ViewOpenedCards();
}

function enter_sweeps_code()
{
    show_loading();
    xajax_EnterSweepsCode(document.getElementById("txtVoucher").value);
}

function terminal_logout(src)
{
    xajax_TerminalLogout(src);
}
function GetRegularGamingCards()
{
    xajax_GetRegularGamingCards();
}
function endterminalsession1()
{
    xajax_endterminalsession();
}
function convert_points()
{
    show_loading();
    xajax_ConvertPoints();
}
function confirmpoints1() 
{
    setTimeout("xajax_confrmConvertPoints()",1000);
}
setInterval ("do_getbalance()", 600000);
