<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 18, 2012
 * 
 * Modified By: Noel Antonio
 * Date Modified: October 11, 2012
 */

include('../controller/launchpadprocess.php');
include('../controller/displaycardsquickpickprocess.php');
error_reporting(E_ALL);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
        <link href="css/lightbox.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript" src="jscripts/CSSPopUp3.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/trans.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/disable_f5.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/disable_rightclick.js"></script>
        <!-- <script language="javascript" type="text/javascript" src="jscripts/quick_pick_button.js"></script> -->
        <script src="jscripts/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="jscripts/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
        <script type="text/javascript">
                //Using document.ready causes issues with Safari when the page loads
                jQuery(window).load(function(){
                        $("#contentContainer").backgroundScale({
                                imageSelector: "#gaBG",
                                centerAlign: true,
                                containerPadding: 0
                        });
                });
        </script>
        <script type="text/javascript">
        function ChangePage(pagenum)
        {
            selectedindex = document.getElementById("pgSelectedPage");
            selectedindex.value = pagenum;
            document.forms[0].submit();
        }
            
        function heartbeat()
        {
            xajax_HeartBeat();
        }

        setInterval ("heartbeat()", 600000);
        //setInterval ("heartbeat()", 100000);
        
        
        </script>
        <title>eSweeps</title>
        <?php $xajax->printJavascript(); ?>
    </head>

    <body>
        <div id="blanket" style="display:none;"></div>
        <div id="popUpDivOpenedCards" style="display:none; border:solid; border-color:grey; font-family:Helvetica; font-size: 20px;">
            <div id="popup_container_displaycards">
                <div id="opened_cards"></div>
            </div>
            <div>
                <div class="quick8" >
                    <div class="sweeps_quick8"  ></div>
                    <div class="sweeps_back8" onclick="popup('popUpDivOpenedCards');"></div>
                </div>
            </div>
        </div>

        <div id="light" class="white_content"><?php include('mechanics.php') ?></div>
        <div id="light2" class="white_content"><?php include('terms.php') ?></div>

        <div id="mainContainer" >
            <div id="banner">
                <div id="logo_landing2"> <img src="images/theSweepsLogo.png" alt="" height="150" width="250" /></div>
            </div>
            <form method="post" name="frmAcctList">
            <div id="contentContainer" style="height: 75%; top: 170px;">  
                <img id="gaBG" src="images/contentbg2.jpg" height="577px" alt="" />          
                <div id="sweepsMainContainer"><?php echo $html_content ?></div>
                <br><br><br><br><br><br><br><br><br><br><br>
                
                <p class="winsumm123"><?php echo $resultmsg ?></p>    
                <br><br><br><br>
<!--                <p class="quick123" ><?php echo $okbtn ?></p>-->
<?php echo $okbtn ?>
            </div>
            </form>
            <div id="footer">
                <div id="footerBox">
                    <div class="footerBox_left"></div>
                    <div class="footerBox_body">
                        <div class="under18"></div>
                        <div class="rules" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
                        <div class="terms" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
                    </div>
                    <div class="footerBox_right"></div>
                </div>
            </div>
        </div>
    </body>
</html>
