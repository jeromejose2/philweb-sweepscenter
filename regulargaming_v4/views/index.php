<?php
/* 
 * Created By: Arlene R. Salazar
 * Created On: June 08, 2012
 * Updated By: Tere Calderon 
 * Updated On: September 27, 2012
 * Purpose: Incorporated the updated css
 */
include('../controller/loginprocess.php');
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login : Launch Pad</title>   
        <link rel="stylesheet" type="text/css" href="css/login.css" /> 
        <style>
            .txtName2{
                font: bold 20px/25px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
                color: #FFF;
                text-transform: uppercase;
                width:150px;
                height:auto;
                margin:10px 0 0 15px;
                float:left;
                width: 100%;
                text-align: center;
            }
        </style>
    </head>
    <body scroll="no">
        <div id="mainContainer">
        <div align="center"><img src="images/login_bg.png" width="100%" height="100%" alt="" id="backgroundimage" /> </div> 
            <div id="contentContainer">
                <div id="Login_Cont">
                    <div class="login_logo"></div>
                        <div class="loginCont_body">
                             <div class="txtName2">CHOOSE LOGIN TYPE:</div>
                             <div style="width: 100%; text-align: center;">
                                   <img src="images/autologin.png" width="150" height="40" style="margin-top: 5px; cursor: pointer;" onclick="window.location.href='index_autologin.php'" />
                                   <img src="images/manuallogin.png" width="150" height="40" style="margin-top: 5px; cursor: pointer;" onclick="window.location.href='index_manuallogin.php'" />
                             </div>
                             <div style="width: 100%; text-align: center;">
                                  <img src="images/registerred.png" width="180" height="40" style="margin-top: 10px; cursor: pointer;" onclick="window.location.href='index_register.php'" />
                             </div>
                        </div>
                        <div class="login_bottom">&nbsp;</div>
                </div>
            </div>
        </div>
    </body>
</html>