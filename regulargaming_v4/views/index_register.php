<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 08, 2012
 * Updated By: Tere Calderon 
 * Updated On: September 27, 2012
 * Purpose: Incorporated the updated css
 */
include('../controller/index_registerprocess.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login : Launch Pad</title>
        <link rel="stylesheet" type="text/css" href="css/login.css" />
        <link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
        <link href="css/lightbox.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript" src="jscripts/CSSPopUp.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/form_validation.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery-1.7.1.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/lightbox.js"></script>
        <script language="javascript">
        $(document).ready(function(){
            $('#btnLogin').click(function(){
                    if(checkform_reg(document.getElementById('frmLogin')))
                    {
                            if (navigator.appVersion.indexOf("Win")!=-1) OSName="2";
                            if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
                            if (navigator.appVersion.indexOf("X11")!=-1 || navigator.appVersion.indexOf("Linux")!=-1) OSName="1";
                            if(OSName == '1')
                            {
                                get_mac_address(OSName);
                            }
                            else // WINDOWS
                            {
                                    try {
                                        var locator = new ActiveXObject ("WbemScripting.SWbemLocator");
                                    } catch(e) {
                                        var msg2 = "Please Enable ActiveX on Internet Explorer.";
                                        $("#err_msg").html(msg2);
                                    }
                                    var service = locator.ConnectServer(".");
                                    var properties = service.ExecQuery("SELECT * FROM Win32_Processor");
                                    var e = new Enumerator (properties);   
                                    properties = service.ExecQuery("SELECT * FROM Win32_NetworkAdapterConfiguration");
                                    e = new Enumerator (properties);
                                    for (;!e.atEnd();e.moveNext ()) {
                                        p = e.item (); 
                                        if(p.MACAddress) {
                                            var macid = p.MACAddress;
                                            break;
                                        }   
                                    }
                                    get_mac_addressforWindows(macid,OSName);
                            }
                    }
            });
        })

        function get_mac_address(OSName)
        {
                show_loading();
                macs.getMacAddress();
                var macadd2 = document.macaddressapplet.getMacAddress();
                var macadd = trim11(macadd2);
                var usr = document.getElementById("txtuser").value;
                var pwd = document.getElementById("txtpass").value;
                $.ajax({
                    url: '../controller/register_terminalajax.php?macadd='+macadd+'&usr='+usr+'&pwd='+pwd+'&os='+OSName,
                    type : 'post',
                    success : function(data)
                    {
                            var json = jQuery.parseJSON(data);
                            hide_loading();
                            $("#err_msg").html(json.msg1);
                            if(json.result == '1')
                            {
                                    $('#btnLogin').removeClass('login2').addClass('proceed');
                                    $('#btnLogin').attr('onclick', 'window.location="index.php"');
                            }
                    },
                    error: function(e)
                    {
//                        alert(e);
                    }
                });
        }
        
        function get_mac_addressforWindows(macadd2,OSName)
        {
                show_loading();
                var usr = document.getElementById("txtuser").value;
                var pwd = document.getElementById("txtpass").value;
                var macadd = trim11(macadd2);
                $.ajax({
                    url: '../controller/register_terminalajax.php?macadd='+macadd+'&usr='+usr+'&pwd='+pwd+'&os='+OSName,
                    type : 'post',
                    success : function(data)
                    {
                        var json = jQuery.parseJSON(data);
                        hide_loading();
                        $("#err_msg").html(json.msg1);
                        if(json.result == '1')
                        {
                            $('#btnLogin').removeClass('login2').addClass('proceed');
                            $('#btnLogin').attr('onclick', 'window.location="index.php"');
                        }
                    },
                    error: function(e)
                    {
//                        alert(e.responseText);
                    }
                });
        }
        
        function checkform_reg ( form )
        {
            if (form.txtuser.value == "")
            {
                popup('popUpDivLoginUname');
                form.txtuser.focus();
                return false ;
            }
            else
            {
                return true;
            }
        }
        function trim11 (str) 
            {
                str = str.replace(/^\s+/, '');
                for (var i = str.length - 1; i >= 0; i--) {
                    if (/\S/.test(str.charAt(i))) {
                        str = str.substring(0, i + 1);
                        break;
                    }
                }
                return str;
            }
        </script>
        <style>
            .txtName2{
                font: bold 20px/25px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
                color: #FFF;
                text-transform: uppercase;
                width:150px;
                height:auto;
                margin:10px 0 0 15px;
                float:left;
                width: 100%;
                text-align: center;
            }
            .login2 {
                background:url(images/registerred.png);
                width:210px;
                height:55px;
                cursor: pointer;
                margin-bottom: 10px;
            }
            .proceed {
                background:url(images/ProceedButton.png);
                width:146px;
                height:47px;
                cursor: pointer;
                margin-top: 10px;
            }
        </style>
    </head>

    <body>
        <div id="light3" class="white_content2">
            <div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div>
        </div>
        <div id="fade" class="black_overlay"></div>
        <div id="mainContainer">
            <div align="center"><img src="images/login_bg.png" width="100%" height="100%" alt="" id="backgroundimage" />
                <div id="blanket" style="display:none;"></div>
                <div id="popUpDivLoginUname" style="display:none; font-family:Helvetica; font-size: 20px;">
                    <div align="center" style=" border-bottom-style: solid; border-color:#139E9E; background-color: #77A6A0; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 200px;">INVALID LOG IN</div></b></div>
                    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
                        <div id="convert" align="center"></div><div id="convert_img" align="center">Please enter a username.</div>
                        <div id="okbtn" align="center" style="margin-top: 20px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLoginUname');" style="cursor:pointer;"/></div>
                    </div>
                </div>
                <div id="banner"></div>
                <div id="contentContainer">
                    <div id="Login_Cont">
                        <div class="login_logo"></div>
                        <div class="login_body">
                            <form id="frmLogin" method="post" action="">
                            <div class="loginCont_body">
                                <div class="txtName">USERNAME</div>
                                <div class="inputbox">
                                     <?php echo $txtuser;?>
                                 </div>
                                <div class="txtName">PASSWORD</div>
                                 <div class="inputbox">
                                     <?php echo $txtpass;?>
                                 </div>
                                 <div class="loginCont_btn" style="width: 100%;  text-align: center;">
                                     <?php echo $btnLogin;?>
                                </div>
                            </div>
                            </form>
                             <div class="login_bottom" style="margin:0 0 0 53px;">&nbsp;</div>
                            <div id="err_msg" align="center" style="margin-left:50px;width:350px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--[if !IE]> Firefox and others will use outer object -->
        <embed type="application/x-java-applet"
               name="macaddressapplet"
               width="0"
               height="0"
               code="MacAddressApplet.class"
               archive="SMacAddressApplet.jar"
               pluginspage="http://java.sun.com/javase/downloads/index.jsp"
               style="position:absolute; top:-1000px; left:-1000px;">
            <noembed>
            <!--<![endif]-->
                <!---->
                <object classid="clsid:CAFEEFAC-0016-0000-FFFF-ABCDEFFEDCBA"
                        type="application/x-java-applet"
                        name="macaddressapplet"
                        style="position:absolute; top:-1000px; left:-1000px;"
                        >
                    <param name="code" value="MacAddressApplet.class">
                    <param name="archive" value="SMacAddressApplet.jar" >
                    <param name="mayscript" value="true">
                    <param name="scriptable" value="true">
                    <param name="width" value="0">
                    <param name="height" value="0">
                  </object>
            <!--[if !IE]> Firefox and others will use outer object -->
            </noembed>
        </embed>
        <!--<![endif]-->

        <script type="text/javascript">
        var macs = {
            getMacAddress : function()
            {
                document.macaddressapplet.setSep( "-" );
                //alert( "Mac Address = " + document.macaddressapplet.getMacAddress() );
            },
            getCPUProduct : function() {
                alert( "CPU Product = " + document.macaddressapplet.getProduct() );
            },
            getCPUVendor : function() {
                alert("CPU Vendor"+ document.macaddressapplet.getVendor());
            },
            getCPUVersion : function() {
                alert("CPU Version"+ document.macaddressapplet.getVersion());
            },
            getCPUSerial : function() {
                alert("CPU Serial"+ document.macaddressapplet.getSerial());
            },
            getMacAddressesJSON : function()
            {
                document.macaddressapplet.setSep( ":" );
                document.macaddressapplet.setFormat( "%02x" );
                var macs = eval( String( document.macaddressapplet.getMacAddressesJSON() ) );
                var mac_string = "";
                for( var idx = 0; idx < macs.length; idx ++ )
                    mac_string += "\t" + macs[ idx ] + "\n ";

                alert( "Mac Addresses = \n" + mac_string );
            }
        }
        </script>
    </body>
</html>