<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 20, 2012
 * Updated By: Tere Calderon 
 * Updated On: September 28, 2012
 * Purpose: Incorporated the updated css
 */
include('../controller/launchpadprocess.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Sweeps Entry Code </title>
        <link rel="stylesheet" type="text/css" href="css/launchpad.css" />
        <link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
        <link href="css/lightbox.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript" src="jscripts/CSSPopUp.js"></script>
        <script src="jscripts/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="jscripts/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
        <script type="text/javascript" language="javascript" src="jscripts/jquery-fieldselection.js"></script>
        <script type="text/javascript" language="javascript" src="jscripts/jquery-ui-personalized-1.5.2.min.js"></script>
        <script type="text/javascript" src="jscripts/cskeyboard.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/trans.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/lightbox.js"></script>
        <script type="text/javascript">
            //Using document.ready causes issues with Safari when the page loads
            jQuery(window).load(function(){
                    $("#contentContainer").backgroundScale({
                            imageSelector: "#gaBG",
                            centerAlign: true,
                            containerPadding: 0
                    });
            });
        </script>
            <script type="text/javascript">
        var clickCtr = 0;
          function ClearTextboxes()
        {
            document.getElementById('txtVoucher').value = '';
        }

        function keycount()
	 {
	     var txt = document.getElementById('txtVoucher').value;
            
             clickCtr = clickCtr + 1;
//             alert(''+clickCtr+'');
            if(clickCtr > 12 && txt.length > 11)
                { 
                    if(txt.length >= 12)
                   {
                   document.getElementById('fade').style.display='block';
                   document.getElementById('light30').style.display='block';
                   document.getElementById('hiddenvoucher').value = txt;
                   return false;
                   }else
                       {
                           return true;
                       }
                }
                else{
                    return true;
                }
        }
       function backvoucher()
        {
            var txt1 = document.getElementById('txtVoucher').value;
            var oldtxt = document.getElementById('hiddenvoucher').value;
            if(txt1.length > 12 || txt1.length == 12)
                    {
                       if(document.getElementById('txtVoucher').value.length >= 12)      
                        {    
                            document.getElementById('txtVoucher').value = oldtxt;
                        }
                    }
        
        }  
       
        </script>
        <?php $xajax->printJavascript(); ?>
    </head>
    <body>
        <div id="blanket" style="display:none;"></div>
        <div id="popUpDivEntryCode" style="display:none; font-family:Helvetica; font-size: 20px;">
            <div align="center" style=" border-bottom-style: solid; border-color:#139E9E; background-color: #77A6A0; color: white; height: 40px;"><b><div id="title" style="margin-top: 8px; position: absolute; margin-left: 180px;"></div></b></div>
            <div style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
                <div align="center"></div><div id="msg" align="center"></div>
                <div id="okbtn" align="center" style="margin-top: 20px;"></div>
            </div>
        </div>

        <div id="light" class="white_content"><?php include('mechanics.php') ?></div>
        <div id="light2" class="white_content"><?php include('terms.php') ?></div>
        <div id="light3" class="white_content2"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>
        
 <div id="light30" style="text-align: center;font-size: 16pt;height: 200px" class="white_content2">
   <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px;">
   <b id="a">Error</b></div>
   <p id="b"><br/>Voucher number must not exceed 12 characters </p><BR />
      <br/>  <input type="button" class="inputBoxEffectPopup1" onclick ="document.getElementById('light30').style.display='none';document.getElementById('fade').style.display='none';return backvoucher();"/>
</div> 
        <div id="fade" class="black_overlay"></div>

        <div id="mainContainer">
	<div id="bg"><img src="images/contentbg.jpg" width="100%" height="100%" alt="" /></div>
            <div id="banner">
                <!--<div id="logo_landing"> <img src="images/theSweepsLogo.png" /></div>-->
            <img src="images/theSweepsLogo.png" alt=""  style="position:absolute; left:50%; margin-left:-200px;" /></td>
            </div>
            <div id="contentContainer">
                <!--<img id="gaBG" src="images/contentbg.jpg" height="458px" />-->
                <div id="codeContainer">
                    <div class="codeP"> Please enter your Sweeps Code in the box below.</div>
                    <div class="inBox">
                        <div class="inputBox_left"></div>
                        <div class="inputBox_body"><input type="text" id="txtVoucher" name="txtVoucher" class="inputbox_code" maxlength="12" /></div>
                        <div class="inputBox_right"></div>
                    </div>
                </div>
 <?php echo $hiddenvoucher;?>
                <div id="codeContainer2">
                    <div id="keyboard" >
                        <div id="row">
                        <input class="button" name="1" type="button" value="1" size="" onclick="javascript: return keycount();" />
                        <input class="button2" name="2" type="button" value="2" size="" onclick="javascript: return keycount();" />
                        <input class="button2" name="3" type="button" value="3" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="4" type="button" value="4" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="5" type="button" value="5" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="6" type="button" value="6" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="7" type="button" value="7" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="8" type="button" value="8" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="9" type="button" value="9" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="0" type="button" value="0" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="backspace" type="button" value="Backspace" style="width: 100px" />
                    </div>
                         <div id="row1">
                        <input class="button" name="q" type="button" value="q" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="w" type="button" value="w" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="e" type="button" value="e" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="r" type="button" value="r" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="t" type="button" value="t" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="y" type="button" value="y" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="u" type="button" value="u" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="i" type="button" value="i" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="o" type="button" value="o" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="p" type="button" value="p" size="" onclick="javascript: return keycount();"/>
                      <input class="button2" name="shift" type="button" value="Shift" id="shift2" style="width: 100px" />
                    </div>
                         <div id="row1_shift">
                        <input class="button" name="Q" type="button" value="Q" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="W" type="button" value="W" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="E" type="button" value="E" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="R" type="button" value="R" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="T" type="button" value="T" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="Y" type="button" value="Y" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="U" type="button" value="U" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="I" type="button" value="I" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="O" type="button" value="O" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="P" type="button" value="P" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="shift" type="button" value="Shift" id="shift2" style="width: 100px" />

                    </div>
                      <div id="row2">
                        <input class="button" name="a" type="button" value="a" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="s" type="button" value="s" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="d" type="button" value="d" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="f" type="button" value="f" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="g" type="button" value="g" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="h" type="button" value="h" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="j" type="button" value="j" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="k" type="button" value="k" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="l" type="button" value="l" size="" onclick="javascript: return keycount();"/>
                        <input class="button2"  type="button" style="visibility: hidden" />
                        <input id="clearall" class="button2" name="clear" style="width: 100px;" type="button" value="Clear All" />
                    </div>
                   <div id="row2_shift">
                        <input class="button" name="a" type="button" value="A" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="s" type="button" value="S" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="d" type="button" value="D" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="f" type="button" value="F" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="g" type="button" value="G" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="h" type="button" value="H" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="j" type="button" value="J" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="k" type="button" value="K" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="l" type="button" value="L" size="" onclick="javascript: return keycount();"/>
                        <input class="button2"  type="button" style="visibility: hidden" />
                        <input id="clearall" class="button2" name="clear" style="width: 100px;" type="button" value="Clear All" />
                    </div>
                    <div id="row3">
                        <input class="button" name="z" type="button" value="z" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="x" type="button" value="x" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="c" type="button" value="c" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="v" type="button" value="v" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="b" type="button" value="b" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="n" type="button" value="n" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="m" type="button" value="m" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" id="leftarrow" name="leftarrow" type="button" value="<-" />
                        <input class="button2" id="rightarrow" name="rightarrow" type="button" value="->" />
                    </div>
                   <div id="row3_shift">
                        <input class="button" name="Z" type="button" value="Z" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="X" type="button" value="X" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="C" type="button" value="C" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="V" type="button" value="V" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="B" type="button" value="B" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="N" type="button" value="N" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="M" type="button" value="M" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" id="leftarrow" name="leftarrow" type="button" value="<-" />
                        <input class="button2" id="rightarrow" name="rightarrow" type="button" value="->" />
                    </div>
                    </div>
                    <div class="code_btn">
                        <div class="code_submit" onclick="enter_sweeps_code();"></div>
                        <div class="code_back" onclick="window.location.href='lobby.php'"></div>
                    </div>
                </div>
            </div>
            <div id="footer">
                    <div id="footerBox">
                    <div class="footerBox_left"></div>
                    <div class="footerBox_body">
                        <div class="under18"></div>
                        <div class="rules" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
                        <div class="terms" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
                    </div>
                    <div class="footerBox_right"></div>
                </div>
             </div>
        </div>
    </body>
</html>