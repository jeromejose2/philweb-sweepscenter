<?php
/*
 * Created By: Jerome F. Jose
 * Created On: February 05, 2013
 * Purpose: for  browse internet confirmation
 */
include('../controller/launchpadprocess.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="css/launchpad.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
        <link href="css/lightbox.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript" src="jscripts/trans.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/CSSPopUp.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/open_new_window.js"></script>
        <script src="jscripts/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="jscripts/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript" src="jscripts/form_validation.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/lightbox.js"></script>
 <script type="text/javascript">
                //Using document.ready causes issues with Safari when the page loads
                jQuery(window).load(function(){
                        $("#contentContainer").backgroundScale({
                                imageSelector: "#gaBG",
                                centerAlign: true,
                                containerPadding: 0
                        });
                });
                
function onload()
{
    document.getElementById('popUpDivLPBrowse').style.display='block';
	document.getElementById('fade').style.display='block';
}

</script>
        <title>Launch Pad</title>
 <?php $xajax->printJavascript(); ?>
</head>
<body onload ="onload();" background='images/browse_net.png' style='background-position: top center; background-repeat:no-repeat;background-color:#0b1933; '>
  <div id="blanket" style="display:none;"></div>  
    
<div id="popUpDivLPBrowse" style="top: 40%;left:30%;display:none; font-family:Helvetica; font-size: 20px;width:560px;height:230px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b>
   <div style="margin-top: 8px; position: absolute; margin-left: 25%;">CONFIRMATION OF ACTION</div></b></div>
    <div id="popup_container_home" style="margin-left:20px; margin-top:15px; width:480px; height:60px; font-weight:lighter;">
       <br></br><div id="convert" align="center"></div>
       <div id="convert_img" align="center"><p>Would you like to <b style="font-weight:bold;">Enter our Sweepstakes?</b></p></div><br></br>
       <div style="height: 40px;width:110%;margin-left:5%;">
       <!--for Enter Sweepstakes--><div style="float: left;margin-left:-10px;"><img src="images/enter_sweeps.png" alt="" onclick="document.getElementById('popUpDivLPBrowse').style.display='none';document.getElementById('light4').style.display='block';" style="cursor:pointer;"/></div>
       <!--for Continue Browsing--><div id="okbtn" align="center" style="float: left;margin-left:10px;"><img src="images/continue_browse.png" alt="" onclick="browse_internet(); xajax_InterBrowsingLogs('conf_btn'); popup('popUpDivLPBrowse');" style="cursor:pointer;"/></div>
       </div>
    </div>
</div>

<div id="popUpDivHome" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 60px;">TERMS AND CONDITION CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"><p>Please confirm that you have read the</p><br/><p> Terms & Conditions.</p></div>
        <div id="okbtn" align="center" style="margin-top: 40px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivHome');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="light" class="white_content"><?php include('mechanics.php') ?></div>
<div id="light2" class="white_content"><?php include('terms.php') ?></div>
<div id="light3" class="white_content2"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>
<div id="light4" class="white_content"><?php include('terms3.php') ?></div>
<div id="light5" class="white_content"><?php include('terms4.php') ?></div>
<div id="fade" class="black_overlay"></div>




    </body>
</html>