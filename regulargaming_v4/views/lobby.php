<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 20, 2012
 * 
 * Modified By: Noel Antonio
 * Date Modified : October 11, 2012
 */

include('../controller/launchpadprocess.php');
include('../controller/lobbyprocess.php');
//checking status Isplayable jfj 04-18-2013
        App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
        $cterminals = new SCC_Terminals();
        $arr = $cterminals->CheckPlayableStatus($_SESSION['user']);
        if($arr[0]['IsPlayable'] == 2 || $arr[0]['IsPlayable'] == 3)
        {
            session_destroy();
            URL::Redirect('index.php');
        }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="css/launchpad.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
        <link href="css/lightbox.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript" src="jscripts/trans.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/CSSPopUp.js"></script>
<!--        <script language="javascript" type="text/javascript" src="jscripts/convert.js"></script>-->
        <script src="jscripts/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="jscripts/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript" src="jscripts/lightbox.js"></script>                
        <script type="text/javascript" src="jscripts/jquery.js"></script>
        <script type="text/javascript" src="jscripts/easySlider1.5.js"></script>
        <script type="text/javascript" src="jscripts/easySlider1.5_1.js"></script>
        <script type="text/javascript">
               function endterminalsession()
                {
                    endterminalsession1();
                }
                function convertpoints2()
                {
                    convert_points();
                }
                
               //Using document.ready causes issues with Safari when the page loads
                $(document).ready(function(){
                    
     function browserTester(browserString)
    {
        return navigator.userAgent.toLowerCase().indexOf(browserString) > -1;
    }

    if(browserTester('chrome'))       {   cssfunctionChrome();     } 
    else if(browserTester('safari'))  {   cssfunctionSafari();     }
    else if(browserTester('firefox')) {   cssfunctionFirefox();    }


		$("#slider").easySlider2({
			prevText: 'Previous',
			nextText: 'Next',		
			vertical: true
		});
                $("#slider1").easySlider1({
			prevText: 'Previous',
			nextText: 'Next',		
			vertical: true
		});
                 jQuery(window).load(function(){
                         $.ajax
                     ({  url: '../controller/get_casinogames.php?ctr=0&mod=0',type : 'post',
                         success : function(data)
                         {$('.result1').html(data);
                         },
                         error: function(e)
                         {alert('Error');}
                     }); 
                     $.ajax
                     ({  url: '../controller/get_casinogames.php?ctr=0&mod=1',type : 'post',
                         success : function(data)
                         {
                             $('.result2').html(data);
                         },
                         error: function(e)
                         {alert('Error');}
                     });                         
                });
                
                
                
	});
        
        var Clicker = 0;
        function scrolldown()
        {
            Clicker = Clicker + 1;
            if(Clicker > 0)
                {
                loading();
                        $.ajax
                     ({
                         url: '../controller/get_casinogames.php?ctr='+Clicker+'&mod=up',
                         type : 'post',
                         success : function(data)
                         {
                            $('.resultup').html(data);
                         },
                         error: function(e)
                         {alert('Error');}
                     });
                      $.ajax
                     ({
                         url: '../controller/get_casinogames.php?ctr='+Clicker+'&mod=down',
                         type : 'post',
                         success : function(data)
                         {$('.resultdown').html(data);
                         },
                         error: function(e)
                         {alert('Error');}
                     });
                     
                }
               
        }
                 function scrollup()
        {
            Clicker = Clicker - 1;
                    document.getElementById('light31').style.display='block';document.getElementById('fade1').style.display='block';
                      setTimeout(
                         function() 
                         {
                           document.getElementById('light31').style.display='none';document.getElementById('fade1').style.display='none';
                         }, 500);
            if(Clicker > 0)
                {
                    $.ajax
                     ({
                         url: '../controller/get_casinogames.php?ctr='+Clicker+'&mod=up',
                         type : 'post',
                         success : function(data)
                         {$('.resultup').html(data);
                         },
                         error: function(e)
                         {alert('Error');}
                     });
                      $.ajax
                     ({
                         url: '../controller/get_casinogames.php?ctr='+Clicker+'&mod=down',
                         type : 'post',
                         success : function(data)
                         {$('.resultdown').html(data);
                         },
                         error: function(e)
                         {alert('Error');}
                     });
                     
                 
                }
        }
        
        function loading()
        {
            if(Clicker > 0)
                {
                    document.getElementById('light31').style.display='block';document.getElementById('fade1').style.display='block';
                     setTimeout(
                         function() 
                         {
                            document.getElementById('light31').style.display='none';document.getElementById('fade1').style.display='none';
                         }, 500);
                }
            
        }
function cssfunctionSafari()
{
 //$(".back_launchPad").css("margin-top","65px"); 
}
function cssfunctionFirefox()
{
    //$(".back_launchPad").css("margin-top","65px"); 
}
function cssfunctionChrome()
{$(".back_launchPad").css("margin-top","65px");
}

        </script>
        
      <style>  
/* Easy Slider */
	#slider ul, #slider li{margin:0;padding:0;list-style:none;}
	#slider li{ width:800px;height:210px;overflow:hidden;}        	
        #slider1 ul, #slider1 li{margin:0;padding:0;list-style:none;}
	#slider1 li{width:800px;height:210px;overflow:hidden;}
/* // Easy Slider */

</style>
        <title>Games List</title>
        <?php $xajax->printJavascript(); ?>
    </head>
    <body onload="do_getbalance();">
        <div id="blanket" style="display:none;"></div>
        <div id="popUpDivConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
            <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 180px;">CONVERT POINTS</div></b></div>
            <div id="popup_container_home">
                <div id="convert" align="center"></div><div id="convert_img" align="center" style="visibility:hidden;"><img src="images/load_bal.gif" height="20px" /></div>
                <div id="okbtn" align="center" style="margin-top: 0px;"></div><div id="okbtn_img" align="center" style="margin-top: -20px; visibility:hidden;">PROCESSING</div>
            </div>
        </div>
        <div id="popUpDivLPConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
            <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 100px;">CONVERT POINTS CONFIRMATION</div></b></div>
            <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
                <div id="convert" align="center"></div><div id="convert_img" align="center"><p>Are you sure you want to convert your <label id="convbal"></label></p><br/> <p> point/s and end your gaming session now?</p></div>
                <div id="okbtn" align="center" style="margin-top: 40px; margin-left: 80px; float: left;"><img src="images/OK Button.png" alt="" onclick="show_loading();confirmpoints1(); popup('popUpDivLPConvert');" style="cursor:pointer;"/></div><div style="margin-top: 35px;"><img src="images/cancelbutton.png" alt="" onclick="popup('popUpDivLPConvert');" style="cursor:pointer;"/></div>
            </div>
        </div>
        <div id="popUpDivLPCheckActiveSession" style="display:none; font-family:Helvetica; font-size: 20px;">
            <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 230px;">ALERT</div></b></div>
            <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
                <div id="msg" align="center"></div>
                <div id="okbtn" align="center" style="margin-top: 20px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLPCheckActiveSession');" style="cursor:pointer;"/></div>
            </div>
        </div>

        <div id="light" class="white_content"><?php include('mechanics.php') ?></div>
        <div id="light2" class="white_content"><?php include('terms.php') ?></div>
        <div id="light3" class="white_content2"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>
        <div id="fade" class="black_overlay"></div>
        <div id="mainContainer">
            <div id="banner">
                <table width="100%" border="0">
                    <tr>
                        <td align="center" width="27%"><img src="images/theSweepsLogo.png" alt="" height="190" width="270" /></td>
                            <!--<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>-->
                            <td align="center" width="40%" >
                            <div style="color: white;top:15%;left: 50%;position: absolute;margin-left: -150px;">Logged In As:
                                <?php
                                    $login = $_SESSION['user'];
                                    $new_string = ereg_replace("[^0-9]", "", $login);
                                    echo "Terminal ".$new_string;
                                ?>
                            </div>
                            <div id="txtBoxContainer_point" style="top:1%;left: 15%;position: absolute;margin-left: 150px;">
                                <div class="txtBox_left"></div>
                                <div class="txtBox_body"><img src="images/load_bal.gif" id="load_bal_img" alt="" style="margin-left: 20px; margin-top: 10px; width: 180px;" /><div id="balance"></div></div>
                                <div class="txtBox_right"></div>
                            </div>
                            <div id="btnContainer" style="top:49%;left: 15%;position: absolute;margin-left: 150px;">
                                    <div class="convertPoints" onclick="show_loading(); xajax_GetBalanceConv();">
                                        <img src="images/convertPoints.png" height="33px" width="150px" />
                                    </div>
                                    <?php echo $conv_btn ?>
                                    <div class="enterCode" onclick="event.cancelBubble = true;//xajax_CheckSessionSweepsCode();">
                                        <img src="images/enterCode.png" height="33px" width="160px" />
                                    </div>
                            </div>
                        </td>
                        <td align="center">
                            <div align="right" id="adContainer">
			<img src="images/adContainer.png"/>  
                        <div class="adContent">	
                                <a href="lobby.php">
                                <img src="images/5000.gif"style="margin-top:-163px; margin-right: 14px; margin-bottom: 10px;"/></a>
                
		                </div>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
        <?php echo $hiddenclicker; ?>
        <img id="gaBG" src="" />
        <div>
<?php
    $div="";
    $user_agent = $_SERVER['HTTP_USER_AGENT']; 

    if (preg_match('/MSIE/i', $user_agent))
    {//"Internet Explorer";
        $div='<div class="back_launchPad" style=" top: 14%;left: 47%;height: 36px;width: 240px;position: absolute;margin-left: -138px;margin-top: 65px;" onclick="location.href=\'launchpad.php\';"></div>';
    }else
    {//"Non-IE Browser";
        $div='<div class="back_launchPad" style=" top: 14%;left: 47%;height: 36px;width: 240px;position: absolute;margin-left: -138px;margin-top: 65px;" onclick="location.href=\'launchpad.php\';"></div>';
    }
    
    echo $div;
?>              
        </div>
        
    <?php if ($activeservice == 1) { ?>
         <?php if(isset($ActiveGames)){?>
      <div style="top: 50%;margin-left: -500px;margin-top: -180px;height: 300px;width: 300px;left: 50%;z-index: -1;position: absolute;">
      <!--<div id="slider" style="margin-left:30%;margin-top:-3%;background-color : '';">-->            
      <div id="slider" style="margin-left:30%;margin-top:7%;background-color : '';">                      
                <ul>
                <li>
                   <div class="result1"></div>
                </li>
               <?php 
                    for($av=0;$av<$list1;$av++)
                        {     ?>
                <li>
                   <div class="resultup"></div>
               </li>
                    <?php }?>
                </ul>
           
        </div> 
        <!--<div id="slider1" style="margin-left: 30%;margin-top:-.1%;background-color :'';">-->            
<div id="slider1" style="margin-left: 30%;margin-top:7.9%;background-color :'';">                        
                <ul>  
                <li>
                    <div class="result2"></div>
                </li>
                    <?php 
                    for($av=0;$av<$list2;$av++)
                        {     ?>
                <li>
                    <div class="resultdown"></div>
                </li>                  
                </ul>
              <?php }?>
                <?php }?>
        </div>
    </div>
  
    <?php } else if ($activeservice == 3) 
         { ?>
        
         <?php if(isset($ActiveGames)){?>
        <div style="top: 50%;margin-left: -500px;margin-top: -180px;height: 300px;width: 300px;left: 50%;z-index: -1;position: absolute;">
            <div id="slider" style="margin-left:30%;margin-top:7%;background-color : '';">            
                    <ul>
                          <li>
                             <div class="result1"></div>
                          </li>
                         <?php 
                              for($av=0;$av<$list1;$av++)
                                  {     ?>
                          <li>
                             <div class="resultup"></div>
                         </li>
                              <?php }?>
                      </ul>

            </div> 
        <div id="slider1" style="margin-left: 30%;margin-top:-.2%;background-color :'darkgray';">            
            <ul>  
                <li>
                    <div class="result2"></div>
                </li>
                    <?php 
                    for($av=0;$av<$list2;$av++)
                        {     ?>
                    <li>
                        <div class="resultdown"></div>
                    </li>
                  
            </ul>
              <?php }?>
                <?php }?>
        </div>
    </div>
    <?php } ?>
         <!--for loading inside the slider-->
  <!--<div style="top: 50%;margin-left: -410px;margin-top: -130px;height: 400px;width: 800px;left: 50%;z-index: -1;position: absolute;">-->

  <div id="light31" class="white_content_forloading">
      <div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 10px;" /></div></div>
 
  <!--</div>-->
  <!--for loading inside the slider-->
   <div id="fade1" class="black_overlay1"></div>
            
            <div id="footer">
                <div id="footerBox">
                    <div class="footerBox_left"></div>
                    <div class="footerBox_body">
                        <div class="under18"></div>
                        <div class="rules" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
                        <div class="terms" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
                    </div>
                    <div class="footerBox_right"></div>
                </div>
             </div>
        </div>
    </body>
</html>
