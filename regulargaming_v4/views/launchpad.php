<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 08, 2012
 * Updated By: Tere Calderon
 * Updated On: September 27, 2012
 * Purpose: Incorporated the updated css
 */
include('../controller/launchpadprocess.php');
include('../controller/lobbyprocess.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="css/launchpad.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
        <link href="css/lightbox.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript" src="jscripts/trans.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/CSSPopUp.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/open_new_window.js"></script>
        <script src="jscripts/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="jscripts/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript" src="jscripts/form_validation.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/lightbox.js"></script>
        <script type="text/javascript">
                //Using document.ready causes issues with Safari when the page loads
                jQuery(window).load(function(){
                        $("#contentContainer").backgroundScale({
                                imageSelector: "#gaBG",
                                centerAlign: true,
                                containerPadding: 0
                        });
                });
        </script>
        <title>Launch Pad</title>
        <?php $xajax->printJavascript(); ?>
    </head>
   <body onload="do_CheckCard();" scroll="no">
<div id="blanket" style="display:none;"></div>
<div id="popUpDivConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 180px;">CONVERT POINTS</div></b></div>
    <div id="popup_container_home">
        <div id="convert" align="center"></div><div id="convert_img" align="center" style="visibility:hidden;"><img src="images/load_bal.gif" height="20px" /></div>
        <div id="okbtn" align="center" style="margin-top: 0px;"></div><div id="okbtn_img" align="center" style="margin-top: -20px; visibility:hidden;">PROCESSING</div>
    </div>
</div>

<div id="popUpDivLPConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 100px;">CONVERT POINTS CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"></div><div id="convert_img" align="center"><p>Are you sure you want to convert your <label id="convbal"></label></p><br/> <p>point/s and end your gaming session now?</p></div>
        <div id="okbtn" align="center" style="margin-top: 40px; margin-left: 80px; float: left;"><img src="images/OK Button.png" alt="" onclick="convert_points(); popup('popUpDivLPConvert');" style="cursor:pointer;"/></div><div style="margin-top: 35px;"><img src="images/cancelbutton.png" alt="" onclick="popup('popUpDivLPConvert');" style="cursor:pointer;"/></div>
    </div>
</div>
<!--NEW CONFIRMATION-->
<div id="popUpDivLPBrowse" style="display:none; font-family:Helvetica; font-size: 20px;width:560px;height:230px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b>
   <div style="margin-top: 8px; position: absolute; margin-left: 25%;">BROWSING CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:20px; margin-top:15px; width:480px; height:60px; font-weight:lighter;">
       <br></br><div id="convert" align="center"></div>
       <div id="convert_img" align="center"><p>Would you like to <b style="font-weight:bold;">Enter our Sweepstakes?</b></p></div><br></br>
       <div style="height: 40px;width:110%;margin-left:5%;">
       <!--for Enter Sweepstakes--><div style="float: left;margin-left:-10px;"><img src="images/enter_sweeps.png" alt="" onclick="document.getElementById('blanket').style.display='none';document.getElementById('popUpDivLPBrowse').style.display='none';document.getElementById('light4').style.display='block';document.getElementById('fade').style.display='block';" style="cursor:pointer;"/></div>
       <!--for Continue Browsing--><div id="okbtn" align="center" style="float: left;margin-left:10px;"><img src="images/continue_browse.png" alt="" onclick="browse_internet(); xajax_InterBrowsingLogs('conf_btn'); popup('popUpDivLPBrowse');" style="cursor:pointer;"/></div>
       </div>
    </div>
</div>
<!--NEW CONFIRMATION-->
<div id="popUpDivLPBrowseNoBal" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 150px;">INSUFFICIENT BALANCE</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"></div><div id="convert_img" align="center"><p>You have insufficient points in your balance.</p> <p style="margin-top: 10px;">Please reload to enjoy continuous</p> <p style="margin-top: 10px;">Internet browsing time.</p></div>
        <div id="okbtn" align="center" style="margin-top: 20px; margin-left: 150px; float: left;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLPBrowseNoBal');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="popUpDivLPCheckActiveSession" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 230px;">ALERT</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="msg" align="center"></div>
        <div id="okbtn" align="center" style="margin-top: 40px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLPCheckActiveSession');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="popUpDivHome" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 60px;">TERMS AND CONDITION CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"><p>Please confirm that you have read the</p><br/><p> Terms & Conditions.</p></div>
        <div id="okbtn" align="center" style="margin-top: 40px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivHome');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="light" class="white_content"><?php include('mechanics.php') ?></div>
<div id="light2" class="white_content"><?php include('terms.php') ?></div>
<div id="light3" class="white_content2"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>
<div id="light4" class="white_content"><?php include('terms3.php') ?></div>
<div id="light5" class="white_content"><?php include('terms4.php') ?></div>
<div id="fade" class="black_overlay"></div>

<div id="mainContainer">
    <div id="bg"><img src="images/contentbg.jpg" width="100%" height="100%" alt="" /></div>
    <div id="banner">
        <table width="100%" border="0">
            <tr>
                 <td align="center" width="30%">
                    <img style="margin-left: 10%;"src="images/theSweepsLogo.png" alt="" height="190" width="270" />
                </td>
                 <td align="center" width="40%" style=" ">
                     <div style="width: auto;height: 50%;position: absolute;left : 33%;top: 10%;" align="center">
                                <div style="color: white;margin-left: 0%;margin-top: 10%;">Logged In As:
                                       <?php
                                       $login = $_SESSION['user'];
                                       $new_string = ereg_replace("[^0-9]", "", $login);
                                       echo "Terminal ".$new_string;
                                       ?>
                                </div>
                               <div class="eks" id="txtBoxContainer_point" style="margin-top:0%;left: auto;margin-left: 1%;" onclick="window.location='launchpad.php';//alert('One is clicked');">
                                        <div class="txtBox_left"></div>
                                        <div class="txtBox_body"><img src="images/load_bal.gif" id="load_bal_img" alt="" style="margin-left: 20px; margin-top: 10px; width: 180px;" />
                                        <div id="balance" style=""></div>
                                        </div>
                                        <div class="txtBox_right"></div>
                               </div>
                     </div>
                    
                </td>
                 <td align="center" width="30%">
                </td>
               
            </tr>
        </table>       
    </div>

</div>

        <div id="buttoncontainer">
            <table width="100%" border="0">
                <tr>
                    <td margin-left="50%" id="launch_Btn2">
                        <div class="launch_game" style ="margin-left: 40%;" onclick="window.location='browse_internet.php';"></div><br />
                        <div class="launch_title" style="width: auto;margin-left: 25%;">Browse&nbsp;Internet</div>
                    </td>
                </tr>
            </table>
        </div>

        <div id="footer">
           <div id="footerBox">
            	<!--<div class="footerBox_left"></div>-->
<!--                    <div class="footerBox_body">
                        <div class="under18"></div>
                        <div class="rules" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
                        <div class="terms" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
                    </div>
                <div class="footerBox_right"></div>-->
            </div>
         </div>
    </body>
</html>