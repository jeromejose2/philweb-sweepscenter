<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 08, 2012
 * Updated By: Tere Calderon 
 * Updated On: September 27, 2012
 * Purpose: Incorporated the updated css
 */
include('../controller/launchpadprocess.php');
include('../controller/lobbyprocess.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="css/launchpad.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
        <link href="css/lightbox.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript" src="jscripts/trans.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/CSSPopUp.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/open_new_window.js"></script>
        <script src="jscripts/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="jscripts/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript" src="jscripts/form_validation.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/lightbox.js"></script>
        <?php $xajax->printJavascript(); ?>
        <script type="text/javascript">
                //Using document.ready causes issues with Safari when the page loads
                jQuery(window).load(function(){
                        $("#contentContainer").backgroundScale({
                                imageSelector: "#gaBG",
                                centerAlign: true,
                                containerPadding: 0
                        });
                });
//$(document).ready(function() {
                    
             function endterminalsession()
                {
                    endterminalsession1();
                }
            function convertpoints2()
                {
                    convert_points();
                }
              
                
//});
               
        </script>
        <title>Launch Pad</title>
        
    </head>
   <body onload="do_getbalance(); show_loading()">
<div id="blanket" style="display:none;"></div>
<div id="popUpDivConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 180px;">CONVERT POINTS</div></b></div>
    <div id="popup_container_home">
        <div id="convert" align="center"></div><div id="convert_img" align="center" style="visibility:hidden;"><img src="images/load_bal.gif" height="20px" /></div>
        <div id="okbtn" align="center" style="margin-top: 0px;"></div><div id="okbtn_img" align="center" style="margin-top: -20px; visibility:hidden;">PROCESSING</div>
    </div>
</div>

<div id="popUpDivLPConvert" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 100px;">CONVERT POINTS CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"></div><div id="convert_img" align="center"><p>Are you sure you want to convert your <label id="convbal"></label></p><br/> <p>point/s and end your gaming session now?</p></div>
        <div id="okbtn" align="center" style="margin-top: 40px; margin-left: 80px; float: left;"><img src="images/OK Button.png" alt="" onclick="show_loading();confirmpoints1(); popup('popUpDivLPConvert');" style="cursor:pointer;"/></div><div style="margin-top: 35px;"><img src="images/cancelbutton.png" alt="" onclick="popup('popUpDivLPConvert');" style="cursor:pointer;"/></div>
    </div>
</div>
<!--for popup Proceed-->
<div id="popUpDivLPProceed" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 100px;">CONVERT POINTS CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">test
        <!--<div id="convert" align="center"></div><div id="convert_img" align="center"><p>Are you sure you want to convert your <label id="convbal"></label></p><br/> <p>point/s and <label id="convbal1"></label>end your gaming session now?</p></div>-->
        <!--<div id="okbtn" align="center" style="margin-top: 40px; margin-left: 80px; float: left;"><img src="images/OK Button.png" alt="" onclick="convert_points(); popup('popUpDivLPProceed');" style="cursor:pointer;"/></div><div style="margin-top: 35px;"><img src="images/cancelbutton.png" alt="" onclick="popup('popUpDivLPConvert');" style="cursor:pointer;"/></div>-->
    </div>
</div>
<!--for popup Proceed-->

<div id="popUpDivLPBrowse" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 60px;">INTERNET BROWSING CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:20px; margin-top:15px; width:480px; height:60px; font-weight:bold;">
	<div id="convert" align="center"></div><div id="convert_img" align="center"><p>You have selected BROWSE INTERNET. Kindly</p><p style="margin-top: 10px;">select OKAY if you wish to continue. Five(5)</p> <p style="margin-top: 10px;">points will be deducted from your point balance.</p> <p style="margin-top: 10px;"> Otherwise, select CANCEL to go back to the Launch Pad. Thank you.</p></div>
        <div id="okbtn" align="center" style="margin-top: 5px; margin-left: 80px; float: left;"><img src="images/OK Button.png" alt="" onclick="browse_internet(); xajax_InterBrowsingLogs('conf_btn'); popup('popUpDivLPBrowse');" style="cursor:pointer;"/></div><div style="margin-top: 3px;"><img src="images/cancelbutton.png" alt="" onclick="xajax_InterBrowsingLogs('cancel_btn'); popup('popUpDivLPBrowse');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="popUpDivLPBrowseNoBal" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 150px;">INSUFFICIENT BALANCE</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"></div><div id="convert_img" align="center"><p>You have insufficient points in your balance.</p> <p style="margin-top: 10px;">Please reload to enjoy continuous</p> <p style="margin-top: 10px;">Internet browsing time.</p></div>
        <div id="okbtn" align="center" style="margin-top: 20px; margin-left: 150px; float: left;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLPBrowseNoBal');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="popUpDivLPCheckActiveSession" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 230px;">ALERT</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="msg" align="center"></div>
        <div id="okbtn" align="center" style="margin-top: 40px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivLPCheckActiveSession');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="popUpDivHome" style="display:none; font-family:Helvetica; font-size: 20px;">
    <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 60px;">TERMS AND CONDITION CONFIRMATION</div></b></div>
    <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
        <div id="convert" align="center"><p>Please confirm that you have read the</p><br/><p> Terms & Conditions.</p></div>
        <div id="okbtn" align="center" style="margin-top: 40px;"><img src="images/OK Button.png" alt="" onclick="popup('popUpDivHome');" style="cursor:pointer;"/></div>
    </div>
</div>

<div id="light" class="white_content"><?php include('mechanics.php') ?></div>
<div id="light2" class="white_content"><?php include('terms.php') ?></div>
<div id="light3" class="white_content2"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>
<div id="light4" class="white_content"><?php include('terms3.php') ?></div>
<div id="light5" class="white_content"><?php include('terms4.php') ?></div>
<div id="fade" class="black_overlay"></div>

<div id="mainContainer">
    <div id="bg"><img src="images/contentbg.jpg" width="100%" height="100%" alt="" /></div>
    <div id="banner">
        <table width="100%" border="0">
            <tr>
                <td align="center">
                    <img src="images/theSweepsLogo.png" alt="" height="190" width="270" />
                </td>
                <!--<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
                <!--</td>-->
                <td align="center">
                    <div style="color: white;">Logged In As:
                    <?php
                    $login = $_SESSION['user'];
                    $new_string = ereg_replace("[^0-9]", "", $login);

                    echo "Terminal ".$new_string;
                    ?>
                    </div>
                    <div id="txtBoxContainer_point">
                    <div class="txtBox_left"></div>
                    <div class="txtBox_body"><img src="images/load_bal.gif" id="load_bal_img" alt="" style="margin-left: -20px; margin-top: 10px;" /><div id="balance"></div></div>
                    <div class="txtBox_right"></div>
                    </div>
                    <div id="btnContainer">
                        <div id="convertPointsContainer">
                            <div class="convertPoints" onclick="show_loading(); xajax_GetBalanceConv();">
                                <div class="convertPoints" style="cursor: pointer;">
                                <img src="images/convertPoints.png" height="33px" width="150px" />

                                </div>
                                <?php echo $conv_btn ?>
                                <div class="enterCode" onclick="xajax_CheckSessionSweepsCode();">
                                <img src="images/enterCode.png" height="33px" width="160px" />
                                </div>
                            </div>
                        </div>
                    </div>          
                </td>
                <td align="center">
                    <div align="right" id="adContainer">
                    <img src="images/adContainer.png"/>  
                    <div class="adContent">
                    <a href="">
                         <img src="images/Ad.gif"style="margin-top:-163px; margin-right: 14px;"/></a>
                    </div>
                    </div>
                </td>
            </tr>
        </table>       
    </div>

</div>

        <div id="buttoncontainer">
            <table width="100%" border="0">
                <tr>
                    <td width="50%" align="center" id="launch_Btn">
                        <div class="launch_game" onclick="document.getElementById('light4').style.display='block';document.getElementById('fade').style.display='block';"></div><br />
                        <div class="launch_title"> Play<br /> Games</div>
                    </td>
                    <td margin-left="50%" id="launch_Btn2">
                        <div class="launch_game" onclick="xajax_InterBrowsingLogs('browse_btn'); document.getElementById('light5').style.display='block';document.getElementById('fade').style.display='block';"></div><br />
                        <div class="launch_title">Browse<br /> Internet</div>
                    </td>
                </tr>
            </table>
        </div>

        <div id="footer">
            <div id="footerBox">
            	<div class="footerBox_left"></div>
                    <div class="footerBox_body">
                        <div class="under18"></div>
                        <div class="rules" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
                        <div class="terms" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
                    </div>
                <div class="footerBox_right"></div>
            </div>
         </div>
    </body>
</html>