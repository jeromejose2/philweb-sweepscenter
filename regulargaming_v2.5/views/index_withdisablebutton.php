<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 19, 2012
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login : Launch Pad</title>
<link rel="stylesheet" type="text/css" href="css/login.css" />

<script language="javascript" type="text/javascript" src="jscripts/jquery-1.7.1.min.js"></script> <!-- Added By Arlene R. Salazar 04/26/2012 -->
<script language="javascript" type="text/javascript" src="jscripts/lightbox.js"></script>

<style>
    .txtName2{
	font: bold 20px/25px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	color: #FFF;
	text-transform: uppercase;
	width:150px;
	height:auto;
	margin:10px 0 0 15px;
	float:left;
        width: 100%;
        text-align: center;
    }

    .disableManualLogin
    {
        margin-top: 5px;
    }
    .disableRegister
    {
        margin-top: 10px;
    }
    .enableManualLogin
    {
        margin-top: 10px;
        cursor: pointer;
    }
    .enableRegister
    {

    }
</style>
</head>
<script>
//Added By Arlene R. Salazar 04/26/2012
$(document).ready(function(){
    macs.getMacAddress();
    var macadd = document.macaddressapplet.getMacAddress();

    $.ajax({
        url: '../controller/checkTerminalCredentials.php?mcadd=' + macadd.trim() ,
        type : 'post',
        success : function(data)
        {
            var json = jQuery.parseJSON(data);
            var manuallogin_flag = json.manuallogin;
            var register_flag = json.register;

            var enabledManualLoginButton = "<img src=\"images/manuallogin.png\" width=\"150\" height=\"40\" style=\"margin-top: 5px; cursor: pointer;\" onclick=\"window.location.href='index_manuallogin.php'\"/>";
            var disabledManualLoginButton = "<img src=\"images/manuallogin.png\" width=\"150\" height=\"40\" style=\"margin-top: 5px;\"/>";
            var enabledRegisterButton = "<img src=\"images/registerred.png\" width=\"180\" height=\"40\" style=\"margin-top: 10px; cursor: pointer;\" onclick=\"window.location.href='index_register.php'\" />";
            var disabledRegisterButton = "<img src=\"images/registerred.png\" width=\"180\" height=\"40\" style=\"margin-top: 10px;\"/>";

            if(manuallogin_flag == 1)
            {
                document.getElementById('buttons_holder').innerHTML += enabledManualLoginButton;
            }
            else
            {
                document.getElementById('buttons_holder').innerHTML += disabledManualLoginButton;
            }
            if(register_flag == 1)
            {
                document.getElementById('buttons_holder').innerHTML += enabledRegisterButton;
            }
            else
            {
                document.getElementById('buttons_holder').innerHTML += disabledRegisterButton;
            }
        },
        error: function(e)
        {
            alert(e.responseText);
        }
    });

})
</script>
<body>
<div id="mainContainer">
    <div id="banner"></div>
    <div id="contentContainer">
        <div id="Login_Cont">
            <div class="login_logo"></div>
            <div class="login_body">
				<input type="hidden" name="hidden_mcadd" id="hidden_mcadd"/>
                <div class="loginCont_body">
                    <div class="txtName2">CHOOSE LOGIN TYPE:</div>
                     <div style="width: 100%; text-align: center;" id="buttons_holder">
                        <img src="images/autologin.png" width="150" height="40" style="margin-top: 5px; cursor: pointer;"  onclick="window.location.href='index_autologin.php'" />
                    </div>
                </div>
                <div class="login_bottom">&nbsp;</div>
                <div id="err_msg" align="center"><?php include('controller/cindex.php'); ?></div>
            </div>
        </div>
        <div id="footer"></div>
    </div>
</div>
</body>
</html>
<!--[if !IE]> Firefox and others will use outer object -->
<embed type="application/x-java-applet"
       name="macaddressapplet"
       width="0"
       height="0"
       code="MacAddressApplet.class"
       archive="SMacAddressApplet.jar"
       pluginspage="http://java.sun.com/javase/downloads/index.jsp"
       style="position:absolute; top:-1000px; left:-1000px;">
    <noembed>
    <!--<![endif]-->
        <!---->
        <object classid="clsid:CAFEEFAC-0016-0000-FFFF-ABCDEFFEDCBA"
                type="application/x-java-applet"
                name="macaddressapplet"
                style="position:absolute; top:-1000px; left:-1000px;"
                >
            <param name="code" value="MacAddressApplet.class">
            <param name="archive" value="SMacAddressApplet.jar" >
            <param name="mayscript" value="true">
            <param name="scriptable" value="true">
            <param name="width" value="0">
            <param name="height" value="0">
          </object>
    <!--[if !IE]> Firefox and others will use outer object -->
    </noembed>
</embed>
<!--<![endif]-->
<script type="text/javascript">
var macs = {
    getMacAddress : function()
    {
        document.macaddressapplet.setSep( "-" );
        //alert( "Mac Address = " + document.macaddressapplet.getMacAddress() );
    },
    getCPUProduct : function() {
        alert( "CPU Product = " + document.macaddressapplet.getProduct() );
    },
    getCPUVendor : function() {
        alert("CPU Vendor"+ document.macaddressapplet.getVendor());
    },
    getCPUVersion : function() {
        alert("CPU Version"+ document.macaddressapplet.getVersion());
    },
    getCPUSerial : function() {
        alert("CPU Serial"+ document.macaddressapplet.getSerial());
    },
    getMacAddressesJSON : function()
    {
        document.macaddressapplet.setSep( ":" );
        document.macaddressapplet.setFormat( "%02x" );
        var macs = eval( String( document.macaddressapplet.getMacAddressesJSON() ) );
        var mac_string = "";
        for( var idx = 0; idx < macs.length; idx ++ )
            mac_string += "\t" + macs[ idx ] + "\n ";

        alert( "Mac Addresses = \n" + mac_string );
    }
}
</script>