/*
* MM Flash Game Wrapper v1.0.0
* WEBiTS R&D
*
* Requires jQuery 1.6.2+, jQuery.swfobject 1.1.1
*
* Usage:
*
*	$('#container').MM_FlashGame_Wrapper({
*		swf: 'media/mini_flash_client.swf',
*		base: 'media/'
*	}, {
*		user: '<encryptedUsername>',
*		sPassword: '<encryptedPassword>',
*		IP: '<gameServerIP>',
*		casinoName: '<casinoName>',
*		gameid: '<gameid>',
*		machid: '<machid>'
*	});
*
*/

(function($){
$.fn.MM_FlashGame_Wrapper = function(flashOptions, flashVars) {
	var flashDefaults = {
		swf: 'media/mini_flash_client.swf',
		height: '100%',
		width: '100%',
		allowScriptAccess: 'always',
		allowFullScreen: 'true',
		menu: 'false',
		base: 'media',
		play: 'true',
		quality: 'high',
		scale: 'exactfit',
		wmode: 'transparent'
	};
	
	var flashVarsDefaults = {
		useLegacySystem: '0',
		user: '',
		sPassword: '',
		encrypted: 'true',
		forReal: 'true',
		IP: '',
		portBase: '0',
		casinoName: '',
		gameid: 0,
		hand: '1',
		machid: 0,
		denom: '100',
		showversion: 'false',
		token: '',
		language: 'EN'
	};
	
	var flashOptions = $.extend(flashDefaults, flashOptions);
	var flashVars = $.extend(flashVarsDefaults, flashVars);
	
	return this.each(function() {
		var obj = $(this);

		flashObject = $.flash.create(
			{
				swf: flashOptions.swf,
				height: flashOptions.height,
				width: flashOptions.width,
				allowScriptAccess: flashOptions.allowScriptAccess,
				allowFullScreen: flashOptions.allowFullScreen,
				menu: flashOptions.menu,
				base: flashOptions.base,
				play: flashOptions.play,
				quality: flashOptions.quality,
				scale: flashOptions.scale,
				wmode: flashOptions.wmode,
				flashvars: {
					useLegacySystem: flashVarsDefaults.useLegacySystem,
					user: flashVarsDefaults.user,
					sPassword: flashVarsDefaults.sPassword,
					encrypted: flashVarsDefaults.encrypted,
					forReal: flashVarsDefaults.forReal,
					IP: flashVarsDefaults.IP,
					portBase: flashVarsDefaults.portBase,
					casinoName: flashVarsDefaults.casinoName,
					gameid: flashVarsDefaults.gameid,
					hand: flashVarsDefaults.hand,
					machid: flashVarsDefaults.machid,
					denom: flashVarsDefaults.denom,
					showversion: flashVarsDefaults.showversion,
					token: flashVarsDefaults.token,
					language: flashVarsDefaults.language
				}
			}
		);

		obj.html(flashObject);		
	});
};
})(jQuery);