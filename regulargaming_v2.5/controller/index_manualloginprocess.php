<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 08, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");

App::LoadControl("TextBox");
App::LoadControl("Button");

$caudittrail = new SCC_AuditTrail();
$cterminals = new SCC_Terminals();
$cterminalsessions = new SCC_TerminalSessions();

$manuallogin_form = new FormsProcessor();

$txtuser = new TextBox("txtuser","txtuser","Username");
$txtuser->CssClass = "inputBoxEffect";
$txtuser->Length = 20; 
$txtuser->Args = "onkeypress='javascript:return DisableSpaceAndDashOnly(event)'";

$txtpass = new TextBox("txtpass","txtpass","Password");
$txtpass->Password = true;
$txtpass->Length = 20;
$txtpass->Args = "onkeypress='javascript:return DisableSpaceAndDashOnly(event)'";
$txtpass->CssClass = "inputBoxEffect";

$btnSubmit = new Button("btnSubmit","btnSubmit"," ");
$btnSubmit->CssClass = "login";
$btnSubmit->IsSubmit = true;

$manuallogin_form->AddControl($txtuser);
$manuallogin_form->AddControl($txtpass);
$manuallogin_form->AddControl($btnSubmit);

$manuallogin_form->ProcessForms();
if($manuallogin_form->IsPostBack)
{
    if($btnSubmit->SubmittedValue == " ")
    {
        $username = $txtuser->SubmittedValue;
        $password = $txtpass->SubmittedValue;
        $userdtls = $cterminals->SelectTerminalUserDtls($username,$password);
        $terminalid = $userdtls[0]['ID'];
        if(count($userdtls) > 0)
        {
            $caudittrail->StartTransaction();
            $arrAuditTrail["SessionID"] = "";
            $arrAuditTrail["AccountID"] = 0;
            $arrAuditTrail["TransDetails"] = 'Login: ' . $username;
            $arrAuditTrail["RemoteIP"] = $_SERVER['REMOTE_ADDR'];;
            $arrAuditTrail["TransDateTime"] = 'now_usec()';
            $caudittrail->Insert($arrAuditTrail);
            if($caudittrail->HasError)
            {
                $caudittrail->RollBackTransaction();
                $error_msg = "Error inserting in audit trail.";
            }
            else
            {
                $caudittrail->CommitTransaction();

                $cterminals->StartTransaction();
                $cterminals->UpdateIsPlayableStatus($terminalid,0);
                if($cterminals->HasError)
                {
                    $cterminals->RollBackTransaction();
                    $error_title = "ERROR";
                    $error_msg = "Error updating terminal\'s Isplayable status.";
                }
                else
                {
                    $cterminals->CommitTransaction();

                    $terminalsessionsdtls = $cterminalsessions->SelectTerminalSessionDetails($terminalid);
                    $_SESSION['user'] = $username;
                    $_SESSION['id'] = $terminalid;
                    $_SESSION['siteid'] = $terminalsessionsdtls[0]['SiteID'];
                    $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
                    $_SESSION['pass'] = $password;
                    $_SESSION['tsi'] = $terminalsessionsdtls[0]['ID'];
                    echo "<script>window.location = 'launchpad.php';</script>";		  
                }   

            }
        }
        else
        {
            $error_msg = "You have entered an invalid account information.";
        }
    }
}
?>