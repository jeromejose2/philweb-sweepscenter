<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 18, 2012
 * 
 * Modified By: Noel Antonio
 * Date Modified: October 10, 2012
 */

if (!isset($_SESSION))
{
    session_start();
}

include('../init.inc.php');
include('pagination.php');

App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");

$cwinningsdump = new SCC_WinningsDump();

$id = $_SESSION['id'];
$resultmsg = "";
$okbtn = "";

$allwinningsdtls = $cwinningsdump->SelectAllWinningsIsOpened($id);
$selectallOpenedCards = $cwinningsdump->SelectOpenCards($id);
$Allcards = $cwinningsdump->SelectAllWinnings($id);

if (count($selectallOpenedCards) == count($Allcards))
{
    echo "<script>window.location = '../views/displaycards_quickpick.php';</script>";	
}
else 
{
            $resultmsg = "<div id='resultmsg'class=\"winsumm\" ></div> ";
             $okbtn = "<p id='okbtn' class=\"quick7\"></p>";

             $avail_cards = count($allwinningsdtls);

             $html_content = "<div class=\"sweeps_quickContainer\">";
             $html_content .= "<div class=\"cardCount\">CARD COUNTER:</div>";
             $html_content .= "<div class=\"countBox_left\"></div>";
             $html_content .= "<div id='avail_cards' class=\"countBox_body\"><p>".$avail_cards."</p></div>";
             $html_content .= "<div class=\"countBox_right\"></div>";
             $html_content .= "<div id='quick_pick_btn' class=\"quick\" >";
             $html_content .= "<div class=\"sweeps_quick\" onclick='location.href=\"displaycards_quickpick.php\";' ></div>";
             $html_content .= "</div>";
             $html_content .= "</div>";

             $i = 0;
             for($i = 0; $i < 10; $i++)
             {
                 if($i == 4)
                 {
                     $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' onclick='reveal_cards($i);' class=\"win_container\"><div class=\"flipCard\"></div></div></div>";
                 }
                 else if($i == 9)
                 {
                     $html_content .= "<div class=\"sweepsContainer2\"><div id='.$i.' onclick='reveal_cards($i);' class=\"win_container\"><div class=\"flipCard\"></div></div></div>";
                 }
                 else
                 {
                     $html_content .= "<div class=\"sweepsContainer\"><div id='.$i.' onclick='reveal_cards($i);' class=\"win_container\"><div class=\"flipCard\"></div></div></div>";
                 }
             }

             $html_content .= "<div class=\"sweeps_quickContainer\">";
             $html_content .= "<div class=\"sweeps_links\" >";
             $html_content .= "<div id=\"view_summary\" class=\"viewSum\" onclick='popup(\"popUpDivOpenedCards\"); view_opened_cards();' style='visibility: hidden; cursor: pointer; width: 250px;'>VIEW CARDS SUMMARY</div>";
             $html_content .= "</div>";
             $html_content .= "<div class=\"sweeps_links2\" >";
             $html_content .= "<div id=\"view_next_set\" class=\"viewNext\" style=\"\"  onclick='javascript:location.reload(true);'>$paginate</div>";
             $html_content .= "</div>";
             $html_content .= "</div>";
 
}


?>