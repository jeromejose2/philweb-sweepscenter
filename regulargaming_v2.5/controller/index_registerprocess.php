<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 08, 2012
 */
include('../init.inc.php');

App::LoadControl("TextBox");
App::LoadControl("Button");

$register_form = new FormsProcessor();

$txtuser = new TextBox("txtuser","txtuser","Username");
$txtuser->CssClass = "inputBoxEffect";

$txtpass = new TextBox("txtpass","txtpass","Password");
$txtpass->CssClass ="inputBoxEffect";
$txtpass->Password = true;

$btnLogin = new Button("btnLogin","btnLogin"," ");
$btnLogin->CssClass = "login2";

$register_form->AddControl($txtuser);
$register_form->AddControl($txtpass);
$register_form->AddControl($btnLogin);

$register_form->ProcessForms();
if($register_form->IsPostBack)
{
    if($btnLogin->SubmittedValue == " ")
    {
        
    }
}
?>