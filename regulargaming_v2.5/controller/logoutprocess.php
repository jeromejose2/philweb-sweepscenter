<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 20, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");
App::LoadModuleClass("SweepsCenter", "SCC_Terminals");

$cwinningsdump = new SCC_WinningsDump();
$cterminals = new SCC_Terminals();
$id = $_SESSION['id'];

$cwinningsdump->StartTransaction();
$cwinningsdump->UpdateIsFinalizedStatus($id);
if($cwinningsdump->HasError)
{
    $cwinningsdump->RollBackTransaction();
}
else
{
  $cwinningsdump->CommitTransaction();
    $cterminals->StartTransaction();
                $cterminals->UpdateIsPlayableStatus($id,1);
                if($cterminals->HasError)
                {
                    $cterminals->RollBackTransaction();
                }
                else
                {
                    $cterminals->CommitTransaction();
                    header("location: launchpad.php");
                }
}


?>