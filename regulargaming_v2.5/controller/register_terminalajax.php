<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 19, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadLibrary("class.rc4crypt.php");

$cterminals = new SCC_Terminals();
$user = $_GET['usr'];
$passwd = $_GET['pwd'];
$mac_address = $_GET['macadd'];
$ip = $_SERVER['REMOTE_ADDR'];

$terminaldtls = $cterminals->SelectByNameAndPassword($user,$passwd);

if(count($terminaldtls) > 0)
{
    $pwd = '23780984';
    $e = rc4crypt::encrypt($pwd, $passwd);
    $encrpt = bin2hex($e);

    $cterminals->StartTransaction();
    $cterminals->UpdateMacAddress($mac_address,$encrpt,$user);
    if($cterminals->HasError)
    {
        $cterminals->RollBackTransaction();
        echo json_encode(array("msg1" => "Terminal registration failed."));
    }
    else
    {
        $cterminals->CommitTransaction();
        echo json_encode(array("msg1" => "Terminal registration successful"));
    }
}
else
{
    echo json_encode(array("msg1" => "Username/Password is invalid. Please try again."));
}
?>