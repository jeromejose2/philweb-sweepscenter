<div class="menutitle" onclick="SwitchMenu('sub1')">
Live Deck Management
</div>
<span class="submenu" id="sub1">
<?php if ($accttype == 1 || $accttype == 3): ?>
   <?php if ($pagename == "deckcounter"): ?>
      <a href="template.php?page=deckcounter" class="linkmenuactive">Deck Counter</a><br />
   <?php else: ?>
      <a href="template.php?page=deckcounter" class="linkmenu">Deck Counter</a><br />
   <?php endif; ?>
<?php endif;?>

<?php if ($pagename == "prizecounter" || $accttype == 3): ?>
   <a href="template.php?page=prizecounter" class="linkmenuactive">Prize Counter</a><br />
<?php else: ?>
   <a href="template.php?page=prizecounter" class="linkmenu">Prize Counter</a><br />
<?php endif;?>
</span>

<div class="menutitle" onclick="SwitchMenu('sub2')">
Queued Deck Management
</div>
<span class="submenu" id="sub2">
<?php if ($accttype == 1): ?>
   <?php if ($pagename == "queueddeck"): ?>
      <a href="template.php?page=queueddeck" class="linkmenuactive">Queued Decks</a><br />
   <?php else: ?>
      <a href="template.php?page=queueddeck" class="linkmenu">Queued Decks</a><br />
   <?php endif; ?>
    <?php if ($pagename == "generatedeck"): ?>
       <a href="template.php?page=generatedeck" class="linkmenuactive">Generate New Deck</a><br />
    <?php else: ?>
       <a href="template.php?page=generatedeck" class="linkmenu">Generate New Deck</a><br />
    <?php endif; ?>
<?php endif;?>
</span>

<div class="menutitle" onclick="SwitchMenu('sub3')">
Deck History
</div>
<span class="submenu" id="sub3">
<?php if ($accttype == 1 || $accttype == 3): ?>
   <?php if ($pagename == "rptdeckusage"): ?>
      <a href="template.php?page=rptdeckusage" class="linkmenuactive">Deck Usage</a><br />
   <?php else: ?>
      <a href="template.php?page=rptdeckusage" class="linkmenu">Deck Usage</a><br />
   <?php endif;?>

   <?php if ($pagename == "rptecn" || $accttype == 3): ?>
      <a href="template.php?page=rptecn" class="linkmenuactive">ECN Report</a><br />
   <?php else: ?>
      <a href="template.php?page=rptecn" class="linkmenu">ECN Report</a><br />
   <?php endif;?>
<?php endif; ?>
</span>

<div class="menutitle" onclick="SwitchMenu('sub4')">
Account Management
</div>
<span class="submenu" id="sub4">
<?php if ($accttype == 1): ?>
   <?php if ($pagename == "usercreate"): ?>
      <a href="template.php?page=usercreate" class="linkmenuactive">Create User Account</a><br />
   <?php else:?>
      <a href="template.php?page=usercreate" class="linkmenu">Create User Account</a><br />
   <?php endif; ?>
<?php endif;?>
   <?php if ($pagename == "changepassword"): ?>
      <a href="template.php?page=changepassword" class="linkmenuactive">Change Password</a><br />
   <?php else: ?>
      <a href="template.php?page=changepassword" class="linkmenu">Change Password</a><br />
   <?php endif;?>
</span>
