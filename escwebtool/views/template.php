<?php 
/*
 * Created By: Noel Antonio
 * Date Modified: May 8, 2012
 * MANAGESESSION CONTROLLER
 * Purpose: Managing Sessions
 */

//error_reporting(E_ALL);
require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCC_Accounts");
App::LoadModuleClass($modulename, "SCCC_AccountSessions");
App::LoadModuleClass($modulename, "SCCC_AuditLog");

$scaccounts = new SCCC_Accounts();
$scacctsessions = new SCCC_AccountSessions();
$scauditlog = new SCCC_AuditLog();

if (isset($_SESSION['sid']))
{
    $currtime = '';
    $timestart = '';
    $id = '';
    $aid = '';
    
    $scacctsessions->StartTransaction();
    $scaccounts->StartTransaction();
    
    // Get the Session Details
    $sessiondtls = $scacctsessions->GetSessionDetails($_SESSION['sid']);
    if (count($sessiondtls) > 0)
    {
        $currtime = $sessiondtls[0]["CurrTime"];
        $timestart = $sessiondtls[0]["TransDate"];
        $id = $sessiondtls[0]["ID"];
        $aid = $sessiondtls[0]["AccountID"];
    }
    
    // Get the User Details
    $userdetails = $scaccounts->SelectByID($aid);
    $_SESSION["username"] = $userdetails[0]["Username"];
    
    // Check for SESSION TIMEOUT
    $timediff = (strtotime($currtime) - strtotime($timestart)) / 60;
    if($timediff >= 20) 
    {
        $updateSession["ID"] = $id;
        $updateSession["DateEnd"] = 'now_usec()';
        $scacctsessions->UpdateByArray($updateSession);
        if ($scacctsessions->HasError)
        {
            $errormsg = $scacctsessions->getError();
            $errormsgtitle = "ERROR!";
            $scacctsessions->RollBackTransaction();
        }

        //log to audit trail
        $scauditlog->StartTransaction();

        $scauditlogparam["SessionID"] = $_SESSION['sid'];
        $scauditlogparam["AccountID"] = $_SESSION['aid'];
        $scauditlogparam["TransDetails"] = "Login Account ID: ".$_SESSION['aid'];
        $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
        $scauditlogparam["TransDateTime"] = "now_usec()";
        $scauditlog->Insert($scauditlogparam);
        if ($scauditlog->HasError)
        {
            $errormsg = $scauditlog->getError();
            $errormsgtitle = "ERROR!";
            $scauditlog->RollBackTransaction();
        }

        $scauditlog->CommitTransaction();
        $scacctsessions->CommitTransaction(); 
        
        session_destroy();
        URL::Redirect('login.php');
    }
    else 
    {
        $updateSession["ID"] = $id;
        $updateSession["TransDate"] = 'now_usec()';
        $scacctsessions->UpdateByArray($updateSession);
        if ($scacctsessions->HasError)
        {
            $errormsg = $scacctsessions->getError();
            $errormsgtitle = "ERROR!";
            $scacctsessions->RollBackTransaction();
        }
        $scacctsessions->CommitTransaction(); 
    }
} else {
    URL::Redirect('login.php');
}

$serverdtls = $scacctsessions->GetServerDateTime();
$servertime = $serverdtls[0][0];
$serverdate = $serverdtls[0][1];
?>
<html>
  <head>
  <script type="text/javascript">
  function preventBackandForward()
  {
     window.history.forward();
  }

  preventBackandForward();
  window.inhibited_load=preventBackandForward;
  window.onpageshow=function(evt){if(evt.persisted)preventBackandForward();};
  window.inhibited_unload=function(){void(0);};
  </script> 
  <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
  <title>Card Management Center</title>
  </head>
  <body onload="updateClock(); setInterval('updateClock()', 1000); "
    <?php if ($page = 'activesession') {?>
      timedRefresh(30000);<?php }?>" style="background-color: #FFFFFF;">

      <form id="frmTemplate" name="frmTemplate" method="post">

	<div id="containertemplate">
	<div id="fade" class="black_overlay"></div>	
	  <div id="logo"></div>
	  <div id="datetime">
	    <div class="labelbold">
	      Welcome <?php echo $_SESSION['username']; ?>!
	    </div>
	    <div>
	      <b>DATE:</b>&nbsp;<span id="servdate" name="servdate">&nbsp;</span>
	    </div>
	    <div>
	      <b>TIME:</b>&nbsp;<span id="clock">&nbsp;</span>
	    </div>
	  </div>
	  <table id="menu_table">
	    <tr>
	      <td>
	        <div id="menu">
	          <?php
	                include_once('menu.php');
	          ?>
	        </div>
	      </td>
	      <td>
	        <div id="maincontent" > 
	
	          <?php
	          // create an array of allowed pages
	          $allowedPages = array(
                        'addprize',                     /*KFG*/
                        'addprizetype',                 /*KFG*/
                        'createproject',                /*KFG*/
                        'redemption',			/*KFG*/
                        'rptsales',                     /*KFG*/
                        'rptbet',                       /*KFG*/
                        'rptwinners',                   /*KFG*/
                        'generatedeck',			/*KFG*/
                        'deckcounter',			/*ESC*/
                        'prizecounter',			/*ESC*/
                        'queueddeck',                   /*ESC*/
                        'generatedeck',			/*ESC*/
                        'rptdeckusage',			/*ESC*/
                        'rptecn',			/*ESC*/
                        'createuser',			/*ESC*/
                        'changepassword',		/*ESC*/
                        'usercreate',			/*ESC & KFG*/
                        'userform'			/*ESC & KFG*/
		  	);
	          
	          // check if the page variable is set and check if it is in the array of allowed pages
	          if(isset($_GET['page']) && in_array($_GET['page'], $allowedPages))
	            {
	            // check that the file exists
	            if(file_exists($_GET['page'].'.php'))
	                {
	                // we include the file
	                include_once($_GET['page'].'.php');
	                }
	            else
	                {
	                // if the file does not exist
	                echo 'Requested page does not exist.';
	                }
	            }
	          // if incorrect url
	          else
	            {
	            // if things are not as they should be, we included the default page
	            if(!file_exists('index.php'))
                    {
                        // if the default page is missing
                        echo 'index.php is missing.';
                    }
	            }
	        ?>
	        </div>
	
	      </td>
	    </tr>
	  </table>

      </div>
      <script type="text/javascript">
	<!--
	var tempDate = "<?php echo $serverdate; ?>";
	var tempTime = "<?php echo $servertime; ?>";
	document.getElementById("servdate").firstChild.nodeValue = tempDate;

	function updateClock ( )
	{
	  var currentTime = new Date ();
	  //var currentHours = currentTime.getHours ( );
	  //var currentMinutes = currentTime.getMinutes ( );
	  var currentSeconds = currentTime.getSeconds ( );
	  
	  var currentHours = tempTime.substring(11,13);
	  var currentMinutes = tempTime.substring(14,16);
	  //var currentSeconds = tempTime.substring(17,19);

	  // Pad the minutes and seconds with leading zeros, if required
	  currentMinutes = ( currentMinutes < 10 ? "" : "" ) + currentMinutes;
	  currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

	  // Choose either "AM" or "PM" as appropriate
	  var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

	  // Convert the hours component to 12-hour format if needed
	  currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

	  // Convert an hours component of "0" to "12"
	  currentHours = ( currentHours == 0 ) ? 12 : currentHours;

	  // Compose the string for display
	  var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;

	  // Update the time display
	  document.getElementById("clock").firstChild.nodeValue = currentTimeString;
	  
	}

	function timedRefresh(timeoutPeriod)
	{
	  setTimeout("location.reload(true);",timeoutPeriod);
	}    
	// -->
      </script>
    </form>
  </body>

</html>
<script>
$(function () {
    $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
});
</script>

