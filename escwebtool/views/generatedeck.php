<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 4, 2012
 * GENERATE DECK PAGE
 * Purpose: For creating new decks.
 */
include("../controller/generatedeckprocess.php");
?>
<script type="text/javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript" src="jscripts/validations.js"></script>
<script language="javascript" src="jscripts/lightbox.js"></script>
<script type="text/javascript" language="Javascript">
    function showAddPrizeType()
    {
        document.getElementById('title1').innerHTML = "Add New Prize Type";
        document.getElementById('msg1').innerHTML = "Please enter a new prize type";
        document.getElementById('txtNewPrizeType').value = '';
        document.getElementById('errormessage').style.display = 'none';
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    function checkprizetypeinput()
    {
      if (document.getElementById("txtNewPrizeType").value.length == 0)
      {
          document.getElementById('errormessage').style.display = 'block';
          $('#errormessage').html('Please enter at least one new prize type.');
          return false;
      }
      return true;
    }
    function checkprizevalueinput()
    {
        var value = document.getElementById('txtNewValue').value;
        if ((value == '') || ($('#rdoEnable').is(':checked') && $('#txtValueQty').val() == ''))
        {
            document.getElementById('title').innerHTML = "Notification";
            document.getElementById('msg').innerHTML = "Please fill up all required fields.";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
        return true;
    }
    
    jQuery(document).ready(function(){
        jQuery('#btnAddPrizeType').click(function(){
                showAddPrizeType();
        });
        jQuery('#btnAddPrize').live('click',function(){
            jQuery('#tdaddmenu').hide();
            jQuery('#tdsavemenu').show();
            jQuery('#tdsavemenu2').show();
        });
        jQuery('#btnCancel').click(function(){
            jQuery('#tdaddmenu').show();
            jQuery('#tdsavemenu').hide();
            jQuery('#tdsavemenu2').hide();
            document.forms[0].submit();
        }); 

        jQuery('#txtValueQty').attr('disabled','disabled');
        jQuery('#rdoEnable').live('click',function(){ jQuery('#txtValueQty').removeAttr('disabled'); });
        jQuery('#rdoDisable').live('click',function(){ jQuery('#txtValueQty').attr('disabled','disabled'); });
        
        jQuery('.txtQty').attr('disabled','disabled');
        jQuery('.enable').live('click',function(){ jQuery(this).parents('tr').children('.quantity').children('input').removeAttr('disabled'); });
        jQuery('.disabled').live('click',function(){ jQuery(this).parents('tr').children('.quantity').children('input').attr('disabled','disabled'); });

        jQuery('#btnSaveGenNewDeck').live('click',function(){
            var data = jQuery(this).parents('form').serialize();
            var url = jQuery('#requestpage').val();
            loading();
            $.ajax({
                url : url,
                type : 'post',
                data : data,
                success : function(callback){
                    jQuery('.white_content2').html(callback);
                },
                error : function(e){
                    jQuery('.white_content2').html(e.responseText);
                    hideLightBox();
                }
            });
            return false;
        });
});
</script>
<div style="margin:10px;">
    <table style="border-color:#FFFFFF;">
        <tr><td colspan="2" class="moduletitle">Generate New Deck</td></tr>
        <tr class="fontboldblack2">
            <td>&nbsp;Guam Sweepstakes Inc. Representative:</td><td><?php echo $txtRep; ?></td>
        </tr>
        <tr class="fontboldblack2">
            <td>&nbsp;Deck Size:</td><td><?php echo $txtDeckSize; ?></td>
        </tr>
        <tr class="fontboldblack2">
            <td>&nbsp;Payout Budget:</td><td><?php echo $txtBudget; ?></td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr class="fontboldblack2" style="text-align:center;">
                <td>Value</td>
                <td>Prize Type</td>
                <td>Quantity</td>
                <td>Action</td>
              </tr>
              <?php for ($i = 0; $i < count($list); $i++) { ?>
              <tr class="fontboldblack2" style="text-align:center;">
                  <td><?php echo $list[$i]["PrizeName"]; ?></td>
                  <td><?php echo $list[$i]["PrizeDescription"]; ?></td>
                  <td class="quantity"><input class="txtQty" id="<?php echo "txtQty".$i; ?>" autocomplete="off" maxlength="10" onkeypress="javascript: return isnumberkey(event);"></td>
                  <td>
                      <input class="enable" type="radio" id="<?php echo "rdoAction".$i ;?>" name="<?php echo "rdoAction".$i ;?>" value="Enable"/>Enable
                      <input class="disabled" type="radio" id="<?php echo "rdoAction".$i ;?>" name="<?php echo "rdoAction".$i ;?>" value="Disable" checked="true" />Disable
                      <input type="hidden" id="<?php echo "hidDesc".$i; ?>" value="<?php echo $list[$i]["PrizeDescription"];?>" />
                      <input type="hidden" id="<?php echo "hidPrize".$i;?>" name="<?php echo "hidPrize".$i;?>" value="<?php echo $list[$i]["PrizeType"]; ?>" />
                      <input type="hidden" id="<?php echo "hidAmt".$i;?>" name="<?php echo "hidAmt".$i;?>" value="<?php echo $list[$i]["Denomination"]; ?>">
                  </td>
              </tr>
              <?php } $ctr = $i; ?>
              <tr id="tdsavemenu" class="fontboldblack2" style="display:none; text-align: center;">
                <td><?php echo $txtNewValue; ?></td>
                <td><?php echo $ddlPrizeTypes; ?></td>
                <td><?php echo $txtValueQty; ?></td>
                <td><input type="radio" id="rdoEnable" name="rdoAction" value="Enable">Enable</input>
                <input type="radio" checked="true" id="rdoDisable" name="rdoAction" value="Disable">Disable</input></td>
              </tr>
              <tr>
                <td colspan="4" align="center">
                    <div id="tdaddmenu">
                        <?php echo $btnAddPrizeType; ?>
                        <?php echo $btnAddPrize; ?>
                        <?php echo $btnGenerate; ?>
                    </div>
                    <div id="tdsavemenu2" style="display: none;">
                        <?php echo $btnSavePrize; ?>
                        <input type="button" id="btnCancel" name="btnCancel" value="CANCEL" class="labelbutton2" />
                    </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    <input type="hidden" id="requestpage" />
      
    <!-- POP UP FOR NEW PRIZE TYPE -->
    <div id="fade" class="black_overlay"></div>
    
    <div id="light" style="text-align: center;font-size: 16pt;height: auto;margin-left: auto;margin-top: 10%;" class="white_content2">
    <div style="width: 100%;height: 27px;background-color: black;top: 0px;color: white;padding-top: 5px;">
        <b id="a"><div id="title"></div></b>
    </div>
    <p id="b"><br/><div id="msg"></div></p><br/>
    <input type="button" value="OKAY" class="labelbutton2" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" />      
    </div>
    
    <div id="light2" style="text-align: center;font-size: 16pt;height: auto;margin-left: auto;margin-top: 10%;" class="white_content2">
    <div style="width: 100%;height: 27px;background-color: black;top: 0px;color: white;padding-top: 5px;">
        <b id="a"><div id="title2"></div></b>
    </div>
    <p id="b"><br/><div id="msg2"></div></p><br/>
    <input type="button" value="OKAY" class="labelbutton2" onclick="document.forms[0].submit();" />      
    </div>

    <div id="light1" style="text-align: center;font-size: 16pt;height: auto;margin-left: auto;" class="white_content2">
        
        <div style="width: 100%;height: 27px;background-color: black;top: 0px;color: white;padding-top: 5px;">
            <div id="title1"></div><br/>
        </div>
        <div id="errormessage" style="color:red; text-align:center"></div>
        <div id="msg1" class="fontboldblack2"></div>
        <div class="fontboldblack2"><?php echo $txtNewPrizeType; ?></div>
        <?php echo $btnSavePrizeType; ?>
        <input type="button" value="CANCEL" class="labelbutton2" style="font-size: 0.8em;" onclick="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';" />      
    </div>
    <!-- POP UP FOR NEW PRIZE TYPE  -->
      
<script type="text/javascript">
    
    <?php if (isset($errormsg)): ?>
        document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
        document.getElementById('msg').innerHTML = "<?php echo $errormsg; ?>";
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
    <?php endif; ?>
        
    <?php if (isset($successmsg)): ?>
        document.getElementById('title2').innerHTML = "<?php echo $successmsgtitle; ?>";
        document.getElementById('msg2').innerHTML = "<?php echo $successmsg; ?>";
        document.getElementById('light2').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
    <?php endif;?>
        
jQuery(document).ready(function(){
   function process(prizeid, prizes, prizename, quantities, ctr) {
      
      var url = '../controller/popupgeneratedeckconfirm.php?decksize=' + document.getElementById("txtDeckSize").value +
                '&elementid=' + prizeid + '&prizes=' + prizes + '&quantities=' + quantities + '&prizename=' + prizename +
                '&ctr=' + ctr + '&rep=' + document.getElementById("txtRep").value + '&proj=' + <?php echo $_SESSION['projectid'];?>;
      jQuery('#requestpage').val(url);
      callAjax(url,'','get','Add New Prize Type');
   }
   
    jQuery('#btnGenerate').live('click',function(){
        checkinput();
    });

    function checkinput() {
        var rep = document.getElementById('txtRep').value;
        var decksize = document.getElementById('txtDeckSize').value;
        var budget = document.getElementById('txtBudget').value;

        if (rep.length == 0)
        {
            document.getElementById('title').innerHTML = "Notification";
            document.getElementById('msg').innerHTML = "Please enter the representative's name.";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            jQuery('#txtRep').css('border','solid 1px red');
            return false;
        }
        if (decksize == '')
        {
            document.getElementById('title').innerHTML = "Notification";
            document.getElementById('msg').innerHTML = "Please enter the deck size.";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            jQuery('#txtDeckSize').css('border','solid 1px red');
            return false;
        }
        if(decksize < 10000 || decksize > 1000000) 
        {
            document.getElementById('title').innerHTML = "Notification";
            document.getElementById('msg').innerHTML = "Deck Size should be between 10,000 and 1,000,000.";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
        if (budget.length == 0)
        {
            document.getElementById('title').innerHTML = "Notification";
            document.getElementById('msg').innerHTML = "Please enter the payout budget.";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            jQuery('#txtBudget').css('border','solid 1px red');
            return false;
        }
        
        jQuery('#btnQueuedDeck').live('click',function(){
          jQuery('#frmTemplate').attr('action','template.php?page=queueddeck');
          jQuery('#frmTemplate').attr('method','get');
          jQuery('#frmTemplate').submit();
          return false;
       });
        
        var recCtr = "<?php echo $ctr; ?>";
        var ctr = 0;
        var actions = "";
        var prizeid = "";
        var quantities = "";
        var prizes = "";
        var prizename = "";
        var rbId = "";                
        var txtId = "";               
        var lblId = "";  
        var hidAmt = "";
        var hidDesc = "";
        var cnt = 0;
        var total = 0;
        var myCounter = 0;
        
        $('.enable').each(function(){
            if($(this).is(':checked')) {
                myCounter++;
            }
        });
        if(myCounter == 0) {
            document.getElementById('title').innerHTML = "Notification";
            document.getElementById('msg').innerHTML = "Please enable prizes to be generated.";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }

        for (x = 0; x < recCtr; x++) {
            rbId = "rdoAction" + x;
            txtId = "txtQty" + x;
            lblId = "hidPrize" + x;
            hidDesc = "hidDesc" + x;
            hidAmt = "hidAmt" + x;
            
            if (document.getElementById(rbId).checked == true) {
                if (document.getElementById(txtId).value.length == 0) {
                    document.getElementById('title').innerHTML = "Notification";
                    document.getElementById('msg').innerHTML = "Please enter the quantity.";
                    document.getElementById('light').style.display = 'block';
                    document.getElementById('fade').style.display = 'block';
                    document.getElementById(txtId).focus();
                    return false;
                }
                actions = actions + document.getElementById(rbId).checked + ",";
                prizeid = prizeid + document.getElementById(lblId).value + ",";
                prizename = prizename +  document.getElementById(hidDesc).value + ",";
                prizes = prizes +  document.getElementById(hidAmt).value + ",";
                quantities = quantities + document.getElementById(txtId).value + ",";
                total = total + (document.getElementById(hidAmt).value * document.getElementById(txtId).value);
                cnt = cnt + eval(document.getElementById(txtId).value);
                ctr = ctr + 1;
            }
            if (total > document.getElementById("txtBudget").value) {
                document.getElementById('title').innerHTML = "Notification";
                document.getElementById('msg').innerHTML = "The total amount of payout you have set for the corresponding prizes has exceeded from the payout budget.";
                document.getElementById('light').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
                return false;
            }
        }
        
        if (total != document.getElementById("txtBudget").value) {
            document.getElementById('title').innerHTML = "Notification";
            document.getElementById('msg').innerHTML = "Total amount of payout is not equal with the payout budget.";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }

        if (cnt != document.getElementById("txtDeckSize").value) {
            document.getElementById('title').innerHTML = "Notification";
            document.getElementById('msg').innerHTML = "Total prize quantity is not the same as the deck size.";
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
      process(prizeid,prizes,prizename,quantities,ctr);
   }
});
</script>
</div>
<script>
$(function () {
    $('input[type=text],[type=password],[class=txtQty]').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
});
</script>