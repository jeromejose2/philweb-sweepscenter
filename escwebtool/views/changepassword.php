<?php
 /***********************************
     * Creation Date: May 7, 2012
     * Created by : Jfj     
     * Description: Viewing Change Password
     **********************************/
include("../init.inc.php");
include("../controller/changepasswordprocess.php");
?> 
<script language="javascript" src="jscripts/validations.js"></script>
<script type="text/javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="jscripts/lightbox.js"></script>
 <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
 
 <form id="frmchangepassword" name="frmchangepassword" action="changepassword.php"  method="post" onsubmit="">  
  <!-- POP UP FOR MESSAGES -->
  <div id="light" style="text-align: center;font-size: 16pt;height: auto;margin-left: auto;" class="white_content2">
    <div style="width: 100%;height: 27px;background-color: black;top: 0px;color: white;padding-top: 5px;">
      <b id="a"><div id="title"></div></b>
    </div>
    <p id="b">
        <br/><div id="msg"></div>
    </p>
    <br/>
    <?php echo $btnOk; ?>    
  </div>
  <div id="fade" class="black_overlay2"></div>

  <div id="light1" style="text-align: center;font-size: 16pt;height: auto;margin-left: auto;" class="white_content2">
    <div style="width: 100%;height: 27px;background-color: black;top: 0px;color: white;padding-top: 5px;">
      <b id="a"><div id="title1"></div></b>
    </div>
    <p id="b">
        <br/><div id="msg1"></div>
    </p>
    <br/>
    <?php echo $btnYes; ?>   <?php echo $btnNo; ?>  
  </div>
  <div id="fade" class="black_overlay2"></div>

   <div id="light2" style="text-align: center;font-size: 16pt;height: auto;margin-left: auto;" class="white_content2">
   <div style="width: 100%;height: 27px;background-color: black;top: 0px;color: white;padding-top: 5px;">
   <b id="a"><div id="title2"></div></b></div>
   <p id="b"><br/><div id="msg2"></div></p>
   <br/><?php echo $btnConfirm;?>
   </div>  <div id="fade" class="black_overlay2"></div>
<!-- POP UP FOR MESSAGES -->

<div>
   <table style="border-color:#FFFFFF;">
    <tr>
     <td colspan="2" style="background-color: #000000; color:#FFFFFF; font-weight:bold; font-size:1.2em; text-align:center; height:30px; width:800px;">
            Fill out this form to change your password.
          </td>
    </tr>
             <tr class="fontboldblack2" style="font-weight: bolder">
            <td align="center">Current Password :</td><td>&nbsp;&nbsp;<?php echo $txtpassword; ?></td>
          </tr>
           <tr class="fontboldblack2" style="font-weight: bolder">
            <td align="center">New Password :</td><td>&nbsp;&nbsp;<?php echo $txtnewpassword; ?></td>
          </tr>
           <tr class="fontboldblack2" style="font-weight: bolder">
            <td align="center">Confirm New Password :</td><td>&nbsp;&nbsp;<?php echo $txtcnewpassword; ?></td>
          </tr>
    <tr>
        <td>
            &nbsp;&nbsp;
        </td>
    </tr>
   
    <tr>
      <td class="fontboldblack">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <?php echo $btnSubmit;?></td>
      <td Class="fontboldblack">&nbsp;&nbsp;
                      <?php // echo $btnCancel;?>
                <input id="cancel" type="reset" value="Cancel" onclick="javascript: location.href='template.php?page=changepassword';" class="labelbutton2" style="width : 200px"></input>
      </td>
    </tr>
  </table>
</div>
<?php if(isset($errorr_msg)):?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $errorr_title;?>";
    document.getElementById('msg').innerHTML = "<?php echo $errorr_msg;?>";
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif; ?>
 
<?php if(isset($confirm_msg)):?>
<script>
    document.getElementById('title1').innerHTML = "Confirmation";
    document.getElementById('msg1').innerHTML = "<?php echo $confirm_msg;?> ";
    document.getElementById('light1').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif;?>

<?php if(isset($createuser_title)):?>
<script>
    document.getElementById('title2').innerHTML = "<?php echo $createuser_title;?>";
    document.getElementById('msg2').innerHTML = "<?php echo $createuser_msg;?>";
    document.getElementById('light2').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif;?>
 </form>
 
<script language="javascript">
  <?php
  if ($isprocessed == 1)
  {
  ?>
  result();
  <?php
  }
  ?>
  
  function result()
  {
    var resid = "<?php echo (isset($resultid)) ? $resultid : ""; ?>";
    var resmsg = "<?php echo (isset($msg)) ? $msg : ""; ?>";
    var title = "";
    
    if (resid == 0)
    {
      title = "Password Changed!";
    }
    else
    {
      title = "Invalid Password";
    }
    /*document.getElementById('title3').innerHTML = title;
    document.getElementById('msg3').innerHTML = resmsg;
    document.getElementById('light3').style.display='block';
    document.getElementById('fade').style.display='block';*/
    showLightBox('<h1>'+resmsg+'</h1><input  id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />',title);	
    return false;
  }
   function disableSpace(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 32)
        return false;

    return true;
}

function DisableSpecialCharacters(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if((charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;

    return true;
}
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

    return true;
}

function isAlphaKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ((charCode > 32 && charCode <65) || (charCode < 97 && charCode > 90) || (charCode < 128 && charCode > 122))
        return false;

    return true;
}
</script>
<script>
$(function () {
    $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
});
</script>

