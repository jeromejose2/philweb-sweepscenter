
function showLightBox(html,title)
{
   var e = document.getElementById('frmTemplate');
   var conheight = e.offsetHeight;
   jQuery('body').append('<div style="" class="black_overlay"></div>');
   jQuery('.black_overlay').css({'height':conheight});
   jQuery('.black_overlay').show(); 
   var inner = '<div id="light" class="white_content2"></div>';
   //html = inner + html;
   jQuery('#frmTemplate').append(inner);
   html = '<div class="lightbox_title"><h1>' + title + '</h1></div>' + html;
   jQuery('.white_content2').html(html);
   jQuery('.white_content2').show();
   jQuery('#alertOk').focus();
   /*var e = document.getElementById('frmTemplate');
   var conheight = e.offsetHeight;
   document.getElementById('fade').style.height = conheight;
   $('#frmTemplate').append('<div id="light" class="white_content"></div>');
   $('.white_content').html(html);

   $('#fade').show();
   $('.white_content').show();*/
}

function showLightBox1(html,title)
{
   var e = document.getElementById('frmTemplate');
   var conheight = e.offsetHeight;
   jQuery('body').append('<div style="" class="black_overlay"></div>');
   jQuery('.black_overlay').css({'height':conheight});
   jQuery('.black_overlay').show(); 
   var inner = '<div id="light" class="white_content2"></div>';
   jQuery('#frmTemplate').append(inner);
   html = '<div class="lightbox_title"><h2>' + title + '</h2></div><br/>' + html;
   jQuery('.white_content2').html(html);
   jQuery('#light').show();
   jQuery('#alertOk').focus();
}


function hideLightBox()
{
   $('.white_content2').fadeOut('slow',function(){
      $('.white_content2').remove();
   });
   $('.black_overlay').fadeOut('slow',function(){
      $('.black_overlay').remove();
   });
   
   //document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';
}

function loading()
{
   $('.white_content2').remove();
   $('.black_overlay').remove();
   var e = document.getElementById('frmTemplate');
   var conheight = e.offsetHeight;
   jQuery('body').append('<div style="" class="black_overlay"></div>');
   jQuery('.black_overlay').css({'height':conheight});
   jQuery('.black_overlay').show(); 
   var inner = '<div id="light" class="white_content2"></div>';
   //html = inner + html;
   jQuery('#frmTemplate').append(inner);
   jQuery('.white_content2').html('<h1>Loading...</h1>');
   jQuery('.white_content2').show();
}

function callAjax(url,data,type,title)
{
   loading();
   $.ajax({
      url : url,
      data : data,
      success : function(callback){
         jQuery('.white_content2').html(callback);   
   
      },
      error : function(e){
         jQuery('.white_content2').html(e.responseText); 
         hideLightBox();      
      }
   });   
}

$('#btnOkay').live('click',function(){
   $('#light').fadeOut('slow',function(){
      $('#light').remove();
   });
   $('#fade').fadeOut('slow');
   $('.black_overlay').fadeOut('slow',function(){
      $('.black_overlay').remove();
   });
   return false;
});

/*
$(document).ready(function(){

   $('#acctname').live('change',function(){
      $('#acctnum').val($(this).val());
   })

   $('#acctnum').live('change',function(){
      $('#acctname').val($(this).val());
   });

});*/

jQuery(document).ready(function(){
   jQuery('#btnCancel').live('click',function(){
      hideLightBox();
      return false;
   })   

   jQuery('#alertOk').live('click',function(){
      hideLightBox();
      return false;
   });
});



function trim(str) {
   return jQuery.trim(str);
}
