/*Login credential checking CREATED ON: 07-25-2011*/
function checklogin()
{
    var uname = document.getElementById('txtUsername').value;
    var pword = document.getElementById('txtPassword').value;
    if(uname.trim() == "")
    {
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title').innerHTML = 'Insufficient Data';
        document.getElementById('msg').innerHTML = 'Please enter your username/password.';
        return false;
    }
    if(pword.length == 0)
    {
        document.getElementById('light').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title').innerHTML = 'Insufficient Data';
        document.getElementById('msg').innerHTML = 'Please enter your username/password.';
        return false;
    }
    return true;
}

function openforgotpw()
{
    document.getElementById('title2').innerHTML= 'Reset Password';
    document.getElementById('msg2').innerHTML = 'You are about to reset your current password. Please enter your registered e-mail address.';
    document.getElementById('light2').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    return false;
}

function redirect()
{
    var email = document.getElementById('txtEmail');
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(email.value)) 
    {
        document.getElementById('title2').innerHTML = "Invalid E-Mail";
        document.getElementById('msg2').innerHTML = "You have entered an invalid e-mail address. Please try again.";
        document.getElementById('txtEmail').value = "";
        document.getElementById('txtEmail').focus();
        document.getElementById('light2').style.display='block';
        document.getElementById('fade').style.display='block';
    return false;
    }
    return true;
}

function specialkeys(e)
{
    var k;
    document.all ? k = e.keyCode : k = e.which;
    return ((k >= 64 && k < 91) || (k > 96 && k < 123) || k == 8 || e.keyCode == 9 || k == 32 || k == 46 || (e.keyCode > 36 && e.keyCode <= 40) || (k >= 48 && k <= 57));
}

function alphanumeric(e)
{
    var k;
    document.all ? k = e.keyCode : k = e.which;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || e.keyCode == 9 || k == 32 || (e.keyCode > 36 && e.keyCode <= 40) || (k >= 48 && k <= 57));
}

function isnumberkey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
/*

Added by: Angelo Niño G. Cubos
Date created: November 14, 2012 
 
 */
function AlphaNumeric(event)//allowing white space
{
    var charCode = (event.which) ? event.which : event.keyCode;  
    if(/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))      
    return false;

    return true;
}
function AlphaNumericOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;  
    if(/*DisableWhiteSpaces*/(charCode == 32) ||/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))      
    return false;

    return true;
}
function AlphaOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;  
    if(/*DisableWhiteSpaces*/(charCode == 32) ||/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))      
    return false;
        var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
        // returns false if a numeric character has been entered
        return (chCode < 48 /* '0' */ || chCode > 57 /* '9' */ );
        return (chCode==32);

    return true;
}
function AlphaAndSpaces(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;  
    if(/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))      
    return false;
        var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
        // returns false if a numeric character has been entered
        return (chCode < 48 /* '0' */ || chCode > 57 /* '9' */ );
        return (chCode==32);

    return true;
}
function OnlyNumbers(event)
{
    var e = event || evt; // for trans-browser compatibility
    var charCode = e.which || e.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;

}
function verifyEmail(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode == 64)||(charCode == 95)||(charCode == 46))/* @=64, _=95, .=46 characters are allowed*/
  {  
    return true;
    return true;
  }else{
  if(/*DisableWhiteSpaces*/(charCode == 32) ||/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128)) 
    return false;
    return true;
  }
}
function DisableSpacesOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if(/*DisableSpaces*/(charCode == 32))
       
        return false;

    return true;
}

