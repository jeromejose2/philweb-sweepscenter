<?php
 /***********************************
     * Creation Date: May 7, 2012
     * Created by : Jfj     
     * Description: Viewing Change Password
     **********************************/
include("controller/changepasswordprocess.php");
?> 

 <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
 
<div style="margin:10px;">
   <table style="border-color:#FFFFFF;">
    <tr>
     <td colspan="2" style="background-color: #000000; color:#FFFFFF; font-weight:bold; font-size:1.2em; text-align:center; height:30px; width:800px;">
            Fill out this form to change your password.
          </td>
    </tr>
    <tr class="fontboldblack2">
      <td colspan="2"style="text-align:center;">&nbsp;&nbsp;<?php  echo $txtpassword;?></td>
    </tr>
    <tr class="fontboldblack2">
     <td colspan="2"style="text-align:center;">&nbsp;&nbsp;<?php  echo $txtnewpassword;?></td>
    </tr>
 <tr class="fontboldblack2">
     <td colspan="2"style="text-align:center;">&nbsp;&nbsp;<?php  echo $txtcnewpassword;?></td>
    </tr>
    <tr>
        <td>
            &nbsp;&nbsp;
        </td>
    </tr>
   
    <tr>
      <td class="fontboldblack">&nbsp;&nbsp;
          <?php echo $btnSubmit;?></td>
      <td Class="fontboldblack">&nbsp;&nbsp;
          <?php echo $btnCancel;?>
<!--        <a href="#" class="labelbutton2" onclick="javascript: return checkinput();"  style="text-decoration:none; color:#000; font-weight:bold;">SUBMIT</a>&nbsp;&nbsp;
		 <input type="submit" name="btnSubmit" id="btnSubmit" value="SUBMIT"  class="labelbutton2" onclick="javascript: return checkinput();" style="font-size:15px;width:auto;"/> 
        <a href="template.php?page=changepassword" class="labelbutton2" style="text-decoration:none; color:#000; font-weight:bold;">CANCEL</a>-->
      </td>
    </tr>
  </table>
</div>

<?php if(isset($confirm_msg)):?>
<script>
	process();
</script>
<?php endif;?>
<script language="javascript">
  <!--
  function checkinput()
  {
    //alert('test'); return false;
    var sCurrPass = document.getElementById("txtPassword");
    var sNewPass = document.getElementById("txtNewPassword");
    var sConfPass = document.getElementById("txtConfPassword");
	//var current_password = "<?php echo MD5($_SESSION['password']);?>";
	var current_password = "<?php echo $_SESSION['password'];?>";

    if (sCurrPass.value.length == 0)
    {
      /*var title = "Insufficient Data";
      var msg = "Please enter your current password to continue.";
      document.getElementById('title').innerHTML = title;
      document.getElementById('msg').innerHTML = msg;
      document.getElementById('light1').style.display='block';
      document.getElementById('fade').style.display='block';*/
      showLightBox('<h1>Please enter your current password to continue.</h1><input  id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />','Insufficient Data');
      return false;
    } 
    if (sCurrPass.value.length < 8 && sCurrPass.value.length > 0)
    {
      /*var title = "Insufficient Data";
      var msg = "Password must not be less than 8 characters.";
      document.getElementById('title').innerHTML = title;
      document.getElementById('msg').innerHTML = msg;
      document.getElementById('light1').style.display='block';
      document.getElementById('fade').style.display='block';*/
      showLightBox('<h1>Password must not be less than 8 characters.</h1><input  id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />','Insufficient Data');
      return false;
    } 
    if (sNewPass.value.length == 0)
    {
      /*var title = "Insufficient Data";
      var msg = "Please enter a new password to continue.";
      document.getElementById('title').innerHTML = title;
      document.getElementById('msg').innerHTML = msg;
      document.getElementById('light1').style.display='block';
      document.getElementById('fade').style.display='block';*/
      showLightBox('<h1>Please enter a new password to continue.</h1><input  id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />','Insufficient Data');
      return false;
    }
    if (sNewPass.value.length < 8 && sNewPass.value.length > 0)
    {
      /*var title = "Insufficient Data";
      var msg = "Password must not be less than 8 characters.";
      document.getElementById('title').innerHTML = title;
      document.getElementById('msg').innerHTML = msg;
      document.getElementById('light1').style.display='block';
      document.getElementById('fade').style.display='block';*/
      showLightBox('<h1>Password must not be less than 8 characters.</h1><input  id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />','Insufficient Data');
      return false;
    }
    if (sConfPass.value.length == 0)
    {
      /*var title = "Insufficient Data";
      var msg = "Please confirm new password.";
      document.getElementById('title').innerHTML = title;
      document.getElementById('msg').innerHTML = msg;
      document.getElementById('light1').style.display='block';
      document.getElementById('fade').style.display='block';*/
      showLightBox('<h1>Please confirm new password.</h1><input  id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />','Insufficient Data');
      return false;
    }
    if (sConfPass.value.length < 8 && sConfPass.value.length > 0)
    {
      /*var title = "Insufficient Data";
      var msg = "Password must not be less than 8 characters.";
      document.getElementById('title').innerHTML = title;
      document.getElementById('msg').innerHTML = msg;
      document.getElementById('light1').style.display='block';
      document.getElementById('fade').style.display='block';*/
      showLightBox('<h1>Password must not be less than 8 characters.</h1><input  id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />','Insufficient Data');
      return false;
    }
    if (sNewPass.value != sConfPass.value)
    {
      /*var title = "Invalid Password";
      var msg = "The new passwords you entered do not match. Please try again.";
      document.getElementById('title').innerHTML = title;
      document.getElementById('msg').innerHTML = msg;
      document.getElementById('light1').style.display='block';
      document.getElementById('fade').style.display='block';*/
      showLightBox('<h1>The new passwords you entered do not match. Please try again.</h1><input  id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />','Insufficient Data');
      return false;
    }
	if(sCurrPass.value != current_password)
	{
		showLightBox('<h1>The current password you entered is invalid. Please try again.</h1><input  id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />','Insufficient Data');
      return false;
	}
	if(sNewPass.value == current_password)
	{
		showLightBox('<h1>New password cannot be similar to current password.</h1><input  id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />','Insufficient Data');
      return false;
	}
    
    process();
    return true;
  }
  
  function process()
  {
      /*var title = "Confirmation";
      var msg = "You are about to change your current password. Do you wish to continue?";
      document.getElementById('title2').innerHTML = title;
      document.getElementById('msg2').innerHTML = msg;
      document.getElementById('light2').style.display='block';
      document.getElementById('fade').style.display='block';*/
      showLightBox('<h1>You are about to change your current password. Do you wish to continue?</h1><input name="btnOkay" type="submit" value="OKAY" class="labelbutton2" class="labelbold2"></input>&nbsp;&nbsp;&nbsp;<input  id="alertOk" type="button" value="CANCEL" class="labelbutton2 okrepresentative" />','Confirmation');
      return false;
  }
  
  <?php
  if ($isprocessed == 1)
  {
  ?>
  result();
  <?php
  }
  ?>
  
  function result()
  {
    var resid = "<?php echo (isset($resultid)) ? $resultid : ""; ?>";
    var resmsg = "<?php echo (isset($msg)) ? $msg : ""; ?>";
    var title = "";
    
    if (resid == 0)
    {
      title = "Password Changed!";
    }
    else
    {
      title = "Invalid Password";
    }
    /*document.getElementById('title3').innerHTML = title;
    document.getElementById('msg3').innerHTML = resmsg;
    document.getElementById('light3').style.display='block';
    document.getElementById('fade').style.display='block';*/
    showLightBox('<h1>'+resmsg+'</h1><input  id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />',title);	
    return false;
  }
  //-->
   function disableSpace(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 32)
        return false;

    return true;
}

function DisableSpecialCharacters(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if((charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;

    return true;
}
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

    return true;
}

function isAlphaKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ((charCode > 32 && charCode <65) || (charCode < 97 && charCode > 90) || (charCode < 128 && charCode > 122))
        return false;

    return true;
}
</script>
