<?php 
    /***********************************
     * Creation Date: May 4, 2012
     * Created by : Jfj     
     * Description: Viewing Prize counter
     **********************************/
include("../controller/prizecounterprocess.php");
?>
<script type = "text/javascript">
	function checkprizecounter()
	{
		var prize = document.getElementById('ddlprizes').options[document.getElementById('ddlprizes').selectedIndex].value;
		if(prize == 0)
		{
			document.getElementById('title').innerHTML = "SEARCH ERROR";
			document.getElementById('msg').innerHTML = "Please select a prize to be searched.";
			document.getElementById('light').style.display = 'block';
                        document.getElementById('fade').style.display = 'block';
			return false;
		}
		else
		{
			return true;
		}
                return true;
	}
</script>
  <div style="margin:10px;">
    
        <div style="text-align: right;">
            <?php echo $btnRefresh; ?>
            <!--<input type="submit" name="btnRefresh" id="btnRefresh" class="labelbutton2" value="REFRESH"/>-->
        </div>
        <div style="height:30px; margin-top:10px;">
          <table style="border-color: #FFFFFF;">
        <tr>
          <td colspan="2" style="background-color: #000000; color:#FFFFFF; font-weight:bold; font-size:1.2em; text-align:center; height:30px; width:800px;">
            Live Deck Information
          </td>
        </tr>
        <tr class="fontboldblack2">
            <td>&nbsp;Deck Control Number:</td>
            <td>&nbsp;&nbsp;<?php echo $deckno;?></td>
        </tr>
        <tr class="fontboldblack2">
            <td>&nbsp;Deck ID:</td>
            <td>&nbsp;&nbsp;<?php echo $deckid;?></td>
        </tr>
         <tr class="fontboldblack2">
            <td>&nbsp;Start Date:</td>
            <td>&nbsp;&nbsp;<?php echo $date;?></td>
        </tr>
    </table>
    <br />
    <table style="width:90%;">
        <tr>
            <td>
                <div style="font-size:1.2em; font-weight: bold;">Prize:&nbsp;&nbsp;
                    <?php echo $ddlprizes;?> 
                </div></td>
                <td>
<!--                 <input type="submit" name="btn" id="btn" value="SEARCH" class="labelbutton2" onclick="javascript: return checkprizecounter(); style="text-decoration:none; color:#000; font-weight:bold;"></input>-->
               <?php echo $btnSearch;?>   &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            </div>
                
        </tr>
    </table>
      <table style="border-color: #FFFFFF;">
        <tr>
          <td colspan="2" style="background-color: #000000; color:#FFFFFF; font-weight:bold; font-size:1.2em; text-align:center; height:30px; width:800px;">
            Prize Counter
          </td>
        </tr>
        <tr class="fontboldblack2">
            <td>&nbsp;Total Quantity of Prize:</td>
            <td>&nbsp;&nbsp;<?php echo (isset($prizequantity)) ? number_format($prizequantity) : 00; ?></td>
        </tr>
            <tr class="fontboldblack2">
            <td>&nbsp;Total Quantity of Prize Used:</td>
            <td>&nbsp;&nbsp;<?php echo (isset($prizeused)) ? number_format($prizeused) : 00; ?></td>
        </tr>
            <tr class="fontboldblack2">
            <td>&nbsp;Prize Balance:</td>
            <td>&nbsp;&nbsp;<?php echo (isset($prizebalance)) ? number_format($prizebalance) : 00; ?></td>
        </tr>
          <tr class="fontboldblack2">
            <td>&nbsp;Percent Consumption (%):</td>
            <td>&nbsp;&nbsp;<?php echo (isset($percentage)) ? number_format($percentage,2) : 00; ?>%</td>
        </tr>
       
      </table>
</div>


<!--pop up message-->
  <div id="light" style="text-align: center;font-size: 16pt;height: auto;margin-left:auto;" class="white_content2">
    <div style="width: 100%;height: 27px;background-color: black;top: 0px;color: white;padding-top: 5px;">
      <b id="a"><div id="title"></div></b>
    </div>
    <p id="b"></p>
    <br>
    <div id="msg">
    </div><br>
    <input type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" />      
  </div>
  <div id="fade" class="black_overlay2"></div>


</div>


