<?php 
/*
 * Created By: Maria Theresa Calderon
 * Modified By: Noel Antonio
 * Date Modified: May 4, 2012
 * LOGIN PAGE
 * Purpose: Entry point for Card Management Center
 */
include("../controller/loginprocess.php");
?>

<html>
    <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
    <script language="javascript" src="jscripts/validations.js"></script>
    <script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
    <title>Deck Management Center</title>
    
    <script type="text/javascript">
        function preventBackandForward()
        {
            window.history.forward(0);
        }
        preventBackandForward();
        window.inhibited_load=preventBackandForward;
        window.onpageshow=function(evt){if(evt.persisted)preventBackandForward();};
        window.inhibited_unload=function(){void(0);};
        
        $(document).ready(function(){
            $('#txtUsername').focus();
        }); 
        $('#txtUsername').live('keypress',function(e){
            if(e.keyCode == 13) {
                $('#txtPassword').focus();
                return false;
            }
        });        
        $('#txtPassword').live('keypress',function(e){
            if(e.keyCode == 13) {
                $('#btnSubmit').focus();
                $('#btnSubmit').click();
                return false;
            }
        });
        $('a').live('click', function(){
            $('#txtEmail').focus();
            return false;
        });
    </script> 
    
    <body>
        <form id="frmLogin" name="frmLogin" action="login.php"  method="post" onsubmit="">    
        <!-- POP UP FOR MESSAGES -->
        <div id="fade" class="black_overlay"></div>
        <div id="light" class="white_content2">
        <div id="title" class="lighttitle"></div><br/>
        <div id="msg"></div><br/>
        <input id="btnOk" class="labelbutton2" type="button" value="OKAY" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" />
        </div>
        <div id="light2" class="white_content2">
        <div id="title2" class="lighttitle"></div><br/>
        <div id="msg2"></div><br/>
        <?php echo $txtEmail; ?><br/><br/>
        <?php echo $btnReset; ?>
        <input id="btnCancel" class="labelbutton2" type="button" value="CANCEL" onclick="document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';document.getElementById('txtUsername').focus();" />
        </div>
        <!-- POP UP FOR MESSAGES -->
        
        <div id="container">
            <table cellspacing="3" width="100%">
                <tr>
                    <td colspan="2" align="center">
                        <div style="background-image: url(images/logo.png); background-repeat:no-repeat;height:139px; width:350px;margin-left:55px;"></div><br />
                        <h2>Deck Management Center</h2><br/><br/>
                    </td>
                </tr>
                <tr align="center" colspan="2">
                    <td><?php echo $txtUsername; ?><br/></td>
                </tr>
                <tr align="center" colspan="2">
                    <td><?php echo $txtPassword; ?><br/><br/></td>
                </tr>
                <tr align="center">
                    <td><?php echo $btnSubmit; ?><br/><br/></td>
                </tr>
                <tr colspan="2" align="center">
                    <td>
                        <a href="#" onclick="javascript: return openforgotpw();" style="font-size:1.0em; font-weight:bold; cursor:pointer; color:#000000;">Forgot Password</a>
                    </td>
                </tr>
            </table>
        </div>
        <?php if (isset($errormsg)): ?>
            <script>
                document.getElementById('title').innerHTML = "<?php echo $errormsgtitle;?>";
                document.getElementById('msg').innerHTML = "<?php echo $errormsg;?>";
                document.getElementById('txtEmail').value = "";
                document.getElementById('light').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
            </script>
        <?php endif; ?>
        <?php if (isset($errormsg2)): ?>
            <script>
                document.getElementById('title2').innerHTML = "<?php echo $errormsgtitle;?>";
                document.getElementById('msg2').innerHTML = "<?php echo $errormsg2;?>";
                document.getElementById('txtEmail').value = "";
                document.getElementById('txtEmail').focus();
                document.getElementById('light2').style.display='block';
                document.getElementById('fade').style.display='block';
            </script>
        <?php endif; ?>    
        </form>
    </body>
</html>
<script>
$(function () {
    $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
});
</script>
