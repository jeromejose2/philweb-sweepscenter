<?php 
/*
 * Added By: Noel Antonio
 * Date: May 8, 2012
 * Purpose: For logout
 */

require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCC_AccountSessions");
App::LoadModuleClass($modulename, "SCCC_AuditLog");

$aid = $_SESSION['aid'];
$scacctsessions = new SCCC_AccountSessions();
$scauditlog = new SCCC_AuditLog();

$scacctsessions->StartTransaction();

$arrAcctSessions = $scacctsessions->IfExists($aid);
if (count($arrAcctSessions) > 0)
{
    //end current active session
    $scupdateparam["DateEnd"] = "now_usec()";
    $scupdateparam["AccountID"] = $aid;
    $scupdateacctsession = $scacctsessions->UpdateByArray($scupdateparam);
    if ($scacctsessions->HasError)
    {
        $errormsg = $scacctsessions->getError();
        $errormsgtitle = "ERROR!";
        $scacctsessions->RollBackTransaction();
    }                       
}
$scacctsessions->CommitTransaction();

session_destroy();
URL::Redirect("login.php");
?>
