<?php
    /***********************************
     * Creation Date: May 4, 2012
     * Created by : Jfj     
     * Description: create User Account
     **********************************/

include("../controller/usercreateprocess.php");
?>
<script language="javascript" src="jscripts/validations.js"></script>
<?php defined('ANTI_DIRECT_ACCESS') || die('Access Denied.'); ?>
 <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />

 
 
 <form id="frmUsercreate" name="frmUsercreate" action="template.php?page=usercreate"  method="post" onsubmit="">
  <!-- POP UP FOR MESSAGES -->
  <div id="light" style="text-align: center;font-size: 16pt;height: auto;margin-left: auto;" class="white_content2">
    <div style="width: 100%;height: 27px;background-color: black;top: 0px;color: white;padding-top: 5px;">
      <b id="a"><div id="title"></div></b>
    </div>
    <p id="b">
        <br/><div id="msg"></div>
    </p>
    <br/>
    <?php echo $btnOk; ?>    
  </div>
  <div id="fade" class="black_overlay2"></div>
<!-- POP UP FOR MESSAGES -->
     <!-- POP UP FOR MESSAGES -->
   <div id="light1" style="text-align: center;font-size: 16pt;height: auto;margin-left: auto;" class="white_content2">
   <div style="width: 100%;height: 27px;background-color: black;top: 0px;color: white;padding-top: 5px;">
   <b id="a"><div id="title1"></div></b></div>
   <p id="b"><br/><div id="msg1"></div></p>
   <br/><?php echo $btnConfirm;?>
<!--   <input id="btnConfirm1" name="btnConfirm1" type="button" class ="labelbutton2 okrepresentative" onclick="javascript: window.location = 'template.php?page=generatedeck';document.getElementById('fade').style.display='none';" value="Okay"/>-->
       <?php // echo $btnOk; ?>
   </div>  <div id="fade" class="black_overlay2"></div>
<!-- POP UP FOR MESSAGES -->
     <!-- POP UP FOR MESSAGES -->
   <div id="light3" style="text-align: center;font-size: 16pt;height: auto;margin-left: auto;" class="white_content2">
   <div style="width: 100%;height: 27px;background-color: black;top: 0px;color: white;padding-top: 5px;">
   <b id="a"><div id="title3"></div></b></div>
   <p id="b"><br/><div id="msg3"></div></p>
   <br/><input id="btnOk" name="btnOk" type="button" onclick="javascript: window.location = 'template.php?page=queueddeck';document.getElementById('fade').style.display='none'; " value="Okay"/>
   </div><div id="fade" class="black_overlay2"></div>
<!-- POP UP FOR MESSAGES 
-->   
<div id="light4" style="text-align: center;font-size: 16pt;height: auto;margin-left: auto;" class="white_content2">
   <div style="width: 100%;height: 27px;background-color: black;top: 0px;color: white;padding-top: 5px;">
   <b id="a"><div id="title4"></div></b></div>
   <p id="b"><br/><div id="msg4"></div></p>
   <br/>
   <?php echo $btnYes;?>
    <?php echo $btnNo;?>
   </div><div id="fade" class="black_overlay2"></div>
<!-- POP UP FOR MESSAGES-->



<div style="margin:10px;">
      <table style="border-color:#FFFFFF;">
        <tr><td colspan="2" class="moduletitle">Create User Account</td>
        </tr>
         <tr class="fontboldblack2" style="font-weight: bolder">
            <td align="center">Username :</td><td>&nbsp;&nbsp;<?php echo $txtUsername; ?></td>
          </tr>
          <tr class="fontboldblack2">
            <td align="center">Password :</td> <td>&nbsp;&nbsp;<?php echo $txtPassword; ?></td>
          </tr>
           <tr class="fontboldblack2">
             <td align="center">Confirm Password :</td><td>&nbsp;&nbsp;<?php echo $confirmPassword; ?></td>
          </tr>
      </table>
    <table width="100%">
   <tr><th colspan="3" align="left">Account Information</th></tr>
   <tr>
      <th align ="left">&nbsp;&nbsp;&nbsp;&nbsp;First Name</th>
      <th align ="center">&nbsp;&nbsp;Middle Name</th>
      <th align ="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Last Name</th>
   </tr>
       <tr>
            <td class="fontboldblack" align ="left">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $txtfname; ?></td>
            <td class="fontboldblack" align ="center"><?php echo $txtmname; ?></td>
            <td class="fontboldblack" align ="right"><?php echo $txtlname; ?></td>
      </tr>
  </table>
    <table colspan="2">
        <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
        <tr class="fontboldblack">
   <th align="right">Email Address :</th><td align="left">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $txtemailaddress;?></td>
   
        <tr class="fontboldblack">
         <th align="right">Position :</th><th align="left">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $txtposition;?></th>
        </tr>
       <tr class="fontboldblack">
        <th align="right">Company :</th><th align="left">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $txtcompany;?></th>
        </tr>
       <tr class="fontboldblack">
          <th align="right">Department :</th><th align="left">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $txtdepartment;?></th>
        </tr>
       <tr class="fontboldblack">
           <th align="right">Group :</th><th align="left">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $txtgroup;?></th>
        </tr>
                   <tr class="fontboldblack">
                    <th align="right">All fields are required.</th>
                    </tr>
   
            <div style="text-align:center">
                <tr>
                    <td></td>      <th><?php echo $btncreate;?></th>
</div></tr>
     </table>   
 </div>
<script>
$(function () {
    $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
});
</script>
<?php if(isset($errormsg)):?>
<script>
	showLightBox('<h1><?php echo $errormsg;?></h1><input id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />','Notification');
</script>
<?php endif;?>
<?php if(isset($adduser_msg)):?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $adduser_title;?>";
    document.getElementById('msg').innerHTML = "<?php echo $adduser_msg;?>";
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif; ?>
<?php if(isset($createuser_msg)):?>
<script>
    document.getElementById('title1').innerHTML = "<?php echo $createuser_title;?>";
    document.getElementById('msg1').innerHTML = "<?php echo $createuser_msg;?>";
    document.getElementById('light1').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif; ?>
<?php if (isset($successmsg)): ?>
		<script>
		    document.getElementById('title3').innerHTML = "<?php echo $successmsg_title;?>";
		    document.getElementById('msg3').innerHTML = "<?php echo $successmsg;?>";
		    document.getElementById('light3').style.display = 'block';
		    document.getElementById('fade').style.display = 'block';
                    
		</script>   
<?php endif; ?>
 
                

                
                
<?php if(isset($confirm_msg)):?>
<script>
    document.getElementById('title4').innerHTML = "CONFIRMATION";
    document.getElementById('msg4').innerHTML = "You are about to create a new user account for the deck management tool. Do you wish to continue?";
    document.getElementById('light4').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif;?>
<?php unset($_SESSION['sticky']); ?>
</form>
 
 <script>
function validateEmail(sEmail) {
  var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
  if(!sEmail.match(reEmail)) {
     
Errormessage('<h2>Invalid email address</h2></br><input id="alertOk" type="button" value="OKAY" class="labelbutton2 okrepresentative" />','Notification');   
  }
  //return true;
}
function Errormessage(html,title)
{
   var e = document.getElementById('frmTemplate');
   var conheight = e.offsetHeight;
   jQuery('body').append('<div style="" class="black_overlay"></div>');
   jQuery('.black_overlay').css({'height':conheight});
   jQuery('.black_overlay').show(); 
   var inner = '<div id="light" class="white_content2"></div>';
   jQuery('#frmTemplate').append(inner);
   html = '<div class="lightbox_title"><h2>' + title + '</h2></div><br/>' + html;
   jQuery('.white_content2').html(html);
   jQuery('#light').show();
   jQuery('#alertOk').focus();
}
</script>




 