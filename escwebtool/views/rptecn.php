<?php 
/*
 * Created By: Noel Antonio
 * Date Created: May 4, 2012
 * ECN REPORT PAGE
 * Purpose: For ECN summary report.
 */

if(!isset($_SESSION))
{
	session_start(); 
}

include("../controller/rptecnprocess.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script language="javascript" src="jscripts/datetimepicker.js"></script>
<script language="javascript" src="jscripts/validations.js"></script>
<script language="javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
    
    function checkParameters()
    {
        var startdate = document.getElementById("txtDateFr").value;
        var enddate = document.getElementById("txtDateTo").value;
        var ddldeckid = document.getElementById('ddlDeckID').options[document.getElementById('ddlDeckID').selectedIndex].value;

        var end_date = new Date(enddate);
        var start_date = new Date(startdate);

        if(startdate.length == 0)
        {
                document.getElementById('title').innerHTML = "Error";
                document.getElementById('msg').innerHTML = "Please select a From date.";
                document.getElementById('light').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
                return false;
        }
        if(enddate.length == 0)
        {
                document.getElementById('title').innerHTML = "Error";
                document.getElementById('msg').innerHTML = "Please select a To date.";
                document.getElementById('light').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
                return false;
        }
        if(start_date > end_date)
        {
                document.getElementById('title').innerHTML = "Error";
                document.getElementById('msg').innerHTML = "Invalid date range.";
                document.getElementById('light').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
                return false;
        }
        if (ddldeckid == 0)
        {
                document.getElementById('title').innerHTML = "Error";
                document.getElementById('msg').innerHTML = "Please select Deck ID.";
                document.getElementById('light').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
                return false;
        }

        return true;
    }
</script>
<div style="margin:15px;text-align:center;">
    <table>
      <tr>
        <td class="labelbold">From:</td>
        <td>
            <?php echo $txtDateFr; ?>
          <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
        </td>

        <td class="labelbold">To:</td>
        <td>
          <?php echo $txtDateTo; ?>
          <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
        </td>
        <td width="100px"><?php echo $btn; ?></td>
      </tr>
      <tr>
          <td><b>Deck ID:&nbsp;</b></td>
          <td><?php echo $ddlDeckID;?></td>
      </tr>
    </table><br/>
    
    <table>
       <tr><td colspan="8" class="moduletitle">ECN Usage Report</td></tr>
        <tr class="fontboldblack2" style="text-align: center; ">
            <td>Date and Time Used</td>
            <td>POS Account Name</td>
            <td>Deck Control #</td>
            <td>Entry Confirmation Number</td>
            <td>Prize</td>
        </tr>
        <?php if (isset($list) && count($list) > 0):?>
            <?php for ($i = 0; $i < count($list); $i++) { ?>
            <tr class="fontboldblack2" style="text-align: center;">                
            <td><?php echo $list[$i]["DateUsed"]; ?></td>
            <td><?php echo $list[$i]["POSAccountName"]; ?></td>
            <td><?php echo $list[$i]["DeckNo"]; ?></td>
            <td><?php echo $list[$i]["ECN"]; ?></td>
            <td><?php echo $list[$i]["PrizeDescription"]; ?></td>
            <?php } ?>
        <?php else: ?>
            <td colspan="8" style="text-align: center;">No Records Found</td>
        <?php endif; ?>
        </tr>
    </table>
    <?php if (isset($list) && count($list) > 0): ?>
        <div style="text-align:center;" class="paging"><?php echo $pgTransactionHistory; ?></div>
    <?php endif; ?>
</div>

  <div id="light" style="text-align: center;font-size: 16pt;height: auto;margin-left: auto;" class="white_content2">
    <div style="width: 100%;height: 27px;background-color: black;top: 0px;color: white;padding-top: 5px;">
      <b id="a"><div id="title"></div></b>
    </div>
    <p id="b">
        <br/><div id="msg"></div>
    </p>
    <br/>
    <input type="button" value="OKAY" class="labelbutton2" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" />      
  </div>
  <div id="fade" class="black_overlay2"></div>
