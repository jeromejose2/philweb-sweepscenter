<?php
/*****************************
 * Author: 
 * Date Created: 05/03/2012
 * Company: 
 *****************************/

require_once("include/core/init.inc.php");
ini_set( 'magic_quotes_gpc', "Off" );
global $_CONFIG;
$_CONFIG["appdir"] = dirname(__FILE__) . "/";
$_CONFIG["apptemplatedir"] = dirname(__FILE__) . "/templates/";
$_CONFIG["appcontrollerdir"] = dirname(__FILE__) . "/controller/";
$_CONFIG["appviewdir"] = dirname(__FILE__) . "/views/";
$_CONFIG["logdir"] = dirname(__FILE__) . "/logs/";
?>
