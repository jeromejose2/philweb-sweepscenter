<?php 
/*
 * Date Modified: May 4, 2012
 * Edited by : jfj
 */
$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCC_DeckInfo");
App::LoadCore("PHPMailer.class.php");
$deckinfo = new SCCC_DeckInfo();
App::LoadControl("Button");

$fproc = new FormsProcessor();

$btnRefresh = new Button("btnRefresh", "btnRefresh", "REFRESH");
$btnRefresh->Args = "onclick = 'template.php?page=deckcounter' ";
$btnRefresh->CssClass = "labelbutton2";
$btnRefresh->Style = "width: 200px";
$btnRefresh->IsSubmit = true;

$fproc->AddControl($btnRefresh);

$fproc->ProcessForms();
$projid = "";
$projid = $_SESSION['projectid'];

$getactivedeck = $deckinfo->GetActiveDeck($projid);
$arractivedeck = new ArrayList();
$arractivedeck->AddArray($getactivedeck);
$createdby ="";

for($a=0;$a<count($arractivedeck);$a++) 
   { 
     $createdby = $arractivedeck[$a]["CreatedByAID"];
   } 

if ($createdby == 0)
{
    $getlivedeck = $deckinfo->GetLiveDeck2($projid);   
}
else
{
    $getlivedeck = $deckinfo->GetLiveDeck($projid);
}

$arrlivedeck = new ArrayList();
$arrlivedeck->AddArray($getlivedeck);


        $decksize = "";
        $winningsused = "";
        $entriesused = "";
        $remainingentries = "";
        $percentage = "";
        $deckid = "";
        $deckno = "";
        $date = "";
        $genby = "";
        $percentage = "";
        $createdby = "";

for($a=0;$a<count($arrlivedeck);$a++) 
   { 
        $decksize = $arrlivedeck[$a]["DeckSize"];
        $winningsused = $arrlivedeck[$a]["WinningsUsed"];
        $entriesused = $arrlivedeck[$a]["EntriesUsed"];
        $remainingentries = $arrlivedeck[$a]["RemainingEntries"];
        $percentage = $arrlivedeck[$a]["Percentage"]; 
        $deckid = $arrlivedeck[$a]["BatchID"];
        $deckno = $arrlivedeck[$a]["ControlNo"];
        $date = $arrlivedeck[$a]["StartDate"];
        $genby = $arrlivedeck[$a]["GeneratedBy"];
        $percentage = $arrlivedeck[$a]["Percentage"];
        $createdby =  $arrlivedeck[$a]["CreatedBy"];
 
    } 
   /*
  if ($percentage > 80) 
      {
         // Sending of Email
       $pm = new PHPMailer();
        
        $pm->AddAddress("mccalderon@philweb.com.ph", "Ma. Theresa C. Calderon");

        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        $folder = $_SERVER["REQUEST_URI"];
        $folder = substr($folder,0,strrpos($folder,'/') + 1);
        if ($_SERVER["SERVER_PORT"] != "80") 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
        } 
        else 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].$folder;
        }
        
        $pm->IsHTML(true);

                
        $pm->Body = "<HTML><BODY>						
            <div>Good Day!</div>
            <br /><br />
            This is to inform you  that the current live deck for the Sweepstakes Café’s Electronic Scratch Card with Deck ID ".$deckid." has now reached 80% consumption level. 
                <br />As such, please check the queued decks to prepare the next deck to be put into live usage.
          
            <br /><br />
            To log on to your account, please click this url  http://www.thesweepscenter.com/ESC_Webtool.
            <br /><br /><br />
            Regards,
            <br /><br />
            The Sweeps Center
           
            </BODY></HTML>";

        $pm->From = "noreply@thesweepscenter.com";
        $pm->FromName = "The Sweeps Center";
        $pm->Host = "localhost";
        $pm->Subject = "NOTIFICATION FOR SWEEPSTAKES DECK CONSUMPTION LEVEL";
        $email_sent = $pm->Send();
        if($email_sent)
        {
            $createuser_title = "Password Changed! ";
            $createuser_msg = "You have successfully changed your password. Please check your email for your new log-in credentials.";
        }
        else
        {
            $errorr_title = "Error";
            $errorr_msg = "An error occurred while sending the email to your email address";
        }
      }
      */
?>
