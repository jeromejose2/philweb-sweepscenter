<?php
/* JFJ 05-29-2012
 * 
 */

require_once("../init.inc.php");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("Label");
App::LoadControl("ComboBox");
$modulename = "SweepsCenter";

App::LoadModuleClass($modulename,"SCCC_DeckInfo");
App::LoadModuleClass($modulename, "SCCC_Accounts");
App::LoadModuleClass($modulename, "SCCC_AuditLog");
App::LoadCore("PHPMailer.class.php");
$deckinfo = new SCCC_DeckInfo();
$accounts = new SCCC_Accounts();
$auditlog = new SCCC_AuditLog();


$fproc = new FormsProcessor();

$txtUsername = new TextBox("txtUsername", "txtUsername");
//$txtUsername->ShowCaption = true;
$txtUsername->Length = 12;
$txtUsername->Style = "width:300px";
$txtUsername->Args = "size='12'";
$txtUsername->Args="autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtPassword = new TextBox("txtPassword", "txtPassword");
$txtPassword->ShowCaption = true;
$txtPassword->Password = true;
$txtPassword->Length = 12;
$txtPassword->Args = "size='12'";
$txtPassword->Args = "autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";
$txtPassword->Style = "width:300px";

$confirmPassword = new TextBox("confirmPassword", "confirmPassword");
$confirmPassword->ShowCaption = true;
$confirmPassword->Password = true;
$confirmPassword->Length = 12;
$confirmPassword->Args = "size='12'";
$confirmPassword->Args = "autocomplete='off' onkeypress='javascript:return AlphaNumericOnly(event);'";
$confirmPassword->Style = "width:300px";

$txtemailaddress = new TextBox("txtemailaddress", "txtemailaddress");
$txtemailaddress->ShowCaption = true;
$txtemailaddress->Length = 50;
$txtemailaddress->Style = "width:200px;";
$txtemailaddress->Args="autocomplete='off' onblur='validateEmail(this.value);' onkeypress='javascript:return verifyEmail(event);'";

$txtposition = new TextBox("txtposition", "txtposition");
$txtposition->ShowCaption = true;
$txtposition->Length = 20;
$txtposition->Style = "width:200px;";
$txtposition->Args = "autocomplete='off' onkeypress = 'javascript : return AlphaNumeric(event);'";


$txtcompany = new TextBox("txtcompany", "txtcompany");
$txtcompany->ShowCaption = true;
$txtcompany->Length = 20;
$txtcompany->Style = "width:200px;";
$txtcompany->Args = "autocomplete='off' onkeypress ='javascript: return AlphaNumeric(event);'";

$txtdepartment = new TextBox("txtdepartment", "txtdepartment");
$txtdepartment->ShowCaption = true;
$txtdepartment->Length = 20;
$txtdepartment->Style = "width:200px;";
$txtdepartment->Args = "autocomplete='off' onkeypress ='javascript : return AlphaNumeric(event);'";

$getgroups = $deckinfo->GetGroups();

$txtgroup = new ComboBox("ddlgroup", "ddlgroup");
$listgroup = null;
$txtgroup->Style = "width:200px;";
$txtgroup->CssClass = "labelbutton5";
$listgroup[] = new ListItem("Please Select","0",true);
$txtgroup->Items = $listgroup;
$listgroup = new ArrayList();
$listgroup->AddArray($getgroups);
$txtgroup->DataSource = $listgroup;
$txtgroup->DataSourceText = "Name";
$txtgroup->DataSourceValue = "ID";
$txtgroup->DataBind();

$btncreate = new Button("btnCreateUser", "btnCreateUser", "Create User");
$btncreate->CssClass = "labelbutton2";
$btncreate->Style = "width : 200px";
//$btncreate->Args = "onclick = 'javascript: return showErrorAndSuccess();";
$btncreate->IsSubmit = true;

$txtfname = new TextBox("txtfname", "txtfname");
//$txtfname->ShowCaption = true;
$txtfname->Length = 30;
$txtfname->Args = "size='30'";
$txtfname->Args = "autocomplete='off' onkeypress = 'javascript : return AlphaOnly(event);'";

$txtmname = new TextBox("txtmname", "txtmname");
//$txtmname->ShowCaption = true;
$txtmname->Length = 2;
$txtmname->Args = "size='2'";
$txtmname->Args = "autocomplete='off' onkeypress = 'javascript : return AlphaOnly(event);'";

$txtlname = new TextBox("txtlname", "txtlname");
//$txtlname->ShowCaption = true;
$txtlname->Length = 30;
$txtlname->Args = "size='30'";
$txtlname->Args = "autocomplete='off' onkeypress='javascript : return AlphaOnly(event);'";

$btnConfirm = new Button("btnConfirm","btnConfirm","OKAY");
$btnConfirm->CssClass = "labelbutton2 okrepresentative";
$btnConfirm->IsSubmit = true;
//$btnConfirm->Args = "onclick='javascript: window.location = 'template.php?page=prizecounter';document.getElementById('fade').style.display='none';'";

$btnYes = new Button("btnYes","btnYes","YES");
$btnYes->CssClass = "labelbutton2 okrepresentative";
$btnYes->IsSubmit = true;

$btnNo = new Button("btnNo","alertOk","NO");
$btnNo->CssClass = "labelbutton2 okrepresentative";
$btnNo->IsSubmit = true;

$btnOk = new Button("btnOk","alertOk","OKAY");
$btnOk->IsSubmit = true;
$btnOk->CssClass = "labelbutton2 okrepresentative";
$btnOk->Args = "onclick='document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';'";

$fproc->AddControl($txtUsername);
$fproc->AddControl($txtPassword);
$fproc->AddControl($confirmPassword);
$fproc->AddControl($txtemailaddress);
$fproc->AddControl($txtposition);
$fproc->AddControl($txtcompany);
$fproc->AddControl($txtdepartment);
$fproc->AddControl($txtgroup);
$fproc->AddControl($btncreate);
$fproc->AddControl($txtfname);
$fproc->AddControl($txtmname);
$fproc->AddControl($txtlname);
$fproc->AddControl($btnConfirm);
$fproc->AddControl($btnYes);
$fproc->AddControl($btnNo);
$fproc->AddControl($btnOk);

$fproc->ProcessForms();
 
if($fproc->IsPostBack){
   $projid = $_SESSION['projectid'];
   $currusername = $_SESSION["username"];
   $status = 1;  //status = 0; not active; status =1 active
   
    if($btncreate->SubmittedValue == "Create User"){
    //checking for fields
    $username = $txtUsername->SubmittedValue;
    $password = $txtPassword->SubmittedValue;
    $cpassword = $confirmPassword->SubmittedValue;
    $fname = $txtfname->SubmittedValue;
    $mname = $txtmname->SubmittedValue;
    $lname = $txtlname->SubmittedValue;
    $email = $txtemailaddress->SubmittedValue;
    $position = $txtposition->SubmittedValue;
    $company = $txtcompany->SubmittedValue;
    $department = $txtdepartment->SubmittedValue;
    $group = $txtgroup->SubmittedValue;
    
        if(strlen($username) == "")
        {
            $adduser_title = "Notification";
            $adduser_msg = "Please enter your username.";
        } else 
        {
            if(strlen($username) < 8)
            {
                 $adduser_title = "Notification";
                 $adduser_msg = "Username should be minimum of 8 and maximum of 12 alphanumeric characters.";
            }
            else if(strlen($password) == "")
            {
                 $adduser_title = "Notification";
                 $adduser_msg = "Please enter your password.";
            }
            else if(strlen($password) < 8)
            {
                 $adduser_title = "Notification";
                 $adduser_msg = "Password should be minimum of 8 and maximum of 12 alphanumeric characters.";
            }           
            else if(strlen($cpassword) == "")
            {
                $adduser_title = "Notification";
                $adduser_msg = "Please enter your confirm password.";
            }
//            else if(strlen($username) < 8 || strlen($password) < 8 )
//            {
//                $adduser_title = "Notification";
//                $adduser_msg = "Please provide an information with more than 8 characters.";
//            }
            else if ($password != $cpassword)
            {
                $adduser_title = "Notification";
                $adduser_msg = "Password and confirm password should be identical";
                
            } elseif(strlen($fname) == "")
            {
                      $adduser_title = "Notification";
                      $adduser_msg = "Please enter your first name.";
            }
             else if(strlen($mname) == "")
            {
                $adduser_title = "Notification";
                $adduser_msg = "Please enter your middle name.";
            } else if(strlen($lname) == "")
            {
                $adduser_title = "Notification";
                $adduser_msg = "Please enter your last name.";
            } 
               else if(strlen($email) == "")
            {
                $adduser_title = "Notification";
                $adduser_msg = "Please enter your email address.";
            }
            else if(strlen($position) == "")
            {
                $adduser_title = "Notification";
                $adduser_msg = "Please enter your position.";
            } else if(strlen($company) == "")
            {
                $adduser_title = "Notification";
                $adduser_msg = "Please enter your company.";
            } else if(strlen($department) == "")
            {
                $adduser_title = "Notification";
                $adduser_msg = "Please enter your department.";

            } else if($group == "0")
            {
                $adduser_title = "Notification";
                $adduser_msg = "Please select your account type.";
            }
            else if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
            {
                $adduser_title = "Notification";
                $adduser_msg = "Invalid email address";
            }
            else{
            
                // final checking before inserting to db
     $emailcount = 0;
    if(trim($txtemailaddress->SubmittedValue) == "")
    {
        $emailcount = 0;
    }else
        {
       $emailchecking = $accounts->CheckEmail(mysql_escape_string(trim($txtemailaddress->SubmittedValue)));
       $emailcount = $emailchecking[0][0];
        }
    if ($emailcount > 0)
    {
            $adduser_title = "ERROR!";
            $adduser_msg = "Email address already taken.";
             //$accounts->RollBackTransaction();
    }
    $unamechecking = $accounts->CheckUsername(mysql_escape_string(trim($txtUsername->SubmittedValue)));
    if($unamechecking[0][0] > 0)
        {

            $adduser_title = "ERROR!";
            $adduser_msg = "Username is already used, please enter different username.";
            //$accounts->RollBackTransaction();
        }
        if(($emailcount == 0) && ($unamechecking[0][0] == 0))
        {
            $confirm_msg = "this is now isset";
        }
}
}
}

if($btnYes->SubmittedValue == "YES") 
    {
            $insertacct["ProjectID"] = $projid;
        $insertacct["Username"] = trim($txtUsername->SubmittedValue); 
	$insertacct["Password"] = MD5(trim($txtPassword->SubmittedValue));
        $insertacct["AccountType"] = trim($txtgroup->SelectedValue);
        $insertacct["Status"] = $status;
        $insertacct["FName"] = trim($txtfname->SubmittedValue);
        $insertacct["MInitial"] = trim($txtmname->SubmittedValue);
        $insertacct["LName"] = trim($txtlname->SubmittedValue);
        $insertacct["Email"] = trim($txtemailaddress->SubmittedValue);
        $insertacct["Position"] = trim($txtposition->SubmittedValue);
        $insertacct["Company"] = trim($txtcompany->SubmittedValue);
        $insertacct["Department"] = trim($txtdepartment->SubmittedValue);
        $insertacct["CreatedOn"] = "now_usec()";
        $insertacct["CreatedBy"] = $currusername; 
	      $adduser = $accounts->Insert($insertacct);
        if($accounts->HasError)
            {
            $adduser_msg = "Error has occured:".$accounts->getError();
            $adduser_title = "Error!";
            $accounts->RollBackTransaction();
            }else
            {
            $auditdtls["SessionID"] = $_SESSION['sid'];
            $auditdtls["AccountID"] = $_SESSION['accttype'];
            $auditdtls["TransDetails"] = "NEW Account Username: " . $txtUsername->SubmittedValue;
            $auditdtls["TransDateTime"] = "now_usec()";
            $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
          
            $insertauditlog = $auditlog->Insert($auditdtls);
            if($auditlog->HasError)
            {
                $adduser_title = "ERROR!";
                $adduser_msg = "Error has occured:" . $accounts->getError();
                $accounts->RollBackTransaction();
            }else
            {
              $username = $txtUsername->SubmittedValue;
              $password = $txtPassword->SubmittedValue;
              $email = $txtemailaddress->SubmittedValue;
              // Sending of Email
        
        $date = date("m/d/Y");
        $time = date("h:i:s A");
        $pm = new PHPMailer();
        $pm->AddAddress($email,$username);
      
        $pm->IsHTML(true);

        $pm->Body = "<HTML><BODY>						
            <div>Dear ".$username."</div>
            <br /><br />
            This is to inform you that your User Account has been successfully created on this date ".$date. " and time ".$time.". 
                Here are your Account Details:<br /> 
                        <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   UserName : ".$username." 
                        <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   Password :  ".$password."    
            <br /><br />
            For security purposes, please change this assigned password upon log in at http://www.thesweepscenter.com/ESC_Webtool.
            <br /><br />
            For inquiries on your account, please contact  our Customer Service email at support.gu@philwebasiapacific.com.
            <br /><br /><br />
            Regards,
            <br /><br />
            The Sweeps Center Team
            <!-- <br />
            PW Guam Management -->
            </BODY></HTML>";

        $pm->From = "noreply@thesweepscenter.com";
        $pm->FromName = "The Sweeps Center";
        $pm->Host = "localhost";
        $pm->Subject = "SUCCESSFUL ACCOUNT CREATION FOR SWEEPSTAKES MANAGEMENT TOOL";
        
        
        $email_sent = $pm->Send();
        if($email_sent)
        {
            $createuser_title = "Successful Account Creation ";
            $createuser_msg = "You have successfully created a new account with username "."$txtUsername->SubmittedValue".".";
          
        }
        else
        {
            $adduser_title = "Error";
            $adduser_msg = "An error occurred while sending the email to your email address";
           
        }
       }
    }
   }
}

 if($btnConfirm->SubmittedValue == "OKAY")
 {
     App::Pr("<script> window.location = 'template.php?page=usercreate'; </script>");
       
            
     }
     
?>


