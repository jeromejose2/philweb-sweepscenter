<?php
 /***********************************
     * Creation Date: May 7, 2012
     * Created by : Jfj     
     * Description: create Change Password
     **********************************/

require_once("../init.inc.php");

App::LoadControl("TextBox");
App::LoadControl("Button");
$modulename = "SweepsCenter";
App::LoadModuleClass($modulename,"SCCC_DeckInfo");
App::LoadModuleClass($modulename, "SCCC_Accounts");
App::LoadModuleClass($modulename, "SCCC_AuditLog");
App::LoadCore("PHPMailer.class.php");
$deckinfo = new SCCC_DeckInfo();
$accounts = new SCCC_Accounts();
$auditlog = new SCCC_AuditLog();

$fproc = new FormsProcessor;

$txtpassword = new TextBox("txtpassword", "txtpassword");
$txtpassword->ShowCaption = true;
$txtpassword->Password = true;
$txtpassword->Length = 12;
$txtpassword->Args = "autocomplete='off' onkeypress='javascript : return AlphaNumericOnly(event)';";

$txtnewpassword = new TextBox("txtnewpassword","txtnewpassword");
$txtnewpassword->ShowCaption = true;
$txtnewpassword->Password = true;
$txtnewpassword->Length = 12;
$txtnewpassword->Args = "autocomplete='off' onkeypress='javascript : return AlphaNumericOnly(event)';";

$txtcnewpassword = new TextBox("txtcnewpassword","txtcnewpassword");
$txtcnewpassword->ShowCaption = true;
$txtcnewpassword->Password = true;
$txtcnewpassword->Length = 12;
$txtcnewpassword->Args = "autocomplete='off' onkeypress = 'javascript : return AlphaNumericOnly(event)';";

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Style = "width : 200px";

$btnCancel = new Button("btnCancel","btnCancel","Cancel");
$btnCancel->CssClass = "labelbutton2";
$btnCancel->Style = "width : 200px; text-decoration:none;color:#000;";
$btnCancel->IsSubmit = true;
//$btnCancel->Args = "onclick='Url::Redirect('template.php?page=changepassword');'";

$btnCancel1 = new Button("","","Cancel");
$btnCancel1->CssClass = "labelbutton2";
$btnCancel1->Style = "width : 200px; text-decoration:none;color:#000;";
//$btnCancel->Args = "onclick='document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';'";

$btnOk = new Button("btnOk","alertOk","OKAY");
$btnOk->IsSubmit = true;
$btnOk->CssClass = "labelbutton2 okrepresentative";
$btnOk->Args = "onclick='document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';'";

$btnYes = new Button("btnYes","btnYes","OKAY");
$btnYes->CssClass = "labelbutton2 okrepresentative";
$btnYes->IsSubmit = true;

$btnNo = new Button("btnNo","alertOk","CANCEL");
$btnNo->CssClass = "labelbutton2 okrepresentative";
$btnNo->IsSubmit = true;

$btnConfirm = new Button("btnConfirm","btnConfirm","OKAY");
$btnConfirm->CssClass = "labelbutton2 okrepresentative";
$btnConfirm->IsSubmit = true;

$fproc->AddControl($txtpassword);
$fproc->AddControl($txtnewpassword);
$fproc->AddControl($txtcnewpassword);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnCancel);
$fproc->AddControl($btnCancel1);
$fproc->AddControl($btnYes);
$fproc->AddControl($btnOk);
$fproc->AddControl($btnNo);
$fproc->AddControl($btnConfirm);

$fproc->ProcessForms();

if($fproc->IsPostBack)
{
    $aid = $_SESSION['aid'];
    $selectpass = $accounts->SelectPassword($aid);
    $sessionpassword =  $selectpass[0][0];//($_SESSION['password']);
    $oldpass =  $txtpassword->SubmittedValue;
    $oldpass1 =  md5($txtpassword->SubmittedValue);
    $newpass1 =  md5($txtnewpassword->SubmittedValue);
    $newpass =  $txtnewpassword->SubmittedValue;
    $cnewpass = $txtcnewpassword->SubmittedValue;
 
    if($btnSubmit->SubmittedValue == "Submit")
    {
        if(strlen($oldpass) == "")
        {
            $errorr_msg = "Please enter your current password to continue.";
            $errorr_title = "Insufficient Data";
        }
        else
        {
            if(strlen($oldpass) < 8)
            {
                $errorr_msg = "Password must not be less than 8 characters.";
                $errorr_title = "Insufficient Data";
            }            
            else if(strlen($newpass) == "")
            {
                $errorr_msg = "Please enter a new password to continue.";
                $errorr_title = "Insufficient Data";
                
            }else if(strlen($newpass) <8)
            {
                $errorr_msg ="Password must not be less than 8 characters.";
                $errorr_title = "Insufficient Data";
                
            } else if (strlen($cnewpass) == "")
            {
                $errorr_msg = "Please confirm new password.";
                $errorr_title = "Insufficient Data";
            }
             else if (strlen($cnewpass) <8)
            {
                $errorr_msg = "Confirm New Password must not be less than 8 characters.";
                $errorr_title = "Insufficient Data";
            }
//            else if (strlen($cnewpass) < 8 && strlen($cnewpass) > 0)
//            {
//                $errorr_msg = "Password must not be less than 8 characters.";
//                $errorr_title = "Insufficient Data";
//                
//            }
            else if($oldpass1 != $sessionpassword)
            {
                $errorr_msg = "The current password you entered is invalid. Please try again.";
                $errorr_title = "Invalid Password";
            }
            else if($newpass != $cnewpass)
            {
                $errorr_msg = "The new passwords you entered do not match. Please try again.";
                $errorr_title = "Invalid Password";
            }
//            else if($newpass1 == $sessionpassword)
//            {
//                $errorr_msg=" New password cannot be similar to current password.Please try again.";
//                $errorr_title = "ERROR";
//            }
            else
            {
                $confirm_msg = "You are about to change your current password. Do you want to continue?";
            }
        }
    }
    if($btnYes->SubmittedValue == "OKAY")
    {
        $password = md5($newpass);
        $currusername = $_SESSION["username"];
        $accounts->StartTransaction();
        $updatepass = $accounts->UpdatePassword($password, $currusername, $aid);
          if($accounts->HasError)
            {
            $errorr_msg = "Error has occured:".$accounts->getError();
            $errorr_title = "Error!";
            $accounts->RollBackTransaction();
            }else
            {
                $auditlog->StartTransaction();
            $auditdtls["SessionID"] = $_SESSION['sid'];
            $auditdtls["AccountID"] = $_SESSION['accttype'];
            $auditdtls["TransDetails"] = "ChangePassword AccountID:".$aid;
            $auditdtls["TransDateTime"] = "now_usec()";
            $auditdtls["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
          
            $insertauditlog = $auditlog->Insert($auditdtls);
            if($auditlog->HasError)
            {
                $errorr_title = "ERROR!";
                $errorr_msg = "Error has occured:" . $accounts->getError();
                $accounts->RollBackTransaction();
            }else
            {
                
           $auditlog->CommitTransaction();
           $accounts->CommitTransaction();
           $updatechangepass = $accounts->UpdateChangePassword($aid);
            // Sending of Email
        $username =  $currusername;
        $email = $_SESSION["Email"];
        $password = $newpass;
        $date = date("m/d/Y");
        $time = date("h:i:s A");
        $pm = new PHPMailer();
        $pm->AddAddress($email, $username);
       
        $pm->IsHTML(true);
        $pm->Body = "<HTML><BODY>						
            <div>Dear ".$username."</div>
            <br /><br />
            This is to inform you that your password has been changed on this date ".$date. " and time ".$time.".
                Here are your new Account Details:<br />
                        <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   UserName : ".$username." 
                        <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   Password :  ".$password."    
            <br /><br />
            If you didn’t perform this procedure, please report this to our Customer Service email at support.gu@philwebasiapacific.com.
            <br /><br /><br />
            Regards,
            <br /><br />
            Customer Support
           
            </BODY></HTML>";

        $pm->From = "noreply@thesweepscenter.com";
        $pm->FromName = "The Sweeps Center";
        $pm->Host = "localhost";
        $pm->Subject = "NOTIFICATION FOR NEW PASSWORD";
        $email_sent = $pm->Send();
        if($email_sent)
        {
            $createuser_title = "Password Changed! ";
            $createuser_msg = "You have successfully changed your password. Please check your email for your new log-in credentials.";
        }
        else
        {
            $errorr_title = "Error";
            $errorr_msg = "An error occurred while sending the email to your email address";
        }
           }
            }
          }
    
        if($btnConfirm->SubmittedValue == "OKAY")
        {
                App::Pr("<script> window.location = 'template.php?page=changepassword'; </script>");
        }
}

?>