<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 4, 2012
 * ECN REPORT CONTROLLER
 * Purpose: For ECN summary report.
 */

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCC_DeckInfo");
App::LoadModuleClass($modulename, "SCCC_Winners");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");

$fproc = new FormsProcessor();
$scdeckinfo = new SCCC_DeckInfo();
$scwinners = new SCCC_Winners();

$ddlDeckID = new ComboBox("ddlDeckID", "ddlDeckID");
$options = null;
$options[] = new ListItem("----","0", true);
$ddlDeckID->Items = $options;
$dbopt = $scdeckinfo->GetECNDeckID();
$ddlopt = new ArrayList();
$ddlopt->AddArray($dbopt);
$ddlDeckID->DataSource = $ddlopt;
$ddlDeckID->DataSourceText = "DeckNo";
$ddlDeckID->DataSourceValue = "DeckID";
$ddlDeckID->DataBind();

$txtDateFr =  new TextBox("txtDateFr", "txtDateFr");
$txtDateFr->Length = 10;
$txtDateFr->Style = "text-align: center";
$txtDateFr->ReadOnly = true;

$txtDateTo =  new TextBox("txtDateTo", "txtDateTo");
$txtDateTo->Length = 10;
$txtDateTo->Style = "text-align: center";
$txtDateTo->ReadOnly = true;

$btn = new Button("btn", "btn", "Search");
$btn->CssClass = "labelbutton2";
$btn->Args="onclick = 'javascript: return checkParameters();'";
$btn->IsSubmit = true;

$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($ddlDeckID);
$fproc->AddControl($btn);

$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btn->SubmittedValue == "Search" || $fproc->GetPostVar('pgSelectedPage') != '')
    {
         $datefrom = date('Y-m-d', strtotime($txtDateFr->SubmittedValue));
        $dateto = date('Y-m-d', strtotime($txtDateTo->SubmittedValue));

        $data = $scwinners->GetECNDetails($datefrom, $dateto, $ddlDeckID->SubmittedValue);
        $count = count($data);
        
        $ecnhistory = $scwinners->GetECNDetailsWithLimit($txtDateFr->SubmittedValue, $txtDateTo->SubmittedValue, $ddlDeckID->SubmittedValue, $pgcon->SelectedItemFrom-1, $itemsperpage);
        $list = new ArrayList();
        $list->AddArray($ecnhistory);
        
        if ($btn->SubmittedValue == "Search"){ $pgcon->SelectedPage = 1; }
    }
    $pgcon->Initialize($itemsperpage, $count);
    $pgTransactionHistory = $pgcon->PreRender();
}

?>
