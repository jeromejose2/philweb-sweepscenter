<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 4, 2012
 * DECK USAGE CONTROLLER
 * Purpose: For Deck Usage report.
 */

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCC_DeckInfo");
//APP::LoadModuleClass($modulename,);
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("PagingControl2");

$fproc = new FormsProcessor();
$scdeckinfo = new SCCC_DeckInfo();


$txtDateFr =  new TextBox("txtDateFr", "txtDateFr");
$txtDateFr->Length = 10;
$txtDateFr->Style = "text-align: center";
$txtDateFr->ReadOnly = true;

$txtDateTo =  new TextBox("txtDateTo", "txtDateTo");
$txtDateTo->Length = 10;
$txtDateTo->Style = "text-align: center";
$txtDateTo->ReadOnly = true;

$btn = new Button("btn", "btn", "Search");
$btn->CssClass = "labelbutton2";
$btn->Args="onclick = 'javascript: return checkParameters();'";
$btn->IsSubmit = true;


$itemsperpage = 10;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;

$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btn);


$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btn->SubmittedValue == "Search" || $fproc->GetPostVar('pgSelectedPage') != '')
    {
       
        $datefrom = date('Y-m-d', strtotime($txtDateFr->SubmittedValue));
        $dateto = date('Y-m-d', strtotime($txtDateTo->SubmittedValue));

        $data = $scdeckinfo->GetDeckHistory($datefrom, $dateto, $_SESSION['projectid']);
        $count = count($data);
        $deckhistory = $scdeckinfo->GetDeckHistoryWithLimit($datefrom,$dateto, $_SESSION['projectid'], $pgcon->SelectedItemFrom-1, $itemsperpage);
        /*$d_id = $deckhistory[0]["DeckID"];
        for ($a = 0; $a < count($deckhistory); $a++)
        {
            $curr_id = $d_id;
            $d_id = $deckhistory[$a]["DeckID"];
            $ctr = 0;
            if ($curr_id == $d_id)
            {
                $arrvar[$ctr]["BatchID"] = $deckhistory[$a]["BatchID"];
                $arrvar[$ctr]["DeckID"] = $deckhistory[$a]["DeckID"];
                $arrvar[$ctr]["DeckSize"] = $deckhistory[$a]["DeckSize"];
                $arrvar[$ctr]["StartDate"] = $deckhistory[$a]["StartDate"];
                $arrvar[$ctr]["EndDate"] = $deckhistory[$a]["EndDate"];
                $arrvar[$ctr]["Duration"] = $deckhistory[$a]["Duration"];
                $arrvar[$ctr]["GeneratedBy"] = $deckhistory[$a]["GeneratedBy"];
                $available_prizes .= $deckhistory[$a]["PrizeDescription"] . "<br/>";
                $arrvar[$ctr]["PrizeDescription"] = $available_prizes;
            } else {
                $ctr++;
            }
        }*/
        $list = new ArrayList();
        $list->AddArray($deckhistory);
       for ($i = 0; $i < count($list); $i++) 
       { 
                $deckId2 =  $deckhistory[$i]["DeckID2"];
                $gettotal[] = $scdeckinfo->GetTotalPayout($deckId2);
        }
        if ($btn->SubmittedValue == "Search"){ $pgcon->SelectedPage = 1; }
    }
        $pgcon->Initialize($itemsperpage, $count);
        $pgTransactionHistory = $pgcon->PreRender();
} 
else 
{
        $data = $scdeckinfo->GetDeckhistorywithprojid($_SESSION['projectid']);
        $count = count($data);
        $deckhistory = $scdeckinfo->GetDeckhistorywithprojidwithLimit($_SESSION['projectid'], $pgcon->SelectedItemFrom-1, $itemsperpage);
        $list = new ArrayList();
        $list->AddArray($deckhistory);
        if($list)
        {
             for ($i = 0; $i < count($deckhistory); $i++) 
                {
                    $deckId2 =  $deckhistory[$i]["DeckID2"];
                    $gettotal[] = $scdeckinfo->GetTotalPayout($deckId2);
                } 
        }
        if($itemsperpage < $count)
        {
                $pgcon->SelectedPage = 1;
                $pgcon->Initialize($itemsperpage, $count);
                $pgTransactionHistory = $pgcon->PreRender();
        }
        
}

?>
