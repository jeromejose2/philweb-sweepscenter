<?php
 /***********************************
     * Creation Date: May 4, 2012
     * Created by : Jfj     
     * Description: create Prize counter
     **********************************/
App::LoadControl("ComboBox");
App::LoadControl("Button");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename,"SCCC_DeckInfo");
App::LoadModuleClass($modulename, "SCCC_AccountSessions");
App::LoadModuleClass($modulename, "SCCC_Prizes");
App::LoadModuleClass($modulename,"SCCC_DeckDetails");

$scacctsessions = new SCCC_AccountSessions();
$deckinfo = new SCCC_DeckInfo();
$prizes = new SCCC_Prizes();
$deckdetails = new SCCC_DeckDetails();

$fproc = new FormsProcessor();

$getprizes = $prizes->GetAllPrizes();

$ddlprizes = new ComboBox("ddlprizes","ddlprizes","Prizes : ");
$ddlprizes->CssClass = "labelbutton5";
$listprizes = null;
$listprizes[] = new ListItem("Please Select","0",true);
$ddlprizes->Items = $listprizes;
$listprizes = new ArrayList();
$listprizes->AddArray($getprizes);
$ddlprizes->DataSource = $listprizes;
$ddlprizes->DataSourceText = "PrizeDescription";
$ddlprizes->DataSourceValue = "PrizeDescription";
$ddlprizes->DataBind();

$btnSearch = new Button("btnSearch","btnSearch","Search");
$btnSearch->CssClass = "labelbutton2";
$btnSearch->IsSubmit = true;
$btnSearch->Style = "width: 200px";
$btnSearch->Args = "onclick='javascript: return checkprizecounter();' ";

$btnRefresh = new Button("btnRefresh", "btnRefresh", "REFRESH");
$btnRefresh->CssClass = "labelbutton2";
$btnRefresh->Style = "width : 200px";
$btnRefresh->IsSubmit = true;

$btnOKAY = new Button("btnOKAY","btnOKAY", "OK");
$btnOKAY->CssClass = "labelbold2";
//$btnOKAY->Args = "onclick='document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'' ";

$fproc->AddControl($ddlprizes);
$fproc->AddControl($btnSearch);
$fproc->AddControl($btnRefresh);
$fproc->AddControl($btnOKAY);

$fproc->ProcessForms();

$projid = $_SESSION['projectid'];

$getactivedeck = $deckinfo->GetActiveDeck($projid);
$arractivedeck = new ArrayList();
$arractivedeck->AddArray($getactivedeck);
$createdby ="";

for($a=0;$a<count($arractivedeck);$a++) 
   { 
     $createdby = $arractivedeck[$a]["CreatedByAID"];
   } 

if ($createdby == 0)
{
    $getlivedeck = $deckinfo->GetLiveDeck2($projid);   
}
else
{
    $getlivedeck = $deckinfo->GetLiveDeck($projid);
}


//$getlivedeck = $deckinfo->GetLiveDeck($projid);
$arrlivedeck = new ArrayList();
$arrlivedeck->AddArray($getlivedeck);
    
        $deckno = "";
        $deckid = "";
        $date = "";
        $decksize = "";
        $winningsused = "";
        $entriesused = "";
        $remainingentries = "";
        $genby = "";
      
for ($a=0;$a<count($arrlivedeck);$a++)
        {
        $deckno = $arrlivedeck[$a]["DeckID"];
        $deckid = $arrlivedeck[$a]["BatchID"];
        $date = $arrlivedeck[$a]["StartDate"];
        $decksize = $arrlivedeck[$a]["DeckSize"];
        $winningsused = $arrlivedeck[$a]["WinningsUsed"];
        $entriesused = $arrlivedeck[$a]["EntriesUsed"];
        $remainingentries = $arrlivedeck[$a]["RemainingEntries"];
         //$percentage = $arrlivedeck[$a]["Percentage"];
        $genby = $arrlivedeck[$a]["GeneratedBy"];
        }
if($fproc->IsPostBack)
{
    if($btnSearch->SubmittedValue == "Search")
  {

$prizeID = $ddlprizes->SubmittedValue;          //session prize ID
$getprizetype = $prizes->GetPrizeType($prizeID);
$arrgetprize = $getprizetype[0];

$getdeck = $deckinfo->GetActiveDeck($projid);

$arraylivedeck = $getdeck[0];

$PrizeType = $arrgetprize["PrizeType"];
$PrizeValue = $arrgetprize["PrizeValue"];
$getDeckID = $arraylivedeck["DeckID"];

$getCardCount = $deckdetails->SelectCardCount($getDeckID,$PrizeType,$PrizeValue);
$arrgetcardcount = new ArrayList();
$arrgetcardcount->AddArray($getCardCount);

$selectECN = $prizes->GetECN($getDeckID,$PrizeType,$PrizeValue);

$prizequantity = $arrgetcardcount[0]["CardCount"];
$prizeused = $selectECN[0]["CountECN"];
$prizebalance = ($prizequantity - $prizeused);

$percentage = (( $prizeused / $prizequantity ) * 100);
   
    }
}

?>
