<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 4, 2012
 * GENERATE DECK CONTROLLER
 * Purpose: For creating new decks.
 */

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCC_AccountSessions");
App::LoadModuleClass($modulename, "SCCC_AuditLog");
App::LoadModuleClass($modulename, "SCCC_Prizes");
App::LoadModuleClass($modulename, "SCCC_ref_PrizeType");

$fproc = new FormsProcessor();
$scacctsessions = new SCCC_AccountSessions();
$scauditlog =  new SCCC_AuditLog();
$scprizes = new SCCC_Prizes();
$screfprizetype = new SCCC_ref_PrizeType();

$txtRep = new TextBox("txtRep", "txtRep");
$txtRep->Length = 100;
$txtRep->Style ="width: 300px";
$txtRep->Args="autocomplete='off' onkeypress='javascript: return AlphaNumeric(event);'";

$txtDeckSize = new TextBox("txtDeckSize", "txtDeckSize");
$txtDeckSize->Length = 10;
$txtDeckSize->Args="autocomplete='off' onkeypress='javascript: return isnumberkey(event);'";

$txtBudget = new TextBox("txtBudget", "txtBudget");
$txtBudget->Length = 10;
$txtBudget->Style = "width: 150px";
$txtBudget->Args="autocomplete='off' onkeypress='javascript: return isnumberkey(event);'";

$btnAddPrizeType = new Button("btnAddPrizeType", "btnAddPrizeType", "Add Prize Type");
$btnAddPrizeType->CssClass = "labelbutton2";
//$btnAddPrizeType->Args="onclick='javascript: return showAddPrizeType();'";

$btnAddPrize = new Button("btnAddPrize", "btnAddPrize", "Add New Value");
$btnAddPrize->CssClass = "labelbutton2";

$btnGenerate = new Button("btnGenerate", "btnGenerate", "Generate New Deck");
$btnGenerate->CssClass = "labelbutton2";
$btnGenerate->Style = "width: 200px";

/* For Adding New Prize Type */
$txtNewPrizeType = new TextBox("txtNewPrizeType", "txtNewPrizeType");
$txtNewPrizeType->Args="autocomplete='off' onkeypress='javascript: return AlphaNumeric(event);'";

$btnSavePrizeType = new Button("btnSavePrizeType", "btnSavePrizeType", "SAVE");
$btnSavePrizeType->CssClass = "labelbutton2";
$btnSavePrizeType->Style = "font-size: 0.8em";
$btnSavePrizeType->Args="onclick='javascript: return checkprizetypeinput();'";
$btnSavePrizeType->IsSubmit = true;

/* For Adding New Prize Value */
$ddlPrizeTypes = new ComboBox("ddlPrizeTypes", "ddlPrizeTypes");
$where = " WHERE Status = 1";
$ptopt = $screfprizetype->SelectByWhere($where);
$ptlist = new ArrayList();
$ptlist->AddArray($ptopt);
$ddlPrizeTypes->DataSource = $ptlist;
$ddlPrizeTypes->DataSourceText = "PrizeDescription";
$ddlPrizeTypes->DataSourceValue = "ID";
$ddlPrizeTypes->DataBind();

$txtNewValue = new TextBox("txtNewValue", "txtNewValue");
$txtNewValue->Style = "width: 90px";
$txtNewValue->Length = 10;
$txtValueQty = new TextBox("txtValueQty", "txtValueQty");
$txtNewValue->Args="autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";

$btnSavePrize = new Button("btnSavePrize", "btnSavePrize", "SAVE");
$btnSavePrize->CssClass = "labelbutton2";
$btnSavePrize->Args="onclick='javascript: return checkprizevalueinput();'";
$btnSavePrize->IsSubmit = true;

$fproc->AddControl($txtRep);
$fproc->AddControl($txtDeckSize);
$fproc->AddControl($txtBudget);
$fproc->AddControl($txtNewPrizeType);
$fproc->AddControl($btnAddPrizeType);
$fproc->AddControl($btnAddPrize);
$fproc->AddControl($btnGenerate);
$fproc->AddControl($btnSavePrizeType);
$fproc->AddControl($ddlPrizeTypes);
$fproc->AddControl($txtNewValue);
$fproc->AddControl($txtValueQty);
$fproc->AddControl($btnSavePrize);
$fproc->ProcessForms();

$deckprizes = $scprizes->GetDeckPrizes($_SESSION['projectid']);
$list = new ArrayList();
$list->AddArray($deckprizes);

if ($fproc->IsPostBack)
{
    if ($btnSavePrizeType->SubmittedValue == "SAVE")
    {
        $session = $scacctsessions->GetSessionDetails($_SESSION['sid']);
        if (count($session) > 0)
        {
            $prizetype = $screfprizetype->IfPrizeTypeExist($txtNewPrizeType->SubmittedValue, $_SESSION['projectid']);
            if (count($prizetype) == 0)
            {
                $screfprizetype->StartTransaction();
                $newprizetype["ProjectID"] = $_SESSION['projectid'];
                $newprizetype["PrizeDescription"] = $txtNewPrizeType->SubmittedValue;
                $newprizetype["Status"] = 1;
                $screfprizetype->Insert($newprizetype);
                if ($screfprizetype->HasError)
                {
                    $errormsg = $screfprizetype->getErrors();
                    $errormsgtitle = "ERROR!";
                    $screfprizetype->RollBackTransaction();
                }
                
                $scauditlog->StartTransaction();
                $scauditlogparam["SessionID"] = $_SESSION['sid'];
                $scauditlogparam["AccountID"] = $_SESSION['aid'];
                $scauditlogparam["TransDetails"] = "New Prize Type ".$txtNewPrizeType->SubmittedValue;
                $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $scauditlogparam["TransDateTime"] = "now_usec()";
                $scauditlog->Insert($scauditlogparam);
                if ($scauditlog->HasError)
                {
                    $errormsg = $scauditlog->getError();
                    $errormsgtitle = "ERROR!";
                    $scauditlog->RollBackTransaction();
                }
                
                $screfprizetype->CommitTransaction();
                $scauditlog->CommitTransaction();
                
                $successmsg = "Successfully created!";
                $successmsgtitle = "Successful";
                
            } else {
                $errormsg = "Prize type already exist.";
                $errormsgtitle = "ERROR!";
            }
        } else {
            $errormsg = "User Session does not exist.";
            $errormsgtitle = "ERROR!";
        }
    }
    
    if ($btnSavePrize->SubmittedValue == "SAVE")
    {
        $session = $scacctsessions->GetSessionDetails($_SESSION['sid']);
        if (count($session) > 0)
        {
            $where = " WHERE PrizeType='".$ddlPrizeTypes->SubmittedValue."' AND Denomination='".$txtNewValue->SubmittedValue."' AND ProjectID='".$_SESSION['projectid']."'";
            $prize = $scprizes->SelectByWhere($where);
            if (count($prize) == 0)
            {
                // Insert New Prize
                $scprizes->StartTransaction();
                $newprize["ProjectID"] = $_SESSION['projectid'];
                $newprize["PrizeType"] = $ddlPrizeTypes->SubmittedValue;
                $newprize["Denomination"] = $txtNewValue->SubmittedValue;
                $newprize["PrizeName"] = "$" . $txtNewValue->SubmittedValue;
                $newprize["PrizeDescription"] = "$" . $txtNewValue->SubmittedValue . " " . $ddlPrizeTypes->SelectedText;
                $newprize["DateCreated"] = 'now_usec()';
                $newprize["CreatedByAID"] = $_SESSION['aid'];
                $newprize["Status"] = 1;
                $scprizes->Insert($newprize);
                if ($scprizes->HasError)
                {
                    $errormsg = $scprizes->getErrors();
                    $errormsgtitle = "ERROR!";
                    $scprizes->RollBackTransaction();
                }
                
                // Log to Audit Trail
                $scauditlog->StartTransaction();
                $scauditlogparam["SessionID"] = $_SESSION['sid'];
                $scauditlogparam["AccountID"] = $_SESSION['aid'];
                $scauditlogparam["TransDetails"] = "New Prize " . "$" . $txtNewValue->SubmittedValue . " " . $ddlPrizeTypes->SelectedText;
                $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $scauditlogparam["TransDateTime"] = "now_usec()";
                $scauditlog->Insert($scauditlogparam);
                if ($scauditlog->HasError)
                {
                    $errormsg = $scauditlog->getError();
                    $errormsgtitle = "ERROR!";
                    $scauditlog->RollBackTransaction();
                }
                
                $scprizes->CommitTransaction();
                $scauditlog->CommitTransaction();
                
                $successmsg = "Successfully created!";
                $successmsgtitle = "Successful";
                
            } else {
                $errormsg = "Prize already exist.";
                $errormsgtitle = "ERROR!";
            }
        } else {
            $errormsg = "User Session does not exist.";
            $errormsgtitle = "ERROR!";
        }
    }
}

?>
