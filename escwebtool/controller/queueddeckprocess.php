<?php
/*
 * Date Modified: May 4, 2012
 * Edited by : Jfj
 */
require_once("../init.inc.php");
$modulename = "SweepsCenter";
App::LoadModuleClass($modulename,"SCCC_DeckInfo");
App::LoadModuleClass($modulename, "SCCC_DeckDetails");
App::LoadModuleClass($modulename, "SCCC_Accounts");
App::LoadModuleClass($modulename, "SCCC_AccountSessions");
App::LoadModuleClass($modulename,"SCCC_ref_ControlNumber");
App::LoadModuleClass($modulename,"SCCC_AuditLog");

APP::LoadControl("Button");
App::LoadControl("Hidden");

$deckinfo = new SCCC_DeckInfo();
$deckdetails = new SCCC_DeckDetails();
$accountinfo = new SCCC_Accounts();
$accountsession = new SCCC_AccountSessions();
$refcontrolnumber = new SCCC_ref_ControlNumber();
$scauditlog = new SCCC_AuditLog();

 $projid = 2; //$_SESSION['projectid'] = $projid
 $fproc = new FormsProcessor;
 
 $btndiscard = new Button("btndiscard","btndiscard","Discard");
 $btndiscard->IsSubmit = true;
 $btndiscard->CssClass="fontboldblack";
 $btndiscard->Style = "width:60px";
 
$btnYes = new Button("btnYes","btnYes","OKAY");
$btnYes->CssClass = "labelbutton2 okrepresentative";
$btnYes->IsSubmit = true;

$btnNo = new Button("btnNo","alertOk","CANCEL");
$btnNo->CssClass = "labelbutton2 okrepresentative";
$btnNo->Args = "onclick=javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';";
$btnNo->IsSubmit = true;


$hiddendeckid  = new Hidden("hiddendeckid","hiddendeckid","");
$hiddenbatchid = new Hidden("hiddenbatchid","hiddenbatchid","");
 
$btnOk = new Button("btnOk","alertOk","OK");
$btnOk->CssClass = "labelbutton2 okrepresentative";
$btnOk->Args = "onclick=javascript: document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';";
$btnOk->IsSubmit = true;

$btnRefresh = new Button("btnrefresh","refresh","REFRESH");
$btnRefresh->IsSubmit = TRUE;
$btnRefresh->CssClass ="labelbutton2";
$btnRefresh->Style = "width:200px";

$fproc->AddControl($btndiscard);
$fproc->AddControl($btnYes);
$fproc->AddControl($btnNo);
$fproc->AddControl($hiddendeckid);
$fproc->AddControl($hiddenbatchid);
$fproc->AddControl($btnOk);
$fproc->AddControl($btnRefresh);

       
 $fproc->ProcessForms();

    $quedeck = $deckinfo->QueuedDeckWithPrizeDescription($projid);
    $listquedeck = new ArrayList();
    $listquedeck->AddArray($quedeck);

    $createdby = "";

for($a=0;$a<count($listquedeck);$a++) 
   { 
        $createdby = $listquedeck[$a]["CreatedByAID"];

   }

if ($createdby == 0)
{
	$createdby = "mysql_job";
}
else
{
  $acctname = $accountinfo->SelectPassword($createdby);
  $acctlist = new ArrayList();
  $acctlist->AddArray($acctname);

  for($a=0;$a<count($acctlist);$a++) 
  { 
        $createdby = $acctlist[$a]["Username"];
  }  

}


if ($fproc->IsPostBack)
{
    $sid = session_id();
    $countdeck = $deckinfo->countDeckID($projid);
    $boolrollback = false;
    //$deckno = $hiddendeckid->Text;
  
if ($btnYes->SubmittedValue == "OKAY")
{
   $updatedeck = $deckinfo->UpdateDeckInfo($hiddendeckid->SubmittedValue);
     if($deckinfo->HasError)
          {
                 $boolrollback = true;
          }
      else
          {
          $refcontrolnumber->StartTransaction();
          $truncateref = $refcontrolnumber->Truncate();
          $insertdeck = $refcontrolnumber->InsertDeck();
          $refcontrolnumber->CommitTransaction();
           // Log to audit trail
            $scauditlog->StartTransaction();
            $scauditlogparam["SessionID"] = $sid;
            $scauditlogparam["AccountID"] = $aid;
            $scauditlogparam["TransDetails"] = "Discard Deck No: ".$hiddendeckid->SubmittedValue;
            $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $scauditlogparam["TransDateTime"] = "now_usec()";
            $scauditlog->Insert($scauditlogparam);
            if ($scauditlog->HasError)
            {
                $boolrollback = true;
            }else
            {
            $deckinfo->StartTransaction();
            $updatedeck1 = $deckinfo->UpdateDeckInfoandRefControlNumber();
            $updatedeck2 = $deckinfo->UpdateDeckInfobyStatus();
            $succes_msg="Successfully discarded.";
            }
          
       if ($boolrollback == true)
         {
             $deckinfo->RollBackTransaction();
             $refcontrolnumber->RollBackTransaction();
          
         }else
         {
             $deckinfo->CommitTransaction();
             $scauditlog->CommitTransaction();
             
         }
          }
}
$deckdigits = $hiddendeckid->SubmittedValue;
$deckdigits = str_pad($deckdigits, 3, "0", STR_PAD_LEFT);
   
if ($btndiscard->SubmittedValue == "Discard")
{ 
   $adduser_msg = "You are about to discard a queued deck with Deck ID: ".$deckdigits.". This deck will be removed from the system and can never be used again. Do you wish to continue?";
} 

}

?>
