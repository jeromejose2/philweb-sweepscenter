<?php 
require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCC_Accounts");
App::LoadModuleClass($modulename, "SCCC_AccountSessions");
App::LoadModuleClass($modulename, "SCCC_AuditLog");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadCore("PHPMailer.class.php");

$fproc = new FormsProcessor();
$scaccounts = new SCCC_Accounts();
$scacctsessions = new SCCC_AccountSessions();
$scauditlog = new SCCC_AuditLog();

$txtUsername = new TextBox("txtUsername", "txtUsername", "Username: ");
$txtUsername->ShowCaption = true;
$txtUsername->Length = 20;
$txtUsername->Args="autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtPassword = new TextBox("txtPassword", "txtPassword", "Password: ");
$txtPassword->ShowCaption = true;
$txtPassword->Password = true;
$txtPassword->Length = 20;
$txtPassword->Args="autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtEmail = new TextBox("txtEmail","txtEmail", "Email");
$txtEmail->Style = "width: 300px";
$txtEmail->Args="onkeypress='javascript: return verifyEmail(event);'";

$btnSubmit = new Button("btnSubmit", "btnSubmit", "LOG-IN");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Args="onclick='javascript: return checklogin();'";
$btnSubmit->IsSubmit = true;

$btnReset = new Button("btnReset", "btnReset", "RESET");
$btnReset->CssClass = "labelbutton2";
$btnReset->Args="onclick='javascript: return redirect();'";
$btnReset->IsSubmit = true;

$fproc->AddControl($txtUsername);
$fproc->AddControl($txtPassword);
$fproc->AddControl($txtEmail);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnReset);

$fproc->ProcessForms();


if ($fproc->IsPostBack)
{    
    if ($btnSubmit->SubmittedValue == "LOG-IN")
    {
        $username = mysql_escape_string(trim($txtUsername->Text));
        $password = mysql_escape_string(trim($txtPassword->Text));
        
        $_SESSION['sid'] = session_id();
        
        //check if account exists      
        $arrAccts = $scaccounts->SelectAID($username, $password);
        if (count($arrAccts) == 1)
        {
            $arrDtls = $arrAccts[0];
            $aid = $arrDtls["ID"];
            $accttype = $arrDtls["AccountType"];
            $status = $arrDtls["Status"];
            $projid = $arrDtls["ProjectID"];
            $_SESSION['aid'] = $aid;
            $_SESSION['projectid'] = $projid;
            $_SESSION['accttype'] = $accttype;
            $sessionpassword = $arrDtls["Password"];
            $_SESSION['password'] = $sessionpassword;
            $activeEmail = $arrDtls["Email"];
            $_SESSION["Email"] = $activeEmail;
            // Check Project ID
            if ($projid == 2)
            {
                // Check if User Account is Active
                if ($status == 1)
                {
                    $scacctsessions->StartTransaction();
                    
                    // Check for Existing Session
                    $arrAcctSessions = $scacctsessions->IfExists($aid);
                    if (count($arrAcctSessions) > 0)
                    {
                        // End Current Active Session
                        $scupdateparam["DateEnd"] = "now_usec()";
                        $scupdateparam["AccountID"] = $aid;
                        $scupdateacctsession = $scacctsessions->UpdateByArray($scupdateparam);
                        if ($scacctsessions->HasError)
                        {
                            $errormsg = $scacctsessions->getError();
                            $errormsgtitle = "ERROR!";
                            $scacctsessions->RollBackTransaction();
                        }                       
                    }
                    
                    // Insert into Account Session
                    $scacctsessionsparam["SessionID"] = $_SESSION['sid'];
                    $scacctsessionsparam["AccountID"] = $_SESSION['aid'];
                    $scacctsessionsparam["TransDate"] = "now_usec()";
                    $scacctsessionsparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                    $scacctsessionsparam["DateStart"] = "now_usec()";
                    $scacctsessionsparam["DateEnd"] = "0";
                    $scacctsessions->Insert($scacctsessionsparam);
                    if ($scacctsessions->HasError)
                    {
                        $errormsg = $scacctsessions->getError();
                        $errormsgtitle = "ERROR!";
                        $scacctsessions->RollBackTransaction();
                    }

                    // Log to Audit Trail
                    $scauditlog->StartTransaction();
                    $scauditlogparam["SessionID"] = $_SESSION['sid'];
                    $scauditlogparam["AccountID"] = $_SESSION['aid'];
                    $scauditlogparam["TransDetails"] = "Login Account ID: ".$_SESSION['aid'];
                    $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                    $scauditlogparam["TransDateTime"] = "now_usec()";
                    $scauditlog->Insert($scauditlogparam);
                    if ($scauditlog->HasError)
                    {
                        $errormsg = $scauditlog->getError();
                        $errormsgtitle = "ERROR!";
                        $scauditlog->RollBackTransaction();
                    }
                    
                    $scauditlog->CommitTransaction();
                    $scacctsessions->CommitTransaction(); 
                    
                    if ($accttype == 1 ||$accttype == 3)
                    {
                        URL::Redirect('template.php?page=deckcounter');
                    }
                    else
                    {
                        URL::Redirect('template.php?page=prizecounter');
                    }
                }
                else
                {
                    $errormsg = "Account is not active.";
                    $errormsgtitle = "ERROR!";               
                }
            } else {    
                // If Project ID is not ESC
                $errormsg = "Your account is not allowed to login.";
                $errormsgtitle = "INVALID!"; 
            }
        }
        else
        {
            $errormsg = "You have entered an invalid account information.";
            $errormsgtitle = "Invalid Log In!";
        }
    }   
    
    if ($btnReset->SubmittedValue == "RESET")
    {
        $emailaddr = $txtEmail->SubmittedValue;
        
        session_regenerate_id();
        
        $scaccounts->StartTransaction();
        
        $where = " WHERE Email = '$emailaddr' AND Status = '1'";
        $userdetails = $scaccounts->SelectByWhere($where);
        $id = $userdetails[0]["ID"];
        $name = $userdetails[0]["Username"];
        $newpass = substr(session_id(),0,8);
        
        if($id != null && $name != null)
        {
        // Update password
        $updateAccount["ID"] = $id;
        $updateAccount["Password"] = MD5($newpass);
        $scaccounts->UpdateByArray($updateAccount);
        if ($scaccounts->HasError)
        {
            $errormsg = $scaccounts->getErrors();
            $errormsgtitle = "ERROR!";
            $scaccounts->RollBackTransaction();
        }
        $scaccounts->CommitTransaction();
        
        // Sending of Email
        $pm = new PHPMailer();
        $pm->AddAddress($emailaddr, $name);
        
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        $folder = $_SERVER["REQUEST_URI"];
        $folder = substr($folder,0,strrpos($folder,'/') + 1);
        if ($_SERVER["SERVER_PORT"] != "80") 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
        } 
        else 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].$folder;
        }
        
        $pm->IsHTML(true);

        $pm->Body = "Good day!<br/><br/>
            This is to inform you that your password has been reset on this date ".date("m/d/Y")." and time ".date("H:i:s").". Here are your new Account Details:<br/><br/>
            Username: <b>".$name."</b><br/>
            Password: <b>".$newpass."</b><br/><br/>".
        "For security purposes, please change this system-generated password upon log in at " . $pageURL ."<br/><br/>".
        "If you didn't perform this procedure, please report this to our Customer Service email at support.gu@philwebasiapacific.com."."<br/><br/>
         Regards,<br/><br/>The Sweeps Center";

        $pm->From = "noreply@thesweepscenter.com";
        $pm->FromName = "The Sweeps Center";
        $pm->Host = "localhost";
        $pm->Subject = "NOTIFICATION FOR NEW PASSWORD";
        $email_sent = $pm->Send();
        }
        if($email_sent)
        {
            $errormsg = "Your new password has been sent to your e-mail account. For security purpose, please change this system-generated password upon log in.";
            $errormsgtitle = "Successful Reset of Password!";
        }
        else
        {
            if($id == null && $name == null){
            $errormsg2 = "You have entered an invalid e-mail address. Please try again.";
            $errormsgtitle = "Invalid E-Mail";
            }else{
            $errormsg = "An error occurred while sending the email to your email address";
            $errormsgtitle = "ERROR!";
            }
        }
    }
}
?>
