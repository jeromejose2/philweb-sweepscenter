<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 10, 2012
 * GENERATE DECK CONTROLLER
 * Purpose: For creating new decks.
 */

if (!isset($_SESSION)) {
    session_start();
}

//error_reporting(E_ALL);
//ignore_user_abort(true);                                                     
//set_time_limit(0); 

$isprocessed = 0;
$deckid = 0;
$prizelist = '';
$issuccessful = false;
$elements = $_GET['elementid'];
$prizes = $_GET['prizes'];
$prizename = $_GET['prizename'];
$quantities = $_GET['quantities'];
$ctr = $_GET['ctr'];
$rep = $_GET['rep'];
$projectid = $_GET['proj'];
$temp_prizes = $prizes;
$temp_prizename = $prizename;
$temp_qty = $quantities;
$temp_elements = $elements;

for ($x = 0; $x < $ctr; $x++) {
    $temp_qty2 = substr($temp_qty, 0, strpos($temp_qty, ','));
    $temp_elements2 = substr($temp_elements, 0, strpos($temp_elements, ','));
    $temp_prizename2 = substr($temp_prizename, 0, strpos($temp_prizename, ','));
    $temp_prizes2 = substr($temp_prizes, 0, strpos($temp_prizes, ','));

    //$prizelist = $prizelist . '(' . $temp_qty2 . ') ' . $temp_prizename2 . '<br />';
    $prizelist = $prizelist . '(' . $temp_qty2 . ') ' . '$'. $temp_prizes2 .' '.$temp_prizename2 . '<br />';
    
    $temp_qty = substr($temp_qty, strpos($temp_qty, ',') + 1);
    $temp_prizename = substr($temp_prizename, strpos($temp_prizename, ',') + 1);
    $temp_prizes = substr($temp_prizes, strpos($temp_prizes, ',') + 1);
    $temp_elements = substr($temp_elements, strpos($temp_elements, ',') + 1);
}

$_SESSION['decksize'] = $_GET['decksize'];
$_SESSION['prizelist'] = $prizelist;
$try = 1;
if (isset($_POST) && count($_POST) > 0) {
    if ($try == 1) {
        $try++;

//var_dump($_SESSION['sid']);echo "<br/>";    //<b>53</b>
//var_dump($_SESSION['aid']);echo "<br/>";//38
//var_dump($projectid);echo "<br/>";//2
//var_dump($_SESSION['decksize']);echo "<br/>";//10000
//var_dump($rep);echo "<br/>";//srtyrty
//var_dump($elements);echo "<br/>";//7
//var_dump($prizes);echo "<br/>";//1
//var_dump($quantities);echo "<br/>";//10000
//var_dump($ctr);echo "<br/>";  // "1"
//exit();
        generatedeck($_SESSION['sid'], $_SESSION['aid'], $projectid, $_SESSION['decksize'], $rep, $elements, $prizes, $quantities, $ctr);

        
        
        
    } else {
        break;
    }
}

function generatedeck($sid, $aid, $projid, $decksize, $rep, $elements, $prize, $qty, $ctr) {
    require_once("../init.inc.php");

    $modulename = "SweepsCenter";
    App::LoadModuleClass($modulename, "SCCC_AccountSessions");
    App::LoadModuleClass($modulename, "SCCC_DeckInfo");
    App::LoadModuleClass($modulename, "SCCC_AuditLog");
    App::LoadModuleClass($modulename, "SCCC_ref_ControlNumber");

    $scacctsessions = new SCCC_AccountSessions();
    $scdeckinfo = new SCCC_DeckInfo();
    $scauditlog = new SCCC_AuditLog();
    $screfcontrolno = new SCCC_ref_ControlNumber();

    $boolrollback = false;

    $session = $scacctsessions->GetSessionDetails($sid);
    if (count($session) > 0) {
        $activedeck = $scdeckinfo->CheckActiveDeck($projid);
        if (count($activedeck) == 0) {
            $status = 1;
            $dateactivated = 'now_usec()';
        } else {
            $status = 0;
            $dateactivated = '';
        }
        // Insert to deckinfo
        $scdeckinfo->StartTransaction();
        $deck["CardCount"] = $decksize;
        $deck["CreatedByAID"] = $aid;
        $deck["ProjectID"] = $projid;
        $deck["Status"] = $status;
        $deck["DateCreated"] = 'now_usec()';
        $deck["DateActivated"] = $dateactivated;
        $deck["Option1"] = $rep;
        $scdeckinfo->Insert($deck);
        if ($scdeckinfo->HasError) {
            $boolrollback = true;
        } else {
            $scdeckinfo->CommitTransaction();

            // Generate new deck table
            $lastinsertdeckid = $scdeckinfo->LastInsertID;
           
            $scdeckinfo->GenerateNewDeck($lastinsertdeckid);
            
            if ($scdeckinfo->HasError) {
                $boolrollback = true;
            } else {
                // Log to audit trail
                $scauditlog->StartTransaction();
                $scauditlogparam["SessionID"] = $sid;
                $scauditlogparam["AccountID"] = $aid;
                $scauditlogparam["TransDetails"] = "Generate Deck ID " . $lastinsertdeckid . " for Project ID " . $projid;
                $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $scauditlogparam["TransDateTime"] = "now_usec()";
                $scauditlog->Insert($scauditlogparam);
                if ($scauditlog->HasError) {
                    $boolrollback = true;
                } else {
                    $scauditlog->CommitTransaction();
                   
                    // Insert to ref_controlnumber
                    $screfcontrolno->StartTransaction();
                    $screfcontrolno->Truncate();
                    $data = $scdeckinfo->GetDeckID();
                    $screfcontrolno->CommitTransaction();
                    for ($i = 0; $i < count($data); $i++) 
                    {
                        $ctrnum["DeckID"] = $data[$i]["DeckID"];
                        $screfcontrolno->Insert($ctrnum);
                        if ($screfcontrolno->HasError) {
                            $boolrollback = true;
                        } else {
                            
                            $scdeckinfo->UpdateDeckbyControlNo();
                        }
                    }
                    if ($scdeckinfo->HasError) {
                        $boolrollback = true;
                        $scdeckinfo->RollBackTransaction();
                        $scauditlog->RollBackTransaction();
                        $screfcontrolno->RollBackTransaction();
                    } else {
                        $prizes = preg_replace('/[\$]/', '', $prize);

                        generateprizelist($projid, $lastinsertdeckid, $decksize, $elements, $prizes, $qty, $ctr);
                    }
                }
            }
        }
    }
}

function generateprizelist($projid, $deckid, $decksize, $elements, $prizes, $qty, $ctr) {
  
 
    require_once("../init.inc.php");

    $modulename = "SweepsCenter";
    App::LoadModuleClass($modulename, "SCCC_Prizes");
    App::LoadModuleClass($modulename, "SCCC_DeckDetails");

    $scprizes = new SCCC_Prizes();
    $scdeckdetails = new SCCC_DeckDetails();

    $tmp_qty = $qty;
    $tmp_prizes = $prizes;
    $tmp_elements = $elements;
 
    $scdeckdetails->StartTransaction();
    for ($x = 0; $x < $ctr; $x++) {
        $qtys = substr($tmp_qty, 0, strpos($tmp_qty, ','));
        $prizetypes = substr($tmp_elements, 0, strpos($tmp_elements, ','));
        $values = substr($tmp_prizes, 0, strpos($tmp_prizes, ','));
        $tmp_qty = substr($tmp_qty, strpos($tmp_qty, ',') + 1);
        $tmp_prizes = substr($tmp_prizes, strpos($tmp_prizes, ',') + 1);
        $tmp_elements = substr($tmp_elements, strpos($tmp_elements, ',') + 1);

        $pd = $scprizes->GetPrizeDescription($projid, $prizetypes, $values);

        $deckdetails["DeckID"] = $deckid;
        $deckdetails["ProjectID"] = $projid;
        $deckdetails["PrizeType"] = $prizetypes;
        $deckdetails["PrizeValue"] = $values;
        $deckdetails["PrizeDescription"] = $pd[0]["PrizeDescription"];
        $deckdetails["CardCount"] = $qtys;
        $array[] = $deckdetails;
    }
    $scdeckdetails->InsertMultiple($array);
    if ($scdeckdetails->HasError) {
        $scdeckdetails->RollBackTransaction();
    } else {
        $scdeckdetails->CommitTransaction();
        
        

        generatecards($deckid, $projid, $decksize, $ctr, $elements, $prizes, $qty);
    }
}

function generatecards($deckid, $projid, $decksize, $ctr1, $elements, $prizes, $qty) {
    require_once("../init.inc.php");

    $modulename = "SweepsCenter";
    App::LoadModuleClass($modulename, "SCCC_DeckInfo");
    App::LoadModuleClass($modulename, "SCCC_Accounts");
    App::LoadCore("PHPMailer.class.php");

    $scdeckinfo = new SCCC_DeckInfo();
    $scaccounts = new SCCC_Accounts();

    $id = $deckid;
    $deckid2 = str_pad($deckid, 3, "0", STR_PAD_LEFT);

    $vchr_CharsToUse = "ABCDEFGHJKMNPQRSTUVWXYZ0123456789";
    $int_RandAlphaLength = STRLEN($vchr_CharsToUse);
    $length = 10;
    $vchr_ECN = "";

    $projectid = str_pad($projid, 2, "0", STR_PAD_LEFT);
    $cardcount = $decksize;
    $ctr = $ctr1;

    $arrPrizeType = "";
    $arrPrizeDenom = "";
    $arrQuantity = "";
    $arrECN = "";
    $winningcardcount = 0;
    $currentcounter = 1;

    $tempPrizeType = $elements;
    $tempPrizeType2 = '';
    $tempDenomination = $prizes;
    $tempDenomination2 = '';
    $tempQuantity = $qty;
    $tempQuantity2 = '';

    for ($x = 1; $x <= $ctr; $x++) {
        $tempPrizeType2 = substr($tempPrizeType, 0, strpos($tempPrizeType, ','));
        $arrPrizeType[$x - 1] = $tempPrizeType2;
        $tempPrizeType = substr($tempPrizeType, strpos($tempPrizeType, ',') + 1);
        $tempDenomination2 = substr($tempDenomination, 0, strpos($tempDenomination, ','));
        $arrPrizeDenom[$x - 1] = $tempDenomination2;
        $tempDenomination = substr($tempDenomination, strpos($tempDenomination, ',') + 1);
        $tempQuantity2 = substr($tempQuantity, 0, strpos($tempQuantity, ','));
        $arrQuantity[$x - 1] = $tempQuantity2;
        $tempQuantity = substr($tempQuantity, strpos($tempQuantity, ',') + 1);
    }

    for ($x = 0; $x < $ctr; $x++) {
        if ($arrPrizeType[$x] == 1 || $arrPrizeType[$x] == 6) { // cash prize type
            if ($arrPrizeDenom[$x] == 0) {
                // non-winning
                for ($y = 0; $y < $arrQuantity[$x]; $y++) {
                    $vchr_ECN = "";
                    for ($p = 0; $p < $length; $p++) {
                        $vchr_ECN .= ($p % 2) ? $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1))] : $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1))];
                    }

                    $ECN["DeckID"] = $deckid2;
                    $ECN["ECN"] = str_pad($deckid, 3, "0", STR_PAD_LEFT) . $vchr_ECN;
                    $ECN["IsWinning"] = 0;
                    $ECN["PrizeType"] = $arrPrizeType[$x];
                    $ECN["PrizeValue"] = $arrPrizeDenom[$x];
                    $arrECN[] = $ECN;
                    $currentcounter += 1;
                }
            } else {
                // winning
                for ($y = 0; $y < $arrQuantity[$x]; $y++) {
                    $vchr_ECN = "";
                    for ($p = 0; $p < $length; $p++) {
                        $vchr_ECN .= ($p % 2) ? $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1))] : $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1))];
                    }
                    $ECN["DeckID"] = $deckid2;
                    $ECN["ECN"] = str_pad($deckid, 3, "0", STR_PAD_LEFT) . $vchr_ECN;
                    $ECN["IsWinning"] = 1;
                    $ECN["PrizeType"] = $arrPrizeType[$x];
                    $ECN["PrizeValue"] = $arrPrizeDenom[$x];
                    $arrECN[] = $ECN;
                    $currentcounter += 1;
                }
            }
        } else {//non-cash prize type
            for ($y = 0; $y < $arrQuantity[$x]; $y++) {
                $vchr_ECN = "";
                for ($p = 0; $p < $length; $p++) {
                    $vchr_ECN .= ($p % 2) ? $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1))] : $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1))];
                }
                $ECN["DeckID"] = $deckid2;
                $ECN["ECN"] = str_pad($deckid, 3, "0", STR_PAD_LEFT) . $vchr_ECN;
                $ECN["IsWinning"] = 1;
                $ECN["PrizeType"] = $arrPrizeType[$x];
                $ECN["PrizeValue"] = $arrPrizeDenom[$x];
                $arrECN[] = $ECN;
                $currentcounter += 1;
            }
        }
    }

    $keys = array_keys($arrECN);
    shuffle($keys);
    $strECN = "";

    for ($i = 0; $i < count($keys); $i++) {
        $currentkey = $keys[$i];
        $newECN[] = $arrECN[$currentkey];
        $strECN .= $arrECN[$currentkey]["DeckID"] . "," . $arrECN[$currentkey]["ECN"] . "," . $arrECN[$currentkey]["IsWinning"] . "," . $arrECN[$currentkey]["PrizeType"] . "," . $arrECN[$currentkey]["PrizeValue"] . "\r\n";
    }
    $arrECN = $newECN;

    
   
    
    $abspath = dirname(__FILE__) . "/";
   
    $fp = fopen($abspath . 'generatedcards.csv', 'w');
    foreach ($arrECN as $fields) {
        fputcsv($fp, $fields);
    }
    fclose($fp);

    $handle = fopen($abspath . "generatedcards.csv", "r");
  
    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        $sql = 'INSERT INTO deck_' . $id . ' (DeckID,ECN,IsWinning,PrizeType,PrizeValue,Status) VALUES (\'' . $data[0] . '\',\'' . $data[1] . '\',\'' . $data[2] . '\',\'' . $data[3] . '\',\'' . $data[4] . '\',\'0\')';
        $scdeckinfo->InsertToDeck($sql);
    }
    if ($scdeckinfo->HasError) {
        $scdeckinfo->RollBackTransaction();
    } else {
       
        $isprocessed = 1;
        $issuccessful = true;
    }

    fclose($handle);
    $myFile = $abspath . "generatedcards.csv";
    unlink($myFile);

    if ($issuccessful) { // then EMAIL NOTIFICATION
        $user = $scaccounts->SelectByID($_SESSION['aid']);
        $name = $user[0]["Username"];
        $emailaddr = $user[0]["Email"];
        
      
        $pm = new PHPMailer();
        $pm->AddAddress($emailaddr, $name);

        $pageURL = 'http';
        if (!empty($_SERVER['HTTPS'])) {$pageURL .= "s";}
        $pageURL .= "://";
        $folder = $_SERVER["REQUEST_URI"];
        $folder = substr($folder,0,strrpos($folder,'/') + 1);
        if ($_SERVER["SERVER_PORT"] != "80") 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
        } 
        else 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].$folder;
        }
       
        $pm->IsHTML(true);
        
        $pm->Body = "Good Day!<br /><br />
            This is to inform you that a new e-scratch card deck has been generated with the following<br />
            details:<br />
            <b>Deck Size: " . $_SESSION['decksize'] . "</b><br />
            <b>Prizes: <br />" . $_SESSION['prizelist'] . "</b><br /><br />
            The newly generated deck is now queued for next succeeding usage. To check queued decks, log<br />
            on to your e-scratch card admin tool account at http://www.thesweepscenter.com/ESC_Webtool.<br /><br />
            Regards,
            <br /><br />
            The Sweeps Center";

        $pm->From = "noreply@thesweepscenter.com";
        $pm->FromName = "DECK MANAGEMENT CENTER";
        $pm->Host = "localhost";
        $pm->Subject = "NOTIFICATION FOR NEW DECK GENERATION";
        $email_sent = $pm->Send();
        $msg = "You have successfully created a new deck. Your new deck attributes are saved for the next automatic deck generation.";
    } else {
        $msg = "Transaction Failed.";
    }

    if ($isprocessed == 1) {
        ?>
        <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
        <div style="background-color:#000; height:30px; text-align:center; font-weight:bold; font-size:1.5em; vertical-align:center;	border-color:#000000;	border-width:thin; border-style:solid;">
        </div>
        <div id="divresult" style="text-align:center; font-weight:bold; font-size:1.5em; vertical-align:center;	border-color:#000000;	border-width:thin; border-style:solid;">
        <?php echo $msg; ?><br/><br/>
            <a href="template.php?page=queueddeck" class="labelbutton2">OKAY</a>
        </div>
        <?php
    }
}

if (count($_POST) == 0):
    ?>

    <form id="popupForm" method="post" name="popupForm" action="">
        <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
        <div style="background-color:	#000; color:#fff; height:30px; text-align:center; font-weight:bold; font-size:1.5em; vertical-align:center; border-color:#000000; border-width:thin; border-style:solid;">
            Generate New Deck
        </div>
        <div style="text-align:center; font-weight:bold; font-size:1.5em; vertical-align:center;	border-color:#000000;	border-width:thin; border-style:solid;">
            You are about to generate a new deck with the following attributes:
            <br />
            Deck Size: <?php echo number_format($_GET['decksize']); ?>
            <br />
            Prizes:
            <br />
    <?php
    echo $prizelist;
    ?>
            <div>
                Do you wish to continue?
                <br /><br />
                <input id="btnSaveGenNewDeck" name="btnSaveGenNewDeck" type="submit" value="OKAY" class="labelbutton2" />
                <input id="btnCancel" name="btnCancel" type="submit" value="CANCEL" class="labelbutton2" />
            </div> 
        </div>
    </form>
<?php endif; ?>
