
function checkinput() {
    var lname = document.getElementById("txtLName").value;
    var fname = document.getElementById("txtFName").value;
    var address = document.getElementById("txtAddress").value;
    var email = document.getElementById("txtEmail").value;
    var bday = document.getElementById("hidbday").value;

//    var currDate = new Date();
    lname = lname.replace(/^\s+|\s+$/, '');
    fname = fname.replace(/^\s+|\s+$/, '');
    address = address.replace(/^\s+|\s+$/, '');
    email = email.replace(/^\s+|\s+$/, '');
    var regEx = /[a-zA-Z0-9]/;
    var date_array = bday.split("-");
    var mon = date_array[1];
    var day = date_array[2];
    var year = date_array[0];
    addOrRemoveErrorFieldClass("lblLName", "txtLName");
    addOrRemoveErrorFieldClass("lblFName", "txtFName");
    addOrRemoveErrorFieldClass("lblBday", "hidbday");
    addOrRemoveErrorFieldClass("lblHomePhone", "txtHomePhone");
    addOrRemoveErrorFieldClass("lblMobilePhone", "txtMobilePhone");
    addOrRemoveErrorFieldClass("lblAddress", "txtAddress");
    addOrRemoveErrorFieldClass("lblEmail", "txtEmail");

    if (lname.length == 0)
    {

        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (fname.length == 0)
    {
        document.getElementById('light2').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if ( (bday == '--')|| (year == '0000') || (mon == '00') || (day == '00'))
    {
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    /*
     if (mon > (currDate.getMonth() + 1))
     {
     document.getElementById('light8').style.display='block';
     document.getElementById('fade').style.display='block';
     return false;
     }
     if (mon <= (currDate.getMonth() + 1))
     {
     if (day > currDate.getDate())
     {
     document.getElementById('light8').style.display='block';
     document.getElementById('fade').style.display='block';
     return false;
     }
     }
     */

    var today = new Date();
    var d = document.getElementById("hidbday").value;
    if (!/\d{4}\-\d{2}\-\d{2}/.test(d))
    { // check valid format
        showMessage();
        return false;
    }

    d = d.split("-");
    var byr = parseInt(d[0]);
    var nowyear = today.getFullYear();
    if (byr >= nowyear || byr < 1900)
    { // check valid year
        showMessage();
        return false;
    }
    var bmth = parseInt(d[1], 10) - 1; // radix 10!
    if (bmth < 0 || bmth > 11)
    { // check valid month 0-11
        showMessage();
        return false;
    }
    var bdy = parseInt(d[2], 10); // radix 10!
    var dim = daysInMonth(bmth + 1, byr);
    if (bdy < 1 || bdy > dim)
    { // check valid date according to month
        showMessage();
        return false;
    }

    var age = nowyear - byr;
    var nowmonth = today.getMonth();
    var nowday = today.getDate();
    var age_month = nowmonth - bmth;
    var age_day = nowday - bdy;

    if (age < 18)
    {
        document.getElementById('light8').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else if (age == 18 && age_month <= 0 && age_day < 0)
    {
        document.getElementById('light8').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    if ((document.getElementById("txtHomePhone").value.length == 0) && (document.getElementById("txtMobilePhone").value.length == 0))
    {
        document.getElementById('light3').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }


    if (address.length == 0)
    {
        document.getElementById('light4').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
     if (address.length > 300 )
    {
        document.getElementById('lightaddress').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (!regEx.test(address))
    {

        document.getElementById('light40').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (address.length > 300)
    {
        document.getElementById('light10').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    if (email.length == 0)
    {
        //var chkEmail;
        //chkEmail = "Please input a valid e-mail address.";
        //document.getElementById('txtEmail').innerHTML = chkEmail;
        document.getElementById('light6').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else
    {
        if (echeck(email) == false)
        {
            return false;
        }
    }

    return true;
}

function echeck(str)
{
    var at = "@"
    var dot = "."
    var lat = str.indexOf(at)
    var lstr = str.length
    var ldot = str.indexOf(dot)
    var chkEmail;

    if (str.indexOf(at) == -1) {
        //chkEmail = "Please enter a valid e-mail address.";
        //document.getElementById('txtEmail').innerHTML = chkEmail;
        document.getElementById('light9').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
        //chkEmail = "Please input a valid e-mail address.";
        //document.getElementById('txtEmail').innerHTML = chkEmail;
        document.getElementById('light9').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
        //chkEmail = "Please input a valid e-mail address.";
        //document.getElementById('txtEmail').innerHTML = chkEmail;
        document.getElementById('light9').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    if (str.indexOf(at, (lat + 1)) != -1) {
        //chkEmail = "Please input a valid e-mail address.";
        //document.getElementById('txtEmail').innerHTML = chkEmail;
        document.getElementById('light9').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
        //chkEmail = "Please input a valid e-mail address.";
        //document.getElementById('txtEmail').innerHTML = chkEmail;
        document.getElementById('light9').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    if (str.indexOf(dot, (lat + 2)) == -1) {
        //chkEmail = "Please input a valid e-mail address.";
        //document.getElementById('txtEmail').innerHTML = chkEmail;
        document.getElementById('light9').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    if (str.indexOf(" ") != -1) {
        //chkEmail = "Please input a valid e-mail address.";
        //document.getElementById('txtEmail').innerHTML = chkEmail;
        document.getElementById('light9').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }










    return true;
}

function isnumberkey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    else
        return true;
}

function isspckey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 32 && (charCode < 44 || charCode > 57) && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122))
        return false;
    else
        return true;
}

function elem(id)
{
    return document.getElementById(id);
}

function hasClass(ele, cls)
{
    return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}

function removeClass(ele, cls)
{
    if (hasClass(ele, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        ele.className = ele.className.replace(reg, ' ');
    }
}

function addClass(ele, cls) {
    if (!this.hasClass(ele, cls))
        ele.className += " " + cls;
}

function addOrRemoveErrorFieldClass(lblId, inputId)
{
    var lblElem = elem(lblId);
    var inputElem = elem(inputId);

    if (inputElem.value.replace(/^\s+|\s+$/, '') == '' || inputElem.value.replace(/^\s+|\s+$/, '') == '0000-00-00' || inputElem.value.replace(/^\s+|\s+$/, '') == '--') {
        addClass(lblElem, "errorField");
    } else {
        removeClass(lblElem, "errorField");
    }
    if (elem("txtMobilePhone").value.replace(/^\s+|\s+$/, '') != '')
    {
        removeClass(elem("lblHomePhone"), "errorField");

    }
    if (elem("txtHomePhone").value.replace(/^\s+|\s+$/, '') != '')
    {
        removeClass(elem("lblMobilePhone"), "errorField");

    }
}

function disableSpace(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 32)
        return false;

    return true;
}


