<?php
/**********************************
* Creation Date: May 4, 2012
* Edited by : Jfj     
* Description: Viewing deck counter
**********************************/
?>
<?php error_reporting(E_ALL);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Guam Sweepstakes Caf&#233; Free Entry</title>
<link rel="stylesheet" type="text/css" href="css/main.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script src="jscript/ypSimpleScrollC.js" type="text/javascript"></script>
<script type="text/javascript">
		var test  = new ypSimpleScroll("myScroll", 20, 0, 750, 350, 40, 700)

function checkform(){
    if(document.getElementById("terms").checked)
{
        document.getElementById('light3').style.display='none';document.getElementById('fade').style.display='none';
}
        else{
            document.getElementById('lightterms').style.display='block';
            document.getElementById('fade').style.display='block';
            //return false;
        }

}
</script>
</head>
<body onload="test.load(); document.getElementById('light3').style.display='block';document.getElementById('fade').style.display='block';">
    <div id="mainContainer">
    	<div id="banner">
        <div id="logo_landing"> <img src="images/theSweepsLogo.png" /></div>
    	</div>
        <div id="contentContainer3"  style="margin-top: -50px;">
            <div id="about_container">
            	<div class="aboutTitle" style="">WELCOME TO THE SWEEPS CENTER!</div>
                    <div id="myScrollContainer">
                        <div  id="myScrollContent">
                          <br />
                          The Sweeps Center is a one-stop managed sweepstakes shop that sells a wide variety of products and offers internet browsing services.  For every purchase you make, you get a chance to win up to US$ 3,000 cash instantly!<br/><br/>
                          We also offer a Free Entry service where you can avail an e – Sweeps Card for free.  All you need to do is visit and register to our website at <a href="www.thesweepscenter.com">www.thesweepscenter.com</a>. After completing the registration form and successfully submitting it, an auto-generated code will be sent to your registered email.  Print your code and go to any of the Sweeps Center branches near you to redeem your free e – Sweeps Card.  You can either choose your own card or let the computer choose it for you.<br/><br/>
                          If you pick a winning card, take note of your Transaction Reference ID.  Most prizes won can be instantly claimed at the Sweeps Center branch.<br/><br/>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/OnlineFreeEntry.png" alt=""/>
                          <br/><br/>
                          Visit the Sweeps Center at (location address) for a higher quality of Sweepstakes service that is more fun and rewarding!<br/><br />
                        </div>
                    </div>
            <div class="goToLaunchPad_quickContainer">
                <form action="registration.php" method="post" onsubmit="">
                    <div style="width:100%; text-align:center; margin-top: -30px;">
                      <br/>
                      <input type="image" name="btnLogin" id="btnLogin" accesskey="L" tabindex="3" title="Proceed" src="images/ProceedButton.png" alt="Login" style="width:150px; height:50px;" align="center" />
                      </div>
                  </form>
              </div>
            </div>
                  <div id="msg" style="float:right;margin-right:70px;margin-top: -400px;">
                    <a href="#" onmouseover="test.scrollNorth()" onmouseout="test.endScroll()">
                        <div style="color:#BDB76B;">
                            <img src="images/scroll_up.png" alt="^"border="2" style="border-color:#BDB76B;" />
                        </div>
                    </a>
                        <br/>

                    <a href="#" onmouseover="test.scrollSouth()" onmouseout="test.endScroll()">
                        <div style="color:#BDB76B; margin-top:5px">
                            <img src="images/scroll_down.png" alt="v"/>
                        </div>
                    </a>
            </div>
        </div>
        
<!--edited by jfj 06-28-2012-->
 <div id="footer">
            <div id="footerBox">
            	<div class="footerBox_left"></div>
              	<div class="footerBox_body">
                     <div class="under18"></div>
                     <div class="rules">
                       <a href="#" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</a>
                     </div>
                     <div class="terms">
                         <a href="#" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</a>
                     </div>
              	</div>
                <div class="footerBox_right"></div>
            </div>
        </div>

<!--edited by jfj 06-28-2012-->











        <div id="light" class="white_content2">
             <div id="mechanics" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';"></div>
           <?php include('mechanics.php') ?>
        </div>
        <div id="light2" class="white_content2">
          <div id="terms1" onclick="document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';"></div>
          <?php include('terms.php') ?>
        </div>

<div id="light3" class="white_content2">
        <?php include('terms2.php') ?>
        </div>

        <div id="fade" class="black_overlay"></div>
    </div>
        <div id="lightterms" class="termsandcondition">
        <div class="popupHeader"><b>TERMS AND CONDITION CONFIRMATION</b></div>
        <br/>
        Please confirm that you have read<br/><br/>the Terms &#38; Conditions.
        <br/><br/>
        <img src="images/okbutton.png" onclick="document.getElementById('lightterms').style.display='none';"/>
    </div>
    
        <div id="light_msg" style="text-align: center;font-size: 14pt;height: auto;" class="white_content">
        <div class="light_msgHeader"><b id="a"></b></div>
        <br/><br/>
        <div id="b" style=" text-align: center; padding: 5px;"><br/></div>
        <br/><br/>
        <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light_msg').style.display='none';document.getElementById('fade').style.display='none';"/>
    </div>

    <div id="fade" class="black_overlay"></div>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-26995949-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
    
    
    
    
