<?php
/*
 * @author JFJ 5-29-2012
 * Purpose   : controller for registration
 */
require_once("../init.inc.php");
$modulename       = "SweepsCenter";
APP::LoadModuleClass($modulename,"SCC_FreeEntry");
APP::LoadModuleClass($modulename,"SCCV_VoucherInfo");
APP::LoadModuleClass($modulename,"SCCV_AuditLog");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("Hidden");
$freeentry        = new SCC_FreeEntry();
$sccv_auditlog    = new SCCV_AuditLog();
$cafinovoucher    = new SCCV_VoucherInfo();
App::LoadCore("PHPMailer.class.php");
$registrationform = new FormsProcessor();

$txtLName           = new TextBox("txtLName","txtLName");
$txtLName->Length   = "30";
$txtLName->CssClass = "inputBoxEffect";
$txtLName->Args     = "onkeypress='javascript: return allowedKeysName(event);' onpaste='return false'";

$txtFname           = new TextBox("txtFName","txtFName");
$txtFname->Length   = "30";
$txtFname->CssClass = "inputBoxEffect";
$txtFname->Args     = "onkeypress='javascript: return allowedKeysName(event);' onpaste='return false'";


$txtHomePhone           = new TextBox("txtHomePhone","txtHomePhone");
$txtHomePhone->Length   = "20";
$txtHomePhone->CssClass = "inputBoxEffect";
$txtHomePhone->Args     = "onkeypress='javascript: return isnumberkey(event);' onpaste='return false'";

$txtMobilePhone           = new TextBox("txtMobilePhone","txtMobilePhone");
$txtMobilePhone->Length   = "20";
$txtMobilePhone->CssClass = "inputBoxEffect";
$txtMobilePhone->Args     = "onkeypress='javascript: return isnumberkey(event);' onpaste='return false'";

$txtEmail           = new TextBox("txtEmail","txtEmail");
$txtEmail->Length   = "100";
$txtEmail->CssClass = "inputBoxEffect";
$txtEmail->Args     = "onkeypress='javascript: return disableSpace(event);' onpaste='return false'";

$btnSubmit           = new Button("btnSubmit","btnSubmit","&nbsp");
$btnSubmit->CssClass = "submitCont";
$btnSubmit->Args     = "onclick='javascript: return checkinput();showData();'";
//$btnSubmit->Args     = "onclick = 'javascript: return showData();'";
$btnSubmit->Style    = "width: 100px; height: 40px; font-weight: bold;";
$btnSubmit->IsSubmit = true;

//$txtAddress            = new TextBox("txtAddress","txtAddress");
//$txtAddress->Multiline = true;
//$txtAddress->Rows      = 5;
//$txtAddress->Columns   = 30;
//$txtAddress->Args      = "onkeypress='javascript: return chkChar(event);' style='resize: none;'";
//$txtAddress->CssClass  = "messageBoxEffect";
$hiddenadd = new Hidden("hiddenadd","hiddenadd");
$hidbday       = new Hidden('hidbday','hidbday');
$hidbday->Text = '0000-00-00';

$registrationform->AddControl($txtLName);
$registrationform->AddControl($txtFname);
$registrationform->AddControl($txtHomePhone);
$registrationform->AddControl($txtMobilePhone);
$registrationform->AddControl($txtEmail);
//$registrationform->AddControl($txtAddress);
$registrationform->AddControl($hidbday);
$registrationform->AddControl($hiddenadd);

$registrationform->ProcessForms();

$surname   = $txtLName->SubmittedValue;
$givenname = $txtFname->SubmittedValue;
$homephone = $txtHomePhone->SubmittedValue;
$mobile    = $txtMobilePhone->SubmittedValue;
$email     = $txtEmail->SubmittedValue;
$address   = $_POST['txtAddress'];

$year  = '0000';
$month = '00';
$day   = '00';

$myCalendar = new tc_calendar("hidbday",true);
$myCalendar->setIcon("images/iconCalendar.gif");
$myCalendar->setDate($day,$month,$year);
$myCalendar->setPath("./");
$myCalendar->setYearInterval(1900,(date('Y') - 18));
$myCalendar->dateAllow('1900-01-01','$year-$month-$day');
$myCalendar->disabledDay("Sat");
$myCalendar->disabledDay("sun");

if ( $registrationform->IsPostBack )
{
    $myCalendar->setDate($_POST['hidbday_day'],$_POST['hidbday_month'],$_POST['hidbday_year']);
    $bday          = $_POST['hidbday_year'] . '-' . $_POST['hidbday_month'] . '-' . $_POST['hidbday_day'];
    $hidbday->Text = $bday;
    if ( isset($_POST["btnSubmit"]) )
    {

        if ( !eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$",$txtEmail->SubmittedValue) )
        {
            $error_msgtitle = "INSUFFICIENT DATA";
            $error_msg      = "Please enter correct email address.";
        }
        else
        {
            $emailcount = 0;
            if ( trim($txtEmail->SubmittedValue) == "" )
            {
                $emailcount = 0;
            }
            else
            {
                $freeentry->StartTransaction();
                $emailchecking      = $freeentry->CheckEmail($email);
                $emailcount         = $emailchecking[0][0];
                $checklnameandfname = $freeentry->CheckLNameandFNAme($txtLName->SubmittedValue,$txtFname->SubmittedValue);
                if ( $emailcount > 0 )
                {
                    $error_msgtitle = "MULTIPLE ENTRIES";
                    $error_msg      = "You have already availed of the Free Entry Electronic Sweepstakes Card for the day. You can only avail one free entry per day.";
                }
                else if ( $checklnameandfname[0][0] > 0 )
                {
                    $error_msgtitle = "MULTIPLE ENTRIES";
                    $error_msg      = "You have already availed of the Free Entry Electronic Sweepstakes Card for the day. You can only avail one free entry per day.";
                }
                if ( ($emailcount == 0) && ($checklnameandfname[0][0] == 0) )
                {

                    //call for the cafino_voucher
                    $cafinovoucher->StartTransaction();
                    $sccv_auditlog->StartTransaction();
                    $getvoucher                     = $cafinovoucher->SelectVoucher();
                    $vcode                          = $getvoucher[0]["VoucherNo"];
                    $date                           = "now_usec()";
                    $freeEntryType                  = 2;
                    //update voucher status = 4
                    $updatevcode                    = $cafinovoucher->UpdateVoucherStatus($freeEntryType,$date,$vcode);
                    //insert in cafino voucher audit log
                    $sccvauditparam["TransDetails"] = "Voucher Code :" . $vcode . ".";
                    $sccvauditparam["IPAddress"]    = $_SERVER['REMOTE_ADDR'];
                    $sccvauditparam["DateCreated"]  = "now_usec()";
                    $sccvauditparam["CreatedBy"]    = $givenname;
                    $sccv_auditlog->Insert($sccvauditparam);

//check for errors

                    if ( $sccv_auditlog->HasError || $cafinovoucher->HasError )
                    {
                        $error_msgtitle = "Error";
                        $error_msg      = "Error has occured" . $sccv_auditlog->getError() . "," . $cafinovoucher->getError();
                        $sccv_auditlog->RollBackTransaction();
                        $cafinovoucher->RollBackTransaction();
                    }
                    else
                    {
                        $sccv_auditlog->CommitTransaction();
                        $cafinovoucher->CommitTransaction();

                        //insert into free entry table
                        $surname                             = strtoupper($txtLName->SubmittedValue);
                        $givenname                           = strtoupper($txtFname->SubmittedValue);
                        $arrfreeentryparam["LName"]          = $surname;
                        $arrfreeentryparam["FName"]          = $givenname;
                        $arrfreeentryparam["BirthDate"]      = $bday;
                        $arrfreeentryparam["HomePhone"]      = $homephone;
                        $arrfreeentryparam["MobilePhone"]    = $mobile;
                        $arrfreeentryparam["Address"]        = $address;
                        $arrfreeentryparam["Email"]          = $email;
                        $arrfreeentryparam["VoucherCode"]    = $vcode;
                        $arrfreeentryparam["DateRegistered"] = $date;
                        $arrfreeentryparam["DatePlayed"]     = "";
                        $arrfreeentryparam["IsSent"]         = "1";
                        $arrfreeentryparam["SiteID"]         = null;
                        $freeentry->Insert($arrfreeentryparam);
                        if ( $freeentry->HasError )
                        {
                            $error_msgtitle = "Error";
                            $error_msg      = "Error in FreeEntry" . $freeentry->getError();
                            $freeentry->RollBackTransaction();
                        }
                        else
                        {
                            $freeentry->CommitTransaction();


                            // Sending of Email

                            $date = date("m/d/Y");
                            $time = date("h:i:s A");
                            $pm   = new PHPMailer();
                            $pm->AddAddress($email,$surname);

                            $pageURL = 'http';
                            if ( $_SERVER["HTTPS"] == "on" )
                            {
                                $pageURL .= "s";
                            }
                            $pageURL .= "://";
                            $folder = $_SERVER["REQUEST_URI"];
                            $folder = substr($folder,0,strrpos($folder,'/') + 1);
                            if ( $_SERVER["SERVER_PORT"] != "80" )
                            {
                                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $folder;
                            }
                            else
                            {
                                $pageURL .= $_SERVER["SERVER_NAME"] . $folder;
                            }
                            //date format DATE_FORMAT(a.TransDateTime,'%m/%d/%Y %h:%i:%s %p')
                            //date and time missing
                            $pm->IsHTML(true);

                            $pm->Body = "<HTML><BODY>						
		      Dear Mr./Ms. $surname,
		      <br /><br />
          Good Day!
          <br /><br />
		      We have received your registration for Sweeps Center's Free Entry promo on this date $date and time $time. 
          Your Free Entry Sweeps Code is <b>$vcode</b>.
			    <br /><br />
			    To claim your free sweepstakes entry, visit the nearest Sweeps Center and enter this code on the allocated Free Entry terminal.
	        <br /><br />
	        Please be informed that this Sweeps code will expire within three days from the time of registration. 
          You can only register for a Sweeps Free Entry once a day. 
		      <br /><br />
		     For further inquiries regarding the Sweeps Center, contact us at our toll free Customer Service hotline at 023385599 or email us at support.gu@philwebasiapacific.com.
		      <br /><br />
		      Regards,
		      <br /><br />
          Customer Support<br />
		      The Sweeps Center
		    </BODY></HTML>";

                            $pm->From     = "support@thesweepscenter.com";
                            $pm->FromName = "The Sweeps Center";
                            $pm->Host     = "localhost";
                            $pm->Subject  = "The Sweeps Center Registration for Free Entry Service";
                            $email_sent   = $pm->Send();
                            if ( $email_sent )
                            {
                                $msg = "You have successfully sent your registration for the Sweeps Center Free Entry. A Free Entry Code was sent to your e-mail address. Please visit any Sweeps Center branch and enter this code at allocated terminals to avail your one Free Entry Sweepstakes.";
                            }
                            else
                            {
                                $error_msgtitle = "Error";
                                $error_msg      = "An error occurred while sending the email to your email address";
                            }
                        }
                    }
                }
            }
        }
    }
//}
}
?>
