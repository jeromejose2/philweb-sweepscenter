<?php

/*
 * Added On : June 29, 2012
 * Created by : Jerome F. Jose
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_Terminals");
App::LoadModuleClass($modulename, "SCC_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCC_AdminAccounts");
App::LoadModuleClass($modulename, "SCC_AuditTrail");
App::LoadModuleClass($modulename, "SCC_TerminalSessions");
App::LoadModuleClass($modulename, "SCC_ref_Services");
App::LoadSettings("swcsettings.inc.php");

APP::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");

$fproc = new FormsProcessor();

$accounts = new SCC_Accounts();
$terminals = new SCC_Terminals();
$adminaccountsession = new SCC_AdminAccountSessions();
$adminaccounts = new SCC_AdminAccounts();
$audittrail = new SCC_AuditTrail();
$terminalsession = new SCC_TerminalSessions();
$refservices = new SCC_ref_Services();


$selectactive = $refservices->SelectActiveService();
$provider = $selectactive[0]['ID'];

$listddl = $accounts->SelectSiteIDbylist();
$ddlPosName = new ComboBox("ddlPosName", "ddlPosName");
$arrlist = new ArrayList();
$arrlist->AddArray($listddl);
$list = null;
$list[] = new ListItem("", "0", true);
$ddlPosName->Items = $list;
$ddlPosName->DataSource = $arrlist;
$ddlPosName->DataSourceText = "Name";
$ddlPosName->DataSourceValue = "SiteID";
$ddlPosName->DataBind();
$ddlPosName->Args = "onchange='javascript: return get_terminals_exceptfreeentry();'";

//$listddl = $accounts->SelectSiteIDbylist();
//$ddlPos = new ComboBox("ddlPos","ddlPos");
//$arrlist = new ArrayList();
//$arrlist->AddArray($listddl);
//$ddlPos->ClearItems();
//$list = null;
//$list[] = new ListItem("Select One","0",true);
//$ddlPos->Items = $list;
//$ddlPos->DataSource = $arrlist;
//$ddlPos->DataSourceText = "Name";
//$ddlPos->DataSourceValue = "SiteID";
//$ddlPos->DataBind();
//$txtTerminalName = new TextBox("ternme","ternme");
//$txtTerminalName->Length = "20";
//$txtTerminalName->Args = "onkeypress='javascript: return isAlphaNumericKey(event);'";

$ddlTerminalName = new ComboBox("ddlTerminalName", "ddlTerminalName");
$list = null;
$list[] = new ListItem("", "", true);
$ddlTerminalName->Items = $list;

$btnSearch = new Button("btn", "btn", "Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checking();' ";

$btnlogout = new button("btnlogout", "btnlogout", "Force Logout");
$btnlogout->IsSubmit = true;
$btnlogout->CssClass = "labelbutton2";
$btnlogout->Args = "onclick='javascript: return forcelogout();' ";

$btnYes = new button("btnYes", "btnYes", "YES");
$btnYes->IsSubmit = true;
$btnYes->CssClass = "labelbutton2";

$fproc->AddControl($ddlPosName);
$fproc->AddControl($ddlTerminalName);
$fproc->AddControl($btnSearch);
$fproc->AddControl($btnlogout);
$fproc->AddControl($btnYes);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    $sPosNum = $ddlPosName->SubmittedValue;
//        $sTerName = $ddlTerminalName->SubmittedValue;

    $arrterminalname = $terminals->SelectTerminalName($ddlTerminalName->SubmittedValue);
    $sTerName = $arrterminalname[0]["Name"];

    $_SESSION['ddlPos'] = $sPosNum;
    $_SESSION['ternme'] = $sTerName;

//    if ($btnSearch->SubmittedValue == "Search")
//    {
    $getlogout = $terminals->GetTerminalLogout($sTerName, $sPosNum);


    $tmpterminal = $ddlTerminalName->SubmittedValue;

    $listddl = $terminals->getIDListExceptFreeEntry($sPosNum);
    $ddlTerminalName = new ComboBox("ddlTerminalName", "ddlTerminalName");
    $arrlist = new ArrayList();
    $arrlist->AddArray($listddl);
    $list = null;
    $list[] = new ListItem("", "0", true);
    $ddlTerminalName->Items = $list;
    $ddlTerminalName->DataSource = $arrlist;
    $ddlTerminalName->DataSourceText = "Name";
    $ddlTerminalName->DataSourceValue = "ID";
    $ddlTerminalName->DataBind();
    $ddlTerminalName->SetSelectedValue($tmpterminal);
//    }

    if ($btnYes->SubmittedValue == "YES")
    {
        $tmpterminal = $ddlTerminalName->SubmittedValue;
        $listddl = $terminals->getIDListExceptFreeEntry($_SESSION['ddlPos']);
        $ddlTerminalName = new ComboBox("ddlTerminalName", "ddlTerminalName");
        $arrlist = new ArrayList();
        $arrlist->AddArray($listddl);
        $list = null;
        $list[] = new ListItem("", "0", true);
        $ddlTerminalName->Items = $list;
        $ddlTerminalName->DataSource = $arrlist;
        $ddlTerminalName->DataSourceText = "Name";
        $ddlTerminalName->DataSourceValue = "ID";
        $ddlTerminalName->DataBind();
        $ddlTerminalName->SetSelectedValue($tmpterminal);

        $getlogout = $terminals->GetTerminalLogout($sTerName, $sPosNum);
        $terminal_id = $getlogout[0]['ID'];

        $where = "  WHERE TerminalID = '$terminal_id' and DateEnd = 0 ";
        $hassession = $terminalsession->SelectByWhere($where);

        if ($hassession || count($hassession) == 1)//has session
        {
            $getsess = $adminaccountsession->GetSessionID($_SESSION['sid']);

            if (count($getsess) > 0)
            {
                $getinfo = $adminaccounts->getremoteip($_SESSION['sid']);
                $accountID = $getinfo[0]['AccountID'];
                $remoteip = $getinfo[0]['RemoteIP'];
            }
            $getname = $terminals->getNAME($sTerName, $sPosNum);

            $playable = $getname[0]['IsPlayable'];
//            if ($playable != 1)
//            {
            $terminals->StartTransaction();
            $terminals->UpdateisPlayable($sTerName, 3);
            if ($terminals->HasError)
            {
                $terminals->RollBackTransaction();
            } else
            {
                $terminals->CommitTransaction();
            }
            $audittrail->StartTransaction();
            $arr_audit['SessionID'] = $_SESSION['sid'];
            $arr_audit['AccountID'] = $accountID;
            $arr_audit['TransDetails'] = "Forced Logout Terminal: " . $sTerName . " ";
            $arr_audit['RemoteIP'] = $remoteip;
            $arr_audit['TransDateTime'] = "now_usec()";
            $audittrail->Insert($arr_audit);
            if ($audittrail->HasError)
            {
                
            } else
            {
                $audittrail->CommitTransaction();
                switch ($provider)
                {
                    case 1:

                        App::LoadModuleClass("CasinoAPI", "RealtimeGamingPlayerAPI");
                        App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
                        App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
                        $playerurl = App::getParam("player_url");
                        $rtg_url = App::getParam("rtg_url");
                        $certFilePath = App::getParam("certFilePath");
                        $keyFilePath = App::getParam("keyFilePath");
                        $rtgwrapper = new RealtimeGamingAPIWrapper($rtg_url, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);
                        $playerapi = new RealtimeGamingAPIWrapper($playerurl, RealtimeGamingAPIWrapper::PLAYER_API, $certFilePath, $keyFilePath);

                        $getpid = $rtgwrapper->_GetPIDFromLogin($sTerName);

                        if ($getpid['IsSucceed'] == 1)
                        {
                            $rtglogout = $playerapi->LogoutPlayer($getpid["PID"]);

                            if ($rtglogout["IsSucceed"] == 1)
                            {
                                $returnmsg2 = "Terminal successfully logged out";
                            }
                        } else
                        {
                            $returnmsg2 = 'Error in Getting PID';
                        }

                        break;
                }
            }
//            }
//            else
//            {
//                $returnmsg2 = "Terminal is not yet logged in";
//            }
        } else
        {
            $getsess = $adminaccountsession->GetSessionID($_SESSION['sid']);

            if (count($getsess) > 0)
            {
                $getinfo = $adminaccounts->getremoteip($_SESSION['sid']);
                $accountID = $getinfo[0]['AccountID'];
                $remoteip = $getinfo[0]['RemoteIP'];
            }
            $getname = $terminals->getNAME($sTerName, $sPosNum);

            $playable = $getname[0]['IsPlayable'];
//            if ($playable != 1)
//            {
            $terminals->StartTransaction();
            $terminals->UpdateisPlayable($sTerName, 2);
            if ($terminals->HasError)
            {
                $terminals->RollBackTransaction();
            } else
            {
                $terminals->CommitTransaction();
            }

            $audittrail->StartTransaction();
            $arr_audit['SessionID'] = $_SESSION['sid'];
            $arr_audit['AccountID'] = $accountID;
            $arr_audit['TransDetails'] = "Forced Logout Terminal: " . $sTerName . " ";
            $arr_audit['RemoteIP'] = $remoteip;
            $arr_audit['TransDateTime'] = "now_usec()";
            $audittrail->Insert($arr_audit);
            if ($audittrail->HasError)
            {
                
            } else
            {
                $audittrail->CommitTransaction();

                switch ($provider)
                {
                    case 1:

                        App::LoadModuleClass("CasinoAPI", "RealtimeGamingPlayerAPI");
                        App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
                        App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
                        $playerurl = App::getParam("player_url");
                        $rtg_url = App::getParam("rtg_url");
                        $certFilePath = App::getParam("certFilePath");
                        $keyFilePath = App::getParam("keyFilePath");
                        $rtgwrapper = new RealtimeGamingAPIWrapper($rtg_url, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);
                        $playerapi = new RealtimeGamingAPIWrapper($playerurl, RealtimeGamingAPIWrapper::PLAYER_API, $certFilePath, $keyFilePath);

                        $getpid = $rtgwrapper->_GetPIDFromLogin($sTerName);

                        if ($getpid['IsSucceed'] == 1)
                        {
                            $rtglogout = $playerapi->LogoutPlayer($getpid["PID"]);

                            if ($rtglogout["IsSucceed"] == 1)
                            {
                                $returnmsg2 = "Terminal successfully logged out";
                            }
                        } else
                        {
                            $returnmsg2 = 'Error in Getting PID';
                        }

                        break;
                }
            }
//            }
//            else
//            {
//                $returnmsg2 = "Terminal is not yet logged in";
//            }
        }
    }
} else
{
    unset($_SESSION['ddlPos']);
    unset($_SESSION['ternme']);
}
?>
