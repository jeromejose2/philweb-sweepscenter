<?php
/*
 * Created By       :       Angelo Cubos
 * Date Created     :       Dec 6, 2012
 * Purpose          :       Get Terminals IsFreeEntry = 0
 */

$acctid = $_POST['ddlacctname'];

require_once('../init.inc.php');
App::LoadModuleClass("SweepsCenter", "SCC_Accounts");
$caccts = new SCC_Accounts();

$arrTerminals = $caccts->POSTerminalNotFreeEntry($acctid);
$data = array();

for ($i = 0; $i < count($arrTerminals); $i++)
{
    $data[$arrTerminals[$i]["ID"]] = $arrTerminals[$i]["Name"];
}

echo json_encode($data);
?>