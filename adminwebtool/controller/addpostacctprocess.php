<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 14, 2012
 * Purpose          :       POS Account Creation Controller
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_Sites");
App::LoadModuleClass($modulename, "SCC_AccountDetails");
App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_AccountBankDetails");
App::LoadModuleClass($modulename,"SCCC_AgentAccounts");
App::LoadModuleClass($modulename, "SCC_AuditTrail");

App::LoadCore("PHPMailer.class.php");
App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("CheckBox");

$agentaccounts = new SCCC_AgentAccounts();
$fproc = new FormsProcessor();
$csites = new SCC_Sites();
$cacctdtls = new SCC_AccountDetails();
$caccts = new SCC_Accounts();
$cacctbankdtls = new SCC_AccountBankDetails();
$caudittrail =  new SCC_AuditTrail();

$txtuname = new TextBox("txtuname", "txtuname", "Cashier Username");
$txtuname->Length = 15;
$txtuname->Args="autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtscode = new TextBox("txtscode", "txtscode", "SWC Code");
$txtscode->Length = 3;
$txtscode->Args="autocomplete='off' onkeypress='javascript: return AlphaOnly(event);'";

$txthadd = new TextBox("txthadd", "txthadd", "Home Address");
$txthadd->Multiline = true;
$txthadd->Style = "margin-left: 30px; width: 400px;";
$txthadd->Length = 100;

$txtfname = new TextBox("txtfname", "txtfname", "First Name");
$txtfname->Args="autocomplete='off' onkeypress='javascript: return AlphaOnly(event);'";
$txtfname->Length = 50;

$txtmname = new TextBox("txtmname", "txtmname", "Middle Name");
$txtmname->Args="autocomplete='off' onkeypress='javascript: return AlphaOnly(event);'";
$txtmname->Length = 50;

$txtlname = new TextBox("txtlname", "txtlname", "Last Name");
$txtlname->Args="autocomplete='off' onkeypress='javascript: return AlphaOnly(event);'";
$txtlname->Length = 50;

$txtAuname = new TextBox("txtAuname","txtAuname","Username");
$txtAuname->Length = 12;
$txtAuname->Args="autocomplete='off' onblur='javascript: return chkuname()'; onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtApword = new TextBox("txtApword","txtApword","Password");
$txtApword->Length = 12;
$txtApword->Password = true;
$txtApword->Args="autocomplete='off' onblur='javascript: return chkpwd()'; onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtzip = new TextBox("txtzip", "txtzip", "Zip Code");
$txtzip->Args="autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtzip->Length = 10;

$txttelno = new TextBox("txttelno", "txttelno", "Telephone Number");
$txttelno->Args="autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txttelno->Length = 20;

$txtmobno = new TextBox("txtmobno", "txtmobno", "Mobile Number");
$txtmobno->Args="autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtmobno->Length = 20;

$txtfax = new TextBox("txtfax", "txtfax", "Fax");
$txtfax->Args="autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtfax->Length = 20;

$txtemail = new TextBox("txtemail", "txtemail", "Email Address");
$txtemail->Args="onkeypress='javascript: return verifyEmail(event);'";
$txtemail->Length = 100;

$txtwebadd = new TextBox("txtwebadd", "txtwebadd", "Website Address");
$txtwebadd->Args="autocomplete='off' onkeypress='javascript: return DisableSpacesOnly(event);'";
$txtwebadd->Length = 100;

$txtcitizen = new TextBox("txtcitizen", "txtcitizen", "Citizen");
$txtcitizen->Args="autocomplete='off' onkeypress='javascript: return AlphaOnly(event);'";
$txtcitizen->Length = 50;

$txtbday = new TextBox("txtbday", "txtbday", "Birthday");
$txtbday->Args="autocomplete='off' readonly='readonly' onkeypress='javascript: return DisableSpacesOnly(event);'";

$txtpassport =  new TextBox("txtpassport", "txtpassport", "Passport");
$txtpassport->Args="autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtpassport->Length = 50;
$txtpassport->Enabled = false;

$txtdriver =  new TextBox("txtdriver", "txtdriver", "Driver");
$txtdriver->Args="autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtdriver->Length = 50;
$txtdriver->Enabled = false;

$txtsss =  new TextBox("txtsss", "txtsss", "SSS");
$txtsss->Args="autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtsss->Length = 50;
$txtsss->Enabled = false;

$txtothers =  new TextBox("txtothers", "txtothers", "Others");
$txtothers->Args="autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtothers->Length = 100;
$txtothers->Enabled = false;

$txtbankbranch = new TextBox("txtbankbranch", "txtbankbranch", "Bank Branch");
$txtbankbranch->Args="autocomplete='off' onkeypress='javascript: return AlphaAndSpaces(event);'";
$txtbankbranch->Length = 100;

$txtbankacctname = new TextBox("txtbankacctname", "txtbankacctname", "Bank Account Name");
$txtbankacctname->Args="autocomplete='off' onkeypress='javascript: return AlphaAndSpaces(event);'";
$txtbankacctname->Length = 100;

$txtbankacctnum = new TextBox("txtbankacctnum", "txtbankacctnum", "Bank Account Number");
$txtbankacctnum->Args="autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtbankacctnum->Length = 50;

$txtpayeebankbranch = new TextBox("txtpayeebankbranch", "txtpayeebankbranch", "Payee Bank Branch");
$txtpayeebankbranch->Args="autocomplete='off' onkeypress='javascript: return AlphaAndSpaces(event);'";
$txtpayeebankbranch->Length = 100;

$txtpayeebankacctname = new TextBox("txtpayeebankacctname", "txtpayeebankacctname", "Payee Bank Account Name");
$txtpayeebankacctname->Args="autocomplete='off' onkeypress='javascript: return AlphaAndSpaces(event);'";
$txtpayeebankacctname->Length = 100;

$txtpayeebankacctnum = new TextBox("txtpayeebankacctnum", "txtpayeebankacctnum", "Payee Bank Account Number");
$txtpayeebankacctnum->Args="autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtpayeebankacctnum->Length = 50;

$ddlcountry = new ComboBox("ddlcountry", "ddlcountry", "Country");
$opt = null;
$opt[] = new ListItem("Select One", "", true);
$opt[] = new ListItem("Guam", "Guam");
$ddlcountry->Items = $opt;

$ddlbank = new ComboBox("ddlbank", "ddlbank", "Bank");
$opt1 = null;
$opt1[] = new ListItem("Select One", "", true);
$opt1[] = new ListItem("Banco Filipino", "Banco Filipino");
$ddlbank->Items = $opt1;

$ddlbanktype = new ComboBox("ddlbanktype", "ddlbanktype", "Bank Type");
$opt2 = null;
$opt2[] = new ListItem("Select One", "", true);
$opt2[] = new ListItem("", "");
$ddlbanktype->Items = $opt2;

$ddlpayeebank = new ComboBox("ddlpayeebank", "ddlpayeebank", "Payee Bank");
$opt3 = null;
$opt3[] = new ListItem("Select One", "", true);
$opt3[] = new ListItem("Banco Filipino", "Banco Filipino");
$ddlpayeebank->Items = $opt3;

$ddlpayeebanktype = new ComboBox("ddlpayeebanktype", "ddlpayeebanktype", "Payee Bank Type");
$opt4 = null;
$opt4[] = new ListItem("Select One", "", true);
$opt4[] = new ListItem("", "");
$ddlpayeebanktype->Items = $opt4;

$chkpass = new CheckBox("chkpass", "chkpass", "Passport");
$chkpass->Args="onchange='javascript: return forpassport();'";

$chkdriver = new CheckBox("chkdriver", "chkdriver", "Driver");
$chkdriver->Args="onchange='javascript: return fordriver();'";

$chksss = new CheckBox("chksss", "chksss", "SSS");
$chksss->Args="onchange='javascript: return forsss();'";

$chkothers = new CheckBox("chkothers", "chkothers", "Others");
$chkothers->Args="onchange='javascript: return forothers();'";

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Args = "onclick='javascript: return checkinput_create_pos_acct(this);'";

$hidden = new Hidden("hidden", "hidden");
$hiddenvalidauname = new Hidden("hiddenvalidauname", "hiddenvalidauname");
$hiddenactiveprovider = new Hidden("hiddenactiveprovider", "hiddenactiveprovider");
$hiddenactiveprovider->Text = $_SESSION['activeprovider'];

$fproc->AddControl($txtuname);
$fproc->AddControl($txtfname);
$fproc->AddControl($txtmname);
$fproc->AddControl($txtlname);
$fproc->AddControl($txtAuname);
$fproc->AddControl($txtApword);
$fproc->AddControl($txtscode);
$fproc->AddControl($txthadd);
$fproc->AddControl($txtzip);
$fproc->AddControl($txttelno);
$fproc->AddControl($txtmobno);
$fproc->AddControl($txtfax);
$fproc->AddControl($txtemail);
$fproc->AddControl($txtwebadd);
$fproc->AddControl($ddlcountry);
$fproc->AddControl($txtcitizen);
$fproc->AddControl($txtbday);
$fproc->AddControl($chkpass);
$fproc->AddControl($txtpassport);
$fproc->AddControl($chkdriver);
$fproc->AddControl($txtdriver);
$fproc->AddControl($chksss);
$fproc->AddControl($txtsss);
$fproc->AddControl($chkothers);
$fproc->AddControl($txtothers);
$fproc->AddControl($ddlbank);
$fproc->AddControl($ddlbanktype);
$fproc->AddControl($txtbankbranch);
$fproc->AddControl($txtbankacctname);
$fproc->AddControl($txtbankacctnum);
$fproc->AddControl($ddlpayeebank);
$fproc->AddControl($ddlpayeebanktype);
$fproc->AddControl($txtpayeebankbranch);
$fproc->AddControl($txtpayeebankacctname);
$fproc->AddControl($txtpayeebankacctnum);
$fproc->AddControl($hidden);
$fproc->AddControl($hiddenvalidauname);
$fproc->ProcessForms();



if ($fproc->IsPostBack)
{
    if(strlen($txtpassport->SubmittedValue)>0)
    {
    $txtpassport->Enabled = true;
    $chkpass->Checked = true;
    }
    if(strlen($txtdriver->SubmittedValue)>0)
    {
    $txtdriver->Enabled = true;
    $chkdriver->Checked = true;
    }
    if(strlen($txtsss->SubmittedValue)>0)
    {
    $txtsss->Enabled = true;
    $chksss->Checked = true;
    }
    if(strlen($txtothers->SubmittedValue)>0)
    {
    $txtothers->Enabled = true;
    $chkothers->Checked = true;
    }
    
    if ($hidden->Text != '')
    {
        $csites->StartTransaction();
        $caccts->StartTransaction();
        $cacctdtls->StartTransaction();
        $cacctbankdtls->StartTransaction();
        
        $chksiteavailable = $csites->CheckSiteName(strtoupper($txtscode->SubmittedValue));
        if ($chksiteavailable[0][0] == 0)
        {
            $swccodeexist = $cacctdtls->CheckExistSWCCode(strtoupper($txtscode->SubmittedValue));
            if (count($swccodeexist) == 0)
            {
                $emailexist = $caccts->CheckEmailExist($txtemail->SubmittedValue);
                if (count($emailexist) == 0) 
                {
                    $userexist = $caccts->CheckUsernameExist($txtuname->SubmittedValue);
                    if (count($userexist) == 0)
                    {
                        // insert to sites
                        $registersite["Name"] = strtoupper($txtscode->SubmittedValue);
                        $csites->Insert($registersite);
                        if ($csites->HasError)
                        {
                            $errormsgtitle = "POS Account Creation";
                            $errormsg = $csites->getErrors();
                            $csites->RollBackTransaction();
                        }
                        else 
                        {
                            $csites->CommitTransaction();
                            $last_siteid = $csites->LastInsertID;
                            $pass = substr(uniqid(),5);
                            
                            // insert to accounts
                            $arracct["AccountType"] = $_SESSION['accttype'];
                            $arracct["SiteID"] = $last_siteid;
                            $arracct["Username"] = $txtuname->SubmittedValue;
                            $arracct["Password"] = md5($pass);
                            $arracct["Email"] = $txtemail->SubmittedValue;
                            $arracct["Status"] = 1;
                            $arracct["DateCreated"] = 'now_usec()';
                            $caccts->Insert($arracct);
                            if ($caccts->HasError)
                            {
                                $errormsgtitle = "POS Account Creation";
                                $errormsg = $caccts->getErrors();
                                $caccts->RollBackTransaction();
                            }
                            else
                            {
                                $caccts->CommitTransaction();
                                // insert to accountdetails
                                $last_acctid = $caccts->LastInsertID;
                                $updacctdtls["AccountID"] = $last_acctid;
                                $updacctdtls["DateCreated"] = 'now_usec()';   
                                $updacctdtls["FirstName"] = $txtfname->SubmittedValue;
                                $updacctdtls["MiddleName"] = $txtmname->SubmittedValue;
                                $updacctdtls["LastName"] = $txtlname->SubmittedValue;
                                $updacctdtls["SWCCode"] = strtoupper($txtscode->SubmittedValue);
                                $updacctdtls["HomeAddress"] = $txthadd->SubmittedValue;
                                $updacctdtls["ZipCode"] = $txtzip->SubmittedValue;
                                $updacctdtls["TelephoneNumber"] = $txttelno->SubmittedValue;
                                $updacctdtls["MobileNumber"] = $txtmobno->SubmittedValue;
                                $updacctdtls["Fax"] = $txtfax->SubmittedValue;
                                $updacctdtls["Email"] = $txtemail->SubmittedValue;
                                $updacctdtls["Website"] = $txtwebadd->SubmittedValue;
                                $updacctdtls["CountryID"] = $ddlcountry->SubmittedValue;
                                $updacctdtls["Citizenship"] = $txtcitizen->SubmittedValue;
                                $updacctdtls["BirthDate"] = $txtbday->SubmittedValue;
                                $updacctdtls["Passport"] = $txtpassport->SubmittedValue;
                                $updacctdtls["DriversLicense"] = $txtdriver->SubmittedValue;
                                $updacctdtls["Others"] = $txtothers->SubmittedValue;
                                $updacctdtls["DateUpdated"] = 'now_usec()';
                                $cacctdtls->Insert($updacctdtls);
                                if ($cacctdtls->HasError)
                                {
                                    $errormsgtitle = "POS Account Creation";
                                    $errormsg = $cacctdtls->getErrors();
                                    $cacctdtls->RollBackTransaction();
                                }
                                else
                                {
                                    $cacctdtls->CommitTransaction();
                                    // insert to accountbankdetails
                                    $updacctbankdtls["AccountID"] = $last_acctid;
                                    $updacctbankdtls["DateCreated"] = 'now_usec()';
                                    $updacctbankdtls["Bank"] = $ddlbank->SubmittedValue;
                                    $updacctbankdtls["BankType"] = $ddlbanktype->SubmittedValue;
                                    $updacctbankdtls["BankBranch"] = $txtbankbranch->SubmittedValue;
                                    $updacctbankdtls["BankAccountName"] = $txtbankacctname->SubmittedValue;
                                    $updacctbankdtls["BankAccountNumber"] = $txtbankacctnum->SubmittedValue;
                                    $updacctbankdtls["PayeeBank"] = $ddlpayeebank->SubmittedValue;
                                    $updacctbankdtls["PayeeBankType"] = $ddlpayeebanktype->SubmittedValue;
                                    $updacctbankdtls["PayeeBankBranch"] = $txtpayeebankbranch->SubmittedValue;
                                    $updacctbankdtls["PayeeBankAccountName"] = $txtpayeebankacctname->SubmittedValue;
                                    $updacctbankdtls["PayeeBankAccountNumber"] = $txtpayeebankacctnum->SubmittedValue;
                                    $updacctbankdtls["DateUpdated"] = 'now_usec()';
                                    $cacctbankdtls->Insert($updacctbankdtls);
                                    if ($cacctbankdtls->HasError)
                                    {
                                        $errormsgtitle = "POS Account Creation";
                                        $errormsg = $cacctbankdtls->getErrors();
                                        $cacctbankdtls->RollBackTransaction();
                                    }
                                    else
                                    {
                                        $cacctbankdtls->CommitTransaction();
                                        if($_SESSION['activeprovider'] == 3)
                                        {
                                            //insert to tbl_agentaccounts
                                            $Auname = $txtAuname->SubmittedValue;
                                            $tmpAID = $agentaccounts->CheckUsername($Auname);

                                            $agentaccounts->StartTransaction();
                                            $arragentacc['ID'] = $tmpAID[0]['ID'];
                                            $arragentacc['SiteID'] = $last_siteid;
                                            $arragentacc['Status'] = 1;
                                            $agentaccounts->UpdateByArray($arragentacc);
                                            if($agentaccounts->HasError)
                                            {
                                            $agentaccounts->RollBackTransaction();
                                            $errormsgtitle = "ERROR";    
                                            $errormsg = $agentaccounts->getError();            
                                            }else{
                                            $agentaccounts->CommitTransaction();    
                                            }
                                        }
                                        // Log to Audit Trail
                                        $caudittrail->StartTransaction();
                                        $scauditlogparam["SessionID"] = $_SESSION['sid'];
                                        $scauditlogparam["AccountID"] = $_SESSION['aid'];
                                        $scauditlogparam["TransDetails"] = "Create POS Account: " . $txtuname->SubmittedValue;
                                        $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                                        $scauditlogparam["TransDateTime"] = "now_usec()";
                                        $caudittrail->Insert($scauditlogparam);
                                        if ($caudittrail->HasError)
                                        {
                                            $errormsg = $caudittrail->getError();
                                            $errormsgtitle = "ERROR!";
                                        }else{
                                            $caudittrail->CommitTransaction();
                                        }                                        
                                        // send email
                                        $pm = new PHPMailer();
                                        $pm->AddAddress($txtemail->SubmittedValue);

                                        $pageURL = 'http';
                                        if (!empty($_SERVER['HTTPS'])) {$pageURL .= "s";}
                                        $pageURL .= "://";
                                        $folder = $_SERVER["REQUEST_URI"];
                                        $folder = substr($folder,0,strrpos($folder,'/') + 1);
                                        if ($_SERVER["SERVER_PORT"] != "80") 
                                        {
                                            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
                                        } 
                                        else
                                        {
                                            $pageURL .= $_SERVER["SERVER_NAME"].$folder;
                                        }

                                        $pm->IsHTML(true);

                                        $pm->Body = "Dear ".$txtfname->SubmittedValue."<br/><br/>
                                            This is to inform you that your POS Account has been successfully created on this date ".date("m/d/Y")." and time ".date("H:i:s").". Here are your new Account Details:<br/><br/>
                                            Username: <b>".$txtuname->SubmittedValue."</b><br/>
                                            Password: <b>".$pass."</b><br/><br/>".
                                        "<b>For security purposes, please change this assigned password upon log in at http://www.thesweepscenter.com/Cashier.<br/><br/>".
                                        "For inquiries on your account, please contact our Customer Service email at support.gu@philwebasiapacific.com ."."<br/><br/>
                                        Regards,<br/><br/>Customer Support<br/>The Sweeps Center";

                                        $pm->From = "operations@thesweepscenter.com";
                                        $pm->FromName = "The Sweeps Center";
                                        $pm->Host = "localhost";
                                        $pm->Subject = "NOTIFICATION FOR NEWLY CREATED POS ACCOUNT";
                                        $email_sent = $pm->Send();
                                        if($email_sent)
                                        {
                                            $successmsg = "New POS account has been successfully created.";
                                            $successmsgtitle = "POS Account Creation";
                                        }
                                        else
                                        {
                                            $errormsg = "An error occurred while sending the email to your email address";
                                            $errormsgtitle = "POS Account Creation";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        $errormsg = "Username already exists";
                        $errormsgtitle = "POS Account Creation";
                    }
                }
                else
                {
                    $errormsg = 'Email Address has already been used';
                    $errormsgtitle = "POS Account Creation";
                }
            }
            else
            {
                $errormsg = 'SWC entered has already been used';
                $errormsgtitle = "POS Account Creation";
            }
        } 
        else 
        {
            $errormsg = "Site name is already in use";
            $errormsgtitle = "POS Account Creation";
        }
    }
}
?>