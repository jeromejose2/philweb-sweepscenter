<?php

/*
 * Added By : Jerome F. Jose
 * Added On : June 25, 2012
 * Purpose : 
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename,"SCC_Accounts");
App::LoadModuleClass($modulename, "SCCC_PointsConversionInfo");


APP::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
aPP::loadcontrol("PagingControl2");

$itemsperpage = 20;
$pgcon = new PagingControl2($itemperpage,1);
$pgcon->URL = "javascript:ChangePage(%currentpage); ";
$pgcon->PageGroup = 5;
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;

$fproc = new FormsProcessor();
$accounts = new SCC_Accounts();
$pointconversion = new SCCC_PointsConversionInfo();

$txtDateFr = new TextBox("txtDateFr","txtDateFr");
$txtDateFr->ReadOnly = true;
$txtDateFr->Length = "10";
$txtDateFr->Style = "text-align:center";
$txtDateFr->Text = date("m/d/Y");
 
$txtDateTo = new TextBox("txtDateTo","txtDateTo");
$txtDateTo->ReadOnly = true;
$txtDateTo->Length = "10";
$txtDateTo->Style = "text-align:center";
$txtDateTo->Text = date("m/d/Y");

$getlist = $accounts->SelectSiteIDbylist();
$ddlPos = new ComboBox("ddlPos","ddlPos");
$Poslist = new ArrayList();
$Poslist->AddArray($getlist);
$ddlPos->ClearItems();
$litem = null;
$litem[] = new ListItem("---","",true);
$litem[] = new ListItem("ALL","0");
$ddlPos->Items = $litem;
$ddlPos->DataSource = $Poslist;
$ddlPos->DataSourceText = "Name";
$ddlPos->DataSourceValue = "SiteID";
$ddlPos->DataBind();

$btnSearch = new Button("btnSearch","btnSearch","Search");
$btnSearch->CssClass = "labelbutton2";
$btnSearch->IsSubmit = true;
$btnSearch->Args = "onclick='javascript: return checkinput();' ";


$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($ddlPos);
$fproc->AddControl($btnSearch);

$fproc->ProcessForms();

if($fproc->IsPostBack)
{
    $daysinterval = (strtotime($txtDateTo->SubmittedValue) - strtotime($txtDateFr->SubmittedValue)) / 86400;
    
    if ($daysinterval <= 90)
    {    
                $dDateFr = $txtDateFr->SubmittedValue;
                $dDateTo = $txtDateTo->SubmittedValue;
                $sPosAcct = $ddlPos->SubmittedValue;
                $_SESSION['start'] = $dDateFr;
                $_SESSION['end'] = $dDateTo;	
                $_SESSION['posacct'] = $sPosAcct;
                $ds = new DateSelector($dDateTo);
                $date2 = $ds->NextDate;
                $dDateTo = $date2;
                $dDateTo = date_create($dDateTo);
                $dDateTo = date_format($dDateTo, "Y-m-d 10:00:00.00000");
                $dDateFr = date_create($dDateFr);
                $dDateFr = date_format($dDateFr, "Y-m-d 09:59:59.99999");
    
            $getdetail1 = $pointconversion->getPointConversion($sPosAcct, $dDateFr, $dDateTo);
            $pgcon->Initialize($itemsperpage, count($getdetail1));
            $pgHist = $pgcon->PreRender();
            $wherelimit = "LIMIT ".($pgcon->SelectedItemFrom-1).",".$itemsperpage." ";
            $getdetail = $pointconversion->getPointConversionwithLimit($sPosAcct, $dDateFr, $dDateTo, $wherelimit);
           
               
       
	
    if($btnSearch->SubmittedValue == 'Search')
        {
            $pgcon->SelectedPage = 1;

        }
    }else{
        $errormsgtitle = "INVALID INPUT";
        $errormsg = "Please limit date range selection to ninety (90) days or three (3) months only.";
    }        
}
?>
