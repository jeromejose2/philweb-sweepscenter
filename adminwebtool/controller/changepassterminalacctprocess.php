<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 27, 2012
 * Purpose          :       Change Terminal Account Password
 */

require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_AccountDetails");
App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_Sites");
App::LoadModuleClass($modulename, "SCC_TopUpLogs");
App::LoadModuleClass($modulename, "SCC_AccountUpdateLog");

App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$fproc = new FormsProcessor();
$cacctdtls = new SCC_AccountDetails();
$caccts = new SCC_Accounts();

$ddlacctname = new ComboBox("ddlacctname", "ddlacctname", "Account Name");
$ddlacctname->Args = "onchange='javascript: SelectPOSAccountNumber();getTerminals1();'";
$opt1 = null;
$opt1[] = new ListItem("Select One", "Select One", true);
$ddlacctname->Items = $opt1;
$posaccts = $cacctdtls->LoadPOSAccounts("Select One");
$list_posaccts = new ArrayList();
$list_posaccts->AddArray($posaccts);
$ddlacctname->DataSource = $list_posaccts;
$ddlacctname->DataSourceValue = "AccountID";
$ddlacctname->DataSourceText = "FullName";
$ddlacctname->DataBind();

$ddlacctno = new ComboBox("ddlacctno", "ddlacctno", "Account Number");
$ddlacctno->Args = "onchange='javascript: SelectPOSAccountName();getTerminals();'";
$opt2 = null;
$opt2[] = new ListItem("Select One", "Select One", true);
$ddlacctno->Items = $opt2;
$posacctsid = $cacctdtls->LoadPOSAccountID();
$list_posacctsid = new ArrayList();
$list_posacctsid->AddArray($posacctsid);
$ddlacctno->DataSource = $list_posacctsid;
$ddlacctno->DataSourceValue = "AccountID";
$ddlacctno->DataSourceText = "AccountID";
$ddlacctno->DataBind();

$ddltacct = new ComboBox("tacct", "tacct", "Terminal Account");
$ddltacct->Enabled = false;
$opt3 = null;
$opt3[] = new ListItem("Select One", "Select One", true);
$ddltacct->Items = $opt3;

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Search");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->IsSubmit = true;

$btnResetPass = new Button("btnresetpass", "btnresetpass", "Reset Password");
$btnResetPass->Style = "width: 170px;";
$btnResetPass->CssClass = "labelbutton2";

$hidfname = new Hidden("fname", "fname", "POS Account Name");
$hidtname = new Hidden("tname", "tname", "POS Terminal Name");

$fproc->AddControl($ddlacctname);
$fproc->AddControl($ddlacctno);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddltacct);
$fproc->AddControl($btnResetPass);
$fproc->AddControl($hidfname);
$fproc->AddControl($hidtname);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSubmit->SubmittedValue == "Search")
    {
        $arrTerminals = $caccts->LoadPOSTerminals($ddlacctname->SubmittedValue);
        for ($i = 0; $i < count($arrTerminals); $i++)
        {
            $option[] = new ListItem($arrTerminals[$i]["Name"],$arrTerminals[$i]["ID"]);
        }
        $ddltacct->Items = $option;
        $ddltacct->SetSelectedValue($ddltacct->SubmittedValue);
        
        $acctname = $hidfname->SubmittedValue;
        $acctid = $ddlacctno->SubmittedValue;
        $terminal_name = $hidtname->SubmittedValue;
    }
}
?>
