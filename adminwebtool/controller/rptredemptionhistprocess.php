<?php

/*
 * Added By : Jerome F. Jose
 * Added On : June 22, 2012
 * Purpose : 
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename,"SCC_Accounts");
App::LoadModuleClass($modulename, "SCCC_Winners");
App::LoadModuleClass($modulename, "SCCC_TransactionSummary");

APP::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
APP::LoadControl("PagingControl2");


$fproc = new FormsProcessor();
$accounts = new SCC_Accounts();
$winners = new SCCC_Winners();
$transactionsummary = new SCCC_TransactionSummary();


$getlist = $accounts->SelectSiteIDbylist();
$ddlPos = new ComboBox("ddlPos","ddlPos");
$Poslist = new ArrayList();
$Poslist->AddArray($getlist);
$ddlPos->ClearItems();
$litem = null;
$litem[] = new ListItem("---","",true);
$litem[] = new ListItem("ALL","0");
$ddlPos->Items = $litem;
$ddlPos->DataSource = $Poslist;
$ddlPos->DataSourceText = "Name";
$ddlPos->DataSourceValue = "SiteID";
$ddlPos->DataBind();

$txtDateFr = new TextBox("txtDateFr","txtDateFr");
$txtDateFr->Length = "10";
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align:center;";
$txtDateFr->Text = date("m/d/Y");

$txtDateTo = new TextBox("txtDateTo","txtDateTo");
$txtDateTo->Length = "10";
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align:center;";
$txtDateTo->Text = date("m/d/Y");

$btnSearch = new Button("btnSearch","btnSearch","Search");
$btnSearch->IsSubmit= true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args ="onclick='javascript: return checkinput();' ";

$itemsperpage = 20;
$pgcon = new PagingControl2($itemperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->PageGroup = 5;
$pgcon->CssClass = "paging";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;


$fproc->AddControl($ddlPos);
$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSearch);



$fproc->ProcessForms();

 
 if($fproc->IsPostBack)
 {
     $daysinterval = (strtotime($txtDateTo->SubmittedValue) - strtotime($txtDateFr->SubmittedValue)) / 86400;
    if ($daysinterval <= 90)
    {      
                $projID = 2;
                $gametype = 0;  
                $dDateFr = $txtDateFr->SubmittedValue;
                $dDateTo = $txtDateTo->SubmittedValue;
                $sPosAcct = $ddlPos->SubmittedValue;
                $_SESSION['start'] = $dDateFr;
                $_SESSION['end'] = $dDateTo;	
                $_SESSION['posacct'] = $sPosAcct;
                $ds = new DateSelector($dDateTo);
                $date2 = $ds->NextDate;
                $dDateTo = $date2;
                $dDateTo = date_create($dDateTo);
                $dDateTo = date_format($dDateTo, "Y-m-d 10:00:00.00000");
                $dDateFr = date_create($dDateFr);
                $dDateFr = date_format($dDateFr, "Y-m-d 09:59:59.99999");
                

            if($gametype == 2)
                    {
                        $isFreeEntry = 1;
                    }  else {
                        $isFreeEntry = 0;
                            }
           if($projID == 1)
           {
               $getwinners = $winners->getdetailsbyProjID($projID, $dDateFr, $dDateTo);
            
           }
           if($projID == 2)
           {
               
                $getTotal = $winners->getTotalCashwithSiteId($projID, $sPosAcct, $dDateFr, $dDateTo);
                $totalCash = $getTotal[0]['dec_TotalCash'];
                ($totalCash == null) ? 0 : $totalCash;
                $getTotalNonCash = $winners->getTotalNonCashwithSiteID($projID, $sPosAcct, $dDateFr, $dDateTo);
                $totalNonCash = $getTotalNonCash[0]['dec_TotalNonCash'];
                ($totalNonCash == null) ? 0 : $totalNonCash;
                $getTransSummary1 = $transactionsummary->getDetailsTransSummary($dDateFr,$dDateTo,$gametype,$freeentry);
                $pgcon->Initialize($itemsperpage, count($getTransSummary1));
                $pgHist = $pgcon->PreRender();
                $wherelimit = "LIMIT ".($pgcon->SelectedItemFrom-1).",".$itemsperpage." ";
                $getTransSummary = $transactionsummary->getDetailsTransSummarywithLIMIT($sPosAcct, $dDateFr, $dDateTo, $gametype, $freeentry,$wherelimit);
                  
            }
           
           
       if($btnSearch->SubmittedValue == "Search")
       {    
         $pgcon->SelectedPage = 1;
       }
    }else{
        $errormsgtitle = "INVALID INPUT";
        $errormsg = "Please limit date range selection to ninety (90) days or three (3) months only.";
    }           
 }
                


?>
