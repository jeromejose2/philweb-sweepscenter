<?php
/*
 * Added By : Jerome F. Jose
 * Added On : June 21, 2012
 * Purpose : Change Password Admin
 */

require_once("../init.inc.php");

App::LoadModuleClass("SweepsCenter","SCC_AdminAccounts");
App::LoadModuleClass("SweepsCenter","SCC_AuditTrail");
App::LoadModuleClass("SweepsCenter","SCC_AdminAccountSessions");

App::LoadCore("PHPMailer.class.php");

APP::LoadControl("TextBox");
App::LoadControl("Button");

$fproc                 = new FormsProcessor();
$cadminaccts           = new SCC_AdminAccounts();
$adminaccounts         = new SCC_AdminAccounts();
$audittrail            = new SCC_AuditTrail();
$adminaccountsession   = new SCC_AdminAccountSessions();
$txtPassword           = new TextBox("txtPassword","txtPassword");
$txtPassword->Style    = "width:200px;";
$txtPassword->Password = true;
$txtPassword->Length   = 12;
$txtPassword->Args     = "autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtNewPassword           = new TextBox("txtNewPassword","txtNewPassword");
$txtNewPassword->Style    = "width:200px;";
$txtNewPassword->Password = true;
$txtNewPassword->Length   = 12;
$txtNewPassword->Args     = "autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtConfPassword           = new TextBox("txtConfPassword","txtConfPassword");
$txtConfPassword->Style    = "width:200px;";
$txtConfPassword->Password = true;
$txtConfPassword->Length   = 12;
$txtConfPassword->Args     = "autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$btnOk           = new Button("btnOK","btnOK","SUBMIT");
$btnOk->CssClass = "labelbutton2";
$btnOk->IsSubmit = true;
$btnOk->Args     = "onclick='javascript: return checkchangepassword();' ";

$passSubmit           = new Button("passSubmit","passSubmit","OK");
$passSubmit->CssClass = "labelbutton2";
$passSubmit->Args     = "onclick=document.getElementById('light11').style.display='none';document.getElementById('fade').style.display='none';";
$passSubmit->IsSubmit = true;

$fproc->AddControl($txtPassword);
$fproc->AddControl($txtNewPassword);
$fproc->AddControl($txtConfPassword);
$fproc->AddControl($btnOk);
$fproc->AddControl($passSubmit);


$fproc->ProcessForms();
$isprocessed = -1;

if ( $fproc->IsPostBack )
{
    $sessionID = $_SESSION['sid'];


    if ( $passSubmit->SubmittedValue == "OK" )
    {


        $_SESSION['acctpword'] = $txtNewPassword->SubmittedValue;
        $_SESSION['newpw']     = $txtNewPassword->SubmittedValue;
        $_SESSION['oldpw']     = $txtPassword->SubmittedValue;
        $oldpass               = $txtPassword->SubmittedValue;
        $newpass               = $txtNewPassword->SubmittedValue;
        $newpassforemail       = $newpass;
        //change password

        $getsessionId = $adminaccountsession->GetSessionID($sessionID);
        if ( count($getsessionId) == 0 )
        {
            $returnmsg = "User Session does not Exist;";
        }
        else
        {

            $getdetail = $adminaccounts->getCashierDetails($sessionID);

            $AcctID   = $getdetail[0]['AccountID'];
            $Email    = $getdetail[0]['Email'];
            $password = $getdetail[0]['Password']; // current pass
            $username = $getdetail[0]['Username'];
            $remoteip = $getdetail[0]['RemoteIP'];


//                    -- update accounts table
            $adminaccounts->StartTransaction();
            $updatePass = $adminaccounts->UpdateChangePassword($newpass,$username,$AcctID);

            if ( $adminaccounts->HasError )
            {
                $adminaccounts->RollBackTransaction();
            }
            else
            {
                $adminaccounts->CommitTransaction();

//                        -- insert in audit trail
                $audittrail->StartTransaction();
                $arrAudittrail['SessionID']     = $sessionID;
                $arrAudittrail['AccountID']     = $AcctID;
                $arrAudittrail['TransDetails']  = "Change password for Admin AcctID :" . $AcctID . " ";
                $arrAudittrail['RemoteIP']      = $remoteip;
                $arrAudittrail['TransDateTime'] = "now_usec()";
                $audittrail->Insert($arrAudittrail);
                if ( $audittrail->HasError )
                {
                    $returnmsg = " Error inserting in audit trail";
                    $audittrail->RollBackTransaction();
                }
                else
                {
                    $audittrail->CommitTransaction();
                    $returnmsg = "You have successfully changed your password. An email notification was sent to the user account. ";
                }
            }

            $emailaddr   = $Email;
            $cadminaccts->StartTransaction();
            $where       = " WHERE Email = '$emailaddr' AND Status = '1'";
            $userdetails = $cadminaccts->SelectByWhere($where);
            $id          = $userdetails[0]["ID"];
            $name        = $userdetails[0]["Username"];
            $acctname    = $userdetails[0]["FirstName"] . " " . $userdetails[0]["MiddleName"] . " " . $userdetails[0]["LastName"];
            $newpass     = substr(session_id(),0,8);


            // Sending of Email
            $pm = new PHPMailer();
            $pm->AddAddress($emailaddr,$name);

            $pageURL = 'http';
            if ( isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) == "on" )
            {
                $pageURL .= "s";
            }
            $pageURL .= "://";
            $folder = $_SERVER["REQUEST_URI"];
            $folder = substr($folder,0,strrpos($folder,'/') + 1);
            if ( $_SERVER["SERVER_PORT"] != "80" )
            {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $folder;
            }
            else
            {
                $pageURL .= $_SERVER["SERVER_NAME"] . $folder;
            }

            $pm->IsHTML(true);

            $pm->Body = "Dear " . $name . "<br/><br/>
            This is to inform you that your password has been changed on this date " . date("m/d/Y") . " and time " . date("H:i:s") . ". Here are your new Account Details:<br/><br/>
            Username: <b>" . $name . "</b><br/>
            Password: <b>" . $newpassforemail . "</b><br/><br/>" .
//        "<b>For security purposes, please change this system-generated password upon log in at </b><a href=" . $pageURL .">http://www.thesweepscenter.com/VoucherWebtool</a><br/><br/>".
                      "If you didn't perform this procedure, please report this to our Customer Service email at support.gu@philwebasiapacific.com." . "<br/><br/>
         Regards,<br/><br/>The Sweeps Center";

            $pm->FromName = "support.gu@philwebasiapacific.com";
            $pm->Host     = "localhost";
            $pm->Subject  = "NOTIFICATION FOR NEW PASSWORD";
            $email_sent   = $pm->Send();

            if ( $email_sent )
            {
                $successmsg = "success";
            }
            else
            {
                $errormsg = "error";
            }
        }
    }
}
?>