<?php

/**
 * Gross Hold Summary
 * 
 * Controller of the report
 * 
 * @author: Jerome F. Jose
 * @datecreated : June 25, 2012
 * 
 * lastupdated by: Noel Antonio
 * dateupdated: February 11, 2013
 * purpose: includes free entry transactions, fix downloads, total cash on hand
 * and total gross holds computations.
 * 
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_TerminalSessionDetails");
App::LoadModuleClass($modulename, "SCC_TerminalSessions");

APP::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("PagingControl2");

$fproc = new FormsProcessor();
$accounts = new SCC_Accounts();
$terminalsession = new SCC_TerminalSessions();

$itemsperpage = 20;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->PageGroup = 5;
$pgcon->ShowMoveToLastPage = true;
$pgcon->ShowMoveToFirstPage = true;

$txtDateFr = new TextBox("txtDateFr", "txtDateFr");
$txtDateFr->Length = "10";
$txtDateFr->Style = "text-align:center";
$txtDateFr->ReadOnly = true;
$txtDateFr->Text = date("m/d/Y");

$txtDateTo = new TextBox("txtDateTo", "txtDateTo");
$txtDateTo->Length = "10";
$txtDateTo->Style = "text-align:center";
$txtDateTo->ReadOnly = true;
$txtDateTo->Text = date("m/d/Y");

$btnSearch = new Button("btnSearch", "btnSearch", "Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checkinput20();' ";

$getlist = $accounts->SelectSiteIDbylist();
$ddlPos = new ComboBox("ddlPos", "ddlPos");
$Poslist = new ArrayList();
$Poslist->AddArray($getlist);
$ddlPos->ClearItems();
$litem = null;
$litem[] = new ListItem("ALL", "0", true);
$ddlPos->Items = $litem;
$ddlPos->DataSource = $Poslist;
$ddlPos->DataSourceText = "Name";
$ddlPos->DataSourceValue = "SiteID";
$ddlPos->DataBind();

$download = new Button("download", "download", "DOWNLOAD");
$download->IsSubmit = true;
$download->CssClass = "labelbutton2";

$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSearch);
$fproc->AddControl($ddlPos);
$fproc->AddControl($download);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
$daysinterval = (strtotime($txtDateTo->SubmittedValue) - strtotime($txtDateFr->SubmittedValue)) / 86400;
    if ($daysinterval <= 90)
    {
        $dDateFr = $txtDateFr->SubmittedValue;
        $dDateTo = $txtDateTo->SubmittedValue;
        $sPosAcct = $ddlPos->SubmittedValue;
        $_SESSION['start'] = $dDateFr;
        $_SESSION['end'] = $dDateTo;
        $_SESSION['posacct'] = $sPosAcct;

        $ds = new DateSelector($dDateTo);
        $date2 = $ds->NextDate;
        $dDateTo = $date2;
        $dDateTo = date_create($dDateTo);
        $dDateTo = date_format($dDateTo, "Y-m-d 10:00:00.00000");
        $dDateFr = date_create($dDateFr);
        $dDateFr = date_format($dDateFr, "Y-m-d 09:59:59.99999");

        $getgrossholdlist1 = $terminalsession->getgrossholdhist($sPosAcct, $dDateFr, $dDateTo);
        $pgcon->Initialize($itemsperpage, count($getgrossholdlist1));
        $pgHist = $pgcon->PreRender();
        $wherelimit = " LIMIT " . ($pgcon->SelectedItemFrom - 1) . "," . $itemsperpage;
        $getgrossholdlist = $terminalsession->getgrossholdhistwithLIMIT($sPosAcct, $dDateFr, $dDateTo, $wherelimit);

        $arr_records = array();
        if (count($getgrossholdlist) > 0)
        {
            $arr_records = GetGrossHoldReport($dDateFr, $dDateTo, $getgrossholdlist);
            $arr_records1 = GetGrossHoldReport($dDateFr, $dDateTo, $getgrossholdlist1);
        }

        if ($btnSearch->SubmittedValue == "Search")
        {
            $pgcon->SelectedPage = 1;
        }


        if ($download->SubmittedValue == "DOWNLOAD")
        {
            $arr_records1 = array();
            if (count($getgrossholdlist1) > 0)
            {
                $arr_records1 = GetGrossHoldReport($dDateFr, $dDateTo, $getgrossholdlist1);
            }

            $_SESSION['report_header'] = array("\"Date and Time In\"", "\"Date and Time Out\"", "\"POS Account Name\"", "\"Terminal Name\"", "\"Initial Deposit\"", "\"Total Reloads\"", "\"Cash Redemptions\"", "\"Non-Cash Redemptions Value\"", "\"Gross Hold\"");

            for ($ctr = 0; $ctr < count($arr_records1); $ctr++)
            {
                $_SESSION['report_values'][$ctr][0] = $arr_records1[$ctr][0];
                $_SESSION['report_values'][$ctr][1] = $arr_records1[$ctr][1];
                $_SESSION['report_values'][$ctr][2] = $arr_records1[$ctr][2];
                $_SESSION['report_values'][$ctr][3] = $arr_records1[$ctr][5];
                $_SESSION['report_values'][$ctr][4] = $arr_records1[$ctr][3];
                $_SESSION['report_values'][$ctr][5] = $arr_records1[$ctr][4];
                $_SESSION['report_values'][$ctr][6] = $arr_records1[$ctr][6];
                $_SESSION['report_values'][$ctr][7] = $arr_records1[$ctr][8];
                $_SESSION['report_values'][$ctr][8] = $arr_records1[$ctr][9];

                $cash_grandtotal = ($cash_grandtotal + $arr_records1[$ctr][6]);
                $deposit_grandtotal = ($deposit_grandtotal + $arr_records1[$ctr][3]);
                $reload_grandtotal = ($reload_grandtotal + $arr_records1[$ctr][4]);
                $grosshold_grandtotal = ($grosshold_grandtotal + $arr_records1[$ctr][9]);
            }

            $cash_on_hand = (($deposit_grandtotal + $reload_grandtotal) - $cash_grandtotal);

            $_SESSION['report_values'][(count($arr_records1))][0] = "Total Sweeps Center Cash On Hand: ";
            $_SESSION['report_values'][(count($arr_records1))][1] = $cash_on_hand;
            $_SESSION['report_values'][(count($arr_records1) + 1)][0] = "Total Sweeps Center Gross Hold: ";
            $_SESSION['report_values'][(count($arr_records1) + 1)][1] = $grosshold_grandtotal;

            URL::Redirect('export_report.php?fn=Gross_Hold_Summary');
        }
    }
    else
    {
        $errormsgtitle = "INVALID INPUT";
        $errormsg = "Please limit date range selection to ninety (90) days or three (3) months only.";
    }
}

/**
 * function GetGrossHoldReport()
 * 
 * A function that will group, organize all
 * transactions per session and compute for total cash on hand
 * and total gross hold
 * 
 * @author: Noel Antonio
 * @datecreated: February 11, 2013
 * @purpose: create a function that will be used to minimize
 * redundancy in using the same processes.
 * 
 * @param date $dDateFr, date $dDateTo, array $getgrossholdlist
 * @return array
 * 
 */
function GetGrossHoldReport($dDateFr, $dDateTo, $getgrossholdlist)
{
    $terminalsessiondetails = new SCC_TerminalSessionDetails();
    $arr_records = array();

    for ($x = 0; $x < count($getgrossholdlist); $x++)
    {
        $row0 = $getgrossholdlist[$x]['TerminalId'];
        $row4 = $getgrossholdlist[$x]['PosName'];
        $row5 = $getgrossholdlist[$x]['DateStart'];
        $row6 = $getgrossholdlist[$x]['DateEnd'];
        $row7 = $getgrossholdlist[$x]['ID'];
        $row8 = $getgrossholdlist[$x]['TransactionSummaryID'];

//                $dateTime = new DateTime($row5);
//                $row2 = $dateTime->format("m/d/Y h:i:s A");
//                $dateTime1 = new DateTime($row6);
//                $row3 = $dateTime1->format("m/d/Y h:i:s A");

        $row2 = date('m/d/Y h:i:s A', strtotime($row5));
        $row3 = date('m/d/Y h:i:s A', strtotime($row6));

        $arr_record = array();
        $cash_total = 0;
        $noncash_total = 0;
        $non_cash = "";
        array_push($arr_record, $row2, $row3, $row4);

        $getamount = $terminalsessiondetails->getamountbyterminalid($row7, $row8, $row0);

        if (count($getamount) > 0)
        {
            for ($a = 0; $a < count($getamount); $a++)
            {
                // $i = 0;
                // 0 AS 'D',
                // 1 AS 'R',
                // 2 B.Name
                $deposit = $getamount[$a][0];
                $reload = $getamount[$a][1];
                if (!isset($deposit))
                    $deposit = number_format(0, 2);
                if (!isset($reload))
                    $reload = number_format(0, 2);

                $amountName = $getamount[$a][2];
                array_push($arr_record, $deposit, $reload, $amountName);
            }
        }

        $getprize = $terminalsessiondetails->detailsPrizebyterminalid($row7, $row8, $row0, $dDateFr, $dDateTo);
        if (count($getprize) > 0)
        {
            for ($b = 0; $b < count($getprize); $b++)
            {
                // 0 B.PrizeType,
                // 1 B.PrizeValue,
                // 2 C.PrizeDescription
                if ($getprize[$b][2] == "Cash")
                {
                    $cash_total = ($cash_total + $getprize[$b][1]);
                }
                else
                {
                    $noncash_total = ($noncash_total + $getprize[$b][1]);
                    $non_cash = $getprize[$b][3];
                    $non_cash = str_replace(',', ' ', substr($non_cash, strpos($non_cash, ',')));
                }

                /*
                 * Previous method of organizing NON-CASH prizes
                 * 
                  if($getprize[$b][2] == "Cash")
                  {
                  $cash_total = ($cash_total + $getprize[$b][1]);
                  }
                  else
                  {
                  $noncash_total = ($noncash_total + $getprize[$b][1]);
                  if ($b == count($getprize))
                  {
                  $non_cash .= "$" . $getprize[$b][1] . " " . $getprize[$b][2];
                  }
                  else
                  {
                  $i = $i;
                  foreach($getprize as $arrays)
                  {
                  if ($arrays["PrizeDescription"]== $getprize[$b][2] && $arrays["PrizeValue"]== $getprize[$b][1])
                  {
                  $i = $i + 1;
                  $mytest = "(".$i.")" . "$" .$getprize[$b][1]. " " . $getprize[$b][2];
                  }
                  }

                  $pos = strpos($non_cash, $mytest);

                  if ($pos === false)
                  {
                  if($non_cash != '')
                  {
                  $non_cash .= ", (".$i.")". "$" .$getprize[$b][1]. " " . $getprize[$b][2] . " ";
                  $mytest = '';
                  $i = 0;
                  }
                  else
                  {
                  $non_cash = "(".$i.")"."$" .$getprize[$b][1]. " " . $getprize[$b][2];
                  $mytest = '';
                  $i = 0;
                  }
                  }
                  else
                  {
                  $i = 0;
                  }
                  }
                  } */
            }
            array_push($arr_record, $cash_total, $noncash_total, $non_cash);
            $grosshold = ($deposit + $reload) - ($cash_total + $noncash_total);
            array_push($arr_record, $grosshold);
        }
        else
        {
            $grosshold = ($deposit + $reload) - ($cash_total + $noncash_total);
            array_push($arr_record, $cash_total, $noncash_total, $non_cash, $grosshold);
        }

        array_push($arr_records, $arr_record);
    }

    return $arr_records;
}

/*
 * back up (dont delete)
 * used in download button for csv file
 * 
 * for($x = 0;$x < count($getgrossholdlist1); $x++)
  {
  $row0 = $getgrossholdlist1[$x]['TerminalId'];
  $row1 = $getgrossholdlist1[$x]['SiteID'];
  $row4 = $getgrossholdlist1[$x]['PosName'];
  $row5 = $getgrossholdlist1[$x]['DateStart'];
  $row6 = $getgrossholdlist1[$x]['DateEnd'];
  $row7 = $getgrossholdlist1[$x]['ID'];

  $dateTime = new DateTime($row5);
  $row2 = $dateTime->format("m/d/Y h:i:s A");
  $dateTime1 = new DateTime($row6);
  $row3 = $dateTime1->format("m/d/Y h:i:s A");

  $arr_record = array();
  $cash_total = 0;
  $noncash_total = 0;
  $non_cash = "";
  array_push($arr_record , $row2 , $row3 , $row4);

  $getamount = $terminalsessiondetails->getamountbyterminalid($row7);
  if(count($getamount) > 0)
  {
  for($a=0;$a<count($getamount);$a++)
  {
  // 0  AS 'D',
  // 1  AS 'R',
  // 2  B.Name
  $deposit = $getamount[$a][0];
  $reload = $getamount[$a][1];
  ($deposit == null) ? 0 : $deposit;
  ($reload == null) ? 0 : $reload;
  $amountName = $getamount[$a][2];
  array_push($arr_record , $deposit , $reload , $amountName);
  }
  }

  $getprize = $terminalsessiondetails->detailsPrizebyterminalid($row7);
  if(count($getprize) > 0)
  {
  for($b = 0;$b < count($getprize); $b++)
  {
  // 0 B.PrizeType,
  // 1 B.PrizeValue,
  // 2 C.PrizeDescription

  if($getprize[$b][2] == "Cash")
  {
  $cash_total = ($cash_total + $getprize[$b][1]);
  }
  else
  {
  $noncash_total = ($noncash_total + $getprize[$b][1]);
  if($b == count($getprize))
  {
  $non_cash = $non_cash . "$" . $getprize[$b][1] . " " . $getprize[$b][2];
  }
  else
  {
  $non_cash = $non_cash . "$" . $getprize[$b][1] . " " . $getprize[$b][2] . ", ";
  }
  }
  }
  array_push($arr_record , $cash_total , $noncash_total , $non_cash);
  $grosshold = ($deposit + $reload) - ($cash_total + $noncash_total);
  array_push($arr_record , $grosshold);
  }
  else
  {
  array_push($arr_record , $cash_total , $noncash_total , $non_cash , 0);
  }
  array_push($arr_records1,$arr_record);
  }
 */
?>