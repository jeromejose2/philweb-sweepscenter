<?php
/*
 * Created By       :       Angelo Cubos
 * Date Created     :       March 13 2013
 * Purpose          :       Viewing for deck table details
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCC_DeckInfo");

App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$fproc = new FormsProcessor();
$ccdeckinfo = new SCCC_Deckinfo();

$ddldeckid = new ComboBox("ddldeckid", "ddldeckid", "");
//$ddldeckid->Args = "onchange='javascript: return SelectPOSAccountNumber()'";
$ddldeckid->Style ="width: 28px;";
$opt1 = null;
$opt1[] = new ListItem("---", "---", true);
$ddldeckid->Items = $opt1;
$posaccts = $ccdeckinfo->deckidonly("---");
$list_posaccts = new ArrayList();
$list_posaccts->AddArray($posaccts);
$ddldeckid->DataSource = $list_posaccts;
$ddldeckid->DataSourceValue = "DeckID";
$ddldeckid->DataSourceText = "DeckID";
$ddldeckid->DataBind();

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Search");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Style = "width:80px; height:30px; font-size:14;";
$btnSubmit->IsSubmit = true;

$fproc->AddControl($ddldeckid);
$fproc->AddControl($btnSubmit);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{   
    if ($btnSubmit->SubmittedValue == "Search" )
    {
        $id = $ddldeckid->SubmittedValue;
        $arrDeckinfo = $ccdeckinfo->deckinfobyid($id);
        $arrValidECN = $ccdeckinfo->getdeckinfobytable($id);
        
        $DeckID         =   $arrDeckinfo[0]["DeckID"];
        $ControlNo      =   $arrDeckinfo[0]["ControlNo"];
        $CardCount      =   $arrDeckinfo[0]["CardCount"];
        $ValidECN       =   $arrValidECN[0]["ValidECNs"];
        $DateCreated    =   $arrDeckinfo[0]["DateCreated"];
        $DateActivated  =   $arrDeckinfo[0]["DateActivated"];
        $DateClosed     =   $arrDeckinfo[0]["DateClosed"];
        $CreatedByAID   =   $arrDeckinfo[0]["CreatedByAID"];
        $Status         =   $arrDeckinfo[0]["Status"];

        
//        $Status = 0;
//        $ValidECN = 0;
        
    }
}
?>
