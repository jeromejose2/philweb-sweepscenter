<?php
/*
 * Created By       :       Angelo Cubos
 * Date Created     :       Mar 25, 2013
 * Purpose          :       Check Active Deck Consumption
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCC_DeckInfo");

App::LoadControl("Label");

$scdeckinfo = new SCCC_DeckInfo();
$fproc = new FormsProcessor();

$deckid = new Label('deckid','deckid');
$deckid->Style = "font-size: 20px; font-weight: bold;";

$fproc->AddControl($deckid);
$fproc->ProcessForms();


$resultarray = $scdeckinfo->getdeckinfostatusone();
$deckid->Text = $resultarray[0][DeckID];

$decknarray = $scdeckinfo->getdeckNdateused($resultarray[0][DeckID]);

if($resultarray[0][UsedCardCount] != $decknarray[0][ActualUsedCardCount]){
    $scdeckinfo->StartTransaction();    
    $scdeckinfo->UpdateUsedcardCount($resultarray[0][DeckID],$decknarray[0][ActualUsedCardCount]);
    if ($scdeckinfo->HasError)
    {
        $errormsg = $scdeckinfo->getErrors();
        $scdeckinfo->RollBackTransaction();
    }
    else
    {
        $scdeckinfo->CommitTransaction();
        $resultarray[0][UsedCardCount] = $decknarray[0][ActualUsedCardCount];
    }
}

if ($fproc->IsPostBack)
{

}
?>
