<?php

/*
 * Added On : March 12, 2013
 * Created by : Frances Ralph DL. Sison
 */
require_once('../init.inc.php');

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_Terminals");

$terminals = new SCC_Terminals();

$sid = $_POST["sid"];

$options = "<option value = '0'>Please select</option>";

$getlist = $terminals->getIDListExceptFreeEntry($sid);

if(count($getlist) > 0)
{
     for($i = 0 ; $i < count($getlist) ; $i++)
    {
        $options .= "<option selected value='".$getlist[$i]["ID"]."'>".$getlist[$i]["Name"]."</option>";
    }
}
echo $options;          


?>
