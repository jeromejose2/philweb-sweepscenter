<?php

/*
 * Created By   :   Noel Antonio
 * Date Created :   June 22, 2012
 * Purpose      :   Update Terminal Password
 */

require_once('../init.inc.php');

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_Terminals");
App::LoadModuleClass($modulename, "SCCSM_AgentSessions");
App::LoadModuleClass($modulename, "SCC_AuditTrail");
App::LoadModuleClass($modulename, "SCC_ref_Services");
App::LoadLibrary("MicrogamingAPI.class.php");
App::LoadCore("PHPMailer.class.php");

$cterminals = new SCC_Terminals();
$csmagentsessions = new SCCSM_AgentSessions();
$caudittrail = new SCC_AuditTrail();
$refservices = new SCC_ref_Services();

$terminal_account = $_POST['tacct'];
$fname = $_POST['fname'];
$selectactive = $refservices->SelectActiveService();
$provider = $selectactive[0]['ID'];
$arrTerminal = $cterminals->GetDetailsByTerminalID($terminal_account);
if (count($arrTerminal) > 0)
{
    $email = $arrTerminal[0][1];
    $un = $arrTerminal[0][2];
    $isFE = $arrTerminal[0][3];
    $oldpw = $arrTerminal[0][5];
}
$pw = substr(mt_rand(), 0, 7);

if ($isFE != 1)
{
    switch ($provider)
    {
        case 1:
            App::LoadModuleClass("CasinoAPI", "RealtimeGamingPlayerAPI");
            App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
            App::LoadSettings("swcsettings.inc.php");
            $playerurl = App::getParam("player_url");
            $certFilePath = App::getParam("certFilePath");
            $keyFilePath = App::getParam("keyFilePath");
            App::LoadModuleClass($modulename, "SCC_Accounts");
            App::LoadModuleClass("CasinoAPI", "RealtimeGamingPlayerAPI");
            $caccts = new SCC_Accounts();
            $arrTerminals = $caccts->LoadPOSTerminals($_SESSION['login1'], $_SESSION['acctname1']);
            $passPhrase = md5($arrTerminals[0]["Password"]);
            $casinoID = 1;
            $capiwrapper = new RealtimeGamingAPIWrapper($playerurl, RealtimeGamingAPIWrapper::PLAYER_API, $certFilePath, $keyFilePath);

            $changepasswordresult = $capiwrapper->ChangePassword($casinoID, $un, $oldpw, $pw);
            if ($changepasswordresult['IsSucceed'] == TRUE)
            {
                $changeres = true;
            }
            else
            {
                $changeres = false;
            }

            break;
        case 3:

            $arrSessionGUID_MG = $csmagentsessions->SelectSessionGUID();
            $sessionGUID_MG = $arrSessionGUID_MG[0]['SessionGUID'];
            $ip_add_MG = $_SERVER['REMOTE_ADDR'];
            $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>" .
                    "<SessionGUID>" . $sessionGUID_MG . "</SessionGUID>" .
                    "<IPAddress>" . $ip_add_MG . "</IPAddress>" .
                    "<ErrorCode>0</ErrorCode>" .
                    "<IsLengthenSession>true</IsLengthenSession>" .
                    "</AgentSession>";

            $client = new nusoap_client("https://entservices.totalegame.net/EntServices.asmx?WSDL", 'wsdl');
            $client->setHeaders($headers);
            $param = array('accountNumber' => $un, 'firstName' => '', 'lastName' => '', 'password' => $pw, 'isMobile' => false, 'mobileNumber' => '', 'isSendGame' => false);
            $result = $client->call('EditAccount', $param);
            $err_result = $result['EditAccountResult']['IsSucceed'];
            if ($err_result == 'false')
            {
                $changeres = false;
                echo "Error";
            }
            else
            {
                $changeres = true;
            }
            break;
    }
}
else
{
    $changeres = true;
}

if ($changeres == true)
{
    // Update Terminal Password
    $cterminals->StartTransaction();
    $updterminal["ID"] = $terminal_account;
    $updterminal["Password"] = MD5($pw);
    $updterminal["ServicePassword"] = $pw;
    $cterminals->UpdateByArray($updterminal);
    if ($cterminals->HasError)
    {
        echo "Error";
        $cterminals->RollBackTransaction();
    }
    else
    {
        $cterminals->CommitTransaction();

        // log to audit trail
        $caudittrail->StartTransaction();
        $scauditlogparam["SessionID"] = $_SESSION['sid'];
        $scauditlogparam["AccountID"] = $_SESSION['aid'];
        $scauditlogparam["TransDetails"] = "Change Terminal Account Password : Terminal ID - " . $terminal_account;
        $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
        $scauditlogparam["TransDateTime"] = "now_usec()";
        $caudittrail->Insert($scauditlogparam);
        if ($caudittrail->HasError)
        {
            echo "Error";
            $caudittrail->RollBackTransaction();
        }
        else
        {
            $caudittrail->CommitTransaction();

            // Sending of Email
            $pm = new PHPMailer();
            $pm->AddAddress($email, $fname);

            $pageURL = 'http';
            if (!empty($_SERVER['HTTPS']))
            {
                $pageURL .= "s";
            }
            $pageURL .= "://";
            $folder = $_SERVER["REQUEST_URI"];
            $folder = substr($folder, 0, strrpos($folder, '/') + 1);
            if ($_SERVER["SERVER_PORT"] != "80")
            {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $folder;
            }
            else
            {
                $pageURL .= $_SERVER["SERVER_NAME"] . $folder;
            }

            $pm->IsHTML(true);

            $pm->Body = "Dear " . $fname . "<br/><br/>
                This is to inform you that your password has been changed on this date " . date("m/d/Y") . " and time " . date("H:i:s") . ". 
                Here are your new Account Details:<br /><br />" .
                    "Username: <b>$un</b><br />
                Password: <b>$pw</b><br /><br />" .
                    "If you didn't perform this procedure, please report this to our Customer Service email at support.gu@philwebasiapacific.com.<br /><br />" .
                    "Regards,<br/><br/>Customer Support<br/><br/>The Sweeps Center";

            $pm->From = "support.gu@philwebasiapacific.com";
            $pm->FromName = "The Sweeps Center";
            $pm->Host = "localhost";
            $pm->Subject = "NOTIFICATION FOR CHANGE PASSWORD OF TERMINAL ACCOUNT";
            $email_sent = $pm->Send();
            if (!$email_sent)
            {
                echo "Error";
            }
            else
            {
                echo "Terminal password successfully changed!";
            }
        }
    }
}
else
{
    echo "Error";
}
?>
