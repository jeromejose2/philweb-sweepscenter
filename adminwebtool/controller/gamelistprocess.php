
<?php

require_once("../init.inc.php");
$pagesubmenuid = 18;
$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/validations.js";

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_CasinoGames");

App::LoadModuleClass($modulename, "SCC_AdminAccounts");
App::LoadModuleClass($modulename, "SCC_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCC_AuditTrail");


App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");

$frmgamelist = new FormsProcessor();
$sccgames = new SCC_CasinoGames();

///*PAGING*/
//$itemsperpage = 10;
//$pgcon = new PagingControl2($itemsperpage,1);
//$pgcon->URL = "javascript:ChangePage(%currentpage)";
//$pgcon->PageGroup = 5;
//$pgcon->ShowMoveToFirstPage = true;
//$pgcon->ShowMoveToLastPage = true;
///*PAGING*/

//$games = $sccgames->SelectAllGames();
//$gamecount = count($games);
//$pgcon->Initialize($itemsperpage, $gamecount);
//$pgTransactionHistory = $pgcon->PreRender();
//$arrproviders = $sccgames->SelectAllGamesWithLimit(($pgcon->SelectedItemFrom - 1), $itemsperpage);
//$gameslist = new ArrayList();
//$gameslist->AddArray($arrproviders);

$ddlProviders = new ComboBox("ddlProviders","ddlProviders","Service : ");
$ddlProviders->ShowCaption = true;
$litem = null;
$litem[] = new ListItem("--Please Select--", 0, true);
$litem[] = new ListItem("RTG", 1);
$litem[] = new ListItem("MG", 3);
$ddlProviders->Items = $litem;

$hiddengameid  = new Hidden("hiddengameid","hiddengameid","Hidden game id");

$hiddenctr = new Hidden("hiddenctr","hiddenctr","Hidden Button Ctr");

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->Args = "onclick='javascript: return checkProviderSubmit()'";

$btnYes = new Button("btnYes","btnYes","YES");
$btnYes->IsSubmit = true;

$btnNo = new Button("btnNo","btnNo","NO");
$btnNo->IsSubmit = true;

$frmgamelist->AddControl($hiddengameid);
$frmgamelist->AddControl($ddlProviders);
$frmgamelist->AddControl($btnSubmit);
$frmgamelist->AddControl($hiddenctr);
$frmgamelist->AddControl($btnYes);




$frmgamelist->ProcessForms();


if($frmgamelist->IsPostBack)
{
/*PAGING*/
$itemsperpage = 10;
$pgcon = new PagingControl2($itemsperpage,1);
$pgcon->URL = "javascript:ChangePage(%currentpage)";
$pgcon->PageGroup = 5;
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
/*PAGING*/     
  
$tmpprovider = $gamestmp[0][ServiceID];
if($btnYes->SubmittedValue == "YES")
{
$btnSubmit->SubmittedValue = "Submit";    
}


if(($tmpprovider != NULL || $btnYes->SubmittedValue == "YES" || $ddlProviders->SelectedValue > 0) &&
   ($btnYes->SubmittedValue == "YES" || $btnSubmit->SubmittedValue == "Submit" || isset($_POST['hiddenctr'])))
    {
        $tmppage = $_POST['pgSelectedPage'];
        $tmppage2 = $_POST['hiddenctr'];
        
        if (strlen($tmppage2) != 0)
        {
            $pgcon->SelectedPage = $_POST['hiddenctr'];
        }else{
            $pgcon->SelectedPage = 1;
        }

        
    if($btnYes->SubmittedValue == "YES")
    {   
        if($tmppage != $tmppage2)
        {
            $pgcon->SelectedPage = $_POST['pgSelectedPage'];
        }
        $id = $_POST['hiddengameid'];
        $where = " WHERE CasinoGameID = $id ";
        $gamestmp = $sccgames->SelectAllGamesWhere($where);

        $tmpstat = $gamestmp[0][Status];
        
        if($tmpstat == 1){
            $tmpstat = 0;
        }
        else{
            $tmpstat = 1;
        }    
        $games = $sccgames->UpdateGameStatus($gamestmp[0][CasinoGameID],$tmpstat);
    }
    if($btnSubmit->SubmittedValue == "Submit" || $btnYes->SubmittedValue == "YES" || isset($_POST['hiddenctr']))
    {

        $tmpprovider = $gamestmp[0][ServiceID];
        $hiddenctr->Text = 1;

        if($ddlProviders->SelectedValue == "1" || $tmpprovider == "1")
        {
        if($tmpprovider == "1"){
        $where = " WHERE ServiceID = " . $tmpprovider . " ORDER BY CasinoGameID ASC";
        }else{
        $where = " WHERE ServiceID = " . $ddlProviders->SelectedValue . " ORDER BY CasinoGameID ASC";
        }
        $games = $sccgames->SelectAllGamesWhere($where);
        }
        
        if($ddlProviders->SelectedValue == "3" || $tmpprovider == "3")
        {
        if($tmpprovider == "3"){    
        $where = " WHERE ServiceID = " . $tmpprovider . " ORDER BY CasinoGameID ASC";
        }else{
        $where = " WHERE ServiceID = " . $ddlProviders->SelectedValue . " ORDER BY CasinoGameID ASC";    
        }
        $games = $sccgames->SelectAllGamesWhere($where);
        }
        
        
        $gameslist = "";
        $gamecount = count($games);
        $pgcon->Initialize($itemsperpage, $gamecount);
        $pgTransactionHistory = $pgcon->PreRender();
        $arrproviders = $sccgames->SelectAllGamesWithLimit($where,($pgcon->SelectedItemFrom - 1), $itemsperpage);
        $gameslist = new ArrayList();
        $gameslist->AddArray($arrproviders);
        
    }
}
}
?>