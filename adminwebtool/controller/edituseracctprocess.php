<?php

/*
 * Added By : Jerome F. Jose
 * Added On : June 15, 2012
 * Purpose : Edit User Admin Account
 */
require_once("../init.inc.php");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_AdminAccounts");
App::LoadModuleClass($modulename, "SCC_ref_AdminAccountTypes");
App::LoadModuleClass($modulename, "SCC_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCC_AuditTrail");

$ref_adminaccounttypes = new SCC_ref_AdminAccountTypes();
$adminaccountsession = new SCC_AdminAccountSessions();
$adminaccount = new SCC_AdminAccounts();
$audittrail = new SCC_AuditTrail();
$fproc = new FormsProcessor();


$ddlUsername = new ComboBox("ddlUsername", "ddlUsername");
$ddlgroupusername = Null;
$ddlgroupusername[] = new ListItem("Select One", "0", true);
$ddlUsername->Items = $ddlgroupusername;
$ddlgroupusername = new ArrayList();
$getgroups = $adminaccount->GetAdminbyActive();
$ddlgroupusername->AddArray($getgroups);
$ddlUsername->DataSource = $ddlgroupusername;
$ddlUsername->DataSourceText = "Username";
$ddlUsername->DataSourceValue = "AccountID";
$ddlUsername->DataBind();
$ddlUsername->Args = "onchange = 'javascript: onchange_username();'";


$hidUsername = new Hidden("hidUsername", "hidUsername");
$hidAccountID = new Hidden("hidAccountID", "hidAccountID");

$btnSearch = new Button("btnSearch", "btnSearch", "Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checkUsername();' ";

$txtUName = new TextBox("txtUName", "txtUName", "Username");
$txtUName->Length = '12';
//$txtUName->ShowCaption = true;
//$txtUName->Args = "";


$txtFName = new TextBox("txtFName", "txtFName");
$txtFName->Length = '50';
$txtFName->Args = "onkeypress ='javascript : return AlphaOnly(event);'";
$txtFName->Style = "width:200px;text-align:center;";

$txtMName = new TextBox("txtMName", "txtMName");
$txtMName->Length = '50';
$txtMName->Args = "onkeypress ='javascript : return AlphaOnly(event);'";
$txtMName->Style = "width:200px;text-align:center;";

$txtLName = new TextBox("txtLName", "txtLName");
$txtLName->Length = '50';
$txtLName->Args = "onkeypress ='javascript : return AlphaOnly(event);'";
$txtLName->Style = "width:200px;text-align:center;";

$txtEmail = new TextBox("txtEmail", "txtEmail");
$txtEmail->Length = '50';
//$txtEmail->Args = "onkeypress = '' ";
$txtEmail->Style = "width:250px;text-align:center;";
$txtEmail->Args = "onkeypress='javascript: return verifyEmail(event);'";

$txtPosition = new TextBox("txtPosition", "txtPosition");
$txtPosition->Length = '50';
$txtPosition->Args = "onkeypress ='javascript : return AlphaOnly(event);'";
$txtPosition->Style = "width:200px;text-align:center;";

$txtCompany = new TextBox("txtCompany", "txtCompany");
$txtCompany->Length = '50';
//$txtCompany->Args = "onkeypress = ''";
$txtCompany->Style = "width:200px;text-align:center;";
$txtCompany->Args = "onkeypress ='javascript : return AlphaOnly(event);'";

$txtDepartment = new TextBox("txtDepartment", "txtDepartment");
$txtDepartment->Length = '50';
//$txtDepartment->Args = "onkeypress = ''";
$txtDepartment->Style = "width:200px;text-align:center;";
$txtDepartment->Args = "onkeypress ='javascript : return AlphaOnly(event);'";

$ddlGroup = new ComboBox("ddlGroup", "ddlGroup");
$grouplist = Null;
$grouplist[] = new ListItem("Select Group", 0, true);
$ddlGroup->Items = $grouplist;
$getall = $ref_adminaccounttypes->GetAccoutTypes();
$grouplist = new ArrayList();
$grouplist->AddArray($getall);
$ddlGroup->DataSource = $grouplist;
$ddlGroup->DataSourceText = "Name";
$ddlGroup->DataSourceValue = "ID";
$ddlGroup->DataBind();

$btnCancel = new Button("btnCancel", "btnCancel", "Cancel");
$btnCancel->IsSubmit = true;
$btnCancel->CssClass = "labelbutton2";
$btnCancel->Args = "onclick='javascript: return resetadduseracct();'";

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Apply Changes");
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Args = "onclick='javascript: return checkedituseracct();'";

$btnback = new Button("Back", "Back", "Back");
$btnback->CssClass = "labelbutton2";
(($_SESSION['tempuname'] == "") && (!isset($_SESSION['tempuname']))) ? $btnback->Args = "onclick='javascript: return redirect();' " : $btnback->IsSubmit = true;


$hidGroup = new Hidden("hidGroup", "hidGroup");

$fproc->AddControl($txtUName);
$fproc->AddControl($txtFName);
$fproc->AddControl($txtMName);
$fproc->AddControl($txtLName);
$fproc->AddControl($txtEmail);
$fproc->AddControl($txtPosition);
$fproc->AddControl($txtCompany);
$fproc->AddControl($txtDepartment);
$fproc->AddControl($ddlGroup);
$fproc->AddControl($hidGroup);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddlUsername);
$fproc->AddControl($hidUsername);
$fproc->AddControl($hidAccountID);
$fproc->AddControl($btnSearch);
$fproc->AddControl($btnback);


$fproc->ProcessForms();


if ($_SESSION['tempuname'] != "")
{

    $username = $_SESSION['tempuname'];
    $ddlUsername->SetSelectedValue($_SESSION['UnameAcctID']);
    $displayinfo = 'ok';
    $getall = $adminaccount->getallinfobyUsername($username);
    $acctid = str_pad($getall[0]["AcctID"], 10, "0", STR_PAD_LEFT);

    $uname = $getall[0]["Username"];
    $datecreated = $getall[0]["DateCreated"];
    $datetime = new DateTime($datecreated);
    $datecreated = $datetime->format("m/d/Y h:i:s A");
    $status = $getall[0]["AcctStatus"];
    if ($status == 1)
    {
        $status = "ACTIVE";
    }
    else if ($status == 2)
    {
        $status = "INACTIVE";
    }
    else if ($status == 3)
    {
        $status = "SUSPENDED";
    }
    else if ($status == 4)
    {
        $status = "TERMINATED";
    }
    else
    {
        $status = "";
    }
    $txtFName->Text = $getall[0]["FirstName"];
    $txtMName->Text = $getall[0]["MiddleName"];
    $txtLName->Text = $getall[0]["LastName"];
    $txtEmail->Text = $getall[0]["Email"];
    $txtPosition->Text = $getall[0]["position"];
    $txtCompany->Text = $getall[0]["Company"];
    $txtDepartment->Text = $getall[0]["Department"];
    $ddltextgroup = $getall[0]["AccountType"];
    $ddlGroup->SetSelectedValue($ddltextgroup);
}
if ($fproc->IsPostBack)
{


    $sessionID = $_SESSION['sid'];

    if ($btnSearch->SubmittedValue == "Search")
    {

        $ddlUsername->SetSelectedValue($ddlUsername->SubmittedValue);
        $username = $_POST['hidUsername'];
        $_SESSION['tempuname'] = $username;
        $displayinfo = 'ok';
        $getall = $adminaccount->getallinfobyUsername($username);

        $acctid = str_pad($getall[0]["AcctID"], 10, "0", STR_PAD_LEFT);
        $uname = $getall[0]["Username"];
        $datecreated = $getall[0]["DateCreated"];
        $datetime = new DateTime($datecreated);
        $datecreated = $datetime->format("m/d/Y h:i:s A");
        $status = $getall[0]["AcctStatus"];
        if ($status == 1)
        {
            $status = "ACTIVE";
        }
        else if ($status == 2)
        {
            $status = "INACTIVE";
        }
        else if ($status == 3)
        {
            $status = "SUSPENDED";
        }
        else if ($status == 4)
        {
            $status = "TERMINATED";
        }
        else
        {
            $status = "";
        }

        $txtFName->Text = $getall[0]["FirstName"];
        $txtMName->Text = $getall[0]["MiddleName"];
        $txtLName->Text = $getall[0]["LastName"];
        $txtEmail->Text = $getall[0]["Email"];
        $txtPosition->Text = $getall[0]["position"];
        $txtCompany->Text = $getall[0]["Company"];
        $txtDepartment->Text = $getall[0]["Department"];
        $ddltextgroup = $getall[0]["AccountType"];
        $ddlGroup->SetSelectedValue($ddltextgroup);
    }

    if ($btnSubmit->SubmittedValue == "Apply Changes")
    {

        $username = $_SESSION['tempuname'];
        $getall = $adminaccount->getallinfobyUsername($username);

        $acctid = str_pad($getall[0]["AcctID"], 10, "0", STR_PAD_LEFT);
        $uname = $getall[0]["Username"];
        $datecreated = $getall[0]["DateCreated"];
        $datetime = new DateTime($datecreated);
        $datecreated = $datetime->format("m/d/Y h:i:s A");
        $status = $getall[0]["AcctStatus"];
        if ($status == 1)
        {
            $status = "ACTIVE";
        }
        else if ($status == 2)
        {
            $status = "INACTIVE";
        }
        else if ($status == 3)
        {
            $status = "SUSPENDED";
        }
        else if ($status == 4)
        {
            $status = "TERMINATED";
        }
        else
        {
            $status = "";
        }

//        function updateuser()
        $getsessdetail = $adminaccountsession->GetSessionID($sessionID);
        if (count($getsessdetail) > 0)
        {
            $getUname = $adminaccount->GetIdUnameRemoteIp($sessionID);
            $Uname = $getUname[0]['Username'];
            $acctid2 = $getUname[0]['ID'];
            $remoteip = $getUname[0]['RemoteIP'];
        }

        $getAccountID = $adminaccount->getID($username);
        $accountid = $getAccountID[0]['AccountID'];
        $getdetails = $adminaccount->GetAccountID($accountid);

        if (count($getdetails) > 0)
        {
            $accountName = "" . $txtFName->SubmittedValue . " " . $txtMName->SubmittedValue . " " . $txtLName->SubmittedValue . " ";
            $username = $getdetails[0]['Username'];

            //update account
            $adminaccount->StartTransaction();
            $adminaccount->UpdateAccount($txtFName->SubmittedValue, $txtMName->SubmittedValue, $txtLName->SubmittedValue, $txtEmail->SubmittedValue, $txtPosition->SubmittedValue, $txtDepartment->SubmittedValue, $txtCompany->SubmittedValue, $ddlGroup->SubmittedValue, $_SESSION['uname'], $accountid);

            if ($adminaccount->HasError)
            {
                $adminaccount->RollBackTransaction();
            }
            else
            {
                $adminaccount->CommitTransaction();

                // -- insert in audit trail
                $audittrail->StartTransaction();
                $arrAudittrail["SessionID"] = $sessionID;
                $arrAudittrail["AccountID"] = $acctid2;
                $arrAudittrail["TransDetails"] = "Update status of AccountID: $accountid ";
                $arrAudittrail["RemoteIP"] = $remoteip;
                $arrAudittrail["TransDateTime"] = "now_usec()";
                $audittrail->Insert($arrAudittrail);
                if ($audittrail->HasError)
                {
                    $audittrail->RollBackTransaction();
                }
                else
                {
                    $audittrail->CommitTransaction();

                    $isProcessed = 0;
                    $returnmsg2 = 'User profile successfully updated';
                }
            }
        }
    }

    if (isset($_POST['Back']))
    {
        unset($_SESSION['tempuname']);
        App::Pr("<script> window.location = 'useracctprofile.php'; </script>");
    }

    if (isset($_POST['btnOkClose1']))
    {
        unset($_SESSION['tempuname']);
        App::Pr("<script> window.location = 'useracctprofile.php'; </script>");
    }
}
?>
