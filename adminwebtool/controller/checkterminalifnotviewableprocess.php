<?php

/*
 * Added On: April 18, 2013
 * Created By: ANGC
 * Purpose: Check If Terminal Is Not Viewable In The POS
 */

require_once("../init.inc.php");
require_once($librarydir . 'MicrogamingAPI.class.php');
include("../config.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_Terminals");
App::LoadModuleClass($modulename, "SCC_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCC_AdminAccounts");
App::LoadModuleClass($modulename, "SCC_AuditTrail");
App::LoadModuleClass($modulename, "SCCSM_AgentSessions");
App::LoadModuleClass($modulename, "SCC_TerminalSessions");
App::LoadModuleClass($modulename, "SCC_TransactionLogs");
App::LoadModuleClass($modulename, "SCC_ManualRedemption");
App::LoadModuleClass($modulename, "SCC_ref_Services");
App::LoadModuleClass("SweepsCenter", "SCCC_AgentAccounts");

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("Label");
App::LoadControl("Hidden");

$refservices = new SCC_ref_Services();

$selectactive = $refservices->SelectActiveService();
$provider = $selectactive[0]['ID'];

$fproc = new FormsProcessor();
$accounts = new SCC_Accounts();
$cagentaccounts = new SCCC_AgentAccounts();
$terminals = new SCC_Terminals();
$servicemapping = new SCCSM_AgentSessions();
$terminalsession = new SCC_TerminalSessions();
$audittrail = new SCC_AuditTrail();
$transactionlogs = new SCC_TransactionLogs();
$manualredemption = new SCC_ManualRedemption();

$manred = "Terminal is subject for manual redemption";
$notmanred = "Terminal is not subject for manual redemption";

$listddl = $accounts->SelectSiteIDbylist();
$ddlPosName = new ComboBox("ddlPosName","ddlPosName");
$arrlist = new ArrayList();
$arrlist->AddArray($listddl);
$list = null;
$list[] = new ListItem("","0",true);
$ddlPosName->Items = $list;
$ddlPosName->DataSource = $arrlist;
$ddlPosName->DataSourceText = "Name";
$ddlPosName->DataSourceValue = "SiteID";
$ddlPosName->DataBind();
$ddlPosName->Args = "onchange='javascript: return get_terminals_exceptfreeentry();'";



$ddlTerminalName = new ComboBox("ddlTerminalName","ddlTerminalName");
$list = null;
$list[] = new ListItem("","",true);
$ddlTerminalName->Items = $list;

$btnSearch = new Button("btn","btn","SEARCH");
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checkmanualredemptionchk();'";
$btnSearch->IsSubmit = true;

$btnReset = new Button("btnReset","btnReset","RESET");
$btnReset->CssClass = "labelbutton2";
$btnReset->Args = "onclick='javascript: return confirmationreset();'";
//$btnReset->IsSubmit = true;

$btnProceed = new Button("btnProceed","btnProceed","OKAY");
$btnProceed->CssClass = "labelbutton2";
$btnProceed->IsSubmit = true;

$recommendation = new Label("rec","rec");


$hidlist = new Hidden("hidlist","hidlist");
       
$fproc->AddControl($ddlPosName);
$fproc->AddControl($ddlTerminalName);
$fproc->AddControl($btnSearch);
$fproc->AddControl($recommendation);
$fproc->AddControl($hidlist);
$fproc->AddControl($btnReset);
$fproc->AddControl($btnProceed);

$fproc->ProcessForms();

if($fproc->IsPostBack)
{
    if($btnProceed->SubmittedValue == "OKAY")
    {   $terminals->StartTransaction();
        $terminals->setstatustoone($_SESSION['mr_terminalid']);
        if($terminals->HasError)
        {
            $terminals->RollBackTransaction();
            
        }else{
            $terminals->CommitTransaction();
        
            $audittrail->StartTransaction();
            $arrAudittrail['SessionID'] = $_SESSION['sid'];
            $arrAudittrail['AccountID'] = $_SESSION['aid'];
            $arrAudittrail['TransDetails'] = "Change IsPlayable to 1 and Balance to 0.00 :".$_SESSION['mr_TerminalName'];
            $arrAudittrail['RemoteIP'] = $_SERVER['REMOTE_ADDR'];
            $arrAudittrail['TransDateTime'] = "now_usec()";
            $audittrail->Insert($arrAudittrail); 
            if($terminals->HasError)
            {
                $audittrail->RollBackTransaction();
            }else{            
                $audittrail->CommitTransaction();
            }
        }
    }    
    $display = false;
    
    $_SESSION['mr_siteid'] = $ddlPosName->SubmittedValue;
    $_SESSION['mr_terminalid'] = $ddlTerminalName->SubmittedValue;
    
    $arrisviewable = $terminals->checkifviewable($_SESSION['mr_terminalid']);
    if(count($arrisviewable)!=0)
    {
        $status = "Viewable";
    }else{
        $status = "Not Viewable";
    }
    
    $listManualRedemption = $terminals->SelectTerminalDetailsforManualRedemptionChk($_SESSION['mr_terminalid']);
    
    $posaccountname = $listManualRedemption[0]['PosAccountName'];
    $terminalname = $listManualRedemption[0]['Name'];
    
    $terminalbalance = $listManualRedemption[0]['Balance'];

    $tsession = $terminalsession->getSessionDtls2($_SESSION['mr_terminalid'], $_SESSION['mr_siteid']);
    
    $datestart = $tsession[0]['DateStart'];
    
    $dateend = $tsession[0]['DateEnd'];
    
   switch ($provider)
   {
       Case 1:
                    App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
                    App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");    
                    App::LoadSettings("swcsettings.inc.php");
                
                    $rtgurl = App::getParam("rtg_url");
                    $certFilePath = App::getParam("certFilePath");
                    $keyFilePath = App::getParam("keyFilePath");
                 
                    $capiwrapper = new RealtimeGamingAPIWrapper($rtgurl,  RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);
                    $login = $terminalname;
                   
                    
                    $a = 1;
                                       
                    
                    while($a < 4)
                    {
                     
                        $result = $capiwrapper->GetBalance($login);
                        $a++;
                    }
                   
                    if ($result['IsSucceed'] == true)
                    {
                        $casinobalance = $result['BalanceInfo']['Balance'];
                    }

                    
                    break;
       Case 3:
                    
                    $getall = $servicemapping->selectbywhere();
                   if(count($getall) == 1)
                   {
                       $sessionGUID_MG = $getall[0]["SessionGUID"];
                   }

                   $ip_add_MG = $_SERVER['REMOTE_ADDR'];
                   $player = $terminalname;

                   $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
                           "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
                           "<IPAddress>".$ip_add_MG."</IPAddress>".
                           "<ErrorCode>0</ErrorCode>".
                           "<IsLengthenSession>true</IsLengthenSession>".
                           "</AgentSession>";

                   $client = new nusoap_client('https://entservices.totalegame.net/EntServices.asmx?WSDL', 'wsdl');
                   $client->setHeaders($headers);
                   $param = array('delimitedAccountNumbers' => $player);
                   $result = $client->call('GetAccountBalance', $param);
                   $casinobalance = $casinobalance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];          
           
                    break;
    
   }

     
    if($btnSearch->SubmittedValue == "SEARCH" || $btnProceed->SubmittedValue == "OKAY")
    {
    $arrisviewable = $terminals->checkifviewable($_SESSION['mr_terminalid']);
    if(count($arrisviewable)!=0)
    {
        $status = "Viewable";
    }else{
        $status = "Not Viewable";
    }
        
        $getlist = $terminals->getIDList($_SESSION['mr_siteid']);
        $list_terminals = new ArrayList();
        $list_terminals->AddArray($getlist);
        $ddlTerminalName->DataSource = $list_terminals;
        $ddlTerminalName->DataSourceText = "Name";
        $ddlTerminalName->DataSourceValue = "ID";
        $ddlTerminalName->DataBind();
        $ddlTerminalName->SetSelectedValue($_SESSION['mr_terminalid']);
        if((($terminalbalance > 0) && ($casinobalance == 0)) && ($dateend != 0) && ($status == "Not Viewable")){//if(($terminalbalance != $casinobalance) && ($dateend != 0)){
            $recommendation = $btnReset;
        }
        else {
            $recommendation = "";
        }
            $display = true;
            $_SESSION['mr_TerminalName'] = $terminalname;
            
    }
    
  
}
?>