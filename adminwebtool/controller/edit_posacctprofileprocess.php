<?php

/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 18, 2012
 * Purpose          :       POS Account Update Profile Controller: Updating of POS Account
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_AccountDetails");
App::LoadModuleClass($modulename, "SCC_AccountBankDetails");
App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_AuditTrail");

App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("CheckBox");

$fproc = new FormsProcessor();
$cacctdtls = new SCC_AccountDetails();
$cacctbankdtls = new SCC_AccountBankDetails();
$caccts = new SCC_Accounts();
$caudittrail = new SCC_AuditTrail();
/* -- LOAD POS ACCOUNT RECORDS ------------------------------------------------- */
$_SESSION['pos_acctid'] = $_POST['hiddenid'];
$arrposdtls = $cacctdtls->GetPOSAccountDetails($_SESSION['pos_acctid']);
$list_posdtls = new ArrayList();
$list_posdtls->AddArray($arrposdtls);

if (count($list_posdtls) > 0)
{
    $acct_name = $list_posdtls[0]["LastName"] . ", " . $list_posdtls[0]["FirstName"] . " " . $list_posdtls[0]["MiddleName"];
    $acct_id = $list_posdtls[0]["AccountID"];
    $uname = $list_posdtls[0]["Username"];
    $lname = $list_posdtls[0]["LastName"];
    $fname = $list_posdtls[0]["FirstName"];
    $mname = $list_posdtls[0]["MiddleName"];
    $swc_code = $list_posdtls[0]["SWCCode"];
    $home_addr = $list_posdtls[0]["HomeAddress"];
    $zip = $list_posdtls[0]["ZipCode"];
    $tel_no = $list_posdtls[0]["TelephoneNumber"];
    $mob_no = $list_posdtls[0]["MobileNumber"];
    $fax = $list_posdtls[0]["Fax"];
    $email = $list_posdtls[0]["Email"];
    $website = $list_posdtls[0]["Website"];
    $country = $list_posdtls[0]["CountryID"];
    $citizen = $list_posdtls[0]["Citizenship"];
    $bday = $list_posdtls[0]["BirthDate"];
    $pass = $list_posdtls[0]["Passport"];
    $d_lic = $list_posdtls[0]["DriversLicense"];
    $sss = $list_posdtls[0]["SocialSecurity"];
    $others = $list_posdtls[0]["Others"];
    $bank = $list_posdtls[0]["Bank"];
    $bank_type = $list_posdtls[0]["BankType"];
    $bank_bnch = $list_posdtls[0]["BankBranch"];
    $bank_acct_nme = $list_posdtls[0]["BankAccountName"];
    $bank_acct_num = $list_posdtls[0]["BankAccountNumber"];
    $payee_bank = $list_posdtls[0]["PayeeBank"];
    $payee_bank_type = $list_posdtls[0]["PayeeBankType"];
    $payee_bank_branch = $list_posdtls[0]["PayeeBankBranch"];
    $payee_bank_acct_nme = $list_posdtls[0]["PayeeBankAccountName"];
    $payee_bank_acct_num = $list_posdtls[0]["PayeeBankAccountNumber"];
    $status = $list_posdtls[0]['Stat'];
    $siteID = $list_posdtls[0]['SI'];
    $agentuname = $list_posdtls[0]["AgentUserName"];
}
else
{
    $acct_name = "";
    $acct_id = "";
    $uname = "";
    $lname = "";
    $fname = "";
    $mname = "";
    $swc_code = "";
    $home_addr = "";
    $zip = "";
    $tel_no = "";
    $mob_no = "";
    $fax = "";
    $email = "";
    $website = "";
    $country = "";
    $citizen = "";
    $bday = "";
    $pass = "";
    $d_lic = "";
    $sss = "";
    $others = "";
    $bank = "";
    $bank_type = "";
    $bank_bnch = "";
    $bank_acct_nme = "";
    $bank_acct_num = "";
    $payee_bank = "";
    $payee_bank_type = "";
    $payee_bank_branch = "";
    $payee_bank_acct_nme = "";
    $payee_bank_acct_num = "";
    $status = "";
    $siteID = "";
    $agentuname = "";
}
/* -- LOAD POS ACCOUNT RECORDS ------------------------------------------------- */

$ddlacctname = new ComboBox("ddlacctname", "ddlacctname", "Account Name");
$ddlacctname->Args = "onchange='javascript: return SelectPOSAccountNumber()'";
$opt1 = null;
$opt1[] = new ListItem("Select One", "Select One", true);
$ddlacctname->Items = $opt1;
$posaccts = $cacctdtls->LoadPOSAccounts("Select One");
$list_posaccts = new ArrayList();
$list_posaccts->AddArray($posaccts);
$ddlacctname->DataSource = $list_posaccts;
$ddlacctname->DataSourceValue = "AccountID";
$ddlacctname->DataSourceText = "FullName";
$ddlacctname->DataBind();
$ddlacctname->SetSelectedValue($_SESSION['pos_acctid']);

$ddlacctno = new ComboBox("ddlacctno", "ddlacctno", "Account Number");
$ddlacctno->Args = "onchange='javascript: return SelectPOSAccountName()'";
$opt2 = null;
$opt2[] = new ListItem("Select One", "", true);
$ddlacctno->Items = $opt2;
$posacctsid = $cacctdtls->LoadPOSAccountID();
$list_posacctsid = new ArrayList();
$list_posacctsid->AddArray($posacctsid);
$ddlacctno->DataSource = $list_posacctsid;
$ddlacctno->DataSourceValue = "AccountID";
$ddlacctno->DataSourceText = "AccountID";
$ddlacctno->DataBind();
$ddlacctno->SetSelectedValue($_SESSION['pos_acctid']);

$btnSearch = new Button("btnSearch", "btnSearch", "Search");
$btnSearch->CssClass = "labelbutton2";
$btnSearch->IsSubmit = true;

$btnSave = new Button("btnSave", "btnSave", "Save");
$btnSave->CssClass = "labelbutton2";
$btnSave->IsSubmit = true;

$btnBack = new Button("btnBack", "btnBack", "Back");
$btnBack->CssClass = "labelbutton2";
$btnBack->Style = "margin-left: 20px;";

$hiddenid = new Hidden("hiddenid", "hiddenid", "Hidden Account ID");
$hiddenid->Text = $acct_id;

$hidden = new Hidden("hidden", "hidden");

// --
$txtuname = new TextBox("txtuname", "txtuname", "Cashier Username");
$txtuname->Length = 15;
$txtuname->Text = $uname;
$txtuname->ReadOnly = true;
$txtuname->Args = "autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtscode = new TextBox("txtscode", "txtscode", "SWC Code");
$txtscode->Length = 3;
$txtscode->Text = $swc_code;
$txtscode->ReadOnly = true;
$txtscode->Args = "autocomplete='off' onkeypress='javascript: return AlphaOnly(event);'";

$txtagentuname = new TextBox("txtagentuname", "txtagentuname", "*Agent Username");
$txtagentuname->Length = 3;
$txtagentuname->Text = $agentuname;
$txtagentuname->ReadOnly = true;
$txtagentuname->Args="autocomplete='off' onkeypress='javascript: return AlphaOnly(event);'";

$txthadd = new TextBox("txthadd", "txthadd", "Home Address");
$txthadd->Multiline = true;
$txthadd->Style = "margin-left: 30px; width: 400px;";
$txthadd->Text = $home_addr;
$txthadd->Length = 100;

$txtfname = new TextBox("txtfname", "txtfname", "First Name");
$txtfname->Text = $fname;
$txtfname->ReadOnly = true;
$txtfname->Args = "autocomplete='off' onkeypress='javascript: return AlphaOnly(event);'";
$txtfname->Length = 50;

$txtmname = new TextBox("txtmname", "txtmname", "Middle Name");
$txtmname->Text = $mname;
$txtmname->ReadOnly = true;
$txtmname->Args = "autocomplete='off' onkeypress='javascript: return AlphaOnly(event);'";
$txtmname->Length = 50;

$txtlname = new TextBox("txtlname", "txtlname", "Last Name");
$txtlname->Text = $lname;
$txtlname->ReadOnly = true;
$txtlname->Args = "autocomplete='off' onkeypress='javascript: return AlphaOnly(event);'";
$txtlname->Length = 50;

$txtzip = new TextBox("txtzip", "txtzip", "Zip Code");
$txtzip->Text = $zip;
$txtzip->Args = "autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtzip->Length = 10;

$txttelno = new TextBox("txttelno", "txttelno", "Telephone Number");
$txttelno->Text = $tel_no;
$txttelno->Args = "autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txttelno->Length = 20;

$txtmobno = new TextBox("txtmobno", "txtmobno", "Mobile Number");
$txtmobno->Text = $mob_no;
$txtmobno->Args = "autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtmobno->Length = 20;

$txtfax = new TextBox("txtfax", "txtfax", "Fax");
$txtfax->Text = $fax;
$txtfax->Args = "autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtfax->Length = 20;

$txtemail = new TextBox("txtemail", "txtemail", "Email Address");
$txtemail->Text = $email;
$txtemail->Args = "autocomplete='off' onkeypress='javascript: return verifyEmail(event);'";
$txtemail->Length = 100;

$txtwebadd = new TextBox("txtwebadd", "txtwebadd", "Website Address");
$txtwebadd->Text = $website;
$txtwebadd->Args = "autocomplete='off' onkeypress='javascript: return DisableSpacesOnly(event);'";
$txtwebadd->Length = 100;

$txtcitizen = new TextBox("txtcitizen", "txtcitizen", "Citizen");
$txtcitizen->Text = $citizen;
$txtcitizen->Args = "autocomplete='off' onkeypress='javascript: return AlphaOnly(event);'";
$txtcitizen->Length = 50;

$txtbday = new TextBox("txtbday", "txtbday", "Birthday");
$txtbday->Text = $bday;
$txtbday->Args = "autocomplete='off' readonly='readonly' onkeypress='javascript: return DisableSpacesOnly(event);'";


$txtpassport = new TextBox("txtpassport", "txtpassport", "Passport");
$txtpassport->Text = $pass;
$txtpassport->Args = "autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtpassport->Length = 50;
$txtpassport->Enabled = false;

$txtdriver = new TextBox("txtdriver", "txtdriver", "Driver");
$txtdriver->Text = $d_lic;
$txtdriver->Args = "autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtdriver->Length = 50;
$txtdriver->Enabled = false;

$txtsss = new TextBox("txtsss", "txtsss", "SSS");
$txtsss->Text = $sss;
$txtsss->Args = "autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtsss->Length = 50;
$txtsss->Enabled = false;

$txtothers = new TextBox("txtothers", "txtothers", "Others");
$txtothers->Text = $others;
$txtothers->Args = "autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtothers->Length = 100;
$txtothers->Enabled = false;

$txtbankbranch = new TextBox("txtbankbranch", "txtbankbranch", "Bank Branch");
$txtbankbranch->Text = $bank_bnch;
$txtbankbranch->Args = "autocomplete='off' onkeypress='javascript: return AlphaAndSpaces(event);'";
$txtbankbranch->Length = 100;

$txtbankacctname = new TextBox("txtbankacctname", "txtbankacctname", "Bank Account Name");
$txtbankacctname->Text = $bank_acct_nme;
$txtbankacctname->Args = "autocomplete='off' onkeypress='javascript: return AlphaAndSpaces(event);'";
$txtbankacctname->Length = 100;

$txtbankacctnum = new TextBox("txtbankacctnum", "txtbankacctnum", "Bank Account Number");
$txtbankacctnum->Text = $bank_acct_num;
$txtbankacctnum->Args = "autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtbankacctnum->Length = 50;

$txtpayeebankbranch = new TextBox("txtpayeebankbranch", "txtpayeebankbranch", "Payee Bank Branch");
$txtpayeebankbranch->Text = $payee_bank_branch;
$txtpayeebankbranch->Args = "autocomplete='off' onkeypress='javascript: return AlphaAndSpaces(event);'";
$txtpayeebankbranch->Length = 100;

$txtpayeebankacctname = new TextBox("txtpayeebankacctname", "txtpayeebankacctname", "Payee Bank Account Name");
$txtpayeebankacctname->Text = $payee_bank_acct_nme;
$txtpayeebankacctname->Args = "autocomplete='off' onkeypress='javascript: return AlphaAndSpaces(event);'";
$txtpayeebankacctname->Length = 100;

$txtpayeebankacctnum = new TextBox("txtpayeebankacctnum", "txtpayeebankacctnum", "Payee Bank Account Number");
$txtpayeebankacctnum->Text = $payee_bank_acct_num;
$txtpayeebankacctnum->Args = "autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";
$txtpayeebankacctnum->Length = 50;

$ddlcountry = new ComboBox("ddlcountry", "ddlcountry", "Country");
$opt = null;
$opt[] = new ListItem("Select One", "");
$opt[] = new ListItem("Guam", "1");
$ddlcountry->Items = $opt;
$ddlcountry->SetSelectedValue($country);

$ddlbank = new ComboBox("ddlbank", "ddlbank", "Bank");
$opt1 = null;
$opt1[] = new ListItem($bank, $bank, true);
$opt1[] = new ListItem("Banco Filipino", "Banco Filipino");
$ddlbank->Items = $opt1;

$ddlbanktype = new ComboBox("ddlbanktype", "ddlbanktype", "Bank Type");
$opt2 = null;
$opt2[] = new ListItem($bank_type, $bank_type, true);
$opt2[] = new ListItem("", "");
$ddlbanktype->Items = $opt2;

$ddlpayeebank = new ComboBox("ddlpayeebank", "ddlpayeebank", "Payee Bank");
$opt3 = null;
$opt3[] = new ListItem($payee_bank, $payee_bank, true);
$opt3[] = new ListItem("Banco Filipino", "Banco Filipino");
$ddlpayeebank->Items = $opt3;

$ddlpayeebanktype = new ComboBox("ddlpayeebanktype", "ddlpayeebanktype", "Payee Bank Type");
$opt4 = null;
$opt4[] = new ListItem($payee_bank_type, $payee_bank_type, true);
$opt4[] = new ListItem("", "");
$ddlpayeebanktype->Items = $opt4;

$chkpass = new CheckBox("chkpass", "chkpass", "Passport");
$chkpass->Args = "onchange='javascript: return forpassport();'";

$chkdriver = new CheckBox("chkdriver", "chkdriver", "Driver");
$chkdriver->Args = "onchange='javascript: return fordriver();'";

$chksss = new CheckBox("chksss", "chksss", "SSS");
$chksss->Args = "onchange='javascript: return forsss();'";

$chkothers = new CheckBox("chkothers", "chkothers", "Others");
$chkothers->Args = "onchange='javascript: return forothers();'";

$fproc->AddControl($ddlacctname);
$fproc->AddControl($ddlacctno);
$fproc->AddControl($btnSearch);
$fproc->AddControl($btnBack);
$fproc->AddControl($hiddenid);
$fproc->AddControl($hidden);
// --
$fproc->AddControl($txtuname);
$fproc->AddControl($txtfname);
$fproc->AddControl($txtmname);
$fproc->AddControl($txtlname);
$fproc->AddControl($txtscode);
$fproc->AddControl($txthadd);
$fproc->AddControl($txtzip);
$fproc->AddControl($txttelno);
$fproc->AddControl($txtmobno);
$fproc->AddControl($txtfax);
$fproc->AddControl($txtemail);
$fproc->AddControl($txtwebadd);
$fproc->AddControl($ddlcountry);
$fproc->AddControl($txtcitizen);
$fproc->AddControl($txtbday);
$fproc->AddControl($chkpass);
$fproc->AddControl($txtpassport);
$fproc->AddControl($chkdriver);
$fproc->AddControl($txtdriver);
$fproc->AddControl($chksss);
$fproc->AddControl($txtsss);
$fproc->AddControl($chkothers);
$fproc->AddControl($txtothers);
$fproc->AddControl($ddlbank);
$fproc->AddControl($ddlbanktype);
$fproc->AddControl($txtbankbranch);
$fproc->AddControl($txtbankacctname);
$fproc->AddControl($txtbankacctnum);
$fproc->AddControl($ddlpayeebank);
$fproc->AddControl($ddlpayeebanktype);
$fproc->AddControl($txtpayeebankbranch);
$fproc->AddControl($txtpayeebankacctname);
$fproc->AddControl($txtpayeebankacctnum);
$fproc->ProcessForms();

$errorID = true;
if (strlen($txtpassport->Text) > 0)
{
    $errorID = false;
    $txtpassport->Enabled = true;
    $chkpass->Checked = true;
}
if (strlen($txtdriver->Text) > 0)
{
    $errorID = false;
    $txtdriver->Enabled = true;
    $chkdriver->Checked = true;
}
if (strlen($txtsss->Text) > 0)
{
    $errorID = false;
    $txtsss->Enabled = true;
    $chksss->Checked = true;
}
if (strlen($txtothers->Text) > 0)
{
    $errorID = false;
    $txtothers->Enabled = true;
    $chkothers->Checked = true;
}

if ($fproc->IsPostBack)
{
    $errorID = true;
    if (strlen($txtpassport->SubmittedValue) > 0)
    {
        $errorID = false;
        $txtpassport->Enabled = true;
        $chkpass->Checked = true;
    }
    if (strlen($txtdriver->SubmittedValue) > 0)
    {
        $errorID = false;
        $txtdriver->Enabled = true;
        $chkdriver->Checked = true;
    }
    if (strlen($txtsss->SubmittedValue) > 0)
    {
        $errorID = false;
        $txtsss->Enabled = true;
        $chksss->Checked = true;
    }
    if (strlen($txtothers->SubmittedValue) > 0)
    {
        $errorID = false;
        $txtothers->Enabled = true;
        $chkothers->Checked = true;
    }    
    if ($hidden->SubmittedValue != '')
    {
        if ($errorID != true)
        {
            $emailexist = $caccts->CheckEmailExist($txtemail->SubmittedValue);
            if ($emailexist[0][ID] == $acct_id || count($emailexist) == 0)
            {
                $cacctdtls->StartTransaction();
                // Update Account Details
                $updacctdtls["AccountID"] = $hiddenid->SubmittedValue;
                $updacctdtls["SWCCode"] = $txtscode->SubmittedValue;
                $updacctdtls["HomeAddress"] = $txthadd->SubmittedValue;
                $updacctdtls["ZipCode"] = $txtzip->SubmittedValue;
                $updacctdtls["TelephoneNumber"] = $txttelno->SubmittedValue;
                $updacctdtls["MobileNumber"] = $txtmobno->SubmittedValue;
                $updacctdtls["Fax"] = $txtfax->SubmittedValue;
                $updacctdtls["Email"] = $txtemail->SubmittedValue;
                $updacctdtls["Website"] = $txtwebadd->SubmittedValue;
                $updacctdtls["CountryID"] = $ddlcountry->SubmittedValue;
                $updacctdtls["Citizenship"] = $txtcitizen->SubmittedValue;
                $updacctdtls["BirthDate"] = $txtbday->SubmittedValue;
                $updacctdtls["Passport"] = $txtpassport->SubmittedValue;
                $updacctdtls["DriversLicense"] = $txtdriver->SubmittedValue;
                $updacctdtls["SocialSecurity"] = $txtsss->SubmittedValue;
                $updacctdtls["Others"] = $txtothers->SubmittedValue;
                $updacctdtls["DateUpdated"] = 'now_usec()';
                $cacctdtls->UpdateByArray($updacctdtls);


                $posid = $caccts->GetPOSID($acct_id);
                $caccts->StartTransaction();
                $updacct["ID"] = $posid[0][ID];
                $updacct["Email"] = $txtemail->SubmittedValue;
                $caccts->UpdateByArray($updacct);
                if ($caccts->HasError)
                {
                    $errormsg = $caccts->getErrors();
                    $errormsgtitle = "POS Account Update";
                    $caccts->RollBackTransaction();
                }
                else
                {

                    if ($cacctdtls->HasError)
                    {
                        $errormsg = $cacctdtls->getErrors();
                        $errormsgtitle = "POS Account Update";
                        $cacctdtls->RollBackTransaction();
                    }
                    else
                    {
                        $caccts->CommitTransaction();
                        $cacctdtls->CommitTransaction();

                        $cacctbankdtls->StartTransaction();
                        // Update Bank Details
                        $updacctbankdtls["AccountID"] = $hiddenid->SubmittedValue;
                        $updacctbankdtls["Bank"] = $ddlbank->SubmittedValue;
                        $updacctbankdtls["BankType"] = $ddlbanktype->SubmittedValue;
                        $updacctbankdtls["BankBranch"] = $txtbankbranch->SubmittedValue;
                        $updacctbankdtls["BankAccountName"] = $txtbankacctname->SubmittedValue;
                        $updacctbankdtls["BankAccountNumber"] = $txtbankacctnum->SubmittedValue;
                        $updacctbankdtls["PayeeBank"] = $ddlpayeebank->SubmittedValue;
                        $updacctbankdtls["PayeeBankType"] = $ddlpayeebanktype->SubmittedValue;
                        $updacctbankdtls["PayeeBankBranch"] = $txtpayeebankbranch->SubmittedValue;
                        $updacctbankdtls["PayeeBankAccountName"] = $txtpayeebankacctname->SubmittedValue;
                        $updacctbankdtls["PayeeBankAccountNumber"] = $txtpayeebankacctnum->SubmittedValue;
                        $updacctbankdtls["DateUpdated"] = 'now_usec()';
                        $cacctbankdtls->UpdateByArray($updacctbankdtls);
                        if ($cacctbankdtls->HasError)
                        {
                            $errormsg = $cacctbankdtls->getErrors();
                            $errormsgtitle = "POS Account Update";
                            $cacctbankdtls->RollBackTransaction();
                        }
                        else
                        {
                            $cacctbankdtls->CommitTransaction();
                            // Log to Audit Trail
                            $caudittrail->StartTransaction();
                            $scauditlogparam["SessionID"] = $_SESSION['sid'];
                            $scauditlogparam["AccountID"] = $_SESSION['aid'];
                            $scauditlogparam["TransDetails"] = "Edit POS Account: " . $txtuname->SubmittedValue;
                            $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                            $scauditlogparam["TransDateTime"] = "now_usec()";
                            $caudittrail->Insert($scauditlogparam);
                            if ($caudittrail->HasError)
                            {
                                $errormsg = $caudittrail->getError();
                                $errormsgtitle = "ERROR!";
                            }
                            else
                            {
                                $caudittrail->CommitTransaction();
                            }
                            /* -- LOAD POS ACCOUNT RECORDS ------------------------------------------------- */
                            $_SESSION['pos_acctid'] = $_POST['hiddenid'];
                            $arrposdtls = $cacctdtls->GetPOSAccountDetails($_SESSION['pos_acctid']);
                            $list_posdtls = new ArrayList();
                            $list_posdtls->AddArray($arrposdtls);

                            if (count($list_posdtls) > 0)
                            {
                                $acct_name = $list_posdtls[0]["LastName"] . ", " . $list_posdtls[0]["FirstName"] . " " . $list_posdtls[0]["MiddleName"];
                                $acct_id = $list_posdtls[0]["AccountID"];
                                $uname = $list_posdtls[0]["Username"];
                                $lname = $list_posdtls[0]["LastName"];
                                $fname = $list_posdtls[0]["FirstName"];
                                $mname = $list_posdtls[0]["MiddleName"];
                                $swc_code = $list_posdtls[0]["SWCCode"];
                                $home_addr = $list_posdtls[0]["HomeAddress"];
                                $zip = $list_posdtls[0]["ZipCode"];
                                $tel_no = $list_posdtls[0]["TelephoneNumber"];
                                $mob_no = $list_posdtls[0]["MobileNumber"];
                                $fax = $list_posdtls[0]["Fax"];
                                $email = $list_posdtls[0]["Email"];
                                $website = $list_posdtls[0]["Website"];
                                $country = $list_posdtls[0]["CountryID"];
                                $citizen = $list_posdtls[0]["Citizenship"];
                                $bday = $list_posdtls[0]["BirthDate"];
                                $pass = $list_posdtls[0]["Passport"];
                                $d_lic = $list_posdtls[0]["DriversLicense"];
                                $sss = $list_posdtls[0]["SocialSecurity"];
                                $others = $list_posdtls[0]["Others"];
                                $bank = $list_posdtls[0]["Bank"];
                                $bank_type = $list_posdtls[0]["BankType"];
                                $bank_bnch = $list_posdtls[0]["BankBranch"];
                                $bank_acct_nme = $list_posdtls[0]["BankAccountName"];
                                $bank_acct_num = $list_posdtls[0]["BankAccountNumber"];
                                $payee_bank = $list_posdtls[0]["PayeeBank"];
                                $payee_bank_type = $list_posdtls[0]["PayeeBankType"];
                                $payee_bank_branch = $list_posdtls[0]["PayeeBankBranch"];
                                $payee_bank_acct_nme = $list_posdtls[0]["PayeeBankAccountName"];
                                $payee_bank_acct_num = $list_posdtls[0]["PayeeBankAccountNumber"];
                                $status = $list_posdtls[0]['Stat'];
                                $siteID = $list_posdtls[0]['SI'];
                                $agentuname = $list_posdtls[0]["AgentUserName"];
                                $txthadd->Text = $home_addr;
                                $txtzip->Text = $zip;
                                $txttelno->Text = $tel_no;
                                $txtmobno->Text = $mob_no;
                                $txtfax->Text = $fax;
                                $txtemail->Text = $email;
                                $txtwebadd->Text = $website;
                                $txtcitizen->Text = $citizen;
                                $txtbday->Text = $bday;
                                $txtpassport->Text = $pass;
                                $txtdriver->Text = $d_lic;
                                $txtsss->Text = $sss;
                                $txtothers->Text = $others;
                                $txtbankbranch->Text = $bank_bnch;
                                $txtbankacctname->Text = $bank_acct_nme;
                                $txtbankacctnum->Text = $bank_acct_num;
                                $txtpayeebankbranch->Text = $payee_bank_branch;
                                $txtpayeebankacctname->Text = $payee_bank_acct_nme;
                                $txtpayeebankacctnum->Text = $payee_bank_acct_num;
                                if (strlen($txtpassport->Text) > 0)
                                {
                                    $txtpassport->Enabled = true;
                                    $chkpass->Checked = true;
                                }
                                else
                                {
                                    $txtpassport->Enabled = false;
                                    $chkpass->Checked = false;
                                }
                                if (strlen($txtdriver->Text) > 0)
                                {
                                    $txtdriver->Enabled = true;
                                    $chkdriver->Checked = true;
                                }
                                else
                                {
                                    $txtdriver->Enabled = false;
                                    $chkdriver->Checked = false;
                                }
                                if (strlen($txtsss->Text) > 0)
                                {
                                    $txtsss->Enabled = true;
                                    $chksss->Checked = true;
                                }
                                else
                                {
                                    $txtsss->Enabled = false;
                                    $chksss->Checked = false;
                                }
                                if (strlen($txtothers->Text) > 0)
                                {
                                    $txtothers->Enabled = true;
                                    $chkothers->Checked = true;
                                }
                                else
                                {
                                    $txtothers->Enabled = false;
                                    $chkothers->Checked = false;
                                }
                            }
                            unset($_SESSION['pos_acctid']);
                            $successmsg = "The POS account status has been updated.";
                            $successmsgtitle = "POS Account Update";
                        }
                    }
                }
            }
            else
            {
                $emailerrormsg = 'Email Address has already been used';
                $emailerrortitle = "ERROR";
            }
        }
        else
        {
                $emailerrormsg = 'Please fill in all required fields';
                $emailerrortitle = "Notification";
        }
    }
}
?>