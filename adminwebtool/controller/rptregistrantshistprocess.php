<?php

/*
 * Added On : June 25, 2012
 * Created by : JFJ
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_FreeEntryRegistrants");

APP::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("PagingControl2");

$fproc = new FormsProcessor();
$accounts = new SCC_Accounts();
$freeregistrants = new SCC_FreeEntryRegistrants();

$txtDateFr = new TextBox("txtDateFr", "txtDateFr");
$txtDateFr->Length = "10";
$txtDateFr->Style = "text-align:center";
$txtDateFr->ReadOnly = true;
$txtDateFr->Text = date("m/d/Y");

$txtDateTo = new TextBox("txtDateTo", "txtDateTo");
$txtDateTo->Length = "10";
$txtDateTo->Style = "text-align:center";
$txtDateTo->ReadOnly = true;
$txtDateTo->Text = date("m/d/Y");

$btnSearch = new Button("btnSearch", "btnSearch", "Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checkrptinput();' ";


$getlist = $accounts->SelectSiteIDbylist();
$ddlPos = new ComboBox("ddlPos", "ddlPos");
$Poslist = new ArrayList();
$Poslist->AddArray($getlist);
$ddlPos->ClearItems();
$litem = null;
$litem[] = new ListItem("---", "", true);
$litem[] = new ListItem("ALL", "0");
$ddlPos->Items = $litem;
$ddlPos->DataSource = $Poslist;
$ddlPos->DataSourceText = "Name";
$ddlPos->DataSourceValue = "SiteID";
$ddlPos->DataBind();

$itemsperpage = 20;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage)";
$pgcon->PageGroup = 5;
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;



$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSearch);
$fproc->AddControl($ddlPos);


$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    $daysinterval = (strtotime($txtDateTo->SubmittedValue) - strtotime($txtDateFr->SubmittedValue)) / 86400;
    if ($daysinterval <= 90)
    {
        if ($btnSearch->SubmittedValue == "Search")
        {
            $pgcon->SelectedPage = 1;
        }
        $dDateFr = $txtDateFr->SubmittedValue;
        $dDateTo = $txtDateTo->SubmittedValue;
        $sPosAcct = $ddlPos->SubmittedValue;
        $_SESSION['start'] = $dDateFr;
        $_SESSION['end'] = $dDateTo;
        $_SESSION['posacct'] = $sPosAcct;


        $ds = new DateSelector($dDateTo);
        $date2 = $ds->NextDate;
        $dDateTo = $date2;
        $dDateTo = date_create($dDateTo);
        $dDateTo = date_format($dDateTo, "Y-m-d 10:00:00.00000");
        $dDateFr = date_create($dDateFr);
        $dDateFr = date_format($dDateFr, "Y-m-d 09:59:59.99999");

        $getrptregistrants1 = $freeregistrants->getrptregistrants($sPosAcct, $dDateFr, $dDateTo);
        $pgcon->Initialize($itemsperpage, count($getrptregistrants1));
        $pgHist = $pgcon->PreRender();
        $wherelimit = " LIMIT " . ($pgcon->SelectedItemFrom - 1) . "," . $itemsperpage . "  ";
        $getrptregistrants = $freeregistrants->getrptregistrantswithLimit($sPosAcct, $dDateFr, $dDateTo, $wherelimit);
    }
    else
    {
        $errormsgtitle = "INVALID INPUT";
        $errormsg = "Please limit date range selection to ninety (90) days or three (3) months only.";
    }
}
?>
