<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 18, 2012
 * Purpose          :       POS Account Profile Controller: Viewing of POS Account
 */

require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_AccountDetails");

App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$fproc = new FormsProcessor();
$cacctdtls = new SCC_AccountDetails();

$ddlacctname = new ComboBox("ddlacctname", "ddlacctname", "Account Name");
$ddlacctname->Args = "onchange='javascript: return SelectPOSAccountNumber()'";
$opt1 = null;
$opt1[] = new ListItem("Select One", "Select One", true);
$ddlacctname->Items = $opt1;
$posaccts = $cacctdtls->LoadPOSAccounts("Select One");
$list_posaccts = new ArrayList();
$list_posaccts->AddArray($posaccts);
$ddlacctname->DataSource = $list_posaccts;
$ddlacctname->DataSourceValue = "AccountID";
$ddlacctname->DataSourceText = "FullName";
$ddlacctname->DataBind();

$ddlacctno = new ComboBox("ddlacctno", "ddlacctno", "Account Number");
$ddlacctno->Args = "onchange='javascript: return SelectPOSAccountName()'";
$opt2 = null;
$opt2[] = new ListItem("Select One", "Select One", true);
$ddlacctno->Items = $opt2;
$posacctsid = $cacctdtls->LoadPOSAccountID();
$list_posacctsid = new ArrayList();
$list_posacctsid->AddArray($posacctsid);
$ddlacctno->DataSource = $list_posacctsid;
$ddlacctno->DataSourceValue = "AccountID";
$ddlacctno->DataSourceText = "AccountID";
$ddlacctno->DataBind();

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Search");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->IsSubmit = true;

$btnEdit = new Button("btnEdit", "btnEdit", "Edit");
$btnEdit->CssClass = "labelbutton2 edit";
$btnEdit->IsSubmit = true;

$hiddenid = new Hidden("hiddenid", "hiddenid", "Hidden Account ID");

$fproc->AddControl($ddlacctname);
$fproc->AddControl($ddlacctno);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnEdit);
$fproc->AddControl($hiddenid);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSubmit->SubmittedValue == "Search")
    {
        $arrposdtls = $cacctdtls->GetPOSAccountDetails($ddlacctname->SubmittedValue);
        $array = get_details($arrposdtls);
//        app::pr($array);
//        exit;
    }
}

if (isset($_POST['hiddenid']))
{
    $arrposdtls = $cacctdtls->GetPOSAccountDetails($_POST['hiddenid']);
    $array = get_details($arrposdtls);
}

function get_details($list_posdtls)
{
    App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
    $cterminals = new SCC_Terminals();
    
    if (count($list_posdtls) > 0)
        {
            $acct_name = $list_posdtls[0]["LastName"] . ", " . $list_posdtls[0]["FirstName"] . " " . $list_posdtls[0]["MiddleName"];
            $acct_id = $list_posdtls[0]["AccountID"];
            $uname = $list_posdtls[0]["Username"];
            $lname = $list_posdtls[0]["LastName"];
            $fname = $list_posdtls[0]["FirstName"];
            $mname = $list_posdtls[0]["MiddleName"];
            $swc_code = $list_posdtls[0]["SWCCode"];
            $home_addr = $list_posdtls[0]["HomeAddress"];
            $zip = $list_posdtls[0]["ZipCode"];
            $tel_no = $list_posdtls[0]["TelephoneNumber"];
            $mob_no = $list_posdtls[0]["MobileNumber"];
            $fax = $list_posdtls[0]["Fax"];
            $email = $list_posdtls[0]["Email"];
            $website = $list_posdtls[0]["Website"];
            $country = $list_posdtls[0]["CountryID"];
            $citizen = $list_posdtls[0]["Citizenship"];
            $bday = $list_posdtls[0]["BirthDate"];
            $pass = $list_posdtls[0]["Passport"];
            $d_lic = $list_posdtls[0]["DriversLicense"];
            $sss = $list_posdtls[0]["SocialSecurity"];
            $others = $list_posdtls[0]["Others"];
            $bank = $list_posdtls[0]["Bank"];
            $bank_type = $list_posdtls[0]["BankType"];
            $bank_bnch = $list_posdtls[0]["BankBranch"];
            $bank_acct_nme = $list_posdtls[0]["BankAccountName"];
            $bank_acct_num = $list_posdtls[0]["BankAccountNumber"];
            $payee_bank = $list_posdtls[0]["PayeeBank"];
            $payee_bank_type = $list_posdtls[0]["PayeeBankType"];
            $payee_bank_branch = $list_posdtls[0]["PayeeBankBranch"];
            $payee_bank_acct_nme = $list_posdtls[0]["PayeeBankAccountName"];
            $payee_bank_acct_num = $list_posdtls[0]["PayeeBankAccountNumber"];
            $status = $list_posdtls[0]['Stat'];
            $siteID = $list_posdtls[0]['SI'];
            $ctr_terminal = $cterminals->GetTerminaCount($siteID);
            $terminal = $ctr_terminal[0][0];
            $ctr_feterminal = $cterminals->GetFETerminalCount($siteID);
            $feterminal = $ctr_feterminal[0][0];
            $agentuname = $list_posdtls[0]["AgentUserName"];
        }
        else
        {
            $acct_name = "";$acct_id = "";$uname = "";$lname = "";$fname = "";$mname = "";$swc_code = "";
            $home_addr = "";$zip = "";$tel_no = "";$mob_no = "";$fax = "";$email = "";$website = "";$country = "";
            $citizen = "";$bday = "";$pass = "";$d_lic = "";$sss = "";$others = "";$bank = "";$bank_type = "";
            $bank_bnch = "";$bank_acct_nme = "";$bank_acct_num = "";$payee_bank = "";$payee_bank_type = "";
            $payee_bank_branch = "";$payee_bank_acct_nme = "";$payee_bank_acct_num = "";$status = "";$siteID = "";
        }
        
        return array($acct_name,$acct_id,$uname,$lname,$fname,$mname,$swc_code,$home_addr,$zip,$tel_no,
                     $mob_no,$fax,$email,$website,$country,$citizen,$bday,$pass,$d_lic,$sss,$others,$bank,
                     $bank_type,$bank_bnch,$bank_acct_nme,$bank_acct_num,$payee_bank,$payee_bank_type,
                     $payee_bank_branch,$payee_bank_acct_nme,$payee_bank_acct_num,$status,$siteID,$terminal,$feterminal,$agentuname);
}
?>