<?php
/*
 * Created By       :       Angelo Cubos
 * Date Created     :       Dec 6, 2012
 * Purpose          :       Reset Terminal Balance
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_AccountDetails");
App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_Sites");
App::LoadModuleClass($modulename, "SCC_TopUpLogs");
App::LoadModuleClass($modulename, "SCC_AccountUpdateLog");
App::LoadModuleClass($modulename, "SCC_AuditTrail");
App::LoadModuleClass($modulename, "SCC_Terminals");

App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$fproc = new FormsProcessor();
$cacctdtls = new SCC_AccountDetails();
$caccts = new SCC_Accounts();
$caudittrail =  new SCC_AuditTrail();
$cterminals=  new SCC_Terminals();

$ddlacctname = new ComboBox("ddlacctname", "ddlacctname", "Account Name");
$ddlacctname->Args = "onchange='javascript: return SelectPOSAccountNumber()'";
$opt1 = null;
$opt1[] = new ListItem("Select One", "Select One", true);
$ddlacctname->Items = $opt1;
$posaccts = $cacctdtls->LoadPOSAccounts("Select One");
$list_posaccts = new ArrayList();
$list_posaccts->AddArray($posaccts);
$ddlacctname->DataSource = $list_posaccts;
$ddlacctname->DataSourceValue = "AccountID";
$ddlacctname->DataSourceText = "FullName";
$ddlacctname->DataBind();

$ddlacctno = new ComboBox("ddlacctno", "ddlacctno", "Account Number");
$ddlacctno->Args = "onchange='javascript: return SelectPOSAccountName()'";
$opt2 = null;
$opt2[] = new ListItem("Select One", "Select One", true);
$ddlacctno->Items = $opt2;
$posacctsid = $cacctdtls->LoadPOSAccountID();
$list_posacctsid = new ArrayList();
$list_posacctsid->AddArray($posacctsid);
$ddlacctno->DataSource = $list_posacctsid;
$ddlacctno->DataSourceValue = "AccountID";
$ddlacctno->DataSourceText = "AccountID";
$ddlacctno->DataBind();

$ddltacct = new ComboBox("tacct", "tacct", "Terminal Account");
$ddltacct->Enabled = false;
//$opt3 = null;
//$opt3[] = new ListItem("Select One", "Select One", true);
//$ddltacct->Items = $opt3;

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Search");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->IsSubmit = true;

$btnUpdate = new Button("btnUpdate", "btnUpdate", "UPDATE");
$btnUpdate->Style = "width: 170px;";
$btnUpdate->CssClass = "labelbutton2";

$hidfname = new Hidden("fname", "fname", "POS Account Name");
$hidtname = new Hidden("tname", "tname", "POS Terminal Name");
$hidans = new Hidden("hidans", "hidans", "hidans");

$fproc->AddControl($ddlacctname);
$fproc->AddControl($ddlacctno);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($ddltacct);
$fproc->AddControl($btnUpdate);
$fproc->AddControl($hidfname);
$fproc->AddControl($hidtname);
$fproc->AddControl($hidans);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($_POST[hidans] == "YES")
    {
        $name = $_POST[tname];
        $sID = $_POST[ddlacctname];

        
        $arrTerminals = $caccts->GetPOSBalance($name,$sID);
        $balance = $arrTerminals[0]["Balance"];
        $cterminals->StartTransaction();
        $arrTerminals = $cterminals->ResetPOSTerminalBalance($name,$sID);
        if ($cterminals->HasError)
        {
            $errormsg = $cterminals->getError();
            $errormsgtitle = "ERROR!";
            $boolrollback = true;
        }else{
            $cterminals->CommitTransaction();
        }
        // Log to Audit Trail
        $scauditlogparam["SessionID"] = $_SESSION['sid'];
        $scauditlogparam["AccountID"] = $_SESSION['aid'];
        $scauditlogparam["TransDetails"] = "Reset terminal balance: ".$_SESSION['uname']. "[".$name.":".$balance . "]" ;
        $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
        $scauditlogparam["TransDateTime"] = "now_usec()";
        $caudittrail->Insert($scauditlogparam);
        if ($caudittrail->HasError)
        {
            $errormsg = $caudittrail->getError();
            $errormsgtitle = "ERROR!";
            $boolrollback = true;
        }else{
            $caudittrail->CommitTransaction();
        }
        // Log to Audit Trail   
    }
   
    if ($btnSubmit->SubmittedValue == "Search" )
    {
        $ID = $_POST[tacct];
        $SiteID = $_POST[ddlacctname];
        
        $arrTerminals = $cterminals->LoadPOSTerminalBalance($ID,$SiteID);
        for ($i = 0; $i < count($arrTerminals); $i++)
        {
            $option[] = new ListItem($arrTerminals[$i]["Name"],$arrTerminals[$i]["ID"]);
        }
        $acctname = $hidfname->SubmittedValue;
        $acctid = $ddlacctno->SubmittedValue;
        $terminal_name = $hidtname->SubmittedValue;
        $terminalbalance =$arrTerminals[0]["Balance"];    
    }
  
        $arrTerminals = $caccts->POSTerminalNotFreeEntry($SiteID);
        $data = array();
        
        $list_terminal = new ArrayList();
        $list_terminal->AddArray($arrTerminals);
        $ddltacct->DataSource = $list_terminal;
        $ddltacct->DataSourceValue = "ID";
        $ddltacct->DataSourceText = "Name";
        $ddltacct->DataBind();

}
?>
