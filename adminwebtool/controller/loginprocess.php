<?php

/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 13, 2012
 * Purpose          :       Login Page Controller: Entry point of Guam Webtool.
 */
require_once("../init.inc.php");

session_regenerate_id();

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_AdminAccounts");
App::LoadModuleClass($modulename, "SCC_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCC_AuditTrail");
App::LoadModuleClass("SweepsCenter", "SCC_ref_Services");

App::LoadCore("PHPMailer.class.php");
App::LoadControl("TextBox");
App::LoadControl("Button");

$fproc = new FormsProcessor();
$cadminaccts = new SCC_AdminAccounts();
$cadminacctsessions = new SCC_AdminAccountSessions();
$caudittrail = new SCC_AuditTrail();
$refservices = new SCC_ref_Services();

$selectactive = $refservices->SelectActiveService();
$activeprovider = $selectactive[0]['ID'];


$txtUsername = new TextBox("txtUsername", "txtUsername", "Username: ");
$txtUsername->Length = 20;
$txtUsername->Args = "autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtPassword = new TextBox("txtPassword", "txtPassword", "Password: ");
$txtPassword->Password = true;
$txtPassword->Length = 20;
$txtPassword->Args = "autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtEmail = new TextBox("txtEmail", "txtEmail", "Email");
$txtEmail->Style = "width: 300px";
//$txtEmail->Args="onkeypress='javascript: return specialkeys(event);'";
$txtEmail->Args = "onkeypress='javascript: return verifyEmail(event);'";

$btnSubmit = new Button("btnSubmit", "btnSubmit", "LOG-IN");
$btnSubmit->CssClass = "labelbold2";
$btnSubmit->Args = "onclick='javascript: return checkLogin_forloginphp();'";
$btnSubmit->IsSubmit = true;

$btnReset = new Button("btnReset", "btnReset", "RESET");
$btnReset->Args = "onclick='javascript: return redirect();'";
$btnReset->CssClass = "labelbold2";
$btnReset->IsSubmit = true;

$fproc->AddControl($txtUsername);
$fproc->AddControl($txtPassword);
$fproc->AddControl($txtEmail);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnReset);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSubmit->SubmittedValue == "LOG-IN")
    {
        $username = mysql_escape_string(trim($txtUsername->Text));
        $password = mysql_escape_string(trim($txtPassword->Text));

        // check if account exist
        $arrAccts = $cadminaccts->CheckAccount($username, $password);
        if (count($arrAccts) == 1)
        {
            $arrDtls = $arrAccts[0];
            $aid = $arrDtls["AccountID"];
            $accttype = $arrDtls["AccountType"];
            $sessionpassword = $arrDtls["Password"];

            $_SESSION['sid'] = session_id();
            $_SESSION['aid'] = $aid;
            $_SESSION['accttype'] = $accttype;
            $_SESSION['password'] = $sessionpassword;
            $_SESSION['uname'] = $username;
            $_SESSION['acctid'] = $arrDtls['AccountID'];
            $_SESSION['actualpassword'] = $password;
            $_SESSION['activeprovider'] = $activeprovider;
            $boolrollback = false;

            // Check for Existing Session
            $cadminacctsessions->StartTransaction();
            $caudittrail->StartTransaction();
            $arrAcctSessions = $cadminacctsessions->IfExists($aid);
            if (count($arrAcctSessions) > 0)
            {
                // End Current Active Session
                $scupdateparam["DateEnded"] = "now_usec()";
                $scupdateparam["AccountID"] = $aid;
                $cadminacctsessions->UpdateByArray($scupdateparam);
                if ($cadminacctsessions->HasError)
                {
                    $errormsg = $cadminacctsessions->getError();
                    $errormsgtitle = "ERROR!";
                    $boolrollback = true;
                }
            }

            // Insert into Account Session
            $scacctsessionsparam["SessionID"] = $_SESSION['sid'];
            $scacctsessionsparam["AccountID"] = $_SESSION['aid'];
            $scacctsessionsparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $scacctsessionsparam["TransDate"] = 'now_usec()';
            $scacctsessionsparam["RecCreOn"] = 'now_usec()';
            $scacctsessionsparam["RecCreBy"] = $username;
            $scacctsessionsparam["DateEnded"] = "0";
            $cadminacctsessions->Insert($scacctsessionsparam);
            if ($cadminacctsessions->HasError)
            {
                $errormsg = $cadminacctsessions->getError();
                $errormsgtitle = "ERROR!";
                $boolrollback = true;
            }

            // Log to Audit Trail
            $scauditlogparam["SessionID"] = $_SESSION['sid'];
            $scauditlogparam["AccountID"] = $_SESSION['aid'];
            $scauditlogparam["TransDetails"] = "Login: " . $username;
            $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $scauditlogparam["TransDateTime"] = "now_usec()";
            $caudittrail->Insert($scauditlogparam);
            if ($caudittrail->HasError)
            {
                $errormsg = $caudittrail->getError();
                $errormsgtitle = "ERROR!";
                $boolrollback = true;
            }

            if ($boolrollback)
            {
                $cadminacctsessions->RollBackTransaction();
                $caudittrail->RollBackTransaction();
            }
            else
            {
                $cadminacctsessions->CommitTransaction();
                $caudittrail->CommitTransaction();
                URL::Redirect('home.php');
            }
        }
        else
        {
            $errormsg = "You have entered an invalid account information.";
            $errormsgtitle = "INVALID LOG-IN!";
        }
    }

    if ($btnReset->SubmittedValue == "RESET")
    {
        $emailaddr = $txtEmail->SubmittedValue;

        $cadminaccts->StartTransaction();
        $where = " WHERE Email = '$emailaddr' AND Status = '1'";
        $userdetails = $cadminaccts->SelectByWhere($where);
        $id = $userdetails[0]["ID"];
        $name = $userdetails[0]["Username"];
        $acctname = $userdetails[0]["FirstName"] . " " . $userdetails[0]["MiddleName"] . " " . $userdetails[0]["LastName"];
        $newpass = substr(session_id(), 0, 8);

        // Update password
        $updateAccount["ID"] = $id;
        $updateAccount["Password"] = MD5($newpass);
        $cadminaccts->UpdateByArray($updateAccount);
        if ($cadminaccts->HasError)
        {
            $errormsg = $cadminaccts->getErrors();
            $errormsgtitle = "ERROR!";
            $cadminaccts->RollBackTransaction();
        }
        $cadminaccts->CommitTransaction();

        // Sending of Email
        $pm = new PHPMailer();
        $pm->AddAddress($emailaddr, $name);

        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) == "on")
        {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        $folder = $_SERVER["REQUEST_URI"];
        $folder = substr($folder, 0, strrpos($folder, '/') + 1);
        if ($_SERVER["SERVER_PORT"] != "80")
        {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $folder;
        }
        else
        {
            $pageURL .= $_SERVER["SERVER_NAME"] . $folder;
        }

        $pm->IsHTML(true);

        $pm->Body = "Dear " . $acctname . "<br/><br/>
            This is to inform you that your password has been reset on this date " . date("m/d/Y") . " and time " . date("H:i:s") . ". Here are your new Account Details:<br/><br/>
            Username: <b>" . $name . "</b><br/>
            Password: <b>" . $newpass . "</b><br/><br/>" .
                "<b>For security purposes, please change this system-generated password upon log in at </b><a href=" . $pageURL . ">$pageURL</a><br/><br/>" .
                "If you didn't perform this procedure, please report this to our Customer Service email at support.gu@philwebasiapacific.com or support@philwebasiapacific.com." . "<br/><br/>
         Regards,<br/><br/>Customer Support<br/>The Sweeps Center";

        $pm->FromName = "support@thesweepscenter.com";
        $pm->Host = "localhost";
        $pm->Subject = "NOTIFICATION FOR NEW PASSWORD";
        $email_sent = $pm->Send();
        if ($email_sent)
        {
            $successmsg = "Your password has been sent to your e-mail account. For security purpose, please change this system-generated password upon log in.";
            $successmsgtitle = "Reset Password";
        }
        else
        {
            $errormsg = "An error occurred while sending the email to your email address";
            $errormsgtitle = "ERROR!";
        }
    }
}
?>
