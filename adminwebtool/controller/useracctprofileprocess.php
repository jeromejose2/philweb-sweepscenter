<?php

/*
 * Added By : Jerome F. Jose
 * Added On : June 14, 2012
 * Purpose : Adding User Account
 */
require_once("../init.inc.php");


App::LoadControl("TextBox");
APP::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("Label");
App::LoadControl("ComboBox");
$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_AdminAccounts");

$adminaccount = new SCC_AdminAccounts();

$fproc = new FormsProcessor();

$ddlUserName = new ComboBox("ddlUsername", "ddlUsername");
$ddlgroupusername = Null;
$ddlgroupusername[] = new ListItem("Select One", "0", true);
$ddlUserName->Items = $ddlgroupusername;
$ddlgroupusername = new ArrayList();
$getgroups = $adminaccount->getUsernameGroups();
$ddlgroupusername->AddArray($getgroups);
$ddlUserName->DataSource = $ddlgroupusername;
$ddlUserName->DataSourceText = "Username";
$ddlUserName->DataSourceValue = "AccountID";
$ddlUserName->DataBind();
$ddlUserName->Args = "onchange = 'javascript: onchange_username();'";


$btnSearch = new Button("btnSearch", "btnSearch", "SEARCH");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
//$btnSearch->Args = "onclick = 'javascript: return checkUsername('light6');' ";


$hidUsername = new Hidden("hidUsername", "hidUsername");
$hidAccountID = new Hidden("hidAccountID", "hidAccountID");

//$txtMName->Enabled = false;
$btnEdit = new Button("btnEdit", "btnEdit", "Edit");
$btnEdit->CssClass = "labelbutton2";

$txtFName = new TextBox("txtFName", "txtFName");
$txtFName->Style = "width:200px;text-align:center;";
$txtFName->Enabled = false;

$txtMName = new TextBox("txtMName", "txtMName");
$txtMName->Style = "width:200px;text-align:center;";
$txtMName->Enabled = false;

$txtLName = new TextBox("txtLName", "txtLName");
$txtLName->Style = "width:200px;text-align:center;";
$txtLName->Enabled = false;

$txtEmail = new TextBox("txtEmail", "txtEmail");
$txtEmail->Style = "width:250px;text-align:center;";
$txtEmail->Length = "50";
$txtEmail->Enabled = false;

$txtPosition = new TextBox("txtPosition", "txtPosition");
$txtPosition->Style = "width:200px;text-align:center;";
$txtPosition->Length = "50";
$txtPosition->Enabled = false;

$txtCompany = new TextBox("txtCompany", "txtCompany");
$txtCompany->Style = "width:200px;text-align:center;";
$txtCompany->Length = "50";
$txtCompany->Enabled = false;

$txtDepartment = new TextBox("txtDepartment", "txtDepartment");
$txtDepartment->Style = "width:200px;text-align:center;";
$txtDepartment->Length = "50";
$txtDepartment->Enabled = false;

$txtStatus = new TextBox("txtStatus", "txtStatus");
$txtStatus->Style = "width:200px;text-align:center;";
$txtStatus->Length = "50";
$txtStatus->Enabled = false;


$fproc->AddControl($txtFName);
$fproc->AddControl($txtMName);
$fproc->AddControl($txtLName);
$fproc->AddControl($txtEmail);
$fproc->AddControl($txtPosition);
$fproc->AddControl($txtCompany);
$fproc->AddControl($txtDepartment);
$fproc->AddControl($txtStatus);
$fproc->AddControl($ddlUserName);
$fproc->AddControl($btnSearch);
$fproc->AddControl($hidUsername);
$fproc->AddControl($btnEdit);
$fproc->AddControl($hidAccountID);


$usertype = $_SESSION['accttype'];

$fproc->ProcessForms();
if (isset($_SESSION['tempuname']) && ($_SESSION['tempuname']))
{
    unset($_SESSION['tempuname']);
    unset($_SESSION['UnameAcctID']);
}
if ($fproc->IsPostBack)
{

    $username = $_POST['hidUsername'];
    $_SESSION['tempuname'] = $username;
    $_SESSION['UnameAcctID'] = $_POST['hidAccountID'];

    if ($usertype == 1 || $usertype == 2)
    {

        $btnEdit->CssClass = "labelbutton2";
        //$btnEdit->IsSubmit = true;
        $btnEdit->Args = "onclick='javascript: return redirect();' ";
    }
    else
    {
        $btnEdit->CssClass = "labelbutton2";
        $btnEdit->Args = "style='cursor: default;'";
        $btnEdit->Style = "cursor: default;";
    }

    if ($btnSearch->SubmittedValue == "SEARCH")
    {


        $getall = $adminaccount->getallinfobyUsername($username);

        $acctid = str_pad($getall[0]["AcctID"], 10, "0", STR_PAD_LEFT);

        $uname = $getall[0]["Username"];
        $datecreated = $getall[0]["DateCreated"];
        $datetime = new DateTime($datecreated);
        $datecreated = $datetime->format("m/d/Y h:i:s A");
        $status = $getall[0]["AcctStatus"];
        if ($status == 1)
        {
            $txtStatus->Text = "ACTIVE";
            $status = "ACTIVE";
        }
        else if ($status == 2)
        {
            $txtStatus->Text = "INACTIVE";
            $status = "INACTIVE";
        }
        else if ($status == 3)
        {
            $txtStatus->Text = "SUSPENDED";
            $status = "SUSPENDED";
        }
        else if ($status == 4)
        {
            $txtStatus->Text = "TERMINATED";
            $status = "TERMINATED";
        }
        else
        {
            $txtStatus->Text = "";
            $status = "";
        }
        $txtFName->Text = $getall[0]["FirstName"];
        $txtMName->Text = $getall[0]["MiddleName"];
        $txtLName->Text = $getall[0]["LastName"];
        $txtEmail->Text = $getall[0]["Email"];
        $txtPosition->Text = $getall[0]["position"];
        $txtCompany->Text = $getall[0]["Company"];
        $txtDepartment->Text = $getall[0]["Department"];
        $groupname = $getall[0]["AccountTypeName"];
    }
}
?>
