<?php

/*
 * Added On : March 25, 2013
 * Created by : Frances Ralph DL. Sison
 * Reason: Checking to know if a deck is subject to/for discarding or not.
 */

require_once("../init.inc.php");
include("../config.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCCC_DeckInfo");

App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("Label");
App::LoadControl("Hidden");

$fproc = new FormsProcessor();
$deckinfo = new SCCC_DeckInfo();


$deckdis = "Deck is subject for discarding.";
$decknotdis = "Deck is not subject for discarding.";

$decklist = $deckinfo->GetQueuedDeckIDList();
$deckidbox = new ComboBox("deckidbox","deckidbox");
$arrlist = new ArrayList();
$arrlist->AddArray($decklist);
$deckidbox->ClearItems();
$list = null;
$list[] = new ListItem("Please Select","0",true);
$deckidbox->Items = $list;
$deckidbox->DataSource = $arrlist;
$deckidbox->DataSourceText = "DeckID";
$deckidbox->DataSourceValue = "DeckID";
$deckidbox->DataBind();

$btnSearch = new Button("btnSearch","btnSearch","SEARCH");
$btnSearch->Args = "onclick='javascript: return checkinput();'";
$btnSearch->CssClass = "labelbutton2";
$btnSearch->IsSubmit = true;

$recommendation = new Label("rec","rec");
       
$fproc->AddControl($deckidbox);
$fproc->AddControl($btnSearch);
$fproc->AddControl($recommendation);

$fproc->ProcessForms();

if($fproc->IsPostBack)
{
    $display = false;
    
    $_SESSION['mr_deckid'] = $deckidbox->SubmittedValue;
      
    $listQueuedDecks = $deckinfo->GetQueuedDeckECN($_SESSION['mr_deckid']);
    
    $deckidname = $listQueuedDecks[0]['DeckID'];
    $deckcontrolno = $listQueuedDecks[0]['ControlNo'];
    $deckcardcount = $listQueuedDecks[0]['CardCount'];
    $deckcardcountecn = $listQueuedDecks[0]['CardCountECN'];
    $deckdatecreated = $listQueuedDecks[0]['DateCreated'];
    $deckcreatedby = $listQueuedDecks[0]['Username'];
  
     
    if($btnSearch->SubmittedValue == "SEARCH")
    {  
        

        if($deckcardcount == $deckcardcountecn) {
            $recommendation = $decknotdis;
        }
        else {
            $recommendation = $deckdis;
        }
            $display = true;
            
    }
    
  
}
?>
