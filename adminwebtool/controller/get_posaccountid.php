<?php
/* 
 * Created by       :       Noel Antonio
 * Date Created     :       June 18, 2012
 * Purpose          :       Get the POS Account ID
 */
$acctname = $_POST["acctname"];

require_once('../init.inc.php');
$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_AccountDetails");

$cacctdtls = new SCC_AccountDetails();
$posacctid = $cacctdtls->LoadPOSAccountID();
if(count($posacctid) > 0)
{
    $options .= "<option value=\"Select One\">Select One</option>";
    for($i = 0 ; $i < count($posacctid) ; $i++)
    {
        if ($acctname == $posacctid[$i]["AccountID"]) {
            $options .= "<option selected value='".$posacctid[$i]["AccountID"]."'>".$posacctid[$i]["AccountID"]."</option>";
        } else {
            $options .= "<option value='".$posacctid[$i]["AccountID"]."'>".$posacctid[$i]["AccountID"]."</option>";
        }
    }    
}
echo $options;
?>