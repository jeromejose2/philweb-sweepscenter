<?php

/*
 * Added By : Jerome F. Jose
 * Added On : June 22, 2012
 * Purpose : 
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_TopUpLogs");

APP::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
APP::LoadControl("Hidden");
App::LoadControl("PagingControl2");
$fproc = new FormsProcessor();
$accounts = new SCC_Accounts();
$topup = new SCC_TopUpLogs();


$getlist = $accounts->SelectSiteIDbylist();
$ddlPos = new ComboBox("ddlPos", "ddlPos");
$Poslist = new ArrayList();
$Poslist->AddArray($getlist);
$ddlPos->ClearItems();
$litem = null;
$litem[] = new ListItem("---", "", true);
$litem[] = new ListItem("ALL", "0");
$ddlPos->Items = $litem;
$ddlPos->DataSource = $Poslist;
$ddlPos->DataSourceText = "Name";
$ddlPos->DataSourceValue = "SiteID";
$ddlPos->DataBind();

$txtDateFr = new TextBox("txtDateFr", "txtDateFr");
$txtDateFr->Length = "10";
$txtDateFr->ReadOnly = true;
$txtDateFr->Style = "text-align:center;";
$txtDateFr->Text = date("m/d/Y");

$txtDateTo = new TextBox("txtDateTo", "txtDateTo");
$txtDateTo->Length = "10";
$txtDateTo->ReadOnly = true;
$txtDateTo->Style = "text-align:center;";
$txtDateTo->Text = date("m/d/Y");

$btnSearch = new Button("btnSearch", "btnSearch", "Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checkrptinput();' ";

$itemsperpage = 20;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;
$pgcon->CssClass = "paging";

$hidPageNum = new Hidden("hidPageNum", "hidPageNum");

$fproc->AddControl($ddlPos);
$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSearch);
$fproc->AddControl($hidPageNum);


$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSearch->SubmittedValue == "Search")
    {
        $pgcon->SelectedPage = 1;
    }

    $daysinterval = (strtotime($txtDateTo->SubmittedValue) - strtotime($txtDateFr->SubmittedValue)) / 86400;

    if ($daysinterval <= 90)
    {
        $dDateFr = $txtDateFr->SubmittedValue;
        $dDateTo = $txtDateTo->SubmittedValue;
        $sPosAcct = $ddlPos->SubmittedValue;
        $_SESSION['start'] = $dDateFr;
        $_SESSION['end'] = $dDateTo;
        $_SESSION['posacct'] = $sPosAcct;

        $ds = new DateSelector($dDateTo);
        $date2 = $ds->NextDate;
        $dDateTo = $date2;
        $dDateTo = date_create($dDateTo);
        $dDateTo = date_format($dDateTo, "Y-m-d 10:00:00.00000");
        $dDateFr = date_create($dDateFr);
        $dDateFr = date_format($dDateFr, "Y-m-d 09:59:59.99999");


        $getTotal = $topup->GetdecTotal($sPosAcct, $dDateFr, $dDateTo);
        $dec_Total = $getTotal[0]['dec_Total'];
        $selectPosAcct1 = $topup->selectPosAcctName($sPosAcct, $dDateFr, $dDateTo);
        $pgcon->Initialize($itemsperpage, count($selectPosAcct1));
        $pgHist = $pgcon->PreRender();
        $wherelimit = "LIMIT " . ($pgcon->SelectedItemFrom - 1) . "," . $itemsperpage . " ";
        $arrposact = $topup->selectPosAcctNamewithLimit($sPosAcct, $dDateFr, $dDateTo, $wherelimit);
        $selectPosAcct = new ArrayList();
        $selectPosAcct->AddArray($arrposact);
    }
    else
    {
        $errormsgtitle = "INVALID INPUT";
        $errormsg = "Please limit date range selection to ninety (90) days or three (3) months only.";
    }
}
?>
