<?php
require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_CasinoGames");
App::LoadModuleClass($modulename, "SCC_AdminAccounts");
App::LoadModuleClass($modulename, "SCC_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCC_AuditTrail");

$cadminaccts = new SCC_AdminAccounts();
$cadminacctsessions = new SCC_AdminAccountSessions();
$caudittrail =  new SCC_AuditTrail();
$sccgames = new SCC_CasinoGames();


App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");


$frmAddgame = new FormsProcessor();

$ddlservice = new ComboBox("ddlservice","ddlservice","");
$ddlservice->ShowCaption = true;
$litem = null;
$litem[] = new ListItem("--Please Select--", 0, true);
$litem[] = new ListItem("RTG", 1);
$litem[] = new ListItem("MG", 3);
$ddlservice->Items = $litem;

$txtgametitle = new TextBox("txtgametitle","txtgametitle","");
$txtgametitle->ShowCaption = true;
$txtgametitle->Length = 20; 
$txtgametitle->Args = "size='34'";
$txtgametitle->Args = "onkeypress='javascript: return AlphaNumeric(event);'";

$txtgameref = new TextBox("txtgameref","txtgameref","");
$txtgameref->ShowCaption = true;
$txtgameref->Length = 20; 
$txtgameref->Args = "size='34'";
$txtgameref->Args = "onkeypress='javascript: return AlphaNumeric(event);'";

$btnBrowse = new Button("btnBrowse","btnBrowse","Browse");
$btnBrowse->IsSubmit = true;

$txtGID = new TextBox("txtGID","txtGID","");
$txtGID->ShowCaption = true;
$txtGID->Length = 5; 
$txtGID->Args = "size='5'";
$txtGID->Args = "onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtMID = new TextBox("txtMID","txtMID","");
$txtMID->ShowCaption = true;
$txtMID->Length = 5; 
$txtMID->Args = "size='5'";
$txtMID->Args = "onkeypress='javascript: return OnlyNumbers(event);'";

$btnOK = new Button("OKAY","OKAY","OKAY");
$btnOK->IsSubmit = true;

$btnPROCEED = new Button("PROCEED","PROCEED","PROCEED");
$btnPROCEED->IsSubmit = true;

$btnCANCEL = new Button("CANCEL","CANCEL","CANCEL");
$btnCANCEL->IsSubmit = true;

$hiddenpath  = new Hidden("hiddenpath","hiddenpath","hiddenpath");

$frmAddgame->AddControl($hiddenpath);
$frmAddgame->AddControl($ddlservice);
$frmAddgame->AddControl($txtgametitle);
$frmAddgame->AddControl($txtgameref);
$frmAddgame->AddControl($btnBrowse);
$frmAddgame->AddControl($txtGID);
$frmAddgame->AddControl($txtMID);
$frmAddgame->AddControl($btnOK);
$frmAddgame->AddControl($btnPROCEED);
$frmAddgame->AddControl($btnCANCEL);



$frmAddgame->ProcessForms();

if($frmAddgame->IsPostBack)
{
//if ($btnPROCEED->SubmittedValue == "PROCEED")
//{}
if ($btnOK->SubmittedValue == "OKAY")
{
        URL::Redirect('gamelist.php');    
}
if ($btnPROCEED->SubmittedValue == "PROCEED")
{      
        
$isGameExists = $sccgames->CheckIfGameNumberExists($ddlservice->SubmittedValue,rtrim(ltrim(strtoupper($txtgametitle->SubmittedValue))));
    if(count($isGameExists) > 0)
        {
        $gametitle = rtrim(ltrim(strtoupper($txtgametitle->SubmittedValue)));
        $serid = $ddlservice->SelectedText;
        $errormsgtitle = "Failed"; 
        $errormsg = "The game ".$gametitle." with Service Name ".$serid." already exists.";
        }
        else 
        {
        $imgpath = "/home/webadmin/www/sweepscenter/regulargaming/views/images/";
        //$imgpath = "/tmp/";
        if ($_FILES["file"]["error"] > 0)
        {
                $errormsgtitle = "ERROR"; 
                $errormsg = "Return Code: " . $_FILES["file"]["error"];
        }
        else
        {
    
            if ((($_FILES["file"]["type"] == "image/png")) && ($_FILES["file"]["size"] < 100000))
            {
                if (file_exists( $imgpath . $_FILES["file"]["name"]))
                {
                $errormsgtitle = "ERROR"; 
                $errormsg = $_FILES["file"]["name"] . " already exists. ";
                }
                else
                {
                move_uploaded_file($_FILES["file"]["tmp_name"], $imgpath . $_FILES["file"]["name"]);
                $completed ="1";
                }
            }
            else
            {
                $errormsgtitle = "ERROR"; 
                $errormsg = "Invalid File";
            }
        }    
        if (isset($completed))
        {        
        //insert game
        $game["ServiceID"] = $ddlservice->SubmittedValue;
        $game["GameTitle"] = rtrim(ltrim(strtoupper($txtgametitle->SubmittedValue)));
        $game["GameName"] = str_replace(" ","",strtolower($txtgameref->SubmittedValue));
        $game["GID"] = strtoupper($txtGID->SubmittedValue);
        $game["MID"] = $txtMID->SubmittedValue;
        $game["DateCreated"] = "now_usec()";
        $game["Status"] = "1";
        $game["CreatedByAID"] = $_SESSION['aid'];
        $game["ImageName"] = $_FILES["file"]["name"];
        //$game["ImagePath"] = $imgpath . $_FILES["file"]["name"];  1.0 ver
        $game["ImagePath"] = $_FILES["file"]["name"];
        $insertgame = $sccgames->Insert($game);        
            if($sccgames->HasError)
            {
                $message = "Error occured: " . $sccgames->getError();
                $title = "ERROR!";
            }
        //insert game
        // Log to Audit Trail
        $scauditlogparam["SessionID"] = $_SESSION['sid'];
        $scauditlogparam["AccountID"] = $_SESSION['aid'];
        $scauditlogparam["TransDetails"] = "Add New Game: " . $_SESSION['uname'];
        $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
        $scauditlogparam["TransDateTime"] = "now_usec()";
        $caudittrail->Insert($scauditlogparam);
        if ($caudittrail->HasError)
        {
            $errormsg = $caudittrail->getError();
            $errormsgtitle = "ERROR!";
            $boolrollback = true;
        }
        // Log to Audit Trail   
        $successmsgtitle = "SUCCESSFUL";
        $successmsg = "New game successfully added.";

        }
        
        }

  
}
}




?>
