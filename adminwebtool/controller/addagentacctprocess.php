<?php
/*
 * Added By : Angelo G. Cubos
 * Added On : Feb 1, 2013
 * Purpose : Adding Agent Accounts
 */
require_once("../init.inc.php");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadCore("PHPMailer.class.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename,"SCCC_AgentAccounts");
App::LoadModuleClass($modulename,"SCC_ref_AdminAccountTypes");
App::LoadModuleClass($modulename,"SCC_AdminAccounts");
APp::LoadModuleClass($modulename,"SCC_AdminAccountSessions");
App::LoadModuleClass($modulename,"SCC_AuditTrail");



$adminaccountsession = new SCC_AdminAccountSessions();
$adminaccount = new SCC_AdminAccounts();
$ref_adminaccounttypes = new SCC_ref_AdminAccountTypes();
$agentaccounts = new SCCC_AgentAccounts();
$caudittrail =  new SCC_AuditTrail();

$fproc = new FormsProcessor();

$txtUName = new TextBox("txtUName","txtUName","Username");
$txtUName->Length = 12;
$txtUName->Args="autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtPWord = new TextBox("txtPWord","txtPWord","Password");
$txtPWord->Length = 12;
$txtPWord->Password = true;
$txtPWord->Args="autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtCPWord = new TextBox("txtCPWord", "txtCPWord","Confirm Password");
$txtCPWord->Length = 12;
$txtCPWord->Password = true;
$txtCPWord->Args="autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Args ="onclick='javascript: return checkaddagentacct();'";

$btnConfirm = new Button("btnConfirm","btnConfirm","YES");
$btnConfirm->IsSubmit = true;
$btnConfirm->CssClass = "labelbutton2";


$fproc->AddControl($txtUName);
$fproc->AddControl($txtPWord);
$fproc->AddControl($txtCPWord);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnConfirm);

$fproc->ProcessForms();

if($fproc->IsPostBack)
{
    $proceed = true;
    if($btnSubmit->SubmittedValue == "Submit")
    {
    $uname = $txtUName->SubmittedValue;
    $pword = $txtPWord->SubmittedValue;
    
        if(strlen($uname) < 8)
        {    
            $errormsgtitle = "ERROR!";
            $errormsg = "Username should not be less than 8 characters.";
            $proceed = false;
        }
        else if(strlen($pword) < 6)
        {
            $errormsgtitle = "ERROR!";
            $errormsg = "Password should not be less than 6 characters.";        
            $proceed = false;
        }
        if($proceed == true)
        {
        $isexist = $agentaccounts->CheckUsername($uname);
            if(count($isexist) == 1)
            {
                $errormsgtitle = "ERROR!";
                $errormsg = "Agent\'s username already exists."; 
            }
            else
            {
                $confirmmsgtitle ="CONFIRMATION";
                $confirmmsg ="Continue creating new agent account?";
            }
        }
    }
    if($btnConfirm->SubmittedValue == "YES")
    {
    $uname = $txtUName->SubmittedValue;
    $pword = $txtPWord->SubmittedValue;

            $maxaccount = $agentaccounts->MaxAccountID();
            $lastID = $maxaccount[0]['LastID'] + 1;
            $agentaccounts->StartTransaction();
            $arragentacc['ID'] = $lastID;
            $arragentacc['UserName'] = $uname;
            $arragentacc['Password'] = $pword;
            $arragentacc['ServiceID'] = 3;
            $arragentacc['Status'] = 1;
            $agentaccounts->Insert($arragentacc);
            if($agentaccounts->HasError)
            {
            $agentaccounts->RollBackTransaction();
            $errormsgtitle = "ERROR!";    
            $errormsg = $agentaccounts->getError();            
            }
            else
            {
            $agentaccounts->CommitTransaction();
            // Log to Audit Trail
            $caudittrail->StartTransaction();

            $scauditlogparam["SessionID"] = $_SESSION['sid'];
            $scauditlogparam["AccountID"] = $_SESSION['aid'];
            $scauditlogparam["TransDetails"] = "Create Agent Account: " . $uname;
            $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $scauditlogparam["TransDateTime"] = "now_usec()";
            $caudittrail->Insert($scauditlogparam);
            if ($caudittrail->HasError)
            {
                $errormsg = $caudittrail->getError();
                $errormsgtitle = "ERROR!";
            }else{
                $caudittrail->CommitTransaction();
            }            
            $successmsgtitle = "SUCCESSFUL";
            $successmsg = "New agent account has been successfully created.";
            $txtUName->Text="";
            $txtPWord->Text="";
            
        }

        
            
    }
}
?>
