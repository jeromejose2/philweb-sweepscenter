<?php
/*
 * Created By       :       Angelo Cubos
 * Date Created     :       Dec 6, 2012
 * Purpose          :       Reset Terminal Balance
 */

include("../controller/terminalresetprocess2.php");
?>
<style>
    div.white_content2{
    position:fixed;
    top: 50%;
    left: 50%;
    margin-top: -9em; /*set to a negative number 1/2 of your height*/
    margin-left: -15em; /*set to a negative number 1/2 of your width*/
    }
</style>
<?php include("header.php"); ?>
<script type="text/javascript">
    function SelectPOSAccountName()
    {
        var acctid = document.getElementById('ddlacctno').options[document.getElementById('ddlacctno').selectedIndex].value;
        $("#ddlacctname").load(
            "../controller/get_posaccountname.php",
            {
                acctid: acctid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    
    function SelectPOSAccountNumber()
    {
        var acctname = document.getElementById('ddlacctname').options[document.getElementById('ddlacctname').selectedIndex].value;
        $("#ddlacctno").load(
            "../controller/get_posaccountid.php",
            {
                acctname: acctname
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }

</script>
<h3><b>Check If Terminal Is Subject For Balance Reset</b></h3>
<br />
<br />
<div align="center" style="margin-left: 10px; margin-top: 5px;">
    <table>
        <tr>
            <td><b>POS Account Name:</b></td>
            <td><?php echo $ddlacctname; ?></td>
        </tr>

        <tr>
            <td><b>Terminal Name:</b></td>
            <td><?php echo $ddltacct; ?></td><td colspan="3" align="center"><?php echo $btnSubmit; ?><?php echo $hidfname; ?><?php echo $hidtname; ?></td>
        </tr>
<!--        <tr><td colspan="3" align="center"><?php echo $btnSubmit; ?><?php echo $hidfname; ?><?php echo $hidtname; ?></td></tr>-->
    </table>
    <br/>
<?php echo $hidans; ?>
    <div id="callbacktable">
        <br/><hr>
        <h3 align="left">Terminal Details</h3>
        <table>
            <tr style="background-color:#FF9C42; height:30px;">
<!--                <th style="width:400px; text-align: left;">&nbsp;&nbsp;&nbsp;POS Account Name</th>
                <th style="width:400px; text-align: left;">&nbsp;&nbsp;&nbsp;Terminal Name</th>
                <th style="width:400px; text-align: left;">&nbsp;&nbsp;&nbsp;Balance</th>-->
                
                <th style="width:300px; text-align: left;">&nbsp;&nbsp;&nbsp;Terminal Name</th>
                <th style="width:300px; text-align: left;">&nbsp;&nbsp;&nbsp;Balance</th>
                <th style="width:300px; text-align: left;">&nbsp;&nbsp;&nbsp;Is Playable</th>                
                <!--<th style="width:300px; text-align: left;">&nbsp;&nbsp;&nbsp;Service ID</th>-->                   
            </tr>
            <tr style="background-color:#FFF1E6; height:30px;">
<!--                <td style="width:400px;">&nbsp;&nbsp;&nbsp;<?php //echo $acctname; ?></td>
                <td style="width:400px;">&nbsp;&nbsp;&nbsp;<?php //echo $terminal_name; ?></td>
                <td style="width:400px;">&nbsp;&nbsp;&nbsp;<?php //echo $terminalbalance; ?></td>-->
                <td style="width:300px;">&nbsp;&nbsp;&nbsp;<?php echo $terminal_name; ?></td>
                <td style="width:300px;">&nbsp;&nbsp;&nbsp;<?php echo $terminalbalance; ?></td>
                <td style="width:300px;">&nbsp;&nbsp;&nbsp;<?php if($isplayable == '1'){echo "Playable";}else{echo "Not Playable";}?></td>
                <!--<td style="width:300px;">&nbsp;&nbsp;&nbsp;<?php echo $serviceid; ?></td>--> 
            </tr>
        </table>
    </div>
    <div id="callbacktable2">
        <br/>
        <h3 align="left">Terminal Session</h3>
        <table>
            <tr style="background-color:#FF9C42; height:30px;">
                <th style="width:400px; text-align: left;">&nbsp;&nbsp;&nbsp;Terminal Name</th>
                <th style="width:400px; text-align: left;">&nbsp;&nbsp;&nbsp;Date Start</th>
                <th style="width:400px; text-align: left;">&nbsp;&nbsp;&nbsp;Date End</th>              
            </tr>
            <tr style="background-color:#FFF1E6; height:30px;">
                <td style="width:400px;">&nbsp;&nbsp;&nbsp;<?php echo $terminalsessionname; ?></td>
                <td style="width:400px;">&nbsp;&nbsp;&nbsp;<?php echo $terminalsessionstart; ?></td>
                <td style="width:400px;">&nbsp;&nbsp;&nbsp;<?php echo $terminalsessionend; ?></td>
            </tr>
        </table>
    </div>
    <div id="callbacktable3">
        <br/>
        <h3 align="left">Transaction Logs</h3>
        <table>
            <tr style="background-color:#FF9C42; height:30px;">
                <th style="width:240px; text-align: left;">&nbsp;&nbsp;&nbsp;Date Created</th>
                <th style="width:240px; text-align: left;">&nbsp;&nbsp;&nbsp;Transaction Type</th>
                <th style="width:240px; text-align: left;">&nbsp;&nbsp;&nbsp;Amount</th>
                <th style="width:240px; text-align: left;">&nbsp;&nbsp;&nbsp;Transaction ID</th>
                <th style="width:240px; text-align: left;">&nbsp;&nbsp;&nbsp;Status</th>                
            </tr>
            
<?php
        for ($i = 0; $i < count($arraytranslog); $i++)
        {
?>           
            <tr style="background-color:#FFF1E6; height:30px;">
                <td style="width:240px;">&nbsp;&nbsp;&nbsp;<?php echo $arraytranslog[$i]["DateCreated"]?></td>
                <td style="width:240px;">&nbsp;&nbsp;&nbsp;
                    <?php
                    if($arraytranslog[$i]["TransactionType"] == D){echo "Deposit";}
                    if($arraytranslog[$i]["TransactionType"] == R){echo "Reload";}
                    if($arraytranslog[$i]["TransactionType"] == W){echo "Withdraw";}
                    ?>
                </td>
                <td style="width:200px;">&nbsp;&nbsp;&nbsp;<?php echo $arraytranslog[$i]["Amount"]?></td>
                <td style="width:240px;">&nbsp;&nbsp;&nbsp;<?php echo $arraytranslog[$i]["ServiceTransactionID"]?></td>
                <td style="width:280px;">&nbsp;&nbsp;&nbsp;
                    <?php
                    if($arraytranslog[$i]["Status"] == 0){echo "Pending";}
                    if($arraytranslog[$i]["Status"] == 1){echo "Successful";}
                    if($arraytranslog[$i]["Status"] == 2){echo "Failed";}
                    if($arraytranslog[$i]["Status"] == 3){echo "Purged";}
                    if($arraytranslog[$i]["Status"] == 4){echo "Manually Redeemed";}
                    ?>
                </td>
            </tr>
<?php                
        }
?>
            
        </table>
    </div>     
    <br />
    
    <div align="center"><?php if ($terminalbalance > 0 && $terminalsessionend != NULL) {echo $btnUpdate;} ?></div>
    
</div>

<script type="text/javascript">
$(document).ready(function(){
   $('#btnresetpass').hide();
   $('#callbacktable').hide();
   $('#callbacktable2').hide();
   $('#callbacktable3').hide();
   $('#btnSubmit').live('click',function(){
      if($('#ddlacctname').val() == 'Select One' ) {
        var html;
        html = '<div class=\"titleLightbox\">Notification</div>';
        html += '<div class=\"msgLightbox\">';
        html += '<p>Please enter an account name.</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" />';
        html += '</div>';
        
        showLightBox(html);
        return false;
      }
      var text = $('#ddlacctname option:selected').text();
      $('#fname').val(text);
      var terminal = $('#tacct option:selected').text();
      $('#tname').val(terminal);
      return true;
   });
   
   <?php if (isset($acctname)): ?>
        $('#tacct').removeAttr("disabled");
        $('#btnresetpass').show();
        $('#callbacktable').show();
        $('#callbacktable2').show();
        $('#callbacktable3').show();
   <?php endif; ?>

   $('#btnUpdate').click(function(){
      var text = $('#ddlacctname option:selected').text();
      $('#fname').val(text);  

      if($('#tacct').val() == 'Select One' || $('#tacct').val() == null || $('#tacct').is(':disabled')) {
         html = '<div class=\"titleLightbox\">Notification</div>';
         html += '<div class=\"msgLightbox\">';
         html += '<p>Please select terminal account</p>';
         html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" />';
         html += '</div>';
      } else {
         html = '<div class=\"titleLightbox\">Confirmation</div>';
         html += '<div class=\"msgLightbox\">';
         html += '<p>Would you like to reset the terminal\'s balance?</p>';
         html += '<br /><input id=\"btnResetBalance\" name=\"btnResetBalance\" type=\"button\" value=\"YES\" class=\"labelbutton2\" />&nbsp;&nbsp;<input id=\"btnCancel\" type=\"button\" value=\"NO\" class=\"labelbutton2\" />';
         html += '</div>';
         
      }
      showLightBox(html);
      return false;
   });

   $('#btnCancel').live('click',function(){
      $('#light').fadeOut('slow',function(){
         $('#light').remove();
      });
      $('#fade').fadeOut('slow');
      return false;
   });

   $('#ddlacctname').bind('change',function(){
      var val = $(this).val();
      if($(this).val() != 'Select One') {
         getTerminals(val);
      } else {
         $('#tacct').html('<option value="Select One">Select One</option>'); 
      }
   });

   $('#ddlacctno').bind('change',function(){
      var val = $(this).val(); 
      if($(this).val() != 'Select One') {
         getTerminals(val);
      } else {
         $('#tacct').html('<option value="Select One">Select One</option>'); 
      }
   });

    function getTerminals(val) 
    {
//        var html = '<h1>Loading...</h1>';
//        showLightBox(html);
        var data = $('#frmTemplate').serialize();
        $.ajax({
        type: 'post',
        url : '../controller/get_terminalsnotfe.php',
        data : data,
        dataType: 'json',
        success : function(data){
            $('#light').fadeOut('slow',function(){
                $('#light').remove();
            });
            $('#fade').fadeOut('slow');

            var opt='';
            $('#tacct').removeAttr('disabled');
            jQuery.each(data,function(k,v){
                opt+='<option value="' + k +'">' + v + '</option>';
            });
            $('#tacct').html(opt);
        },
        error : function(){
            $('#tacct').html('<option value="">no terminal account</option>');
            $('#light').fadeOut('slow',function(){
                $('#light').remove();
            });
            $('#fade').fadeOut('slow');

            var html;
            html = '<div class=\"titleLightbox\">Notification</div>';
            html += '<div class=\"msgLightbox\">';
            html += '<p>Oops! Something went wrong</p>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" />';
            html += '</div>';
            showLightBox(html);
        }
        });
    }

   $('#btnResetBalance').live('click',function(){
    document.getElementById("hidans").value =(document.getElementById("btnResetBalance").value);
      $('#light').remove();
      showLightBox('<h1>Loading...</h1>');
      var url = '../controller/terminalresetprocess.php';
      var data = $('#frmTemplate').serialize();
      
      $.ajax({
         url : url,
         data : data,
         type : 'post',
         success : function(data) {
            if(data != 'Error' ) {
                    html = '<div class=\"titleLightbox\">SUCCESSFUL</div>';
                    html += '<div class=\"msgLightbox\">';
                    html += '<p>Transaction Completed</p>';
                    html += '<br /><input id=\"btnDone\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" onclick=\"javascript: return reload()\"></input>';
                    html += '</div>';
                     $('#light').html(html); 
                     
            } 
         },
         error : function() {
            var html = '<h1>Oops! Something went wrong.2</h1>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" />';
            $('#light').html(html); 
         }
      });
      return false;
   });
});
function reload()
{
    window.location = "terminalreset2.php";
}

<?php if (isset($ID)): ?>
    $("#tacct").val(<?php echo $ID; ?>);
<?php endif; ?>



</script>

<?php include("footer.php"); ?>