<?php

/*
 * Added By : Jerome F. Jose
 * Added On : June 21, 2012
 * Purpose : Change Password Admin User Account
 */
include("../controller/changepasswordprocess.php");

?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script type="text/javascript">
function checkchangepassword()
{
    var sPassword = document.getElementById("txtPassword");
    var sNewPassword = document.getElementById("txtNewPassword");
    var sConfirm = document.getElementById("txtConfPassword");

    var currentpassword = '<?php echo $_SESSION['actualpassword'];?>';
        
    if (sPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
                var msg = "Please enter the current password to continue.";
                var msgtitle = "INVALID INPUT";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
//		document.getElementById('light10').style.display='block';
//		document.getElementById('fade').style.display='block';
//		document.getElementById('title3').innerHTML = "INVALID INPUT";
//		document.getElementById('msg3').innerHTML = "Please enter your current password.";
		sPassword.focus();
		return false;
    }
    if (sPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 8)
    {
                var msg = "Password must not be less than 8 characters.";
                var msgtitle = "INVALID INPUT";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
		sPassword.focus();
		return false;
    }
    if (sNewPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {

                var msg = "Please enter your new password.";
                var msgtitle = "INVALID INPUT";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
		sNewPassword.focus();
		return false;
    }
    if (sNewPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 8)
    {

                var msg = "Password must not be less than 8 characters.";
                var msgtitle = "INVALID INPUT";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
		sNewPassword.focus();
		return false;
    }
    if (sConfirm.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    { 

                var msg = "Confirm password is required.";
                var msgtitle = "INVALID INPUT";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
		sConfirm.focus();
		return false;
    }
    if (sConfirm.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 8)
    {

                var msg = "Password must not be less than 8 characters.";
                var msgtitle = "INVALID INPUT";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
		sConfirm.focus();
		return false;
    }
    if (sNewPassword.value != sConfirm.value)
    {

                var msg = "The new passwords you enter do not match.";
                var msgtitle = "INVALID INPUT";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
		sConfirm.focus();
		return false;
    }
    
	if (sPassword.value != currentpassword)
    {
		var msg = "The current password you entered is invalid. Please try again.";
                var msgtitle = "INVALID INPUT";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
		return false;
    }
	if (sNewPassword.value == sPassword.value)
    {

		var msg = "New password cannot be similar to the current password.";
                var msgtitle = "INVALID INPUT";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
		return false;
    }
    document.getElementById('light11').style.display='block';
    document.getElementById('fade').style.display='block';
    document.getElementById('title2').innerHTML = "CONFIRMATION";
    document.getElementById('msg2').innerHTML = "You are about to change your current password. Do you wish to continue?";
    


    return false;
} 
</script>



<?php include("header.php")?>
<form action="changepassword.php" method="post">
  <div id="page">
    <table width="100%">
      <tr>
        <td colspan="2"><div id="header">Fill out this form to change your password.</div>
        </td>
      </tr>
      <tr style="background-color:#FFF1E6; height:30px;">
        <td class="fontboldblack" style="width:400px;">&nbsp;&nbsp;Current Password:</td>
        <td style="width:400px;">
          &nbsp;&nbsp;<?php echo $txtPassword;?>
        </td>
      </tr>
      <tr style="background-color:#FFF1E6; height:30px;">
        <td class="fontboldblack">&nbsp;&nbsp;New Password:</td>
        <td>
          &nbsp;&nbsp;<?php echo $txtNewPassword;?>
        </td>
      </tr>
      <tr style="background-color:#FFF1E6; height:30px;">
        <td class="fontboldblack">&nbsp;&nbsp;Confirm New Password:</td>
        <td>
          &nbsp;&nbsp;<?php echo $txtConfPassword;?>
        </td>
      </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align="center">
            <?php echo $btnOk;?>
<!--	  <input type="button" name="btnOK" id="btnOK" class="labelbutton2" onclick="javascript: return checkchangepassword('<?php echo $_SESSION['sid']; ?>','');" value="SUBMIT" /> -->
        </td>
      </tr>
    </table>
	<!-- ERROR MESSAGE(Invalid change password) -->
      <div id="light12" style="text-align: center;font-size: 16pt;height: 280px;width:470px; margin-left:-95px;" class="white_content">
        <div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
   		
	</div>
        <br />
	<br />
	<div id="msg">
  		
	</div>
	<br/>
	<br/>
	<br />
        <!--<input id="btnOk2" type="button" value="OK" class="labelbutton2" onclick="document.getElementById('light12').style.display='none';document.getElementById('fade').style.display='none';"/>-->
        <input id="btnClose" type="button" value="OK" class="labelbutton2" />
      </div>
      <!-- END OF ERROR MESSAGE(Invalid change password) -->	
	<!-- CONFIRMATION MESSAGE -->
      <div id="light11" style="text-align: center;font-size: 16pt;height: 250px; margin-left:-95px;" class="white_content">
        <div id="title2" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">
   
	</div>
        <br />
	<br />
	<div id="msg2">
  
	</div>
	<br/>
	<br/>
	<br />
        <?php echo $passSubmit;?>
        <input id="btnClose" type="button" value="CANCEL" class="labelbutton2" />
      </div>
      <!-- END OF CONFIRMATION MESSAGE -->	
	<!-- ERROR MESSAGES -->
      <div id="light10" style="text-align: center;font-size: 16pt;height: 250px; margin-left:-95px;" class="white_content">
        <div id="title3" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">
   
	</div>
        <br />
	<br />
	<div id="msg3">
  
	</div>
	<br/>
	<br/>
	<br />
        <input id="passSubmit1" type="button" name="passSubmit1" value="OK" class="labelbutton2" />
      </div>
      <!-- END OF ERROR MESSAGES -->
  </div>

</form>

<script type="text/javascript">
  <?php
  if ($isprocessed >= 0)
  {

  ?>
  result();
  <?php
  }
  ?>

$('#passSubmit').live('click',function(){
   $('#light10').fadeOut('slow');
   $('#fade').fadeOut('slow');   
});

$('#passSubmit1').live('click',function(){
   $('#light10').fadeOut('slow');
   $('#fade').fadeOut('slow');   
});

$('#btnClose').live('click',function(){
   $('#light11').fadeOut('slow');
   $('#light12').fadeOut('slow');
   $('#fade').fadeOut('slow');   
});
       
    <?php if (isset($successmsg)): ?>
    
                var msg = "You have successfully changed your password. Please check your email for new log-in credentials.";
                var msgtitle = "SUCCESSFUL PASSWORD CHANGE";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
//                var currentpassword = sNewPassword;
                $("input#txtPassword,input#txtNewPassword,input#txtConfPassword").val("");
                
    
    <?php endif; ?>
        
</script>

<?php include("footer.php");?>
<script>
$(function () {
    $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
});
</script>
