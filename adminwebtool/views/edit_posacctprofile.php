<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 18, 2012
 * Purpose          :       POS Account Update Profile: Updating of POS Account
 */

include("../controller/edit_posacctprofileprocess.php");
?>
<link href="css/base/jquery.ui.all.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css">
<script language="javascript" src="jscripts/jquery-1.5.2.min.js" ></script>
<script language="javascript" src="jscripts/jquery-ui-1.8.13.custom.min.js" ></script>
<script language="javascript" src="jscripts/lightbox.js" ></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>

<?php include("header.php"); ?>
<style type="text/css">
    .hasSpace {
        border-collapse: collapse;
    }
    
    .hasSpace td {
        padding-top: .3em;
        padding-bottom: .3em;
    }
</style>
<script type="text/javascript">
    function forpassport()
    {
    var chkpass = document.getElementById("chkpass").checked;
    if(chkpass) $("#txtpassport").removeAttr("disabled");
    else $("#txtpassport").attr("disabled", true);$('#txtpassport').val(''); 
    }
    function forsss()
    {
    var chksss = document.getElementById("chksss").checked;
    if(chksss) $("#txtsss").removeAttr("disabled");
    else $("#txtsss").attr("disabled", true);$('#txtsss').val('');     
    }
    function fordriver()
    {
    var chkdriver = document.getElementById("chkdriver").checked;
    if(chkdriver) $("#txtdriver").removeAttr("disabled");
    else $("#txtdriver").attr("disabled", true);$('#txtdriver').val(''); 
    }
    function forothers()
    {
    var chkothers = document.getElementById("chkothers").checked;
    if(chkothers) $("#txtothers").removeAttr("disabled");
    else $("#txtothers").attr("disabled", true);$('#txtothers').val(''); 
    }    
    function SelectPOSAccountName()
    {
        var acctid = document.getElementById('ddlacctno').options[document.getElementById('ddlacctno').selectedIndex].value;
        $("#ddlacctname").load(
            "../controller/get_posaccountname.php",
            {
                acctid: acctid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    
    function SelectPOSAccountNumber()
    {
        var acctname = document.getElementById('ddlacctname').options[document.getElementById('ddlacctname').selectedIndex].value;
        $("#ddlacctno").load(
            "../controller/get_posaccountid.php",
            {
                acctname: acctname
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
</script>

<div align="center" style="margin-left: 10px; margin-top: 5px;">
    <form method="post" action="posacctprofile.php">
        <table>
            <tr>
                <td><b>Account Name:</b></td>
                <td><?php echo $ddlacctname; ?></td>
            </tr>
            <tr>
                <td><b>Account Number:</b></td>
                <td><?php echo $ddlacctno; ?></td>
            </tr>
        </table>
        <br/>
        <div align="center"><?php echo $btnSearch; ?><?php echo $hiddenid; ?></div>
    </form>
</div>
<br/><hr>

<form name="frmeditacct" id="frmeditacct" method="POST" action="edit_posacctprofile.php">
<div align="center" style="margin-left: 10px; margin-top: 5px;">
    <table>
        <tr>
            <td><b>Account Name:</b></td><td><?php echo $acct_name ?></td>
        </tr>
        <tr>
            <td><b>Account Number:</b></td><td><b><?php echo $acct_id ?><?php echo $hiddenid; ?></b></td>
        </tr>
    </table>
    <br/>
</div>

<br/><hr>
<div style="margin-left: 10px; margin-top: 5px;">
    <div style="font-weight: bold;"><u>ACCOUNT INFORMATION</u></div>
        <table class="hasSpace" style="margin-top: 15px;">
            <tr>
                <td>*Cashier Username</td><td><?php echo $txtuname; ?></td><td><label style="font-style: italic; font-size: 10px;">Minimum of 8 and maximum of 15 characters.</label></td>
            </tr>
            <tr><td>*Name of Operator</td></tr>
            <tr><td>First Name</td><td>Middle Name</td><td>Last Name</td></tr>
            <tr>
                <td><?php echo $txtfname; ?></td>
                <td><?php echo $txtmname; ?></td>
                <td><?php echo $txtlname; ?></td>
            </tr>
            <tr><td>*SWC Code</td><td><?php echo $txtscode; ?></td></tr>
            <?php if($_SESSION['activeprovider'] == 3){//1 for RTG ?> 
            <tr><td>*Agent Username</td><td><?php echo $txtagentuname; ?></td></tr>
            <?php }?> 
        </table>
    
        <table style="margin-top: 10px;">
            <tr>
                <td id="lblHomeAddr">*Home Address</td><td><?php echo $txthadd; ?></td>
            </tr>
        </table>
    
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td id="lblZipcode">*Zip Code</td><td><?php echo $txtzip; ?></td>
            </tr>
            <tr>
                <td id="lblTel">*Telephone No.</td><td><?php echo $txttelno; ?></td>
            </tr>
        </table>
    
        <div style="font-weight: bold; margin-top: 10px;"><u>ADDITIONAL INFORMATION</u></div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td>Mobile No.</td><td><?php echo $txtmobno; ?></td>
            </tr>
            <tr>
                <td>Fax No.</td><td><?php echo $txtfax; ?></td>
            </tr>
            <tr>
                <td id="lblEmail">*E-mail Address</td><td><?php echo $txtemail; ?></td>
            </tr>
            <tr>
                <td>Website Address</td><td><?php echo $txtwebadd; ?></td>
            </tr>
            <tr>
                <td>Country of Residence</td><td><?php echo $ddlcountry; ?></td>
            </tr>
            <tr>
                <td>Citizenship</td><td><?php echo $txtcitizen; ?></td>
            </tr>
            <tr>
                <td id="lblBday">*Date of Birth</td><td><?php echo $txtbday; ?></td>
            </tr>
        </table>
        
        <div id="provideID" style="font-weight: bold; margin-top: 10px;">*Please provide at least one form of identification.</div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td><?php echo $chkpass; ?></td>
                <td id="lblPassport">Passport</td>
                <td><?php echo $txtpassport; ?></td>
            </tr>
            <tr>
                <td><?php echo $chkdriver; ?></td>
                <td id="lblDriverLicense">Driver&#39;s License</td>
                <td><?php echo $txtdriver; ?></td>
            </tr>
            <tr>
                <td><?php echo $chksss; ?></td>
                <td id="lblSss">Social Security ID</td>
                <td><?php echo $txtsss; ?></td>
            </tr>
            <tr>
                <td><?php echo $chkothers; ?></td>
                <td id="lblOthers">Others</td>
                <td><?php echo $txtothers; ?></td>
            </tr>
        </table>
        
        <div style="font-weight: bold; margin-top: 10px;"><u>BANK INFORMATION</u></div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td>Bank</td>
                <td><?php echo $ddlbank; ?></td>
            </tr>
            <tr>
                <td>Bank Account Type</td>
                <td><?php echo $ddlbanktype; ?></td>
            </tr>
            <tr>
                <td>Bank Branch</td>
                <td><?php echo $txtbankbranch; ?></td>
            </tr>
            <tr>
                <td>Bank Account Name</td>
                <td><?php echo $txtbankacctname; ?></td>
            </tr>
            <tr>
                <td>Bank Account Number</td>
                <td><?php echo $txtbankacctnum; ?></td>
            </tr>
        </table>
        
        <div style="font-weight: bold; margin-top: 10px;"><u>PAYEE BANK INFORMATION</u></div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td>Bank</td>
                <td><?php echo $ddlpayeebank; ?></td>
            </tr>
            <tr>
                <td>Bank Account Type</td>
                <td><?php echo $ddlpayeebanktype; ?></td>
            </tr>
            <tr>
                <td>Bank Branch</td>
                <td><?php echo $txtpayeebankbranch; ?></td>
            </tr>
            <tr>
                <td>Bank Account Name</td>
                <td><?php echo $txtpayeebankacctname; ?></td>
            </tr>
            <tr>
                <td>Bank Account Number</td>
                <td><?php echo $txtpayeebankacctnum; ?></td>
            </tr>
        </table>
        <br/>
        <div align="center"><?php echo $btnSave; ?><?php echo $btnBack; ?><?php echo $hidden; ?></div>
        <br/>
</div>
</form>
<script type="text/javascript">
$(document).ready(function(){
   $('#btnSearch').live('click',function(){
      if($('#ddlacctname option:selected').val() == 'Select One') {
         html = '<div class=\"titleLightbox\">Notification<\/div>';
         html += '<div class=\"msgLightbox\">';
         html += '<p>Please select an account name and number</p>';
         html += '<br \/><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"><\/input>';
         html += '</div>';
         showLightBox(html);
         return false;
      }
      $('#hiddenid').val($('#ddlacctname').val());
      $('#frmTemplate').attr('action','posacctprofile.php');
      $('#frmTemplate').submit();
   });

   $('#btnSave').live('click',function(){
      if(validateInput() > 0) {
         html = '<div class=\"titleLightbox\">Notification</div>';
         html += '<div class=\"msgLightbox\">';
         html += '<p>Please fill in all required fields</p>';
         html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
         html += '</div>';
         showLightBox(html);
         return false;
      }
      $('#hidden').val($('#ddlacctname').val());
      $('#frmeditacct').attr('action','edit_posacctprofile.php');
      $('#frmeditacct').submit();
   });

   $('#btnBack').live('click',function(){
        $('#hiddenid').val($('#ddlacctname').val());
        $('#frmTemplate').attr('action','posacctprofile.php');
        $('#frmTemplate').submit();
   });

   function validateInput()
   {
      var numberoferror = 0;
      numberoferror += addOrRemoveErrorFieldClass("lblHomeAddr","txthadd");
      numberoferror += addOrRemoveErrorFieldClass("lblZipcode","txtzip");
      numberoferror += addOrRemoveErrorFieldClass("lblTel","txttelno");
      numberoferror += addOrRemoveErrorFieldClass("lblEmail","txtemail");
      numberoferror += addOrRemoveErrorFieldClass("lblBday","txtbday");
      numberoferror += addOrRemoveErrorFieldClass("lblPassport","txtpassport","chkpass");
      numberoferror += addOrRemoveErrorFieldClass("lblDriverLicense","txtdriver","chkdriver");
      numberoferror += addOrRemoveErrorFieldClass("lblSss","txtsss","chksss");
      numberoferror += addOrRemoveErrorFieldClass("lblOthers","txtothers","chkothers");
      return numberoferror
   }

   function trim(str)
   {
      return jQuery.trim(str);
   }

   function addOrRemoveErrorFieldClass(lblId,inputId,checkId)
   {
      var error = 0;
        if(($('#txtpassport').val().length === 0) && ($('#txtdriver').val().length === 0)&& ($('#txtsss').val().length === 0) && ($('#txtothers').val().length === 0)){
            $('#provideID').addClass('errorField');
            error = 1;
        }       
      if(checkId !== null) {
         if($('#'+checkId).is(':checked') && trim($('#'+inputId).val()) == '') {
            $('#'+lblId).addClass('errorField');
            error = 1;
         } else {
            $('#'+lblId).removeClass('errorField');
         } 
      } else {
         if(trim($('#'+inputId).val()) === '') {
            $('#'+lblId).addClass('errorField');
            error = 1;
         } else {
            $('#'+lblId).removeClass('errorField');
         }   
      }   
      return error;
   }
})

<?php if (isset($successmsg)): ?>
    var msg = "<?php echo $successmsg; ?>";
    var msgtitle = "<?php echo $successmsgtitle; ?>";
    html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
    html += '<div class=\"msgLightbox\">';
    html += '<p>' + msg + '</p>';
    html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" ;></input>';
    html += '</div>';
    showLightBox(html);
<?php endif; ?>
<?php if (isset($emailerrormsg)): ?>
    var msg = "<?php echo $emailerrormsg; ?>";
    var msgtitle = "<?php echo $emailerrortitle; ?>";
    html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
    html += '<div class=\"msgLightbox\">';
    html += '<p>' + msg + '</p>';
    html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
    html += '</div>';
    showLightBox(html);
<?php endif; ?>
</script>
<?php include("footer.php"); ?>
<script>
$(function () {
    $('input[type=text],[type=password] , [id=txthadd]').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
});
</script>