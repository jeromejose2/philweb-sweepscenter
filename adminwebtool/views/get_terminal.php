<?php

/*
 * Added On : July 2, 2012
 * Created by : Jerome F. Jose
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_Terminals");

$terminals = new SCC_Terminals();

$sid = $_POST["sid"];

$options = "<option value = '0'>Please select</option>";

$getlist = $terminals->getIDList($sid);

if(count($getlist) > 0)
{
     for($i = 0 ; $i < count($getlist) ; $i++)
    {
        $options .= "<option value='".$getlist[$i]["ID"]."'>".$getlist[$i]["Name"]."</option>";
    }
}
echo $options;          


?>
