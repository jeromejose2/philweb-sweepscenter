<?php

/*
 * Added By : Angelo G. Cubos
 * Added On : Feb 1, 2013
 * Purpose : Adding Agent Accounts
 */
include("../controller/addagentacctprocess.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<?php include("header.php")?>
<div id="page">
    <form name ="frmAdduser" method="POST">
        
  <table width="100%">
    <tr>
      <td colspan="2">
        <div id="header">Create Agent Account</div>
      </td>
    </tr>
     <tr style="background-color:#FFF1E6; height:30px;">
      <td class="fontboldblack" style="width:50%;">&nbsp;&nbsp;Username</td>
      <td style="width:50%;"><?php echo $txtUName;?>
      </td>
    </tr>
    <tr style="background-color:#FFF1E6; height:30px;">
      <td class="fontboldblack">&nbsp;&nbsp;Password</td>
      <td><?php echo $txtPWord;?>
    </tr>  
<!--    <tr style="background-color:#FFF1E6; height:30px;">
      <td class="fontboldblack">&nbsp;&nbsp;Confirm Password:</td>
      <td><?php echo $txtCPWord;?>
    </tr>-->
  </table>
  <br />
  <center>
         <?php echo $btnSubmit;?></center>

  <?php include("footer.php"); ?>
<script>
$(function () {
    $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
});

</script>
<script type="text/javascript">   
<?php if (isset($errormsg)) : ?>
        var msg = "<?php echo $errormsg; ?>";
        var msgtitle = "<?php echo $errormsgtitle; ?>";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input> ';
        html += '</div>';
        showLightBox(html);
<?php endif; ?>   
<?php if (isset($confirmmsg)): ?>
        var msg = "<?php echo $confirmmsg;?>";
        var msgtitle = "<?php echo $confirmmsgtitle; ?>";
        var button = "<?php echo $btnConfirm; ?>";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br />'+ button+'      <input id=\"btnOkay\" type=\"button\" value=\"NO\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        
<?php endif; ?>
<?php if (isset($successmsg)) : ?>
        var msg = "<?php echo $successmsg; ?>";
        var msgtitle = "<?php echo $successmsgtitle; ?>";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input> ';
        html += '</div>';
        showLightBox(html);
<?php endif; ?>
    
</script>

