<?php
/*
 * Created by Jerome Jose
 * Date June 22-2012
 * Purpose : View Terminal Deposit history
 */
include("../controller/rptterminaldeposithistprocess.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script language="javascript" src="jscripts/datepicker.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script language="javascript" type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
       
    }
</script>
<?php include('header.php')?>
<form name="Frmterminaldeposit" method="POST">
<div style="width:100%; text-align:center;">
  <table width="100%">
    <tr>
      <td class="labelbold2">From:</td>
      <td>
          <?php echo $txtDateFr;?>
        <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
      </td>
            
      <td class="labelbold2">To:</td>
      <td> <?php echo $txtDateTo; ?>
        <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
      </td>
           
       </tr>
    <tr>
      <td colspan="5" class="labelbold2">POS Account Name:&nbsp;&nbsp;
         <?php echo $ddlPos;?>&nbsp;&nbsp;&nbsp;&nbsp;
         <?php echo $btnSearch; ?>
      </td>
    </tr>
    <tr>
       <?php if(isset($_SESSION['start'])): ?>
      <td colspan="5">
          <?php
         echo "<br />";
         echo "<table style=\"overflow-x:scroll; width:100%;\" ><tr><th colspan =\"3\" style=\"height:30px;background-color: #FF9C42; color:#000000;\">SALES HISTORY</th></tr>";
         echo "<tr><th class=\"th\" style=\"width:30%;\">Date Processed</th><th class=\"th\" style=\"width:30%;\">POS Account Name</th><th class=\"th\" style=\"width:40%;\">Deposit Details</th></tr>";
            
             if(count($getredemption) > 0) 
                  { 
         echo  "<br/>";
         
         if($pgcon->SelectedItemTo > count($getredemption1))
         {
         echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".count($getredemption1)." of ".count($getredemption1)."</b></p>";    
         }else{
         echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".$pgcon->SelectedItemTo." of ".count($getredemption1)."</b></p>";
         }
               for($x=0;$x<count($getredemption);$x++)
                 {
                        $name =  $getredemption[$x]['Name'];
                        $dateprocess =   $getredemption[$x]['TransDate'];
                        $amount = $getredemption[$x]['Amount'];
                        $transID = $getredemption[$x]['TransID'];
                        $posaccountname =  $getredemption[$x]['Name1'];
                       
                         $mod = $ctr % 2;
                 if ($mod == 0)
                        {
                            echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"td\">".$dateprocess."</td><td class=\"td\">".$posaccountname;
                            echo "</td><td class=\"td\"><table><tr><td><b>Transaction Number:</b></td><td>".$transID."</td></tr>";
                            echo "<tr><td><b>Terminal Name:</b></td><td>".$name."</td></tr><tr><td><b>Points:</b></td><td>".number_format($amount,2)."</td></tr></table></td></tr>";
                        }
                        else
                        {
                            echo "<tr style=\"height:30px;\"><td class=\"td\">".$dateprocess."</td><td class=\"td\">".$posaccountname;
                            echo "</td><td class=\"td\"><table><tr><td><b>Transaction Number:</b></td><td>".$transID."</td></tr>";
                            echo "<tr><td><b>Terminal Name:</b></td><td>".$name."</td></tr><tr><td><b>Points:</b></td><td>".number_format($amount,2)."</td></tr></table></td></tr>";
                        }
                   }
                  if(($pgcon->SelectedItemFrom-1 + count($getredemption)) == count($getredemption1))
                  {
                	echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"labelbold\">&nbsp;Total Terminal Deposits</td><td colspan=\"2\" class=\"labelbold\">&nbsp;".number_format($total,2)."</td></tr>";
                  }
                echo "</td></tr><tr><td colspan=\"3\" align=\"center\"></td></tr><tr><td colspan=\"3\" align=\"center\">";
                echo "<div>";
                echo  "<br/>"; 
                echo "<b class=\"paging\">$pgHist</b>";
                echo "</div>";
                echo "</td></tr>";
                echo '<tr><td colspan="5" align="center"><br/>   
                    <b><a href="export_report.php?fn=Terminal_Deposit_History" class="labelbutton2">DOWNLOAD</a></b>
                    </td></tr>';
           }else
            {
              echo "<tr><td colspan=\"3\" align=\"center\"><b>No Records Found.</b></td></tr>";
            }
           echo "</table>";
         ?>
        </td>
      <?php else:?>
      <?php endif;?>
        <?php
         unset($_SESSION['report_header']);
	 unset($_SESSION['report_values']);
         
         $_SESSION['report_header']=array("\"DATE PROCESSED\"","\"POS ACCOUNT NAME\"","\"TRANSACTION NUMBER\"","\"TERMINAL NAME\"","\"POINTS\"");
         for($counter=0;$counter<count($getredemption1);$counter++)
		{
			 $POSAcctName = str_replace(',', '', $getredemption1[$counter]['Name1']);
			$_SESSION['report_values'][$counter][0] = $getredemption1[$counter]['TransDate'];
			$_SESSION['report_values'][$counter][1] = $POSAcctName;
			$_SESSION['report_values'][$counter][2] = $getredemption1[$counter]['TransID'];
			$_SESSION['report_values'][$counter][3] = $getredemption1[$counter]['Name'];
			$_SESSION['report_values'][$counter][4] = $getredemption1[$counter]['Amount'];
		} 
		$_SESSION['report_values'][count($getredemption1)][0] = "Total Terminal Deposits";	
		$_SESSION['report_values'][count($getredemption1)][1] = $total;
         
         ?>
    </tr>
    <tr>
      <td colspan="5">&nbsp;</td>
    </tr>

  </table>
    
    	<!-- ERROR MESSAGE -->
	<div id="light22" style="text-align: center;font-size: 16pt;height: 300px;width:500px;" class="white_content">
		<div id="title" style="width: p00px;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">
		
		</div>
		<br />
		<br />
		<div id="msg"></div>
		<br/>
		<br/>
		<br />	
		<input id="btnOk" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light22').style.display='none';document.getElementById('fade').style.display='none';" />
	</div>
	<!-- END OF NOTIFICATION MESSAGE -->	
 </div>
    </form>
<script type="text/javascript">

<?php if (isset($errormsg)) : ?>
var msg = "<?php echo $errormsg; ?>";
var msgtitle = "<?php echo $errormsgtitle; ?>";
html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
html += '<div class=\"msgLightbox\">';
html += '<p>' + msg + '</p>';
html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input> ';
html += '</div>';
showLightBox(html);
<?php endif; ?>

</script>    
    <?php include('footer.php')?>