<?php

/*
 * Added On: April 18, 2013
 * Created By: ANGC
 * Purpose: Check If Terminal Is Not Viewable In The POS
 */
include('../controller/checkterminalifnotviewableprocess.php');

?>

<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script type="text/javascript" src="jscripts/jquery-1.5.2.min.js" ></script>
<script type="text/javascript" src="jscripts/jquery-ui-1.8.13.custom.min.js" ></script>
<script language="javascript" src="jscripts/datepicker.js" type="text/javascript"></script>
<script language="javascript" src="jscripts/checkinputs.js" type="text/javascript"></script>

<!--<style>
    div.white_content2{
    position:fixed;
    top: 50%;
    left: 50%;
    margin-top: -9em; /*set to a negative number 1/2 of your height*/
    margin-left: -15em; /*set to a negative number 1/2 of your width*/
    }
</style>-->

<?php include('header.php');?>

<script type="text/javascript">

    function get_terminals_exceptfreeentry()
    {
        var acctid = document.getElementById('ddlPosName').options[document.getElementById('ddlPosName').selectedIndex].value;

        $("#ddlTerminalName").load(
            "../controller/get_terminal_exceptfreeentry.php",
            {
                sid : acctid
            }
//            function(status, xhr)
//            {
//                if(status == "error")
//                {
//                    document.getElementById('title1').innerHTML = "ERROR!";
//                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
//                    document.getElementById('light1').style.display = "block";
//                    document.getElementById('fade').style.display = "block";
//                }
//            }
        );
    }
    
    
    function checkmanualredemptionchk()
    {
        var posacctid = document.getElementById('ddlPosName').options[document.getElementById('ddlPosName').selectedIndex].value;
        var terminalid = document.getElementById('ddlTerminalName').options[document.getElementById('ddlTerminalName').selectedIndex].value;
        
        if(posacctid == "0")
        {
            var msgtitle = "INSUFFICIENT DATA";
            var msg = "Please enter POS Account Name.";
            html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
            html += '<div class=\"msgLightbox\">';
            html += '<p>' + msg + '</p>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
            html += '</div>';
            showLightBox(html);
            return false;
        }
        if(terminalid == "Please Select")
        {
             var msgtitle = "INSUFFICIENT DATA";
            var msg = "Please enter Terminal Name.";
            html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
            html += '<div class=\"msgLightbox\">';
            html += '<p>' + msg + '</p>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
            html += '</div>';
            showLightBox(html);
            return false;
        }
        

        return true;
    }
    
    function disablealphakeys(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if((charCode == 8) || (charCode == 46) || (charCode > 47 && charCode < 58))
            return true;

        return false;
    }
    
    function displayconfirmation()
    {
        document.getElementById('light24').style.display = "block";
        document.getElementById('fade').style.display = "block";
    }
    
</script>
<h3><b>Terminal Is Not Viewable In The POS</b></h3>
<br />
<br />
<form name="frmmanualRedemPtionchk" method ="POST"> 
<div align="center" style="margin-left: 10px; margin-top: 5px;">
    <table>
        <tr> <?php echo $hidlist;?>
            <td class="labelbold2">POS Account Name:</td>
            <td><?php echo $ddlPosName;?></td>
        </tr>
        <tr>
            <td class="labelbold2">Terminal Name:</td>
            <td><?php echo $ddlTerminalName;?></td><td colspan="3" align="center"><?php echo $btnSearch; ?></td>
        </tr>
<!--        <tr>
            <td colspan="4" align="left">
                <center><?php // echo $btnSearch; ?></center>
            </td>
        </tr>-->
    </table>
    <br />
    
     <!-- Display Record-->
    <?php if ($display):?>
    <br /><hr>
    <h3 align="left">Terminal Details</h3>
    <table>
        <tr style="background-color:#FF9C42; height:30px;">
            <th style="width:300px; text-align: left;">&nbsp;&nbsp;&nbsp;Terminal Name</th>
            <th style="width:300px; text-align: left;">&nbsp;&nbsp;&nbsp;Balance</th>
            <th style="width:300px; text-align: left;">&nbsp;&nbsp;&nbsp;Casino Balance</th>
            <th style="width:300px; text-align: left;">&nbsp;&nbsp;&nbsp;Status</th>            
        </tr>
        <tr style="background-color:#FFF1E6; height:30px;">
            <td style="width:300px;">&nbsp;&nbsp;&nbsp;<?php echo $terminalname?></td>
            <td style="width:300px;">&nbsp;&nbsp;&nbsp;<?php echo $terminalbalance?></td>
            <td style="width:300px;">&nbsp;&nbsp;&nbsp;<?php echo $casinobalance?></td>
            <td style="width:300px;">&nbsp;&nbsp;&nbsp;<?php echo $status?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr> 
    </table>
    <br />
    <h3 align="left">Terminal Session</h3>
    <table>
        <tr style="background-color:#FF9C42; height:30px;">
            <th style="width:400px; text-align: left;">&nbsp;&nbsp;&nbsp;Terminal Name</th>
            <th style="width:400px; text-align: left;">&nbsp;&nbsp;&nbsp;Date Start</th>
            <th style="width:400px; text-align: left;">&nbsp;&nbsp;&nbsp;Date End</th>
        </tr>
        <?php if(!isset($activesessions) || isset($activesessions))?>
              <?php {?>
        <tr style="background-color:#FFF1E6; height:30px;">
            <td style="width:400px;">&nbsp;&nbsp;&nbsp;<?php echo $terminalname?></td>
            <td style="width:400px;">&nbsp;&nbsp;&nbsp;<?php echo $datestart?></td>
            <td style="width:400px;">&nbsp;&nbsp;&nbsp;<?php echo $dateend?></td>
        </tr>
         <?php }?>
        <tr>
            <td>&nbsp;</td>
        </tr> 
    </table>
    <br/>
    <?php echo $recommendation?>
<!--    <div id ="rec"><b>Recommendation: <?php //echo $recommendation?></b></div>-->
    <?php endif;?>
    
<!--    </table>-->
    <!-- END OF NOTIFICATION MESSAGE -->
    </div>
<div id="light1" class="white_content">
    <div class="light-title" id="title1"></div>
    <div class="light-msg" id="msg1"></div>
    <div type="button" class="inputBoxEffectPopup" id="button1" style="margin-left:auto;margin-right:auto;"  onclick ="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';"></div>
</div>
<div id="light2" class="white_content">
    <div class="light-title" id="title2"></div>
    <div class="light-msg" id="msg2"></div>
    <?php echo $btnOkay;?>
    <input id="btnCancel" type="button" value="" class="inputBoxEffectPopupCancel" onclick ="document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';javascript:location.reload(true)"/>
</div>
<div id="fade" class="black_overlay"></div>    
</form>
<script type="text/javascript">
    function confirmationreset()
    {
        var msgtitle = "CONFIRMATION";
        var msg = "Continue to reset terminal?";
        var btnproceed = '<?php echo addslashes($btnProceed);?>';
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br />'+btnproceed+'&nbsp;&nbsp;&nbsp;&nbsp;<input id=\"btnOkay\" type=\"button\" value=\"CANCEL\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);        
    }    
    
   $(document).ready(function(){
  $('#rec').hide();
   $('.rec').hide();
//   $('#btn').live('click',function(){
//      if($('#ddlPosName').val() == 'Select One' ) {
//        var html;
//        html = '<div class=\"titleLightbox\">Notification</div>';
//        html += '<div class=\"msgLightbox\">';
//        html += '<p>Please enter a POS Account name.</p>';
//        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" />';
//        html += '</div>';
//        
//        showLightBox(html);
//        return false;
//      }
//      
//   });
   <?php if (isset($terminalname
            )): ?>
        $('#rec').show();
        $('.rec').show();
   <?php endif; ?>
   });
</script>
<?php include('footer.php');?>