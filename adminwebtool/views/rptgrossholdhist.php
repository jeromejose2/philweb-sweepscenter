<?php
/*
 * Date June 25-2012
 * Purpose : 
 */
include("../controller/rptgrossholdhistprocess.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script language="javascript" src="jscripts/datepicker.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script language="javascript" type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
       
    }
</script>
<?php include('header.php')?>
<form name="frmrptgrossholdhist" method="POST">
    <div style="width:100%; text-align:center;">
  <table>
    <tr>
      <td class="labelbold2">From:</td>
      <td><?php echo $txtDateFr ?>
        <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
      </td>
      <td class="labelbold2">To:</td>
      <td><?php echo $txtDateTo ?>
<!--        <input id="txtDateTo" name="txtDateTo" maxlength="10" value="<?php //echo $dDateTo; ?>"  onkeypress="javascript: return isNumberKey(event);" style="text-align:center;" readonly/>-->
        <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
      </td>
      <td width="100px">
          <?php echo $btnSearch; ?>
      </td>
    </tr>
     <tr>
      <td colspan="5" class="labelbold2">POS Account Name:&nbsp;&nbsp;
          <?php echo $ddlPos; ?>
           </td>
    </tr>
    <?php if(isset($_SESSION['start'])):?>
    <tr>
      <td colspan="5">
          <?php 
         echo "<br />";
         echo "<table style=\"overflow-x:scroll;\" ><tr><th colspan =\"7\" style=\"height:30px;background-color: #FF9C42; color:#000000;\">CASH ON HAND SUMMARY</th></tr>";
         echo "<tr><th class=\"th\">Date</th><th class=\"th\">POS Account Name</th><th class=\"th\">Terminal Name</th><th class=\"th\">Total Sales</th><th class=\"th\">Total Reloads</th><th class=\"th\">Total Cash Redemptions</th><th class=\"th\">Cash On Hand</th></tr>";
           if(count($getgrosshold) > 0)
           {
         echo  "<br/>";
         if($pgcon->SelectedItemTo > count($getgrosshold1))
         {
         echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".count($getgrosshold1)." of ".count($getgrosshold1)."</b></p>";
         }else{
         echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".$pgcon->SelectedItemTo." of ".count($getgrosshold1)."</b></p>";    
         }
           
            for($x=0;$x<count($getgrosshold);$x++)
                    {
              
               $mod = $x % 2;
                  $row01a =  $getgrosshold[$x][0];
                    $dateTime1 = new DateTime($row01a);
                    $row0 = $dateTime1->format("m/d/Y h:i:s a");
                  $row1 =  $getgrosshold[$x][1];
                  $row2 =  $getgrosshold[$x][2];
                  $row3 =  $getgrosshold[$x][3];
                  $row4 = $getgrosshold[$x][4];
                  $row5 = $getgrosshold[$x][5];
                  $row6 =  $getgrosshold[$x][6];
                
                    if ($mod == 0)
                    {
                        echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"td\">".$row0."</td><td class=\"td\">".$row1."</td><td class=\"td\">".$row6."</td><td class=\"td\">".$row2."</td><td class=\"td\">".$row3."</td><td class=\"td\">".number_format($row4,2)."</td><td class=\"td\">".$row5."</td></tr>";
                    }
                    else
                    {
                        echo "<tr style=\"height:30px;\"><td class=\"td\">".$row0."</td><td class=\"td\">".$row1."</td><td class=\"td\">".$row6."</td><td class=\"td\">".$row2."</td><td class=\"td\">".$row3."</td><td class=\"td\">".number_format($row4,2)."</td><td class=\"td\">".$row5."</td></tr>";
                    }
                  
                  }
                                         
              if((($pgcon->SelectedItemFrom-1) + count($getgrosshold)) == count($getgrosshold1))
               {
                  echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td colspan=\"7\" class=\"labelbold\" style=\"text-align:right;\">&nbsp;Total Cash On Hand: &nbsp;".number_format($totalsales,2)."&nbsp;&nbsp;&nbsp;</td></tr>";
               }
                echo "</table>";
                echo "<div>";
                echo  "<br/>"; 
                
                echo "</div>";
                echo "</td></tr><tr><td colspan=\"7\" align=\"center\"></td></tr><tr><td colspan=\"7\" align=\"center\">";
                echo "<p class=\"paging\">$pgHist </p>";
                echo  "<br/>";
                echo "<b><a href='export_report.php?fn=Cash_On_Hand_History' class=\"labelbutton2\">DOWNLOAD</a></b></td></tr>";

           }else
             {
              echo "<tr><td colspan=\"5\" align=\"center\"><b>No Records Found.</b></td></tr>";
              echo "</table>";
             }
          ?>
          
        <?php
         unset($_SESSION['report_header']);
	 unset($_SESSION['report_values']);
$_SESSION['report_header']=array("\"Date\"","\"POS Account Name\"","\"Terminal Name\"","\"Total Sales\"","\"Total Reloads\"","\"Total Cash Redemptions\"","\"Cash On Hand\"");
          for($counter=0;$counter< count($getgrosshold1);$counter++)
            {
              
                  $row01ab =  $getgrosshold1[$counter][0];
                   $dateTime1 = new DateTime($row01ab);
                    $row0 = $dateTime1->format("m/d/Y h:i:s A");
                  $row1 =  $getgrosshold1[$counter][1];
//                  str_replace(',', '', $row1);
                  $row2 =  $getgrosshold1[$counter][2];
                  $row3 =  $getgrosshold1[$counter][3];
                  $row4 = $getgrosshold1[$counter][4];
                  $row5 = $getgrosshold1[$counter][5];
                  $row6 =  $getgrosshold1[$counter][6];
                
                $_SESSION['report_values'][$counter][0] = $row0;
                $_SESSION['report_values'][$counter][1] = $row1;
                $_SESSION['report_values'][$counter][2] = $row6;
                $_SESSION['report_values'][$counter][3] = $row2;
                $_SESSION['report_values'][$counter][4] = $row3;
                $_SESSION['report_values'][$counter][5] = $row4;
                $_SESSION['report_values'][$counter][6] = $row5;
            }
            $_SESSION['report_values'][count($getgrosshold1)][0] = "Total Cash On Hand";
            $_SESSION['report_values'][count($getgrosshold1)][1] = number_format($totalsales,2);

        ?>
      </td>
    </tr>
    <?php else:?>
    <?php endif;?>
    <tr>
      <td colspan="5">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="5" align="center">
      </td>
    </tr>
  </table>
        	<!-- ERROR MESSAGE -->
	<div id="light13" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
		<div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
   		
		</div>
		<br />
		<br />
		<div id="msg">
  
		</div>
		<br/>
		<br/>
		<br />
		<input id="btnOk" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light13').style.display='none';document.getElementById('fade').style.display='none';" />
	</div>
	<!-- END OF ERROR MESSAGE -->
 </div>
    
</form>
<script type="text/javascript">

<?php if (isset($errormsg)) : ?>
var msg = "<?php echo $errormsg; ?>";
var msgtitle = "<?php echo $errormsgtitle; ?>";
html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
html += '<div class=\"msgLightbox\">';
html += '<p>' + msg + '</p>';
html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input> ';
html += '</div>';
showLightBox(html);
<?php endif; ?>

</script>
<?php include('footer.php')?>
