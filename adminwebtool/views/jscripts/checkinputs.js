function checklogin()
{
    var sUsername = document.getElementById("txtUsername");
    var sPassword = document.getElementById("txtPassword");

    sUsername = sUsername.trim();
    sPasword = sPassword.trim();

    if (sUsername.value.length == 0)
    {
        alert("Please enter your username.");
        sUsername.focus();
        return false;
    }
    else if (sPassword.value.length == 0)
    {
        alert("Please enter your password.");
        sPassword.focus();
        return false;
    }
    return true;
}

function processchangepassword(sid)
{
    document.getElementById('light12').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    document.getElementById('title2').innerHTML = "CONFIRMATION";
    document.getElementById('msg2').innerHTML = "You are about to change your current password. Do you wish to continue?";
}

function checkadduseracct()
{
    var sFName = document.getElementById("txtFName");
    var sMName = document.getElementById("txtMName");
    var sLName = document.getElementById("txtLName");
    var sEmail = document.getElementById("txtEmail");
    var sPosition = document.getElementById("txtPosition");
    var sCompany = document.getElementById("txtCompany");
    var sDepartment = document.getElementById("txtDepartment");
    var sGroup = document.getElementById("ddlGroup").options[document.getElementById("ddlGroup").selectedIndex].text;
    var sUName = document.getElementById("txtAuname");
    var sPWord = document.getElementById("txtPWord");
    var sCPWord = document.getElementById("txtCPWord");

    if (sUName.value.length == 0 && sPWord.value.length == 0 &&
            sCPWord.value.length == 0 && sFName.value.length == 0 &&
            sMName.value.length == 0 && sLName.value.length == 0 &&
            sEmail.value.length == 0 && sPosition.value.length == 0 &&
            sCompany.value.length == 0 && sDepartment.value.length == 0 && sGroup == "Select Group") {

        var msgtitle = "INVALID INPUT";
        var msg = "Please fill in all fields.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

        //showLightBoxMessage("Please fill in all fields.");
        sUName.focus();
        return false;
    }

    if (jQuery.trim(sUName.value) == '')
    {
        //showLightBoxMessage("Please supply the username.");
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the username.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sUName.focus();
        return false;
    }
    if (sUName.value.length < 6 || jQuery.trim(sUName.value) == '')
    {
//        showLightBoxMessage("Invalid Username.");
        var msgtitle = "INVALID INPUT";
        var msg = "Invalid Username.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sUName.focus();
        return false;
    }
    if (jQuery.trim(sPWord.value) == '')
    {
//        showLightBoxMessage("Please supply the password.");
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the password.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sPWord.focus();
        return false;
    }
    if (sPWord.value.length < 6 || jQuery.trim(sPWord.value) == '')
    {
//        showLightBoxMessage("Invalid Password.");
        var msgtitle = "INVALID INPUT";
        var msg = "Invalid Password.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sPWord.focus();
        return false;
    }
    if (jQuery.trim(sCPWord.value) == '')
    {
//        showLightBoxMessage("Please confirm password.");
        var msgtitle = "INVALID INPUT";
        var msg = "Please confirm password.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sCPWord.focus();
        return false;
    }
    if (sPWord.value != sCPWord.value)
    {
//        showLightBoxMessage("Password you've entered do not match.");
        var msgtitle = "INVALID INPUT";
        var msg = "Password you've entered do not match.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sCPWord.focus();
        return false;
    }
    if (jQuery.trim(sFName.value) == '')
    {
//        showLightBoxMessage("Please supply the first name.");
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the first name.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sFName.focus();
        return false;
    }
    if (jQuery.trim(sMName.value) == '')
    {
//        showLightBoxMessage("Please supply the middle name.");
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the middle name.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sMName.focus();
        return false;
    }
    if (jQuery.trim(sLName.value) == '')
    {
//        showLightBoxMessage("Please supply the last name.");
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the last name.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sLName.focus();
        return false;
    }
    if (jQuery.trim(sEmail.value) == '')
    {
//        showLightBoxMessage("Please supply the email address.");
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the email address.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sLName.focus();
        return false;
    }
    if (!checkemail(sEmail))
    {
//        showLightBoxMessage("Please supply valid email address.");
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply valid email address.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sLName.focus();
        return false;
    }
    if (jQuery.trim(sPosition.value) == '')
    {
//        showLightBoxMessage("Please supply the position.");
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the position.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sPosition.focus();
        return false;
    }
    if (jQuery.trim(sCompany.value) == '')
    {
//        showLightBoxMessage("Please supply the company.");
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the company.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sCompany.focus();
        return false;
    }
    if (jQuery.trim(sDepartment.value) == '')
    {
//        showLightBoxMessage("Please supply the department.");
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the department.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDepartment.focus();
        return false;
    }
    if (sGroup == "Select Group")
    {
        //showLightBoxMessage("Please supply the group type.");
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the group type.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        document.getElementById("ddlGroup").focus();
        return false;
    }
    document.getElementById("hidGroup").value = document.getElementById("ddlGroup").options[document.getElementById("ddlGroup").selectedIndex].value;
    return true;
}

// date created june 29,2011
function showLightBoxMessage(msg) {
    var e = document.getElementById('frmTemplate');
    var conheight = e.offsetHeight;
    document.getElementById('fade').style.height = conheight;
    document.getElementById('light14').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    document.getElementById('title').innerHTML = "INVALID INPUT";
    document.getElementById('msg').innerHTML = msg;
}

function resetadduseracct()
{
    document.getElementById("txtFName").value = "";
    document.getElementById("txtMName").value = "";
    document.getElementById("txtLName").value = "";
    document.getElementById("txtEmail").value = "";
    document.getElementById("txtPosition").value = "";
    document.getElementById("txtCompany").value = "";
    document.getElementById("txtDepartment").value = "";
    document.getElementById("ddlGroup").value = "0";
    document.getElementById("txtAuname").value = "";
    document.getElementById("txtPWord").value = "";
    document.getElementById("txtCPWord").value = "";
}

function onchange_username()
{

    document.getElementById("hidUsername").value = document.getElementById("ddlUsername").options[document.getElementById("ddlUsername").selectedIndex].text;
    document.getElementById("hidAccountID").value = document.getElementById("ddlUsername").options[document.getElementById("ddlUsername").selectedIndex].value;

}


function checkedituseracct()
{
    var sFName = document.getElementById("txtFName");
    var sMName = document.getElementById("txtMName");
    var sLName = document.getElementById("txtLName");
    var sEmail = document.getElementById("txtEmail");
    var sPosition = document.getElementById("txtPosition");
    var sCompany = document.getElementById("txtCompany");
    var sDepartment = document.getElementById("txtDepartment");
    var sGroup = document.getElementById("ddlGroup").options[document.getElementById("ddlGroup").selectedIndex].text;

    if (sFName.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the first name.";
        sFName.focus();
        return false;
    }
    if (sMName.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the middle name.";
        sMName.focus();
        return false;
    }
    if (sLName.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the last name.";
        sLName.focus();
        return false;
    }
    if (sEmail.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the email address.";
        sLName.focus();
        return false;
    }
    if (!checkemail(sEmail))
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply valid email address.";
        sLName.focus();
        return false;
    }
    if (sPosition.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the position.";
        sPosition.focus();
        return false;
    }
    if (sCompany.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the company.";
        sCompany.focus();
        return false;
    }
    if (sDepartment.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the department.";
        sDepartment.focus();
        return false;
    }
    if (sGroup == "Select Group")
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please choose the group type.";
        document.getElementById("ddlGroup").focus();
        return false;
    }

    document.getElementById("hidGroup").value = document.getElementById("ddlGroup").options[document.getElementById("ddlGroup").selectedIndex].value;
    document.getElementById("hidUsername").value = document.getElementById("ddlUsername").options[document.getElementById("ddlUsername").selectedIndex].value;
    return true;
}

function checkupdateacctstatus()
{
    var sRemarks = document.getElementById("txtRemarks");
    var sStatus = document.getElementById("ddlStatus").options[document.getElementById("ddlStatus").selectedIndex].text;

    if (sStatus == "----")
    {
        document.getElementById('light15').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please choose the account status.";
        document.getElementById("ddlStatus").focus();
        return false;
    }
    if (sRemarks.value.length == 0)
    {
        document.getElementById('light15').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please enter the reason for changing the account status.";
        sRemarks.focus();
        return false;
    }

    document.getElementById('light8').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    document.getElementById("hidStatus").value = document.getElementById("ddlStatus").options[document.getElementById("ddlStatus").selectedIndex].value;
    document.getElementById("hidStatusval").value = document.getElementById("ddlStatus").options[document.getElementById("ddlStatus").selectedIndex].text;
    return true;
}

function checkinput30()
{
    var sDateFrom = document.getElementById("txtDateFr");
    var sDateTo = document.getElementById("txtDateTo");
    var sAccountId = document.getElementById("ddlPosName").value;
    var re = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;

    if (sDateFrom.value.length == 0)
    {

        document.getElementById('msg').innerHTML = "";
        var msg = "Please enter the start date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

        sDateFrom.focus();
        return false;
    }
    if (re.test(sDateFrom.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sDateTo.value.length == 0)
    {

        var msg = "Please enter the end date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    if (re.test(sDateTo.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    var from = sDateFrom.value;
    var to = sDateTo.value;
    from = from.split('/', 3);
    from = from[2] + from[0] + from[1];
    to = to.split('/', 3);
    to = to[2] + to[0] + to[1];
    if (to < from)//if (sDateTo.value < sDateFrom.value)
    {
        //var msg = "'Date To' should be greater than 'Date From.' ";
        var msg = "Invalid date range.";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sAccountId == "---")
    {

        var msg = "Insufficient Data ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        return false;
    }
    if (sAccountId == "")
    {

        var msg = " Insufficient Data";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

        return false;
    }
    return true;
}


function checkinput()
{
    var sDateFrom = document.getElementById("txtDateFr");
    var sDateTo = document.getElementById("txtDateTo");
    var sAccountId = document.getElementById("ddlPos").value;
    var re = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;

    if (sDateFrom.value.length == 0)
    {

        document.getElementById('msg').innerHTML = "";
        var msg = "Please enter the start date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

        sDateFrom.focus();
        return false;
    }
    if (re.test(sDateFrom.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sDateTo.value.length == 0)
    {

        var msg = "Please enter the end date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    if (re.test(sDateTo.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    var from = sDateFrom.value;
    var to = sDateTo.value;
    from = from.split('/', 3);
    from = from[2] + from[0] + from[1];
    to = to.split('/', 3);
    to = to[2] + to[0] + to[1];

    if (to < from)//if (sDateTo.value < sDateFrom.value)
    {
        //var msg = "'Date To' should be greater than 'Date From.' ";
        var msg = "Invalid date range.";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sAccountId == "---")
    {

        var msg = "Insufficient Data ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        return false;
    }
    if (sAccountId == "")
    {

        var msg = " Insufficient Data";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

        return false;
    }
    return true;
}
function checkinput20()
{
    var sDateFrom = document.getElementById("txtDateFr");
    var sDateTo = document.getElementById("txtDateTo");
    var sAccountId = document.getElementById("ddlPos").value;
    var re = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;
    if (sDateFrom.value.length == 0)
    {

        document.getElementById('msg').innerHTML = "";
        var msg = "Please enter the start date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

        sDateFrom.focus();
        return false;
    }
    if (re.test(sDateFrom.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sDateTo.value.length == 0)
    {

        var msg = "Please enter the end date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    if (re.test(sDateTo.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    var from = sDateFrom.value;
    var to = sDateTo.value;
    from = from.split('/', 3);
    from = from[2] + from[0] + from[1];
    to = to.split('/', 3);
    to = to[2] + to[0] + to[1];

    if (to < from)//if (sDateTo.value < sDateFrom.value)
    {

        //var msg = "'Date To' should be greater than 'Date From.' ";
        var msg = "Invalid date range.";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }


    return true;
}

function num_only()
{
    if (event.keyCode < 47 || event.keyCode > 57)
        return false;
}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function isLetter(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode < 31 && (charCode > 48 || charCode < 57))
        return false;

    return true;
}

function checkemail(email)
{
    var str = email.value
    var filter = /^.+@.+\..{2,3}$/

    if (filter.test(str))
        return true;
    else
    {
        document.getElementById("txtEmail").focus();
        return false;
    }
    return true;
}

/*Added by Arlene*/
function checkUsername(lightbox)
{
    if (document.getElementById('ddlUsername').value == 0)
    {
        document.getElementById(lightbox).style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else
    {
        return true;
    }
}

function redirect2(page)
{
    window.location = 'template.php?page=' + page;
}

function sample()
{
    alert("ok");
}

function success()
{
    document.getElementById('light17').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    document.getElementById('title2').innerHTML = "INVALID INPUT";
    document.getElementById('msg2').innerHTML = "New user account has been successfully created. Log in credentials have been sent to registered email address.";
}

function redirectUserAccount()
{
    window.location = 'template.php?page=useracctprofile';
}

function numeric(evt)
{
    keyHit = evt.which;
    NumericCode = "61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122";
    if (NumericCode.indexOf(keyHit) < 0)
    {
        return false;
    }
    return true;
}

function checkrptinput()
{
    var sDateFrom = document.getElementById("txtDateFr");
    var sDateTo = document.getElementById("txtDateTo");
    var sAccountId = document.getElementById("ddlPos").value;
    var re = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;

    if (sDateFrom.value.length == 0)
    {

        var msg = "Please enter the start date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (re.test(sDateFrom.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sDateTo.value.length == 0)
    {

        var msg = "Please enter the end date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    if (re.test(sDateTo.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    var from = sDateFrom.value;
    var to = sDateTo.value;
    from = from.split('/', 3);
    from = from[2] + from[0] + from[1];
    to = to.split('/', 3);
    to = to[2] + to[0] + to[1];
    if (to < from)//if (sDateTo.value < sDateFrom.value)
    {

        //var msg = "'Date To' should be greater than 'Date From.' ";
        var msg = "Invalid date range.";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }

    if (sAccountId == "---" || sAccountId == "")
    {

        var msg = "Insufficient Data ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        return false;
    }


    return true;
}

function checkvoucherinput()
{
    var sDateFrom = document.getElementById("txtDateFr");
    var sDateTo = document.getElementById("txtDateTo");
    var sAccountId = document.getElementById("ddlPos").value;
    var sVoucherCode = document.getElementById("vchrcode").value;
    var sVoucherUsage = document.getElementById("vchrusage").value;
    var re = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;

    if (sDateFrom.value.length == 0)
    {

        var msg = "Please enter the start date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (re.test(sDateFrom.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sDateTo.value.length == 0)
    {

        var msg = "Please enter the end date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    if (re.test(sDateTo.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    var from = sDateFrom.value;
    var to = sDateTo.value;
    from = from.split('/', 3);
    from = from[2] + from[0] + from[1];
    to = to.split('/', 3);
    to = to[2] + to[0] + to[1];
    if (to < from) //
            // if (sDateTo.value < sDateFrom.value)
            {

                //var msg = "Date To' should be greater than 'Date From. ";
                var msg = "Invalid date range.";
                var msgtitle = "INVALID INPUT";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
                sDateFrom.focus();
                return false;
            }
    if (sAccountId == "" || sAccountId == "---")
    {

        var msg = "Insufficient Data ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        return false;
    }
    /*
     if(sVoucherCode == "")
     {
     document.getElementById('light24').style.display='block';
     document.getElementById('fade').style.display='block';
     document.getElementById('title').innerHTML = "INVALID INPUT";
     document.getElementById('msg').innerHTML = "Insufficient Data.";
     return false;
     }
     */
    return true;
}

/** Copy from checkinputs_.js **/
function validateEmail(elementValue)
{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(elementValue);
}

/**************************************************
 * Author: Bryan Salazar
 * Date Created: June 6, 2011
 * Description: Wrapper for document.getElementById
 *************************************************/
function elem(id)
{
    return document.getElementById(id);
}

/**************************************************
 * Author: Bryan Salazar
 * Date Created: June 6, 2011
 * Parameter: string lblId
 * Parameter: string inputId
 * Description:  
 *************************************************/
function addOrRemoveErrorFieldClass(lblId, inputId, checkId)
{
    var lblElem = elem(lblId);
    var inputElem = elem(inputId);
    if (checkId != null) {
        var chkElem = elem(checkId);
        if (chkElem.checked && inputElem.value.trim() == '') {
            addClass(lblElem, "errorField");
        } else {
            removeClass(lblElem, "errorField");
        }
    } else {
        if (inputElem.value.trim() == '') {
            addClass(lblElem, "errorField");
        } else {
            removeClass(lblElem, "errorField");
        }
        if (inputId == "uname") {
            var inputVal = inputElem.value.trim();
            if (inputVal.length <= 7 || inputVal.length > 15) {
                addClass(lblElem, "errorField");
            } else {
                removeClass(lblElem, "errorField");
            }
        }
        if (inputId == "email") {
            if (!validateEmail(inputElem.value.trim())) {
                addClass(lblElem, "errorField");
            } else {
                removeClass(lblElem, "errorField");
            }
        }
    }
}

function atleastOne()
{
    var passport = elem('txtpassport');
    var dlisnse = elem('txtdriver');
    var sss = elem('txtsss');
    var others = elem('txtothers');
    var please = elem('please');
    if (passport.value.trim() == '' && dlisnse.value.trim() == '' && sss.value.trim() == '' && others.value.trim() == '') {
        addClass(please, 'errorField');
    } else {
        removeClass(please, 'errorField');
    }
}

/********************************************
 * Author: Bryan Salazar
 * Date Created: June 6, 2011
 * Parameter: object ele (element)
 * Parameter: string cls (class name)
 * Description: Check if element has this cls
 ********************************************/
function hasClass(ele, cls)
{
    return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}

/**************************************
 * Author: Bryan Salazar
 * Date Created: June 6, 2011
 * Parameter: object ele (element)
 * Parameter: string cls (class name)
 * Description: Remove class name (cls)
 **************************************/
function removeClass(ele, cls)
{
    if (hasClass(ele, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        ele.className = ele.className.replace(reg, ' ');
    }
}

/************************************
 * Author: Bryan Salazar
 * Date Created: June 6, 2011
 * Parameter: object ele (element)
 * Parameter: string cls (class name)
 * Description: Add class name (cls)
 ************************************/
function addClass(ele, cls) {
    if (!this.hasClass(ele, cls))
        ele.className += " " + cls;
}

/** added by bryan **/
function setFocusAndReturnFalse(elem)
{
    var e = document.getElementById('frmTemplate');
    var conheight = e.offsetHeight;
    document.getElementById('fade').style.height = conheight;
    html = '<div class=\"titleLightbox\">Notification<\/div>';
    html += '<div class=\"msgLightbox\">';
    html += '<br /><br /><p>Please fill in all required fields</p><br /><br />';
    html += '<input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"><\/input>';
    html += '</div>';

    $('#frmTemplate').append('<div id="light" class="white_content2"></div>');
    $('.white_content2').html(html);

    $('#fade').show();
    $('.white_content2').show();
    elem.select();
    elem.focus();
    return false;
}

function checkinput_create_pos_acct()
{
    var validagent = document.getElementById('hiddenvalidauname').value;
    var acctiveprovider = document.getElementById('hiddenactiveprovider').value;
    var sUname = elem("txtuname");
    var sfname = elem("txtfname");
    var smname = elem("txtmname");
    var slname = elem("txtlname");
    var sscode = elem("txtscode");
    var sAuname = elem("txtAuname");
//    var sApword = elem("txtApword");
    var shadd = elem("txthadd");
    var szip = elem("txtzip");
    var stelno = elem("txttelno");
    var semail = elem("txtemail");
    var sbday = elem("txtbday");
    var spassport = elem("txtpassport");
    var sdlisnse = elem("txtdriver");
    var ssss = elem("txtsss");
    var sothers = elem("txtothers");

    addOrRemoveErrorFieldClass("lblCashier", "txtuname");
    addOrRemoveErrorFieldClass("lblFname", "txtfname");
    addOrRemoveErrorFieldClass("lblMname", "txtmname");
    addOrRemoveErrorFieldClass("lblLname", "txtlname");
    addOrRemoveErrorFieldClass("lblSwc", "txtscode");
    if (acctiveprovider == 3)
    {
        addOrRemoveErrorFieldClass("lblAus", "txtAuname");
    }
    //addOrRemoveErrorFieldClass("lblApw","txtApword");    
    addOrRemoveErrorFieldClass("lblHomeAddr", "txthadd");
    addOrRemoveErrorFieldClass("lblZipcode", "txtzip");
    addOrRemoveErrorFieldClass("lblTel", "txttelno");
    addOrRemoveErrorFieldClass("lblEmail", "txtemail");
    addOrRemoveErrorFieldClass("lblBday", "txtbday");
    addOrRemoveErrorFieldClass("lblPassport", "txtpassport", "chkpass");
    addOrRemoveErrorFieldClass("lblDriverLicense", "txtdriver", "chkdriver");
    addOrRemoveErrorFieldClass("lblSss", "txtsss", "chksss");
    addOrRemoveErrorFieldClass("lblOthers", "txtothers", "chkothers");

    var lblElem = elem('please');
    removeClass(lblElem, "errorField");

    atleastOne();

    if (sUname.value.length <= 7)
    {
        return setFocusAndReturnFalse(sUname);
    }
    else if (sfname.value.trim() == '')
    {
        return setFocusAndReturnFalse(sfname);
    }
    else if (smname.value.trim() == '')
    {
        return setFocusAndReturnFalse(smname);
    }
    else if (slname.value.trim() == '')
    {
        return setFocusAndReturnFalse(slname);
    }
    else if (sscode.value.trim() == '')
    {
        return setFocusAndReturnFalse(sscode);
    }
    else if (acctiveprovider == 3) {
        if (sAuname.value.trim() == '' && acctiveprovider == 3)
        {
            return setFocusAndReturnFalse(sAuname);
        }
    }
//    else if(sApword.value.trim() == '')
//    {
//	return setFocusAndReturnFalse(sApword);
//    }
    else if (shadd.value.trim() == '')
    {
        return setFocusAndReturnFalse(shadd);
    }
    else if (szip.value.trim() == '')
    {
        return setFocusAndReturnFalse(szip);
    }
    else if (stelno.value.trim() == '')
    {
        return setFocusAndReturnFalse(stelno);
    }
    else if (semail.value.trim() == '')
    {
        return setFocusAndReturnFalse(semail);
    }
    else if (!validateEmail(semail.value.trim()))
    {
        return setFocusAndReturnFalse(semail);
    }
    else if (trim(sbday.value.trim()) == '')
    {
        return setFocusAndReturnFalse(sbday);
    }
    else if (isChecked("chkpass") && spassport.value.trim() == '') {
        return setFocusAndReturnFalse(spassport);
    }
    else if (isChecked("chkdriver") && sdlisnse.value.trim() == '') {
        return setFocusAndReturnFalse(sdlisnse);
    }
    else if (isChecked("chksss") && ssss.value.trim() == '') {
        return setFocusAndReturnFalse(ssss);
    }
    else if (isChecked("chkothers") && sothers.value.trim() == '') {
        return setFocusAndReturnFalse(sothers);
    }
    if (spassport.value.length == 0 && sdlisnse.value.trim() == '' && ssss.value.trim() == '' && sothers.value.trim() == '') {
        addClass(lblElem, "errorField");
        return setFocusAndReturnFalse(spassport);
    } else {
        if (acctiveprovider == 3)
        {
            if (sAuname.value.length < 8 && sAuname.value.length != 0 && acctiveprovider == 3)
            {
                var msgtitle = "ERROR";
                var msg = "Agent Username should not be less than 8 character.";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
                return false;
            } else if (validagent == "notvalid" && acctiveprovider == 3) {
                var msgtitle = "ERROR";
                var msg = "Please supply a valid agent username.";
                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                html += '<div class=\"msgLightbox\">';
                html += '<p>' + msg + '</p>';
                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                html += '</div>';
                showLightBox(html);
                return false;
            }
        }
        else {
            document.getElementById('hidden').value = 1;
            document.forms[0].submit();
            return true;
        }
    }
}

/** add by bryan **/
function isChecked(chkId)
{
    var chkElem = elem(chkId);
    if (chkElem.checked) {
        return true;
    }
    return false;
}

function checkinputvoucherC()
{
    //if (document.getElementById("vouchercde").value == '' && document.getElementById("ddldenom").value == 0)
    if (document.getElementById("vouchercde").value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0 && document.getElementById("ddldenom").value == 0)
    {
        document.getElementById('light24').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Please select at least one.";
        return false;
    }
    return true;
}

function checkinputvoucherC2()
{
    if (document.getElementById("remarks").value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('light24').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Please input remarks.";
        return false;
    }
    //void_voucher();
    return true;

}

function checkinputvoucherLO()
{
    if (document.getElementById("ternme").value == '' || document.getElementById("ddlPos").value == 0)
    {
        document.getElementById('light24').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "All fields are required.";
        return false;
    }
    return true;
}
//added from www/sweepscenter/swc_ver3/adminwebtool/controller/loginprocess.php
//  
//      function specialkeys(e)
//    {
//        var k;
//        document.all ? k = e.keyCode : k = e.which;
//        return ((k >= 64 && k < 91) || (k > 96 && k < 123) || k == 8 || e.keyCode == 9 || k == 32 || k == 46 || (e.keyCode > 36 && e.keyCode <= 40) || (k >= 48 && k <= 57));
//    }
//        
//    function alphanumeric(e)
//    {
//        var k;
//        document.all ? k = e.keyCode : k = e.which;
//        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || e.keyCode == 9 || k == 32 || (e.keyCode > 36 && e.keyCode <= 40) || (k >= 48 && k <= 57));
//    }
//
//    function checkLogin_forloginphp()
//    {
//        var oUsername = document.getElementById("txtUsername");
//        var oPassword = document.getElementById("txtPassword");
//        var pattern=/\s/;
//
//        if (oUsername.value.length == 0 || oUsername.value.match(pattern))
//        {
//            document.getElementById('a1').innerHTML = 'INVALID LOG-IN';
//            document.getElementById('b1').innerHTML = 'Please enter your username.';
//            document.getElementById('light1').style.display='block';
//            document.getElementById('fade').style.display='block';
//            document.getElementById('txtUsername').focus();
//            return false;
//        }
//        else if (oPassword.value.length == 0)
//        {
//            document.getElementById('a1').innerHTML = 'INVALID LOG-IN';
//            document.getElementById('b1').innerHTML = 'Please enter your password.';
//            document.getElementById('light1').style.display='block';
//            document.getElementById('fade').style.display='block';
//            document.getElementById('txtPassword').focus();
//            return false;
//        }
//        return true;
//    }
//    
//    function openforgotpw()
//    {
//        document.getElementById('a4').innerHTML = 'RESET PASSWORD CONFIRMATION ';
//        document.getElementById('b4').innerHTML = 'You are about to reset your current password. Please enter your registered e-mail address.';
//        document.getElementById('light4').style.display='block';
//        document.getElementById('fade').style.display='block';
//        return false;
//    }
//    
//    function redirect()
//    {
//        var email = document.getElementById('txtEmail').value;
//        if(email.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
//        {
//            document.getElementById('a4').innerHTML = "BLANK E-MAIL";
//            document.getElementById('b4').innerHTML = "Please enter your e-mail address.";
//            document.getElementById('fade').style.display='block';
//            return false;
//        }
//        if(!checkemail(email))
//        {
//            document.getElementById('a4').innerHTML = "INVALID E-MAIL";
//            document.getElementById('b4').innerHTML = "You have entered an invalid e-mail address. Please try again.";
//            document.getElementById('fade').style.display='block';
//            return false;
//        }
//        return true;
//    }
//    
//    function checkemail(email)
//    {
//        var str=email
//        var filter=/^.+@.+\..{2,3}$/
//
//        if (filter.test(str))
//                return true;
//        else
//                return false;
//        return true;
//        
//        
//    }

/*
 
 Added by: Angelo Niño G. Cubos
 Date created: November 9, 2012 
 
 */

function AlphaNumericOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if (/*DisableWhiteSpaces*/(charCode == 32) || /*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;

    return true;
}
function AlphaOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if (/*DisableWhiteSpaces*/(charCode == 32) || /*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
    // returns false if a numeric character has been entered
    return (chCode < 48 /* '0' */ || chCode > 57 /* '9' */);
    return (chCode == 32);

    return true;
}
function AlphaAndSpaces(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if (/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
    // returns false if a numeric character has been entered
    return (chCode < 48 /* '0' */ || chCode > 57 /* '9' */);
    return (chCode == 32);

    return true;
}
function OnlyNumbers(event)
{
    var e = event || evt; // for trans-browser compatibility
    var charCode = e.which || e.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;

}
function verifyEmail(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode == 64) || (charCode == 95) || (charCode == 46))/* @=64, _=95, .=46 characters are allowed*/
    {
        return true;
        return true;
    } else {
        if (/*DisableWhiteSpaces*/(charCode == 32) || /*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
            return false;
        return true;
    }
}
function DisableSpacesOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if (/*DisableSpaces*/(charCode == 32))
        return false;

    return true;
}
function chkuname()
{
    var uname = document.getElementById("txtAuname");
    if (uname.value.length < 8 && uname.value.length != 0)
    {
        var msgtitle = "ERROR";
        var msg = "Agent Username should not be less than 8 character.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        //txtAuname.focus();
        $('#txtAuname').css('background-color', 'red');
        return false;
    } else if (uname.value.length == 0) {
        $('#txtAuname').css('background-color', '');
    } else {
        verify_agentuname()
    }


}
//function chkpwd()
//{ 
//      var pwd = document.getElementById("txtApword");
//      
//      if (pwd.value.length < 8 && pwd.value.length != 0)
//      { 
//        var msgtitle = "ERROR";
//        var msg = "Password should not be less than 8 character.";
//        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
//        html += '<div class=\"msgLightbox\">';
//        html += '<p>' + msg + '</p>';
//        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
//        html += '</div>';
//        showLightBox(html);
//        txtApword.focus();
//        $('#txtApword').css('background-color', 'red');
//        return false;	
//      }else{
//          $('#txtApword').css('background-color', '#d6eb75');
//      }
//      if(pwd.value.length == 0){$('#txtApword').css('background-color', '');}
//          
//}
