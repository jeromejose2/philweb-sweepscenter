<?php
/*
 * Created by Jerome Jose
 * Date June 25-2012
 * Purpose : View Conversion Point History
 */
include("../controller/rptpointsconversionhistprocess.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script language="javascript" src="jscripts/datepicker.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script language="javascript" type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
       
    }
</script>
<?php include('header.php')?>
<form name="frmrptpointsconversionhistory" method="POST">
    <div style="width:100%; text-align:center;">
  <table>
    <tr>
      <td class="labelbold2">From:</td>
      <td><?php echo $txtDateFr;?>
        <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
      </td>
            <td class="labelbold2">To:</td>
      <td><?php echo $txtDateTo;?>
        <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
      </td>
       </tr>
    <tr>
      <td colspan="4" class="labelbold2">POS Account Name:&nbsp;&nbsp;
          <?php echo $ddlPos; ?>&nbsp;&nbsp;&nbsp;<?php echo $btnSearch;?>
      </td>
    </tr>
  <?php if(isset($_SESSION['start'])):?>
    <tr>
      <td colspan="5">
       
         <?php
         //include_once('pointsconversionhist.php');
         echo "<br />";
         echo "<table style=\"overflow-x:scroll;\" ><tr><th colspan =\"8\" style=\"height:30px;background-color: #FF9C42; color:#000000;\">POINTS CONVERSION SUMMARY</th></tr>";
         echo "<tr><th class=\"th\">Date of Conversion</th><th class=\"th\">POS Account Name</th><th class=\"th\">Terminal Name</th><th class=\"th\">Transaction Number</th><th class=\"th\">Converted Points</th><th class=\"th\">Equivalent Cards</th><th class=\"th\">Winning Cards</th><th class=\"th\">Winnings</th></tr>";

         if(count($getdetail) > 0)
         {
         echo  "<br/>";
         if($pgcon->SelectedItemTo > count($getdetail1))
         {
         echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".count($getdetail1)." of ".count($getdetail1)."</b></p>";
         }else{
         echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".$pgcon->SelectedItemTo." of ".count($getdetail1)."</b></p>";    
         }
             
         
                for($x=0;$x<count($getdetail);$x++)
                {
                    $mod = $x % 2;
                    
                        $row0 = $getdetail[$x]['TransSummaryID'];
                        $row1 = $getdetail[$x]['Name'];
                        $row2 = $getdetail[$x]['TransDate'];
                        $row3 = $getdetail[$x]['Balance'];
                        $row4 =  $getdetail[$x]['EquivalentCards'];
                        $row5 =  $getdetail[$x]['NoOfWinningCards'];
                        $row6 =  $getdetail[$x]['Winnings'];
                        $row7 =  $getdetail[$x]['Name1'];
                   if ($mod == 0)
                  {
                    echo "<tr style=\"background-color:	#FFF1E6; height:30px;\"><td class=\"td\">".$row2."</td><td class=\"td\">".$row7."</td><td class=\"td\">".$row1."</td><td class=\"td\">".$row0."</td><td class=\"td\">".$row3."</td><td class=\"td\">".$row4."</td><td class=\"td\">".$row5."</td><td class=\"td\">".$row6."</td></tr>";
                  }
                  else
                  {
                      echo "<tr style=\"height:30px;\"><td class=\"td\">".$row2."</td><td class=\"td\">".$row7."</td><td class=\"td\">".$row1."</td><td class=\"td\">".$row0."</td><td class=\"td\">".$row3."</td><td class=\"td\">".$row4."</td><td class=\"td\">".$row5."</td><td class=\"td\">".$row6."</td></tr>";
                  }
                }
            echo "<tr><td colspan=\"8\" align=\"center\">";
            echo "<div>";
            echo  "<br/>"; 
            echo "<b class=\"paging\">$pgHist </b>";
            echo "</div>";
            echo "</td></tr>";
            echo "<tr><td>&nbsp;</td></tr>";
            echo "<tr><td colspan='6' align='center'>
                <b><a href='export_report.php?fn=Points_Conversion_History' class=\"labelbutton2\" style=\"margin-left: 200px;\">DOWNLOAD</a></b>
                </td></tr>";
         }
         else
            {
                echo "<tr><td colspan=\"7\" align=\"center\"><b>No Records Found.</b></td></tr>";
            }
            
            
            echo "</table>";
        ?>
      </td>
    </tr>
    <?php else:?>
    <?php endif;?>
    <?php 
    	unset($_SESSION['report_header']);
	unset($_SESSION['report_values']);
     
         $_SESSION['report_header']=array("\"Date of Conversion\"","\"POS Account Name\"","\"Terminal Name\"","\"Transaction Number\"","\"Converted Points\"","\"Equivalent Cards\"","\"Winning Cards\"","\"Winnings\"");
           for($counter=0;$counter<  count($getdetail1);$counter++)
            {
                    $_SESSION['report_values'][$counter][0] = $getdetail1[$counter]['TransDate'];
                    $_SESSION['report_values'][$counter][1] = $getdetail1[$counter]['Name1'];
                    $_SESSION['report_values'][$counter][2] = $getdetail1[$counter]['Name'];
                    $_SESSION['report_values'][$counter][3] = $getdetail1[$counter]['TransSummaryID'];
                    $_SESSION['report_values'][$counter][4] = $getdetail1[$counter]['Balance'];
                    $_SESSION['report_values'][$counter][5] = $getdetail1[$counter]['EquivalentCards'];
                    $_SESSION['report_values'][$counter][6] = $getdetail1[$counter]['NoOfWinningCards'];
                    $_SESSION['report_values'][$counter][7] = $getdetail1[$counter]['Winnings'];
            }
    ?>
    <tr>
      <td colspan="5">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="5" align="center">
      </td>
    </tr>
  </table>
       <!-- ERROR MESSAGE -->
	<div id="light13" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
		<div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
   		</div>
		<br />
		<br />
		<div id="msg">
  
		</div>
		<br/>
		<br/>
		<br />
		<input id="btnOk" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light13').style.display='none';document.getElementById('fade').style.display='none';" />
	</div>
	<!-- END OF ERROR MESSAGE -->
 </div> 
    
</form>
<script type="text/javascript">

<?php if (isset($errormsg)) : ?>
var msg = "<?php echo $errormsg; ?>";
var msgtitle = "<?php echo $errormsgtitle; ?>";
html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
html += '<div class=\"msgLightbox\">';
html += '<p>' + msg + '</p>';
html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input> ';
html += '</div>';
showLightBox(html);
<?php endif; ?>

</script>
<?php include('footer.php');?>