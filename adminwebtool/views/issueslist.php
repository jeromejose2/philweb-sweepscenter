<?php
/**
 * @author Noel Antonio 03-19-2013
 * @copyright 2013 PhilWeb Corporation
 *  
 */
?>
<style type="text/css">
ul, li {
   list-style-type: square;
   margin-left: 2%;
   padding-top: 1em;
   padding-left: 1em;
   text-indent: 1em;
}

a {
    text-decoration: none;
}
</style>
<?php include("header.php"); ?>
<ul>
<?php if (($_SESSION['accttype'] == 1)  || ($_SESSION['accttype'] == 3)){ ?>
    <li><a href="manualredemptioncheck.php">Check If Terminal Is Subject For Manual Redemption</a><br/></li>
    <li><a href="terminalreset3.php">Check If Terminal Is Subject For Re-crediting</a><br/></li>
    <li><a href="terminalreset2.php">Check If Terminal Is Subject For Balance Reset</a><br/></li>
    <li><a href="checkterminalifnotviewable.php">Terminal Is Not Viewable In The POS</a><br/></li>
<?php } ?>
<?php if (($_SESSION['accttype'] == 1) || ($_SESSION['accttype'] == 4)){ ?>
    <li><a href="terminallogout.php">Terminal Logout</a><br/></li>
<?php } ?>
<?php if (($_SESSION['accttype'] == 1)  || ($_SESSION['accttype'] == 3)){ ?>
    <li><a href="checksimultaneoustrans.php">Check Simultaneous Reload and Withdraw</a><br/></li>
<?php } ?>
<?php if ($_SESSION['accttype'] == 1){ ?>
    <li><a href="checkqueueddecks.php">Check If Queued Deck Is Subject For Discarding</a><br/></li>
    <li><a href="checkdeckconsumption.php">Check Active Deck Consumption</a><br/></li>
<?php } ?>
</ul>    
<?php include("footer.php"); ?>
