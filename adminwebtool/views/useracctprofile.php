<?php
/*
 * Created by : Jerome Jose
 * Date Created : June 14-2012
 * Purpose : view Admin Accounts
 */
require_once("../controller/useracctprofileprocess.php");
require_once("header.php");
?>

<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>

<form id="frmUserProfile" method="post">

    <div id="page">
        <table width="100%">
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div id="fontboldblack"><b>Username:</b>&nbsp;&nbsp;&nbsp;
                        <?php echo $ddlUserName; ?>
                        &nbsp;&nbsp;&nbsp;
                        <?php echo $hidUsername ?>
                        <?php echo $hidAccountID ?>
                        <?php echo $btnSearch; ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <?php if ((!isset($_SESSION['tempuname'])) || ($_SESSION['tempuname'] == "")): ?>
                <tr><td width="750px;"></td></tr>
            <?php else: ?>
                <tr>


                    <td colspan="2">
                        <div id="header">User Account Profile</div>
                    </td>
                </tr>
                <tr style="background-color:#FFF1E6; height:30px;">
                    <td class="fontboldblack" style="width:50%;">&nbsp;&nbsp;Account Number</td>
                    <td style="width:50%;">&nbsp;&nbsp;<?php echo (isset($acctid)) ? $acctid : ""; ?></td>
                </tr>
                <tr style="background-color:#FFF1E6; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Username:</td>
                    <td>&nbsp;&nbsp;<?php echo (isset($uname)) ? $uname : ""; ?></td>
                </tr>
                <tr style="background-color:#FFF1E6; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Date Of Creation:</td>
                    <td>&nbsp;&nbsp;<?php echo (isset($datecreated)) ? $datecreated : ""; ?></td>
                </tr>
                <tr style="background-color:#FFF1E6; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Account Status:</td>
                    <td>&nbsp;&nbsp;<?php echo (isset($status)) ? $status : ""; ?></td>
                </tr>
            </table>
            <br />
            <div id="header2">Account Information</div>
            <div style="text-align:center;">
                <table>
                    <tr>
                        <td class="fontboldblackcenter">First Name</td>
                        <td class="fontboldblackcenter">Middle Name</td>
                        <td class="fontboldblackcenter">Last Name</td>
                    </tr>
                    <tr>
                        <td><?php echo $txtFName; ?>
                        </td>
                        <td><?php echo $txtMName; ?>
                        </td>
                        <td><?php echo $txtLName; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="fontboldblackcenter">E-mail Address</td>
                        <td colspan="2"><?php echo $txtEmail; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="fontboldblackcenter">Position</td>
                        <td colspan="2"><?php echo $txtPosition; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="fontboldblackcenter">Company</td>
                        <td colspan="2"><?php echo $txtCompany; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="fontboldblackcenter">Department</td>
                        <td colspan="2"><?php echo $txtDepartment; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="fontboldblackcenter">Group</td>
                        <td colspan="2">
                            <select id="ddlGroup" disabled>
                                <option><?php echo (isset($groupname)) ? $groupname : ''; ?></option>
                            </select>
                            <input type="hidden" id="hidGroup" name="hidGroup" value =""></input>
                        </td>
                    </tr>
                    <tr>
                        <td class="fontboldblackcenter">Status</td>
                        <td colspan="2"><?php echo $txtStatus; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <?php if ($_SESSION['accttype'] == 1 || $_SESSION['accttype'] == 2): ?>
                                <?php echo $btnEdit; ?>
                            <?php endif; ?>
                        </td>
                    <?php endif; ?>
                </tr>
            </table>



            <!-- ERROR MESSAGE(No Username Selected) -->
            <div id="light6" style="text-align: center;font-size: 16pt;height: 220px; margin-left:-95px;" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">
                    Error Message
                </div>
                <br />
                <br />
                Please select a username.
                <br/>
                <br/>
                <br />
                <!--<input id="btnOkClose" type="button" value="OK" class="labelbutton2" onclick="document.getElementById('light6').style.display='none';document.getElementById('fade').style.display='none'; return redirect2('useracctprofile');<?php //$_SESSION['tempuname'] = ''; ?>" />-->
                <input id="btnOkClose" type="button" value="OK" class="labelbutton2" />
            </div>
            <!-- END OF ERROR MESSAGE(No Username Selected) -->	
            <br />
        </div>
    </div>
</form>

<script type="text/javascript" lang="Javascript">
    $('#btnSearch').live('click', function() {
        if (document.getElementById('ddlUsername').value == 0)
        {
            //document.getElementById('light6').style.display='block';
            //document.getElementById('fade').style.display='block';
            var msgtitle = "Error Message!";
            var msg = "Please select a username.";
            html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
            html += '<div class=\"msgLightbox\">';
            html += '<p>' + msg + '</p>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
            html += '</div>';
            showLightBox(html);
            return false;
        }
        else
        {
            return true;
        }
    });

    $('#btnOkClose').live('click', function() {
        $('#light6').fadeOut('slow');
        $('#fade').fadeOut('slow');
    });

    $('#ddlUsername').change(function() {
        var username = $('#ddlUsername option:selected').text();
        $('#hidUsername').val(username);
    });

    function redirect() {
        var username = $('#ddlUsername option:selected').text();
        $('#hidUsername').val(username);
        window.location = 'edituseracct.php';
        //window.location = 'template.php?page=edituseracct?hidUsername=' + document.getElementById('hidUsername').value;
    }

</script>
<?php include("footer.php"); ?>



