<?php

/*
 * Added On: March 25, 2013
 * Created By: Frances Ralph DL. Sison 
 * Purpose: Tool for Checking for Queued Decks
 * Reason: Checking to know if a deck is subject to/for discarding or not.
 */
include('../controller/checkqueueddecksprocess.php');

?>

<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script type="text/javascript" src="jscripts/jquery-1.5.2.min.js" ></script>
<script type="text/javascript" src="jscripts/jquery-ui-1.8.13.custom.min.js" ></script>
<script language="javascript" src="jscripts/datepicker.js" type="text/javascript"></script>
<script language="javascript" src="jscripts/checkinputs.js" type="text/javascript"></script>

<?php include('header.php');?>
<script type="text/javascript">
    function checkinput()
    {
        var deckacctid = document.getElementById('deckidbox').options[document.getElementById('deckidbox').selectedIndex].value;
        
        if(deckacctid == "" || deckacctid == '0' || deckacctid == null)
        {
            var msgtitle = "INSUFFICIENT DATA";
            var msg = "Please enter Deck ID.";
            html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
            html += '<div class=\"msgLightbox\">';
            html += '<p>' + msg + '</p>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
            html += '</div>';
            showLightBox(html);
            return false;
        }
        
        return true;
    }
</script>
<h3><b>Check If Queued Deck is Subject For Discarding</b></h3>
<br />
<br />
<form name="frmcheckqueueddecks" method ="POST"> 
<div align="center" style="margin-left: 10px; margin-top: 5px;">
    <table>
        <tr> <?php echo $hidlist;?>
            <td class="labelbold2">Deck ID:</td>
            <td><?php echo $deckidbox;?></td><td colspan="3" align="center"><?php echo $btnSearch; ?></td>
        </tr>
    </table>
    <br />
    
    <!-- Display Record-->
    <?php if ($display):?>
    <br /><hr>
    <h3 align="left">Deck Details</h3>
    <table>
        <tr style="background-color:#FF9C42; height:30px; text-align: center;">
            <th style="width:400px;">Deck ID</th>
            <th style="width:400px;">Control Number</th>
            <th style="width:400px;">Card Count</th>
            <th style="width:400px;">Card Count with ECN</th>
            <th style="width:400px;">Date Created</th>
            <th style="width:400px;">Created By</th>
        </tr>
        <tr style="background-color:#FFF1E6; height:30px; text-align: center;">
            <td style="width:400px;"><?php echo $deckidname?></td>
            <td style="width:400px;"><?php echo $deckcontrolno?></td>
            <td style="width:400px;"><?php echo $deckcardcount?></td>
            <td style="width:400px;"><?php echo $deckcardcountecn?></td>
            <td style="width:400px;"><?php echo $deckdatecreated?></td>
            <td style="width:400px;"><?php echo $deckcreatedby?></td>

        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr> 
    </table>
    <br/>
    <div id ="rec"><b>Recommendation: <?php echo $recommendation?></b></div>
    <?php endif;?>
<!--    </table>-->
    <!-- END OF NOTIFICATION MESSAGE -->
</div>    
</form>
<?php include('footer.php');?>