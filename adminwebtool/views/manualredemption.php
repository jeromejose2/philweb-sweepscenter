<?php
/*
 * Created By: Jerome F. Jose 6-29-2012
 * Purpose: Tool for Manual Redemption
 * Modified By: Noel Antonio
 * Date Modified: September 19, 2012
 * Reason: To cover all deposit, reload and withdraw in the manual redeem.
 */
include('../controller/manualredemptionprocess.php');

?>

<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script type="text/javascript" src="jscripts/jquery-1.5.2.min.js" ></script>
<script type="text/javascript" src="jscripts/jquery-ui-1.8.13.custom.min.js" ></script>
<script language="javascript" src="jscripts/datepicker.js" type="text/javascript"></script>
<script language="javascript" src="jscripts/checkinputs.js" type="text/javascript"></script>
<?php include('header.php');?>

<script type="text/javascript">
    function get_terminals()
    {
        var acctid = document.getElementById('ddlPosName').options[document.getElementById('ddlPosName').selectedIndex].value;

        $("#ddlTerminalName").load(
            "get_terminal.php",
            {
                sid : acctid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title23').innerHTML = "ERROR!";
                    document.getElementById('msg23').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light23').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }

    function checkmanualredemption()
    {
        var posacctid = document.getElementById('ddlPosName').options[document.getElementById('ddlPosName').selectedIndex].value;
        var amnt = document.getElementById('txtAmount').value;
        var terminalid = document.getElementById('ddlTerminalName').options[document.getElementById('ddlTerminalName').selectedIndex].value;
        //var transrefid = document.getElementById('txtTransRefID').value;
        //var periodlocation = amnt.indexOf('.');
        //var numbersAfterPeriod = amnt.substr(periodlocation + 1 , amnt.length);
        if(posacctid == "")
        {
            var msgtitle = "INSUFFICIENT DATA";
            var msg = "Please enter data on the search filters.";
            html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
            html += '<div class=\"msgLightbox\">';
            html += '<p>' + msg + '</p>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
            html += '</div>';
            showLightBox(html);
            return false;
        }
        if(terminalid == "0" || amnt == "")
        {
             var msgtitle = "INSUFFICIENT DATA";
            var msg = "Please enter data on the search filters.";
            html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
            html += '<div class=\"msgLightbox\">';
            html += '<p>' + msg + '</p>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
            html += '</div>';
            showLightBox(html);
            return false;
        }
//        if(transrefid.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
//        {
//            var msgtitle = "INSUFFICIENT DATA";
//            var msg = "Please enter data on the search filters.";
//            html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
//            html += '<div class=\"msgLightbox\">';
//            html += '<p>' + msg + '</p>';
//            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
//            html += '</div>';
//            showLightBox(html);
//            return false;
//        }
        if(amnt.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
        {
            var msgtitle = "INSUFFICIENT DATA";
            var msg = "Please enter data on the search filters.";
            html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
            html += '<div class=\"msgLightbox\">';
            html += '<p>' + msg + '</p>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
            html += '</div>';
            showLightBox(html);
            return false;
        }
        else if (amnt == 0)
        {
            var msgtitle = "INSUFFICIENT DATA";
            var msg = "Amount must be greater than zero.";
            html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
            html += '<div class=\"msgLightbox\">';
            html += '<p>' + msg + '</p>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
            html += '</div>';
            showLightBox(html);
            return false;
        }
//        if(numbersAfterPeriod.length != 2)
//        {
//            var msgtitle = "INVALID INPUT";
//            var msg = "Please enter the amount in this format: 0.00";
//            html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
//            html += '<div class=\"msgLightbox\">';
//            html += '<p>' + msg + '</p>';
//            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
//            html += '</div>';
//            showLightBox(html);
//            return false;
//        }
        return true;
    }

    function disablealphakeys(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if((charCode == 8) || (charCode == 46) || (charCode > 47 && charCode < 58))
            return true;

        return false;
    }
  function displayconfirmation()
  {
         document.getElementById('light24').style.display = "block";
        document.getElementById('fade').style.display = "block";
  }
</script>
<form name="frmmanualRedemPtion" method ="POST">
    </script> 
<div style="width:100%; text-align:center;">
    <table width="100%">
        <tr> <?php echo $hidlist;?>
            <td class="labelbold2">POS Account Name:</td>
            <td><?php echo $ddlPosName;?></td>
            
            <!--<td class="labelbold2">Transaction Ref. ID:</td>
            <td>  
                <?php //echo $stransrefid1;?>
            </td>-->
        </tr>
        <tr>
            <td class="labelbold2">Terminal Name:</td>
            <td><?php echo $ddlTerminalName;?></td>
        </tr>
        <tr>
            <td class="labelbold2">Amount:</td>
            <td><?php echo $txtamount;?></td>
            <!--<p style="font-size: 10px;">*Please provide amount in the following format: 0.00</p>-->
            <td colspan="4" align="left">
                <?php echo $btnSearch; ?>
            </td>
        </tr>
    </table>
    
     <!-- Display Record-->
    <?php if ($display):?>
    <table width="100%">
        <tr>
<!--            <th class="th">Transaction Date</th>-->
            <th class="th">POS Account Name</th>
            <th class="th">Terminal Name</th>
            <th class="th">Amount</th>
<!--            <th class="th">Transaction Reference ID</th>-->
        </tr>
        <tr style="background-color:#FFF1E6; height:30px;">
<!--            <td class="td"><?php //echo $dateprocessed?></td>-->
            <td class="td"><?php echo $posaccountname?></td>
            <td class="td"><?php echo $terminalname?></td>
            <td class="td"><?php echo $amount?></td>
<!--            <td class="td"><?php //echo $transactionRefID?></td>-->
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7" align="right">
                <?php echo $btnRedeem;?>
            </td>
        </tr> 
     
        <?php
                /*for($x=0;$x<count($listManualRedemption);$x++)
                {
                    $posaccountname = $listManualRedemption[$x]['PosAccountName'];
                    $terminalname = $listManualRedemption[$x]['Name'];
                    $terminalsessionID = $listManualRedemption[$x]['TerminalSessionID'];
                    $dateprocessed = $listManualRedemption[$x]['DateProcessed'];
                    $transactionType = $listManualRedemption[$x]['TransactionType'];
                    $amount = $listManualRedemption[$x]['Amount'];
                    $transactionRefID = $listManualRedemption[$x]['ServiceTransactionID'];
                    $name1 = $listManualRedemption[$x]['Name1'];
                    $_SESSION['mr_Amount'] = $amount;
                    $_SESSION['mr_TerminalName'] = $terminalname;
                    $_SESSION['mr_DateCreated'] = $dateprocessed;

                    switch($transactionType)
                    {
                        case 'D':
                        $type = "Deposit";
                        break;
                        case 'R':
                        $type = "Reload";
                        break;
                        case 'W':
                        $type = "Withdrawal";
                        break;
                    }
                    }
                    */
        ?>              
        <?php endif;?>
    </table>
      <div id="light24" style="text-align: center;font-size: 16pt;height: auto;width:500px;" class="white_content">
        <div id="title24" style="width: p00px;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">
            CONFIRMATION
        </div>
        <br />
        <div id="msg24">
                You are about to manually redeem amount of <?php echo $_SESSION['mr_Amount']?> on <?php echo $_SESSION['mr_TerminalName']?>. Do you wish to continue?
        </div>
        <br/>
       <?php echo $btnProcess;?>
        <input id="btnOk24" type="button" value="CANCEL" class="labelbold2" onclick="document.getElementById('light24').style.display='none';document.getElementById('fade').style.display='none';" />
    </div>
    <!-- END OF NOTIFICATION MESSAGE -->
</div>
<script>
<?php if(isset($returnmsg)):?>
	
        <?php if($status == 0):?>
        var msgtitle = "SUCCESSFUL REDEMPTION";
        <?php else:?>
        var msgtitle = "ERROR!";
        <?php endif;?>
        var msg = "<?php echo $returnmsg?>";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
	
<?php endif;?>
 </script>
    
</form>
<?php include('footer.php');?>
