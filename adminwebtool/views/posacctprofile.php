<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 18, 2012
 * Purpose          :       POS Account Profile: Viewing of POS Account
 */

include("../controller/posacctprofileprocess.php");

function getStatus($id) {
   $stat = array('' =>'',1=>'Active',2=>'Suspended',3=>'Terminated');
   return $stat[$id];
}
?>
<?php include("header.php"); ?>
<style type="text/css">
    .hasSpace {
        border-collapse: collapse;
    }
    
    .hasSpace td {
        padding-top: .3em;
        padding-bottom: .3em;
    }
</style>
<script type="text/javascript">
    function SelectPOSAccountName()
    {
        var acctid = document.getElementById('ddlacctno').options[document.getElementById('ddlacctno').selectedIndex].value;
        $("#ddlacctname").load(
            "../controller/get_posaccountname.php",
            {
                acctid: acctid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    
    function SelectPOSAccountNumber()
    {
        var acctname = document.getElementById('ddlacctname').options[document.getElementById('ddlacctname').selectedIndex].value;
        $("#ddlacctno").load(
            "../controller/get_posaccountid.php",
            {
                acctname: acctname
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
</script>

<form id="frmpostacctprofile" name="frmpostacctprofile" method="post" action="posacctprofile.php">
    <div align="center" style="margin-top: 5px;">
        <table>
            <tr>
                <td><b>Account Name:</b></td>
                <td><?php echo $ddlacctname; ?></td>
            </tr>
            <tr>
                <td><b>Account Number:</b></td>
                <td><?php echo $ddlacctno; ?></td>
            </tr>
        </table>
        <br/>
        <?php echo $btnSubmit; ?>
    </div>
</form>

<?php if (isset($array)): ?>
    <br/><hr>
    <div style="margin-left: 10px; margin-top: 5px;">
    <form id="frm" method="POST" action="edit_posacctprofile.php">
        <?php echo $hiddenid; ?>
        <table>
            <tr>
                <td>ACCOUNT NAME:</td><td><b><?php echo $array[0]; ?></b></td>
            </tr>
            <tr>
                <td>ACCOUNT NUMBER:</td><td><b><?php echo $array[1]; ?></b></td>
            </tr>
	     <tr>
                <td>TERMINALS:</td><td><b><?php echo $array[33]; ?></b></td>
            </tr>
  	     <tr>
                <td>FREE ENTRY TERMINALS:</td><td><b><?php echo $array[34]; ?></b></td>
            </tr>
	     <tr>
                <td>ACCOUNT STATUS:</td><td><b><?php echo getStatus($array[31]); ?></b></td>
            </tr>
        </table>
        <br/>
        
        <div style="font-weight: bold;"><u>ACCOUNT INFORMATION</u></div>
        <table class="hasSpace" style="margin-top: 15px;">
            <tr>
                <td>*Cashier Username:&nbsp;&nbsp;</td><td><?php echo $array[2]; ?></td>
            </tr>
            <tr><td>*Name of Operator:</td></tr>
            <tr>
                <td>First Name</td>
                <td>Middle Name</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;Last Name</td>
            </tr>
            <tr>
                <td><?php echo $array[4]; ?></td>
                <td><?php echo $array[5]; ?></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $array[3]; ?></td>
            </tr>
            <tr>
                <td>*SWC Code: </td><td><?php echo $array[6]; ?></td>
            </tr>
            <?php if($_SESSION['activeprovider'] == 3){//1 for RTG ?>             
            <tr>
                <td>*Agent Username: </td><td><?php echo $array[35]; ?></td>
            </tr>
            <?php }?> 
        </table>
        
        <table style="margin-top: 10px;">
            <tr>
                <td>*Home Address:&nbsp;&nbsp;</td><td><?php echo $array[7]; ?></td>
            </tr>
        </table>
        
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td>*Zip Code:</td><td><?php echo $array[8]; ?></td>
            </tr>
            <tr>
                <td>*Telephone No.:&nbsp;&nbsp;</td><td><?php echo $array[9]; ?></td>
            </tr>
        </table>
        
        <div style="font-weight: bold; margin-top: 10px;"><u>ADDITIONAL INFORMATION</u></div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td style>Mobile No.:</td><td><?php echo $array[10]; ?></td>
            </tr>
            <tr>
                <td>Fax No.:</td><td><?php echo $array[11]; ?></td>
            </tr>
            <tr>
                <td>*E-mail Address:</td><td><?php echo $array[12]; ?></td>
            </tr>
            <tr>
                <td>Website Address:</td><td><?php echo $array[13]; ?></td>
            </tr>
            <tr>
                <td>Country of Residence:</td><td><?php if($array[14] == 1){echo "Guam"; }else{ echo "";} ?></td>
            </tr>
            <tr>
                <td>Citizenship:</td><td><?php echo $array[15]; ?></td>
            </tr>
            <tr>
                <td>*Date of Birth:</td><td><?php echo $array[16]; ?></td>
            </tr>
        </table>
        
        <div style="font-weight: bold; margin-top: 10px;">*Please provide at least one form of identification.</div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td><input type="checkbox" <?php if($array[17] != ""){echo "checked";}?> disabled/><td></td><td>Passport:</td><td><?php echo $array[17]; ?></td>
            </tr>
            <tr>
                <td><input type="checkbox" <?php if($array[18] != ""){echo "checked";}?> disabled/></td><td></td><td>Driver&#39;s License:</td><td><?php echo $array[18]; ?></td>
            </tr>
            <tr>
                <td><input type="checkbox" <?php if($array[19] != ""){echo "checked";}?> disabled/></td><td></td><td>Social Security ID:</td><td><?php echo $array[19]; ?></td>
            </tr>
            <tr>
                <td><input type="checkbox" <?php if($array[20] != ""){echo "checked";}?> disabled/></td><td></td><td>Others:</td><td><?php echo $array[20]; ?></td>
            </tr>
        </table>
        
        <div style="font-weight: bold; margin-top: 10px;"><u>BANK INFORMATION</u></div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td>Bank:</td><td><?php echo $array[21]; ?></td>
            </tr>
            <tr>
                <td>Bank Account Type:</td><td><?php echo $array[22]; ?></td>
            </tr>
            <tr>
                <td>Bank Branch:</td><td><?php echo $array[23]; ?></td>
            </tr>
            <tr>
                <td>Bank Account Name:</td><td><?php echo $array[24]; ?></td>
            </tr>
            <tr>
                <td>Bank Account Number:</td><td><?php echo $array[25]; ?></td>
            </tr>
        </table>
        
        <div style="font-weight: bold; margin-top: 10px;"><u>PAYEE BANK INFORMATION</u></div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td>Bank:</td><td><?php echo $array[26]; ?></td>
            </tr>
            <tr>
                <td>Bank Account Type:</td><td><?php echo $array[27]; ?></td>
            </tr>
            <tr>
                <td>Bank Branch:</td><td><?php echo $array[28]; ?></td>
            </tr>
            <tr>
                <td>Bank Account Name:</td><td><?php echo $array[29]; ?></td>
            </tr>
            <tr>
                <td>Bank Account Number:</td><td><?php echo $array[30]; ?></td>
            </tr>
        </table>
        <br/>
        
        <?php if($_SESSION['accttype'] == 1 || $_SESSION['accttype'] == 2): ?>
            <div align="center"><?php echo $btnEdit; ?></div>
        <?php endif; ?>
        <br/>
    </form>
    </div>
    <?php endif; ?>
<script type="text/javascript">
$(document).ready(function(){
   $('#btnSubmit').click(function(){
      if($('#ddlacctname option:selected').val() == 'Select One') {
         html = '<div class=\"titleLightbox\">Notification</div><br />';
         html += '<div class=\"msgLightbox\">';
         html += '<p>Please select an account name or account number.</p>';
         html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
         html += '</div>';
         showLightBox(html);
         return false;
      }
      return true;
  });
  
  $('.edit').live('click',function(){
     $('#hiddenid').val($('#ddlacctname').val());
  });
});
</script>
<?php include("footer.php"); ?>