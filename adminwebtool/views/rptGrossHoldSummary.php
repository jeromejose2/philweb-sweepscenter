<?php
/*
 * Gross Hold Summary
 * 
 * View page of the report
 * 
 * @author: Jerome F. Jose
 * @date created : June 25, 2012
 * 
 * last updated by: Noel Antonio
 * date updated: February 6, 2013
 * purpose: includes free entry transactions, fix downloads, total cash on hand
 * and total gross holds computations.
 * 
 */

include("../controller/rptGrossHoldSummaryprocess.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script type="text/javascript" src="jscripts/jquery-1.5.2.min.js" ></script>
<script type="text/javascript" src="jscripts/jquery-ui-1.8.13.custom.min.js" ></script>
<script language="javascript" src="jscripts/datepicker.js" type="text/javascript"></script>
<script language="javascript" src="jscripts/checkinputs.js" type="text/javascript"></script>
<script type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
    function get_terminals()
    {
        var acctid = document.getElementById('ddlPosName').options[document.getElementById('ddlPosName').selectedIndex].value;

        $("#ddlTerminalName").load(
            "get_terminal.php",
            {
                sid : acctid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title23').innerHTML = "ERROR!";
                    document.getElementById('msg23').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light23').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }

    function checkrptmanualredemption()
    {
        var posacctid = document.getElementById('ddlPosName').options[document.getElementById('ddlPosName').selectedIndex].value;
        var terminalid = document.getElementById('ddlTerminalName').options[document.getElementById('ddlTerminalName').selectedIndex].value;

        if(posacctid != 0)
        {
            if(terminalid == "")
            {
                document.getElementById('title23').innerHTML = "ERROR!";
                document.getElementById('msg23').innerHTML = "Please select Terminal Name.";
                document.getElementById('light23').style.display = "block";
                document.getElementById('fade').style.display = "block";
                return false;
            }
        }
        
        return true;
    }
</script>
<?php include('header.php') ?>
<form name="frmgrossholdsummary" method="POST">
    <div style="width:100%; text-align:center;">
    <table width="100%">
        <tr>
            <td class="labelbold2">From:</td>
            <td><?php echo $txtDateFr;?>
                <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
            </td>
             <td class="labelbold2">To:</td>
            <td><?php echo $txtDateTo;?>
                <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
            </td>
              </tr>
        <tr>
            <td class="labelbold2" colspan="5">POS Account Name:
                <?php echo $ddlPos;?>
                 </td>
                     <td>
                         <?php echo $btnSearch;?>
            </td>
               </tr>
    </table>
       
  <?php if(isset($_SESSION['start'])):?>
        <table>
           <tr>
                <th class="th">Date and Time In</th>
                <th class="th">Date and Time Out</th>
                <th class="th">POS Account Name</th>
                <th class="th">Terminal Name</th>
                <th class="th">Initial Deposit</th>
                <th class="th">Total Reloads</th>
                <th class="th">Cash Redemptions</th>
                <th class="th">Non-Cash Redemptions Value</th>
                <th class="th">Gross Hold</th>
            </tr>
            <?php
                if(count($arr_records) > 0)
                {
                        echo  "<br/>";
                        if($pgcon->SelectedItemTo > count($getgrossholdlist1))
                            echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".count($getgrossholdlist1)." of ".count($getgrossholdlist1)."</b></p>";
                        else
                            echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".$pgcon->SelectedItemTo." of ".count($getgrossholdlist1)."</b></p>";

                        $cash_grandtotal = 0;
                        $deposit_grandtotal = 0;
                        $reload_grandtotal = 0;
                        $grosshold_grandtotal = 0;
                        for($x = 0; $x < count($arr_records); $x++) {
                                if(($x % 2) == 0) { ?>
                                    <tr style="background-color:#FFF1E6; height:30px;">
                                <?php } else { ?>
                                    <tr style="height:30px;">
                                <?php } ?>
                                        <td class="td"><?php echo $arr_records[$x][0];?></td>
                                        <td class="td"><?php echo $arr_records[$x][1];?></td>
                                        <td class="td"><?php echo $arr_records[$x][2];?></td>
                                        <td class="td"><?php echo $arr_records[$x][5];?></td>
                                        <td class="td"><?php echo $arr_records[$x][3];?></td>
                                        <td class="td"><?php echo $arr_records[$x][4];?></td>
                                        <td class="td"><?php echo number_format($arr_records[$x][6] , 2 , '.' , ',');?></td>
                                        <td class="td"><?php echo $arr_records[$x][8];?></td>
                                        <td class="td"><?php echo number_format($arr_records[$x][9] , 2 , '.' , ',');?></td>
                                    </tr>
                                    
                        <?php
                                }
                                
                                // For retrieving Total Cash On Hand and Total Gross Hold
                                for($x = 0; $x < count($arr_records1); $x++) {
                                    $cash_grandtotal = ($cash_grandtotal + $arr_records1[$x][6]);
                                    $deposit_grandtotal = ($deposit_grandtotal + $arr_records1[$x][3]);
                                    $reload_grandtotal = ($reload_grandtotal + $arr_records1[$x][4]);
                                    $grosshold_grandtotal = ($grosshold_grandtotal + $arr_records1[$x][9]);
                                }
                        ?>
                        <?php  $cash_on_hand = (($deposit_grandtotal + $reload_grandtotal) - $cash_grandtotal); ?>
                        <?php if(($pgcon->SelectedItemFrom-1 + count($getgrossholdlist)) == count($getgrossholdlist1)): ?>
			<tr style="background-color:#FFF1E6; height:30px;">
				<td colspan="4">&nbsp;</td>
				<td colspan="5" align="left"><b>Total Sweeps Center Cash On Hand: <?php echo number_format($cash_on_hand , 2 , '.' , ',');?></b></td>
			</tr>
			<tr style="height:30px;">
				<td colspan="4">&nbsp;</td>
				<td colspan="5" align="left"><b>Total Sweeps Center Gross Hold: <?php echo number_format($grosshold_grandtotal , 2 , '.' , ',');?></b></td>
			</tr>
			<?php endif;?>   
                        <tr>
                            <td colspan="5" align="center">
                            <?php   
                                    echo "<div>";
                                    echo "</div>"; 
                                    echo "</td></tr><tr><td colspan=\"9\" align=\"center\"></td></tr><tr><td colspan=\"9\" align=\"center\">";
                                    echo "<p class=\"paging\">$pgHist </p>";
                                    echo  "<br/>";
                                    echo $download;
                                    //echo "<b><a href='export_report.php?fn=Gross_Hold_Summary' class=\"labelbutton2\">DOWNLOAD</a></b></td></tr>";
                            ?>
                            </td>
                        </tr>
            <?php } else { ?>
                <tr>
                    <td colspan="9" align="center"><b>No Records Found.</b></td>
                </tr>
            <?php } ?>
            <?php endif;?>
        </table>
</form>
<script type="text/javascript">

<?php if (isset($errormsg)) : ?>
var msg = "<?php echo $errormsg; ?>";
var msgtitle = "<?php echo $errormsgtitle; ?>";
html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
html += '<div class=\"msgLightbox\">';
html += '<p>' + msg + '</p>';
html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input> ';
html += '</div>';
showLightBox(html);
<?php endif; ?>

</script>
<?php include('footer.php') ?>








