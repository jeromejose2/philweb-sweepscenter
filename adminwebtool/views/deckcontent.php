<?php
/*
 * Created By       :       Angelo Cubos
 * Date Created     :       March 13 2013
 * Purpose          :       Viewing for deck table details
 */
include("../controller/deckcontentprocess.php");
?>
<style>
    div.white_content2{
    position:fixed;
    top: 50%;
    left: 50%;
    margin-top: -9em; /*set to a negative number 1/2 of your height*/
    margin-left: -15em; /*set to a negative number 1/2 of your width*/
    }
    h3.red{
    color:red;
    }    
</style>

<?php include("header.php"); ?>
<div align="center" style="margin-left: 10px; margin-top: 5px;">
<h3 align="left">Check Deck Contents</h3><br>    
    <table align="left">
        <tr>
            <td><b>Deck ID:</b></td>
            <td><?php echo $ddldeckid; ?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $btnSubmit; ?></td>
        </tr>
    </table>
    <br/>
    <div>
        <br/><hr>
        <table>
            <tr style="background-color:#FF9C42; height:30px; width:1200px;">
                <th style="width:100px; text-align: center;">Deck ID</th>
                <th style="width:100px; text-align: center;">Control Number</th>
                <th style="width:100px; text-align: center;">Card Count</th>
                <th style="width:100px; text-align: center;">No. of Valid ECN's</th>
                <th style="width:190px; text-align: center;">Date Created</th>
                <th style="width:190px; text-align: center;">Date Activated</th>
                <th style="width:190px; text-align: center;">Date Closed</th>
                <th style="width:100px; text-align: center;">Created By</th>
                <th style="width:130px; text-align: center;">Status</th>
            </tr>
            <tr style="background-color:#FFF1E6; height:30px; width:1200px;">
                <td style="widtd:100px; text-align: center;"><?php echo $DeckID?></td>
                <td style="widtd:100px; text-align: center;"><?php echo $ControlNo?></td>
                <td style="widtd:100px; text-align: center;"><?php echo $CardCount?></td>
                <td style="widtd:100px; text-align: center;"><?php echo $ValidECN?></td>
                <td style="widtd:190px; text-align: center;"><?php echo $DateCreated?></td>
                <td style="widtd:190px; text-align: center;"><?php echo $DateActivated?></td>
                <td style="widtd:190px; text-align: center;"><?php echo $DateClosed?></td>
                <td style="widtd:100px; text-align: center;"><?php echo $CreatedByAID?></td>
                <td style="widtd:130px; text-align: center;">
                <?php 
                if($Status == '0'){echo "Queued";}
                if($Status == '1'){echo "Active";}
                if($Status == '2'){echo "Depleted";}
                if($Status == '3'){echo "Deactivated";}
                ?>
                </td>
            </tr>
        </table>
    </div>
  
</div>
<div id="recommendation">
<h3 class="red" align="center">Recommendation: Deck is subject for discarding.</h3>  
</div>
<script type="text/javascript">
$('#recommendation').hide();

<?php if (isset($Status)): ?>
        <?php if ($Status == '0' && $ValidECN == '0'): ?>
          $('#recommendation').show();
        <?php endif; ?>
<?php endif; ?>
</script>
<?php include("footer.php");?>