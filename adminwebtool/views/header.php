<?php
/*
 * Created By   :           Noel D. Antonio
 * Created On   :           June 14, 2012
 * Purpose      :           Header Page
 */
require_once("../init.inc.php");
App::LoadModuleClass("SweepsCenter", "SCC_AccountSessions");
App::LoadModuleClass("SweepsCenter", "SCC_AdminAccountSessions");
App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");


$cacctsessions = new SCC_AccountSessions();
$cadminacctsessions = new SCC_AdminAccountSessions();
$caudittrail = new SCC_AuditTrail();


/* -- START OF MANAGE SESSION -- */
if (isset($_SESSION['sid']) || isset($_SESSION['aid']))
{
    $sessiondtls = $cadminacctsessions->GetSessionDetails($_SESSION['sid']);
    if (count($sessiondtls) > 0)
    {
        $currtime = $sessiondtls[0]["CurrTime"];
        $timestart = $sessiondtls[0]["TransDate"];
        $id = $sessiondtls[0]["ID"];
    }
    else
    {
        unset($_SESSION['sid']);
        unset($_SESSION['aid']);
        URL::Redirect('login.php');
    }

    $boolrollback = false;
    $cadminacctsessions->StartTransaction();

    $timediff = (strtotime($currtime) - strtotime($timestart)) / 60;
    if ($timediff >= 20)
    {
        // end active session
        $updsession["DateEnded"] = 'now_usec()';
        $updsession["ID"] = $id;
        $cadminacctsessions->UpdateByArray($updsession);
        if ($cadminacctsessions->HasError)
        {
            $boolrollback = true;
        }

        // log to audit trail
        $caudittrail->StartTransaction();
        $scauditlogparam["SessionID"] = $_SESSION['sid'];
        $scauditlogparam["AccountID"] = $_SESSION['aid'];
        $scauditlogparam["TransDetails"] = "Logout: " . $_SESSION['uname'];
        $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
        $scauditlogparam["TransDateTime"] = "now_usec()";
        $caudittrail->Insert($scauditlogparam);
        if ($caudittrail->HasError)
        {
            $boolrollback = true;
        }

        // commit or rollback transactions
        if ($boolrollback)
        {
            $caudittrail->RollBackTransaction();
            $cadminacctsessions->RollBackTransaction();
        }
        else
        {
            $caudittrail->CommitTransaction();
            $cadminacctsessions->CommitTransaction();
            session_destroy();
            URL::Redirect('login.php');
        }
    }

    $updateSession["ID"] = $id;
    $updateSession["TransDate"] = 'now_usec()';
    $cadminacctsessions->UpdateByArray($updateSession);
    if ($cadminacctsessions->HasError)
        $cadminacctsessions->RollBackTransaction();
    else
        $cadminacctsessions->CommitTransaction();
}
else
{
    URL::Redirect('login.php');
}
/* -- END OF MANAGE SESSION -- */


$project_title = "Guam Sweepstakes Caf&#233; Admin Tool";
$date_time = $cacctsessions->selectNow();
$serverdatetime = $date_time[0][0];

$stylesheets[] = "css/default.css";
$stylesheets[] = "css/datepicker.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$javascripts[] = "jscripts/checkinputs.js";
$javascripts[] = "jscripts/leftmenu.js";
$javascripts[] = "jscripts/lightbox.js";
$javascripts[] = "jscripts/jquery-ui-1.8.13.custom.min.js";
$javascripts[] = "jscripts/datepicker.js";
//$stylesheets[] = "css/base/jquery.ui.all.css";
$headerinfo = "";
for ($i = 0; $i < count($javascripts); $i++)
{
    $js = $javascripts[$i];
    $headerinfo .= "<script language='javascript' type='text/javascript' src='$js'></script>";
}
for ($i = 0; $i < count($stylesheets); $i++)
{
    $css = $stylesheets[$i];
    $headerinfo .= "<link rel='stylesheet' type='text/css' media='screen' href='$css' />";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script type="text/javascript">
            function preventBackandForward()
            {
                window.history.forward();
            }

            preventBackandForward();
            window.onload = preventBackandForward();
            window.inhibited_load = preventBackandForward;
            window.onpageshow = function(evt) {
                if (evt.persisted)
                    preventBackandForward();
            };
            window.inhibited_unload = function() {
                void(0);
            };
        </script>
        <title><?php echo $project_title; ?></title>
<?php echo $headerinfo; ?>
    </head>
    <body onload="<?php if ($pagename == 'activesession')
{ ?>timedRefresh(30000);<?php } ?>">
        <!--        <form id="frmTemplate" name="frmTemplate" method="post" >-->
        <form id="frmTemplate" name="frmTemplate" method="post" enctype="multipart/form-data">
            <!--            added enctype="multipart/form-data" for addgame.php-->
            <div id="containertemplate">
                <div id="fade" class="black_overlay"></div>	
                <div id="logo" style="background-image: url(images/sweepslogo.png); height:139px; width:293px;"></div>
                <div id="datetime" style ="height: 100px; width:230px; position: relative; margin-top: -120px; margin-left: 880px;">
                    <div class="labelbold" style="text-align:left;">
                        Welcome <?php echo $_SESSION['uname']; ?>!
                    </div>
                    <div>
                        <b>DATE:</b>&nbsp;<span id="servdate" name="servdate">&nbsp;</span>
                    </div>
                    <div>
                        <b>TIME:</b>&nbsp;<span id="clock">&nbsp;</span>
                    </div>
                </div>

                <script type="text/javascript">
                var tempDate = "<?php echo $serverdatetime; ?>";
                var tempTime = "<?php echo $serverdatetime; ?>";
                tempDate = tempDate.substring(5, 7) + "/" + tempDate.substring(8, 10) + "/" + tempDate.substring(0, 4);
                document.getElementById("servdate").firstChild.nodeValue = tempDate;


<?php $startdate = date("F d, Y H:i:s"); ?>
                var timesetter = new Date('<?php echo $startdate ?>');
                var TimeNow = '';
                function MakeTime() {
                    timesetter.setTime(timesetter.getTime() + 1000);
                    var hhN = timesetter.getHours();
                    if (hhN > 12) {
                        var hh = String(hhN - 12);
                        var AP = 'PM';
                    } else if (hhN == 12) {
                        var hh = '12';
                        var AP = 'PM';
                    } else if (hhN == 0) {
                        var hh = '12';
                        var AP = 'AM';
                    } else {
                        var hh = String(hhN);
                        var AP = 'AM';
                    }
                    var mm = String(timesetter.getMinutes());
                    var ss = String(timesetter.getSeconds());
                    TimeNow = ((hh < 10) ? ' ' : '') + hh + ((mm < 10) ? ':0' : ':') + mm + ((ss < 10) ? ':0' : ':') + ss + ' ' + AP;
                    document.getElementById("clock").firstChild.nodeValue = TimeNow;
                    setTimeout(function() {
                        MakeTime();
                    }, 1000);
                }
                MakeTime();

                function timedRefresh(timeoutPeriod)
                {
                    setTimeout("location.reload(true);", timeoutPeriod);
                }
                </script>

                <table>
                    <tr>
                        <td>
                            <div id="menu" style="vertical-align:top; width:255px;">
<?php
include_once('menu.php');
?>
                            </div>
                        </td>
                        <td>
                            <div id="maincontent" >