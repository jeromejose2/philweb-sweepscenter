<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 14, 2012
 * Purpose          :       POS Create Account Page.
 */
include("../controller/addpostacctprocess.php");
?>
<link href="css/base/jquery.ui.all.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css">
<script language="javascript" src="jscripts/jquery-1.5.2.min.js" ></script>
<script language="javascript" src="jscripts/jquery-ui-1.8.13.custom.min.js" ></script>
<script language="javascript" src="jscripts/lightbox.js" ></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>

<?php include("header.php"); ?>
<style type="text/css">
    .hasSpace {
        border-collapse: collapse;
    }

    .hasSpace td {
        padding-top: .3em;
        padding-bottom: .3em;
    }
</style>
<script>
    function forpassport()
    {
        var chkpass = document.getElementById("chkpass").checked;
        if (chkpass)
            $("#txtpassport").removeAttr("disabled");
        else
            $("#txtpassport").attr("disabled", true);
        $('#txtpassport').val('');
    }
    function forsss()
    {
        var chksss = document.getElementById("chksss").checked;
        if (chksss)
            $("#txtsss").removeAttr("disabled");
        else
            $("#txtsss").attr("disabled", true);
        $('#txtsss').val('');
    }
    function fordriver()
    {
        var chkdriver = document.getElementById("chkdriver").checked;
        if (chkdriver)
            $("#txtdriver").removeAttr("disabled");
        else
            $("#txtdriver").attr("disabled", true);
        $('#txtdriver').val('');
    }
    function forothers()
    {
        var chkothers = document.getElementById("chkothers").checked;
        if (chkothers)
            $("#txtothers").removeAttr("disabled");
        else
            $("#txtothers").attr("disabled", true);
        $('#txtothers').val('');
    }
    function verify_agentuname()
    {
        var uname = document.getElementById('txtAuname').value;

        if (uname != "" || uname != NULL)
        {
            $.ajax
                    ({
                        url: '../controller/agent_exist.php?uname=' + uname,
                        type: 'post',
                        success: function(data)
                        {

                            if (data == "notexist")
                            {
                                $('#hiddenvalidauname').val("notvalid")
                                $('#txtAuname').css('background-color', 'red');
                                var msgtitle = "ERROR";
                                var msg = "Please register new agent account";
                                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                                html += '<div class=\"msgLightbox\">';
                                html += '<p>' + msg + '</p>';
                                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                                html += '</div>';
                                showLightBox(html);
                                //txtAuname.focus();
                                //return false;
                            }
                            else if (data == "notavailable")
                            {
                                $('#hiddenvalidauname').val("notvalid")
                                $('#txtAuname').css('background-color', 'red');
                                var msgtitle = "ERROR";
                                var msg = "This agent username has already been used";
                                html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                                html += '<div class=\"msgLightbox\">';
                                html += '<p>' + msg + '</p>';
                                html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                                html += '</div>';
                                showLightBox(html);
                                //txtAuname.focus();
                                //return false;
                            } else {
                                $('#hiddenvalidauname').val("valid")
                                $('#txtAuname').css('background-color', '#d6eb75');
                            }

                        }
                    });
        }

    }
</script>    

<form id="frmaddaccount" name="frmaddaccount" method="post" action="addposacct.php">
    <?php echo $hidden; ?>
    <?php echo $hiddenvalidauname; ?>        
    <?php echo $hiddenactiveprovider; ?> 
    
    <div id="scrollablecontainer" style="margin-left: 10px; margin-top: 5px;">
        <div style="font-weight: bold;"><u>ACCOUNT INFORMATION</u></div>
        <table class="hasSpace" style="margin-top: 15px;">

            <tr>
                <td id="lblCashier" >*Cashier Username</td>
                <td><?php echo $txtuname; ?></td>
                <td><label style="font-style: italic; font-size: 10px;">Minimum of 8 and maximum of 15 characters.</label></td>
            </tr>
            <tr><td>*Name of Operator</td></tr>
            <tr><td id="lblFname">First Name</td><td id="lblMname">Middle Name</td><td id="lblLname">Last Name</td></tr>
            <tr>
                <td><?php echo $txtfname; ?></td>
                <td><?php echo $txtmname; ?></td>
                <td><?php echo $txtlname; ?></td>
            </tr>
            <tr>
                <td id="lblSwc">*SWC Code</td>
                <td><?php echo $txtscode; ?></td>
            </tr>
            <?php if($_SESSION['activeprovider'] == 3){//1 for RTG ?>           
            <tr>
                <td id="lblAus">*Agent Username</td>
                <td><?php echo $txtAuname; ?></td>
            </tr>
            <?php }?>            
<!--            <tr>
                <td id="lblApw">*Agent Password</td>
                <td><?php echo $txtApword; ?></td>
            </tr>              -->
        </table>

        <table style="margin-top: 10px;">
            <tr>
                <td id="lblHomeAddr">*Home Address</td>
                <td><?php echo $txthadd; ?></td>
            </tr>
        </table>

        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td id="lblZipcode">*Zip Code</td>
                <td><?php echo $txtzip; ?></td>
            </tr>
            <tr>
                <td id="lblTel">*Telephone No.</td>
                <td><?php echo $txttelno; ?></td>
            </tr>
        </table>

        <div style="font-weight: bold; margin-top: 10px;"><u>ADDITIONAL INFORMATION</u></div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td>Mobile No.</td>
                <td><?php echo $txtmobno; ?></td>
            </tr>
            <tr>
                <td>Fax No.</td>
                <td><?php echo $txtfax; ?></td>
            </tr>
            <tr>
                <td id="lblEmail">*E-mail Address</td>
                <td><?php echo $txtemail; ?></td>
            </tr>
            <tr>
                <td>Website Address</td>
                <td><?php echo $txtwebadd; ?></td>
            </tr>
            <tr>
                <td>Country of Residence</td>
                <td><?php echo $ddlcountry; ?></td>
            </tr>
            <tr>
                <td>Citizenship</td>
                <td><?php echo $txtcitizen; ?></td>
            </tr>
            <tr>
                <td id="lblBday">*Date of Birth</td>
                <td><?php echo $txtbday; ?></td>
            </tr>
        </table>

        <div style="font-weight: bold; margin-top: 10px;" id="please">*Please provide at least one form of identification.</div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td><?php echo $chkpass; ?></td>
                <td id="lblPassport">Passport</td>
                <td><?php echo $txtpassport; ?></td>
            </tr>
            <tr>
                <td><?php echo $chkdriver; ?></td>
                <td id="lblDriverLicense">Driver&#39;s License</td>
                <td><?php echo $txtdriver; ?></td>
            </tr>
            <tr>
                <td><?php echo $chksss; ?></td>
                <td id="lblSss">Social Security ID</td>
                <td><?php echo $txtsss; ?></td>
            </tr>
            <tr>
                <td><?php echo $chkothers; ?></td>
                <td id="lblOthers">Others</td>
                <td><?php echo $txtothers; ?></td>
            </tr>
        </table>

        <div style="font-weight: bold; margin-top: 10px;"><u>BANK INFORMATION</u></div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td>Bank</td>
                <td><?php echo $ddlbank; ?></td>
            </tr>
            <tr>
                <td>Bank Account Type</td>
                <td><?php echo $ddlbanktype; ?></td>
            </tr>
            <tr>
                <td>Bank Branch</td>
                <td><?php echo $txtbankbranch; ?></td>
            </tr>
            <tr>
                <td>Bank Account Name</td>
                <td><?php echo $txtbankacctname; ?></td>
            </tr>
            <tr>
                <td>Bank Account Number</td>
                <td><?php echo $txtbankacctnum; ?></td>
            </tr>
        </table>

        <div style="font-weight: bold; margin-top: 10px;"><u>PAYEE BANK INFORMATION</u></div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td>Bank</td>
                <td><?php echo $ddlpayeebank; ?></td>
            </tr>
            <tr>
                <td>Bank Account Type</td>
                <td><?php echo $ddlpayeebanktype; ?></td>
            </tr>
            <tr>
                <td>Bank Branch</td>
                <td><?php echo $txtpayeebankbranch; ?></td>
            </tr>
            <tr>
                <td>Bank Account Name</td>
                <td><?php echo $txtpayeebankacctname; ?></td>
            </tr>
            <tr>
                <td>Bank Account Number</td>
                <td><?php echo $txtpayeebankacctnum; ?></td>
            </tr>
        </table>
        <br/>
        <div style="font-style: italic; font-size: 14px;">Items with * are required.</div>
        <br/>
        <div align="center"><?php echo $btnSubmit; ?></div>
        <br/>
    </div>
</form>

<script type="text/javascript">

<?php if (isset($errormsg)) : ?>
        var msg = "<?php echo $errormsg; ?>";
        var msgtitle = "<?php echo $errormsgtitle; ?>";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input> ';
        html += '</div>';
        showLightBox(html);
<?php endif; ?>

<?php if (isset($successmsg)): ?>
        var msg = "<?php echo $successmsg; ?>";
        var msgtitle = "<?php echo $successmsgtitle; ?>";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay2\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

<?php endif; ?>

</script>
<?php include("footer.php"); ?>
<script>

    $('input[id=btnOkay2]').click(function() {
        document.location.href = 'addposacct.php';
    });


//$(function () {
//    $('input[type=text],[type=password],[id=txthadd]').bind('cut copy paste', function (e) {
//        e.preventDefault();
//    });
//});
</script>