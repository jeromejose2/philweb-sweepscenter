<?php
/*
 * Created By   :           Noel D. Antonio
 * Created On   :           June 14, 2012
 * Purpose      :           Menu Page : List of Menu modules.
 */

$request_url = $_SERVER['REQUEST_URI'];
$pathinfo = pathinfo($request_url);
$file = explode(".", $pathinfo['basename']);
$pagename = $file[0];
$accttype = $_SESSION['accttype'];
?>
<div id="masterdiv">
    <div class="menutitle" onclick="SwitchMenu('sub1')">POS Accounts</div>
    <span class="submenu" id="sub1">
        <?php
        if ($accttype == 1 || $accttype == 2)
        {
            if ($pagename == "addposacct")
            {
                ?>
                <a href="addposacct.php" class="linkmenuactive">* Create POS Account</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="addposacct.php" class="linkmenu">* Create POS Account</a><br />
                <?php
            }
        }

        if ($pagename == "posacctprofile")
        {
            ?>
            <a href="posacctprofile.php" class="linkmenuactive">* POS Account Profile</a><br />
            <?php
        }
        else
        {
            ?>
            <a href="posacctprofile.php" class="linkmenu">* POS Account Profile</a><br />
            <?php
        }

        if ($accttype == 1 || $accttype == 2 || $accttype == 4 || $accttype == 6)
        {
            if ($pagename == "manageposacct")
            {
                ?>
                <a href="manageposacct.php" class="linkmenuactive">* Manage POS Accounts</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="manageposacct.php" class="linkmenu">* Manage POS Accounts</a><br />
                <?php
            }
        }

        if ($accttype == 1 || $accttype == 6)
        {
            if ($pagename == "reloadbcf")
            {
                ?>
                <a href="reloadbcf.php" class="linkmenuactive">* Reload SCF</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="reloadbcf.php" class="linkmenu">* Reload SCF</a><br />
                <?php
            }
        }
        if ($accttype == 1 && $_SESSION['activeprovider'] == 3)
        {
            if ($pagename == "addagentacct")
            {
                ?>
                <a href="addagentacct.php" class="linkmenuactive">* Create Agent Account</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="addagentacct.php" class="linkmenu">* Create Agent Account</a><br />
                <?php
            }
        }
        ?>

    </span>
    <div class="menutitle" onclick="SwitchMenu('sub2')">
        Terminal Accounts
    </div>
    <span class="submenu" id="sub2">
        <?php
        if ($accttype == 1 || $accttype == 2)
        {
            if ($pagename == "addterminalacct")
            {
                ?>
                <a href="addterminalacct.php" class="linkmenuactive">* Create Terminal Account</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="addterminalacct.php" class="linkmenu">* Create Terminal Account</a>
                <br />
                <?php
            }
        }

        if ($accttype == 1 || $accttype == 2 || $accttype == 4)
        {
            if ($pagename == "changepassterminalacct")
            {
                ?>
                <a href="changepassterminalacct.php" class="linkmenuactive">* Change Terminal Account Password</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="changepassterminalacct.php" class="linkmenu">* Change Terminal Account Password</a><br />
                <?php
            }
        }
        ?>
    </span>
    <div class="menutitle" onclick="SwitchMenu('sub3')">
        User Accounts
    </div>
    <span class="submenu" id="sub3">
        <?php
        if ($accttype == 1 || $accttype == 2)
        {
            if ($pagename == "adduseracct")
            {
                ?>
                <a href="adduseracct.php" class="linkmenuactive">* Create User Account</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="adduseracct.php" class="linkmenu">* Create User Account</a><br />
                <?php
            }
        }

        if ($pagename == "useracctprofile")
        {
            ?>
            <a href="useracctprofile.php" class="linkmenuactive">* View User Account Profile</a><br />
            <?php
        }
        else
        {
            ?>
            <a href="useracctprofile.php" class="linkmenu">* View User Account Profile</a><br />
            <?php
        }

        if ($accttype == 1 || $accttype == 2)
        {
            if ($pagename == "edituseracct")
            {
                ?>
                <a href="edituseracct.php" class="linkmenuactive">* Edit User Account Profile</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="edituseracct.php" class="linkmenu">* Edit User Account Profile</a><br />
                <?php
            }

            if ($pagename == "manageuseracct")
            {
                ?>
                <a href="manageuseracct.php" class="linkmenuactive">* Manage User Account</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="manageuseracct.php" class="linkmenu">* Manage User Account</a><br />
                <?php
            }
        }
        ?>
    </span>
    <div class="menutitle" onclick="SwitchMenu('sub5')">
        Tools
    </div>
    <span class="submenu" id="sub5">
        <?php
        if ($accttype == 4 || $accttype == 1)
        {
            if ($pagename == "cancelvoucher")
            {
                ?>
                <a href="cancelvoucher.php" class="linkmenuactive">* Cancel Voucher Code</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="cancelvoucher.php" class="linkmenu">* Cancel Voucher Code</a><br />
                <?php
            }

            if ($pagename == "manualredemption")
            {
                ?>
                <a href="manualredemption.php" class="linkmenuactive">* Manual Redemption</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="manualredemption.php" class="linkmenu">* Manual Redemption</a><br />
                <?php
            }
        }

        if ($accttype == 3 || $accttype == 1 || $accttype == 4)
        {
            if ($pagename == "logs")
            {
                ?>
                <a href="logs.php" class="linkmenuactive">* API Logs</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="logs.php" class="linkmenu">* API Logs</a><br />
                <?php
            }
        }
        ?>

        <?php
//        if ($accttype == 1)
//        {
//            if ($pagename == "issueslist")
//            {
//                ?>
                <!--<a href="issueslist.php" class="linkmenuactive">* Issues List</a><br />-->
                <?php
//            }
//            else
//            {
//                ?>
                <!--<a href="issueslist.php" class="linkmenu">* Issues List</a><br />-->
                <?php
//            }
//        }
        ?>
                 <?php
        if ($accttype == 1)
        {
            if ($pagename == "terminallogout")
            {
                ?>
                <a href="terminallogout.php" class="linkmenuactive">* Terminal Logout</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="terminallogout.php" class="linkmenu">* Terminal Logout</a><br />
                <?php
            }
        }
        ?>

        <?php
        /*
          if ($pagename == "terminallogout"){
          ?>
          <a href="terminallogout.php" class="linkmenuactive">* Terminal Logout</a><br />
          <?php
          }
          else
          {
          ?>
          <a href="terminallogout.php" class="linkmenu">* Terminal Logout</a><br />
          <?php
          }

          if ($pagename == "manualredemptioncheck"){
          ?>
          <a href="manualredemptioncheck.php" class="linkmenuactive">* Manual Redemption Check</a><br />
          <?php
          }
          else
          {
          ?>
          <a href="manualredemptioncheck.php" class="linkmenu">* Manual Redemption Check</a><br />
          <?php
          }
          }

          if ($accttype == 3 || $accttype == 1 || $accttype == 4)
          {
          if ($pagename == "terminalreset2"){
          ?>
          <a href="terminalreset2.php" class="linkmenuactive">* Terminal Reset(NEW)</a><br />
          <?php
          }
          else
          {
          ?>
          <a href="terminalreset2.php" class="linkmenu">* Terminal Reset(NEW)</a><br />
          <?php
          }
          ?><?php

          }

          if ($accttype == 1 || $accttype == 3)
          {
          if ($pagename == "resetterminalbalance"){
          ?>
          <a href="template.php?page=resetterminalbalance" class="linkmenuactive">* Reset Terminal Balance</a><br />
          <?php
          }
          else
          {
          ?>
          <a href="template.php?page=resetterminalbalance" class="linkmenu">* Reset Terminal Balance</a><br />
          <?php
          } */

        /* if ($pagename == "managelogin"){
          ?>
          <a href="template.php?page=managelogin" class="linkmenuactive">* Manage Login Buttons</a><br />
          <?php
          }
          else
          {
          ?>
          <a href="template.php?page=managelogin" class="linkmenu">* Manage Login Buttons</a><br />
          <?php
          }
          } */
        ?>
    </span>

    <div class="menutitle" onclick="SwitchMenu('sub4')">
        Reports
    </div>
    <span class="submenu" id="sub4">
        <?php
        if ($pagename == "rptbcfbalance")
        {
            ?> 
            <a href="rptbcfbalance.php" class="linkmenuactive">* SCF Balance</a><br />
            <?php
        }
        else
        {
            ?>
            <a href="rptbcfbalance.php" class="linkmenu">* SCF Balance</a><br />
            <?php
        }

        if ($pagename == "rptbcfreloadhist")
        {
            ?>
            <a href="rptbcfreloadhist.php" class="linkmenuactive">* SCF Reload History</a><br />
            <?php
        }
        else
        {
            ?>
            <a href="rptbcfreloadhist.php" class="linkmenu">* SCF Reload History</a><br />
            <?php
        }

        if ($pagename == "rptterminaldeposithist")
        {
            ?>
            <a href="rptterminaldeposithist.php" class="linkmenuactive">* Sales History</a><br />
            <?php
        }
        else
        {
            ?>
            <a href="rptterminaldeposithist.php" class="linkmenu">* Sales History</a><br />
            <?php
        }

        if ($pagename == "rptredemptionhist")
        {
            ?>
            <a href="rptredemptionhist.php" class="linkmenuactive">* Terminal Redemptions History</a><br />
            <?php
        }
        else
        {
            ?>
            <a href="rptredemptionhist.php" class="linkmenu">* Terminal Redemptions History</a><br />
            <?php
        }

        if ($pagename == "rptpointsconversionhist")
        {
            ?>
            <a href="rptpointsconversionhist.php" class="linkmenuactive">* Points Conversion History</a><br />
            <?php
        }
        else
        {
            ?>
            <a href="rptpointsconversionhist.php" class="linkmenu">* Points Conversion History</a><br />
            <?php
        }

        if ($accttype == 1)
        {
            if ($pagename == "rptgrossholdhist")
            {
                ?>
                <a href="rptgrossholdhist.php" class="linkmenuactive">* Cash On Hand Summary</a><br />
                <?php
            }
            else
            {
                ?>
                <a href="rptgrossholdhist.php" class="linkmenu">* Cash On Hand Summary</a><br />
                <?php
            }
        }

        if ($pagename == "rptGrossHoldSummary")
        {
            ?>
            <a href="rptGrossHoldSummary.php" class="linkmenuactive">* Gross Hold Summary</a><br />
            <?php
        }
        else
        {
            ?>
            <a href="rptGrossHoldSummary.php" class="linkmenu">* Gross Hold Summary</a><br />
            <?php
        }

        if ($pagename == "rptvoucherusagehist2")
        {
            ?>
            <a href="rptvoucherusagehist2.php" class="linkmenuactive">* Voucher Usage History</a>
            <br />
            <?php
        }
        else
        {
            ?>
            <a href="rptvoucherusagehist2.php" class="linkmenu">* Voucher Usage History</a>
            <br />
            <?php
        }

        if ($pagename == "rptregistrantshist")
        {
            ?>
            <a href="rptregistrantshist.php" class="linkmenuactive">* Free Entry Registrants History</a><br />
            <?php
        }
        else
        {
            ?>
            <a href="rptregistrantshist.php" class="linkmenu">* Free Entry Registrants History</a><br />
            <?php
        }

        if ($pagename == "rptManualRedemption")
        {
            ?>
            <a href="rptManualRedemption.php" class="linkmenuactive">* Manual Redemption History</a><br />
            <?php
        }
        else
        {
            ?>
            <a href="rptManualRedemption.php" class="linkmenu">* Manual Redemption History</a><br />
            <?php
        }
        ?>
    </span>
    <div class="menutitle">
        <a href="changepassword.php" style="text-decoration:none; color:#000;">Change Password</a>
    </div>
    <div class="menutitle">
        <a href="logout.php" style="text-decoration:none; color:#000;">Log Out</a>
    </div>
</div>

