<?php
/* 
 * Date June 26-2012
 * Purpose : View for Manual Redemption History
 */
include("../controller/rptManualRedemptionprocess.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script type="text/javascript" src="jscriptsjquery-1.5.2.min.js" ></script>
<script type="text/javascript" src="jscripts/jquery-ui-1.8.13.custom.min.js" ></script>
<script language="javascript" src="jscripts/datepicker.js" type="text/javascript"></script>
<script language="javascript" src="jscripts/checkinputs.js" type="text/javascript"></script>
<script type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
    function get_terminals()
    {
        var acctid = document.getElementById('ddlPosName').options[document.getElementById('ddlPosName').selectedIndex].value;

        $("#ddlTerminalName").load(
            "get_terminal.php",
            {
                sid : acctid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title23').innerHTML = "ERROR!";
                    document.getElementById('msg23').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light23').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }

    function checkrptmanualredemption()
    {
        var posacctid = document.getElementById('ddlPosName').options[document.getElementById('ddlPosName').selectedIndex].value;
        var terminalid = document.getElementById('ddlTerminalName').options[document.getElementById('ddlTerminalName').selectedIndex].value;

        /*if(posacctid == "")
        {
            document.getElementById('title23').innerHTML = "ERROR!";
            document.getElementById('msg23').innerHTML = "Please select POS Account Name.";
            document.getElementById('light23').style.display = "block";
            document.getElementById('fade').style.display = "block";
            return false;
        }*/
		if(posacctid != 0)
		{
		    if(terminalid == "")
		    {
		        document.getElementById('title23').innerHTML = "ERROR!";
		        document.getElementById('msg23').innerHTML = "Please select Terminal Name.";
		        document.getElementById('light23').style.display = "block";
		        document.getElementById('fade').style.display = "block";
		        return false;
		    }
		}
        return true;
    }
</script>
<?php include('header.php')?>
<form name="frmrptmanualredemption" method ="POST">
    <div style="width:100%; text-align:center;">
    <table width="100%">
        <tr>
            <td class="labelbold2">From:</td>
            <td><?php echo $txtDateFr;?>
<!--                <input id="txtDateFr" name="txtDateFr" maxlength="10" value="<?php //echo $dDateFr;?>" onkeypress="javascript: return isNumberKey(event);" style="text-align:center;" readonly/>-->
                <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
            </td>
            <td class="labelbold2">To:</td>
            <td><?php echo $txtDateTo; ?>
<!--                <input id="txtDateTo" name="txtDateTo" maxlength="10" value="<?php // echo $dDateTo; ?>"  onkeypress="javascript: return isNumberKey(event);" style="text-align:center;" readonly/>-->
                <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
            </td>
            </tr>
        <tr>
            <td class="labelbold2" colspan="5">POS Account Name:
                <?php echo $ddlPos;?>
                 </td>


                 
                 <td>
                     <?php echo $btnSearch;?>
            </td>
        </tr>
    </table>
        
  <?php if(isset($getmanualredemp)):?>
        
        <table>
            <tr>
                <th class="th">Date Processed</th>
                <th class="th">POS Account Name</th>
                <th class="th">Terminal Name</th>
                <th class="th">Amount</th>
                <!--<th class="th">MG Transaction ID</th>-->
            </tr>
              <?php  if(count($getmanualredemp) > 0) : ?>
            <?php  echo  "<br/>";
         if($pgcon->SelectedItemTo > count($getmanualredemp1))
         {
         echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".count($getmanualredemp1)." of ".count($getmanualredemp1)."</b></p>";
         }else{
         echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".$pgcon->SelectedItemTo." of ".count($getmanualredemp1)."</b></p>";    
         }
?>
<!--             //pagination -->
                 <?php for($x=0;$x<count($getmanualredemp);$x++) : ?>
                  
                  <?php
                    $dateprocessed = $getmanualredemp[$x]['DateProcessed'];
                    $dateTime = new DateTime($dateprocessed);
                    $dateprocessed = $dateTime->format("m/d/Y h:i:s A");
                    $transactiondate = $getmanualredemp[$x]['TransactionDate'];
                    $dateTime = new DateTime($transactiondate);
                    $transactiondate = $dateTime->format("m/d/Y h:i:s A");
                    $posname = " ".$getmanualredemp[$x]['LastName'].",".$getmanualredemp[$x]['FirstName']." ". $getmanualredemp[$x]['MiddleName']."     ";
                    $terminalname = $getmanualredemp[$x]['Name'];
                    $amount = $getmanualredemp[$x]['Amount'];
                    $transID = $getmanualredemp[$x]['TransactionReferenceID'];
                    $mod = $x % 2;
                    
                    ?>
            
             <?php if ($mod == 0):?>
                        <tr style="background-color:#FFF1E6; height:30px;">
                            <td class="td"><?php echo $dateprocessed;?></td>
                            <td class="td"><?php echo $posname;?></td>
                            <td class="td"><?php echo $terminalname;?></td>
                            <td class="td"><?php echo $amount;?></td>
                            <!--<td class="td"><?php echo $transID;?></td>-->
                        </tr>
                        <?php else:?>
                        <tr style="height:30px;">
                            <td class="td"><?php echo $dateprocessed;?></td>
                            <td class="td"><?php echo $posname;?></td>
                            <td class="td"><?php echo $terminalname;?></td>
                            <td class="td"><?php echo $amount;?></td>
                            <!--<td class="td"><?php echo $transID;?></td>-->
                        </tr>
                        <?php endif;?>
            
            <?php endfor;?>
              <?php if(($pgcon->SelectedItemFrom-1 + count($getmanualredemp)) == count($getmanualredemp1)) { ?>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2" align="right"><b>Total Amount: <?php echo number_format($total , 2 , '.' , ',');?></b></td>
			</tr>
                        <?php }?>
            <?php   
            
             echo "</td></tr><tr><td colspan=\"5\" align=\"center\"></td></tr><tr><td colspan=\"5\" align=\"center\">";
                                echo "<p class=\"paging\">$pgHist </p>";
                                echo  "<br/>";
                                echo "<b><a href='export_report.php?fn=Manual_Redemption_History' class=\"labelbutton2\">DOWNLOAD</a></b></td></tr>";
           ?>
<!--        <tr></tr><tr></tr>
                        <tr>
                                <td colspan="5" align="center">
                                        <a href="export_report.php?fn=Manual_Redemption_History" class="labelbutton2">DOWNLOAD</a>
                                </td>
                        </tr>-->
        </table>
        
            <?php  else : ?>
            <tr>
             <td colspan="5" style="text-align:center;">No Records Found.</td>
            </tr>
            <?php  endif; ?>
           
            <?php endif;?>
    <?php 
          	unset($_SESSION['report_header']);
		unset($_SESSION['report_values']);
		$_SESSION['report_header']=array("\"Date Processed\"","\"POS Account Name\"","\"Terminal Name\"","\"Amount\"");
        for($counter = 0 ; $counter < count($getmanualredemp1) ; $counter++)
                        {
                    $dateprocessed = $getmanualredemp1[$counter]['DateProcessed'];
                    $dateTime = new DateTime($dateprocessed);
                    $dateprocessed = $dateTime->format("m/d/Y h:i:s A");
                    $transactiondate = $getmanualredemp1[$counter]['TransactionDate'];
                    $dateTime = new DateTime($transactiondate);
                    $transactiondate = $dateTime->format("m/d/Y h:i:s A");
                    $posname = " ".$getmanualredemp1[$counter]['LastName'].",".$getmanualredemp1[$counter]['FirstName']." ". $getmanualredemp1[$counter]['MiddleName']."     ";
                    $terminalname = $getmanualredemp1[$counter]['Name'];
                    $amount = $getmanualredemp1[$counter]['Amount'];
                    $transID = $getmanualredemp1[$counter]['TransactionReferenceID'];
                    $_SESSION['report_values'][$counter][0] = $dateprocessed;
                    $_SESSION['report_values'][$counter][1] = $posname;
                    $_SESSION['report_values'][$counter][2] = $terminalname;
                    $_SESSION['report_values'][$counter][3] = $amount;                    
                    }
                    $_SESSION['report_values'][($total_results)][0] = "Total Amount";	
                    $_SESSION['report_values'][($total_results)][1] = $total;
    ?>
    
   
    
        <!-- ERROR MESSAGE -->
    <div id="light23" style="text-align: center;font-size: 16pt;height: auto;width:500px;margin: auto;" class="white_content">
        <div id="title23" style="width: p00px;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">
        </div>
        <br />
        <div id="msg23"></div>
        <br/>
        <input id="btnOk23" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light23').style.display='none';document.getElementById('fade').style.display='none';" />
    </div>
    <!-- END OF NOTIFICATION MESSAGE -->
</div>
</form>
<script type="text/javascript">

<?php if (isset($errormsg)) : ?>
var msg = "<?php echo $errormsg; ?>";
var msgtitle = "<?php echo $errormsgtitle; ?>";
html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
html += '<div class=\"msgLightbox\">';
html += '<p>' + msg + '</p>';
html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input> ';
html += '</div>';
showLightBox(html);
<?php endif; ?>

</script>
<?php include('footer.php') ?>