<?php
/*
 * Created By       :       Angelo Cubos
 * Date Created     :       Mar 25, 2013
 * Purpose          :       Check Active Deck Consumption
 */

include("../controller/checkdeckconsumptionprocess.php");
?>
<?php include("header.php"); ?>
<div align="left" style="margin-left: 10px; margin-top: 5px;">
    <h3>Check Active Deck Consumption</h3>
    <br>
    <table>
        <tr>
            <td><b>Deck ID:</b> <?php echo $deckid?> </td>
        </tr>
    </table>
    <br/>

        <table>
            <tr style="background-color:#FF9C42; height:30px;">
                <th style="width:150px; text-align: center;">Control Number</th>
                <th style="width:150px; text-align: center;">Card Count</th>
                <th style="width:150px; text-align: center;">Used Card Count</th>
                <th style="width:150px; text-align: center;">Actual Used<br>Card Count</th>
                <th style="width:150px; text-align: center;">Date Activated</th>
                <th style="width:150px; text-align: center;">Created By</th>
            </tr>
            <tr style="background-color:#FFF1E6; height:30px;">
                 <!--a.ControlNo,a.CardCount,a.UsedCardCount,a.DateActivated,b.Username-->
                <td style="width:150px; text-align: center;"><?php echo $resultarray[0][ControlNo]; ?></td>
                <td style="width:150px; text-align: center;"><?php echo $resultarray[0][CardCount]; ?></td>
                <td style="width:150px; text-align: center;"><?php echo $resultarray[0][UsedCardCount]; ?></td>
                <td style="width:150px; text-align: center;"><?php echo $decknarray[0][ActualUsedCardCount]; ?></td>
                <td style="width:150px; text-align: center;"><?php echo $resultarray[0][DateActivated]; ?></td>
                <td style="width:150px; text-align: center;"><?php echo $resultarray[0][Username]; ?></td>
            </tr>
        </table>

    
</div>

<?php include("footer.php"); ?>