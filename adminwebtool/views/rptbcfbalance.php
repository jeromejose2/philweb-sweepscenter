<?php

/*
 * Created by Jerome Jose
 * Date June 21-2012
 * Purpose : View SCF Balance for Each Account
 */
include("../controller/rptbcfbalanceprocess.php");
?>

<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<?php include('header.php')?>
<form name="frmbcfbal" method="POST">
    <?php
      $currdatetime = date('m/d/Y h:i:s A');
       echo "<br /><table><tr><td><h3 align=\"left\">As of ".$currdatetime."</h3></td>";
       echo "<td width=\"200px\"></td>";
       echo "<td>";
       echo $refresh;
       echo "  </td></tr></table></br>";
       echo "<table style=\"overflow-x:scroll; width:100%;\" ><tr><th colspan =\"3\" style=\"height:30px;background-color: #FF9C42; color:#000000;\">SCF Balance Summary</th></tr>";
       echo "<tr><th class=\"th\">POS Account Number</th><th class=\"th\">POS Account Name</th><th class=\"th\">SCF Balance</th></tr>";

      if(count($getdetail) > 0)
      {
          
      
       for($x=0;$x<count($getdetail);$x++)
                {

                        $posacctno =  $getdetail[$x]["AcctID"];
                        $posacctno = str_pad($posacctno,"10","0", STR_PAD_LEFT);
                        $firstname = $getdetail[$x]["FirstName"];
                        $midlname = $getdetail[$x]["MiddleName"];
                        $lastname = $getdetail[$x]["LastName"];
                        $posacctname ="".$lastname.", ".$firstname." ".$midlname."";
                        $balance = $getdetail[$x]["Balance"];
                        $email = $getdetail[$x]["Email"];

                        $mod = $x % 2;
                    if ($mod == 0)
                        {
                                if ($balance >= "1500")
                                {
                                        $balance = number_format($balance,2);
                                        echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"td\">".$posacctno."</td><td class=\"td\">".$posacctname."</td><td class=\"td\">".$balance."</td></tr>";
                                }
                                else
                                {       $balance = number_format($balance,2);
                                        echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"td\">".$posacctno."</td><td class=\"td\">".$posacctname."</td><td class=\"fontred\">".$balance."</td></tr>";
                                }
                        }else
                        {
                                if ($balance >= "1500")
                                {
                                        $balance = number_format($balance,2);
                                        echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"td\">".$posacctno."</td><td class=\"td\">".$posacctname."</td><td class=\"td\">".$balance."</td></tr>";
                                }
                                else
                                {       $balance = number_format($balance,2);
                                        echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"td\">".$posacctno."</td><td class=\"td\">".$posacctname."</td><td class=\"fontred\">".$balance."</td></tr>";
                                }
                        }
       
          }
      }else
          {
             echo "<tr><td colspan=\"7\" align=\"center\"><b>No Records Found.</b></td></tr>";
                echo "</table>";  
          }
      
       ?>
    </form>
           
           <?php include('footer.php')?>