<?php

require_once("include/core/init.inc.php");
include("../controller/gamelistprocess.php");
$pagesubmenuid = 18;

?>

<?php include("header.php"); ?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script language="javascript" src="jscripts/datepicker.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script type="text/javascript" src="jscripts/jquery-1.5.2.min.js" lang="Javascript"></script>
<script language="javascript" type="text/javascript"></script>
<script>    
    function ChangePage(pagenum)
{
    selectedindex = document.getElementById("pgSelectedPage");
   document.getElementById("hiddenctr").value = selectedindex.value = pagenum;
    selectedindex.value = pagenum;
    document.forms[0].submit();

}
function redirectPage()
{
window.location = "addgame.php";
}
</script>


    <form action="gamelist.php" name="frmGameLists" method="post" >
        <?php echo $hiddenctr;?>
        <div class="content-page">
            <div class="search-container" style="width: 100%;">

                <div class="form-button"  align="right"><?php //echo $btnAddGame; ?><input type="button" value="Add New Game" onclick="redirectPage()"></div>
                <div class="form-view">
                    <table>
                    <tr>
                        <td><?php echo $ddlProviders;?></td>
                        <td><?php echo $btnSubmit;?></td>
                    </tr>
                </table>
                </div>
                
            </div>
        </div>
<!--    </form>
    <form action="gamelist.php" name="frmGameLists" method="post" >-->
    <?php if(isset($gameslist)): ?>
    <?php echo $hiddengameid; ?>

        <div>
<!--            <div>
                <?php echo $pgTransactionHistory;?>
            </div>           -->
        <table>
        <tr>
            <th width=150 style='table-layout:fixed' class="th">GameTitle</th>
            <th width=150 style='table-layout:fixed' class="th">GameName</th>
            <th width=150 style='table-layout:fixed' class="th">ImageName</th>
            <th width=150 style='table-layout:fixed' class="th">GID</th>
            <th width=150 style='table-layout:fixed' class="th">MID</th>
            <th width=100 style='table-layout:fixed' class="th">Status</th>
        </tr>
<div class="form-page"><?php echo $pgTransactionHistory;?></div>
        <?php if(count($gameslist) != 0):?>
            <?php for($i = 0 ; $i < count($gameslist) ; $i++): ?>
            
            <tr style="background-color:#FFF1E6; height:30px;">    
                <td ><?php echo $gameslist[$i]["GameTitle"]; ?></td>
                <td><?php echo $gameslist[$i]["GameName"]; ?></td>
                <td><?php echo $gameslist[$i]["ImageName"]; ?></td>
                <td><?php echo $gameslist[$i]["GID"]; ?></td>
                <td><?php echo $gameslist[$i]["MID"]; ?></td>
                <?php if($gameslist[$i]["Status"] == 1): ?>
                <td><a href="#" onclick="javascript: return inactive(<?php echo $gameslist[$i]["CasinoGameID"]; ?>)">Active</a></td>
                <?php else: ?>
                <td><a href="#" onclick="javascript: return active(<?php echo $gameslist[$i]["CasinoGameID"]; ?>)">Inactive</a></td>
                <?php endif; ?>
            </tr>
            
            <?php endfor; ?>
        <?php else: ?>
                <tr class="no-record">
                    <td colspan="4">No result to display</td>
                </tr>
        <?php endif; ?>
    </table>
    <?php endif; ?>
    <div class="form-page">
        <?php echo $pgTransactionHistory;?>
    </div>
    </div>
<!--MSG BOX-->
<div id="light2" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
<div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px;">
<b id="a2"></b>
</div>
<br/>
<p id="b2"><br/></p>
<br/>
<p><?php echo $btnYes; ?><?php echo $btnNo; ?></p>
</div>
    </form>
<?php include("footer.php"); ?>
<!--//////////////////////////////////////////////////////////-->
<script>
function inactive(id)
{
        document.getElementById('a2').innerHTML = "Confirmation";
        document.getElementById('b2').innerHTML = "Would you like to Inactivate this game?";
        document.getElementById('light2').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById("hiddengameid").value = id;
        document.getElementById("hiddenctr").value = (document.frmGameLists.elements['pgSelectedPage'].value);
}
function active(id)
{   
        document.getElementById('a2').innerHTML = "Confirmation";
        document.getElementById('b2').innerHTML = "Would you like to Activate this game?";
        document.getElementById('light2').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById("hiddengameid").value = id;
        document.getElementById("hiddenctr").value = (document.frmGameLists.elements['pgSelectedPage'].value);
}
</script>




