<?php
require_once("include/core/init.inc.php");
include("../controller/addgameprocess.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<?php include("header.php"); ?>
<body>
<div id="page">
    
 <table>
        <tr>
            <td><h3>Game Profile:</h3></td>
        </tr>
        <tr>
            <td>Service : </td>
            <td><?php echo $ddlservice; ?></td>
        </tr>
        <tr>
            <td>Game Title : </td>
            <td><?php echo $txtgametitle; ?></td>
        </tr>
        <tr>
            <td>Game Reference Name : </td>
            <td><?php echo $txtgameref; ?></td>
        </tr>
        <tr>
            <td>Image : </td>
            <td>  
                
        <input type="file" name="file" id="file" />
        
           </td>
        </tr>
        <tr>
            <td>GID : </td>
            <td><?php echo $txtGID; ?></td>
        </tr>
        <tr>
            <td>MID : </td>
            <td><?php echo $txtMID; ?></td>
        </tr>
        <tr>
<td>
     <p style="color: red; font-size: 12pt">All fields are required.</p>
</td>
        </tr>  
        <tr>
            <td></td>
            <td><button type="button" name="submit" id="submit" onclick="notification()" disabled="disable">   SUBMIT   </button></td>
        </tr>        
       
</table>

<div id="light1" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
<div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px;">
<b id="a1"></b>
</div>
<br/>
<p id="b1"><br/></p>
<br/>
<input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';"/>
</div>
    
    
<div id="light2" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
<div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px;">
<b id="a2"></b>
</div>
<br/>
<p id="b2"><br/>    
</p>
<br/>
<p id="b3"></p>
</div>
<script>
    <?php if (isset($errormsg)): ?>
        document.getElementById('a1').innerHTML = "<?php echo $errormsgtitle; ?>";
        document.getElementById('b1').innerHTML = "<?php echo $errormsg; ?>";
        document.getElementById('light1').style.display='block';
        document.getElementById('fade').style.display='block';
    <?php endif; ?>
        
    <?php if (isset($successmsg)): ?>
        document.getElementById('a2').innerHTML = "<?php echo $successmsgtitle; ?>";
        document.getElementById('b2').innerHTML = "<?php echo $successmsg; ?>";
        document.getElementById('b3').innerHTML = "<?php echo $btnOK; ?>";
        document.getElementById('light2').style.display='block';
        document.getElementById('fade').style.display='block';     
    <?php endif; ?>
        
</script>     
<script>

function notification(){
        document.getElementById('a2').innerHTML = "CONFIRMATION";
        document.getElementById('b2').innerHTML = "Would you like to add this game?";
        document.getElementById('b3').innerHTML = "<?php echo $btnPROCEED; ?> <?php echo $btnCANCEL; ?>";
        document.getElementById('light2').style.display='block';
        document.getElementById('fade').style.display='block';    
}
$(function() {
$('input[id=txtgametitle],[id=txtgameref],[id=txtGID],[id=txtMID],[id=file],select[name=ddlservice]').change(function() {
    $('#submit').attr('disabled', 
            !($('input[id=txtgametitle]').val()
        && $('input[id=txtgameref]').val()
        && $('input[id=txtGID]').val()
        && $('input[id=txtMID]').val()
        && $('input[id=file]').val()
        && $('select[name=ddlservice]').val()   
    ));
});
});

</script>
</div>
</body>
<?php include("footer.php"); ?>




