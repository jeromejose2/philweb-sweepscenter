<?php

/*
 * Created by : Jerome Jose
 * Date Created : June 15-2012
 * Purpose :  Edit  Admin Accounts
 */
include("../controller/edituseracctprocess.php");


?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<?php include("header.php")?>
<form id="frmUserProfile" method="post">
<div id="page">
  <table width="100%">
    <tr>
      <td colspan="2">
        &nbsp;
      </td>
    </tr>
     <tr>
      <td colspan="2" align="center">
        <div id="fontboldblack"><b>Username:</b>&nbsp;&nbsp;&nbsp;
            <?php echo $ddlUsername?>
            &nbsp;&nbsp;&nbsp;
            <?php echo $hidUsername?>
            <?php echo $hidAccountID?>
            <?php echo $btnSearch?>
            <?php echo $hidGroup ?>
        </div>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        &nbsp;
      </td>
    </tr>
    
    
    <?php if($_SESSION['tempuname'] != ""):?>
    <tr>
      <td colspan="2">
        <div id="header">User Account Profile</div>
      </td>
    </tr>
    <tr style="background-color:#FFF1E6; height:30px;">
      <td class="fontboldblack" style="width:50%;">&nbsp;&nbsp;Account Number</td>
      <td style="width:50%;">&nbsp;&nbsp;<?php echo $acctid ?></td>
    </tr>
    <tr style="background-color:#FFF1E6; height:30px;">
      <td class="fontboldblack">&nbsp;&nbsp;Username:</td>
      <td>&nbsp;&nbsp;<?php echo $uname ?></td>
    </tr>
    <tr style="background-color:#FFF1E6; height:30px;">
      <td class="fontboldblack">&nbsp;&nbsp;Date Of Creation:</td>
      <td>&nbsp;&nbsp;<?php echo $datecreated ?></td>
    </tr>
  </table>
  <br />
  <div id="header2">Account Information</div>
  <div style="text-align:center;">
   <table>
      <tr>
        <td class="fontboldblackcenter">First Name</td>
        <td class="fontboldblackcenter">Middle Name</td>
        <td class="fontboldblackcenter">Last Name</td>
      </tr>
      <tr>
        <td><?php echo $txtFName ;?>
        </td>
        <td><?php echo $txtMName ;?>
        </td>
        <td><?php echo $txtLName ;?>
        </td>
      </tr>
     <tr>
        <td class="fontboldblackcenter">E-mail Address</td>
        <td colspan="2"><?php echo $txtEmail ;?>
        </td>
      </tr>
        <tr>
        <td class="fontboldblackcenter">Position</td>
        <td colspan="2"><?php echo $txtPosition?>
        </td>
      </tr>
      <tr>
        <td class="fontboldblackcenter">Company</td>
        <td colspan="2"><?php echo $txtCompany?>
        </td>
      </tr>
      <tr>
        <td class="fontboldblackcenter">Department</td>
        <td><?php echo $txtDepartment?></td>
      </tr>
      <tr>
          <td class="fontboldblackcenter">Group</td>
        <td colspan="2"><?php echo $ddlGroup?></td>
      </tr>
      <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="3" align="center">
<!--          <a href="useracctprofile.php"></a>-->
            <?php echo $btnback;?>
          <?php echo $btnSubmit ?>
<!--          <input id="btnSubmit" name="btnSubmit"  type="submit" value="Apply Changes" class="labelbutton2" onclick="javascript: return checkedituseracct();"></input>-->
        </td>
      </tr> 
    <?php else:?>
   <tr><td width="750px;"></td></tr>
    <?php endif;?>
    </table>
      
      <!-- ERROR MESSAGE(No Username Selected) -->
	      <div id="light8" style="text-align: center;font-size: 16pt;height: 220px; margin-left:-95px;" class="white_content">
		<div style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">
		 Error Message
		</div>
		<br />
		<br />
		Please select username.
		<br/>
		<br/>
		<br />
		<!--<input id="btnOkClose" type="button" value="OK" class="labelbutton2" onclick="document.getElementById('light8').style.display='none';document.getElementById('fade').style.display='none'; return redirect2('edituseracct');<?php //$_SESSION['tempuname'] = '';?>" />-->
	        <input id="btnOkClose" type="button" value="OK" class="labelbutton2" onclick="document.getElementById('light8').style.display='none';document.getElementById('fade').style.display='none';" />
              </div>
	<!-- END OF ERROR MESSAGE(No Username Selected) -->
	<!-- CONFIRMATION MESSAGE -->
	      <div id="light20" style="text-align: center;font-size: 16pt;height: 270px;width:400px;margin-left:-95px;" class="white_content">
		<div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">
		 
		</div>
		<br />
		<br />
		<div id="msg">

		</div>
		<br/>
		<br/>
		<br />
		<input id="btnOkClose1" name ="btnOkClose1" type="submit" value="OK" class="labelbutton2" onclick="document.getElementById('light20').style.display='none';document.getElementById('fade').style.display='none';" />
	        
             </div>
	<!-- END OF CONFIRMATION MESSAGE -->
	<!-- CONFIRMATION MESSAGE -->
	      <div id="light21" style="text-align: center;font-size: 16pt;height: 270px;width:400px;margin-left:-95px;" class="white_content">
		<div id="title2" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">
		 
		</div>
		<br />
		<br />
		<div id="msg2">
 
		</div>
		<br/>
		<br/>
		<br />
		<!--<input id="btnOk" type="button" value="OK" class="labelbutton2" onclick="document.getElementById('light21').style.display='none';document.getElementById('fade').style.display='none';" />-->
	        <input id="btnOkClose" type="button" value="OK" class="labelbutton2" />
             </div>
	<!-- END OF CONFIRMATION MESSAGE -->		
    <br />
  </div>
</div>


    <script type="text/javascript">
    	function checkUsername()
	{	
            if(document.getElementById('ddlUsername').value == 0)
            {
                    document.getElementById('light8').style.display='block';
                    document.getElementById('fade').style.display='block';
                    return false;
            }
            else
            {
                    return true;
            }
	}
  <?php if (isset($isProcessed))
  {?>
  result();
  <?php
  }else{echo "";}?>
$('#btnOkClose').live('click',function(){
   $('#light21').fadeOut('slow');
   $('#light8').fadeOut('slow');
   $('#fade').fadeOut('slow');
});


  function result()
  {
    
    var resid = "<?php echo isset($returnid2) ? $returnid2 : ''; ?>";
    var resmsg = "<?php echo isset($returnmsg2) ? $returnmsg2 : ''; ?>";
    var session = "<?php echo $_SESSION['tempuname'];?>";
    var title = "";
    
    if (resid == 0)
    {
      title = "SUCCESSFUL INFORMATION UPDATE";
    }
    else
    {
      title = "ERROR!";
    }
    	document.getElementById('light20').style.display='block';
	document.getElementById('fade').style.display='block';
	document.getElementById('title').innerHTML = title;
	document.getElementById('msg').innerHTML = resmsg;
    
  }


</script>
</form>
<?php include("footer.php");?>
<script>
$(function () {
    $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
});
</script>