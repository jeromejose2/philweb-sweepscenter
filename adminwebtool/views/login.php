<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 13, 2012
 * Purpose          :       Login Page: Entry point of Guam Webtool.
 */
include("../controller/loginprocess.php");
?>
<html>
  <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
  <script language="Javascript" src="jscripts/jquery-1.5.2.min.js" ></script>
  <script language="Javascript" src="jscripts/checkinputs.js" ></script>
  <title>
    Guam Sweepstakes Caf&#233; Admin Tool
  </title>
  <body>
    <form id="frmLogin" name="frmLogin" action="login.php"  method="post">
      <div id="container">
        <div id="login">
          <table cellspacing="6" width="100%">
            <tr>
              <td colspan="2" align="center">
                <div style="background-image: url(images/sweepslogo.png); height:139px; width:293px;"></div>
                <br /><h2>Guam Sweepstakes Caf&#233; <br />Admin Tool</h2>
              </td>
            </tr>
            <tr>
              <td colspan="2" class="fontboldblack" align="center">
                <br/>Username: <?php echo $txtUsername; ?>
              </td>
            </tr>
            <tr>
              <td colspan="2" class="fontboldblack" align="center">
                Password: <?php echo $txtPassword; ?>
              </td>
            </tr>
            <tr>
              <td colspan="2" align="center">
                <?php echo $btnSubmit; ?>
              </td>
            </tr>
            <tr>
              <td colspan="2" align="center">
                <div>
                  <a href="#" onclick="javascript: return openforgotpw();" style="font-size:1.0em; font-weight:bold; cursor:pointer; color:#000000;">Forgot Password</a>
                </div>
              </td>
            </tr>
          </table>
        </div>
      </div>
        
      <div id="light1" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
        <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px;">
          <b id="a1"></b>
        </div>
        <br/>
        <p id="b1"><br/></p>
        <br/>
        <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';"/>
      </div>

      <!-- FORGOT PASSWORD-->
      <div id="light4" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
        <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px;">
          <b id="a4"></b>
        </div>
        <br/>
        <p id="b4"><br/></p>
        <br/><?php echo $txtEmail; ?><br /><br />
        <?php echo $btnReset; ?>
        <input id="btnCancel" type="button" value="CANCEL" class="labelbold2" onclick="document.getElementById('light4').style.display='none';document.getElementById('fade').style.display='none';document.getElementById('txtEmail').value='';"></input>
      </div>
      <!-- FORGOT PASSWORD-->
      <div id="fade" class="black_overlay"></div>
    </form>
<script type="text/javascript">        
  
      function specialkeys(e)
    {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k >= 64 && k < 91) || (k > 96 && k < 123) || k == 8 || e.keyCode == 9 || k == 32 || k == 46 || (e.keyCode > 36 && e.keyCode <= 40) || (k >= 48 && k <= 57));
    }
        
    function alphanumeric(e)
    {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || e.keyCode == 9 || k == 32 || (e.keyCode > 36 && e.keyCode <= 40) || (k >= 48 && k <= 57));
    }

    function checkLogin_forloginphp()
    {
        var oUsername = document.getElementById("txtUsername");
        var oPassword = document.getElementById("txtPassword");
        var pattern=/\s/;

        if (oUsername.value.length == 0 || oUsername.value.match(pattern))
        {
            document.getElementById('a1').innerHTML = 'INVALID LOG-IN';
            document.getElementById('b1').innerHTML = 'Please enter your username.';
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            document.getElementById('txtUsername').focus();
            return false;
        }
        else if (oPassword.value.length == 0)
        {
            document.getElementById('a1').innerHTML = 'INVALID LOG-IN';
            document.getElementById('b1').innerHTML = 'Please enter your password.';
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            document.getElementById('txtPassword').focus();
            return false;
        }
        return true;
    }
    
    function openforgotpw()
    {
        document.getElementById('a4').innerHTML = 'RESET PASSWORD CONFIRMATION ';
        document.getElementById('b4').innerHTML = 'You are about to reset your current password. Please enter your registered e-mail address.';
        document.getElementById('light4').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }
    
    function redirect()
    {
        var email = document.getElementById('txtEmail').value;
        if(email.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
        {
            document.getElementById('a4').innerHTML = "BLANK E-MAIL";
            document.getElementById('b4').innerHTML = "Please enter your e-mail address.";
            document.getElementById('fade').style.display='block';
            return false;
        }
        if(!checkemail_reset(email))
        {
            document.getElementById('a4').innerHTML = "INVALID E-MAIL";
            document.getElementById('b4').innerHTML = "You have entered an invalid e-mail address. Please try again.";
            document.getElementById('fade').style.display='block';
            return false;
        }
        return true;
    }
    
    function checkemail_reset(email)
    {
        var str=email
        var filter=/^.+@.+\..{2,3}$/

        if (filter.test(str))
                return true;
        else
                return false;
        return true;
        
        
    }
    <?php if (isset($errormsg)): ?>
        document.getElementById('a1').innerHTML = "<?php echo $errormsgtitle; ?>";
        document.getElementById('b1').innerHTML = "<?php echo $errormsg; ?>";
        document.getElementById('light1').style.display='block';
        document.getElementById('fade').style.display='block';
    <?php endif; ?>
        
    <?php if (isset($successmsg)): ?>
        document.getElementById('a1').innerHTML = "<?php echo $successmsgtitle; ?>";
        document.getElementById('b1').innerHTML = "<?php echo $successmsg; ?>";
        document.getElementById('light1').style.display='block';
        document.getElementById('fade').style.display='block';
    <?php endif; ?>


</script>
<script>
$(function () {
    $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
});
</script>
  </body>
</html>