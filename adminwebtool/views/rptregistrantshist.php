<?php
/*
 * Date Created: June 25-2012
 * Purpose : View Admin Account Details
 */
include("../controller/rptregistrantshistprocess.php");
?>

<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script language="javascript" src="jscripts/datepicker.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script language="javascript" type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();

    }
</script>
<?php include('header.php') ?>
<form name="frmrpstregistrants" method="POST">


    <div style="width:100%; text-align:center;">
        <table width="100%">
            <tr>
                <td class="labelbold2">From:</td>
                <td><?php echo $txtDateFr; ?>
                    <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
                </td>
                <td class="labelbold2">To:</td>
                <td><?php echo $txtDateTo; ?>
                    <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
                </td>
                <td width="100px"><?php echo $btnSearch; ?>
                </td>
            </tr>
            <tr>
                <td colspan="5" class="labelbold2">POS Account Name:&nbsp;&nbsp;
                    <?php echo $ddlPos; ?>
                </td>
            </tr>
            <tr>
                <?php if (isset($_SESSION['start'])): ?>
                    <td colspan="5">
                        <?php
                        echo "<br />";
                        echo "<table style=\"overflow-x:scroll;\" ><tr><th colspan =\"13\" style=\"height:30px;background-color: #FF9C42; color:#000000;\">FREE ENTRY REGISTRATION HISTORY</th></tr>";
                        echo "<tr><th class=\"th\">Name</th><th class=\"th\">Birth Date</th><th class=\"th\">Home Phone</th><th class=\"th\">Mobile Phone</th><th class=\"th\">Address</th><th class=\"th\">Email</th><th class=\"th\">Voucher Code</th><th class=\"th\">Date Registered</th><th class=\"th\">Date Used</th><th class=\"th\">Date and Time Generated</th><th class=\"th\">POS Account Name</th><th class=\"th\">Terminal Name</th><th class=\"th\">Winnings</th></tr>";
                        if (count($getrptregistrants) > 0)
                        {
                            echo "<br/>";
                            if ($pgcon->SelectedItemTo > count($getrptregistrants1))
                            {
                                echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-" . count($getrptregistrants1) . " of " . count($getrptregistrants1) . "</b></p>";
                            }
                            else
                            {
                                echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-" . $pgcon->SelectedItemTo . " of " . count($getrptregistrants1) . "</b></p>";
                            }

                            for ($x = 0; $x < count($getrptregistrants); $x++)
                            {
                                $name = " " . $getrptregistrants[$x]['LName'] . "," . $getrptregistrants[$x]['FName'] . " ";
                                $birthdate = $getrptregistrants[$x]['BirthDate'];
                                $homephone = $getrptregistrants[$x]['HomePhone'];
                                $mobilephone = $getrptregistrants[$x]['MobilePhone'];
                                $address = $getrptregistrants[$x]['Address'];
                                $email = $getrptregistrants[$x]['Email'];
                                $voucherno = $getrptregistrants[$x]['VoucherCode'];
                                $terminalname = $getrptregistrants[$x]['TerminalName'];
                                $winnings = $getrptregistrants[$x]['Winnings'];
                                $siteID = $getrptregistrants[$x]['SiteID'];

                                $dateregistered = $getrptregistrants[$x]['DateRegistered'];
                                $dateTime = new DateTime($dateregistered);
                                $dateregistered = $dateTime->format("m/d/Y");
                                $dateused = $getrptregistrants[$x]['DateUsed'];
                                $dateTime = new DateTime($dateused);
                                $dateused = $dateTime->format("m/d/Y");
                                $dategenerated = $getrptregistrants[$x]['DateGenerated'];
                                $dategenerated = new DateTime($dategenerated);
                                $dategenerated = $dateTime->format("m/d/Y h:i:s A");
                                $name1 = $getrptregistrants[$x]['Name1'];

                                $mod = $x % 2;

                                if ($mod == 0)
                                {
                                    echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"td\">" . $name . "</td><td class=\"td\">" . $birthdate . "
                                            </td><td class=\"td\">" . $homephone . "</td><td class=\"td\">" . $mobilephone . "</td><td class=\"td\">" . $address . "
                                            </td><td class=\"td\">" . $email . "</td><td class=\"td\">" . $voucherno . "</td><td class=\"td\">" . $dateregistered . "
                                            </td><td class=\"td\">" . $dateused . "</td><td class=\"td\">" . $dategenerated . "</td><td class=\"td\">" . $name1 . "</td><td class=\"td\">" . $terminalname . "</td><td class=\"td\">" . $winnings . "</tr>";
                                }
                                else
                                {
                                    echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"td\">" . $name . "</td><td class=\"td\">" . $birthdate . "
                                            </td><td class=\"td\">" . $homephone . "</td><td class=\"td\">" . $mobilephone . "</td><td class=\"td\">" . $address . "
                                            </td><td class=\"td\">" . $email . "</td><td class=\"td\">" . $voucherno . "</td><td class=\"td\">" . $dateregistered . "
                                            </td><td class=\"td\">" . $dateused . "</td><td class=\"td\">" . $dategenerated . "</td><td class=\"td\">" . $name1 . "</td><<td class=\"td\">" . $terminalname . "</td><td class=\"td\">" . $winnings . "</tr>";
                                }
                            }
                            echo "<tr><td colspan=\"11\" align=\"center\">";
                            echo "<br/>";
                            echo "<p class=\"paging\">$pgHist </p>";
                            echo "<br/>";
                            echo "</td></tr>";

                            echo '<tr>
                <td colspan="11" align="center">
                 <b><a href="export_report.php?fn=Free_Entry_Registration_History" class="labelbutton2">DOWNLOAD</a></b>
                </td>
                </tr>';
                        }
                        else
                        {
                            echo "<tr><td colspan=\"11\" align=\"center\"><b>No Records Found.</b></td></tr>";
                        }

                        echo "</table>";
                        ?>

                        <?php
                        unset($_SESSION['report_header']);
                        unset($_SESSION['report_values']);
                        $_SESSION['report_header'] = array("\"NAME\"", "\"BIRTH DATE\"", "\"HOME PHONE\"", "\"MOBILE PHONE\"", "\"ADDRESS\"", "\"EMAIL\"", "\"VOUCHER CODE\"", "\"DATE REGISTERED\"", "\"DATE USED\"", "\"DATE GENERATED\"", "\"POS Account Name\"", "\"TERMINAL NAME\"", "\"WINNINGS\"");
                        for ($counter = 0; $counter < count($getrptregistrants1); $counter++)
                        {
                            $name = " " . $getrptregistrants1[$counter]['LName'] . "," . $getrptregistrants1[$counter]['FName'] . " ";
                            $birthdate = $getrptregistrants1[$counter]['BirthDate'];
                            $homephone = $getrptregistrants1[$counter]['HomePhone'];
                            $mobilephone = $getrptregistrants1[$counter]['MobilePhone'];
                            $address = $getrptregistrants1[$counter]['Address'];
                            $email = $getrptregistrants1[$counter]['Email'];
                            $voucherno = $getrptregistrants1[$counter]['VoucherCode'];
                            $terminalname = $getrptregistrants1[$counter]['TerminalName'];
                            $winnings = $getrptregistrants1[$counter]['Winnings'];
                            $siteID = $getrptregistrants1[$counter]['SiteID'];

                            $dateregistered = $getrptregistrants1[$counter]['DateRegistered'];
                            $dateTime = new DateTime($dateregistered);
                            $dateregistered = $dateTime->format("Y/m/d");
                            $dateused = $getrptregistrants1[$counter]['DateUsed'];
                            $dateTime = new DateTime($dateused);
                            $dateused = $dateTime->format("m/d/Y h:i:s A");
                            $dategenerated = $getrptregistrants1[$counter]['DateGenerated'];
                            $dategenerated = new DateTime($dategenerated);
                            $dategenerated = $dateTime->format("m/d/Y h:i:s A");
                            $name1 = $getrptregistrants1[$counter]['Name1'];


                            $_SESSION['report_values'][$counter][0] = $name;
                            $_SESSION['report_values'][$counter][1] = $birthdate;
                            $_SESSION['report_values'][$counter][2] = $homephone;
                            $_SESSION['report_values'][$counter][3] = $mobilephone;
                            $_SESSION['report_values'][$counter][4] = $address;
                            $_SESSION['report_values'][$counter][5] = $email;
                            $_SESSION['report_values'][$counter][6] = $voucherno;
                            $_SESSION['report_values'][$counter][7] = $dateregistered;
                            $_SESSION['report_values'][$counter][8] = $dateused;
                            $_SESSION['report_values'][$counter][9] = $dategenerated;
                            $_SESSION['report_values'][$counter][10] = $name1;
                            $_SESSION['report_values'][$counter][11] = $terminalname;
                            $_SESSION['report_values'][$counter][12] = $winnings;
                        }
                        ?>
                    </td>
                <?php else: ?>
                <?php endif; ?>
            </tr>
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>

        </table>
    </div>
    <!-- ERROR MESSAGE -->
    <div id="light22" style="text-align: center;font-size: 16pt;height: 300px;width:500px;" class="white_content">
        <div id="title" style="width: p00px;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">

        </div>
        <br />
        <br />
        <div id="msg"></div>
        <br/>
        <br/>
        <br />	
        <input id="btnOk" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light22').style.display = 'none';
        document.getElementById('fade').style.display = 'none';" />
    </div>
    <!-- END OF NOTIFICATION MESSAGE -->	
</div>
</form>  
<script type="text/javascript">

<?php if (isset($errormsg)) : ?>
var msg = "<?php echo $errormsg; ?>";
var msgtitle = "<?php echo $errormsgtitle; ?>";
html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
html += '<div class=\"msgLightbox\">';
html += '<p>' + msg + '</p>';
html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input> ';
html += '</div>';
showLightBox(html);
<?php endif; ?>

</script>
<?php include('footer.php') ?>
