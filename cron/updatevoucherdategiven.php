<?php
include("../init.inc.php");
$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCCV_VoucherInfo");

$svvoucherinfo = new SCCV_VoucherInfo();

//Update Voucher Info
$svvoucherinfo->StartTransaction();
$svvoucherinfo->UpdateExpiredVouchers();
if ($svvoucherinfo->HasError) 
{
    $errormsg = $svvoucherinfo->getError();
    $svvoucherinfo->RollBackTransaction();
} 
else
{
    $svvoucherinfo->CommitTransaction();
}


?>
