<?php
/*
 * Added by JFJ 06-06 2012
 * 1st Modification: Noel Antonio 11-05-2012
 * 2nd Modification: Noel Antonio 11-13-2012
 */

require_once("include/core/init.inc.php");
include 'class.phpmailer.php';
$modulename = "SweepsCenter";
App::LoadModuleClass($modulename,"SCC_TerminalSessionDetails");
App::LoadModuleClass($modulename,"SCC_Sites");
App::LoadModuleClass($modulename,"SCCC_Winners");
App::LoadModuleClass($modulename,"SCCC_PointsConversionInfo");
App::LoadModuleClass($modulename,"SCC_FreeEntryRegistrants");
App::LoadModuleClass($modulename, "SCCV_VoucherInfo");
App::LoadLibrary("phpexcel/Classes/PHPExcel.php");

$terminalsessiondetails = new SCC_TerminalSessionDetails();
$winners = new SCCC_Winners();
$pointsconversioninfo = new SCCC_PointsConversionInfo();
$csites = new SCC_Sites();

$path = dirname(__FILE__);
chmod($path . '/performancesummary.php', 0777);
mkdir ($path . '/graphimg');

$arrPOS = $csites->SelectPOSAcctActiveSites();
$list_POS = new ArrayList();
$list_POS->AddArray($arrPOS);

$colspan = count($list_POS) + 2;

$currentdate = date("Y-m-d");

if (date('m', strtotime($currentdate)) == '01' && date('d', strtotime($currentdate)) == '01')
{
	$dateFr = date("Y-m-d", strtotime($currentdate . " -1 month")). ' 10:00:00.00000';
        $first_day_year = date("Y-m-d", strtotime($currentdate . " -12 months")) . ' 10:00:00.00000';
        $year = date('Y', strtotime("-1 year", strtotime($currentdate)));
}
else
{
        if (date('Y', strtotime($currentdate)) == date('Y'))
        {
                if (date('d', strtotime($currentdate)) == '01')
                {
                        // Get previous month
                        $temp_prev_month = date("Y-m-d", strtotime($currentdate . " -1 month"));
                        $dateFr = date('Y-m-d', mktime(0, 0, 0, date('m', strtotime($temp_prev_month)), '01', date("Y"))) . ' 10:00:00.00000';
                }
                else
                {
                        $dateFr = date('Y-m-d', mktime(0, 0, 0, date('m', strtotime($currentdate)), '01', date("Y"))) . ' 10:00:00.00000';
                }

                $first_day_year = date('Y-m-d', mktime(0, 0, 0, '01', '01', date("Y"))) . ' 10:00:00.00000';
        }
        
        else
        {
                if (date('d', strtotime($currentdate)) == '01')
                {
                        // Get previous month
                        $temp_prev_month = date("Y-m-d", strtotime($currentdate . " -1 month"));
                        $dateFr = date('Y-m-d', mktime(0, 0, 0, date('m', strtotime($temp_prev_month)), '01', date('Y', strtotime($currentdate)))) . ' 10:00:00.00000';
                }
                else
                {
                        $dateFr = date('Y-m-d', mktime(0, 0, 0, date('m', strtotime($currentdate)), '01', date('Y', strtotime($currentdate)))) . ' 10:00:00.00000';
                }
            
                $first_day_year = date('Y-m-d', mktime(0, 0, 0, '01', '01', date('Y', strtotime($currentdate)))) . ' 10:00:00.00000';
        }
        
        $year = date('Y', strtotime($currentdate));
}

$dateTo = $currentdate . ' 09:59:59.99999';

do
{
    $weekfrom = date('Y-m-d', strtotime("-1 day", strtotime($currentdate)));
    $currentdate = $weekfrom;
    $weekfrom = $weekfrom . ' 10:00:00.00000';
} 
while (date('N', strtotime($weekfrom)) != 1);


/* -------------------------------------------- EXCEL FILE ---------------------------------------- */ 

$arr_record = array();

// deposit and reloads
$xlsrec = $terminalsessiondetails->GetTotalDeposits($dateFr, $dateTo, 1);
if (count($xlsrec) > 0)
{
    for ($i = 0; $i < count($xlsrec); $i++)
    {
        $arr_record[$xlsrec[$i]["AccountID"]][date('Y-m-d', strtotime($xlsrec[$i]["DateStart"]))]["Deposit"] += $xlsrec[$i]["D"] + $xlsrec[$i]["R"];
    }
}
unset($xlsrec);

// redemptions (cash)
$xlsrec = $terminalsessiondetails->GetTotalRedemptions($dateFr, $dateTo, 1);
if (count($xlsrec) > 0)
{
    for ($i = 0; $i < count($xlsrec); $i++)
    {
        $arr_record[$xlsrec[$i]["AccountID"]][date('Y-m-d', strtotime($xlsrec[$i]["DateStart"]))]["Redemption"] += $xlsrec[$i]["Redeemed"];
    }
}
unset($xlsrec);

// redemptions (noncash)
$xlsrec = $winners->GetTotalRedemptionsFreeEntry($dateFr, $dateTo, 1);
if (count($xlsrec) > 0)
{
    for ($i = 0; $i < count($xlsrec); $i++)
    {
        $arr_record[$xlsrec[$i]["AccountID"]][date('Y-m-d', strtotime($xlsrec[$i]["DateUsed"]))]["Redemption"] += $xlsrec[$i]["NonCashTotals"];
    }
}
unset($xlsrec);

// sweeps input
$xlsrec = $pointsconversioninfo->GetTotalSweepsInput($dateFr, $dateTo, 1);
if (count($xlsrec) > 0)
{
    for ($i = 0; $i < count($xlsrec); $i++)
    {
        $arr_record[$xlsrec[$i]["AccountID"]][date('Y-m-d', strtotime($xlsrec[$i]["DateTimeTrans"]))]["SweepsInput"] += $xlsrec[$i]["SweepsInput"];
    }
}
unset($xlsrec);

$days_between = ceil(abs(strtotime($dateTo) - strtotime($dateFr)) / 86400);
for ($h = 0; $h < count($list_POS); $h++)
{
    $ds = new DateTime($dateTo);
    for ($i = 0;$i <= $days_between - 1;$i++)
    {
            $ds->modify('-1 days');
            $date = $ds->format('Y-m-d');
            if (!isset($arr_record[$list_POS[$h]["AccountID"]][$date]["Deposit"]))
                $arr_record[$list_POS[$h]["AccountID"]][$date]["Deposit"] += 0;
            
            if (!isset($arr_record[$list_POS[$h]["AccountID"]][$date]["Redemption"]))
                $arr_record[$list_POS[$h]["AccountID"]][$date]["Redemption"] += 0;
            
            if (!isset($arr_record[$list_POS[$h]["AccountID"]][$date]["SweepsInput"]))
                $arr_record[$list_POS[$h]["AccountID"]][$date]["SweepsInput"] += 0;
            
            ksort($arr_record[$list_POS[$h]["AccountID"]][$date]);
    }
    
    ksort($arr_record[$list_POS[$h]["AccountID"]]);
}

$objPHPExcel = new PHPExcel();

for ($i = 0; $i < count($list_POS); $i++)
{
        // create worksheet
        $objWorkSheet = $objPHPExcel->createSheet($i);
        $objWorkSheet->setTitle(substr($list_POS[$i]["FullName"], 0, 30));

        $objWorkSheet->mergeCells('A1:G1');
        $objWorkSheet->getStyle('A1:G'.$objWorkSheet->getHighestRow())->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                ->setWrapText(true);
        
        $objWorkSheet->setCellValue('A1', 'The Sweeps Center Performance Report for ' . $list_POS[$i]["FullName"]);
        
        if (count($arr_record[$list_POS[$i]["AccountID"]]) > 0)
        {       
                $rec = $arr_record[$list_POS[$i]["AccountID"]];
                $ctr = 0;
                foreach ($rec as $key => $val)
                {
                        $total_gross_holds = $val["Deposit"] - $val["Redemption"];
                        $total_mg_winnings = $val["Deposit"] - $val["SweepsInput"];
                        $total_sweeps_revenue = $val["SweepsInput"] - $val["Redemption"];
                        
                        $objWorkSheet->setCellValue('A'.($ctr + 3), $key)
                            ->setCellValue('B'.($ctr + 3), $val["Deposit"])
                            ->setCellValue('C'.($ctr + 3), $val["Redemption"])
                            ->setCellValue('D'.($ctr + 3), $total_gross_holds)
                            ->setCellValue('E'.($ctr + 3), $val["SweepsInput"])
                            ->setCellValue('F'.($ctr + 3), $total_mg_winnings)
                            ->setCellValue('G'.($ctr + 3), $total_sweeps_revenue)
                        ;
                        $ctr++;
                }
        }
        
        $objWorkSheet->setCellValue('A2', 'Date')
                    ->setCellValue('B2', 'Total Deposit')
                    ->setCellValue('C2', 'Total Sweeps Cash and Non-Cash Redemptions')
                    ->setCellValue('D2', 'Total Sweeps Center Gross Holds')
                    ->setCellValue('E2', 'Total Sweeps Inputs')
                    ->setCellValue('F2', 'Total Microgaming Winnings')
                    ->setCellValue('G2', 'Total Sweepstakes Card Winnings')
        ;
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save($path . '/Performance_Summary.xls');

chmod($path . "/Performance_Summary.xls", 0777);



/* -------------------------------------------- PERFORMANCE SUMMARY ---------------------------------------- */

$ds = new DateTime($dateTo);
$current = $ds->format('Y-m-d') . ' 09:59:59.99999';
$ds->modify('-1 days');
$previous = $ds->format('Y-m-d') . ' 10:00:00.00000';

$deposit = array();
$redemption = array();
$swcinput = array();
$grosshold = array();
$mgwinnings = array();
$swcrevenue = array();
$td = 0;
$tr = 0;
$tgh = 0;
$tsi = 0;
$tmw = 0;
$tscw = 0;

$message .= '<table border="1" bordercolor="#000" style="width:100%; font-family: Arial; color:#000000; border: solid 1px #000; border-collapse: collapse; text-align: center;">';
$message .= '<tr><td colspan="7" style="background-color: #FF9C42; color:#000000; font-weight:bold; font-size:1.2em; text-align: center;">The Sweeps Center Performance Report for ' . date ( 'Y-M-d' , strtotime ( '-1 day' , strtotime ( date('Y-m-d') ) ) ) . '</td></tr>'; 
$message .= '<tr style="font-weight: bold;">
        <td style="background-color: #FFBB7D; width:12%;">POS Site Name</td>
        <td style="background-color: #FFBB7D;">Total Deposit</td>
        <td style="background-color: #FFBB7D;">Total Sweeps Cash & Non-Cash Redemptions</td>
        <td style="background-color: #FFBB7D;">Total Sweeps Center Gross Holds</td>
        <td style="background-color: #FFBB7D;">Total Sweeps Inputs</td>
        <td style="background-color: #FFBB7D;">Total Microgaming Winnings</td>
        <td style="background-color: #FFBB7D;">Total Sweepstakes Card Winnings</td>
      </tr>';

        // deposit and reloads
        $deposit = Deposit($previous, $current);

        // redemptions (cash and noncash)
        $redemption = Redemption($previous, $current);

        // sweeps input
        $swcinput = SweepsInput($previous, $current);
        
        for ($i = 0; $i < count($list_POS); $i++)
        {
            $mod = ($i % 2);
            $message .= ($mod == 0) ? '<tr style="background-color:#FFF1E6; height:20px;">' : '<tr style="height:20px;">'; 
            
            $grosshold[$list_POS[$i]["AccountID"]] = $deposit[$list_POS[$i]["AccountID"]] - $redemption[$list_POS[$i]["AccountID"]];
            $mgwinnings[$list_POS[$i]["AccountID"]] = $deposit[$list_POS[$i]["AccountID"]] - $swcinput[$list_POS[$i]["AccountID"]];
            $swcrevenue[$list_POS[$i]["AccountID"]] = $grosshold[$list_POS[$i]["AccountID"]] - $mgwinnings[$list_POS[$i]["AccountID"]];
            
            $message .= '<td>'. $list_POS[$i]["FullName"] .'</td>';
            
            if (array_key_exists($list_POS[$i]["AccountID"], $deposit))
                $message .= '<td>'.number_format($deposit[$list_POS[$i]["AccountID"]], 2 , '.' , ',').'</td>';
            else
                $message .= '<td>'.number_format(0, 2 , '.' , ',').'</td>';
            
            if (array_key_exists($list_POS[$i]["AccountID"], $redemption))
                $message .= '<td>'.number_format($redemption[$list_POS[$i]["AccountID"]], 2 , '.' , ',').'</td>';
            else
                $message .= '<td>'.number_format(0, 2 , '.' , ',').'</td>';
            
            if (array_key_exists($list_POS[$i]["AccountID"], $grosshold))
                $message .= '<td>'.number_format($grosshold[$list_POS[$i]["AccountID"]], 2 , '.' , ',').'</td>';
            else
                $message .= '<td>'.number_format(0, 2 , '.' , ',').'</td>';
            
            if (array_key_exists($list_POS[$i]["AccountID"], $swcinput))
                $message .= '<td>'.number_format($swcinput[$list_POS[$i]["AccountID"]], 2 , '.' , ',').'</td>';
            else
                $message .= '<td>'.number_format(0, 2 , '.' , ',').'</td>';
            
            if (array_key_exists($list_POS[$i]["AccountID"], $mgwinnings))
                $message .= '<td>'.number_format($mgwinnings[$list_POS[$i]["AccountID"]], 2 , '.' , ',').'</td>';
            else
                $message .= '<td>'.number_format(0, 2 , '.' , ',').'</td>';
            
            if (array_key_exists($list_POS[$i]["AccountID"], $swcrevenue))
                $message .= '<td>'.number_format($swcrevenue[$list_POS[$i]["AccountID"]], 2 , '.' , ',').'</td>';
            else
                $message .= '<td>'.number_format(0, 2 , '.' , ',').'</td>';
            
            $message .= '</tr>';
            
            // getting the total figures
            $td += $deposit[$list_POS[$i]["AccountID"]];
            $tr += $redemption[$list_POS[$i]["AccountID"]];
            $tgh += $grosshold[$list_POS[$i]["AccountID"]];
            $tsi += $swcinput[$list_POS[$i]["AccountID"]];
            $tmw += $mgwinnings[$list_POS[$i]["AccountID"]];
            $tscw += $swcrevenue[$list_POS[$i]["AccountID"]];
        }
        
        $message .= '<tr style="font-weight: bold"><td>Total</td>
                    <td>' . number_format($td, 2 , '.' , ',') . '</td>
                    <td>' . number_format($tr, 2 , '.' , ',') . '</td>
                    <td>' . number_format($tgh, 2 , '.' , ',') . '</td>
                    <td>' . number_format($tsi, 2 , '.' , ',') . '</td>
                    <td>' . number_format($tmw, 2 , '.' , ',') . '</td>
                    <td>' . number_format($tscw, 2 , '.' , ',') . '</td>';
        
        $message .= '</tr></table>';


        
/* -------------------------------------------- MONTH TO DATE SUMMARY ---------------------------------------- */

$mtd_deposit = array();
$mtd_redemption = array();
$mtd_swcinput = array();
$mtd_grosshold = array();
$mtd_mgwinnings = array();
$mtd_swcrevenue = array();

// deposit and reloads
$mtd_deposit = Deposit($dateFr, $dateTo);

// redemptions (cash and noncash)
$mtd_redemption = Redemption($dateFr, $dateTo);

// sweeps input
$mtd_swcinput = SweepsInput($dateFr, $dateTo);

$message .= '<table style="padding:5px; font-weight:bold; font-family: Arial;">';
$message .= '<tr><td>';

$message .= '<table border="1" bordercolor="#000" style="font-weight:bold; font-family: Arial; color:#000000; border: solid 1px #000; border-collapse: collapse;">';
$message .= '<tr align="center"><td colspan=' . $colspan . ' style="background-color: #FFBB7D;">Month to Date Summary</td></tr>';

$message .= '<tr align="center"><td style="background-color: #FFBB7D;"></td>';
for ($i = 0; $i < count($list_POS); $i++)
{
    $message .= '<td style="background-color: #FFBB7D;">' . $list_POS[$i]["FullName"] . '</td>';
    $mtd_grosshold[$list_POS[$i]["AccountID"]] = $mtd_deposit[$list_POS[$i]["AccountID"]] - $mtd_redemption[$list_POS[$i]["AccountID"]];
    $mtd_mgwinnings[$list_POS[$i]["AccountID"]] = $mtd_deposit[$list_POS[$i]["AccountID"]] - $mtd_swcinput[$list_POS[$i]["AccountID"]];
    $mtd_swcrevenue[$list_POS[$i]["AccountID"]] = $mtd_grosshold[$list_POS[$i]["AccountID"]] - $mtd_mgwinnings[$list_POS[$i]["AccountID"]];
}
$message .= '<td style="background-color: #FFBB7D;">Total</td></tr>';

$message .= '<tr style="background-color:#FFF1E6; height:20px;"><td width="300px">MTD Deposit</td>';
$message .= toHTML($list_POS, $mtd_deposit);

$message .= '<tr style="background-color:#FFF1E6; height:20px;"><td width="300px">MTD Sweeps Cash & Non-Cash Redemptions</td>';
$message .= toHTML($list_POS, $mtd_redemption);

$message .= '<tr style="background-color:#FFF1E6; height:20px;"><td width="300px">MTD Sweeps Center Gross Holds</td>';
$message .= toHTML($list_POS, $mtd_grosshold);

$message .= '<tr style="background-color:#FFF1E6; height:20px;"><td width="300px">MTD Sweeps Center Input</td>';
$message .= toHTML($list_POS, $mtd_swcinput);

$message .= '<tr style="background-color:#FFF1E6; height:20px;"><td width="300px">MTD MicroGaming Winnings</td>';
$message .= toHTML($list_POS, $mtd_mgwinnings);

$message .= '<tr style="background-color:#FFF1E6; height:20px;"><td width="300px">MTD Sweepstakes Card Winnings</td>';
$message .= toHTML($list_POS, $mtd_swcrevenue);

$message .= '</table></td>';


/* -------------------------------------------- YEAR TO DATE SUMMARY ---------------------------------------- */

$ytd_deposit = array();
$ytd_redemption = array();
$ytd_swcinput = array();
$ytd_grosshold = array();
$ytd_mgwinnings = array();
$ytd_swcrevenue = array();

// deposit and reloads
$ytd_deposit = Deposit($first_day_year, $dateTo);

// redemptions (cash and noncash)
$ytd_redemption = Redemption($first_day_year, $dateTo);
   
//get previous 2012 cash redemptions
//$year = date('Y');
if ($year == 2012)
{
    $arrYTDPrevRed = $winners->GetPreviousYTDRedemption();
    if(count($arrYTDPrevRed) >= 1 && is_array($arrYTDPrevRed))
    {
        for ($i = 0; $i < count($arrYTDPrevRed); $i++)
        {
            $ytd_redemption[$arrYTDPrevRed[$i]["AccountID"]] += $arrYTDPrevRed[$i]["PrevRedemptions"];
        }
    }
}

// sweeps input
$ytd_swcinput = SweepsInput($first_day_year, $dateTo);

$message .= '<td>';
$message .= '<table border="1" bordercolor="#000" style="font-weight:bold; font-family: Arial; color:#000000; border: solid 1px #000; border-collapse: collapse;">';
$message .= '<tr align="center"><td width="400px" colspan=' . $colspan . ' style="background-color: #FFBB7D;">Year to Date Summary</td></tr>';
$message .= '<tr align="center"><td style="background-color: #FFBB7D;"></td>';
for ($i = 0; $i < count($list_POS); $i++)
{
    $message .= '<td style="background-color: #FFBB7D;">' . $list_POS[$i]["FullName"] . '</td>';
    $ytd_grosshold[$list_POS[$i]["AccountID"]] = $ytd_deposit[$list_POS[$i]["AccountID"]] - $ytd_redemption[$list_POS[$i]["AccountID"]];
    $ytd_mgwinnings[$list_POS[$i]["AccountID"]] = $ytd_deposit[$list_POS[$i]["AccountID"]] - $ytd_swcinput[$list_POS[$i]["AccountID"]];
    $ytd_swcrevenue[$list_POS[$i]["AccountID"]] = $ytd_grosshold[$list_POS[$i]["AccountID"]] - $ytd_mgwinnings[$list_POS[$i]["AccountID"]];
}
$message .= '<td style="background-color: #FFBB7D;">Total</td></tr>';

$message .= '<tr style="background-color:#FFF1E6; height:20px;"><td width="300px">YTD Deposit</td>';
$message .= toHTML($list_POS, $ytd_deposit);

$message .= '<tr style="background-color:#FFF1E6; height:20px;"><td width="300px">YTD Sweeps Cash & Non-Cash Redemptions</td>';
$message .= toHTML($list_POS, $ytd_redemption);

$message .= '<tr style="background-color:#FFF1E6; height:20px;"><td width="300px">YTD Sweeps Center Gross Holds</td>';
$message .= toHTML($list_POS, $ytd_grosshold);

$message .= '<tr style="background-color:#FFF1E6; height:20px;"><td width="300px">YTD Sweeps Center Input</td>';
$message .= toHTML($list_POS, $ytd_swcinput);

$message .= '<tr style="background-color:#FFF1E6; height:20px;"><td width="300px">YTD MicroGaming Winnings</td>';
$message .= toHTML($list_POS, $ytd_mgwinnings);

$message .= '<tr style="background-color:#FFF1E6; height:20px;"><td width="300px">YTD Sweepstakes Card Winnings</td>';
$message .= toHTML($list_POS, $ytd_swcrevenue);

$message .= '</table></td>';
$message .= '</tr></table>';
        
        
/* -------------------------------------------- FREE ENTRY USAGE ---------------------------------------- */

$fecolspan = $colspan + 1;

$message .= '<table border="1" bordercolor="#000" style="width: 100%; font-family: Arial; color:#000000; border: solid 1px #000; border-collapse: collapse;">';
$message .= '<tr>';
$message .= '<td colspan='.$fecolspan.' style="background-color: #FF9C42; color:#000000; font-weight:bold; font-size:1.2em; text-align: center;">Free Entry Usage Summary</td>';
$message .= '</tr>';
$message .= '<tr style="font-weight: bold;">';
$message .= '<td style="background-color: #FFBB7D;"></td>';
for ($i = 0; $i < count($list_POS); $i++)
{
    $message .= '<td style="text-align: center; background-color: #FFBB7D;">' . $list_POS[$i]["FullName"] . '</td>';
}
$message .= '<td style="text-align: center; background-color: #FFBB7D;">Online Registration</td>
             <td style="text-align: center; background-color: #FFBB7D;">TOTAL</td></tr>';

// WTD - Free Entry
$wtd_fer = FreeEntryRegistrations($weekfrom, $dateTo);
$wtd_feu = FreeEntriesUsed($weekfrom, $dateTo);
$wtd_wfe = WinningsFromFreeEntry($weekfrom, $dateTo);

// WTD - Online Free Entry
$wtd_ofer = OnlineFreeEntryRegistrations($weekfrom, $dateTo);


$message .= '<tr style="background-color:#FFF1E6; height:20px;">';
$message .= '<td width="300px">Week To Date Free Entry Registrations</td>';
$message .= toHTMLFreeEntryUsage($list_POS, $wtd_fer, $wtd_ofer);

$message .= '<tr style="background-color:#FFF1E6; height:20px;">';
$message .= '<td>Week To Date Free Entries Used</td>';
$message .= toHTMLFreeEntryUsage($list_POS, $wtd_feu, 0);

$message .= '<tr style="background-color:#FFF1E6; height:20px;">';
$message .= '<td>Week To Date Winnings from Free Entry</td>';
$message .= toHTMLWinnings($list_POS, $wtd_wfe);


// MTD - Free Entry
$mtd_fer = FreeEntryRegistrations($dateFr, $dateTo);
$mtd_feu = FreeEntriesUsed($dateFr, $dateTo);
$mtd_wfe = WinningsFromFreeEntry($dateFr, $dateTo);

// MTD - Online Free Entry
$mtd_ofer = OnlineFreeEntryRegistrations($dateFr, $dateTo);


$message .= '<tr style="background-color:#FFF1E6; height:20px;">';
$message .= '<td width="300px">Month To Date Free Entry Registrations</td>';
$message .= toHTMLFreeEntryUsage($list_POS, $mtd_fer, $mtd_ofer);

$message .= '<tr style="background-color:#FFF1E6; height:20px;">';
$message .= '<td>Month To Date Free Entries Used</td>';
$message .= toHTMLFreeEntryUsage($list_POS, $mtd_feu, 0);

$message .= '<tr style="background-color:#FFF1E6; height:20px;">';
$message .= '<td>Year To Date Winnings from Free Entry</td>';
$message .= toHTMLWinnings($list_POS, $mtd_wfe);


// YTD - Free Entry
$ytd_fer = FreeEntryRegistrations($first_day_year, $dateTo);
$ytd_feu = FreeEntriesUsed($first_day_year, $dateTo);
$ytd_wfe = WinningsFromFreeEntry($first_day_year, $dateTo);

// YTD - Online Free Entry
$ytd_ofer = OnlineFreeEntryRegistrations($first_day_year, $dateTo);


$message .= '<tr style="background-color:#FFF1E6; height:20px;">';
$message .= '<td width="300px">Year To Date Free Entry Registrations</td>';
$message .= toHTMLFreeEntryUsage($list_POS, $ytd_fer, $ytd_ofer);

$message .= '<tr style="background-color:#FFF1E6; height:20px;">';
$message .= '<td>Year To Date Free Entries Used</td>';
$message .= toHTMLFreeEntryUsage($list_POS, $ytd_feu, 0);

$message .= '<tr style="background-color:#FFF1E6; height:20px;">';
$message .= '<td>Year To Date Winnings from Free Entry</td>';
$message .= toHTMLWinnings($list_POS, $ytd_wfe);

$message .= "</table></td>";

//echo $message; exit;

/* -------------------------------------------- EMAIL SENDING ---------------------------------------- */        

$yesterday = strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . " -1 day");                      
$subject = "The Sweeps Center Performance Summary for " . date("Y-M-d", $yesterday);
$mailer = new PHPMailer();
$mailer->IsSMTP();

$mailer->AddAddress("ndantonio@philweb.com.ph", "Noel Antonio");
$mailer->AddBCC("mccalderon@philweb.com.ph", "Tere Calderon");

$mailer->From = 'no-reply@philwebasiapacific.com';
$mailer->FromName = 'no-reply';
$mailer->Host = 'localhost';
$mailer->isHtml = true;
$mailer->Subject = $subject;
$mailer->MsgHTML($message);

$embed = '';

$mailer->AddEmbeddedImage($path . '/graphimg/totalgh.jpg', 'pic', 'totalgh.jpg', 'base64','image/jpeg');
$embed = "<img src='cid:pic' alt='pic'><br/>";

for ($i = 0; $i <count($list_POS); $i++)
{
    $mailer->AddAttachment($path . '/' . $list_POS[$i]["FullName"] . '.jpg');
    $mailer->AddEmbeddedImage($path . '/graphimg/' . $list_POS[$i]["FullName"] . '.jpg', 'pic' . $i, $list_POS[$i]["FullName"] . '.jpg', 'base64','image/jpeg');
    $embed .= "<img src='cid:pic" . $i . "' alt='pic" . $i . "'><br/>";
}

$mailer->AddAttachment($path . '/Performance_Summary.xls');
$mailer->AddAttachment($path . '/totalgh.jpg');

$mailer->Body = "<div style=\"font-size: 15px;\">&nbsp;</div>
                <div>".$message."</div>
                <br/><br/>".$embed;

if(!$mailer->Send())
        echo "Error sending: " . $mailer->ErrorInfo;
else
        echo "Email sent";


/* -------------------------------------------- USER DEFINED FUNCTIONS ---------------------------------------- */

function Deposit($dateFr, $dateTo)
{
        $terminalsessiondetails = new SCC_TerminalSessionDetails();
        $array = array();
        
        $rec_dep_mtd = $terminalsessiondetails->GetTotalDeposits($dateFr, $dateTo);
        if(count($rec_dep_mtd) >= 1 && is_array($rec_dep_mtd))
        {
            for ($i = 0; $i < count($rec_dep_mtd); $i++)
            {
                $array[$rec_dep_mtd[$i]["AccountID"]] = $rec_dep_mtd[$i]["D"]  + $rec_dep_mtd[$i]["R"];
            }
        }
        
        return $array;
}

function Redemption($dateFr, $dateTo)
{
        $terminalsessiondetails = new SCC_TerminalSessionDetails();
        $winners = new SCCC_Winners();
        $array = array();
        
        // cash
        $rec_red_mtd = $terminalsessiondetails->GetTotalRedemptions($dateFr, $dateTo);
        if(count($rec_red_mtd) >= 1 && is_array($rec_red_mtd))
        {
            for ($i = 0; $i < count($rec_red_mtd); $i++)
            {
                $array[$rec_red_mtd[$i]["AccountID"]] += $rec_red_mtd[$i]["Redeemed"];
            }
        }
        
        // noncash

        $rec_noncash_mtd = $winners->GetTotalRedemptionsFreeEntry($dateFr, $dateTo);
        if(count($rec_noncash_mtd) >= 1 && is_array($rec_noncash_mtd))
        {
            for ($i = 0; $i < count($rec_noncash_mtd); $i++)
            {
                $array[$rec_noncash_mtd[$i]["AccountID"]] += $rec_noncash_mtd[$i]["NonCashTotals"];
            }
        }
        
        return $array;
}

function SweepsInput($dateFr, $dateTo)
{
        $pointsconversioninfo = new SCCC_PointsConversionInfo();
        $array = array();
        
        $arrMTDSweepsInput = $pointsconversioninfo->GetTotalSweepsInput($dateFr, $dateTo);
        if(count($arrMTDSweepsInput) >= 1 && is_array($arrMTDSweepsInput))
        {
            for ($i = 0; $i < count($arrMTDSweepsInput); $i++)
            {
                $array[$arrMTDSweepsInput[$i]["AccountID"]] += $arrMTDSweepsInput[$i]["SweepsInput"];
            }
        }
        
        return $array;
}

function FreeEntryRegistrations($dateFr, $dateTo)
{
        $cfreeentryregistrants = new SCC_FreeEntryRegistrants();
        $array = array();
        
        $arrFER = $cfreeentryregistrants->GetTotalFreeEntryRegistrants($dateFr, $dateTo);
        if(count($arrFER) >= 1 && is_array($arrFER))
        {
            for ($i = 0; $i < count($arrFER); $i++)
            {
                $array[$arrFER[$i]["AccountID"]] = $arrFER[$i]["Total"];
            }
        }
        
        return $array;
}

function FreeEntriesUsed($dateFr, $dateTo)
{
        $cfreeentryregistrants = new SCC_FreeEntryRegistrants();
        $array = array();
        
        $arrFEU = $cfreeentryregistrants->GetTotalFreeEntriesUsed($dateFr, $dateTo);
        if(count($arrFEU) >= 1 && is_array($arrFEU))
        {
            for ($i = 0; $i < count($arrFEU); $i++)
            {
                $array[$arrFEU[$i]["AccountID"]] = $arrFEU[$i]["Total"];
            }
        }
        
        return $array;
}

function WinningsFromFreeEntry($dateFr, $dateTo)
{
        $cvvoucherinfo = new SCCV_VoucherInfo();
        $array = array();
        
        $arrWFE = $cvvoucherinfo->GetTotalFreeEntryWinnings($dateFr, $dateTo);
        if(count($arrWFE) >= 1 && is_array($arrWFE))
        {
            for ($i = 0; $i < count($arrWFE); $i++)
            {
                $array[$arrWFE[$i]["AccountID"]][$arrWFE[$i]["Winnings"]] = $arrWFE[$i]["Total"];
            }
        }
        
        return $array;
}

function OnlineFreeEntryRegistrations($dateFr, $dateTo)
{
        $cfreeentryregistrants = new SCC_FreeEntryRegistrants();
        $total = 0;
        
        $arrFER = $cfreeentryregistrants->GetTotalOnlineFreeEntryRegistrants($dateFr, $dateTo);
        $total = $arrFER[$i]["Total"];
        
        return $total;
}

/*function OnlineFreeEntriesUsed($dateFr, $dateTo)
{
        $cfreeentryregistrants = new SCC_FreeEntryRegistrants();
        $total = 0;
        
        $arrFEU = $cfreeentryregistrants->GetTotalOnlineFreeEntriesUsed($dateFr, $dateTo);
        $total = $arrFEU[$i]["Total"];
        
        return $total;
}

function OnlineWinningsFromFreeEntry($dateFr, $dateTo)
{
        $cvvoucherinfo = new SCCV_VoucherInfo();
        $array = array();
        
        $arrWFE = $cvvoucherinfo->GetTotalOnlineFreeEntryWinnings($dateFr, $dateTo);
        if(count($arrWFE) >= 1 && is_array($arrWFE))
        {
            for ($i = 0; $i < count($arrWFE); $i++)
            {
                $array[$arrWFE[$i]["Winnings"]] = $arrWFE[$i]["Total"];
            }
        }
        
        return $array;
}*/

function toHTML($list_POS, $array)
{
        $str = '';
        $total = 0;
        
        for ($i = 0; $i < count($list_POS); $i++)
        {   
            if (array_key_exists($list_POS[$i]["AccountID"], $array))
            {
                $str .= ' <td align="right" width="100px">' . number_format($array[$list_POS[$i]["AccountID"]], 2 , '.' , ',') . ' </td>';
                $total += $array[$list_POS[$i]["AccountID"]];
            }
            else
            {
                $str .= '<td align="right" width="100px">' . number_format(0, 2 , '.' , ',') . '</td>';
            }
        }
        $str .= ' <td align="right" width="100px">' . number_format($total, 2 , '.' , ',') . ' </td>';
        $str .= '</tr>';
        
        return $str;
}

function toHTMLFreeEntryUsage($list_POS, $array, $onlinereg)
{
        $str = '';
        $total = 0;
        $onlinereg += 0;
        
        for ($i = 0; $i < count($list_POS); $i++)
        {   
            if (array_key_exists($list_POS[$i]["AccountID"], $array))
            {
                $str .= '<td align="right" width="100px">';
                $str .= $array[$list_POS[$i]["AccountID"]];
                $str .= '</td>';
                $total += $array[$list_POS[$i]["AccountID"]];
            }
            else
            {
                $str .= '<td align="right" width="100px">0</td>';
            }
        }
        $str .= '<td align="right" width="100px">' . $onlinereg . '</td>';
        $total += $onlinereg;
        $str .= '<td align="right" width="100px">';
        $str .= $total;
        $str .= '</td>';
        $str .= '</tr>';
        
        return $str;
}

function toHTMLWinnings($list_POS, $array)
{
        $str = '';
        $prizes = '';
        $ctr = 1;
        $groupnoncash = array();
        $noncashdisplay = array();
        $tempfinal = array();
        $finaldisplay = array();
        $noncashdisplay[0] = 0;
        $finaldisplay[0] = 0;
        $amount = 0;
        $total_cash = 0;
        $overall_total_cash = 0;
        
        for ($i = 0; $i < count($list_POS); $i++)
        {
                $records = $array[$list_POS[$i]["AccountID"]];

                if (isset($records) || $records != NULL)
                {
                        foreach ($records as $key => $val)
                        {
                                if (strpos($key, '(') !== false)
                                {
                                        // This will remove parenthesis (and string contains inside it) on the winnings
                                        $key = trim(preg_replace('/\s*\([^)]*\)/', '', $key));
                                }
                                    
                                if (strpos($key, 'Cash') !== false)
                                {
                                        $amount = preg_replace('/[^0-9]/','',$key);
                                        $total_cash += $amount * $val;
                                }
                                else
                                {    
                                        // To group different types of coupons w/ total values
                                        $groupnoncash[$key] += $val;
                                }
                        }
                        
                        // Cash
                        $overall_total_cash += $total_cash;
                        if ($total_cash == 0) {
                            $ctr = 0;
                        } else {
                            $noncashdisplay[0] = "$" . number_format($total_cash) . " Cash";
                        }
                        
                        // Non-Cash
                        foreach ($groupnoncash as $a => $b)
                        {
                            $noncashdisplay[$ctr] = "(" . $b . ") " . $a;
                            $ctr++;
                            $tempfinal[$a] += $b;
                        }
                        
                        $prizes =  implode(";", $noncashdisplay);
                        $str .= "<td align=\"right\" width=\"100px\">".$prizes."</td>";

                        unset($total_cash);
                        unset($groupnoncash);
                        unset($noncashdisplay);
                        unset($prizes);
                }
                else
                {
                        $str .= "<td align=\"right\" width=\"100px\">---</td>";
                }
        }

        if ($overall_total_cash == 0) {
            $ctr = 0;
        } else {
            $finaldisplay[0] = "$" . number_format($overall_total_cash) . " Cash";
        }
        
        foreach ($tempfinal as $e => $f)
        {
            $finaldisplay[$ctr] = "(" . $f . ") " . $e;
            $ctr++;
        }
        
        $str .= "<td align=\"right\" width=\"100px\">---</td>";
        $prizestotal = implode(";", $finaldisplay);
        $str .= "<td align=\"right\" width=\"100px\">" . $prizestotal ."</td></tr>";
        
        return $str;
}
?>