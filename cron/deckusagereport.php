<?php
/**
 * Created by JFJ 06-05-2012 
 * 1st Modified By: Noel Antonio 11-05-2012
 */

require_once ("include/core/init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_TerminalSessionDetails");
App::LoadModuleClass($modulename,"SCCC_DeckInfo");
App::LoadModuleClass($modulename,"SCCC_Prizes");
App::LoadModuleClass($modulename,"SCCC_DeckDetails");
App::LoadCore("PHPMailer.class.php");

$terminalsessiondetails = new SCC_TerminalSessionDetails();
$deckinfo = new SCCC_DeckInfo();
$scprizes = new SCCC_Prizes();
$deckdetails = new SCCC_DeckDetails();

$currdate = "now";
$ds = new DateSelector($currdate);
$date = $ds->PreviousDate;
$date1 = $ds->CurrentDate; 

function my_date_diff($start, $end="NOW")
{
    $sdate = strtotime($start);
    $edate = strtotime($end);

    $time = $edate - $sdate;
    if($time>=0 && $time<=59) {
            // Seconds
            $timeshift = $time.' seconds ';

    } elseif($time>=60 && $time<=3599) {
            // Minutes + Seconds
            $pmin = ($edate - $sdate) / 60;
            $premin = explode('.', $pmin);

            $presec = $pmin-$premin[0];
            $sec = $presec*60;

            $timeshift = $premin[0].' min '.round($sec,0).' sec ';

    } elseif($time>=3600 && $time<=86399) {
            // Hours + Minutes
            $phour = ($edate - $sdate) / 3600;
            $prehour = explode('.',$phour);

            $premin = $phour-$prehour[0];
            $min = explode('.',$premin*60);

            $presec = '0.'.$min[1];
            $sec = $presec*60;

            $timeshift = $prehour[0].' hrs '.$min[0].' min '.round($sec,0).' sec ';

    } elseif($time>=86400) {
            // Days + Hours + Minutes
            $pday = ($edate - $sdate) / 86400;
            $preday = explode('.',$pday);

            $phour = $pday-$preday[0];
            $prehour = explode('.',$phour*24);

            $premin = ($phour*24)-$prehour[0];
            $min = explode('.',$premin*60);

            $presec = '0.'.$min[1];
            $sec = $presec*60;

            //$timeshift = $preday[0].' days '.$prehour[0].' hrs '.$min[0].' min '.round($sec,0).' sec ';
            $timeshift = $preday[0];

    }
    return $timeshift;
}


    $getdate = $deckinfo->GetUsageDate();
    $dateactivated2 = date('Y-m-d', strtotime($getdate[0]["DateActivated2"]));
    $dateactivated = $getdate[0]["DateActivated"];
    $usedcrdcnt = $getdate[0]["UsedCardCount"];


    $start_date = $dateactivated2;
    $end_date = date('Y-m-d');

    $datedifference = my_date_diff($start_date, $end_date);
    $ave = $usedcrdcnt/$datedifference;

    $pm = new PHPMailer();
    /*
    $mailer->AddAddress("mtgrandinetti@philweb.com.ph", "Michael Grandinetti");
    $mailer->AddAddress("sasproule@philweb.com.ph", "Scott Sproule");
    $mailer->AddAddress("akgarcia@philweb.com.ph", "Antonio Jose Garcia");
    $mailer->AddAddress("zmprieto@philweb.com.ph", "Zaldy Prieto");
    $mailer->AddAddress("business.solutions@philweb.com.ph", "Business Solutions");
    $mailer->AddAddress("acmanabal@philweb.com.ph", "Alexander Manabal");
    $mailer->AddAddress("gydichoso@philweb.com.ph", "Guia Paula Dichoso");
    $mailer->AddAddress("csvillanueva@philweb.com.ph", "Clarivic Villanueva");
    $mailer->AddAddress("rmbuenaventura@philweb.com.ph", "Rocky Buenaventura");
    $mailer->AddAddress("ddpayumo@thesweepscenter.com", "Danny Payumo");
    $mailer->AddBCC("jbpormento@philweb.com.ph", "James Oliver Pormento");
    $mailer->AddBCC("mccalderon@philweb.com.ph", "Tere Calderon");
    $mailer->AddBCC("rpsanchez@philweb.com.ph", "Roger Sanchez");
    */
    $pm->AddAddress("mccalderon@philweb.com.ph", "Tere Calderon");
        
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    $folder = $_SERVER["REQUEST_URI"];
    $folder = substr($folder,0,strrpos($folder,'/') + 1);
    if ($_SERVER["SERVER_PORT"] != "80") 
    {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
    } 
    else 
    {
        $pageURL .= $_SERVER["SERVER_NAME"].$folder;
    }
       
    $pm->IsHTML(true);
    $message =  $pm->Body;
       
    $DateTime = new DateTime($dateactivated);
    $dateactivated = $DateTime->format("m/d/Y H:i");

    $message = "<table style='width: 35%; font-family: Arial; color:#000000;'>";
    $message .= "<tr>
                <td align='center' style='background-color: #000000; color:#FFFFFF; font-weight:bold; text-align:center; height:30px;'>START DATE OF USAGE</td>

    <td align='center' style='background-color:#dcdcdc; height:30px; font-weight:bold;'>$dateactivated</td>
            </tr>
            <tr>
                <td align='center' style='background-color: #000000; color:#FFFFFF; font-weight:bold; text-align:center; height:30px;'>AVE. # of CARDS USED/DAY</td>
                <td align='center' style='background-color:#dcdcdc; height:30px; font-weight:bold;'>".number_format($ave)."</td>
            </tr>";
    $message .= "</table>";

//Deck Consumption
$projid = 2;
$deckusage = $deckinfo->GetDeckUsage($projid);
$cardcount = $deckusage[0]["DeckSize"];
$winningused = $deckusage[0]["WinningsUsed"];
$usedcardcount = $deckusage[0]["EntriesUsed"];
$percentage = (($usedcardcount / $cardcount )* 100);
$remainingEntries = ($cardcount - $usedcardcount);

$message .= "<table style='width:50%; font-family: Arial; color:#000000; float:left; margin-right: 5%;'>";
$message .= "<tr><td colspan='5' style='background-color: #000000; color:#FFFFFF; font-weight:bold; font-size:1.2em; text-align:center; height:30px; width:300px;'>DECK CONSUMPTION</td>";
$message .= "<tr style='background-color:#dcdcdc; height:30px; font-weight: bold;'>
        <td align='center'>Deck Size</td>
        <td align='center'>Total Winning Entries Used</td>
        <td align='center'>Total Entries Used</td>
        <td align='center'>Balance Entries</td>
        <td align='center'>Deck Percent Consumption (&#37;)</td>
      </tr>";

if(count($deckusage) != 0)
{
     $message .= "<tr style='background-color:#dcdcdc; height:30px;'><td align='center'>
            ".number_format($cardcount)."</td><td align='center'>
                ".number_format($winningused)."</td><td align='center'>
                    ".number_format($usedcardcount)."</td><td align='center'>
                        ".number_format($remainingEntries)."</td><td align='center'>
                            ".number_format($percentage, 2, '.', '')."&#37;</td></tr>";
}else
{
     $message .= "<tr><td colspan='3' align='center'><b>No Records Found.</b></td></tr>";
}

 $message .= "</table>";
 
//Prize Consumption

$getactivedeck = $deckinfo->GetActiveDeck($projid);
$activedeck = $getactivedeck[0][0];

$gettotalprize = $deckinfo->GetPrizeValue($activedeck);
$gettotalprizeused = $deckinfo->GetPrizeUsed($activedeck);

$totalprize = $gettotalprize[0][0];
$totalprizeused = $gettotalprizeused[0][0];

$prizeleft = $totalprize - $totalprizeused;
$percentage =(($totalprizeused/$totalprize) * 100) ;

$message .= "<table style='width:35%; font-family: Arial; color:#000000;'>";
$message .= "<tr><td colspan='4' style='background-color: #000000; color:#FFFFFF; font-weight:bold; font-size:1.2em; text-align:center; height:30px; width:300px;'>PRIZE CONSUMPTION (in USD)</td>";
$message .= "<tr style='background-color:#dcdcdc; height:30px; font-weight: bold;'>
        <td align='center'>Total Prize</td>
        <td align='center'>Prize Left</td>
        <td align='center'>Prize Used</td>
        <td align='center'>Prize Percent Consumption (&#37;)</td>
      </tr>";
if(count($gettotalprize) != 0 )
{
       $message .= "<tr style='background-color:#dcdcdc; height:30px;'><td align='center'>
            ".number_format($totalprize)."</td><td align='center'>
                ".number_format($prizeleft)."</td><td align='center'>
                    ".number_format($totalprizeused)."</td><td align='center'>
                        ".number_format($percentage, 2, '.', '')."&#37;</td></tr>";
}else
{
 $message .= "<tr><td colspan='3' align='center'><b>No Records Found.</b></td></tr>";
}
$message .= "</table>";

//prize counter
$message .= "<table style='width:90%; font-family: Arial; color:#000000; margin-top: 2%;'>";
$message .= "<tr><td colspan='6' style='background-color: #000000; color:#FFFFFF; font-weight:bold; font-size:1.2em; text-align:center; height:30px; width:300px;'>Prize Counter</td>";
$message .= "<tr style='background-color:#dcdcdc; height:30px; font-weight: bold;'>
        <td align='center'>Prize</td>
        <td align='center'>Total Quantity of Prize</td>
        <td align='center'>Total Quantity of Prize Used</td>
        <td align='center'>Prize Balance</td>
        <td align='center'>Percent Consumption (%)</td>
        <td align='center'>Amount of Prize Used (in USD)</td>
      </tr>";
$getprizes = $scprizes->GetPrizeID();

for($x = 0;$x<count($getprizes);$x++)
{
    $prizeid = $getprizes[$x]["ID"];
$prizedesc = $getprizes[$x]["PrizeDescription"];

$getactivedeck = $deckinfo->GetActiveDeck($projid);
$activedeck = $getactivedeck[0][0];
$getprizetype = $scprizes->GetPrizeTypeandValue($prizeid);
for($a = 0;$a<count($getprizetype);$a++)
{
$prizetype = $getprizetype[$a]["PrizeType"];
$prizevalue = $getprizetype[$a]["PrizeValue"];
}
       $getCardCount = $deckdetails->SelectCardCount($activedeck, $prizetype, $prizevalue);
for($b = 0;$b<count($getCardCount);$b++)
{       
       $cardcount = $getCardCount[$b]["CardCount"];   
}
$getCountandTotal = $deckinfo->GetCountCardCount($activedeck, $prizetype, $prizevalue);
 for($c = 0;$c<count($getCountandTotal);$c++)
{ 
      $cardcountperprize = $getCountandTotal[$c]["CardCntPerPrize"];
      $totalamount = $getCountandTotal[$c]["TotalAmount"];      
}
$balance = ($cardcount - $cardcountperprize);
$percentage = (($cardcountperprize / $cardcount ) * 100);

if(count($activedeck) != 0)
{


     $message .= "<tr style='background-color:#dcdcdc; height:30px;'><td align='center'>
            <b>$prizedesc</b></td><td align='center'>".number_format($cardcount)."
                </td><td align='center'>".number_format($cardcountperprize)."
                    </td><td align='center'>".number_format($balance)."
                        </td><td align='center'>".number_format($percentage, 2, '.', '')."
                            %</td><td align='center'>".number_format($totalamount)."</td></tr>";
}else
{
    $message .= "<tr><td colspan='3' align='center'><b>No Records Found.</b></td></tr>";
}
    
}
$message .= "</table>";

$pm->Body.=$message; 
          $pm->From = "no-reply@philwebasiapacific.com";
        $pm->FromName = "no-reply";
        $pm->Host = "localhost";
         $dateTime = new DateTime($date1);
        $date1 = $dateTime->format("Y-m-d");
        $pm->Subject = "The Sweeps Center Deck Usage ".date ( 'Y-m-d' , strtotime ( '-1 day' , strtotime ( date('Y-m-d') ) ) );
        $email_sent = $pm->Send();


?>
