<?php
/**
 * Created by JFJ 06-05-2012 
 */
require_once ("include/core/init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_TerminalSessionDetails");

App::LoadCore("PHPMailer.class.php");

$terminalsessiondetails = new SCC_TerminalSessionDetails();


$currdate = "now";
$ds = new DateSelector($currdate);
$date = $ds->PreviousDate;
$date1 = $ds->CurrentDate;


    $siteid = '0';
    $dateFrom = $date;
    $dateFrom = date_create($dateFrom);
    $dateFrom = date_format($dateFrom, "Y-m-d 09:59:59.99999");
    $dateTo = $date1;
   // $dateinterval = new DateInterval('P1D');
    $dateTo = date_create($dateTo);
   // $dateTo = date_add($dateTo, $dateinterval);
    $dateTo = date_format($dateTo, "Y-m-d 10:00:00.00000");
    $pagedeposit = 0;
    $pagereload = 0;
    $pageredemption = 0;
    $pagesales = 0;
    $totaldeposit = 0;
    $totalreload = 0;
    $totalredemption = 0;
    $totalsales = 0;

    if ($siteid == 0)
    {
        $terminalsessiondetails->StartTransaction();
        $getholdhistory = $terminalsessiondetails->SelectGrossHoldHistory($dateFrom, $dateTo);
    }else
    {
           $terminalsessiondetails->StartTransaction();
           $getholdhistory = $terminalsessiondetails->SelectGrossHoldHistorybySiteID($siteid, $dateFrom, $dateTo);
    }




    if(isset($getholdhistory))
    {
    for($i=0;$i<count($getholdhistory);$i++)
    {
        $pagedeposit = $pagedeposit + $getholdhistory[$i]["Sales"];
        $pagereload = $pagereload + $getholdhistory[$i]["Reload"];
        $pageredemption = $pageredemption + $getholdhistory[$i]["Redemption"];
        $pagesales = $pagesales + $getholdhistory[$i]["CashOnHand"];

    }
        // getting the total..
        $totaldeposit = $totaldeposit + $pagedeposit;
        $totalreload = $totalreload + $pagereload;
        $totalredemption = $totalredemption + $pageredemption;
        $totalsales = $totalsales + $pagesales;
        
        
        $pm = new PHPMailer();
        $pm->AddAddress("mccalderon@philweb.com.ph", "Tere Calderon");
        
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        $folder = $_SERVER["REQUEST_URI"];
        $folder = substr($folder,0,strrpos($folder,'/') + 1);
        if ($_SERVER["SERVER_PORT"] != "80") 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
        } 
        else 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].$folder;
        }
       
       $pm->IsHTML(true);
       $message =  $pm->Body;
       $message = "<table style='font-family: Arial; color:#000000;'>
                <tr>
                    <th colspan='7' style='height:30px; background-color: #FF9C42; color:#000000;'>CASH ON HAND SUMMARY</th>
                </tr>";
       
        $message .= "<tr style='background-color:#FFF1E6; height:30px;'>
                        <td></td><td></td><td></td>
                        <td>&nbsp;<b>Total Sales: </b>&nbsp;<b>".number_format($totaldeposit,2)."</b>&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;<b>Total Reload: </b>&nbsp;<b>".number_format($totalreload,2)."</b>&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;<b>Total Redemption: </b>&nbsp;<b>".number_format($totalredemption,2)."</b>&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;<b>Total Cash On Hand: </b>&nbsp;<b>".number_format($totalsales,2)."</b>&nbsp;&nbsp;&nbsp;</td>
                     </tr>";
        
        $message .= "<tr>
                <th style='background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:230px; text-align:center;'>Date</th>
                <th style='background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:260px; text-align:center;'>POS Account Name</th>
                <th style='background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:100px; text-align:center;'>Terminal Name</th>
                <th style='background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:215px; text-align:center;'>Sales</th>
                <th style='background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:215px; text-align:center;'>Reloads</th>
                <th style='background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:180px; text-align:center;'>Cash Redemptions</th>
                <th style='background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:215px; text-align:center;'>Cash On Hand</th>
                </tr>";
        
        if (count($getholdhistory[0][0]) !=0)
        {
         $color = 1;
        for($i=0;$i<count($getholdhistory);$i++)
        {
            $transdate = $getholdhistory[$i]["TransDate"];
            $DateTime = new DateTime($transdate);
            $transdate = $DateTime->format("Y-m-d h:i:s A");
            $name = "".$getholdhistory[$i]["LastName"].",".$getholdhistory[$i]['MiddleName'].",".$getholdhistory[$i]['FirstName']."";      
            $sales = $getholdhistory[$i]["Sales"];
            $reload = $getholdhistory[$i]["Reload"];
            $redemption = $getholdhistory[$i]["Redemption"];
            $cashonhand = $getholdhistory[$i]["CashOnHand"];
            $terminalname = $getholdhistory[$i]["TerminalName"];
        if ($color == 1)
        {
            $message .= "<tr style='background-color:#FFF1E6; height:30px;'>
                                <td class='td'>".$transdate."</td>
                                <td class='td'>".$name."</td>
                                <td class='td'>".$terminalname."</td>
                                <td class='td'>".number_format($sales,2)."</td>
                                <td class='td'>".number_format($reload,2)."</td>
                                <td class='td'>".number_format($redemption,2)."</td>
                                <td class='td'>".number_format($cashonhand,2)."</td>
                             </tr>";
         
            $color = 2;
        }else
        {
            $message .= "<tr style='height:30px;'>
                                <td class='td'>".$transdate."</td>
                                <td class='td'>".$name."</td>
                                <td class='td'>".$terminalname."</td>
                                <td class='td'>".number_format($sales,2)."</td>
                                <td class='td'>".number_format($reload,2)."</td>
                                <td class='td'>".number_format($redemption,2)."</td>
                                <td class='td'>".number_format($cashonhand,2)."</td>
                             </tr>";
            
            
            $color = 1;
         }
        }
        }else
        {
             $message .= "<tr><td colspan='7' align='center'><b>No Records Found.</b></td></tr>";
        }
        
        $message .= "</table>";
          $pm->Body.=$message; 
          $pm->From = "no-reply@philwebasiapacific.com";
        $pm->FromName = "no-reply";
        $pm->Host = "localhost";
         $dateTime = new DateTime($date);
        $date = $dateTime->format("Y-m-d");
        $pm->Subject = "The Sweeps Center Revenue for ".$date;
        $email_sent = $pm->Send();
}

?>
