<?php
/*
 * Created By: Jerome F. Jose
 * Date Created: November 20, 2012
 * Purpose: creating auto generated decks.
 */

require_once ("include/core/init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCC_Winners");
App::LoadModuleClass($modulename,"SCCC_TransactionSummary");
App::LoadModuleClass($modulename,"SCCC_DeckInfo");
App::LoadModuleClass($modulename,"SCCC_DeckDetails");
App::LoadModuleClass($modulename,"SCCC_AuditLog");
APp::LoadModuleClass($modulename,"SCCC_ref_ControlNumber");

App::LoadCore("PHPMailer.class.php");

$scdeckinfo = new SCCC_DeckInfo();
$deckdetails = new SCCC_DeckDetails();
$auditlog = new SCCC_AuditLog();
$ref_controlnumber = new SCCC_ref_ControlNumber();

    $projid = 2;
     //check if project id has an active deck
    $getstats = $scdeckinfo->SelectStatus($projid);
  
    if(Count($getstats) > 0)
    {
        $status = 0;
    }else
    {
        $status = 1;
    }
    $countquedeck = $scdeckinfo->SelectCountquedeck($projid);
    if($countquedeck[0]["QueuedDeckCnt"] > 2)
    {
        echo "There are still enough decks on queue.";
        $deckid = 0;
    }
        $countquedecks = $countquedeck[0]["QueuedDeckCnt"];
        // get deck count
        $deckcount = 3 - $countquedecks;
        //get LAST DECK ID
        $selectlastdeck = $scdeckinfo->SelectLastDeckID($projid);
        $lastdeck = $selectlastdeck[0]["LastDeckID"];
        $cardcount = $selectlastdeck[0]["CardCount"];
        $vchr_CharsToUse = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $int_RandAlphaLength = STRLEN($vchr_CharsToUse);
        $length = 10;
        $vchr_ECN = "";
        
       for($a=0;$a<$deckcount;$a++)
     {
                // -- insert into deck info table
                $scdeckinfo->StartTransaction();
                $deckparam["CardCount"] = $cardcount;
                $deckparam["CreatedByAID"] = "0";
                $deckparam["ProjectID"] = $projid;
                $deckparam["Status"] = $status;
                $deckparam["DateCreated"] = "now_usec()";
                $deckparam["Option1"] = "mysql_job";
                $scdeckinfo->Insert($deckparam);
                if($scdeckinfo->HasError)
                {
                    $msg = $scdeckinfo->getError();
                    $scdeckinfo->RollBackTransaction();
                }else
                {
                $scdeckinfo->CommitTransaction();
                // deck id
                $deckid = $scdeckinfo->LastInsertID;
             
                //create new deck 
                $generatenewdeck = $scdeckinfo->GenerateNewDeck($deckid);
                
                        //insert in audittrail
                        $auditlog->StartTransaction();
                        $auditlogparam["SessionID"] = "";
                        $auditlogparam["AccountID"] = "0";
                        $auditlogparam["TransDetails"] = "Auto-generated deck id:".$deckid." for project id:".$projid."";
                        $auditlogparam["RemoteIP"] = "";
                        $auditlogparam["TransDateTime"] = "now_usec()";
                        $auditlog->Insert($auditlogparam);
                        if($auditlog->HasError)
                        {
                            $msg = $auditlog->getError();
                            $auditlog->RollBackTransaction();

                        }else
                        {
                            $auditlog->CommitTransaction();
                        } //end of audit trail
                        
                        $getprizetype = $deckdetails->GetPrizetype($lastdeck);
                        $totalcardcount = 0;
                        $currentcounter = 0;
                       
                        for($b=0;$b<count($getprizetype);$b++)
                        {
                            $prizetype = $getprizetype[$b]["PrizeType"];
                            $prizevalue = $getprizetype[$b]["PrizeValue"];
                            $cardcount1 = $getprizetype[$b]["CardCount"];
                            $prizedesc = $getprizetype[$b]["PrizeDescription"];
                            $totalcardcount += $totalcardcount + $cardcount1;
                            
                            $deckdetails->StartTransaction();
                            $deckdetailsparam["DeckID"] = $deckid ;
                            $deckdetailsparam["ProjectID"] = $projid ;
                            $deckdetailsparam["PrizeType"] = $prizetype ;
                            $deckdetailsparam["PrizeValue"] = $prizevalue ;
                            $deckdetailsparam["PrizeDescription"] = $prizedesc ;
                            $deckdetailsparam["CardCount"] = $cardcount1 ;
                            $deckdetails->Insert($deckdetailsparam);
                            if($deckdetails->HasError)
                            {
                                $msg = $deckdetails->getError();
                                $deckdetails->RollBackTransaction();
                            }else
                            {
                                $deckdetails->CommitTransaction();
                                $abspath = dirname(__FILE__) . "/";
                                $fp = fopen($abspath . 'generatedcards.csv', 'w');
                            if($lastdeck != $deckid)
                            {
                                
                                if ($prizetype == 1 || $prizetype == 6)//cash prize type
                                {
                                    if ($prizevalue == 0)
                                    {
                                        //non-winning
					for ($y = 0; $y < $cardcount1; $y++)
					{
                                            //  Generate random ECN 
						$vchr_ECN = "";
						$ECN = array();
						for ($p = 0; $p < $length; $p++)
						{
							$vchr_ECN .= ($p%2) ? $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1) )] : $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1) )];
						}
                                                $ECN["DeckID"] = str_pad($deckid, 3, "0", STR_PAD_LEFT);
                                                $ECN["ECN"] = str_pad($deckid, 3, "0", STR_PAD_LEFT) . $vchr_ECN;
                                                $ECN["IsWinning"] = 0;
                                                $ECN["PrizeType"] = $prizetype;
                                                $ECN["PrizeValue"] = $prizevalue;
                                                $arrECN[] = $ECN;
                                                $currentcounter += 1;
                                        }
                                        
                                    }
                                    else
                                    { //winning
                                        for ($y = 0; $y < $cardcount1; $y++)
					{
                                            //  Generate random ECN 
						$vchr_ECN = "";
						$ECN = array();
						for ($p = 0; $p < $length; $p++)
						{
							$vchr_ECN .= ($p%2) ? $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1) )] : $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1) )];
						}
                                                $ECN["DeckID"] = str_pad($deckid, 3, "0", STR_PAD_LEFT);
                                                $ECN["ECN"] = str_pad($deckid, 3, "0", STR_PAD_LEFT) . $vchr_ECN;
                                                $ECN["IsWinning"] = 1;
                                                $ECN["PrizeType"] = $prizetype;
                                                $ECN["PrizeValue"] = $prizevalue;
                                                $arrECN[] = $ECN;
                                                $currentcounter += 1;
                                        }
                                        
                                    }
                                }  
                                else //non-cash prize type
                                 {
                                    for ($y = 0; $y < $cardcount1; $y++)
					{
                                            //  Generate random ECN 
						$vchr_ECN = "";
						$ECN = array();
						for ($p = 0; $p < $length; $p++)
						{
							$vchr_ECN .= ($p%2) ? $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1) )] : $vchr_CharsToUse[mt_rand(0, ($int_RandAlphaLength - 1) )];
						}
                                                $ECN["DeckID"] = str_pad($deckid, 3, "0", STR_PAD_LEFT);
                                                $ECN["ECN"] = str_pad($deckid, 3, "0", STR_PAD_LEFT) . $vchr_ECN;
                                                $ECN["IsWinning"] = 1;
                                                $ECN["PrizeType"] = $prizetype;
                                                $ECN["PrizeValue"] = $prizevalue;
                                                $arrECN[] = $ECN;
                                                $currentcounter += 1;
                                        }

                                 }
                        
                        
                            } 
                        }
                        }
                                    $handle = fopen($abspath . "generatedcards.csv", "r");
				    $dir = dirname(__FILE__);				    
                                    $csvfile = $dir . "generatedcards.csv";
                                    $fieldterminator = ",";
                                    $lineterminator = "\n";
                                    $sample_deckid = $deckid;

                                    $query2 = "LOAD DATA LOCAL INFILE '$csvfile'
                                                            INTO TABLE chancecards.deck_$sample_deckid
                                                            FIELDS TERMINATED BY '$fieldterminator'
                                                            LINES TERMINATED BY '$lineterminator'
                                                            (DeckID,ECN,IsWinning,PrizeType,PrizeValue)";
                                         $scdeckinfo->InsertToDeck($query2);
                             
                                    //random data
                                    $keys = array_keys($arrECN);
                                    shuffle($keys);
                                    $strECN = "";

                                for ($i = 0; $i < count($keys); $i++)
                                {
                                    $currentkey = $keys[$i];
                                    $newECN[] = $arrECN[$currentkey];
                                    $strECN .= $arrECN[$currentkey]["DeckID"] . "," . $arrECN[$currentkey]["ECN"] . "," . $arrECN[$currentkey]["IsWinning"] . "," . $arrECN[$currentkey]["PrizeType"] . "," . $arrECN[$currentkey]["PrizeValue"] . "\r\n";
                                }
                                $arrECN = $newECN;
                                foreach ($arrECN as $fields)
                                    {
                                    fputcsv($fp, $fields);
                                    }
                               
                                while (($data = fgetcsv($handle, 0, ",")) !== FALSE) 
                                   {
                                    $sql = 'INSERT INTO chancecards.deck_'.$deckid.' (DeckID,ECN,IsWinning,PrizeType,PrizeValue,Status) VALUES (\''.$data[0].'\',\''.$data[1].'\',\''.$data[2].'\',\''.$data[3].'\',\''.$data[4].'\',\'0\')';
                                    $scdeckinfo->InsertToDeck($sql);
                                   }
                                    fclose($fp);
                                    fclose($handle);
                                    $myFile = $abspath . "generatedcards.csv";
                                    unlink($myFile);
                                    
                                    
                                    // Insert to ref_controlnumber
                                    $ref_controlnumber->StartTransaction();
                                    $ref_controlnumber->Truncate();
                                    $data = $scdeckinfo->GetDeckID();
                                    $ref_controlnumber->CommitTransaction();
                                    for ($i = 0; $i < count($data); $i++) 
                                    {
                                        $ctrnum["DeckID"] = $data[$i]["DeckID"];
                                        $ref_controlnumber->Insert($ctrnum);
                                    if ($ref_controlnumber->HasError) 
                                        {
                                            $ref_controlnumber->RollBackTransaction();
                                            $msg = $ref_controlnumber->getError();
                                        } 
                                        else
                                        {
                                            $scdeckinfo->UpdateDeckbyControlNo();
                                        }
                                    }
                        
                        $keys = NULL;
                        $newECN = NULL;
                        $arrECN = NULL;
                }
                                    
     }

?>