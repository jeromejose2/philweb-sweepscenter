<?php
//Runs every 5 AM(Guam Time)

$dir = dirname(__FILE__);
$dir = substr($dir,0,strpos($dir,'cron'));

$dblogfilesize =  filesize($dir."DBLogs/logs.txt");

if($dblogfilesize >= 5242880)
{
    //rename current log file
    $datenow = date("mdY");
    rename($dir."DBLogs/logs.txt",$dir."DBLogs/logs_".$datenow.".txt");

    //create new log file
    $filename = $dir."DBLogs/logs.txt";
    fopen($filename, 'w') or die("can't open file");
    fclose($filename);

    chmod($filename, octdec(777));

    chown($filename, "www-data");
    chgrp($filename, "www-data");
}
else
{

}

$apilogfilesize =  filesize($dir."APILogs/logs.txt");

if($apilogfilesize >= 5242880)
{
    //rename current log file
    $datenow = date("mdY");
    rename($dir."APILogs/logs.txt",$dir."APILogs/logs_".$datenow.".txt");

    //create new log file
    $filename = $dir."APILogs/logs.txt";
    fopen($filename, 'w') or die("can't open file");
    fclose($filename);

    chmod($filename, octdec(777));

    chown($filename, "www-data");
    chgrp($filename, "www-data");
}
else
{

}

$browselogfilesize =  filesize($dir."BrowseInternetLogs/logs.txt");

if($browselogfilesize >= 5242880)
{
    //rename current log file
    $datenow = date("mdY");
    rename($dir."BrowseInternetLogs/logs.txt",$dir."BrowseInternetLogs/logs_".$datenow.".txt");

    //create new log file
    $filename = $dir."BrowseInternetLogs/logs.txt";
    fopen($filename, 'w') or die("can't open file");
    fclose($filename);

    chmod($filename, octdec(777));

    chown($filename, "www-data");
    chgrp($filename, "www-data");
}
else
{
    
}

$xmllogfilesize =  filesize($dir."XMLLogs/apixml.txt");

if($xmllogfilesize >= 5242880)
{
    //rename current log file
    $datenow = date("mdY");
    rename($dir."XMLLogs/apixml.txt",$dir."XMLLogs/apixml_".$datenow.".txt");

    //create new log file
    $filename = $dir."XMLLogs/apixml.txt";
    fopen($filename, 'w') or die("can't open file");
    fclose($filename);

    chmod($filename, octdec(777));

    chown($filename, "www-data");
    chgrp($filename, "www-data");
}
else
{

}
?> 