<?php 

/**
 * Created by JFJ 06-05-2012 
 */
require_once ("include/core/init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCC_Winners");
App::LoadModuleClass($modulename,"SCCC_TransactionSummary");
App::LoadCore("PHPMailer.class.php");

$winners = new SCCC_Winners();
$transactionsummary = new SCCC_TransactionSummary();
$currdate = "now";
$ds = new DateSelector($currdate);
$date = $ds->PreviousDate;
$date1 = $ds->CurrentDate;


    $siteid = '0';
    $projid = 2;
    $gametype = 0;
    
    $dateFrom = $date;
    $dateFrom = date_create($dateFrom);
    $dateFrom = date_format($dateFrom, "Y-m-d 09:59:59.99999");
    $dateTo = $date1;
    //$dateinterval = new DateInterval('P1D');
    $dateTo = date_create($dateTo);
    //$dateTo = date_add($dateTo, $dateinterval);
    $dateTo = date_format($dateTo, "Y-m-d 10:00:00.00000");

        $pm = new PHPMailer();
        $pm->AddAddress("mccalderon@philweb.com.ph", "Tere Calderon");
        
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        $folder = $_SERVER["REQUEST_URI"];
        $folder = substr($folder,0,strrpos($folder,'/') + 1);
        if ($_SERVER["SERVER_PORT"] != "80") 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
        } 
        else 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].$folder;
        }
       
        $pm->IsHTML(true);
       $message =  $pm->Body;
    
        $message = "<table style='width:100%; font-family: Arial; color:#000000;'>
                <tr>
                    <th colspan='3' style='height:30px; background-color: #FF9C42; color:#000000;'>REDEMPTION HISTORY</th>
                </tr>";
        if($gametype == 2)
        {
            $freeentry = 1;
        }else if ($gametype == 1)
        {
            $freeentry = 0;
        }

        if($projid ==1 )
        {
            $winners->StartTransaction();
            $winners->SelectDetailsbyProjID($projid, $dateFrom, $dateTo);
        }
        if($projid == 2)
        {
            if($siteid == 0)
            {
            $gettotalcash = $winners->SelectTotalCashRedemption($projid, $dateFrom, $dateTo);
            $getnontotalcash = $winners->SelectNonTotalCashRedemption($projid, $dateFrom, $dateTo);
            $totalcash = $gettotalcash[0]["TotalCash"];
            $nontotalcash = $getnontotalcash[0]["TotalNonCash"];
                ($totalcash == null) ? 0 : $totalcash;
            ($nontotalcash == null) ? 0 : $nontotalcash;
            $selecttranssummary = $transactionsummary->SelectRedemptionHistory($gametype, $freeentry, $dateFrom, $dateTo);

            }else
            {
                $gettotalcash = $winners->SelectTotalCashRedemptionbySiteId($siteid,$projid, $dateFrom, $dateTo);
                $getnontotalcash = $winners->SelectNonTotalCashRedemptionbySiteId($siteid,$projid, $dateFrom, $dateTo);
                $totalcash = $gettotalcash[0]["TotalCash"];
                $nontotalcash = $getnontotalcash[0]["TotalNonCash"];
                    ($totalcash == null) ? 0 : $totalcash;
                ($nontotalcash == null) ? 0 : $nontotalcash;
                $selecttranssummary = $transactionsummary->SelectRedemptionHistorybySiteId($siteid, $gametype, $freeentry, $dateFrom, $dateTo);
            }
            }



            $message .= "<tr style='background-color:#FFF1E6; height:30px;'><td colspan='2'>&nbsp;<b>Total Cash Redemptions</b></td><td>&nbsp;<b>".number_format($totalcash,2)."</b></td></tr>";
            $message .= "<tr style='background-color:#FFF1E6; height:30px;'><td colspan='2'>&nbsp;<b>Total Value of Non-Cash Redemptions</b></td><td>&nbsp;<b>".number_format($nontotalcash,2)."</b></td></tr>";


            $message .= "<tr>
                            <th style='width:30%; background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:215px; text-align:center;'>Date Processed</th>
                <th style='width:30%; background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:215px; text-align:center;'>POS Account Name</th>
                <th style='width:40%; background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:215px; text-align:center;'>Withdrawal Details</th>
                </tr>";


if(count($selecttranssummary) != 0)
{
        $color = 1;
      for($i = 0;$i<count($selecttranssummary);$i++)
      {
          $transactionNumber = $selecttranssummary[$i]["TransactionNumber"];
          $transactionNumber = str_pad($transactionNumber, 10,0,STR_PAD_LEFT);
          $terminalname = $selecttranssummary[$i]["TerminalName"];
           $winnings = $selecttranssummary[$i]["Winnings"];
          $entrytype = $selecttranssummary[$i]["EntryType"];
          if($entrytype == 0)
          {
              $entrytype = "Regular Gaming";
          }else{
              $entrytype = "Free Entry";
          }
          $dateclaimed = $selecttranssummary[$i]["DateClaimed"];
          $dateTime = new DateTime($dateclaimed);
          $dateclaimed = $dateTime->format("Y-m-d h:i:s A");
         $posAccountName = "".$selecttranssummary[$i]["LastName"].",".$selecttranssummary[$i]['FirstName'].",".$selecttranssummary[$i]['MiddleName']."";      
      
           if($color == 1)
            {
                $message .= "<tr style='background-color:#FFF1E6; height:30px;'>
                                <td class='td'>".$dateclaimed."</td>
                                <td class='td'>".$posAccountName."</td>
                                <td class='td'>
                                    <table>
                                        <tr>
                                            <td><b>Transaction Number:</b></td>
                                            <td>".$transactionNumber."</td>
                                        </tr>
                                        <tr>
                                            <td><b>Terminal Name:</b></td>
                                            <td>".$terminalname."</td>
                                        </tr>
                                        <tr>
                                            <td><b>Entry Type:</b></td>
                                            <td>".$entrytype."</td>
                                        </tr>
                                        <tr>
                                            <td><b>Winnings:</b></td>
                                            <td>".$winnings."</td>
                                        </tr>
                                     </table>
                                 </td>
                             </tr>";

                $color = 2;
            }
            else
            {
                $message .= "<tr style='height:30px;'>
                                 <td class='td'>".$dateclaimed."</td>
                                <td class='td'>".$posAccountName."</td>
                                <td class='td'>
                                    <table>
                                        <tr>
                                            <td><b>Transaction Number:</b></td>
                                            <td>".$transactionNumber."</td>
                                        </tr>
                                        <tr>
                                            <td><b>Terminal Name:</b></td>
                                            <td>".$terminalname."</td>
                                        </tr>
                                        <tr>
                                            <td><b>Entry Type:</b></td>
                                            <td>".$entrytype."</td>
                                        </tr>
                                        <tr>
                                            <td><b>Winnings:</b></td>
                                            <td>".$winnings."</td>
                                        </tr>
                                     </table>
                                 </td>
                             </tr>";

                $color = 1;
            }
         
      }
}else
    {
        $message .= "<tr><td colspan='3' align='center'><b>No Records Found.</b></td></tr>";
    }

    $message .= "</table>";
    $pm->Body.=$message; 
        $pm->From = "no-reply@philwebasiapacific.com";
        $pm->FromName = "no-reply";
        $pm->Host = "localhost";
        $dateTime = new DateTime($date);
        $date = $dateTime->format("Y-m-d");
        $pm->Subject = "The Sweeps Center Daily Redemptions Summary for ".$date;
        $email_sent = $pm->Send();
?>
