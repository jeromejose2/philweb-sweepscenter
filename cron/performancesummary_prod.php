<?php
/*
* Added by JFJ 06-06 2012
* 1st Modified By: Noel Antonio 11-05-2012
*/
require_once("include/core/init.inc.php");
include 'class.phpmailer.php';
$modulename = "SweepsCenter";
App::LoadModuleClass($modulename,"SCC_TerminalSessionDetails");
App::LoadModuleClass($modulename,"SCCC_Winners");
App::LoadModuleClass($modulename,"SCCC_PointsConversionInfo");

$terminalsessiondetails = new SCC_TerminalSessionDetails();
$winners = new SCCC_Winners();
$pointsconversioninfo = new SCCC_PointsConversionInfo();

if (date('d') == '01')
{
$todayDate = date("Y-m-d");
$FromDate = strtotime(date("Y-m-d", strtotime($todayDate)) . " -1 month");
$FromDate = date("Y-m-d", $FromDate);
$dateFr = $FromDate. ' 10:00:00.00000';
$dateTo = $todayDate . ' 09:59:59.99999';
}
else
{
$dateFr = date('Y-m-d', mktime(0, 0, 0, date("m"), '01', date("Y"))) . ' 10:00:00.00000';
$dateTo = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d"), date("Y"))) . ' 09:59:59.99999';
}

if (date('d') == 01 && date('m') == 01)
{
    $first_day_year = date("Y-m-d", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . " -12 months")) . ' 10:00:00.00000';
}
else
{
    $first_day_year = date('Y-m-d', mktime(0, 0, 0, '01', '01', date("Y"))) . ' 10:00:00.00000';
}

/************MONTH TO DATE************/
//get MTD Deposits and Reloads
$arrMTDDep = $terminalsessiondetails->GetTotalDeposits($dateFr, $dateTo);
if(count($arrMTDDep) == 1 && is_array($arrMTDDep))
{
    $mtd_total_deposits = $arrMTDDep[0]['D'] + $arrMTDDep[0]['R'];
}
else
{
    $mtd_total_deposits = 0;
}

//get MTD Redemptions (Cash)
$arrMTDRed = $terminalsessiondetails->GetTotalRedemptions($dateFr, $dateTo);
if(count($arrMTDRed) == 1 && is_array($arrMTDRed))
{
    $mtd_total_redemptions = $arrMTDRed[0]['Redeemed'];
}
else
{
    $mtd_total_redemptions = 0;
}

//get MTD Redemptions (Non-Cash)
$arrMTDRedNonCash = $winners->GetTotalRedemptionsFreeEntry($dateFr, $dateTo);
if(count($arrMTDRedNonCash) == 1 && is_array($arrMTDRedNonCash))
{
    $mtd_total_redemptions = $mtd_total_redemptions + $arrMTDRedNonCash[0]['NonCashTotals'];
}
else
{
    $mtd_total_redemptions = $mtd_total_redemptions + 0;
}

//get MTD Sweeps Input
$arrMTDSweepsInput = $pointsconversioninfo->GetTotalSweepsInput($dateFr, $dateTo);
if(count($arrMTDSweepsInput) == 1 && is_array($arrMTDSweepsInput))
{
    $mtd_total_swcinput = $arrMTDSweepsInput[0]['SweepsInput'];
}
else
{
    $mtd_total_swcinput = 0;
}

$mtd_gross_holds = $mtd_total_deposits - $mtd_total_redemptions;
$mtd_total_mg_winnings = $mtd_total_deposits - $mtd_total_swcinput;
$mtd_total_sweeps_revenue = $mtd_gross_holds - $mtd_total_mg_winnings;


/************YEAR TO DATE************/
//get YTD Deposits and Reloads
$arrYTDDep = $terminalsessiondetails->GetTotalDeposits($first_day_year, $dateTo);
if(count($arrYTDDep) == 1 && is_array($arrYTDDep))
{
    $ytd_total_deposits = $arrYTDDep[0]['D'] + $arrYTDDep[0]['R'];
}
else
{
    $ytd_total_deposits = 0;
}


//get YTD Redemptions (Cash)
$arrYTDRed = $terminalsessiondetails->GetTotalRedemptions($first_day_year, $dateTo);
if(count($arrYTDRed) == 1 && is_array($arrYTDRed))
{
    $ytd_total_redemptions = $arrYTDRed[0]['Redeemed'];
}
else
{
    $ytd_total_redemptions = 0;
}
    
//get previous 2012 cash redemptions
$year = date('Y');
if ($year == 2012)
{
    $arrYTDPrevRed = $winners->GetPreviousYTDRedemption();
    if(count($arrYTDPrevRed) == 1 && is_array($arrYTDPrevRed))
    {
        $ytd_total_prev_cash_redemptions = $arrYTDPrevRed[0]['PrevRedemptions'];
    }
    else
    {
        $ytd_total_prev_cash_redemptions = 0;
    }
    
    $ytd_total_redemptions = $ytd_total_redemptions + $ytd_total_prev_cash_redemptions;
}

//get YTD Redemptions (Non-Cash)
$arrYTDRedNonCash = $winners->GetTotalRedemptionsFreeEntry($first_day_year, $dateTo);
if(count($arrYTDRedNonCash) == 1 && is_array($arrYTDRedNonCash))
{
    $ytd_total_redemptions = $ytd_total_redemptions + $arrYTDRedNonCash[0]['NonCashTotals'];
}
else
{
    $ytd_total_redemptions = $ytd_total_redemptions + 0;
}

//get YTD Sweeps Input
$arrYTDSweepsInput = $pointsconversioninfo->GetTotalSweepsInput($first_day_year, $dateTo);
if(count($arrYTDSweepsInput) == 1 && is_array($arrYTDSweepsInput))
{
    $ytd_total_swcinput = $arrYTDSweepsInput[0]['SweepsInput'];
}
else
{
    $ytd_total_swcinput = 0;
}

$ytd_gross_holds = $ytd_total_deposits - $ytd_total_redemptions;
$ytd_total_mg_winnings = $ytd_total_deposits - $ytd_total_swcinput;
$ytd_total_sweeps_revenue = $ytd_gross_holds - $ytd_total_mg_winnings;

/* -- Month to Date Summary -- */
$message = "<table style=\"padding:5px; font-weight:bold; font-family: Arial;\"><tr><td>";
$message .= "<table border=\"1\" bordercolor=\"#000\" style=\"font-weight:bold; font-family: Arial; color:#000000; border: solid 1px #000; border-collapse: collapse;\">";
$message .= "<tr align='center'><td width=\"400px\" colspan=\"2\" style=\"background-color: #FFBB7D;\">Month to Date Summary</td></tr>";
$message .= "<tr style='background-color:#FFF1E6; height:20px;'><td width=\"300px\">MTD Deposit</td><td width=\"100px\">".number_format($mtd_total_deposits, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='height:20px;'><td>MTD Sweeps Cash & Non-Cash Redemptions</td><td>".number_format($mtd_total_redemptions, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='background-color:#FFF1E6; height:20px;'><td>MTD Sweeps Center Gross Holds</td><td>".number_format($mtd_gross_holds, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='height:20px;'><td>MTD Sweeps Center Input</td><td>".number_format($mtd_total_swcinput, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='background-color:#FFF1E6; height:20px;'><td>MTD MicroGaming Winnings</td><td>".number_format($mtd_total_mg_winnings, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='height:20px;'><td>MTD Sweeps Center Revenue</td><td>".number_format($mtd_total_sweeps_revenue, 0 , '.' , ',')."</td></tr>";
$message .= "</table></td>";

/* -- Year to Date Summary -- */
$message .= "<td>";
$message .= "<table border=\"1\" bordercolor=\"#000\" style=\"font-weight:bold; font-family: Arial; color:#000000; border: solid 1px #000; border-collapse: collapse;\">";
$message .= "<tr align='center'><td width=\"400px\" colspan=\"2\" style=\"background-color: #FFBB7D;\">Year to Date Summary</td></tr>";
$message .= "<tr style='background-color:#FFF1E6; height:20px;'><td width=\"300px\">YTD Deposit</td><td width=\"100px\">".number_format($ytd_total_deposits, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='height:20px;'><td>YTD Sweeps Cash & Non-Cash Redemptions</td><td>".number_format($ytd_total_redemptions, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='background-color:#FFF1E6; height:20px;'><td>YTD Sweeps Center Gross Holds</td><td>".number_format($ytd_gross_holds, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='height:20px;'><td>YTD Sweeps Center Input</td><td>".number_format($ytd_total_swcinput, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='background-color:#FFF1E6; height:20px;'><td>YTD MicroGaming Winnings</td><td>".number_format($ytd_total_mg_winnings, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='height:20px;'><td>YTD Sweeps Center Revenue</td><td>".number_format($ytd_total_sweeps_revenue, 0 , '.' , ',')."</td></tr>";
$message .= "</table></td>";
$message .= "</tr></table><br/>";

/* -- Performace Report -- */
$message .= "<table border=\"1\" bordercolor=\"#000\" style=\"width:100%; font-family: Arial; color:#000000; border: solid 1px #000; border-collapse: collapse; text-align: center;\">";
$message .= "<tr><td colspan=\"7\" style=\"background-color: #FF9C42; color:#000000; font-weight:bold; font-size:1.2em; text-align: center;\">The Sweeps Center Performance Report</td>";
$message .= "<tr style=\"font-weight: bold;\">
<td style='background-color: #FFBB7D; width:12%;'>Date</td>
<td style='background-color: #FFBB7D;'>Total Deposit</td>
<td style='background-color: #FFBB7D;'>Total Sweeps Cash & Non-Cash Redemptions</td>
<td style='background-color: #FFBB7D;'>Total Sweeps Center Gross Holds</td>
<td style='background-color: #FFBB7D;'>Total Sweeps Inputs</td>
<td style='background-color: #FFBB7D;'>Total Microgaming Winnings</td>
<td style='background-color: #FFBB7D;'>Total Sweeps Revenue</td>
</tr>";

        $ds = new DateTime($dateTo);
        $days_between = ceil(abs(strtotime($dateTo) - strtotime($dateFr)) / 86400);
        for ($i = 0;$i < $days_between - 1;$i++)
        {
                $current = $ds->format('Y-m-d') . ' 10:00:00.00000';
                $ds->modify('-1 days');
                $previous = $ds->format('Y-m-d') . ' 09:59:59.99999';
                $previous2 = $ds->format('Y-m-d');
                
                //daily deposits
                $arrDep = $terminalsessiondetails->GetTotalDeposits($previous, $current);
                if(count($arrDep) == 1 && is_array($arrDep))
                {
                    $total_deposits = $arrDep[0]['D'] + $arrDep[0]['R'];
                }
                else
                {
                    $total_deposits = 0;
                }
                //daily cash redemptions
                $arrRed = $terminalsessiondetails->GetTotalRedemptions($previous, $current);
                if(count($arrRed) == 1 && is_array($arrRed))
                {
                    $total_redemptions = $arrRed[0]['Redeemed'];
                }
                else
                {
                    $total_redemptions = 0;
                }
                //daily non-cash redemptions
                $arrRedNonCash = $winners->GetTotalRedemptionsFreeEntry($previous, $current);
                if(count($arrRedNonCash) == 1 && is_array($arrRedNonCash))
                {
                    $total_redemptions = $total_redemptions + $arrRedNonCash[0]['NonCashTotals'];
                }
                else
                {
                    $total_redemptions = $total_redemptions + 0;
                }
                //daily sweeps input
                $arrSweepsInput = $pointsconversioninfo->GetTotalSweepsInput($previous, $current);
                if(count($arrSweepsInput) == 1 && is_array($arrSweepsInput))
                {
                    $total_sweeps_input = $arrSweepsInput[0]['SweepsInput'];
                }
                else
                {
                    $total_sweeps_input = 0;
                }

                $total_gross_holds = $total_deposits - $total_redemptions;
                $total_mg_winnings = $total_deposits - $total_sweeps_input;
                $total_sweeps_revenue = $total_gross_holds - $total_mg_winnings;

                $mod = ($i % 2);
                $message .= ($mod == 0) ? "<tr style='background-color:#FFF1E6; height:20px;'>" : "<tr style='height:20px;'>";
                $message .= "<td>".date('m-d-Y', strtotime($previous2))."</td>
<td>".number_format($total_deposits,2)."</td>
<td>".number_format($total_redemptions,2)."</td>
<td>".number_format($total_gross_holds,2)."</td>
<td>".number_format($total_sweeps_input,2)."</td>
<td>".number_format($total_mg_winnings,2)."</td>
<td>".number_format($total_sweeps_revenue,2)."</td>
</tr>";

                $total_deposits = 0;
                $total_redemptions = 0;
                $total_gross_holds = 0;
                $total_mg_winnings = 0;
                $total_sweeps_revenue = 0;
        }
        
$message .= "</table><br/>";

/* -- Send email -- */
$subject = "[TEST]The Sweeps Center Performance Summary for ".date('Y-M-d');
$mailer = new PHPMailer();
$mailer->IsSMTP();

$mailer->AddAddress("mccalderon@philweb.com.ph", "Tere Calderon");
$mailer->AddBCC("mccalderon@philweb.com.ph", "Tere Calderon");

$mailer->From = 'no-reply@philwebasiapacific.com';
$mailer->FromName = 'no-reply';
$mailer->Host = 'localhost';
$mailer->isHtml = true;
$mailer->Subject = $subject;
$mailer->MsgHTML($message);
$mailer->AddEmbeddedImage('mnt/var/sites/www.thesweepscenter.com/graphimg/salesandredemptions.jpg', 'pic1', 'salesandredemptions.jpg', 'base64','image/jpeg');
$mailer->AddEmbeddedImage('mnt/var/sites/www.thesweepscenter.com/graphimg/totalgh.jpg', 'pic2', 'totalgh.jpg', 'base64','image/jpeg');

$mailer->Body = "<div style=\"font-size: 15px;\">&nbsp;</div>
<div>".$message."</div>
<br/><br/>
<img src='http://www.thesweepscenter.com/graphimg/totalgh.jpg' />
<img src='http://www.thesweepscenter.com/graphimg/salesandredemptions.jpg' />";

if(!$mailer->Send())
        echo "Error sending: " . $mailer->ErrorInfo;
else
        echo "Email sent";
?>
