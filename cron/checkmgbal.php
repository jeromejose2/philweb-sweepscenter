<?php

/*
 * Author: Noel Antonio
 * Date Created: November 6, 2012
 * Purpose: Checking MicroGaming Balance. (convert to RS Framework)
 */

ini_set('display_errors', 1);
ini_set('log_errors', 1);

require_once("include/core/init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCSM_AgentSessions");
App::LoadLibrary("MicrogamingAPI.class.php");
App::LoadCore("PHPMailer.class.php");

$ip = $_SERVER['REMOTE_ADDR'];

$csmagentsessions = new SCCSM_AgentSessions();
$arrSessionGUID_MG = $csmagentsessions->SelectSessionGUID();
$sessionGUID_MG = $arrSessionGUID_MG[0][0];

$headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
           "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
           "<IPAddress>".$ip."</IPAddress>".
           "<ErrorCode>0</ErrorCode>".
           "<IsLengthenSession>true</IsLengthenSession>".
           "</AgentSession>";

$client = new nusoap_client('https://entservices.totalegame.net/EntServices.asmx?WSDL', 'wsdl');
$client->setHeaders($headers);
$result = $client->call('GetMyBalance');

$member_balance = $result['GetMyBalanceResult']['MemberBalances']['MemberBalance']['Amount'];

if($member_balance != '')
{
    if($member_balance <= 5000)
    {
        $pm = new PHPMailer();
        
        $pm->AddAddress("mccalderon@philweb.com.ph", "Tere Calderon");
        
        $pm->IsHTML(true);
        //$pm->IsSMTP();

        $body = "<div>Good Day!</div>
                 <br/>
                 <div>This is to inform you that the current MicroGaming credit balance of Sweeps Center has reached the Credit Balance Threshold. Please log on to your TOTAL eGAME Management System to deposit credits to this account and replenish it's credit balance.</div>
                 <br/>
                 <div>Current Credit Balance is: ".number_format($member_balance, 2, '.', '')."</div>
                 <br/>
                 <div>To log on to your account, please click this url: <a href='https://www.totalegame.net/login.php'>https://www.totalegame.net</a>.</div>
                 <br/></br>
                 <div>Regards,</div>
                 <br/>
                 The Sweeps Center
                 <br/>
                 PhilWeb Guam Corporation
                 <br/><br/>

                 <div style='float: left; font-size: 10px; margin-top: 30px; font-gamily: Arial'>
                 <div>System generated email on ".date('D, j F Y H:i:s e')."</div>
                 <br/>
                 <div>The information in this email, and any attachments, may contain confidential information and is intended solely for the attention and use of the named addressee(s). It must not be disclosed to any person(s) without authorization. If you are not the intended recipient, or a person responsible for delivering it to the intended recipient, you are not authorized to, and must not, disclose, copy, distribute, or retain this message or any part of it. If you have received this communication in error, please notify the sender immediately.</div>";
        
        $pm->Body = $body;
        $pm->From = "no-reply@philwebasiapacific.com";
        $pm->FromName = "no-reply";
        $pm->Host = "localhost";
        $pm->Subject = "The Sweeps Center Notification for MicroGaming Credit Balance on Threshold Level";
        
        if(!$pm->Send())
                echo "Error sending: " . $pm->ErrorInfo;
        else
                echo "Email sent";
    }
    else
    {
        
    }
}
else
{
    
}
?>
