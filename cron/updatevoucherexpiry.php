<?php
include("init.inc.php");
$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCCV_VoucherInfo");
App::LoadModuleClass($modulename, "SCCV_AuditLog");

$svvoucherinfo = new SCCV_VoucherInfo();
$svauditlog = new SCCV_AuditLog();

$errormsg = "message";
$boolrollback = false;



//Update Voucher Info
$svvoucherinfo->StartTransaction();
$svvoucherinfo->UpdateExpiredVouchers();
if ($svvoucherinfo->HasError) 
{
    $errormsg = $svvoucherinfo->getError();
    $boolrollback = true;
}  

// Log to Audit Trail
$svauditlog->StartTransaction();
$scauditlogparam["TransDetails"] = "Update expired vouchers";
$scauditlogparam["IPAddress"] = "";
$scauditlogparam["DateCreated"] = "now_usec()";
$scauditlogparam["CreatedBy"] = "cron";

$svauditlog->Insert($scauditlogparam);
if ($svauditlog->HasError) 
{
    $errormsg = $svauditlog->getError();
    $boolrollback = true;
}  

// Commit and Rollback Transactions
if ($boolrollback)
{
    $svvoucherinfo->RollBackTransaction();
    $svauditlog->RollBackTransaction();
} else {
    $svvoucherinfo->CommitTransaction();
    $svauditlog->CommitTransaction();
}

?>
