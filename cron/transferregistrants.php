<?php

/*
 * Author: Noel Antonio
 * Date Created : November 6, 2012
 * Purpose: To transfer registrants from Free Entry. (converted to RS Framework)
 */

require_once("include/core/init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_FreeEntry");
App::LoadModuleClass($modulename, "SCC_FreeEntryRegistrants");
App::LoadModuleClass($modulename, "SCC_AuditTrail");

$cfreeentry = new SCC_FreeEntry();
$cfreeentryregistrants = new SCC_FreeEntryRegistrants();
$caudittrail = new SCC_AuditTrail();

$ds = new DateSelector("now");
$previousdate = $ds->PreviousDate;

$records = $cfreeentry->SelectCount();
$count = $records[0][0];
if ($count > 0)
{
    $cfreeentryregistrants->StartTransaction();
    $cfreeentryregistrants->InsertRegistrantsFromFreeEntry($previousdate);
    if ($cfreeentryregistrants->HasError)
    {
        $cfreeentryregistrants->RollBackTransaction();
        echo $cfreeentryregistrants->getErrors();
    }
    else
    {
        $cfreeentryregistrants->CommitTransaction();

        $cfreeentry->StartTransaction();
        $cfreeentry->TruncateTable();
        if ($cfreeentry->HasError)
        {
            $cfreeentry->RollBackTransaction();
            echo $cfreeentry->getErrors();
        }
        else
        {
            $cfreeentry->CommitTransaction();

            $caudittrail->StartTransaction();
            $arrAuditTrail["SessionID"] = '';
            $arrAuditTrail["AccountID"] = 0;
            $arrAuditTrail["TransDetails"] = "Transfer Registrants for " . $previousdate;
            $arrAuditTrail["RemoteIP"] = '';
            $arrAuditTrail["TransDateTime"] = 'now_usec()';
            $caudittrail->Insert($arrAuditTrail);
            if ($caudittrail->HasError)
            {
                $caudittrail->RollBackTransaction();
                echo $caudittrail->getErrors();
            }
            else
            {
                $caudittrail->CommitTransaction();

                //$filename = "/home/webadmin/www/tvsabong/jobs/cronlogs.txt";
                $dir = dirname(__FILE__);
                $filename = $dir . "cronlogs.txt";
                $fp = fopen($filename , "a");
                fwrite($fp, date("Y-m-d H:i:s") . " :  0  : Registrants successfully transferred. \r\n");
                fclose($fp);
            }
        }
    }
}
?>
