<?php
include("init.inc.php");
$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_FreeEntryRegistrants");

$scfreeentryregistrants = new SCC_FreeEntryRegistrants();

$boolrollback = false;

$scfreeentryregistrants->StartTransaction();


//$scfreeentryregistrants->SelectFreeEntryRegistrants();
$arrCnt = $scfreeentryregistrants->SelectFreeEntryRegistrants();
if (count($arrCnt) > 0)
{
    //Update free entry registrants table
    $scfreeentryregistrants->UpdateFreeEntryRegistrants();
    if ($scfreeentryregistrants->HasError) 
    {
        $errormsg = $scfreeentryregistrants->getError();
        $boolrollback = true;
    } 
    
    // Commit and Rollback Transactions
    if ($boolrollback)
    {
        $scfreeentryregistrants->RollBackTransaction();
    }
    else 
    {
        $scfreeentryregistrants->CommitTransaction();
    }
}
?>
