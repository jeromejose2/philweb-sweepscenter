<?php
include("init.inc.php");
$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCCV_VoucherInfo");
App::LoadModuleClass($modulename, "SCCV_AuditLog");

$svvoucherinfo = new SCCV_VoucherInfo();
$svauditlog = new SCCV_AuditLog();

$boolrollback = false;
$currdate = date('Y-m-d');
$date = strtotime ( '-2 day' , strtotime ( $currdate ) ) ;
$date = date ( 'Y-m-d' , $date );

//Update Voucher Info
$svvoucherinfo->StartTransaction();
$svvoucherinfo->UpdateExpiredRegistrationVouchers($date);
if ($svvoucherinfo->HasError) 
{
    $errormsg = $svvoucherinfo->getError();
    $boolrollback = true;
}  

// Log to Audit Trail
$svauditlog->StartTransaction();
$scauditlogparam["TransDetails"] = "Update expired registration vouchers";
$scauditlogparam["IPAddress"] = "";
$scauditlogparam["DateCreated"] = "now_usec()";
$scauditlogparam["CreatedBy"] = "cron";

$svauditlog->Insert($scauditlogparam);
if ($svauditlog->HasError) 
{
    $errormsg = $svauditlog->getError();
    $boolrollback = true;
}  

// Commit and Rollback Transactions
if ($boolrollback)
{
    $svvoucherinfo->RollBackTransaction();
    $svauditlog->RollBackTransaction();
} else {
    $svvoucherinfo->CommitTransaction();
    $svauditlog->CommitTransaction();
}

?>
