<?php
/*
 * Daily Gross Hold Report
 * Sweeps Center v.2
 * 
 * A report that will show daily gross hold and will run
 * every second using cron.
 * 
 * @author: Jerome F. Jose 
 * @date created: June 5, 2012
 * 
 * modified by: Noel Antonio
 * date modified: November 16, 2012
 * purpose: replace all stored procedures used by queries
 * 
 * last updated by: Noel Antonio
 * date updated: February 5, 2013
 * purpose: to include free entry transactions in the report.
 * 
 */

require_once ("include/core/init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_TerminalSessions");
App::LoadModuleClass($modulename, "SCC_TerminalSessionDetails");
App::LoadModuleClass($modulename,"SCC_Sites");
App::LoadCore("PHPMailer.class.php");
App::LoadLibrary("phpexcel/Classes/PHPExcel.php");

$terminalsessions = new SCC_TerminalSessions();
$terminalsessiondetails = new SCC_TerminalSessionDetails();
$csites = new SCC_Sites();

chmod(dirname(__FILE__) . '/dailygrossholdreport.php', 0777);

$currdate = "now";
$ds = new DateSelector($currdate);
$date = $ds->PreviousDate;
$date1 = $ds->CurrentDate;

$dateFrom = $date;
$dateFrom = date_create($dateFrom);
$dateFrom = date_format($dateFrom, "Y-m-d 10:00:00.00000");
$dateTo = $date1;
$dateTo = date_create($dateTo);
$dateTo = date_format($dateTo, "Y-m-d 09:59:59.99999");

$arrPOS = $csites->SelectPOSAcctActiveSites();
$list_POS = new ArrayList();
$list_POS->AddArray($arrPOS);

/* -------------------------------------------- EXCEL FILE ---------------------------------------- */

$getgrosshold = $terminalsessions->SelectGrossHold($dateFrom, $dateTo);
$arr_record = array();
if(count($getgrosshold) != 0)
{
        for($a = 0;$a < count($getgrosshold); $a++)
        {
                $arr_record[$getgrosshold[$a]["AccountID"]][$a]["DateStart"] = $getgrosshold[$a]["DateStart"];
                $arr_record[$getgrosshold[$a]["AccountID"]][$a]["POSName"] = $getgrosshold[$a]["PosName"];
                $tsid = $getgrosshold[$a]["ID"];
                
                $cash_total = 0;
		$noncash_total = 0;
		$non_cash = "";
                $grosshold = 0;
                
                $getterminal = $terminalsessiondetails->GetTransactiontype($tsid);
                if(count($getterminal) != 0)
                {
                        for($b = 0;$b < count($getterminal); $b++)
                        {
                                $deposit = $getterminal[$b]["D"];
                                $reload = $getterminal[$b]["R"];
                                $terminalname = $getterminal[$b]["Name"];
                                ($deposit == null) ? 0 : $deposit;
                                ($reload == null) ? 0 : $reload;
                                ($terminalname == null) ? 0 : $terminalname;
                                
                                $arr_record[$getgrosshold[$a]["AccountID"]][$a]["D"] = $deposit;
                                $arr_record[$getgrosshold[$a]["AccountID"]][$a]["R"] = $reload;
                                $arr_record[$getgrosshold[$a]["AccountID"]][$a]["TerminalName"] = $terminalname;
                        }
                }
                
                $getprizetypebyterminal = $terminalsessiondetails->GetPrizetypebyTerminalID($tsid);
                if(count($getprizetypebyterminal) != 0)
                {
                        for($c = 0;$c < count($getprizetypebyterminal); $c++)
                        {
                                $prizevalue = $getprizetypebyterminal[$c]["PrizeValue"];
                                $prizedesc = $getprizetypebyterminal[$c]["PrizeDescription"];
                                $i = 0;
                                if($prizedesc == "Cash")
                                {
                                        $cash_total += $prizevalue;
                                }
                                else
                                {
                                        $noncash_total += $prizevalue;
                                        if($c == count($getprizetypebyterminal))
                                        {
                                                $non_cash = $non_cash . "$" . $prizevalue . " " .$prizedesc;
                                        }
                                        else
                                        {
                                                foreach($getprizetypebyterminal as $arrays)
                                                {
                                                        if($arrays["PrizeDescription"] == $prizedesc && $arrays["PrizeValue"] == $prizevalue)
                                                        {
                                                                $i += 1;
                                                                $mytest = "(".$i.")"."$" .$prizevalue. " " . $prizedesc; 
                                                        }
                                                }             
                                                $pos = strpos($non_cash, $mytest);

                                                if ($pos === false) 
                                                {
                                                        if($non_cash != '') 
                                                        {
                                                                $non_cash = $non_cash .", (".$i.")". "$" .$prizevalue. " " . $prizedesc . " ";
                                                                $mytest = '';
                                                                $i = 0;
                                                        } 
                                                        else 
                                                        {
                                                                $non_cash = "(".$i.")"."$" .$prizevalue. " " . $prizedesc; 
                                                                $mytest = '';
                                                                $i = 0;
                                                        }
                                                }
                                        }      
                                }
                        }
                }
                
                $arr_record[$getgrosshold[$a]["AccountID"]][$a]["CashTotal"] = $cash_total;
                $arr_record[$getgrosshold[$a]["AccountID"]][$a]["NonCashTotal"] = $noncash_total;
                $arr_record[$getgrosshold[$a]["AccountID"]][$a]["NonCash"] = $non_cash;
                
                $grosshold = ($arr_record[$getgrosshold[$a]["AccountID"]][$a]["D"] + $arr_record[$getgrosshold[$a]["AccountID"]][$a]["R"]) - ($arr_record[$getgrosshold[$a]["AccountID"]][$a]["CashTotal"] + $arr_record[$getgrosshold[$a]["AccountID"]][$a]["NonCashTotal"]);
                $arr_record[$getgrosshold[$a]["AccountID"]][$a]["GrossHold"] = $grosshold;
                
                $i = 0;
        }
}

$deposit = array();
$reload = array();
$redcash = array();
$rednoncash = array();

$objPHPExcel = new PHPExcel();

for ($i = 0; $i < count($list_POS); $i++)
{
        // create worksheet
        $objWorkSheet = $objPHPExcel->createSheet($i);
        $objWorkSheet->setTitle(substr($list_POS[$i]["FullName"], 0, 30));

        $objWorkSheet->mergeCells('A1:G1');
        $objWorkSheet->mergeCells('A2:B2');
        $objWorkSheet->getStyle('A1:G'.$objWorkSheet->getHighestRow())->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                ->setWrapText(true);
        
        $objWorkSheet->setCellValue('A1', 'Sweeps Center Gross Hold Report: ' . $list_POS[$i]["FullName"]);
        
        $deposit_grandtotal = 0;
        $reload_grandtotal = 0;
        $cash_grandtotal = 0;
        $noncash_grandtotal = 0;
        $grosshold_grandtotal = 0;
        
        if (count($arr_record[$list_POS[$i]["AccountID"]]) > 0)
        {
                for ($h = 0; $h < count($arr_record[$list_POS[$i]["AccountID"]]); $h++)
                {
                        $rec = array_values($arr_record[$list_POS[$i]["AccountID"]]);
                        
                        // getting total
                        $deposit_grandtotal += $rec[$h]["D"];
                        $reload_grandtotal += $rec[$h]["R"];
                        $noncash_grandtotal += $rec[$h]["NonCashTotal"];
                        $cash_grandtotal += $rec[$h]["CashTotal"];
                        $grosshold_grandtotal += $rec[$h]["GrossHold"];
                        
                        $objWorkSheet->setCellValue('A'.($h + 4), $rec[$h]["DateStart"])
                            ->setCellValue('B'.($h + 4), $rec[$h]["TerminalName"])
                            ->setCellValue('C'.($h + 4), $rec[$h]["D"])
                            ->setCellValue('D'.($h + 4), $rec[$h]["R"])
                            ->setCellValue('E'.($h + 4), $rec[$h]["CashTotal"])
                            ->setCellValue('F'.($h + 4), $rec[$h]["NonCash"])
                            ->setCellValue('G'.($h + 4), $rec[$h]["GrossHold"])
                        ;
                }
        }
        
        $objWorkSheet->setCellValue('C2', 'Total Initial Deposit: ' . $deposit_grandtotal)
                    ->setCellValue('D2', 'Total Reloads: ' . $reload_grandtotal)
                    ->setCellValue('E2', 'Total Cash Redemption: ' . $cash_grandtotal)
                    ->setCellValue('F2', 'Total Value Non-Cash Redemption: ' . $noncash_grandtotal)
                    ->setCellValue('G2', 'Total Gross Hold: ' . $grosshold_grandtotal)
                    ->setCellValue('A3', 'Date')
                    ->setCellValue('B3', 'Terminal Name')
                    ->setCellValue('C3', 'Initial Deposit')
                    ->setCellValue('D3', 'Reloads')
                    ->setCellValue('E3', 'Cash Redemption')
                    ->setCellValue('F3', 'Non-Cash Redemption')
                    ->setCellValue('G3', 'Gross Hold')
        ;
        
        $deposit[$list_POS[$i]["AccountID"]] += $deposit_grandtotal;
        $reload[$list_POS[$i]["AccountID"]] += $reload_grandtotal;
        $redcash[$list_POS[$i]["AccountID"]] += $cash_grandtotal;
        $rednoncash[$list_POS[$i]["AccountID"]] += $noncash_grandtotal;
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save(dirname(__FILE__) . '/Daily_Gross_Holds.xls');

chmod(dirname(__FILE__) . "/Daily_Gross_Holds.xls", 0777);

/* -------------------------------------------- GROSS HOLD SUMMARY ---------------------------------------- */

$total_deposits = 0;
$total_redemptions = 0;
$total_gross_holds = 0;
$td = 0;
$tr = 0;
$tgh = 0;
$tc = 0;
$tnc = 0;

$message = '<table border="1" bordercolor="#000" style="width:100%; font-family: Arial; color:#000000; border: solid 1px #000; border-collapse: collapse; text-align: center;">';
$message .= '<tr><td colspan="6" style="background-color: #FF9C42; color:#000000; font-weight:bold; font-size:1.2em;">Sweeps Center Gross Hold Report</td></tr>';
$message .= '<tr>
		<th style="background-color: #FFBB7D;">POS Account Name</th>
		<th style="background-color: #FFBB7D;">Total Initial Deposit</th>
		<th style="background-color: #FFBB7D;">Total Reloads</th>
		<th style="background-color: #FFBB7D;">Total Cash Redemptions</th>
		<th style="background-color: #FFBB7D;">Total Value of Non-Cash Redemptions</th>
		<th style="background-color: #FFBB7D;">Total Gross Holds</th>
            </tr>';

for ($i = 0; $i < count($list_POS); $i++)
{ 
        $mod = ($i % 2);
        $message .= ($mod == 0) ? "<tr style='background-color:#FFF1E6; height:20px;'>" : "<tr style='height:20px;'>"; 
        
        $total_deposits = $deposit[$list_POS[$i]["AccountID"]] + $reload[$list_POS[$i]["AccountID"]];
        $total_redemptions = $redcash[$list_POS[$i]["AccountID"]] + $rednoncash[$list_POS[$i]["AccountID"]];
        $total_gross_holds = $total_deposits - $total_redemptions;
        
        $message .= '<td>' . $list_POS[$i]["FullName"] . '</td>
                     <td>' . number_format($deposit[$list_POS[$i]["AccountID"]], 2 , '.' , ',') . '</td>
                     <td>' . number_format($reload[$list_POS[$i]["AccountID"]], 2 , '.' , ',') . '</td>
                     <td>' . number_format($redcash[$list_POS[$i]["AccountID"]], 2 , '.' , ',') . '</td>
                     <td>' . number_format($rednoncash[$list_POS[$i]["AccountID"]], 2 , '.' , ',') . '</td>
                     <td>' . number_format($total_gross_holds, 2 , '.' , ',') . '</td></tr>';
        
        // getting the total figures
        $td += $deposit[$list_POS[$i]["AccountID"]];
        $tr += $reload[$list_POS[$i]["AccountID"]];
        $tc += $redcash[$list_POS[$i]["AccountID"]];
        $tnc += $rednoncash[$list_POS[$i]["AccountID"]];
        $tgh += $total_gross_holds;
}

$message .= "<tr style='font-weight: bold'><td>Total</td>
            <td>" . number_format($td, 2 , '.' , ',') . "</td>
            <td>" . number_format($tr, 2 , '.' , ',') . "</td>
            <td>" . number_format($tc, 2 , '.' , ',') . "</td>
            <td>" . number_format($tnc, 2 , '.' , ',') . "</td>
            <td>" . number_format($tgh, 2 , '.' , ',') . "</td>
            </tr></table>";

$pm = new PHPMailer();
//$pm->AddAddress("ndantonio@philweb.com.ph", "Noel Antonio");
$pm->AddBCC("mccalderon@philweb.com.ph", "Tere Calderon");
$pm->IsHTML(true);
$pm->AddAttachment(dirname(__FILE__) . '/Daily_Gross_Holds.xls');
$pm->Body.= $message; 
$pm->From = "no-reply@philwebasiapacific.com";
$pm->FromName = "no-reply";
$pm->Host = "localhost";
$dateTime = new DateTime($date);
$date = $dateTime->format("Y-M-d");
$pm->Subject = "[TEST]The Sweeps Center Gross Hold Report for  ".$date;
if(!$pm->Send())
        echo "Error sending: " . $pm->ErrorInfo;
else
        echo "Email sent";
?>