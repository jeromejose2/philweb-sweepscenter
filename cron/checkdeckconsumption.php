<?php

require_once ("include/core/init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCC_DeckInfo");
App::LoadCore("PHPMailer.class.php");

$deckinfo = new SCCC_DeckInfo();
$projectid = 2;

$arrDeckInfo = $deckinfo->SelectCurrentDeck($projectid);

if (isset($arrDeckInfo))
{
    $percentage = $arrDeckInfo[0]["Percentage"];
    if ($percentage > 80)
    {
        $subject = "E-Mail Notification for 80% Deck Consumption Level";
        $mailer = new PHPMailer();

        $mailer->AddAddress("mccalderon@philweb.com.ph", "Tere Calderon");
        $mailer->AddBCC("mccalderon@philweb.com.ph", "Tere Calderon");

        $mailer->From = 'no-reply@philwebasiapacific.com';
        $mailer->FromName = 'no-reply';
        $mailer->Host = 'localhost';
        $mailer->isHtml = true;
        $mailer->Subject = $subject;
        
        $message = $mailer->Body;
        $message = "<div>Good Day!</div>
                     <br/>
                     <div>This is to inform you  that the current live deck for the Sweepstakes Cafe's Electronic Scratch Card has now reached 80% consumption level. Please log on to your e-scratch card admin tool account to check availability of queued decks.</div>
                     <br/>
                     <div>To log on to your account, please click this url: <a href='http://www.thesweepscenter.com/ESC_Webtool/views/index.php'>http://www.thesweepscenter.com/ESC_Webtool</a>.</div>
                     <br/></br>
                     <div>Regards,</div>
                     <br/>
                     The Sweeps Center
                     <br/>
                     PhilWeb Guam Corporation
                     <br/><br/>

                     <div style='float: left; font-size: 10px; margin-top: 30px; font-gamily: Arial'>
                     <div>System generated email on " . date('D, j F Y H:i:s e') . "</div>
                     <br/>
                     <div>The information in this email, and any attachments, may contain confidential information and is intended solely for the attention and use of the named addressee(s). It must not be disclosed to any person(s) without authorization. If you are not the intended recipient, or a person responsible for delivering it to the intended recipient, you are not authorized to, and must not, disclose, copy, distribute, or retain this message or any part of it. If you have received this communication in error, please notify the sender immediately.</div>";
        $mailer->Body.=$message;
        $mailer->MsgHTML($message);
        if (!$mailer->Send())
            echo "Error sending: " . $mailer->ErrorInfo;
        else
            echo "Email sent";
    }
    else
    {
        
    }
}
?>
