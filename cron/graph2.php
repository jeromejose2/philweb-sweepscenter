<?php
/*
 * Created by JFJ 11-06-2012
 * Modified By: Noel Antonio 11-16-2012
 */
require_once("include/core/init.inc.php");
require_once("include/lib/jpgraph/jpgraph.php");
require_once("include/lib/jpgraph/jpgraph_line.php");
include ("include/lib/jpgraph/jpgraph_bar.php"); 

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename,"SCC_TerminalSessionDetails");
App::LoadModuleClass($modulename,"SCCC_Winners");
App::LoadModuleClass($modulename,"SCC_Sites");

$terminalsessiondetails = new SCC_TerminalSessionDetails();
$winners = new SCCC_Winners();
$csites = new SCC_Sites();

if (date('d') == '01')
{
	$todayDate = date("Y-m-d");
	$FromDate = strtotime(date("Y-m-d", strtotime($todayDate)) . " -1 month");
	$FromDate = date("Y-m-d", $FromDate);
	$dateFr = $FromDate. ' 09:59:59.99999';
	$dateTo = $todayDate . ' 10:00:00.00000';
}
else
{
	$dateFr = date('Y-m-d', mktime(0, 0, 0, date("m"), '01', date("Y"))) . ' 09:59:59.99999';
	$dateTo = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d"), date("Y"))) . ' 10:00:00.00000';
}

$rec_activepos = $csites->SelectPOSAcctActiveSites();
$list_POS = new ArrayList();
$list_POS->AddArray($rec_activepos);

$tmpsale = array();
$tmpred = array();
$prevdate = array();
$grosshold = array();
$tmpgh = array();

$getsales = $terminalsessiondetails->SelectSalesAmount($dateFr, $dateTo);
if (is_array($getsales) && count($getsales) >= 1)
{
    for ($i = 0; $i < count($getsales); $i++)
    {
        if (!isset($tmpsale[$getsales[$i]["AccountID"]][date('Y-m-d', strtotime($getsales[$i]["TransDate"]))]))
            $tmpsale[$getsales[$i]["AccountID"]][date('Y-m-d', strtotime($getsales[$i]["TransDate"]))] = $getsales[$i]["Sales"];
        else
            $tmpsale[$getsales[$i]["AccountID"]][date('Y-m-d', strtotime($getsales[$i]["TransDate"]))] += $getsales[$i]["Sales"];
    }
}

$getred = $winners->SelectSumValue($dateFr, $dateTo);
if (is_array($getred) && count($getred) >= 1)
{
    for ($i = 0; $i < count($getred); $i++)
    {
        if (!isset($tmpred[$getred[$i]["AccountID"]][date('Y-m-d', strtotime($getred[$i]["DateUsed"]))]))
            $tmpred[$getred[$i]["AccountID"]][date('Y-m-d', strtotime($getred[$i]["DateUsed"]))] = $getred[$i]["Redemption"];
        else
            $tmpred[$getred[$i]["AccountID"]][date('Y-m-d', strtotime($getred[$i]["DateUsed"]))] += $getred[$i]["Redemption"];
    }
}

$ds = new DateTime($dateTo);
$days_between = ceil(abs(strtotime($dateTo) - strtotime($dateFr)) / 86400);

for ($i = 0;$i < $days_between - 1;$i++)
{
        $date = $ds->format('Y-m-d');
        $ds->modify('-1 days');
        $prevdate[] = $ds->format('M d'); 
        
        for ($j = 0; $j <count($list_POS); $j++)
        {
            if (!isset($tmpsale[$list_POS[$j]["AccountID"]][$date]))
                $tmpsale[$list_POS[$j]["AccountID"]][$date] = 0;
            
            if (!isset($tmpred[$list_POS[$j]["AccountID"]][$date]))
                $tmpred[$list_POS[$j]["AccountID"]][$date] = 0;
            
            ksort($tmpsale[$list_POS[$j]["AccountID"]]);
            ksort($tmpred[$list_POS[$j]["AccountID"]]);
        }
}
ksort($tmpsale);
ksort($tmpred);
sort($prevdate);

$tmpsale = array_filter($tmpsale);
$tmpred = array_filter($tmpred);

$ds = new DateTime($dateTo);
for ($i = 0;$i < $days_between - 1;$i++)
{
        $date = $ds->format('Y-m-d');
        $prevdate[] = $ds->format('M d'); 
        $ds->modify('-1 days');
        
        for ($j = 0; $j < count($list_POS); $j++)
        {
            $tmpgh[$list_POS[$j]["AccountID"]][$date] = $tmpsale[$list_POS[$j]["AccountID"]][$date] - $tmpred[$list_POS[$j]["AccountID"]][$date];
            ksort($tmpgh[$list_POS[$j]["AccountID"]]);
        }
}
ksort($tmpgh);
$tmpgh = array_filter($tmpgh);
   

/*if ((count($prevdate) == 1))
{
    $prevdate[1] = $prevdate[0];
    $prevdate[0] = ' ';
    
    $grosshold[1] = $grosshold[0];
    $grosshold[0] = 0;
}*/


// Setup the graph
$width=700;
$height=500;

$graph = new Graph($width,$height);
$graph->SetScale("textlin");

$theme_class=new UniversalTheme;

$graph->SetTheme($theme_class);
$graph->img->SetAntiAliasing(false);
$graph->title->Set('Month to Date Gross Holds');
$graph->SetBox(false);

$graph->img->SetAntiAliasing();

$graph->yaxis->HideZeroLabel();
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

$graph->xaxis->SetTickLabels($prevdate);
$graph->xaxis->SetTickSide(SIDE_BOTTOM);
$graph->xaxis->SetLabelAngle(90);

for ($i = 0; $i < count($list_POS); $i++)
{
        $grosshold = array_values($tmpgh[$list_POS[$i]["AccountID"]]);

        // Create the first line
        $p1 = new LinePlot($grosshold);
        $graph->Add($p1);
        $p1->SetLegend($list_POS[$i]["FullName"]);
}

$graph->legend->SetFrameWeight(1);

$abspath = dirname(__FILE__) . '/';
//chmod($abspath . "graphimg/", 0777);

// Output line
$graph->Stroke($abspath . 'totalgh.jpg');
chmod($abspath . "totalgh.jpg", 0777);

/*$graph->Stroke($abspath . 'graphimg/totalgh.jpg');
chmod($abspath . "graphimg/totalgh.jpg", 0777);*/
?>