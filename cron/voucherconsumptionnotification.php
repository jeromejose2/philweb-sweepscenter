<?php
include("init.inc.php");
$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCCV_BatchInfo");
App::LoadModuleClass($modulename, "SCCV_VoucherInfo");
App::LoadModuleClass($modulename, "SCCV_AdminAccounts");
App::LoadModuleClass($modulename, "SCCV_AuditLog");
App::LoadCore("PHPMailer.class.php");

$svbatchinfo = new SCCV_BatchInfo();
$svvoucherinfo = new SCCV_VoucherInfo();
$svadminaccount = new SCCV_AdminAccounts();
$svauditlog = new SCCV_AuditLog();

$boolrollback = false;
$batch = '';
$percentage = 0;
$usedcnt = 0;
$email = '';

//Check Free Entry Voucher Consumption
//get currently active batch 
$arrBatch = $svvoucherinfo->GetActiveFreeEntryBatch();
if (count($arrBatch) == 1)
{
    $batchdtls = $arrBatch[0];
    $batch = $batchdtls["BatchID"];
    
    //check if there is a queued batch
    $arrQueued = $svvoucherinfo->GetQueuedFreeEntryBatch($batch);
    if (count($arrQueued) > 0)
    {
        
    }
    else
    {
        //get quantity of current batch
        $arrCnt = $svbatchinfo->GetActiveFreeEntryVoucherCount($batch);
        if (count($arrCnt) == 1)
        {
            $quantitydtls = $arrCnt[0];
            $quantity = $quantitydtls["Quantity"];
            $generatedby = $quantitydtls["GeneratedBy"];

            //get unused voucher count
            $arrCnt = $svvoucherinfo->GetUnusedActiveFreeEntryVoucherCount($batch);
            if (count($arrCnt) == 1)
            {
                $unuseddtls = $arrCnt[0];
                $unusedcnt = $unuseddtls["UnusedCnt"];   
                $usedcnt = $quantity - $unusedcnt;
                //get used percentage
                $percentage = ($usedcnt / $quantity) * 100;
                if ($percentage > 79)
                {
                    //get email of the user who generated the voucher
                    $arrAdminAccts = $svadminaccount->SelectGeneratedByEmail($generatedby);
                    if (count($arrAdminAccts) == 1)
                    {
                        $acctdtls = $arrAdminAccts[0];
                        $email = $acctdtls["Email"];
                        $name = $acctdtls["FirstName"] . ' ' . $acctdtls["MiddleName"] . ' ' . $acctdtls["Last Name"]; 
                    }

                    //email the administrator
                    $pm = new PHPMailer();
                    $pm->AddAddress($email, $name);
                    $pm->AddBCC("mccalderon@philweb.com.ph", "Tere Calderon");

                    $pm->IsHTML(true);

                    $pm->Body = "Dear $name,<br/><br/>
                                This is to inform you that the free entry voucher code batch for the Guam Sweeps Center with Batch ID " . $batch . 
                                " has now reached 80% consumption level. As such, please check the queued Voucher Code Batch to prepare 
                                the next list to be put into live usage.<br /><br />
                                To log on to your account, please click this url: http://www.thesweepscenter.com/VoucherWebtool/.<br /><br />
                                Regards, <br /><br />Customer Support<br />The Sweeps Center Team";

                    $pm->From = "operations@thesweepscenter.com";
                    $pm->FromName = "The Sweeps Center";  
                    $pm->Host = "localhost";
                    $pm->Subject = "NOTIFICATION FOR FREE ENTRY VOUCHER CODES CONSUMPTION LEVEL";
                    $email_sent = $pm->Send();
                    if($email_sent)
                    {
                        $errormsg = "Voucher Consumption Notification has been sent successfully.";
                        App::Pr($errormsg);
                    }
                    else
                    {
                        $emailmsg = "An error occurred while sending the email.";
                        App::Pr($errormsg);
                    }              
                }            
            }
            else 
            {
                 $errormsg = "Error: There is no active free entry voucher batch.";
            }
        }
        else
        {
            $errormsg = "Error: There is no active free entry voucher batch.";
        } 
    }
}
else
{
    $errormsg = "Error: There is no active free entry voucher batch.";
}

// Log to Audit Trail
$svauditlog->StartTransaction();
$scauditlogparam["TransDetails"] = "Sent Voucher Consumption Notification";
$scauditlogparam["IPAddress"] = "";
$scauditlogparam["DateCreated"] = "now_usec()";
$scauditlogparam["CreatedBy"] = "cron";

$svauditlog->Insert($scauditlogparam);
if ($svauditlog->HasError) 
{
    $errormsg = $svauditlog->getError();
    $boolrollback = true;
}  

// Commit and Rollback Transactions
if ($boolrollback)
{
    $svauditlog->RollBackTransaction();
} 
else 
{
    $svauditlog->CommitTransaction();
}

?>
