<?php
include 'class/class.phpmailer.php';

$currentdate = date("Y-m-d");
$currentdate2 = date("Y-M-d");

if (date('m', strtotime($currentdate)) == '01' && date('d', strtotime($currentdate)) == '01')
{
	$dateFr = date("Y-m-d", strtotime($currentdate . " -1 month")). ' 10:00:00.00000';
        $first_day_year = date("Y-m-d", strtotime($currentdate . " -12 months")) . ' 10:00:00.00000';
        $year = date('Y', strtotime("-1 year", strtotime($currentdate)));
}
else
{
        if (date('Y', strtotime($currentdate)) == date('Y'))
        {
            $dateFr = date('Y-m-d', mktime(0, 0, 0, date('m', strtotime($currentdate)), '01', date("Y"))) . ' 10:00:00.00000';
            $first_day_year = date('Y-m-d', mktime(0, 0, 0, '01', '01', date("Y"))) . ' 10:00:00.00000';
        }
        else
        {
            $dateFr = date('Y-m-d', mktime(0, 0, 0, date('m', strtotime($currentdate)), '01', date('Y', strtotime($currentdate)))) . ' 10:00:00.00000';
            $first_day_year = date('Y-m-d', mktime(0, 0, 0, '01', '01', date('Y', strtotime($currentdate)))) . ' 10:00:00.00000';
        }
        $year = date('Y', strtotime($currentdate));
}
$dateTo = $currentdate . ' 09:59:59.99999';


$mos_total_deposits = GetTotalDeposits($dateFr, $dateTo);
$mos_total_redemptions = GetTotalRedemptions($dateFr, $dateTo);
$mos_total_sweeps_input = GetTotalSweepsInput($dateFr, $dateTo);
$mos_gross_holds = $mos_total_deposits - $mos_total_redemptions;
$mos_total_mg_winnings = $mos_total_deposits - $mos_total_sweeps_input;
$mos_total_sweeps_revenue = $mos_gross_holds - $mos_total_mg_winnings;

$yr_total_deposits = GetTotalDeposits($first_day_year, $dateTo);
$yr_total_redemptions = GetTotalRedemptions($first_day_year, $dateTo);
//start add 07252012 mtcc
if ($year == 2012)
{
   $yr_total_redemptions = $yr_total_redemptions + GetPreviousYTDRedemption();
}
//end add 07252012 mtcc
$yr_total_sweeps_input = GetTotalSweepsInput($first_day_year, $dateTo);
$yr_gross_holds = $yr_total_deposits - $yr_total_redemptions;
$yr_total_mg_winnings = $yr_total_deposits - $yr_total_sweeps_input;
$yr_total_sweeps_revenue = $yr_gross_holds - $yr_total_mg_winnings;

/* -- Month to Date Summary -- */
$message = "<table style=\"padding:5px; font-weight:bold; font-family: Arial;\"><tr><td>";
$message .= "<table border=\"1\" bordercolor=\"#000\" style=\"font-weight:bold; font-family: Arial; color:#000000; border: solid 1px #000; border-collapse: collapse;\">";
$message .= "<tr align='center'><td width=\"400px\" colspan=\"2\" style=\"background-color: #FFBB7D;\">Month to Date Summary</td></tr>";
$message .= "<tr style='background-color:#FFF1E6; height:20px;'><td width=\"300px\">MTD Deposit</td><td width=\"100px\">".number_format($mos_total_deposits, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='height:20px;'><td>MTD Sweeps Cash & Non-Cash Redemptions</td><td>".number_format($mos_total_redemptions, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='background-color:#FFF1E6; height:20px;'><td>MTD Sweeps Center Gross Holds</td><td>".number_format($mos_gross_holds, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='height:20px;'><td>MTD Sweeps Center Input</td><td>".number_format($mos_total_sweeps_input, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='background-color:#FFF1E6; height:20px;'><td>MTD MicroGaming Winnings</td><td>".number_format($mos_total_mg_winnings, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='height:20px;'><td>MTD Sweeps Center Revenue</td><td>".number_format($mos_total_sweeps_revenue, 0 , '.' , ',')."</td></tr>";
$message .= "</table></td>";

/* -- Year to Date Summary -- */
$message .= "<td>";
$message .= "<table border=\"1\" bordercolor=\"#000\" style=\"font-weight:bold; font-family: Arial; color:#000000; border: solid 1px #000; border-collapse: collapse;\">";
$message .= "<tr align='center'><td width=\"400px\" colspan=\"2\" style=\"background-color: #FFBB7D;\">Year to Date Summary</td></tr>";
$message .= "<tr style='background-color:#FFF1E6; height:20px;'><td width=\"300px\">YTD Deposit</td><td width=\"100px\">".number_format($yr_total_deposits, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='height:20px;'><td>YTD Sweeps Cash & Non-Cash Redemptions</td><td>".number_format($yr_total_redemptions, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='background-color:#FFF1E6; height:20px;'><td>YTD Sweeps Center Gross Holds</td><td>".number_format($yr_gross_holds, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='height:20px;'><td>YTD Sweeps Center Input</td><td>".number_format($yr_total_sweeps_input, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='background-color:#FFF1E6; height:20px;'><td>YTD MicroGaming Winnings</td><td>".number_format($yr_total_mg_winnings, 0 , '.' , ',')."</td></tr>";
$message .= "<tr style='height:20px;'><td>YTD Sweeps Center Revenue</td><td>".number_format($yr_total_sweeps_revenue, 0 , '.' , ',')."</td></tr>";
$message .= "</table></td>";
$message .= "</tr></table><br/>";

/* -- Performace Report -- */
$message .= "<table border=\"1\" bordercolor=\"#000\" style=\"width:100%; font-family: Arial; color:#000000; border: solid 1px #000; border-collapse: collapse; text-align: center;\">";
$message .= "<tr><td colspan=\"7\" style=\"background-color: #FF9C42; color:#000000; font-weight:bold; font-size:1.2em; text-align: center;\">The Sweeps Center Performance Report</td></tr>";
$message .= "<tr style=\"font-weight: bold;\">
        <td style='background-color: #FFBB7D; width:12%;'>Date</td>
        <td style='background-color: #FFBB7D;'>Total Deposit</td>
        <td style='background-color: #FFBB7D;'>Total Sweeps Cash & Non-Cash Redemptions</td>
        <td style='background-color: #FFBB7D;'>Total Sweeps Center Gross Holds</td>
        <td style='background-color: #FFBB7D;'>Total Sweeps Inputs</td>
        <td style='background-color: #FFBB7D;'>Total Microgaming Winnings</td>
        <td style='background-color: #FFBB7D;'>Total Sweeps Revenue</td>
      </tr>";

        $ds = new DateTime($dateTo);
        $days_between = ceil(abs(strtotime($dateTo) - strtotime($dateFr)) / 86400);
        for ($i = 0;$i <= $days_between - 1;$i++)
        {
                $current = $ds->format('Y-m-d') . ' 10:00:00.00000';
                $ds->modify('-1 days');
                $previous = $ds->format('Y-m-d') . ' 09:59:59.99999';
                $previous2 = $ds->format('Y-m-d');

                $total_deposits = GetTotalDeposits($previous, $current);
                $total_redemptions = GetTotalRedemptions($previous, $current);
                $total_sweeps_input = GetTotalSweepsInput($previous, $current);
                $total_gross_holds = $total_deposits - $total_redemptions;
                $total_mg_winnings = $total_deposits - $total_sweeps_input;
                $total_sweeps_revenue = $total_gross_holds - $total_mg_winnings;

                $mod = ($i % 2);
                $message .= ($mod == 0) ? "<tr style='background-color:#FFF1E6; height:20px;'>" : "<tr style='height:20px;'>"; 
                $message .= "<td>".date('m-d-Y', strtotime($previous2))."</td>
                <td>".number_format($total_deposits,2)."</td>
                <td>".number_format($total_redemptions,2)."</td>
                <td>".number_format($total_gross_holds,2)."</td>
                <td>".number_format($total_sweeps_input,2)."</td>
                <td>".number_format($total_mg_winnings,2)."</td>
                <td>".number_format($total_sweeps_revenue,2)."</td>
                </tr>";

                $total_deposits = 0;
                $total_redemptions = 0;
                $total_gross_holds = 0;
                $total_mg_winnings = 0;
                $total_sweeps_revenue = 0;
        }
        
$message .= "</table><br/>";

echo $message;exit;


/* -- Send email -- */
$subject = "The Sweeps Center Performance Summary for ".$currentdate2;
$mailer = new PHPMailer();
$mailer->IsSMTP();

$mailer->AddAddress("ddpayumo@thesweepscenter.com", "Danny Payumo");
$mailer->AddAddress("mtgrandinetti@philweb.com.ph", "Michael Grandinetti");
$mailer->AddAddress("sasproule@philweb.com.ph", "Scott Sproule");
$mailer->AddAddress("akgarcia@philweb.com.ph", "Antonio Jose Garcia");
$mailer->AddAddress("zmprieto@philweb.com.ph", "Zaldy Prieto");
$mailer->AddAddress("business.solutions@philweb.com.ph", "Business Solutions");
$mailer->AddAddress("acmanabal@philweb.com.ph", "Alexander Manabal");
$mailer->AddAddress("gydichoso@philweb.com.ph", "Guia Paula Dichoso");
$mailer->AddAddress("jpc@seadevgroup.com", "JP Calvo");
$mailer->AddAddress("csvillanueva@philweb.com.ph", "Clarivic Villanueva");
$mailer->AddAddress("rmbuenaventura@philweb.com.ph", "Rocky Buenaventura");
$mailer->AddBCC("jbpormento@philweb.com.ph", "James Oliver Pormento");
$mailer->AddBCC("mccalderon@philweb.com.ph", "Tere Calderon");
$mailer->AddBCC("rpsanchez@philweb.com.ph", "Roger Sanchez");
//$mailer->AddBCC("masalavante@philweb.com.ph", "Marc Ian Salavante");
//$mailer->AddBCC("jdgavica@philweb.com.ph", "Joelle Lyn Gavica");

//$mailer->AddBCC("bdcarpio@philweb.com.ph", "Butch Carpio");

$mailer->From = 'no-reply@philwebasiapacific.com';
$mailer->FromName = 'no-reply';
$mailer->Host = 'localhost';
$mailer->isHtml = true;
$mailer->Subject = $subject;
$mailer->MsgHTML($message);
$mailer->AddEmbeddedImage('/var/sites/www.thesweepscenter.com/graphimg/salesandredemptions.jpg', 'pic1', 'salesandredemptions.jpg', 'base64','image/jpeg');
$mailer->AddEmbeddedImage('/var/sites/www.thesweepscenter.com/graphimg/totalgh.jpg', 'pic2', 'totalgh.jpg', 'base64','image/jpeg');

$mailer->Body = "<div style=\"font-size: 15px;\">&nbsp;</div>
                <div>".$message."</div>
                <br/><br/>
                <img src='http://www.thesweepscenter.com/graphimg/totalgh.jpg' />
                <img src='http://www.thesweepscenter.com/graphimg/salesandredemptions.jpg' />";

if(!$mailer->Send())
        echo "Error sending: " . $mailer->ErrorInfo;
else
        echo "Email sent";

function GetTotalDeposits($dateFr, $dateTo)
{
        include 'conn/connstr.php';
        
        $query = "SELECT ifnull(SUM(IF(A.transactiontype='D',A.amount,0)), 0) AS 'D', 
                    ifnull(SUM(IF(A.transactiontype='R',A.amount,0)), 0) AS 'R'
                    FROM tbl_terminalsessiondetails A 
                    INNER JOIN tbl_terminalsessions C ON A.terminalsessionid = C.id 
                    INNER JOIN tbl_terminals B ON C.terminalid = B.id 
                    WHERE B.isFreeEntry = '0' AND
                    C.datestart > '" . $dateFr . "' and C.datestart < '" . $dateTo . "';"; 
        $result = mysqli_query($dbConn, $query) or die ("Cannot perform database connection");
        if($result->num_rows > 0)
        {
                while($row = mysqli_fetch_row($result))
                {
                        $total_deposits = $row[0] + $row[1];
                }
        }
        mysqli_next_result($dbConn);
        mysqli_free_result($result);
        mysqli_close($dbConn);
        
        return $total_deposits;
}

function GetTotalRedemptions($dateFr, $dateTo)
{
        include 'conn/connstr.php';
        
        $query = "SELECT SUM(B.PrizeValue) AS Redeemed
                    FROM cafino.tbl_terminalsessiondetails A 
                    INNER JOIN chancecards.winners B ON A.ServiceTransactionID = B.TransactionSummaryID
                    INNER JOIN cafino.tbl_terminalsessions C ON A.TerminalSessionID = C.ID 
                    WHERE C.datestart > '" . $dateFr . "' and C.datestart < '" . $dateTo . "';";
        $result = mysqli_query($dbConn, $query) or die ("Cannot perform database connection");
        if($result->num_rows > 0)
        {
                while($row = mysqli_fetch_row($result))
                {
                        $total_redemptions = $row[0];
                }
        }
        mysqli_next_result($dbConn);
        mysqli_free_result($result);
        mysqli_close($dbConn);
        
        return $total_redemptions;
}

function GetTotalSweepsInput($dateFr, $dateTo)
{
        include 'conn/connstr.php';
        /*
        $query = "SELECT sum(Amount) AS dec_sweepsinput
                FROM cafino.tbl_transactionlogs
                WHERE TransactionType = 'W' AND Status = 1
                AND DateCreated > '" . $dateFr . "'
                AND DateCreated < '" . $dateTo . "';";*/
        $query = "SELECT SUM(ConvertedPoints)  AS dec_sweepsinput
                 FROM chancecards.pointsconversioninfo 
                 WHERE DateTimeTrans > '" . $dateFr . "' AND DateTimeTrans < '" . $dateTo . "';";
        $result = mysqli_query($dbConn, $query) or die ("Cannot perform database connection");
        if($result->num_rows > 0)
        {
                while($row = mysqli_fetch_row($result))
                {
                        $total_sweeps_input = $row[0];
                }
        }
        mysqli_next_result($dbConn);
        mysqli_free_result($result);
        mysqli_close($dbConn);
        
        return $total_sweeps_input;
}

function GetPreviousYTDRedemption()
{
    include 'conn/connstr.php';
    
    $query = "SELECT SUM(PrizeValue) FROM chancecards.winners WHERE DateGiven > '2012-01-01 09:59:59.99999' and DateGiven < '2012-06-30 10:00:00.00000';";
    //$query = "SELECT SUM(Redemptions) FROM cafino.tbl_dailyreports WHERE ReportDate >= '2012-01-01' AND ReportDate <= '2012-06-29';";
    $result = mysqli_query($dbConn, $query) or die ("Cannot perform database connection");
    if($result->num_rows > 0)
    {
        while($row = mysqli_fetch_row($result))
        {
            $total_prev_ytd_redemptions = $row[0];
        }
    }
    mysqli_next_result($dbConn);
    mysqli_free_result($result);
    mysqli_close($dbConn);

    return $total_prev_ytd_redemptions;
}


?>
