<?php
/**
 * Created by JFJ 06-05-2012 
 */
require_once ("include/core/init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_TerminalSessionDetails");

App::LoadCore("PHPMailer.class.php");

$terminalsessiondetails = new SCC_TerminalSessionDetails();


$currdate = "now";
$ds = new DateSelector($currdate);
$date = $ds->PreviousDate;
$date1 = $ds->CurrentDate;


    $siteid = '0';
    $dateFrom = $date;
    $dateFrom = date_create($dateFrom);
    $dateFrom = date_format($dateFrom, "Y-m-d 09:59:59.99999");
    $dateTo = $date1;
    // $dateinterval = new DateInterval('P1D');
    $dateTo = date_create($dateTo);
    //$dateTo = date_add($dateTo, $dateinterval);
    $dateTo = date_format($dateTo, "Y-m-d 10:00:00.00000");

    if ($siteid == 0)
    {
        $gettotalsales = $terminalsessiondetails->SelectTotalSales($dateFrom, $dateTo);
        $totalterminaldeposit = $gettotalsales[0]["Total"];
        ($totalredemption == null) ? 0 : $totalredemption;
        $getdailysales = $terminalsessiondetails->SelectDailySalesReport($dateFrom, $dateTo);



    }else
    {
        $gettotalsales = $terminalsessiondetails->SelectTotalSalesbySiteId($siteid,$dateFrom, $dateTo);
        $totalterminaldeposit = $gettotalsales[0]["Total"];
        ($totalredemption == null) ? 0 : $totalredemption;
        $getdailysales = $terminalsessiondetails->SelectDailySalesReportbySiteId($siteid,$dateFrom, $dateTo);
    }

        $pm = new PHPMailer();
        $pm->AddAddress("mccalderon@philweb.com.ph", "Tere Calderon");
        
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        $folder = $_SERVER["REQUEST_URI"];
        $folder = substr($folder,0,strrpos($folder,'/') + 1);
        if ($_SERVER["SERVER_PORT"] != "80") 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
        } 
        else 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].$folder;
        }
       
        $pm->IsHTML(true);
       $message =  $pm->Body;
        $message = "<table style='width:100%; font-family: Arial; color:#000000;'>
                        <tr>
                            <th colspan='3' style='height:30px; background-color: #FF9C42; color:#000000;'>SALES HISTORY</th>
                        </tr>";

        $message .= "<tr style='background-color:#FFF1E6; height:30px;'><td>&nbsp;<b>Total Terminal Deposits</b></td><td colspan='2'>&nbsp;<b>".number_format($totalterminaldeposit,2)."</b></td></tr>";

        $message .= "<tr>
                        <th style='width:30%; background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:215px; text-align:center;'>Date Processed</th>
                        <th style='width:30%; background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:215px; text-align:center;'>POS Account Name</th>
                <th style='width:40%; background-color: #FFBB7D; color:#000000; font-weight:bold; font-size:1em; width:215px; text-align:center;'>Deposit Details</th>
             </tr>";
if(count($getdailysales) != 0)
{
     $color = 1;
     for($i=0;$i<count($getdailysales);$i++)
    {
         $terminalname = $getdailysales[$i]["Name"];
         $transdate = $getdailysales[$i]["TransDate"];
         $dateTime = new DateTime($transdate);
         $transdate = $dateTime->format("Y-m-d h:i:s A");
         $points = $getdailysales[$i]["Amount"];
         $transNumber = $getdailysales[$i]["TransID"];
         $posname = "".$getdailysales[$i]["LastName"].",".$getdailysales[$i]['MiddleName'].",".$getdailysales[$i]['FirstName']."";   
    if($color == 1)
            {
                $message .= "<tr style='background-color:#FFF1E6; height:30px;'>
                                <td class='td'>".$transdate."</td>
                                <td class='td'>".$posname."</td>
                                <td class='td'>
                                    <table>
                                        <tr>
                                            <td><b>Transaction Number:</b></td>
                                            <td>".$transNumber."</td>
                                        </tr>
                                        <tr>
                                            <td><b>Terminal Name:</b></td>
                                            <td>".$terminalname."</td>
                                        </tr>
                                        <tr>
                                            <td><b>Points:</b></td>
                                            <td>".number_format($points,2)."</td>
                                        </tr>
                                     </table>
                                 </td>
                             </tr>";
                
                $color = 2;
            }
            else
            {
                $message .= "<tr style='height:30px;'>
                                <td class='td'>".$transdate."</td>
                                <td class='td'>".$posname."</td>
                                <td class='td'>
                                    <table>
                                        <tr>
                                            <td><b>Transaction Number:</b></td>
                                            <td>".$transNumber."</td>
                                        </tr>
                                        <tr>
                                            <td><b>Terminal Name:</b></td>
                                            <td>".$terminalname."</td>
                                        </tr>
                                        <tr>
                                            <td><b>Points:</b></td>
                                            <td>".number_format($points,2)."</td>
                                        </tr>
                                     </table>
                                 </td>
                             </tr>";

                $color = 1;
            } 
            }

    } else
        {
            $message .= "<tr><td colspan='3' align='center'><b>No Records Found.</b></td></tr>";
        }

       $message .= "</table>";
       $pm->Body.=$message; 
       $pm->From = "no-reply@philwebasiapacific.com";
       $pm->FromName = "no-reply";
       $pm->Host = "localhost";
       $dateTime = new DateTime($date);
        $date = $dateTime->format("Y-m-d");
        $pm->Subject = "The Sweeps Center Daily Sales Summary for ".$date;
        $email_sent = $pm->Send();


?>
