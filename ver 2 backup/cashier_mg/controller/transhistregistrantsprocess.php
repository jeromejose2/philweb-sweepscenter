<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 04, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_FreeEntry");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("PagingControl2");

$cfreeentry = new SCC_FreeEntry();
$stylesheets[] = "css/datepicker.css";
$javascripts[] = "jscripts/datepicker.js";
$javascripts[] = "jscripts/checking.js";
$pagename = "transhistregistrants";

$transhistregistrants_form = new FormsProcessor();

$txtDateFr = new TextBox("txtDateFr","txtDateFr","Date From");
$txtDateFr->Length = 10;
$txtDateFr->Args = 'onkeypress="javascript: return isNumberKey(event);" style="text-align:center;"';
$txtDateFr->Text = date("m/d/Y");
$txtDateFr->ReadOnly = true;

$txtDateTo = new TextBox("txtDateTo","txtDateTo","Date To");
$txtDateTo->Length = 10;
$txtDateTo->Args = 'onkeypress="javascript: return isNumberKey(event);" style="text-align:center;"';
$txtDateTo->Text = date("m/d/Y");
$txtDateTo->ReadOnly = true;

$btnSearch = new Button("btnSearch","btnSearch","Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = 'onclick="javascript: return checkinput();"';

$transhistregistrants_form->AddControl($txtDateFr);
$transhistregistrants_form->AddControl($txtDateTo);
$transhistregistrants_form->AddControl($btnSearch);

$transhistregistrants_form->ProcessForms();
/*PAGING*/
$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
/*PAGING*/
if($transhistregistrants_form->IsPostBack)
{
    if($btnSearch->SubmittedValue == "Search")
    {
        $pgcon->SelectedPage = 1;
    }
    $siteid = $_SESSION['siteid'];
    $dateFrom = $txtDateFr->SubmittedValue;
    $dateFrom = date_create($dateFrom);
    $dateFrom = date_format($dateFrom, "Y-m-d 09:59:59.99999");
    $dateTo = $txtDateTo->SubmittedValue;
    /*$dateinterval = new DateInterval('P1D');
    $dateTo = date_create($dateTo);
    $dateTo = date_add($dateTo, $dateinterval);
    $dateTo = date_format($dateTo, "Y-m-d 10:00:00.00000");*/
	$dateTo = strtotime ( '+1 day' , strtotime ( $dateTo ) ) ;
	$dateTo = date ( 'Y-m-d 10:00:00.00000' , $dateTo );
  
    $displayrecords = "ok";
    $transhist = $cfreeentry->SelectRegistrants($siteid,$dateFrom,$dateTo);
    $transhistcount = count($transhist);
    $pgcon->Initialize($itemsperpage, $transhistcount);
    $pgTransactionHistory = $pgcon->PreRender();
    $start = ($transhistcount > 0) ? $pgcon->SelectedItemFrom : 0;
    $end = (($start - 1) + $itemsperpage);
    $selectedpage = $pgcon->SelectedPage;
    $totalpages = ceil(($transhistcount / $itemsperpage));
    $transhistregistrantsdtls = $cfreeentry->SelectRegistrantsWithLimit($siteid,$dateFrom,$dateTo,($pgcon->SelectedItemFrom - 1),$itemsperpage);
}
?>
