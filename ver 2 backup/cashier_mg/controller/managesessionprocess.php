<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 21, 2012
 */
require_once("../init.inc.php");

$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$javascripts[] = "jscripts/checking.js";

App::LoadModuleClass("SweepsCenter", "SCC_AccountSessions");
App::LoadModuleClass("SweepsCenter", "SCC_Accounts");

$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$project_title = "Guam Sweepstakes Caf&#233; Cashier";

$caccounts = new SCC_Accounts();
$cacctsessions = new SCC_AccountSessions();

$date_time = $cacctsessions->selectNow();
$serverdatetime = $date_time[0][0];
$bcfbalance = $caccounts->SelectBCFBalance();
if(count($bcfbalance) > 0)
{
    $_SESSION["Bal"] = number_format($bcfbalance[0]["Balance"],2);
}
?>