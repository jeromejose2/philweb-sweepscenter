<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 31, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessionDetails");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");

$cterminals = new SCC_Terminals();
$cterminalsessiondtls = new SCC_TerminalSessionDetails();
$stylesheets[] = "css/datepicker.css";
$javascripts[] = "jscripts/datepicker.js";
$javascripts[] = "jscripts/checking.js";
$pagename = "transhistdeposit";

$transhistdeposit_form = new FormsProcessor();

$txtDateFr = new TextBox("txtDateFr","txtDateFr","Date From");
$txtDateFr->Length = 10;
$txtDateFr->Args = 'onkeypress="javascript: return isNumberKey(event);" style="text-align:center;"';
$txtDateFr->Text = date("m/d/Y");
$txtDateFr->ReadOnly = true;

$txtDateTo = new TextBox("txtDateTo","txtDateTo","Date To");
$txtDateTo->Length = 10;
$txtDateTo->Args = 'onkeypress="javascript: return isNumberKey(event);" style="text-align:center;"';
$txtDateTo->Text = date("m/d/Y");
$txtDateTo->ReadOnly = true;

$terminals = $cterminals->SelectTerminalsForTransHistory($_SESSION['siteid']);
$terminal_list = new ArrayList();
$terminal_list->AddArray($terminals);
$ddlTerminal = new ComboBox("ddlTerminal","ddlTerminal","Terminal");
$options = null;
$options[] = new ListItem("-----","0",true);
$ddlTerminal->Items = $options;
$ddlTerminal->DataSource = $terminal_list;
$ddlTerminal->DataSourceText = "Name";
$ddlTerminal->DataSourceValue = "ID";
$ddlTerminal->DataBind();

$btnSearch = new Button("btnSearch","btnSearch","Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = 'onclick="javascript: return checkinput();"';

$transhistdeposit_form->AddControl($txtDateFr);
$transhistdeposit_form->AddControl($txtDateTo);
$transhistdeposit_form->AddControl($ddlTerminal);
$transhistdeposit_form->AddControl($btnSearch);

$transhistdeposit_form->ProcessForms();
/*PAGING*/
$itemsperpage = 50;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
/*PAGING*/
if($transhistdeposit_form->IsPostBack)
{
    if($btnSearch->SubmittedValue == "Search")
    {
        $pgcon->SelectedPage = 1;
    }

    $siteid = $_SESSION['siteid'];
    $dateFrom = $txtDateFr->SubmittedValue;
    $dateFrom = date_create($dateFrom);
    $dateFrom = date_format($dateFrom, "Y-m-d 09:59:59.99999");
    $dateTo = $txtDateTo->SubmittedValue;
    /*$dateinterval = new DateInterval('P1D');*/
    //$dateTo = date_create($dateTo);
    //$dateTo = date_add($dateTo, $dateinterval);
    //$dateTo = date_format($dateTo, "Y-m-d 10:00:00.00000");
	$dateTo = strtotime ( '+1 day' , strtotime ( $dateTo ) ) ;
	$dateTo = date ( 'Y-m-d 10:00:00.00000' , $dateTo );
    $terminalid = $ddlTerminal->SubmittedValue;
	
    $displayrecords = "ok";
    $totaldtls = $cterminalsessiondtls->SelectTotalRedemption($terminalid,$dateFrom, $dateTo, $siteid);
    $transhist = $cterminalsessiondtls->SelectTransactionHistory($terminalid, $dateFrom, $dateTo, $siteid);
    $transhistcount = count($transhist);
    $pgcon->Initialize($itemsperpage, $transhistcount);
    $pgTransactionHistory = $pgcon->PreRender();
    $start = ($transhistcount > 0) ? $pgcon->SelectedItemFrom : 0;
    $end = (($start - 1) + $itemsperpage);
    $selectedpage = $pgcon->SelectedPage;
    $totalpages = ceil(($transhistcount / $itemsperpage));
    $transhistdtls = $cterminalsessiondtls->SelectTransactionHistoryWithLimit($terminalid, $dateFrom, $dateTo, $siteid,($pgcon->SelectedItemFrom - 1),$itemsperpage);
}
?>
