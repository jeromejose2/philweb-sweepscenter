<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 29, 2012
 */
error_reporting(E_ALL);
include('../init.inc.php');
include("../config.php");

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
App::LoadModuleClass("SweepsCenter", "SCC_TransactionLogs");
App::LoadModuleClass("SweepsCenter", "SCC_AccountSessions");
App::LoadModuleClass("SweepsCenter", "SCC_Sites");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessionDetails");
App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
App::LoadLibrary("MicrogamingAPI.class.php");
App::LoadLibrary("microseconds.php");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$cterminals = new SCC_Terminals();
$cterminalsessions = new SCC_TerminalSessions();
$ctranslog = new SCC_TransactionLogs();
$cacctsessions = new SCC_AccountSessions();
$csites = new SCC_Sites();
$cterminalsessiondtls = new SCC_TerminalSessionDetails();
$caudittrail = new SCC_AuditTrail();
$csmagentsessions = new SCCSM_AgentSessions();
$pagename = "reloadterminal";

$reloadsession_form = new FormsProcessor();

$terminals = $cterminals->SelectTerminalsForReloadSession($_SESSION['siteid']);
$terminal_list = new ArrayList();
$terminal_list->AddArray($terminals);
$ddlTerminal = new ComboBox("ddlTerminal","ddlTerminal","Terminal");
$options = null;
$options[] = new ListItem("----","0",true);
$ddlTerminal->Items = $options;
$ddlTerminal->DataSource = $terminal_list;
$ddlTerminal->DataSourceText = "Name";
$ddlTerminal->DataSourceValue = "ID";
$ddlTerminal->DataBind();
$ddlTerminal->CssClass = "options";

$ddlAmount = new ComboBox("ddlAmount","ddlAmount","Points:");
$options = null;
$options[] = new ListItem("----","0",true);
$options[] = new ListItem("1","1");
$options[] = new ListItem("5","5");
$options[] = new ListItem("10","10");
$options[] = new ListItem("20","20");
$options[] = new ListItem("50","50");
$options[] = new ListItem("100","100");
$ddlAmount->Items = $options;
$ddlAmount->CssClass = "options";
$ddlAmount->Args = 'onchange="javascript: return onchange_amount();"';

$txtAmount = new TextBox("txtAmount","txtAmount","Amount");
$txtAmount->Length = 10;
$txtAmount->Args = 'style="width:100px;" onkeypress="javascript: return isNumberKey(event);"';

$btnReloadSession = new Button("btnReloadSession","btnReloadSession","RELOAD SESSION");
$btnReloadSession->Args = 'onclick="javascript: return onSubmit();"';
$btnReloadSession->CssClass = "labelbutton2";

$btnOkay = new Button("btnOkay","btnOkay"," ");
$btnOkay->IsSubmit = true;
$btnOkay->CssClass = "inputBoxEffectPopup";

$reloadsession_form->AddControl($ddlTerminal);
$reloadsession_form->AddControl($ddlAmount);
$reloadsession_form->AddControl($txtAmount);
$reloadsession_form->AddControl($btnReloadSession);
$reloadsession_form->AddControl($btnOkay);

$reloadsession_form->ProcessForms();
if ($reloadsession_form->IsPostBack)
{
    if($btnOkay->SubmittedValue == " ")
    {
        $terminalid = $ddlTerminal->SubmittedValue;
        if($ddlAmount->SubmittedValue != 0)
        {
            $amount = $ddlAmount->SubmittedValue;
        }
        else
        {
            $amount = $txtAmount->SubmittedValue;
        }
        $arrterminalname = $cterminals->SelectTerminalName($terminalid);
        $terminalname = $arrterminalname[0]["Name"];
        $terminalsessionid = udate('YmdHisu');
        $sessiondtls = $cterminalsessions->SelectTerminalSessionDetails($terminalid);
        if(count($sessiondtls) > 0)
        {
            $terminalsessid = $sessiondtls[0]['ID'];
            $sessionguid = $csmagentsessions->SelectSessionGUID();
            $sessionGUID_MG = $sessionguid[0]["SessionGUID"];
			$ip_add_MG = '172.20.60.15';

            //insert to transaction logs
            $ctranslog->StartTransaction();
            $arrTransactionLog["TerminalSessionID"] = $terminalsessionid;
            $arrTransactionLog["TerminalID"] = $terminalid;
            $arrTransactionLog["ServiceID"] = 4;
            $arrTransactionLog["DateCreated"] = 'now_usec()';
            $arrTransactionLog["DateUpdated"] = 'now_usec()';
            $arrTransactionLog["TransactionType"] = 'D';
            $arrTransactionLog["Amount"] = $amount;
            $arrTransactionLog["LaunchPad"] = 0;
            $ctranslog->Insert($arrTransactionLog);
            if($ctranslog->HasError)
            {
                $ctranslog->RollBackTransaction();
                $error_title = "ERROR";
                $error_msg = "INSERT Failed - Failed to insert tbl_transactionlogs";
            }
            else
            {
                $ctranslog->CommitTransaction();
                //insert to logs
                $filename = "../../DBLogs/logs.txt";
                $fp = fopen($filename , "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: RELOAD || ".$terminalname." || INSERT TO tbl_transactionlogs \r\n");
                fclose($fp);

                $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
                "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
                "<IPAddress>".$ip_add_MG."</IPAddress>".
                "<ErrorCode>0</ErrorCode>".
                "<IsLengthenSession>true</IsLengthenSession>".
                "</AgentSession>";

                //added 03/21/2012 mtcc
                $client = new nusoap_client($mg_url, 'wsdl');
                $client->setHeaders($headers);
                $param = array('delimitedAccountNumbers' => $terminalname);
                $filename = "../../APILogs/logs.txt";
                $fp = fopen($filename , "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GetAccountBalance) || ".$terminalname." || ".serialize($param)."\r\n");
                fclose($fp);
                $a = 1;
                while($a < 4)
                {
                    $result = $client->call('GetAccountBalance', $param);
                    if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
                    {
                        $a++;
                    }
                    else
                    {
                        break;
                    }
                }


                $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];

                $string = serialize($result);
                $filename = "../../APILogs/logs.txt";
                $fp = fopen($filename , "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GET TERMINAL BALANCE(RELOAD) || ".$terminalname." || ".$string."\r\n");
                fclose($fp);

                $client = new nusoap_client($mg_url, 'wsdl');
                $client->setHeaders($headers);
                $param = array('accountNumber' => $terminalname, 'amount' => $amount, 'currency' => 1);
                $filename = "../../APILogs/logs.txt";
                $fp = fopen($filename , "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: Deposit) || ".$terminalname." || ".serialize($param)."\r\n");
                fclose($fp);
                $result = $client->call('Deposit', $param);

                $mgtransid = $result['DepositResult']['TransactionId'];
                $mgtransstatus = $result['DepositResult']['IsSucceed'];

                if ($mgtransstatus == 'true')
                {
                    $mgtransstat = '1';
                    //update transactionlog
                    $ctranslog->StartTransaction();
                    $ctranslog->UpdateTransactionLog($mgtransid, $mgtransstatus, $mgtransstat, $terminalsessionid);
                    if($ctranslog->HasError)
                    {
                        $ctranslog->RollBackTransaction();
                        $error_title = "ERROR";
                        $error_msg = "Update Failed - Failed to update tbl_transactionlogs";
                    }
                    else
                    {
                        $ctranslog->CommitTransaction();
                        //insert to logs
                        $filename = "../../DBLogs/logs.txt";
                        $fp = fopen($filename , "a");
                        fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: RELOAD || ".$terminalname." || UPDATE tbl_transactionlogs \r\n");
                        fclose($fp);

                        //start session
                        $sessionsdtls = $cacctsessions->SelectSessionDetails($_SESSION['sid']);
                        if(count($sessionsdtls) > 0)
                        {
                            $acctid = $sessionsdtls[0]['ID'];
                            $siteid = $sessionsdtls[0]['SiteID'];
                            
                            $sitedtls = $csites->SelectSiteDetails($siteid);
                            $balance = $sitedtls[0]['Balance'];
                            if($balance < $amount)
                            {
                                $error_title = "ERROR";
                                $error_msg = "Account balance is less than the deposit amount.";
                            }
                            else
                            {
                                $newbalance = ($balance - $amount);
                                $csites->StartTransaction();
                                $csites->UpdateSiteBalance($newbalance, $siteid);
                                if($csites->HasError)
                                {
                                    $csites->RollBackTransaction();
                                    $error_title = "ERROR";
                                    $error_msg = "Error updating site balance.";
                                }
                                else
                                {
                                    $csites->CommitTransaction();
                                    //insert to DB logs
                                    $filename = "../../DBLogs/logs.txt";
                                    $fp = fopen($filename , "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || UPDATE tbl_sites \r\n");
                                    fclose($fp);

                                    $cterminals->StartTransaction();
                                    $cterminals->UpdateTerminalBalance($newbalance, $terminalid);
                                    if($cterminals->HasError)
                                    {
                                        $cterminals->RollBackTransaction();
                                        $error_title = "ERROR";
                                        $error_msg = "Error updating terminal balance.";
                                    }
                                    else
                                    {
                                        $cterminals->CommitTransaction();

                                        $terminalsessiondetails = $cterminalsessiondtls->SelectMaxId();
                                        $lastTransId = ($terminalsessiondetails[0]["max"] != null) ? $terminalsessiondetails[0]["max"] : 0;
                                        $lastTransId = ($lastTransId + 1);

                                        $transRefId = 'R' . str_pad($terminalsessid,6,0,STR_PAD_LEFT) . str_pad($lastTransId,10,0,STR_PAD_LEFT);

                                        $cterminalsessiondtls->StartTransaction();
                                        $arrTerminalSessionDetails["TerminalSessionID"] = $terminalsessid;
                                        $arrTerminalSessionDetails["TransactionType"] = 'R';
                                        $arrTerminalSessionDetails["Amount"] = $amount;
                                        $arrTerminalSessionDetails["ServiceID"] = '1';
                                        $arrTerminalSessionDetails["ServiceTransactionID"] = $mgtransid;
                                        $arrTerminalSessionDetails["TransID"] = $transRefId;
                                        $arrTerminalSessionDetails["AcctID"] = $acctid;
                                        $arrTerminalSessionDetails["TransDate"] = 'now_usec()';
                                        $arrTerminalSessionDetails["DateCreated"] = 'now_usec()';
                                        //App::Pr($arrTerminalSessionDetails);exit;
                                        $cterminalsessiondtls->Insert($arrTerminalSessionDetails);
                                        //App::Pr($cterminalsessiondtls->LastInsertID);exit;
                                        if($cterminalsessiondtls->HasError)
                                        {
                                            $cterminalsessiondtls->RollBackTransaction();
                                            $error_title = "ERROR";
                                            $error_msg = "Error inserting new terminal session details.";
                                        }
                                        else
                                        {
                                            $cterminalsessiondtls->CommitTransaction();
                                            //insert to DB logs
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp = fopen($filename , "a");
                                            fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || INSERT TO tbl_terminalsessiondetails \r\n");
                                            fclose($fp);

                                            $caudittrail->StartTransaction();
                                            $arrAuditTrail["SessionID"] = $_SESSION['sid'];
                                            $arrAuditTrail["AccountID"] = $acctid;
                                            $arrAuditTrail["TransDetails"] = 'Reload for terminalid: ' . $terminalid;
                                            $arrAuditTrail["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                                            $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                            $caudittrail->Insert($arrAuditTrail);
                                            if($caudittrail->HasError)
                                            {
                                                $caudittrail->RollBackTransaction();
                                                $error_title = "ERROR";
                                                $error_msg = "Error inserting in audit trail.";
                                            }
                                            else
                                            {
                                                $caudittrail->CommitTransaction();
                                                //insert to DB logs
                                                $filename = "../../DBLogs/logs.txt";
                                                $fp = fopen($filename , "a");
                                                fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || INSERT TO tbl_audittrail \r\n");
                                                fclose($fp);

                                                //$error_title = "SUCCESS";
                                                //$error_msg = "Terminal successfully reloaded.";

                                                $transdate = date("Y-m-d h:i:s A");
                                                $url = "\"print.php?points='".$amount."'&terminal='".$terminalname."'&refnum='".$transRefId."'&transdate='".$transdate . "'\"";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            $error_title = "ERROR";
                            $error_msg = "Account session does not exist.";
                        }
                    }
                }
                else
                {
                    $mgtransstat = '2';
					$ctranslog->StartTransaction();
                    $ctranslog->UpdateTransactionLog($mgtransid, $mgtransstatus, $mgtransstat, $terminalsessionid);
                    if($ctranslog->HasError)
                    {
                        $ctranslog->RollBackTransaction();
                        $error_title = "ERROR";
                        $error_msg = "Update Failed - Failed to update tbl_transactionlogs";
                    }
                    else
                    {
                        $ctranslog->CommitTransaction();
                        //insert to logs
                        $filename = "../../DBLogs/logs.txt";
                        $fp = fopen($filename , "a");
                        fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: RELOAD || ".$terminalname." || UPDATE tbl_transactionlogs  \r\n");
                        fclose($fp);
					}
                }
            }
        }
        else
        {
            $error_title = "INVALID RELOAD";
            $error_msg = "Terminal does not have an active session.";
        }
    }
}
?>
