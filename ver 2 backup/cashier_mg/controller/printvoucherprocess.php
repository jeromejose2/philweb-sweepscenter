<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 21, 2012
 */
include('../init.inc.php');
include("../config.php");

App::LoadModuleClass("SweepsCenter", "SCCV_Denominationref");
App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
App::LoadModuleClass("SweepsCenter", "SCC_TransactionLogs");
App::LoadModuleClass("SweepsCenter", "SCC_AccountSessions");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
App::LoadModuleClass("SweepsCenter", "SCC_Sites");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessionDetails");
App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
App::LoadLibrary("MicrogamingAPI.class.php");
App::LoadLibrary("microseconds.php");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$cvdenominationref = new SCCV_DenominationRef();
$cterminals = new SCC_Terminals();
$csmagentsessions = new SCCSM_AgentSessions();
$ctranslog = new SCC_TransactionLogs();
$cacctsessions = new SCC_AccountSessions();
$cterminalsessions = new SCC_TerminalSessions();
$csites = new SCC_Sites();
$cterminalsessiondtls = new SCC_TerminalSessionDetails();
$caudittrail = new SCC_AuditTrail();
$pagename = "printvoucher";

$startsession_form = new FormsProcessor();

$terminals = $cterminals->SelectTerminalsForStartSession($_SESSION['siteid']);
$terminal_list = new ArrayList();
$terminal_list->AddArray($terminals);
$ddlTerminal = new ComboBox("ddlTerminal","ddlTerminal","Gaming Terminal: ");
$options = null;
$options[] = new ListItem("----","0",true);
$ddlTerminal->Items = $options;
$ddlTerminal->DataSource = $terminal_list;
$ddlTerminal->DataSourceText = "Name";
$ddlTerminal->DataSourceValue = "ID";
$ddlTerminal->DataBind();

$ddlAmount = new ComboBox("ddlAmount","ddlAmount","Points:");
$options = null;
$options[] = new ListItem("----","0",true);
$options[] = new ListItem("1","1");
$options[] = new ListItem("5","5");
$options[] = new ListItem("10","10");
$options[] = new ListItem("20","20");
$options[] = new ListItem("50","50");
$options[] = new ListItem("100","100");
$ddlAmount->Items = $options;
$ddlAmount->Args = "onchange='javascript: return disableText();'";

$txtAmount = new TextBox("txtAmount","txtAmount","Amount");
$txtAmount->Length = 10;
$txtAmount->Args = "maxlength=\"10\"  style=\"width:100px;\" onkeypress='javascript: return isNumberKey(event);'";
//onblur='javascript: return onchange_amounttxtbox();'

$btnStartSession = new Button("btnStartSession","btnStartSession","START SESSION");
$btnStartSession->CssClass = "labelbutton2";
$btnStartSession->Args = 'onclick="javascript: return checkinput();"';

$btnOkay = new Button("btnOkay","btnOkay"," ");
$btnOkay->IsSubmit = true;
$btnOkay->CssClass = "inputBoxEffectPopup";

$startsession_form->AddControl($ddlTerminal);
$startsession_form->AddControl($ddlAmount);
$startsession_form->AddControl($txtAmount);
$startsession_form->AddControl($btnStartSession);
$startsession_form->AddControl($btnOkay);

$startsession_form->ProcessForms();

if ($startsession_form->IsPostBack)
{
    if($btnOkay->SubmittedValue == " ")
    {
        $terminalsessionid = udate('YmdHisu');
        $terminal = $ddlTerminal->SubmittedValue;
        if($ddlAmount->SubmittedValue != 0)
        {
            $amount = $ddlAmount->SubmittedValue;
        }
        else
        {
            $amount = $txtAmount->SubmittedValue;
        }
        $arrterminalname = $cterminals->SelectTerminalName($terminal);
        $terminalname = $arrterminalname[0]["Name"];

        $sessionguid = $csmagentsessions->SelectSessionGUID();
        $sessionGUID_MG = $sessionguid[0]["SessionGUID"];
        $ip_add_MG = '172.20.60.15';
        $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
               "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
               "<IPAddress>".$ip_add_MG."</IPAddress>".
               "<ErrorCode>0</ErrorCode>".
               "<IsLengthenSession>true</IsLengthenSession>".
               "</AgentSession>";
        $client = new nusoap_client($mg_url, 'wsdl');
        $client->setHeaders($headers);
        $param = array('delimitedAccountNumbers' => $terminalname);

        //insert to API logs
        $filename = "../../APILogs/logs.txt";
        $fp = fopen($filename , "a");
        fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GetAccountBalance || ".$terminalname." || ".serialize($param)."\r\n");
        fclose($fp);

        $a = 1;
        while($a < 4)
        {
            $result = $client->call('GetAccountBalance', $param);
            if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
            {
                $a++;
            }
            else
            {
                break;
            }
        }
        $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];

        //insert to API logs
        $string = serialize($result);
        $filename = "../../APILogs/logs.txt";
        $fp = fopen($filename , "a");
        fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GET TERMINAL BALANCE(DEPOSIT) || ".$terminalname." || ".$string."\r\n");
        fclose($fp);

	 ///App::Pr($balance);exit;
        if ($balance > 0)
        {
            $error_title = "INVALID DEPOSIT";
            $error_msg = "Balance must be zero.";
        }
        else
        {
			$param = array('accountNumber' => $terminalname, 'amount' => $amount, 'currency' => 1);
            //insert to API logs
            $filename = "../../APILogs/logs.txt";
            $fp = fopen($filename , "a");
            fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: Deposit || ".$terminalname." || ".serialize($param)."\r\n");
            fclose($fp);
            $result = $client->call('Deposit', $param);

            $mgtransid = $result['DepositResult']['TransactionId'];
            $mgtransstatus = $result['DepositResult']['IsSucceed'];

            //insert to transaction logs
            $ctranslog->StartTransaction();
            $arrTransactionLog["TerminalSessionID"] = $terminalsessionid;
            $arrTransactionLog["TerminalID"] = $terminal;
            $arrTransactionLog["ServiceID"] = 4;
            $arrTransactionLog["DateCreated"] = 'now_usec()';
            $arrTransactionLog["DateUpdated"] = 'now_usec()';
            $arrTransactionLog["TransactionType"] = 'D';
            $arrTransactionLog["Amount"] = $amount;
            $arrTransactionLog["LaunchPad"] = 0;
            $ctranslog->Insert($arrTransactionLog);
            if($ctranslog->HasError)
            {
                $ctranslog->RollBackTransaction();
                $error_title = "ERROR";
                $error_msg = "INSERT Failed - Failed to insert tbl_transactionlogs";
            }
            else
            {
                $ctranslog->CommitTransaction();

                //insert to DB logs
                $filename = "../../DBLogs/logs.txt";
                $fp = fopen($filename , "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || INSERT TO tbl_transactionlogs \r\n");
                fclose($fp);
				
                if ($mgtransstatus == 'true')
                {
                    $mgtransstat = '1';

                    //update transactionlog
                    $ctranslog->StartTransaction();
                    $ctranslog->UpdateTransactionLog($mgtransid, $mgtransstatus, $mgtransstat, $terminalsessionid);
                    if($ctranslog->HasError)
                    {
                        $ctranslog->RollBackTransaction();
                        $error_title = "ERROR";
                        $error_msg = "Update Failed - Failed to update tbl_transactionlogs";
                    }
                    else
                    {
                        $ctranslog->CommitTransaction();
                        //insert to DB logs
                        $filename = "../../DBLogs/logs.txt";
                        $fp = fopen($filename , "a");
                        fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || UPDATE tbl_transactionlogs \r\n");
                        fclose($fp);

                        $bal = $_SESSION['Bal'] - $amount;
                        $_SESSION['Bal'] = $bal;

                        //start session
                        $sessiondtls = $cacctsessions->SelectSessionDetails($_SESSION['sid']);
                        if(count($sessiondtls) > 0)
                        {
                            $terminaldtls = $cterminals->SelectTerminalDetails($terminal);
                            if(count($terminaldtls) > 0)
                            {
                                $error_title = "ERROR";
                                $error_msg = "Terminal is already in use.";
                            }
                            else
                            {
                                $terminalsessiondtls = $cterminalsessions->SelectTerminalSessionDetails($terminal);
                                if(count($terminalsessiondtls) > 0)
                                {
                                    $error_title = "ERROR";
                                    $error_msg = "Terminal session already exists.";
                                }
                                else
                                {
                                    if(($amount == 0) && ($isFreeEntry != 1))
                                    {
                                        $error_title = "ERROR";
                                        $error_msg = "Initial deposit amount must be greater than 0.";
                                    }
                                    else
                                    {
                                        $isFreeEntry = $cterminals->SelectTerminalName($terminal);
                                        $isFreeEntry = $isFreeEntry[0]['IsFreeEntry'];
                                        $siteid = $sessiondtls[0]['SiteID'];
                                        $acctid = $sessiondtls[0]['ID'];
                                        $sitedtls = $csites->SelectSiteDetails($siteid);
                                        $balance = $sitedtls[0]['Balance'];
                                        if($balance < $amount)
                                        {
                                            $error_title = "ERROR";
                                            $error_msg = "Account balance is less than the deposit amount.";
                                        }
                                        else
                                        {
                                            if($isFreeEntry != 1)
                                            {
                                                $newbalance = ($balance - $amount);
                                                $csites->StartTransaction();
                                                $csites->UpdateSiteBalance($newbalance, $siteid);
                                                if($csites->HasError)
                                                {
                                                    $csites->RollBackTransaction();
                                                    $error_title = "ERROR";
                                                    $error_msg = "Error updating site balance.";
                                                }
                                                else
                                                {
                                                    $csites->CommitTransaction();
                                                    //insert to DB logs
                                                    $filename = "../../DBLogs/logs.txt";
                                                    $fp = fopen($filename , "a");
                                                    fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || UPDATE tbl_sites \r\n");
                                                    fclose($fp);

                                                    $cterminals->StartTransaction();
                                                    $cterminals->UpdateTerminalBalanceServiceIDEquivalentPoints($amount,$terminal);
                                                    if($cterminals->HasError)
                                                    {
                                                        $cterminals->RollBackTransaction();
                                                        $error_title = "ERROR";
                                                        $error_msg = "Error updating terminal balance.";
                                                    }
                                                    else
                                                    {
                                                        $cterminals->CommitTransaction();
                                                        //insert to DB logs
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename , "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || UPDATE tbl_terminals \r\n");
                                                        fclose($fp);

														$cterminalsessions->StartTransaction();
														$arrTerminalSessions["TerminalID"] = $terminal;
														$arrTerminalSessions["SiteID"] = $_SESSION['siteid'];
														$arrTerminalSessions["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
														$arrTerminalSessions["DateStart"] = 'now_usec()';
														$arrTerminalSessions["DateCreated"] = 'now_usec()';
														$cterminalsessions->Insert($arrTerminalSessions);
														if($cterminalsessions->HasError)
														{
															$cterminalsessions->RollBackTransaction();
															$error_title = "INVALID START SESSION";
															$error_msg = "Error inserting new terminal session.";
														}
														else
														{
															$cterminalsessions->CommitTransaction();
															$terminalsessionlastinsertid = $cterminalsessions->LastInsertID;
															//insert to DB logs
															$filename = "../../DBLogs/logs.txt";
															$fp = fopen($filename , "a");
															fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || INSERT TO tbl_terminalsessions \r\n");
															fclose($fp);

	                                                        $terminalsessiondetails = $cterminalsessiondtls->SelectMaxId();
	                                                        $lastTransId = ($terminalsessiondetails[0]["max"] != null) ? $terminalsessiondetails[0]["max"] : 0;
	                                                        $lastTransId = ($lastTransId + 1);

	                                                        $transRefId = 'D' . str_pad($terminalsessionlastinsertid,6,0,STR_PAD_LEFT) . str_pad($lastTransId,10,0,STR_PAD_LEFT);

	                                                        $cterminalsessiondtls->StartTransaction();
	                                                        $arrTerminalSessionDetails["TerminalSessionID"] = $terminalsessionlastinsertid;
	                                                        $arrTerminalSessionDetails["TransactionType"] = 'D';
	                                                        $arrTerminalSessionDetails["Amount"] = $amount;
	                                                        $arrTerminalSessionDetails["ServiceID"] = '1';
	                                                        $arrTerminalSessionDetails["ServiceTransactionID"] = $mgtransid;
	                                                        $arrTerminalSessionDetails["TransID"] = $transRefId;
	                                                        $arrTerminalSessionDetails["AcctID"] = $acctid;
	                                                        $arrTerminalSessionDetails["TransDate"] = 'now_usec()';
	                                                        $arrTerminalSessionDetails["DateCreated"] = 'now_usec()';
	                                                        $cterminalsessiondtls->Insert($arrTerminalSessionDetails);
	                                                        if($cterminalsessiondtls->HasError)
	                                                        {
	                                                            $cterminalsessiondtls->RollBackTransaction();
	                                                            $error_title = "ERROR";
	                                                            $error_msg = "Error inserting new terminal session details.";
	                                                        }
	                                                        else
	                                                        {
	                                                            $cterminalsessiondtls->CommitTransaction();
	                                                            //insert to DB logs
	                                                            $filename = "../../DBLogs/logs.txt";
	                                                            $fp = fopen($filename , "a");
	                                                            fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || INSERT TO tbl_terminalsessiondetails \r\n");
	                                                            fclose($fp);

	                                                            $caudittrail->StartTransaction();
	                                                            $arrAuditTrail["SessionID"] = $_SESSION['sid'];
	                                                            $arrAuditTrail["AccountID"] = $acctid;
	                                                            $arrAuditTrail["TransDetails"] = 'Session start for terminalid: ' . $terminal;
	                                                            $arrAuditTrail["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
	                                                            $arrAuditTrail["TransDateTime"] = 'now_usec()';
	                                                            $caudittrail->Insert($arrAuditTrail);
	                                                            if($caudittrail->HasError)
	                                                            {
	                                                                $caudittrail->RollBackTransaction();
	                                                                $error_title = "ERROR";
	                                                                $error_msg = "Error inserting in audit trail.";
	                                                            }
	                                                            else
	                                                            {
	                                                                $caudittrail->CommitTransaction();
	                                                                //insert to DB logs
	                                                                $filename = "../../DBLogs/logs.txt";
	                                                                $fp = fopen($filename , "a");
	                                                                fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || INSERT TO tbl_audittrail \r\n");
	                                                                fclose($fp);

	                                                                //$error_title = "SUCCESS";
	                                                                //$error_msg = "Session successfully started.";

	                                                                $transdate = date("Y-m-d h:i:s A");
	                                                                $url = "\"print.php?points='".$amount."'&terminal='".$terminalname."'&refnum='".$transRefId."'&transdate='".$transdate . "'\"";
	                                                            }
	                                                        }
														}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            $error_title = "ERROR";
                            $error_msg = "Account session does not exist.";
                        }
                    }
                }
                else
                {
                    $mgtransstat = '2';

                    //update transactionlog
                    $ctranslog->StartTransaction();
                    $ctranslog->UpdateTransactionLog($mgtransid, $mgtransstatus, $mgtransstat, $terminalsessionid);
                    if($ctranslog->HasError)
                    {
                        $ctranslog->RollBackTransaction();
                        $error_title = "RESET PASSWORD";
                        $error_msg = "Update Failed - Failed to update tbl_transactionlogs";
                    }
                    else
                    {
                        $ctranslog->CommitTransaction();
                        //insert to logs
                        $filename = "../../DBLogs/logs.txt";
                        $fp = fopen($filename , "a");
                        fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || UPDATE tbl_transactionlogs \r\n");
                        fclose($fp);
                        $error_title = "INVALID DEPOSIT";
                        $error_msg = "An Error Occured. Please try again. API Deposit Error.";
                    }
                }
            }
        }
    }
}
?>
