<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 21, 2012
 */
require_once("../init.inc.php");
include("../config.php");

$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$pagename = "activesession";

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
App::LoadLibrary("MicrogamingAPI.class.php");

$cterminals = new SCC_Terminals();
$csmagentsessions = new SCCSM_AgentSessions();

$activesessions = $cterminals->SelectActiveSessions($_SESSION['siteid']);

$sessionguid = $csmagentsessions->SelectSessionGUID();
$sessionGUID_MG = $sessionguid[0]["SessionGUID"];
$ip_add_MG = '172.20.60.15';
$headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
       "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
       "<IPAddress>".$ip_add_MG."</IPAddress>".
       "<ErrorCode>0</ErrorCode>".
       "<IsLengthenSession>true</IsLengthenSession>".
       "</AgentSession>";
$client = new nusoap_client($mg_url, 'wsdl');
$client->setHeaders($headers);

if(count($activesessions) > 0)
{
    for ($i = 0 ; $i < count($activesessions) ; $i++ )
    {
        $param = array('delimitedAccountNumbers' => $activesessions[$i]["Name"]);
        $filename = "../../APILogs/logs.txt";
        $fp = fopen($filename , "a");
        fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GetAccountBalance || ".$activesessions[$i]["Name"]." || ".serialize($param)."\r\n");
        fclose($fp);
        $a = 1;
        while($a < 4)
        {
            $result = $client->call('GetAccountBalance', $param);
            if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
            {
                $a++;
            }
            else
            {
                break;
            }
        }

        $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];

        $cterminals->StartTransaction();
        $cterminals->UpdateTerminalBalance($balance, $activesessions[$i]["ID"]);
        //$cterminals->UpdateTerminalBalance('1.00', $activesessions[$i]["ID"]);
        if($cterminals->HasError)
        {
            $cterminals->RollBackTransaction();
        }
        else
        {
            $cterminals->CommitTransaction();
        }
    }
}
          $activesessions = $cterminals->SelectActiveSessions($_SESSION['siteid']);
?>
