<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 31, 2012
 */
?>
<div style="height:55px; width:100%; text-align:center;">
    <?php if ($pagename == 'activesession'):?>
        <a href="activesession.php" style="background-image: url(images/ActiveSessionButton-Hover.png); height:55px; width:210px; cursor:pointer; float:left; position:relative;"></a>
    <?php else: ?>
        <a href="activesession.php" style="background-image: url(images/ActiveSessionButton.png); height:55px; width:210px; cursor:pointer; float:left; position:relative;"></a>
    <?php endif; ?>

    <?php if ($pagename == 'printvoucher'):?>
        <a href="printvoucher.php" style="background-image: url(images/StartSessionButton-Hover.png); height:55px; width:210px; cursor:pointer; float:left; position:relative;"></a>
    <?php else: ?>
        <a href="printvoucher.php" style="background-image: url(images/StartSessionButton.png); height:55px; width:210px; cursor:pointer; float:left; position:relative;"></a>
    <?php endif; ?>

    <?php if ($pagename == 'redemption'):?>
        <a href="redemption.php" style="background-image: url(images/RedemptionButton-Hover.png); height:55px; width:210px; cursor:pointer; float:left; position:relative;"></a>
    <?php else: ?>
        <a href="redemption.php" style="background-image: url(images/RedemptionButton.png); height:55px; width:210px; cursor:pointer; float:left; position:relative;"></a>
    <?php endif; ?>

    <?php if ($pagename == 'reloadterminal'): ?>
        <a href="reloadterminal.php" style="background-image: url(images/ReloadSessionButton-Hover.png); height:55px; width:210px; cursor:pointer; float:left; position:relative;"></a>
    <?php else: ?>
        <a href="reloadterminal.php" style="background-image: url(images/ReloadSessionButton.png); height:55px; width:210px; cursor:pointer; float:left; position:relative;"></a>
    <?php endif; ?>

    <?php if ($pagename == 'transhistpayout' || $pagename == 'transhistdeposit' || $pagename == 'transhistconversion' || $pagename == 'transhistgrosshold' || $pagename == 'transhistregistrants' || $pagename == 'transhistwinnings'):?>
        <a href="transhistdeposit.php" style="background-image: url(images/TransactionHistoryButton-Hover.png); height:55px; width:210px; cursor:pointer; float:left; position:relative;"></a>
    <?php else: ?>
        <a href="transhistdeposit.php" style="background-image: url(images/TransactionHistoryButton.png); height:55px; width:210px; cursor:pointer; float:left; position:relative;"></a>
    <?php endif;?>

    <?php if ($pagename == 'voucherusage'): ?>
        <a href="voucherusage.php" style="background-image: url(images/VoucherUsageButton-Hover.png); height:55px; width:210px; cursor:pointer; float:left; position:relative;"></a>
    <?php else: ?>
        <a href="voucherusage.php" style="background-image: url(images/VoucherUsageButton.png); height:55px; width:210px; cursor:pointer; float:left; position:relative;"></a>
    <?php endif; ?>
</div>