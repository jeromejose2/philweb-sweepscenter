<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 05, 2012
 */
include('../controller/changepasswordprocess.php');
?>
<?php include('header.php');?>
<script type="text/javascript">
function checkinput()
{
    var sCurrPass = document.getElementById("txtPassword");
    var sNewPass = document.getElementById("txtNewPassword");
    var sConfPass = document.getElementById("txtConfPassword");

    if (sCurrPass.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        var title = "INSUFFICIENT DATA";
        var msg = "Please fill in all the fields on the form to change your password.";
        document.getElementById('a1').innerHTML = "<font color='#FF0000'>*</font>";
        document.getElementById('title').innerHTML = title;
        document.getElementById('msg').innerHTML = msg;
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }
    else
    {
        document.getElementById('a1').innerHTML = "";
    }

    if (sCurrPass.value.length < 8 && sCurrPass.value.length > 0)
    {
        var title = "INSUFFICIENT DATA";
        var msg = "Password must not be less than 8 characters.";
        document.getElementById('a1').innerHTML = "<font color='#FF0000'>*</font>";
        document.getElementById('title').innerHTML = title;
        document.getElementById('msg').innerHTML = msg;
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }

    if (sNewPass.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        var title = "INSUFFICIENT DATA";
        var msg = "Please fill in all the fields on the form to change your password.";
        document.getElementById('a2').innerHTML = "<font color='#FF0000'>*</font>";
        document.getElementById('title').innerHTML = title;
        document.getElementById('msg').innerHTML = msg;
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }
    else
    {
        document.getElementById('a2').innerHTML = "";
    }

    if (sNewPass.value.length < 8 && sNewPass.value.length > 0)
    {
        var title = "INSUFFICIENT DATA";
        var msg = "Password must not be less than 8 characters.";
        document.getElementById('a2').innerHTML = "<font color='#FF0000'>*</font>";
        document.getElementById('title').innerHTML = title;
        document.getElementById('msg').innerHTML = msg;
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }

    if (sConfPass.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        var title = "INSUFFICIENT DATA";
        var msg = "Please fill in all the fields on the form to change your password.";
        document.getElementById('a3').innerHTML = "<font color='#FF0000'>*</font>";
        document.getElementById('title').innerHTML = title;
        document.getElementById('msg').innerHTML = msg;
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }
    else
    {
        document.getElementById('a3').innerHTML = "";
    }

    if (sConfPass.value.length < 8 && sConfPass.value.length > 0)
    {
        var title = "INSUFFICIENT DATA";
        var msg = "Password must not be less than 8 characters.";
        document.getElementById('a3').innerHTML = "<font color='#FF0000'>*</font>";
        document.getElementById('title').innerHTML = title;
        document.getElementById('msg').innerHTML = msg;
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }
/*
    if ((sCurrPass.value.length < 8 && sCurrPass.value.length > 0) || (sNewPass.value.length < 8 && sNewPass.value.length > 0) || (sConfPass.value.length < 8 && sConfPass.value.length > 0))
    {
        var title = "INSUFFICIENT DATA";
        var msg = "Password must not be less than 8 characters.";
        document.getElementById('title').innerHTML = title;
        document.getElementById('msg').innerHTML = msg;
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }
*/
    if (sNewPass.value != sConfPass.value)
    {
        var title = "UNMATCHING PASSWORDS";
        var msg = "Your passwords information do not match. Please try again.";
        document.getElementById('title').innerHTML = title;
        document.getElementById('msg').innerHTML = msg;
        document.getElementById('light').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }

    var title = "CONFIRMATION";
    var msg = "You are about to change your password. Do you wish to continue?";
    document.getElementById('title2').innerHTML = title;
    document.getElementById('msg2').innerHTML = msg;
    document.getElementById('light2').style.display='block';
    document.getElementById('fade').style.display='block';
    return false;
}
function isAlphaNumericKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if((charCode > 31 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128) || (charCode > 127))
        return false;

    return true;
}
</script>
<div id="page">
    <table>
        <tr>
            <td colspan="2" style="background-color: #5C5858;color:#FFFFFF;font-weight:bold;font-size:1.2em; text-align:center; height:30px; width:800px;">
             Please enter the following information to change your password.
            </td>
        </tr>
        <tr style="background-color:#E9F1EA; height:30px;">
            <td class="fontboldblack">&nbsp;&nbsp;<span id="a1"></span>&nbsp;&nbsp;Current Password:</td>
            <td>
            &nbsp;&nbsp;<?php echo $txtPassword;?>
            </td>
        </tr>
        <tr style="background-color:#E9F1EA; height:30px;">
            <td class="fontboldblack">&nbsp;&nbsp;<span id="a2" style="color: '#FF0000';"></span>&nbsp;&nbsp;New Password:</td>
            <td>
            &nbsp;&nbsp;<?php echo $txtNewPassword;?>
            </td>
        </tr>
        <tr style="background-color:#E9F1EA; height:30px;">
            <td class="fontboldblack">&nbsp;&nbsp;<span id="a3"></span>&nbsp;&nbsp;Confirm New Password:</td>
            <td>
            &nbsp;&nbsp;<?php echo $txtConfPassword;?>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <?php //echo $btnSubmit;?>
                <a href="" id="btnSubmit" name="btnSubmit" class="labelbutton2" onclick="javascript: return checkinput();" style="text-decoration:none; color:#000;">SUBMIT</a>
                <a href="activesession.php" class="labelbutton2" style="text-decoration:none; color:#000;">CANCEL</a>
            </td>
        </tr>
    </table>
</div>
<div id="light" class="white_content">
    <div class="light-title" id="title"></div>
    <div class="light-msg" id="msg"></div>
    <div class="inputBoxEffectPopup" style="margin-left:auto;margin-right:auto;" onclick ="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';"></div>
</div>
<div id="light2" class="white_content">
    <div class="light-title" id="title2"></div>
    <div class="light-msg" id="msg2"></div>
    <?php echo $btnOkay;?>
  <input id="btnCancel" type="button" value="CANCEL" class="labelbold2" onclick="document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';"/>
</div>
<div id="fade" class="black_overlay"></div>
<?php if(isset($error_msg)):?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $error_title;?>";
    document.getElementById('msg').innerHTML = "<?php echo $error_msg;?>";;
    document.getElementById('light').style.display='block';
    document.getElementById('fade').style.display='block';
</script>
<?php endif;?>
<?php include('footer.php');?>