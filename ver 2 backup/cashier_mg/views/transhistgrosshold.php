<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 04, 2012
 */
include('../controller/transhistgrossholdprocess.php');
?>
<?php include('header.php');?>
<script type="text/javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
    function checkdate()
    {
        var date = document.getElementById("txtDateFr").value
        if(date.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
        {
            document.getElementById('light').style.display='block';
            document.getElementById('fade').style.display='block';
            document.getElementById('title').innerHTML = "INVALID INPUT";
            document.getElementById('msg').innerHTML = "Please enter the date.";
            return false;
        }
    }
</script>
<div id="page">
    <table width="1200px">
        <tr>
            <td align="right"><?php include_once('menutrans.php'); ?>
        </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="labelbold2">From:
                            <?php echo $txtDateFr;?>
                            <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
                            <?php echo $btnSearch;?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <?php if(isset($displayrecords)):?>
                <table style="overflow-x:scroll; width:100%;">
                    <tr>
                        <th colspan ="6" style="height:30px;background-color: #5C5858;color:#FFFFFF;">CASH ON HAND</th>
                    </tr>
                    <tr>
                        <th class="th">Transaction Date &amp; Time</th>
                        <th class="th">Terminal Name</th>
                        <th class="th">Sales</th>
                        <th class="th">Reloads</th>
                        <th class="th">Redemptions</th>
                        <th class="th">Cash On Hand</th>
                    </tr>
                    <?php if(count($transhistgrossholddtls) > 0):?>
                    <p class="paging"><b>Displaying <?php echo $start;?>-<?php echo ($end > $transhistcount) ? $transhistcount : $end;?> of <?php echo $transhistcount;?></b></p>
                    <?php for($i = 0 ; $i < count($transhistgrossholddtls) ; $i++):?>
                    <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
                    <tr class="<?php echo $class;?>">
                        <td class="td"><?php echo date_format(date_create($transhistgrossholddtls[$i]['TransDate']), "m/d/Y");?></td>
                        <td class="td"><?php echo $transhistgrossholddtls[$i]['Name'];?></td>
                        <td class="td"><?php echo $transhistgrossholddtls[$i]['Sales'];?></td>
                        <td class="td"><?php echo $transhistgrossholddtls[$i]['Reload'];?></td>
                        <td class="td"><?php echo $transhistgrossholddtls[$i]['Redemption'];?></td>
                        <td class="td"><?php echo $transhistgrossholddtls[$i]['CashOnHand'];?></td>
                    </tr>
                    <?php endfor;?>
                    <?php else:?>
                    <tr>
                        <td colspan="6" align="center"><b>No Records Found.</b></td>
                    </tr>
                    <?php endif;?>
                    <?php if($selectedpage == $totalpages):?>
                    <tr class="evenrow">
                        <td class="labelbold" colspan="6" style="text-align:right;">
                            Total Cash On Hand:<?php echo number_format($totalcashonhand , 2 , '.' , ',')?>
                        </td>
                    </tr>
                    <?php endif;?>
                    <tr>
                        <td colspan="6" align="center"><?php echo $pgTransactionHistory;?></td>
                    </tr>
                </table>
                <?php endif;?>
            </td>
        </tr>
    </table>
</div>
<div id="light" class="white_content">
    <div class="light-title" id="title"></div>
    <div class="light-msg" id="msg"></div>
    <div class="inputBoxEffectPopup" style="margin-left:auto;margin-right:auto;" onclick ="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';"></div>
</div>
<div id="fade" class="black_overlay"></div>
<?php include('footer.php');?>