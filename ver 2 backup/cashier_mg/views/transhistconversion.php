<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 01, 2012
 */
include('../controller/transhistconversionprocess.php');
?>
<?php include('header.php');?>
<script type="text/javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
</script>
<div id="page">
    <table width="1200px">
        <tr>
            <td align="right"><?php include_once('menutrans.php'); ?>
        </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="labelbold2">From:</td>
                        <td>
                            <?php echo $txtDateFr;?>
                            <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
                        </td>
                        <td class="labelbold2">To:</td>
                        <td>
                            <?php echo $txtDateTo;?>
                            <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
                        </td>
                        <td class="labelbold2">Terminal:&nbsp;<?php echo $ddlTerminal;?></td>
                        <td width="100px"><?php echo $btnSearch;?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <?php if(isset($displayrecords)): ?>
                <table style="overflow-x:scroll; width:100%;">
                    <tr>
                        <th colspan ="7" style="height:30px;background-color: #5C5858;color:#FFFFFF;">POINTS CONVERSION HISTORY</th>
                    </tr>
                    <tr>
                        <th class="th">Transaction Reference ID</th>
                        <th class="th">Terminal Name</th>
                        <th class="th">Date & Time of Transaction</th>
                        <th class="th">Converted Points</th>
                        <th class="th">Equivalent Cards</th>
                        <th class="th">Number of Winning Cards</th>
                        <th class="th">Amount of Winnings</th>
                    </tr>
                    <?php if(count($transhistconversiondtls) > 0):?>
                    <p class="paging"><b>Displaying <?php echo $start;?>-<?php echo ($end > $transhistcount) ? $transhistcount : $end;?> of <?php echo $transhistcount;?></b></p>
                    <?php for($i = 0 ; $i < count($transhistconversiondtls) ; $i++):?>
                    <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
                    <tr class="<?php echo $class;?>">
                        <td class="td"><?php echo $transhistconversiondtls[$i]["TransSummID"];?></td>
                        <td class="td"><?php echo $transhistconversiondtls[$i]["Name"];?></td>
                        <td class="td"><?php echo date_format(date_create($transhistconversiondtls[$i]["TransDate"]), "m/d/Y H:i:s");?></td>
                        <td class="td"><?php echo $transhistconversiondtls[$i]["Balance"];?></td>
                        <td class="td"><?php echo $transhistconversiondtls[$i]["EquivalentCards"];?></td>
                        <td class="td"><?php echo $transhistconversiondtls[$i]["NoOfWinningCards"];?></td>
                        <td class="td"><?php if (strpos($transhistconversiondtls[$i]["Winnings"],',') == 0){ echo substr($transhistconversiondtls[$i]["Winnings"],1); } else {echo $transhistconversiondtls[$i]["Winnings"];} ?></td>
                    </tr>
                    <?php endfor;?>
                    <?php else:?>
                    <tr>
                        <td colspan="7" align="center"><b>No Records Found.</b></td>
                    </tr>
                    <?php endif;?>
                    <tr>
                        <td colspan="7" align="center"><?php echo $pgTransactionHistory;?></td>
                    </tr>
                </table>
                <?php endif;?>
            </td>
        </tr>
    </table>
</div>
<div id="light" class="white_content">
    <div class="light-title" id="title"></div>
    <div class="light-msg" id="msg"></div>
    <div class="inputBoxEffectPopup" style="margin-left:auto;margin-right:auto;" onclick ="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';"></div>
</div>
<div id="fade" class="black_overlay"></div>
<?php include('footer.php');?>