<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 05, 2012
 */
include('../controller/transhistwinningsprocess.php');
?>
<?php include('header.php');?>
<style>
.sweepsContainer{
    background: url(images/cardsContainer.png) 0 0 no-repeat;
    width:177px;
    height:178px;
    margin:10px 25px 0 0px;
    float:left;
}
.win_container{
	background: url(images/card02.png) 0 0 no-repeat;
	width:143px;
	height:143px;
	margin: 17px 0 0 17px;
	float:left;
        cursor: pointer;
}
.win_container p{
	font:  20px/20px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
        font-weight: bold;
	color:#000;
	text-decoration:none;
	text-align:center;
	width:143px;
	height:85px;
	margin: 40px 0 0 0px;
	float:left;
}
.win_container span{
	font:  12px/20px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	color:#000;
	text-decoration:none;
	text-align:center;
	width:143px;
	height:20px;
	margin: 0px 0 0 0px;
	float:left;
}
.winsumm{
	font:  18px/20px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	color:#6b0e00;
	text-decoration:none;
	text-align: center;
	width: 85%;
	height:20px;
	margin: -5px 0 0 80px;
	float:left;
        font-weight: bold;
}
</style>
<script type="text/javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
    function gettransnum()
    {
        document.getElementById("ddltransno").disabled = false;
        var tid = document.getElementById('ddlternme').value;

        $.ajax({
            url: 'transhistwinningsajax.php?tid='+tid,
            type : 'post',
            success : function(data)
            {
            $('#ddltransno').html(data)
            },
            error: function(e)
            {
            alert("Error");
            }
        });
    }
</script>
<div id="page">
    <table width="1200px">
        <tr>
            <td align="right"><?php include_once('menutrans.php'); ?>
        </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="labelbold2">Terminal Name:
                            <?php echo $ddlternme;?>
                        </td>
                        <td class="labelbold2">Transaction Reference ID:
                            <?php echo $ddltransno;?>
                        </td>
                        <td class="labelbold2">
                            <?php echo $btnSearch;?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <?php if(isset($winningdtls)):?>
                    <div style='margin-left: 80px;'><?php echo $winningdtls;?></div>
                <?php endif;?>
                <?php if(isset($displayrecords)):?>
                <?php for($i = 0 ; $i < count($carddtls) ; $i++):?>
                <?php $type = ($carddtls[$i]['PrizeValue'] == '0') ? "Try Again" : "$". $carddtls[$i]['PrizeValue'] ." Cash";?>
                    <div class="sweepsContainer">
                        <div class="win_container"><p><?php echo $type;?></p><span><?php echo $carddtls[$i]['ECN'];?></span></div>
                    </div>
                <?php endfor;?>
                <?php endif;?>
                <?php if(isset($winnings)):?>
                <?php if($winnings == ""):?>
                    <div class='winsumm'>
                        You did not get any winning card. Your Transaction Reference ID is <?php echo str_pad($transsummid,14,'0',STR_PAD_LEFT);?>.
                    </div>
                <?php else:?>
                    <div class='winsumm'>
                        You've won the following prize/s: <?php echo $winnings;?>. Your Transaction Reference ID is <?php echo str_pad($transsummid,14,'0',STR_PAD_LEFT);?>.
                    </div>
                <?php endif;?>
                <?php endif;?>
            </td>
        </tr>
    </table>
</div>
<div id="light" class="white_content">
    <div class="light-title" id="title"></div>
    <div class="light-msg" id="msg"></div>
    <div class="inputBoxEffectPopup" style="margin-left:auto;margin-right:auto;" onclick ="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';"></div>
</div>
<div id="fade" class="black_overlay"></div>
<?php include('footer.php');?>