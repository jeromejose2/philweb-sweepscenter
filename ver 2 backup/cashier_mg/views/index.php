<?php
/* 
 * Created By: Arlene R. Salazar
 * Created On: May 21, 2012
 */
include('../controller/loginprocess.php');
?>
<html>
    <head>
        <script type="text/javascript">
            function preventBackandForward()
            {
            window.history.forward();
            }

            preventBackandForward();
            window.onload=preventBackandForward();
            window.inhibited_load=preventBackandForward;
            window.onpageshow=function(evt){if(evt.persisted)preventBackandForward();};
            window.inhibited_unload=function(){void(0);};
        </script>
        <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
        <title>Guam Sweepstakes Caf&#233; Cashier</title>
        <script type="text/javascript">
        <!--
        function checkLogin()
        {
            var oUsername = document.getElementById("txtUsername");
            var oPassword = document.getElementById("txtPassword");

            oUsername = oUsername.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
            oPassword = oPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

            if (oUsername.length == 0)
            {
                document.getElementById('title1').innerHTML = "INVALID LOG IN";
                document.getElementById('msg1').innerHTML = "Please enter your username.";
                document.getElementById('light1').style.display='block';
                document.getElementById('fade').style.display='block';
                return false;
            }
            if (oPassword.length == 0)
            {
                document.getElementById('title1').innerHTML = "INVALID LOG IN";
                document.getElementById('msg1').innerHTML = "Please enter your password.";
                document.getElementById('light1').style.display='block';
                document.getElementById('fade').style.display='block';
                return false;
            }
            return true;
        }

        function openforgotpw()
        {
            document.getElementById('light4').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }

        function redirect()
        {
            var email = document.getElementById('txtEmail').value;
            if(email.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
            {
                document.getElementById('title4').innerHTML = "INVALID E-MAIL";
                document.getElementById('sub-msg4').innerHTML = "Please provide e-mail address.";
                document.getElementById('light4').style.display='block';
                document.getElementById('fade').style.display='block';
                return false;
            }
            if(!checkemail(email))
            {
                document.getElementById('title4').innerHTML = "INVALID E-MAIL";
                document.getElementById('sub-msg4').innerHTML = "You have entered an invalid e-mail address. Please try again.";
                document.getElementById('light4').style.display='block';
                document.getElementById('fade').style.display='block';
                return false;
            }
            return true;
        }

        function checkemail(email)
        {
            var str=email
            var filter=/^.+@.+\..{2,3}$/

            if (filter.test(str))
            return true;
            else
            return false;
            return true;
        }
        </script>
    </head>
    <body>
    <form id="frmLogin" name="frmLogin" action="index.php"  method="post">
        <div id="container">
            <div id="login">
                <table cellspacing="6" width="100%">
                    <tr>
                        <td colspan="2" align="center">
                        <div style="background-image: url(images/SweepsLogo.png); height:139px; width:293px;"></div>
                        <br />
                        <h2>Cashier</h2>
                        </td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td colspan="2" class="fontboldblack" align="center">Username:&nbsp;&nbsp;<?php echo $txtUsername;?></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="fontboldblack" align="center">Password:&nbsp;&nbsp;&nbsp;<?php echo $txtPassword;?></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><?php echo $btnSubmit;?></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                        <div>
                        <a href="#" onclick="javascript: return openforgotpw();" style="font-size:1.0em; font-weight:bold; cursor:pointer; color:#000000;">Forgot Password</a>
                        </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="light1" class="white_content">
            <div class="light-title" id="title1"></div>
            <div class="light-message" id="msg1"></div>
            <div type="button" class="inputBoxEffectPopup" style="margin-left:auto;margin-right:auto;" onclick ="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';document.getElementById('txtUsername').focus();"></div>
        </div>
        <!-- FORGOT PASSWORD-->
        <div id="light4" class="white_content">
            <div class="light-title" id="title4">RESET PASSWORD CONFIRMATION</div>
            <div class="light-message" id="msg4">
                <div id="sub-msg4">You are about to reset your current password. Please enter your registered e-mail address.</div>
                <br/><br/>
                <?php echo $txtEmail;?>
            </div>
            <br />
            <?php echo $btnReset;?>
            <input id="btn" type="button" value="CANCEL" class="labelbold2" onclick="document.getElementById('light4').style.display='none';document.getElementById('fade').style.display='none';document.getElementById('txtEmail').value='';"></input>
        </div>
        <!-- FORGOT PASSWORD-->
        <div id="fade" class="black_overlay"></div>
        <?php if(isset($error_msg)):?>
        <script>
            document.getElementById('title1').innerHTML = "<?php echo $error_title;?>";
            document.getElementById('msg1').innerHTML = "<?php echo $error_msg;?>";
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
        </script>
        <?php endif;?>
    </form>
    </body>
</html>