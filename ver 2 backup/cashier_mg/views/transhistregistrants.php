<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 04, 2012
 */
include('../controller/transhistregistrantsprocess.php');
?>
<?php include('header.php');?>
<script type="text/javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
</script>
<div id="page">
    <table width="1200px">
        <tr>
            <td align="right"><?php include_once('menutrans.php'); ?>
        </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="labelbold2">From:
                            <?php echo $txtDateFr;?>
                            <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
                        </td>
                        <td class="labelbold2">To:
                            <?php echo $txtDateTo;?>
                            <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
                        </td>
                        <td class="labelbold2">
                            <?php echo $btnSearch;?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <?php if(isset($displayrecords)):?>
                <table style="overflow-x:scroll;width:100%;">
                    <tr>
                        <th colspan ="11" style="height:30px;background-color: #5C5858; color:#FFFFFF;">FREE ENTRY REGISTRATION HISTORY</th>
                    </tr>
                    <tr>
                        <th class="th">Name</th>
                        <th class="th">Birth Date</th>
                        <th class="th">Home Phone</th>
                        <th class="th">Mobile Phone</th>
                        <th class="th">Address</th>
                        <th class="th">Email</th>
                        <th class="th">Voucher Code</th>
                        <th class="th">Date Registered</th>
                        <th class="th">Date Used</th>
                        <th class="th">Terminal Name</th>
                        <th class="th">Winnings</th>
                    </tr>
                    <?php if(count($transhistregistrantsdtls) > 0):?>
                    <p class="paging"><b>Displaying <?php echo $start;?>-<?php echo ($end > $transhistcount) ? $transhistcount : $end;?> of <?php echo $transhistcount;?></b></p>
                    <?php for($i = 0 ; $i < count($transhistregistrantsdtls) ; $i++):?>
                    <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
                    <tr class="<?php echo $class;?>">
                        <td class="td"><?php echo $transhistregistrantsdtls[$i]['LName'] . " ," . $transhistregistrantsdtls[$i]['FName'];?></td>
                        <td class="td"><?php echo $transhistregistrantsdtls[$i]['BirthDate']?></td>
                        <td class="td"><?php echo $transhistregistrantsdtls[$i]['HomePhone']?></td>
                        <td class="td"><?php echo $transhistregistrantsdtls[$i]['MobilePhone']?></td>
                        <td class="td"><?php echo $transhistregistrantsdtls[$i]['Address']?></td>
                        <td class="td"><?php echo $transhistregistrantsdtls[$i]['Email']?></td>
                        <td class="td"><?php echo $transhistregistrantsdtls[$i]['VoucherCode']?></td>
                        <td class="td"><?php echo date_format(date_create($transhistregistrantsdtls[$i]['DateRegistered']), "Y-m-d")?></td>
                        <td class="td"><?php echo date_format(date_create($transhistregistrantsdtls[$i]['DateUsed']), "Y-m-d")?></td>
                        <td class="td"><?php echo $transhistregistrantsdtls[$i]['Name']?></td>
                        <td class="td"><?php echo $transhistregistrantsdtls[$i]['Winnings']?></td>
                    </tr>
                    <?php endfor;?>
                    <?php else:?>
                    <tr>
                        <td colspan="11" align="center"><b>No Records Found.</b></td>
                    </tr>
                    <?php endif;?>
                    <tr>
                        <td colspan="11" align="center"><?php echo $pgTransactionHistory;?></td>
                    </tr>
                </table>
                <?php endif;?>
            </td>
        </tr>
    </table>
</div>
<div id="light" class="white_content">
    <div class="light-title" id="title"></div>
    <div class="light-msg" id="msg"></div>
    <div class="inputBoxEffectPopup" style="margin-left:auto;margin-right:auto;" onclick ="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';"></div>
</div>
<div id="fade" class="black_overlay"></div>
<?php include('footer.php');?>