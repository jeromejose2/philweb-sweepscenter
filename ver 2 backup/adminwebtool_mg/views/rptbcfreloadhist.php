<?php

/*
 * Created by Jerome Jose
 * Date June 22,2012
 * Purpose : View SCF Reload history
 */
include("../controller/rptbcfreloadhistprocess.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />

<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script language="javascript" src="jscripts/datepicker.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script language="javascript" type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
       
    }
</script>
    
<?php include('header.php')?>
<form name="frmreload" method ="POST">
<div style="width:100%; text-align:center;">
  <table width="100%">
    <tr>
      <td class="labelbold2">From:</td>
      <td>
          <?php echo $txtDateFr;?>
        <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
      </td>
      <td class="labelbold2">To:</td>
      <td><?php echo $txtDateTo;?>
        <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
      </td>
      <td width="100px">
          <?php echo $btnSearch;?>
      </td>
    </tr>
    <tr>
      <td colspan="5" class="labelbold2">POS Account Name:&nbsp;&nbsp;
        <?php echo $ddlPos;?>
      </td>
    </tr>
    <tr>
      <?php if(isset($_SESSION['start'])){?>
      <td colspan="5">
          <br />
         <table style="overflow-x:scroll;width:100%;" ><tr><th colspan ="3" style="height:30px;background-color: #FF9C42; color:#000000;">SCF RELOAD HISTORY</th></tr>
        <tr><th class="th" style="width:25%;">Date Processed</th><th class="th" style="width:20%;">Processed By</th><th class="th" style="width:55%;">Reload Details</th></tr>

          
          <?php 
          if(count($selectPosAcct) > 0)
          {
         echo  "<br/>";
         echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".count($selectPosAcct1)." of ".count($selectPosAcct1)."</b></p>";
            $total = 0;
            for($x=0;$x<count($selectPosAcct);$x++)
                 {
                        $datecreated = $selectPosAcct[$x]['RecCreOn'];
                        $username = $selectPosAcct[$x]['Username'];
                        $refNo = $selectPosAcct[$x]['RefNo'];
                        $amount = $selectPosAcct[$x]['Amount'];
                        $posacctname = $selectPosAcct[$x]['POSAcctName'];
                        $posacctno = $selectPosAcct[$x]['POSAcctNo'];
                        $total = $amount + $total;
               $mod = $x % 2;
               
                 if ($mod == 0)
                  {
                    echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"td\">".$datecreated."</td><td class=\"td\">".$username;
                    echo "</td><td class=\"td\"><table><tr><td><b>Transaction Number:</b></td><td>".$refNo."</td></tr>";
                    echo "<tr><td><b>Reloaded Points:</b></td><td>".number_format($amount,2)."</td></tr><tr><td><b>POS Account Name:</b></td><td>".$posacctname."</td></tr><tr><td><b>POS Account Number:</b></td><td>".$posacctno."</td></tr></table></td></tr>";
                  }
                  else
                  {
                    echo "<tr style=\"height:30px;\"><td class=\"td\">".$datecreated."</td><td class=\"td\">".$username;
                    echo "</td><td class=\"td\"><table><tr><td><b>Transaction Number:</b></td><td>".$refNo."</td></tr>";
                    echo "<tr><td><b>Reloaded Points:</b></td><td>".number_format($amount,2)."</td></tr><tr><td><b>POS Account Name:</b></td><td>".$posacctname."</td></tr><tr><td><b>POS Account Number:</b></td><td>".$posacctno."</td></tr></table></td></tr>";
                  }
                 }
                 
                   if(($pgcon->SelectedItemFrom-1 + count($selectPosAcct)) == count($selectPosAcct1))
                  {
                   echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"labelbold\">&nbsp;Total Reload Deposits</td><td colspan=\"2\" class=\"labelbold\">&nbsp;".number_format($total,2)."</td></tr>";

                  }
                    echo "</td></tr><tr><td colspan=\"3\" align=\"center\"></td></tr><tr><td colspan=\"3\" align=\"center\">";
                    echo "<div>";
                    echo  "<br/>"; 
                    echo "<b class=\"paging\">$pgHist </b>";
                    echo "</div>";
                   
              //download button
           echo  "<br/>"; 
           echo " <b><a href='export_report.php?fn=BCF_Reload_History' class=\"labelbutton2\">DOWNLOAD</a></b>";

            }else
            {
                echo "<tr><td colspan=\"3\" align=\"center\"><b>No Records Found.</b></td></tr>";
            }
           echo "<tr><td colspan=\"3\" align=\"center\">";
           echo "</table>";
          ?>
          
          
          
          <?php
         unset($_SESSION['report_header']);
	 unset($_SESSION['report_values']);
       //$_SESSION['report_header']=array("\"DATE PROCESSED\"","\"PROCESSED BY\"","\"TRANSACTION NUMBER\"","\"RELOADED POINTS\"","\"POS ACCOUNT NAME\"","\"POS ACCOUNT NUMBER\"");
       $_SESSION['report_header']=array("DATE PROCESSED,PROCESSED BY,TRANSACTION NUMBER,RELOADED POINTS,POS ACCOUNT NAME,POS ACCOUNT NUMBER");
          for($counter=0;$counter< count($selectPosAcct1);$counter++)
		{
               
			$_SESSION['report_values'][$counter][0] = "".$selectPosAcct1[$counter]['RecCreOn'].",";
			$_SESSION['report_values'][$counter][1] = "".$selectPosAcct1[$counter]['Username'].",";
			$_SESSION['report_values'][$counter][2] = "".$selectPosAcct1[$counter]['RefNo'].",";
			$_SESSION['report_values'][$counter][3] = "".$selectPosAcct1[$counter]['Amount'].",";
			$_SESSION['report_values'][$counter][4] = "".$selectPosAcct1[$counter]['POSAcctName'].",";
			$_SESSION['report_values'][$counter][5] = "".$selectPosAcct1[$counter]['POSAcctNo']."\r\n";
                 }
                $_SESSION['report_values'][($selectPosAcct1)][0] = "Total Reload Deposits";	
		$_SESSION['report_values'][($selectPosAcct1)][1] = $total;
         ?>
      </td>
      <?php }?>

   </tr>
   
    <tr>
      <td colspan="5">&nbsp;</td>
    </tr>
      <tr>
      <td colspan="5" align="center">
        <!--<input id="btnDownload" name="btnDownload"  type="button" value="Download PDF" class="labelbutton2" onclick="javascript: return alert('download here');"></input>-->
      </td>
    </tr>
  </table>
    	<!-- ERROR MESSAGE -->
	<div id="light22" style="text-align: center;font-size: 16pt;height: 300px;width:500px;" class="white_content">
		<div id="title" style="width: p00px;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">
		
		</div>
		<br />
		<br />
		<div id="msg"></div>
		<br/>
		<br/>
		<br />	
		<input id="btnOk" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light22').style.display='none';document.getElementById('fade').style.display='none';" />
	</div>
	<!-- END OF NOTIFICATION MESSAGE -->	
 </div>
    </form>
    
    <?php include('footer.php')?>