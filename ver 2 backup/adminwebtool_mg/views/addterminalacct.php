<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 21, 2012
 * Purpose          :       Add Terminal Account View Page.
 */
include("../controller/addterminalacctprocess.php");
?>
<?php include("header.php"); ?>
<script type="text/javascript">
    function SelectPOSAccountName()
    {
        var acctid = document.getElementById('ddlacctno').options[document.getElementById('ddlacctno').selectedIndex].value;
        $("#ddlacctname").load(
            "../controller/get_posaccountname.php",
            {
                acctid: acctid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    
    function SelectPOSAccountNumber()
    {
        var acctname = document.getElementById('ddlacctname').options[document.getElementById('ddlacctname').selectedIndex].value;
        $("#ddlacctno").load(
            "../controller/get_posaccountid.php",
            {
                acctname: acctname
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
</script>

<div id="divselect">
    <div align="center" style="margin-left: 10px; margin-top: 5px;">
        <table>
            <tr>
                <td><b>POS Account Name:</b></td>
                <td><?php echo $ddlacctname; ?></td>
            </tr>
            <tr>
                <td><b>POS Account Number:</b></td>
                <td><?php echo $ddlacctno; ?></td>
            </tr>
        </table>
        <br/>
        <div align="center"><?php echo $btnSubmit; ?></div>
    </div>
</div>

<div id="showrecord">
    <br/><br/>
    <table width="100%">
        <tr style="background-color:#FF9C42; height:30px;">
            <td class="fontboldblack" style="width:50%;">&nbsp;&nbsp;Account Name:</td>
            <td style="width:50%;">&nbsp;&nbsp;<b><?php echo $acct_name ?></b>
                <input type="hidden" name="fname" value="<?php echo $acct_name ?>" />
                <input type="hidden" name="txtEmail" value="<?php echo $email ?>" /></td>
        </tr>
        <tr style="background-color:#FFF1E6; height:30px;">
            <td class="fontboldblack" style="width:400px;">&nbsp;&nbsp;Account Number:</td>
            <td style="width:400px;">&nbsp;&nbsp;<?php echo $acct_id ?></td>
        </tr>
        <tr style="background-color:#FFF1E6; height:30px;">
            <td class="fontboldblack" style="width:400px;">&nbsp;&nbsp;SWC Code:</td>
            <td style="width:400px;">&nbsp;&nbsp;<?php echo $swccode ?>
            <input type="hidden" value="<?php echo $swccode ?>" name="swcode" /></td>
        </tr>
        <tr style="background-color:#FFF1E6; height:30px;">
            <td class="fontboldblack" style="width:400px;">&nbsp;&nbsp;Terminal Accounts:</td>
            <td style="width:400px;">&nbsp;&nbsp;<?php echo $terminal_count ?></td>
        </tr>
    </table><br/>
    <table style="margin-left: 10px;">
        <tr>
            <td><b>Add Terminal Accounts:</b></td>
            <td><?php echo $txtnumofterminals; ?></td>
        </tr>
        <tr>
            <td><b>Terminal Type:</b></td>
            <td><?php echo $ddlterminaltype; ?></td>
        </tr>
    </table>
    <br/>
    <div align="center"><input <?php if($status != 1) { echo 'disabled="disabled" style="cursor: default;"'; } ?> id="btnCreate" name="btnCreate"  type="submit" value="Create" class="labelbutton2" onclick="" /></div>
</div>
    
<script type="text/javascript">
$(document).ready(function(){
   $('#showrecord').hide(); 
   
   $('#btnSubmit').click(function(){
      if($('#ddlacctname option:selected').val() == 'Select One') {
         html = '<div class=\"titleLightbox\">Notification</div>';
         html += '<div class=\"msgLightbox\">';
         html += '<p>Please select an account name and number.</p>';
         html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
         html += '</div>';
         showLightBox(html);
         return false;
      }
      return true;
   });
   
   <?php if (isset($acct_name)): ?>
        $('#showrecord').show(); 
    <?php endif; ?>
   
   $('#btnCreate').click(function(){
        var type = $('#ddlterminaltype option:selected').text();
        html = '<div class=\"titleLightbox\">Confirmation</div>';
        html += '<div class=\"msgLightbox\">';
        html += '<p>A ' + type + ' account will be created for <?php echo $swccode ?>. Do you wish to continue? </p>';
        html += '<br /><input id=\"btnSubmitOkay\" name=\"btnSubmitOkay\" type=\"submit\" value=\"OKAY\" class=\"labelbutton2\" />&nbsp;&nbsp;<input id=\"btnCancel\" type=\"button\" value=\"CANCEL\" class=\"labelbutton2\" />';
        html += '</div>';
        showLightBox(html);
        return false;
   });
   
   $('#btnCancel').live('click',function(){
      $('#light').fadeOut('slow',function(){
         $('#light').remove();
      });
      $('#fade').fadeOut('slow');
      return false;
   });
});

<?php if (isset($errormsg)) : ?>
    var msg = "<?php echo $errormsg; ?>";
    var msgtitle = "<?php echo $errormsgtitle; ?>";
    html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
    html += '<div class=\"msgLightbox\">';
    html += '<p>' + msg + '</p>';
    html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
    html += '</div>';
    showLightBox(html);
<?php endif; ?>
</script>
<?php include("footer.php"); ?>