<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 19, 2012
 * Purpose          :       Manage POS Account: Managing of POS Account
 */

include("../controller/manageposacctprocess.php");
?>
<?php include("header.php"); ?>
<style type="text/css">
    .hasSpace {
        border-collapse: collapse;
    }
    
    .hasSpace td {
        padding-top: .3em;
        padding-bottom: .3em;
    }
</style>
<script type="text/javascript">
    function SelectPOSAccountName()
    {
        var acctid = document.getElementById('ddlacctno').options[document.getElementById('ddlacctno').selectedIndex].value;
        $("#ddlacctname").load(
            "../controller/get_posaccountname.php",
            {
                acctid: acctid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    
    function SelectPOSAccountNumber()
    {
        var acctname = document.getElementById('ddlacctname').options[document.getElementById('ddlacctname').selectedIndex].value;
        $("#ddlacctno").load(
            "../controller/get_posaccountid.php",
            {
                acctname: acctname
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
</script>

<div align="center" style="margin-left: 10px; margin-top: 5px;">
    <table>
        <tr>
            <td><b>Account Name:</b></td>
            <td><?php echo $ddlacctname; ?></td>
        </tr>
        <tr>
            <td><b>Account Number:</b></td>
            <td><?php echo $ddlacctno; ?></td>
        </tr>
    </table>
    <br/>
    <div align="center"><?php echo $btnSubmit; ?></div>
</div>

<?php if ($display) : ?>
    <br/><hr>
    <div style="margin-left: 10px; margin-top: 5px;">
        <div style="font-weight: bold;">Manage POS Account</div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr>
                <td>ACCOUNT NAME:</td><td><b><?php echo $acct_name ?>
                <input type="hidden" name="fname" value="<?php echo $acct_name; ?>" /></b>
                <input type="hidden" name="txtEmail" value="<?php echo $email; ?>" /></b>
                </td>
            </tr>
            <tr>
                <td>ACCOUNT NUMBER:</td>
                <td>
                   <b>
                      <?php echo $acct_id ?>
                       <input type="hidden" name="acctid" value="<?php echo $acct_id; ?>" />
                   </b>
               </td>
            </tr>
        </table>
        <br/><hr><br/>
        
        <div style="font-weight: bold;"><u>Account Status Information</u></div>
        <table class="hasSpace" style="margin-top: 10px;">
            <tr><td>Current Account Status:</td><td><b><?php echo $wordstat ?></b><input type="hidden" name="oldstat" value="<?php echo $wordstat ?>" /></td></tr>
            <tr id="showremarks"><td>Remarks:</td><td><b><?php echo $remarks; ?></b></td></tr>
            <tr id="editstatus">
                <td>Set Account Status:</td>
                <td>
                    <select id="ddlacctstatus" name="ddlacctstatus" style=" border-color: #FF9C42;">
                        <option value="selectone">Select One</option>
                        <?php if($wordstat != 'Active') echo '<option value="1">Active</option>'; ?>
                        <?php if($wordstat != 'Suspended') echo '<option value="2">Suspended</option>'; ?>
                        <?php if($wordstat != 'Terminated') echo '<option value="3">Terminated</option>'; ?>
                    </select>
                    <input type="hidden" name="newstat" value="" />
                </td>
            </tr>
            <tr id="editremarks">
                <td>Remarks:</td><td><textarea id="remarks" name="remarks" cols="" rows="" style="width: 400px; height: 100px; border-color: #FF9C42; border-style: solid;" onKeyDown="limitText(this.form.remarks,this.form.countdown,500);" onKeyUp="limitText(this.form.remarks,this.form.countdown,500);"><?php echo $remarks; ?></textarea></td>
            </tr>
            <tr id="maxlength">
                <td></td><td>Max. 500 chars <input readonly type="text" name="countdown" size="3" value="500" style=" border-color: #FF9C42;"></td>
            </tr>
        </table>
        <br/>
        
        <div id="divedit" align="center">
            <input <?php if($wordstat == 'Terminated') echo 'disabled="disabled"'; ?> id="btnEdit" name="btnEdit" value="Edit" class="labelbutton2" onclick="" />
        </div>
        <div id="divapply" align="center">
           <input id="btnBack" name="btnBack" value="Back" class="labelbutton2" />&nbsp;&nbsp;
           <input id="btnApply" name="btnApply" value="Apply" class="labelbutton2" />
        </div>
    </div>
<?php endif; ?>

<script type="text/javascript">
$(document).ready(function(){
    $('#editstatus').hide();
    $('#editremarks').hide();
    $('#maxlength').hide();
    $('#divapply').hide();
    
    $('#btnSubmit').click(function(){
    if($('#ddlacctname option:selected').val() == 'Select One') {
        html = '<div class=\"titleLightbox\">Notification</div>';
        html += '<div class=\"msgLightbox\">';
        html += '<p>Please select an account name and number</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        return false;
    }
    return true;
    })
    
    $('#btnApply').live('click',function(){
         html = '<div class=\"titleLightbox\">Notification</div>';
         html += '<div class=\"msgLightbox\">';
      if($('#ddlacctstatus').val() == 'selectone') {
         html += '<p>Please enter new account status.</p>';
         html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
         html += '</div>';
         showLightBox(html);
         return false;
      }
      if($('#remarks').val() == '') {
         html += '<p>Please enter the reason for changing the account status.</p>';
         html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
         html += '</div>';
         showLightBox(html);
         return false;
      }
      
        html = '<div class=\"titleLightbox\">Confirmation</div>';
        html += '<div class=\"msgLightbox\">';
        var newstatus = $('#ddlacctstatus option:selected').text();
        html += '<p>Are you sure you want to change <?php echo $acct_name ?>\'s status from <?php echo $wordstat ?> to ' + newstatus + '?</p>';
        html += '<br /><br />';
        html += '<input id=\"btnSubmitOkay\" name=\"btnSubmitOkay\" type=\"submit\" value=\"OKAY\" class=\"labelbutton2\" />&nbsp;&nbsp;';
        html += '<input id=\"btnSubmitCancel\" name=\"btnSubmitCancel\" type=\"submit\" value=\"CANCEL\" class=\"labelbutton2\" />';
        showLightBox(html);
        return false;
   });
   
   $('#btnSubmitCancel').live('click',function(){
      $('#light').fadeOut('slow',function(){
         $('#light').remove();
      });
      $('#fade').fadeOut('slow');
      return false;
   });
   
   $('#btnSubmitOkay').live('click', function(){
        $('#newstat').val($("#ddlacctstatus option:selected").text());
        $('#frmTemplate').attr('action','manageposacct.php');
        $('#frmTemplate').submit();
   })
    
    $('#btnEdit').click(function(){
        $('#editstatus').show();
        $('#editremarks').show();
        $('#maxlength').show();
        $('#divapply').show();
        $('#showremarks').hide();
        $('#divedit').hide();
        return false;
    })
    
    $('#btnBack').click(function(){
        $('#editstatus').hide();
        $('#editremarks').hide();
        $('#maxlength').hide();
        $('#divapply').hide();
        $('#showremarks').show();
        $('#divedit').show();
    })
});

<?php if (isset($errormsg)) : ?>
    var msg = "<?php echo $errormsg; ?>";
    var msgtitle = "<?php echo $errormsgtitle; ?>";
    html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
    html += '<div class=\"msgLightbox\">';
    html += '<p>' + msg + '</p>';
    html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
    html += '</div>';
    showLightBox(html);
<?php endif; ?>
</script>
<?php include("footer.php"); ?>
