<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 27, 2012
 * Purpose          :       Change Terminal Account Password
 */

include("../controller/changepassterminalacctprocess.php");
?>
<?php include("header.php"); ?>
<script type="text/javascript">
    function SelectPOSAccountName()
    {
        var acctid = document.getElementById('ddlacctno').options[document.getElementById('ddlacctno').selectedIndex].value;
        $("#ddlacctname").load(
            "../controller/get_posaccountname.php",
            {
                acctid: acctid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    
    function SelectPOSAccountNumber()
    {
        var acctname = document.getElementById('ddlacctname').options[document.getElementById('ddlacctname').selectedIndex].value;
        $("#ddlacctno").load(
            "../controller/get_posaccountid.php",
            {
                acctname: acctname
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
</script>

<div align="center" style="margin-left: 10px; margin-top: 5px;">
    <table>
        <tr>
            <td><b>POS Account Name:</b></td>
            <td><?php echo $ddlacctname; ?></td>
        </tr>
        <tr>
            <td><b>POS Account Number:</b></td>
            <td><?php echo $ddlacctno; ?></td>
        </tr>
        <tr>
            <td><b>Terminal Account:</b></td>
            <td><?php echo $ddltacct; ?></td>
        </tr>
        <tr><td colspan="3" align="center"><?php echo $btnSubmit; ?><?php echo $hidfname; ?><?php echo $hidtname; ?></td></tr>
    </table>
    <br/>

    <div id="callbacktable">
        <br/><hr>
        <table>
            <tr style="background-color:#FF9C42; height:30px;">
                <th style="width:400px; text-align: left;">&nbsp;&nbsp;&nbsp;POS Account Name</th>
                <td style="width:400px;">&nbsp;&nbsp;&nbsp;<?php echo $acctname; ?></td>
            </tr>
            <tr style="background-color:#FFF1E6; height:30px;">
                <th style="width:400px; text-align: left;">&nbsp;&nbsp;&nbsp;Account Number</th>
                <td style="width:400px;">&nbsp;&nbsp;&nbsp;<?php echo $acctid; ?></td>
            </tr>
            <tr style="background-color:#FFF1E6; height:30px;">
                <th style="width:400px; text-align: left;">&nbsp;&nbsp;&nbsp;Terminal Account</th>
                <td style="width:400px;">&nbsp;&nbsp;&nbsp;<?php echo $terminal_name; ?></td>
            </tr>
        </table>
    </div>
    <br /><br />
    <div align="center"><?php echo $btnResetPass; ?></div>
</div>

<script type="text/javascript">
$(document).ready(function(){
   $('#btnresetpass').hide();
   $('#callbacktable').hide();
    
   $('#btnSubmit').live('click',function(){
      if($('#ddlacctname').val() == 'Select One' ) {
        var html;
        html = '<div class=\"titleLightbox\">Notification</div>';
        html += '<div class=\"msgLightbox\">';
        html += '<p>Please enter an account name and number.</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" />';
        html += '</div>';
        showLightBox(html);
        return false;
      }
      var text = $('#ddlacctname option:selected').text();
      $('#fname').val(text);
      var terminal = $('#tacct option:selected').text();
      $('#tname').val(terminal);
      return true;
   });
   
   <?php if (isset($acctname)): ?>
        $('#tacct').removeAttr("disabled");
        $('#btnresetpass').show();
        $('#callbacktable').show();
   <?php endif; ?>

   $('#btnresetpass').click(function(){
      var text = $('#ddlacctname option:selected').text();
      $('#fname').val(text);  

      if($('#tacct').val() == 'Select One' || $('#tacct').val() == null || $('#tacct').is(':disabled')) {
         html = '<div class=\"titleLightbox\">Notification</div>';
         html += '<div class=\"msgLightbox\">';
         html += '<p>Please select terminal account</p>';
         html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" />';
         html += '</div>';
      } else {
         html = '<div class=\"titleLightbox\">Notification</div>';
         html += '<div class=\"msgLightbox\">';
         html += '<p>You are about to reset password for ' + $('#tacct option:selected').text() +'. Do you wish to continue?</p>';
         html += '<br /><input id=\"btnOkayChangePassword\" name=\"btnOkayChangePassword\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" />&nbsp;&nbsp;<input id=\"btnCancel\" type=\"button\" value=\"CANCEL\" class=\"labelbutton2\" />';
         html += '</div>';      
      }
      showLightBox(html);
      return false;
   });

   $('#btnCancel').live('click',function(){
      $('#light').fadeOut('slow',function(){
         $('#light').remove();
      });
      $('#fade').fadeOut('slow');
      return false;
   });

   $('#ddlacctname').bind('change',function(){
      var val = $(this).val(); 
      if($(this).val() != 'Select One') {
         getTerminals(val);
      } else {
         $('#tacct').html('<option value="Select One">Select One</option>'); 
      }
   });

   $('#ddlacctno').bind('change',function(){
      var val = $(this).val(); 
      if($(this).val() != 'Select One') {
         getTerminals(val);
      } else {
         $('#tacct').html('<option value="Select One">Select One</option>'); 
      }
   });

    function getTerminals(val) 
    {
        var html = '<h1>Loading...</h1>';
        showLightBox(html);

        var data = $('#frmTemplate').serialize();
        $.ajax({
        type: 'post',
        url : '../controller/get_terminals.php',
        data : data,
        dataType: 'json',
        success : function(data){
            $('#light').fadeOut('slow',function(){
                $('#light').remove();
            });
            $('#fade').fadeOut('slow');

            var opt='';
            $('#tacct').removeAttr('disabled');
            jQuery.each(data,function(k,v){
                opt+='<option value="' + k +'">' + v + '</option>';
            });
            $('#tacct').html(opt);
        },
        error : function(){
            $('#tacct').html('<option value="">no terminal account</option>');
            $('#light').fadeOut('slow',function(){
                $('#light').remove();
            });
            $('#fade').fadeOut('slow');

            var html;
            html = '<div class=\"titleLightbox\">Notification</div>';
            html += '<div class=\"msgLightbox\">';
            html += '<p>Oops! Something went wrong</p>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" />';
            html += '</div>';
            showLightBox(html);
        }
        });
    }

   $('#btnOkayChangePassword').live('click',function(){
      $('#light').remove();
      showLightBox('<h1>Loading...</h1>');
      var url = '../controller/updatepassword.php';
      var data = $('#frmTemplate').serialize();
      $.ajax({
         url : url,
         data : data,
         type : 'post',
         success : function(data) {
            if(data == 'MG APi Error' ) {
               var html = '<h1>Oops! Something went wrong.1</h1>';
               html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" />';
               $('#light').html(html); 
            } else {
               $('#light').html(data);
            }
         },
         error : function() {
            var html = '<h1>Oops! Something went wrong.2</h1>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" />';
            $('#light').html(html); 
         }
      });
      return false;
   });
});
</script>
<?php include("footer.php"); ?>