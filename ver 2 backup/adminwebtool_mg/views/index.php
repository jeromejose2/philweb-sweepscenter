<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 13, 2012
 * Purpose          :       Redirect to login.php page.
 */

header("Location: login.php");
?>