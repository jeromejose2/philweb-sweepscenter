<?php 
/*
 * Created By       :       Noel Antonio
 * Date Created     :       June 26, 2012
 * Purpose          :       For log-out user and end active session.
 */

include("../init.inc.php");
App::LoadModuleClass("SweepsCenter", "SCC_AdminAccountSessions");
App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
$cadminacctsessions = new SCC_AdminAccountSessions();
$caudittrail = new SCC_AuditTrail();

$sessiondtls = $cadminacctsessions->GetSessionDetails($_SESSION['sid']);
$id = $sessiondtls[0]["ID"];

// end current active session
$cadminacctsessions->StartTransaction();
$scupdateparam["DateEnded"] = "now_usec()";
$scupdateparam["ID"] = $id;
$cadminacctsessions->UpdateByArray($scupdateparam);
if ($cadminacctsessions->HasError)
{
    $cadminacctsessions->RollBackTransaction();
}
else
{
    $cadminacctsessions->CommitTransaction();
    
    // log to audit trail
    $caudittrail->StartTransaction();
    $scauditlogparam["SessionID"] = $_SESSION['sid'];
    $scauditlogparam["AccountID"] = $_SESSION['aid'];
    $scauditlogparam["TransDetails"] = "Logout: " . $_SESSION['uname'];
    $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
    $scauditlogparam["TransDateTime"] = "now_usec()";
    $caudittrail->Insert($scauditlogparam);
    if ($caudittrail->HasError)
    {
        $caudittrail->RollBackTransaction();
    }
    else
    {
        $caudittrail->CommitTransaction();
        session_destroy();
        URL::Redirect('login.php');
    }
}
?>
