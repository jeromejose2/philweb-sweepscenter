
function showLightBox(html)
{
   var e = document.getElementById('frmTemplate');
   var conheight = e.offsetHeight;
   document.getElementById('fade').style.height = conheight;
   $('#frmTemplate').append('<div id="light" class="white_content2"></div>');
   $('.white_content2').html(html);

   $('#fade').show();
   $('.white_content2').show();
   
}

function hideLightBox()
{
   document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';
}

function hideLightBox2()
{
   $('#light').fadeOut('slow',function(){
      $('#light').remove();
   });
   $('#fade').fadeOut('slow');
}

$('#btnOkay').live('click',function(){
   $('#light').fadeOut('slow',function(){
      $('#light').remove();
   });
   $('#fade').fadeOut('slow');
   return false;
});

$(document).ready(function(){


   $('#bday').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
      yearRange: '-90Y:-10Y',
   });

   $('#acctname').live('change',function(){
      $('#acctnum').val($(this).val());
   })

   $('#acctnum').live('change',function(){
      $('#acctname').val($(this).val());
   });

});

function trim(str) {
   return jQuery.trim(str);
}
