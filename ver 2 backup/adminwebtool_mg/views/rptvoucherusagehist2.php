<?php
/*
 * Date June 25-2012
 * Purpose : View Voucher Usage History
 */
include("../controller/rptvoucherusagehist2process.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script language="javascript" src="jscripts/datepicker.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script language="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.form[0].submit();
    }
</script>
<?php include('header.php') ?>
<form name="frmrptvoucherusagehist2" method="POST">
    <div style="width:100%; text-align:center;">
  <table width="100%">
    <tr>
      <td class="labelbold2">From:</td>
      <td><?php echo $txtDateFr; ?>
        <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
      </td>
      <td class="labelbold2">To:</td>
      <td><?php echo $txtDateTo; ?>
        <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
      </td>
      <td width="100px"><?php echo $btnSearch; ?>
      </td>
    </tr>
    
    <tr>
      <td colspan="5" class="labelbold2">POS Account Name:&nbsp;&nbsp;
          <?php echo $ddlPos; ?>
          </td>
      </tr>
      <tr>
      <td width="100px" colspan="5" class="labelbold2">
            Voucher Code:
            <?php echo $vchrcode;?>
            Voucher Usage:
            <?php echo $vchrusage;?>

      </td>
    </tr>
    <?php if(isset($_SESSION['start'])):?>
    <tr>
      <td colspan="5">
<?php    
            echo "<br />";
         echo "<table style=\"overflow-x:scroll; width:100%;\" ><tr><th colspan =\"5\" style=\"height:30px;background-color: #FF9C42; color:#000000;\">VOUCHER USAGE HISTORY</th></tr>";
         echo "<tr><th class=\"th\" style=\"width:25%;\">Date & Time Generated</th><th class=\"th\" style=\"width:20%;\">POS Account Name</th><th class=\"th\" style=\"width:20%;\">Voucher Code</th><th class=\"th\" style=\"width:20%;\">Voucher Points</th><th class=\"th\" style=\"width:35%;\">Usage Details</th></tr>";
         
     if(count($getvoucher) > 0)
     {
        echo  "<br/>";
        echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".count($getvoucher1)." of ".count($getvoucher1)."</b></p>";
      
         for($x=0;$x<count($getvoucher);$x++)
         {
             
             if($sVchrCode != "")
                     {
                         $dategenerated = $getvoucher[$x]['DateGenerated'];
                         $dateTime = new DateTime($dategenerated);
                         $dategenerated = $dateTime->format("m/d/Y h:i:s A");
                         $voucherno = $getvoucher[$x]['VoucherNo'];
                         $description = $getvoucher[$x]['Description'];
                         $winnings = $getvoucher[$x]['Winnings'];
                         $voucherstatus = $getvoucher[$x]['VoucherStatus'];
                         $dateused = $getvoucher[$x]['DateUsed'];
                         $dateTime = new DateTime($dateused);
                         $dateused = $dateTime->format("m/d/Y h:i:s A");
                         $name = $getvoucher[$x]['Name'];
                         $posaccountname = "".$getvoucher[$x]['LastName']."".$getvoucher[$x]['FirstName']."".$getvoucher[$x]['MiddleName']."";
                       }elseif($sVchrUsage == "5")
                       {
                                    $dategenerated = $getvoucher[$x]['DateGenerated'];
                                    $dateTime = new DateTime($dategenerated);
                                    $dategenerated = $dateTime->format("m/d/Y h:i:s A");
                                    $voucherno = $getvoucher[$x]['VoucherNo'];
                                    $description = $getvoucher[$x]['Description'];
                                    $winnings = $getvoucher[$x]['Winnings'];
                                    $dateused = $getvoucher[$x]['DateUsed'];
                                    $dateTime = new DateTime($dateused);
                                    $dateused = $dateTime->format("m/d/Y h:i:s A");
                                    $name = $getvoucher[$x]['Name'];
                                    $voucherstatus = "Cancelled";
                       }elseif ($sVchrUsage == "4") 
                           {
                                    $dategenerated = $getvoucher[$x]['DateGenerated'];
                                    $dateTime = new DateTime($dategenerated);
                                    $dategenerated = $dateTime->format("m/d/Y h:i:s A");
                                    $voucherno = $getvoucher[$x]['VoucherNo'];
                                    $description = $getvoucher[$x]['Description'];
                                    $winnings = $getvoucher[$x]['Winnings'];
                                    $dateused = $getvoucher[$x]['DateUsed'];
                                    $dateTime = new DateTime($dateused);
                                    $dateused = $dateTime->format("m/d/Y h:i:s A");
                                    $name = $getvoucher[$x]['Name'];
                                    $voucherstatus = "Unused";
                            }elseif ($sVchrUsage == "3") {
                                    $dategenerated = $getvoucher[$x]['DateGenerated'];
                                    $dateTime = new DateTime($dategenerated);
                                    $dategenerated = $dateTime->format("m/d/Y h:i:s A");
                                    $voucherno = $getvoucher[$x]['VoucherNo'];
                                    $description = $getvoucher[$x]['Description'];
                                    $winnings = $getvoucher[$x]['Winnings'];
                                    $dateused = $getvoucher[$x]['DateUsed'];
                                    $dateTime = new DateTime($dateused);
                                    $dateused = $dateTime->format("m/d/Y h:i:s A");
                                    $name = $getvoucher[$x]['Name'];
                                    $voucherstatus = "Expired";
                            }elseif($sVchrUsage == "2")
                            {
                                $dategenerated = $getvoucher[$x]['DateGenerated'];
                                $dateTime = new DateTime($dategenerated);
                                $dategenerated = $dateTime->format("m/d/Y h:i:s A");
                                $voucherno = $getvoucher[$x]['VoucherNo'];
                                $description = $getvoucher[$x]['Description'];
                                $winnings = $getvoucher[$x]['Winnings'];
                                $voucherstatus = "Used";
                                $dateused = $getvoucher[$x]['DateUsed'];
                                $dateTime = new DateTime($dateused);
                                $dateused = $dateTime->format("m/d/Y h:i:s A");
                                $name = $getvoucher[$x]['Name'];
                                $posaccountname = "".$getvoucher[$x]['LastName']."".$getvoucher[$x]['FirstName']."".$getvoucher[$x]['MiddleName']." ";
                            }elseif ($sVchrUsage == "0") 
                                {
                                $dategenerated = $getvoucher[$x]['DateGenerated'];
                                $dateTime = new DateTime($dategenerated);
                                $dategenerated = $dateTime->format("m/d/Y h:i:s A");
                                $voucherno = $getvoucher[$x]['VoucherNo'];
                                $description = $getvoucher[$x]['Description'];
                                $winnings = $getvoucher[$x]['Winnings'];
                                $dateused = $getvoucher[$x]['DateUsed'];
                                $dateTime = new DateTime($dateused);
                                $dateused = $dateTime->format("m/d/Y h:i:s A");
                                $name = $getvoucher[$x]['Name'];
                              
                                $posaccountname = "".$getvoucher[$x]['LastName']."".$getvoucher[$x]['FirstName']."".$getvoucher[$x]['MiddleName']." ";
                                $voucherstatus = $getvoucher[$x]['VoucherStatus'];
                                
                                }
                                
                                if($voucherstatus == "4")
                                {$voucherstatus = "Unused";
                                }elseif ($voucherstatus == "3") 
                                    {$voucherstatus = "Expired";
                                    }elseif($voucherstatus == "2")
                                    {$voucherstatus = "Used";
                                    }elseif($voucherstatus == "5")
                                    {$voucherstatus = "Cancelled";
                                    }else
                                    {$voucherstatus = "Status Unknown";
                                    }
                                ($posaccountname == null) ? " " : $posaccountname;
                                ($name == null) ? " " : $name;
                                ($dateused == null) ? " " : $dateused;
             
             $mod = $x % 2;
              if ($mod == 0)
                  {
                    echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"td\">".$dategenerated."</td><td class=\"td\">".$posaccountname."</td><td class=\"td\">".$voucherno."</td><td class=\"td\">".$description;
                    echo "</td><td class=\"td\"><table cellspacing=\"2px;\" cellpadding=\"2px;\"><tr><td><b>Status:</b></td><td>".$voucherstatus."</td></tr>";
                    echo "<tr><td><b>Used On:</b></td><td>".$dateused."</td></tr><tr><td><b>Terminal:</b></td><td>".$name."</td></tr></table></td></tr>";
                  }
                  else
                  {
                    echo "<tr style=\"height:30px;\"><td class=\"td\">".$dategenerated."</td><td class=\"td\">".$posaccountname."</td><td class=\"td\">".$voucherno."</td><td class=\"td\">".$description;
                    echo "</td><td class=\"td\"><table><tr><td><b>Status:</b></td><td>".$voucherstatus."</td></tr>";
                    echo "<tr><td><b>Used On:</b></td><td>".$dateused."</td></tr><tr><td><b>Terminal:</b></td><td>".$name."</td></tr></table></td></tr>";
                  }
         }
         
            echo "<tr><td colspan=\"4\" align=\"center\">";
            
            echo "<div>";
            echo  "<br/>"; 
            echo "<b class=\"paging\">$pgHist </b>";
            echo "</div>";
            
            echo "</td></tr><tr><td colspan=\"4\" align=\"center\"></td></tr><tr><td colspan=\"4\" align=\"center\">";
            echo "<b><a href='export_report.php?fn=Voucher_Usage_History' class=\"labelbutton2\">DOWNLOAD</a></b></td></tr>";
            echo "</table>";
     }else
            {
              echo "<tr><td colspan=\"4\" align=\"center\"><b>No Records Found.</b></td></tr>";
            }
         ?>
          
          
          
          
          
          
          
          
          
        <?php
         unset($_SESSION['report_header']);
	 unset($_SESSION['report_values']);
    
         $_SESSION['report_header']=array("\"Date & Time Generated\"","\"POS Account Name\"","\"Voucher Code\"","\"Voucher Points\"","\"Status\"","\"Used On\"","\"Terminal\"");
            for($counter=0;$counter<  count($getvoucher1);$counter++)
                        {
                
                         $dategenerated = $getvoucher1[$counter]['DateGenerated'];
                         $dateTime = new DateTime($dategenerated);
                         $dategenerated = $dateTime->format("m/d/Y h:i:s A");
                         $voucherno = $getvoucher1[$counter]['VoucherNo'];
                         $description = $getvoucher1[$counter]['Description'];
                         $winnings = $getvoucher1[$counter]['Winnings'];
                         $voucherstatus = $getvoucher1[$counter]['VoucherStatus'];
                         $dateused = $getvoucher1[$counter]['DateUsed'];
                         $dateTime = new DateTime($dateused);
                         $dateused = $dateTime->format("m/d/Y h:i:s A");
                         $name = $getvoucher1[$counter]['Name'];
                         $posaccountname = "".$getvoucher1[$counter]['LastName']."".$getvoucher1[$counter]['FirstName']."".$getvoucher1[$counter]['MiddleName']."";
                              if($voucherstatus == "4")
                                {$voucherstatus = "Unused";
                                }elseif ($voucherstatus == "3") 
                                    {$voucherstatus = "Expired";
                                    }elseif($voucherstatus == "2")
                                    {$voucherstatus = "Used";
                                    }elseif($voucherstatus == "5")
                                    {$voucherstatus = "Cancelled";
                                    }else
                                    {$voucherstatus = "Status Unknown";
                                    }
                                ($posaccountname == null) ? " " : $posaccountname;
                                ($name == null) ? " " : $name;
                                ($dateused == null) ? " " : $dateused;
                                
                                $_SESSION['report_values'][$counter][0] = $dategenerated;
                                $_SESSION['report_values'][$counter][1] = $posaccountname;
                                $_SESSION['report_values'][$counter][2] = $voucherno;
                                $_SESSION['report_values'][$counter][3] = $description;
                                $_SESSION['report_values'][$counter][4] = $voucherstatus;
                                $_SESSION['report_values'][$counter][5] = $dateused;
                                $_SESSION['report_values'][$counter][6] = $name;
                        }
        ?>
      </td>
    </tr>
    <?php else:?>
    <?php endif;?>
    <tr>
      <td colspan="5">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="5" align="center">
        <!--<input id="btnDownload" name="btnDownload"  type="button" value="Download" class="labelbutton2" onclick="javascript: return alert('download here');"></input>-->
      </td>
    </tr>
  </table>
	<!-- ERROR MESSAGE -->
	<div id="light24" style="text-align: center;font-size: 16pt;height: 300px;width:500px;" class="white_content">
		<div id="title" style="width: p00px;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">
		
		</div>
		<br />
		<br />
		<div id="msg"></div>
		<br/>
		<br/>
		<br />	
		<input id="btnOk" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light24').style.display='none';document.getElementById('fade').style.display='none';" />
	</div>
	<!-- END OF NOTIFICATION MESSAGE -->	
 </div>

    
</form>
<?php include('footer.php')?>
