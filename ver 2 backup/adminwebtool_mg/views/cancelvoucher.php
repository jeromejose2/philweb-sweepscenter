<?php

/*
 * Added by : Jerome F. Jose
 * Created On : June 29 2012
 * Purpose : 
 */

include('../controller/cancelvoucherprocess.php');
?>

<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script language="javascript" src="jscripts/datepicker.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery-1.5.2.min.js"></script>


<script language="javascript" type="text/javascript">
function isAlphaNumericKey(evt)
{
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if((charCode > 31 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128) || (charCode > 127))
        return false;
	return true;
}
function void_voucher()
{
    document.getElementById("btnvoid").style.visibility = "hidden";
    document.getElementById("remarks").style.visibility = "hidden";
    document.getElementById("rmklbl").style.visibility = "hidden";
    var rmrks = document.getElementById('remarks').value;
    var vchrcde = document.getElementById('vouchercde').value;
	
    $.ajax({
         url: 'vouchercancelajax.php?rmrks='+rmrks+'&vchrcde='+vchrcde,
         type : 'post',
         success : function(data)
         {
		    $('#rtnmsg').html(data)
         },
         error: function(e)
         {
            alert("Error");
         }
      });
}
  function checkinputvoucherCC()
  {
      if (document.getElementById("rmklbl").value.length == 0)
     {
        var msg = "Please input remarks.";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        return false;
     }
    
     //void_voucher();
     return true;
         
  }


  function checkinputvoucherCD()
  {
    if(document.getElementById("ddlChangeStatus").value == 1)
        {
                if (document.getElementById("vouchercde").value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
                {
                    var msg = "Please provide a valid voucher code.";
                    var msgtitle = "INVALID INPUT";
                    html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                    html += '<div class=\"msgLightbox\">';
                    html += '<p>' + msg + '</p>';
                    html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                    html += '</div>';
                    showLightBox(html);
                    return false;
                    document.getElementById("ddlvoucherbatch").value = '';
                }
        }
        else  if(document.getElementById("ddlChangeStatus").value == 2)
              {
                    if (document.getElementById("ddlvoucherbatch").value == 0)
                    {
                        var msg = "Please select voucher batch.";
                        var msgtitle = "ERROR";
                        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                        html += '<div class=\"msgLightbox\">';
                        html += '<p>' + msg + '</p>';
                        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                        html += '</div>';
                        showLightBox(html);
                        return false;
                        document.getElementById("vouchercde").value = '';
                    }
              }
              else
              {
                    var msg = "Please select mode of cancellation.";
                    var msgtitle = "ERROR";
                    html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                    html += '<div class=\"msgLightbox\">';
                    html += '<p>' + msg + '</p>';
                    html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
                    html += '</div>';
                    showLightBox(html);
                    return false;
              }
     return true;
  }
 function update_selection()
  {
      if(document.getElementById("ddlChangeStatus").value == 1)
          {
                jQuery('#batcherdiv').hide();
                jQuery('#voucherdiv').show();
          }
          else  if(document.getElementById("ddlChangeStatus").value == 2)
              {
                jQuery('#batcherdiv').show();
                jQuery('#voucherdiv').hide();
              }
                else if(document.getElementById("ddlChangeStatus").value == 0)
              {
                jQuery('#batcherdiv').hide();
                jQuery('#voucherdiv').hide();
              }
  }
  
jQuery(document).ready(function(){

        jQuery('#batcherdiv').hide();
        jQuery('#voucherdiv').hide();
        jQuery('#divremarks').hide();
        if(document.getElementById('ddlChangeStatus').value == 1)
            {
                             jQuery('#voucherdiv').show();
            }
            else  if(document.getElementById("ddlChangeStatus").value == 2)
              {
                   jQuery('#batcherdiv').show();
              }
}); 
        
        

function Check(chk)
{
	if(chk == undefined)
	{
//		alert("ok");
	}
	else
	{
		if(document.forms["frmTemplate"].maincheckbox.checked == true)
		{
			document.getElementById('divremarks').style.display = "block";
                            if(chk.length > 0)
                            {
                                for (i = 0; i < chk.length; i++)
                                    chk[i].checked = true ;
                            }
                            else
                            {
                                chk.checked = true ;
                            }
		    /*document.getElementById('btnchngestatus').style.display = "block";
		    for (i = 0; i < chk.length; i++)
		    chk[i].checked = true ;*/
		}
                else
		{
                    document.getElementById('divremarks').style.display = "none";
                            if(chk.length > 0)
                            {
                                for (i = 0; i < chk.length; i++)
                                    chk[i].checked = false ;
                            }
                            else
                            {
                                chk.checked = false ;
                            }
		}
	}
}
function enablebutton(chk)
{
    document.forms["frmTemplate"].maincheckbox.checked = false;
    var chkBoxCount = chk.length;
    var totalChecked = 0;

    if(chkBoxCount > 0)
    {
        for (var i = 0; i < chkBoxCount; i++)
        {
            //check the state of each CheckBox
            //replace "YourFormName" with the name of your form
            if (eval("chk[" + i + "].checked") == true)
            {
                //it's checked so increment the counter
                totalChecked += 1;
            }
        }
    }
    else
    {
        if (eval("chk.checked") == true)
        {
            //it's checked so increment the counter
            totalChecked += 1;
	document.forms["frmTemplate"].maincheckbox.checked = true;
        }
    }
		if(totalChecked == chkBoxCount)
		{
		    document.forms["frmTemplate"].maincheckbox.checked = true;
		}
                if(totalChecked > 0)
		   document.getElementById('divremarks').style.display = "block";
		else
		   document.getElementById('divremarks').style.display = "none";
                
	
}

 $('#btnCancel').live('click',function(){
      $('#light').fadeOut('slow',function(){
         $('#light').remove();
      });
      $('#fade').fadeOut('slow');
      document.getElementById('rmklbl').value = '';
      return false;
   });
</script>

<?php include('header.php') ?>

<form id="frmTemplate" name="frmTemplate" method="POST" >  
<div style="width:100%; text-align:center;">
  <table>
      <tr>
                <td width="40%" class="labelbold2">
                  Mode of Cancellation:&nbsp;<?php echo $ddlChangeStatus;?>
                </td>
                <td class="labelbold2">
                        <div id="voucherdiv">
                            Voucher Code:
                                <?php echo $txtVchrCode;?>
                        </div>
                </td>
                <td class="labelbold2">
                        <div id="batcherdiv">
                            Voucher Batches:
                            <?php echo $ddlvoucherbatch;?>
                        </div>
                </td> 
            <td width="5%">
                <?php echo $btnSearch; ?>
            </td>
    </tr>
      </table>
  
    
    <table> 
  
    <?php if($viewtable != NULL ):?>
    <tr>
      <td colspan="5">
        <?php
        echo "<table style=\"overflow-x:scroll;\" ><tr><th colspan =\"5\" style=\"height:30px;background-color: #FF9C42; color:#000000;\">CANCEL VOUCHER</th></tr>";
        echo "<tr><th class=\"th\">
<input type='checkbox' id='maincheckbox' name='maincheckbox' value='yes' onClick='Check(document.frmTemplate.checkbox)' /></th> ";
        echo "<th class=\"th\">Voucher Code</th>";echo " <th class=\"th\">Value</th><th class=\"th\">Status</th><th class=\"th\">Entry Type</th></tr>";
        if(count($getvoucher) > 0)
        {
            for($x=0;$x<count($getvoucher);$x++)
            {
                        $vchr_no = $getvoucher[$x]['VoucherNo'];
                        $value = $getvoucher[$x]['Description'];
                        $status = $getvoucher[$x]['Status'];
                        $entry_type = $getvoucher[$x]['EntryType'];

                        if  ($status == '1')
                                $status = "Unused";
                        else if($status == '2')
                                $status = "Used";
                        else if($status == '3')
                                $status = "Expired";
                        else if($status == '4')
                                $status = "Assigned";
                        else if($status == '5')       
                                $status = "Void";

                        if($entry_type == '1')
                            $entry_type = "Free Entry";
                        else if($entry_type == '2')
                            $entry_type = "Online Free Entry";
                        else if($entry_type == '3')
                            $entry_type = "Regular Gaming";
                    echo "<tr style=\"background-color:#FFF1E6; height:30px;\">";
     if($status == 'Unused' || $status == 'Assigned')
                {  echo "   <td align='center'>
<input type='checkbox' id='checkbox' name='checkbox[]' value='".$getvoucher[$x]['ID']."' onclick='javascript: return enablebutton(document.frmTemplate.checkbox);'/></td>  
                        ";
                }
                else
                {
                    echo "<td></td>";
                }
                    echo "<td  class=\"td\">".$vchr_no."</td>
                            <td class=\"td\">".$value."</td>
                            <td class=\"td\">".$status."</td>
                            <td class=\"td\">".$entry_type."</td>
                        </tr>";

                    }
                 
            echo "</td></tr><tr><td colspan=\"4\" align=\"center\"></td></tr><tr><td colspan=\"4\" align=\"center\">";
            echo "</table>";
                        echo "<div id='divremarks'> ";
                        echo $rtnmsg;
                        echo "<table>";echo "<br/>";echo "<tr><td>";
                        echo "<div id='rmklbl1'> ";echo "<b>Remarks: </b>";echo "</div>";echo "</td>";echo "<br/>";
                        echo "<td>";echo $remarks1;echo "</td>";echo "<br/>";
                        echo "<td>";echo "&nbsp;&nbsp;&nbsp;";echo $btnvoid;echo "</td>";
                        echo "</table>";
                        echo "</div>";
        }else
        {
            echo "<tr><td colspan=\"5\" align=\"center\"><b>No Record(s) Found.</b></td></tr>";
        }
          
        
        
        
        ?>
      </td>
    </tr>
    <?php else:?>
    <?php endif;?>
    <tr>
      <td colspan="5">&nbsp;<label id="rtnmsg" name="rtnmsg" style="font-size: 20px; color: red; font-weight: bold;"></label></td>
    </tr>
    <tr>
      <td colspan="5" align="center">
        <!--<input id="btnDownload" name="btnDownload"  type="button" value="Download" class="labelbutton2" onclick="javascript: return alert('download here');"></input>-->
      </td>
    </tr>
  </table> </div>
    <?php if(isset($confirm_msg)):?>
    <script>
      var msg = "<?php echo $btnyes;?>";
                       var msgtitle = "CONFIRMATION";
                    html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                    html += '<div class=\"msgLightbox\">';
                    html += '<p>Are you sure you want to cancel the selected  voucher(s)?</p>';
                    html += '<br />' + msg +'&nbsp;<input id=\"btnCancel\" type=\"button\" value=\"NO\" class=\"labelbutton2\" />';
                    html += '</div>';
                    showLightBox(html);
                    document.getElementById('divremarks').style.display = "none";
</script>
   <?php   endif;?>
    <?php if(isset($return_msg)):?>
<script>
   
     var msg = "<?php echo $linkbtn;?>";
     var msgtitle = "SUCCESSFUL";
                    html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
                    html += '<div class=\"msgLightbox\">';
                    html += '<p>Voucher(s) successfully cancelled.</p>';
                    html += '<br />' +msg+' <br />';
                    html += '</div>';
                    showLightBox(html);
                    document.getElementById('divremarks').style.display = "none";
</script>

<?php endif;?>
	<!-- ERROR MESSAGE -->
	<div id="light24" style="text-align: center;font-size: 16pt;height: 300px;width:500px;" class="white_content">
		<div id="title" style="width: p00px;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">

		</div>
		<br />
		<br />
		<div id="msg"></div>
		<br/>
		<br/>
		<br />
		<input id="btnOk" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light24').style.display='none';document.getElementById('fade').style.display='none';" />
	</div>
        <div id="light25" style="text-align: center;font-size: 16pt;height: 300px;width:500px;" class="white_content">
		<div id="title1" style="width: p00px;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">

		</div>
		<br />
		<br />
		<div id="msg1"></div>
		<br/>
		<br/>
		<br />
		<input id="btnOk" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light25').style.display='none';document.getElementById('fade').style.display='none';" />
	</div>
	<!-- END OF NOTIFICATION MESSAGE -->
        </form>

           
<?php include('footer.php')?>