<?php

/*
 * Added by : Jerome F. Jose
 * Created On : June 29 2012
 * Purpose : 
 */
include('../controller/terminallogoutprocess.php');
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script language="javascript" src="jscripts/datepicker.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
<?php include('header.php')?>
<form name="frmterminallogout" method="POST">
    <script language="javascript">
function force_logout()
{
    document.getElementById("btnlogout").style.visibility = "hidden";
    var ternme = document.getElementById('ternme').value;
    var ddlPos = document.getElementById('ddlPos').value;

    $.ajax({
         url: 'logoutterminalajax.php?ternme='+ternme+'&ddlPos='+ddlPos,
         type : 'post',
         success : function(data)
         {
            $('#rtnmsg').html(data)
         },
         error: function(e)
         {
            alert("Error");
         }
      });
}

function isAlphaNumericKey(evt)
{
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if((charCode > 31 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128) || (charCode > 127))
        return false;

	return true;
}
function checking()
{
     if (document.getElementById("ternme").value == '' || document.getElementById("ddlPos").value == 0)
     {
	 var msg = "All fields are required.";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        return false;
	
     }
     
}
</script>
    <div style="width:100%; text-align:center;">
  <table>

      <tr>
      <td colspan="5" class="labelbold2">POS Account Name:&nbsp;&nbsp;
        <?php echo $ddlPos; ?>
      </td>
      </tr>

      <tr>
      <td class="labelbold2">
	Terminal Name:
        <?php echo $txtTerminalName;?>
      </td>
      <td width="100px">
        <?php echo $btnSearch;?>
      </td>
    </tr>
    <?php if(isset($_SESSION['ternme']) && isset($_SESSION['ddlPos'])):?>
    <tr>
      <td colspan="5">
        <?php
        //include_once('logoutterminal.php');
        
echo "<table style=\"overflow-x:scroll;\" ><tr><th colspan =\"4\" style=\"height:30px;background-color: #FF9C42; color:#000000;\">LOGOUT TERMINAL</th></tr>";
echo "<tr><th class=\"th\">Terminal Name</th><th class=\"th\">POS Account Name</th><th class=\"th\">IsLoggedIn</th><th class=\"th\">Terminal Type</th></tr>";

if(count($getlogout) > 0)
{
    

for($x=0;$x<count($getlogout);$x++)
        {
            
                $name = $getlogout[$x]['Name'];
                $pos_name = $getlogout[$x]['PosName'];
                $isplayable = $getlogout[$x]['IsPlayable'];
                $isfreeentry = $getlogout[$x]['IsFreeEntry'];
               
                if($isplayable == '0')
                    {
                        $isplayable = "Yes";
                    }
                    else if($isplayable == '1')
                    {
                        $isplayable = "No";
                    }


                    if($isfreeentry == '0')
                    {
                        $isfreeentry = "Regular Gaming";
                    }
                    else if($isfreeentry == '1')
                    {
                        $isfreeentry = "Free Entry";
                    }
    
            echo "<tr style=\"background-color:#FFF1E6; height:30px;\">
                    <td class=\"td\">".$name."</td>
                    <td class=\"td\">".$pos_name."</td>
                    <td class=\"td\">".$isplayable."</td>
                    <td class=\"td\">".$isfreeentry."</td>
                </tr>";
            
            }
            echo "</td></tr><tr><td colspan=\"4\" align=\"center\"></td></tr><tr><td colspan=\"4\" align=\"center\">";
            echo "</table>";
            
}  else {
    if(!isset($returnmsg2))
    {
     echo "<tr><td colspan=\"4\" align=\"center\"><b>No Record(s) Found.</b></td></tr>";
        }
}   
        
if (count($getlogout) == 1)
{
    echo "<label id=\"rtnmsg\" name=\"rtnmsg\" style=\"font-size: 20px; color: red; font-weight: bold;\"></label>";
    echo "<br/>";
        echo $btnlogout;
        
}

        ?>
      </td>
    </tr>
    <?php else:?>
    <?php endif;?>
    <tr>
      <td colspan="5">&nbsp;<label id="rtnmsg" name="rtnmsg" style="font-size: 20px; color: red; font-weight: bold;"></label></td>
    </tr>
     <tr>
      <td colspan="5" align="center">
      </td>
    </tr>
  </table>
<?php if(isset($returnmsg2)):?>
<script>
	document.getElementById('rtnmsg').innerHTML = "<?php echo $returnmsg2;?>";
</script>
<?php endif;?>
	<!-- ERROR MESSAGE -->
	<div id="light24" style="text-align: center;font-size: 16pt;height: 300px;width:500px;" class="white_content">
		<div id="title" style="width: p00px;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">

		</div>
		<br />
		<br />
		<div id="msg"></div>
		<br/>
		<br/>
		<br />
		<input id="btnOk" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light24').style.display='none';document.getElementById('fade').style.display='none';" />
	</div>
	<!-- END OF NOTIFICATION MESSAGE -->
 </div>

    
</form>
<?php include('footer.php')?>