<?php

/*
 * Added By : Jerome F. Jose
 * Added On : June 15, 2012
 * Purpose : Adding User Account
 */
include("../controller/adduseracctprocess.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<?php include("header.php")?>
<div id="page">
    <form name ="frmAdduser" method="POST">
        
  <table width="100%">
    <tr>
      <td colspan="2">
        <div id="header">Create User Account</div>
      </td>
    </tr>
     <tr style="background-color:#FFF1E6; height:30px;">
      <td class="fontboldblack" style="width:50%;">&nbsp;&nbsp;Username</td>
      <td style="width:50%;"><?php echo $txtUName;?>
      </td>
    </tr>
    <tr style="background-color:#FFF1E6; height:30px;">
      <td class="fontboldblack">&nbsp;&nbsp;Password</td>
      <td><?php echo $txtPWord;?>
    </tr>
    <tr style="background-color:#FFF1E6; height:30px;">
      <td class="fontboldblack">&nbsp;&nbsp;Confirm Password:</td>
      <td><?php echo $txtCPWord;?>
    </tr>
  </table>
  <br />
  <div id="header2">Account Information</div>
  <div style="text-align:center;">
    <table>
         <tr>
        <td class="fontboldblackcenter">First Name</td>
        <td class="fontboldblackcenter">Middle Name</td>
        <td class="fontboldblackcenter">Last Name</td>
      </tr>
      <tr>
        <tr>
        <td><?php echo $txtFName;?></td>
        <td><?php echo $txtMName;?></td>
        <td><?php echo $txtLName;?></td>
        </tr>
        
       <tr>
        <td class="fontboldblackcenter">E-mail Address</td>
        <td colspan="2"><?php echo $txtEmail;?></td>
      </tr>
      
      <tr>
        <td class="fontboldblackcenter">Position</td>
        <td colspan="2"><?php echo $txtPosition;?></td>
      </tr>
      
      <tr>
        <td class="fontboldblackcenter">Company</td>
        <td colspan="2"><?php echo $txtCompany;?></td>
      </tr>
      
       <tr>
        <td class="fontboldblackcenter">Department</td>
        <td colspan="2"><?php echo $txtDepartment;?></td>
      </tr>
      <tr>
        <td class="fontboldblackcenter">Group</td>
        <td colspan="2">
             <?php echo $ddlGroup;?>&nbsp;&nbsp;&nbsp;
             <?php echo $hidGroup;?></td>
      </tr>
      
      <tr>
        <td class="fontboldred"  colspan="3">All fields are required.</td>
      </tr>
      <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
      
       <tr>
        <td colspan="3" align="center">
            <?php echo $btnCancel ?>&nbsp;&nbsp;&nbsp; <?php echo $btnSubmit;?>

        </td>
      </tr>
    </table>
      
  </div>
  <br/>
  <!-- ERROR MESSAGE -->
	<div id="light14" style="text-align: center;font-size: 16pt;height: 230px; margin-left:-95px;" class="white_content">
		<div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
		</div>
		<br />
		<br />
		<div id="msg">
		</div>
		<br/>
		<br/>
		<br />
        <input id="btnOkClose" type="button" value="OKAY" class="labelbutton2" onclick="document.getElementById('light14').style.display='none';document.getElementById('fade').style.display='none';"/>
	</div>
	<!-- END OF ERROR MESSAGE -->
	<!-- ERROR MESSAGE -->
	<div id="light22" style="text-align: center;font-size: 16pt;height: 290px; margin-left:-95px;" class="white_content">
		<div id="title2" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
   			
		</div>
		<br />
		<br />
		<div id="msg2">
  			
		</div>
		<br/>
		<br/>
		<br />
                <input id="btnOkLightbox"  value="OKAY" class="labelbutton2" type="button"/>
	</div>
	<!-- END OF ERROR MESSAGE -->
      </form>
<script type="text/javascript">
    <?php if(isset($_POST["btnSubmit"])): ?>
       error();  
    <?php endif; ?>

    $('#btnOkClose').live('click',function(){
       $('#light14').fadeOut('slow');
       $('#fade').fadeOut('slow');
    });

    function error()
    {
     	var resid = "<?php echo isset($returnid2) ? $returnid2 : '' ; ?>";
        var resmsg = "<?php echo isset($returnmsg) ? $returnmsg : ''; ?>";
	
        var e = document.getElementById('frmTemplate');
        var conheight = e.offsetHeight;
        document.getElementById('fade').style.height = conheight;
	document.getElementById('light22').style.display='block';
	document.getElementById('fade').style.display='block';
	document.getElementById('title2').innerHTML = "<?php echo $_SESSION['title'];?>";//title;
	document.getElementById('msg2').innerHTML = resmsg;
    }

      $('#btnOkLightbox').live('click',function(){
       <?php if(isset($_POST["btnSubmit"]) && $returnid2 != 0): ?>
          document.getElementById('light22').style.display='none';
          document.getElementById('fade').style.display='none';
       <?php else: ?>
          <?php unset($_SESSION['sticky']); ?>
         window.location = 'useracctprofile.php';
       <?php endif; ?>
    });
    
  </script>
</div>

  <?php include("footer.php"); ?>

