<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 20, 2012
 * Purpose          :       Reload BCF
 */

include("../controller/reloadbcfprocess.php");
?>
<?php include("header.php"); ?>
<script type="text/javascript">
    function SelectPOSAccountName()
    {
        var acctid = document.getElementById('ddlacctno').options[document.getElementById('ddlacctno').selectedIndex].value;
        $("#ddlacctname").load(
            "../controller/get_posaccountname.php",
            {
                acctid: acctid
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
    
    function SelectPOSAccountNumber()
    {
        var acctname = document.getElementById('ddlacctname').options[document.getElementById('ddlacctname').selectedIndex].value;
        $("#ddlacctno").load(
            "../controller/get_posaccountid.php",
            {
                acctname: acctname
            },
            function(status, xhr)
            {
                if(status == "error")
                {
                    document.getElementById('title1').innerHTML = "ERROR!";
                    document.getElementById('msg1').innerHTML = "Error: " + xhr.status + " " + xhr.statusText;
                    document.getElementById('light1').style.display = "block";
                    document.getElementById('fade').style.display = "block";
                }
            }
        );
    }
</script>
<div id="divselect">
    <div align="center" style="margin-left: 10px; margin-top: 5px;">
        <table>
            <tr>
                <td><b>Account Name:</b></td>
                <td><?php echo $ddlacctname; ?></td>
            </tr>
            <tr>
                <td><b>Account Number:</b></td>
                <td><?php echo $ddlacctno; ?></td>
            </tr>
        </table>
        <br/>
        <div align="center"><?php echo $btnSubmit; ?></div>
    </div>
</div>

<div id="showbcf">
    <div align="center" style="margin-left: 10px; margin-top: 5px;">
        <table>
            <tr style="background-color:#FF9C42; height:30px;">
                <td class="fontboldblack" style="width:400px;">&nbsp;&nbsp;SCF Balance:</td>
                <td style="width:400px;">&nbsp;&nbsp;<b><?php echo $bcf ?></b></td>
            </tr>
            <tr style="background-color:#FFF1E6; height:30px;">
                <td class="fontboldblack" style="width:400px;">&nbsp;&nbsp;Account Name:</td>
                <td style="width:400px;">&nbsp;&nbsp;<?php echo $acct_name ?></td>
            </tr>
            <tr style="background-color:#FFF1E6; height:30px;">
                <td class="fontboldblack" style="width:400px;">&nbsp;&nbsp;Account Number:</td>
                <td style="width:400px;">&nbsp;&nbsp;<?php echo $acct_id ?></td>
            </tr>
        </table>
        <div align="center"><input <?php if($status == 2 || $status == 3) echo 'disabled="disabled" style="cursor: default;"';  ?> id="btnReload" name="btnReload"  type="submit" value="RELOAD SCF" class="labelbutton2" onclick=""/></div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
   $('#showbcf').hide();
   
   $('#btnSubmit').click(function(){
      if($('#ddlacctname option:selected').val() == 'Select One') {
         html = '<div class=\"titleLightbox\">Notification</div>';
         html += '<div class=\"msgLightbox\">';
         html += '<p>Please select an account name and number.</p>';
         html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
         html += '</div>';
         showLightBox(html);
         return false;
      }
      return true;
   });
   
   <?php if (isset($acct_name)): ?>
        $('#divselect').hide();
        $('#showbcf').show();
    <?php endif; ?>
        
    $('#btnReload').click(function(){
        html = '<div class=\"titleLightbox\">Reload SCF</div>';
        html += '<div class=\"msgLightbox\">';
        html += '<p>1500 points will be loaded to <?php echo $acct_name ?>. Do you wish to continue?</p>';
        html += '<br /><br />';
        html += '<input id=\"btnSubmitOkay\" name=\"btnSubmitOkay\" type=\"submit\" value=\"OKAY\" class=\"labelbutton2\" />&nbsp;&nbsp;';
        html += '<input id=\"btnSubmitCancel\" name=\"btnSubmitCancel\" value=\"CANCEL\" class=\"labelbutton2\" />';
        showLightBox(html);
        return false;
    });
    
    $('#btnSubmitCancel').live('click',function(){
        $('#light').fadeOut('slow',function(){
        $('#light').remove();
        });
        $('#fade').fadeOut('slow');
        return false;
    })
});
</script>
<?php include("footer.php"); ?>
