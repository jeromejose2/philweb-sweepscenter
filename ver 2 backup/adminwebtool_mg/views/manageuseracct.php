<?php
/*
 * Created by : Jerome Jose
 * Date Created : June 15-2012
 * Purpose :  Manage/view Admin Accounts
 */

include("../controller/manageuseracctprocess.php");
?>
<script type="text/javascript" src="jscripts/jquery-1.5.2.min.js" ></script>
<script type="text/javascript" src="jscripts/jquery-ui-1.8.13.custom.min.js" ></script>
<script type="text/javascript" src="jscripts/lightbox.js" ></script>
<?php include("header.php")?>
<form id="frmTemplate" method="post">
   <div id="containertemplate">
	<div id="fade" class="black_overlay"></div></div>
    <div align="center" style="margin-left: 10px; margin-top: 5px;">
        <table>
            <tr>
                <td><b>User Name:</b></td><td>
                    <?php echo $uname1;?></td>
            </tr>
        </table>
        <br/>
           <div align="center">
               <?php echo $btnSubmit;?>
           </div>
     </div>
    
    
     <!--         manageuseractt_form        -->
    
<?php if(isset($displayinfo)) : ?>
   


    <div style="margin-left: 10px; margin-top: 5px;">
        <div style="font-weight: bold;">Manage User Account</div>
        <Table style="margin-top: 10px;">
            <tr>
                <td>
                USER NAME:</td><td><b><?php echo $uname ?>
                <input type="hidden" name="fname" value="<?php echo $uname; ?>" /></b>
                </td>
            </tr>
            <tr></tr><tr></tr>
            <tr>
                <td>ACCOUNT NUMBER:</td>
                <td>
                   <b>
                      <?php echo $acctid ?>
                   </b>
                   <input type="hidden" name="hidid" value="<?php echo $acctid ?>" />
               </td>
            </tr>
        </Table>
        <br/>
        <hr>
        <br/>
        <div style="font-weight: bold;"><u>Account Status Information</u></div>
        <Table style="margin-top: 10px;">
            <tr>
                <td>Current Account Status:</td><td><b><?php echo $status2; ?></b></td>
            </tr>
            <tr><td>Remarks:</td><td><b><?php echo $remarks; ?></b></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr>
               <td></td>
            </tr>
          <tr></tr><tr></tr><tr></tr><tr></tr>
        </Table>
       
        <br/>
        <div align="center"><?php echo $btnEdit; ?>
		
        </div>
    </div>
    
<?php endif;?>
    
   
<?php if(isset($displayupdate)) : ?>
   <?php unset($displayinfo);?>
     <div style="margin-left: 10px; margin-top: 5px;">
        <div style="font-weight: bold;">Manage User Account</div>
        <Table style="margin-top: 10px;">
            <tr>
                <td>
                USER NAME:</td><td><b><?php echo $uname ?>
                <input type="hidden" name="fname" value="<?php echo $uname; ?>" /></b>
                <input type="hidden" name="txtEmail" value="<?php echo $email; ?>" /></b>
                </td>
            </tr>
            <tr></tr><tr></tr>
            <tr>
                <td>ACCOUNT NUMBER:</td>
                <td>
                   <b>
                      <?php echo $acctid ?>
                   </b>
                   <input type="hidden" name="hidid" value="<?php echo $acctid ?>" />
               </td>
            </tr>
        </Table>
        <br/>
        <hr>
        <br/>
        <div style="font-weight: bold;"><u>Account Status Information</u></div>
        <Table style="margin-top: 10px;">
            <tr>
               
                <td>Current Account Status:</td><td><b><?php echo $status ?></b></td>
            </tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr>
               <td></td>
            </tr>
            <tr>
                <td>Set Account Status:</td>
                    <td><?php echo $acctstatus; ?>
                       
                    </td>
                
            </tr>
            <tr></tr><tr></tr><tr></tr><tr></tr>
            <tr>
                <td>Remarks:</td><td><textarea id="remarks1" name="remarks1" cols="" rows="" style="width: 400px; height: 100px; border-color: #FF9C42; border-style: solid;" onKeyDown="limitText(this.form.remarks1,this.form.countdown,500);" onKeyUp="limitText(this.form.remarks1,this.form.countdown,500);"><?php echo $remarks; ?></textarea></td>
            </tr>
            <tr>
                <td></td><td>Max. 500 chars <input readonly type="text" name="countdown" size="3" value="500" style=" border-color: #FF9C42;"></td>
            </tr>
        </Table>

        <br/>
      
        <div align="center">
           <?php  echo $btnback ?>
          &nbsp;&nbsp;
         
           <?php echo $btnApply; ?>
        </div>
    </div>
   <?php endif;?>
   
<script type="text/javascript">
$(document).ready(function(){
$('#btnSubmit').live('click',function(){

   if($('#uname option:selected').val() == '0') {
       
      html = '<div class=\"titleLightbox\">Invalid Search<\/div>';
      html += '<div class=\"msgLightbox\">';
      html += '<p>Please select username.</p>';
      html += '<br \/><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"><\/input>';
      html += '</div>';
         
      showLightBox(html);
 
      
      return false;
   }

 
})
 
 
    $('#btnApply').live('click',function(){
         html = '<div class=\"titleLightbox\">Notification<\/div>';
         html += '<div class=\"msgLightbox\">';
      if($('#acctstatus').val() == 'selectone') {
         html += '<p>Please enter new account status</p>';
         html += '<br \/><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"><\/input>';
         html += '</div>';
         showLightBox(html);
         return false;
      }
      if($('#remarks').val() == '') {
         html += '<p>Please enter the reason for changing the account status</p>';
         html += '<br \/><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"><\/input>';
         html += '</div>';
         showLightBox(html);
         return false;
      }

      html = '<div class=\"titleLightbox\">Update Account Status<\/div>';
      html += '<div class=\"msgLightbox\">';
      var newstatus = $('#acctstatus option:selected').text();
      html += '<p>Are you sure you want to change <?php echo $uname ?>\'s status from <?php echo $status ?> to ' + newstatus + '</p>';
      html += '<br /><br />';
      html += '<input id=\"btnSubmitOkay\" name=\"btnSubmit2\" type=\"submit\" value=\"OKAY\" class=\"labelbutton2\" \/>&nbsp;&nbsp;';
      html += '<input id=\"btnSubmitCancel\" name=\"btnSubmit\" type=\"submit\" value=\"CANCEL\" class=\"labelbutton2\" \/>';
      showLightBox(html);
      return false;
   });

   $('#btnSubmitCancel').live('click',function(){
      $('#light').fadeOut('slow',function(){
         $('#light').remove();
      });
      $('#fade').fadeOut('slow');
      return false;
   });
 
   $('#btnSubmitOkay').live('click',function(){
   
        html = '<div class=\"titleLightbox\">Successful<\/div>';
      html += '<div class=\"msgLightbox\">';
      html += '<p>User status successfully updated.</p>';
      html += '<br \/><input id=\"btnOkayLAST\" name=\"btnSubmit3\"  type=\"submit\" value=\"OKAY\" class=\"labelbutton2\"><\/input>';
      html += '</div>';
         
      showLightBox(html);
      $('#frm').submit(); 
      
    
   });
    $('#btnOkayLAST').live('click',function(){
         
         //window.location = 'manageuseracct.php';
   });
 
 
 
 
 
 
 
});

</script>
</form>
<?php include("footer.php");?>