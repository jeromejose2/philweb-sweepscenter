<?php
/*
 * Created by Jerome Jose
 * Date June 22-2012
 * Purpose : 
 */
include("../controller/rptredemptionhistprocess.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script language="javascript" src="jscripts/datepicker.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script language="javascript" type="text/javascript">
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
       
    }
</script>
<?php include('header.php')?>
<form name="frmrptredemptionhist" method="post">
<div style="width:100%; text-align:center;">
  <table width="100%">
    <tr>
      <td class="labelbold2">From:</td>
      <td><?php echo $txtDateFr;?>
        <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
      </td>
        <td class="labelbold2">To:</td>
            <td><?php echo $txtDateTo?>
                <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
            </td>
                    </tr>
            <tr>
            <td colspan="4" class="labelbold2">POS Account Name:&nbsp;&nbsp;
                    <?php echo $ddlPos;?>&nbsp;&nbsp;
                <?php echo $btnSearch;?>
<!--                <input id="btn" name="btn"  type="submit" value="Search" class="labelbutton2" onclick="javascript: return checkinput();"></input> -->
    </td>
    </tr>
    <tr>
      <?php if(isset($_SESSION['start'])):?>
      <td colspan="5">
         <?php   
         echo "<br />";
         echo "<table style=\"overflow-x:scroll;width:100%;\" ><tr><th colspan =\"3\" style=\"height:30px;background-color: #FF9C42; color:#000000;\">REDEMPTION HISTORY</th></tr>";
         echo "<tr><th class=\"th\" style=\"width:25%;\">Date Processed</th><th class=\"th\" style=\"width:25%;\">POS Account Name</th><th class=\"th\" style=\"width:50%;\">Withdrawal Details</th></tr>";

         if(count($getTransSummary) > 0)
         {
         echo  "<br/>";
         echo "<p class=\"paging\"><b>Displaying $pgcon->SelectedItemFrom-".count($getTransSummary1)." of ".count($getTransSummary1)."</b></p>";
      
          for($x=0;$x<count($getTransSummary);$x++)
                        {
                         $transSummaryID = $getTransSummary[$x]['TransSummaryID'];
                         $name = $getTransSummary[$x]['Name'];
                         $winnings = $getTransSummary[$x]['Winnings'];
                         $entrytype = $getTransSummary[$x]['EntryType'];
                         $dataclaimed = $getTransSummary[$x]['DateClaimed'];
                         $posaccountname = $getTransSummary[$x]['POSAccountName'];
                         ($entrytype == 0) ? 'Regular Gaming' : 'Free Entry';
                         $mod = $x % 2;

                        if ($mod == 0)
                        {
                            echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td class=\"td\">".$dataclaimed."</td><td class=\"td\">".$posaccountname;
                            echo "</td><td class=\"td\"><table align=\"left\"><tr><td><b>Transaction Number:</b></td><td>".$transSummaryID."</td></tr>";
                            echo "<tr><td><b>Terminal Name:</b></td><td>".$name."</td></tr><tr><td><b>Entry Type:</b></td><td>".$entrytype."</td></tr><tr><td><b>Winnings:</b></td><td>".$winnings."</td></tr></table></td></tr>";
                        }
                        else
                        {
                            echo "<tr style=\"height:30px;\"><td class=\"td\">".$dataclaimed."</td><td class=\"td\">".$posaccountname;
                            echo "</td><td class=\"td\"><table align=\"left\"><tr><td><b>Transaction Number:</b></td><td>".$transSummaryID."</td></tr>";
                            echo "<tr><td><b>Terminal Name:</b></td><td>".$name."</td></tr><tr><td><b>Entry Type:</b></td><td>".$entrytype."</td></tr><tr><td><b>Winnings:</b></td><td>".$winnings."</td></tr></table></td></tr>";
                        }
                      
                    }
               
                if(($pgcon->SelectedItemFrom-1 + count($getTransSummary)) == count($getTransSummary1))
		{
              
                echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td colspan=\"2\" class=\"labelbold\">&nbsp;Total Cash Redemptions:</td><td class=\"labelbold\">&nbsp;$".number_format($totalCash,2)."</td></tr>";
                echo "<tr style=\"background-color:#FFF1E6; height:30px;\"><td colspan=\"2\" class=\"labelbold\">&nbsp;Total Value of Non-Cash Redemptions:</td><td class=\"labelbold\">&nbsp;$".number_format($totalNonCash,2)."</td></tr>";
                }
               
            echo "<div>";
            echo  "<br/>"; 
            echo "<b class=\"paging\">$pgHist </b>";
            echo "</div>";
            echo "<tr><td>&nbsp;</td></tr>";
            echo "</td></tr><tr><td colspan=\"3\" align=\"center\"></td></tr><tr><td colspan=\"3\" align=\"center\">";
            echo "<b><a href='export_report.php?fn=Redemption_History' class=\"labelbutton2\">DOWNLOAD</a></b></td></tr>";
           
               
         }
         else
            {
              echo "<tr><td colspan=\"3\" align=\"center\"><b>No Records Found.</b></td></tr>";
            }

            echo "</table>";
         ?>
          
        <?php
            unset($_SESSION['report_header']);
	    unset($_SESSION['report_values']);
         
         $_SESSION['report_header']=array("\"Date Processed\"","\"Processed By\"","\"Transaction Number\"","\"Terminal Name\"","\"Entry Type\"","\"Winnings\"");
           //for excel
                        for($counter=0;$counter<  count($getTransSummary1);$counter++)
                            {
                                  
                                    $_SESSION['report_values'][$counter][0] = $getTransSummary1[$counter]['DateClaimed'];
                                    $_SESSION['report_values'][$counter][1] = $getTransSummary1[$counter]['POSAccountName'];
                                    $_SESSION['report_values'][$counter][2] = $getTransSummary1[$counter]['TransSummaryID'];
                                    $_SESSION['report_values'][$counter][3] = $getTransSummary1[$counter]['Name'];
                                    $_SESSION['report_values'][$counter][4] = $getTransSummary1[$counter]['EntryType'];
                                    $_SESSION['report_values'][$counter][5] = $getTransSummary1[$counter]['Winnings'];
                            }
                            $_SESSION['report_values'][($getTransSummary1)][0] = "Total Cash Redemptions";	
                            $_SESSION['report_values'][($getTransSummary1)][1] = $totalCash;	
                            $_SESSION['report_values'][($getTransSummary1)][2] = "Total Value of Non-Cash Redemptions";	
                            $_SESSION['report_values'][($getTransSummary1)][3] = $totalNonCash;	

        ?>
      </td>
      <?php else:?>
      <?php endif;?>
    </tr>
    <tr>
      <td colspan="5">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="5" align="center">
        <!--<input id="btnDownload" name="btnDownload"  type="button" value="Download" class="labelbutton2" onclick="javascript: return alert('download here');"></input>-->
      </td>
    </tr>
  </table>
	<!-- ERROR MESSAGE -->
	<div id="light13" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
		<div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
   		
		</div>
		<br />
		<br />
		<div id="msg">
  
		</div>
		<br/>
		<br/>
		<br />
		<input id="btnOk" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light13').style.display='none';document.getElementById('fade').style.display='none';" />
	</div>
	<!-- END OF ERROR MESSAGE -->
 </div>

                
                </form>
    <?php include('footer.php')?>