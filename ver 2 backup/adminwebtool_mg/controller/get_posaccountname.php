<?php
/* 
 * Created by       :       Noel Antonio
 * Date Created     :       June 18, 2012
 * Purpose          :       Get the POS Account Name
 */
$acctid = $_POST["acctid"];

include("../init.inc.php");
$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_AccountDetails");

$cacctdtls = new SCC_AccountDetails();
$posacctid = $cacctdtls->LoadPOSAccounts();
if(count($posacctid) > 0)
{
    $options .= "<option value=\"Select One\">Select One</option>";
    for($i = 0 ; $i < count($posacctid) ; $i++)
    {
        if ($acctid == $posacctid[$i]["AccountID"]) {
            $options .= "<option selected value='".$posacctid[$i]["AccountID"]."'>".$posacctid[$i]["FullName"]."</option>";
        } else {
            $options .= "<option value='".$posacctid[$i]["AccountID"]."'>".$posacctid[$i]["FullName"]."</option>";
        }
    }    
}
echo $options;
?>