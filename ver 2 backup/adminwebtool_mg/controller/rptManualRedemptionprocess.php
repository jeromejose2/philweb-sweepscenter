<?php

/*
 * Added By : Jerome F. Jose
 * Added On : June 25, 2012
 * Purpose : 
 */
require_once("include/core/init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename,"SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_ManualRedemption");


APP::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("PagingControl2");

$fproc = new FormsProcessor();
$accounts = new SCC_Accounts();
$manualredemption = new SCC_ManualRedemption();

$txtDateFr = new TextBox("txtDateFr","txtDateFr");
$txtDateFr->ReadOnly = true;
$txtDateFr->Length = "10";
$txtDateFr->Style = "text-align:center";
$txtDateFr->Text = date("m/d/Y");
 
$txtDateTo = new TextBox("txtDateTo","txtDateTo");
$txtDateTo->ReadOnly = true;
$txtDateTo->Length = "10";
$txtDateTo->Style = "text-align:center";
$txtDateTo->Text = date("m/d/Y");

$getlist = $accounts->SelectSiteIDbylist();
$ddlPos = new ComboBox("ddlPosName","ddlPosName");
$Poslist = new ArrayList();
$Poslist->AddArray($getlist);
$ddlPos->ClearItems();
$litem = null;
//$litem[] = new ListItem("---","",true);
$litem[] = new ListItem("ALL","0");
$ddlPos->Items = $litem;
$ddlPos->DataSource = $Poslist;
$ddlPos->DataSourceText = "Name";
$ddlPos->DataSourceValue = "SiteID";
$ddlPos->DataBind();
$ddlPos->Args = "onchange='javascript: return get_terminals()';";


$btnSearch = new Button("btnSearch","btnSearch","Search");
$btnSearch->CssClass = "labelbutton2";
$btnSearch->IsSubmit = true;
$btnSearch->Args = "onclick='javascript: return checkinput30();' ";
$itemsperpage = 20;
$pgcon = new PagingControl2($itemsperpage,1);
$pgcon->URL = "javascript:ChangePage(%currentpage); ";
$pgcon->PageGroup = 5;
$pgcon->ShowMoveToLastPage = true;
$pgcon->ShowMoveToFirstPage = true;

$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($ddlPos);
$fproc->AddControl($btnSearch);

$fproc->ProcessForms();

if($fproc->IsPostBack)
{   $total = 0;
    if($btnSearch->SubmittedValue == "Search")
    {
        $pgcon->SelectedPage = 1;
    }
        $sPosAcct = $ddlPos->SubmittedValue;
        $dDateFr = $txtDateFr->SubmittedValue;
        $dDateTo = $txtDateTo->SubmittedValue;
        $_SESSION['sPosAcct'] = $sPosAcct;
        $_SESSION['dDateFr'] = $dDateFr;
        $_SESSION['dDateTo'] = $dDateTo;
     
        //$query = "CALL cafino.proc_getmanualredemptionhistory('".$sPosAcct."','0','".$dDateFr."','00:00','".$dDateTo."','00:00')";
        $ds = new DateSelector($dDateTo);
        $date2 = $ds->NextDate;
        $dDateTo = $date2;
        $dDateTo = date_create($dDateTo);
        $dDateTo = date_format($dDateTo, "Y-m-d 10:00:00.00000");
        $dDateFr = date_create($dDateFr);
        $dDateFr = date_format($dDateFr, "Y-m-d 09:59:59.99999");

        $getmanualredemp1 = $manualredemption->getmanualredemptionhist($sPosAcct, $dDateFr, $dDateTo);
        $pgcon->Initialize($itemsperpage,  count($getmanualredemp1));
        $pgHist = $pgcon->PreRender();
        $wherelimit = " LIMIT ".($pgcon->SelectedItemFrom-1).",".$itemsperpage." ";
        $getmanualredemp = $manualredemption->getmanualredemptionhistwithlimit($sPosAcct, $dDateFr, $dDateTo, $wherelimit);
               
    
    for($x=0;$x<count($getmanualredemp1);$x++)
    {
        $amount = $getmanualredemp1[$x]['Amount'];
        $total = $total + $amount;
    }
}
?>
