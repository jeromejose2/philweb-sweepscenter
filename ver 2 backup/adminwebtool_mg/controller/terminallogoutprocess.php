<?php

/*
 * Added On : June 29, 2012
 * Created by : Jerome F. Jose
 */
require_once("include/core/init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_Terminals");
App::LoadModuleClass($modulename, "SCC_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCC_AdminAccounts");
App::LoadModuleClass($modulename, "SCC_AuditTrail");


APP::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");

$fproc = new FormsProcessor();

$accounts = new SCC_Accounts();
$terminals = new SCC_Terminals();
$adminaccountsession = new SCC_AdminAccountSessions();
$adminaccounts = new SCC_AdminAccounts();
$audittrail = new SCC_AuditTrail();



$listddl = $accounts->SelectSiteIDbylist();
$ddlPos = new ComboBox("ddlPos","ddlPos");
$arrlist = new ArrayList();
$arrlist->AddArray($listddl);
$ddlPos->ClearItems();
$list = null;
$list[] = new ListItem("Select One","0",true);
$ddlPos->Items = $list;
$ddlPos->DataSource = $arrlist;
$ddlPos->DataSourceText = "Name";
$ddlPos->DataSourceValue = "SiteID";
$ddlPos->DataBind();

$txtTerminalName = new TextBox("ternme","ternme");
$txtTerminalName->Length = "20";
$txtTerminalName->Args = "onkeypress='javascript: return isAlphaNumericKey(event);'";

$btnSearch = new Button("btn","btn","Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checking();' ";

$btnlogout = new button("btnlogout","btnlogout","Force Logout");
$btnlogout->IsSubmit = true;
$btnlogout->CssClass = "labelbutton2";


$fproc->AddControl($ddlPos);
$fproc->AddControl($txtTerminalName);
$fproc->AddControl($btnSearch);
$fproc->AddControl($btnlogout);

$fproc->ProcessForms();

if($fproc->IsPostBack)
{
        $sPosNum = $ddlPos->SubmittedValue;
        $sTerName = $txtTerminalName->SubmittedValue;
        $_SESSION['ddlPos'] = $sPosNum;
        $_SESSION['ternme'] = $sTerName;
       
    if($btnSearch->SubmittedValue == "Search")
        {
            $getlogout = $terminals->GetTerminalLogout($sTerName, $sPosNum);
        }
  if($btnlogout->SubmittedValue == "Force Logout")
	{
       $getlogout = $terminals->GetTerminalLogout($sTerName, $sPosNum);
       
       
            $getsess = $adminaccountsession->GetSessionID($_SESSION['sid']);
           
            if(count($getsess) > 0)
            {
                $getinfo = $adminaccounts->getremoteip($_SESSION['sid']);
                $accountID = $getinfo[0]['AccountID'];
                $remoteip = $getinfo[0]['RemoteIP'];
            }
            $getname = $terminals->getNAME($sTerName, $sPosNum);
           
           $playable = $getname[0]['IsPlayable'];
            if($playable != 1 )
            {
                    $terminals->StartTransaction();
                    $terminals->UpdateisPlayable($sTerName);
                    $terminals->CommitTransaction();
                    
                     $audittrail->StartTransaction();
                    $arr_audit['SessionID'] = $_SESSION['sid'];
                    $arr_audit['AccountID'] = $accountID;
                    $arr_audit['TransDetails'] ="Forced Logout Terminal: ".$sTerName." ";
                    $arr_audit['RemoteIP'] = $remoteip;
                    $arr_audit['TransDateTime'] = "now_usec()";
                    $audittrail->Insert($arr_audit);
                    if($audittrail->HasError)
                    {

                    }  else 
                    {
                    $audittrail->CommitTransaction();
                    $returnmsg2 = "Terminal successfully logged out";
                    }
            }  else 
                {
                $returnmsg2 = "Terminal is not yet logged in";
                }

           


	}  
	 
    
    
    
    
    
    
    
    
    
}








?>
