<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 14, 2012
 * Purpose          :       POS Account Creation Controller
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_Sites");
App::LoadModuleClass($modulename, "SCC_AccountDetails");
App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_AccountBankDetails");

App::LoadCore("PHPMailer.class.php");
App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("CheckBox");

$fproc = new FormsProcessor();
$csites = new SCC_Sites();
$cacctdtls = new SCC_AccountDetails();
$caccts = new SCC_Accounts();
$cacctbankdtls = new SCC_AccountBankDetails();

$txtuname = new TextBox("txtuname", "txtuname", "Cashier Username");
$txtuname->Length = 15;

$txtscode = new TextBox("txtscode", "txtscode", "SWC Code");
$txtscode->Length = 3;

$txthadd = new TextBox("txthadd", "txthadd", "Home Address");
$txthadd->Multiline = true;
$txthadd->Style = "margin-left: 30px; width: 400px;";

$txtfname = new TextBox("txtfname", "txtfname", "First Name");
$txtmname = new TextBox("txtmname", "txtmname", "Middle Name");
$txtlname = new TextBox("txtlname", "txtlname", "Last Name");
$txtzip = new TextBox("txtzip", "txtzip", "Zip Code");
$txttelno = new TextBox("txttelno", "txttelno", "Telephone Number");
$txtmobno = new TextBox("txtmobno", "txtmobno", "Mobile Number");
$txtfax = new TextBox("txtfax", "txtfax", "Fax");
$txtemail = new TextBox("txtemail", "txtemail", "Email Address");
$txtwebadd = new TextBox("txtwebadd", "txtwebadd", "Website Address");
$txtcitizen = new TextBox("txtcitizen", "txtcitizen", "Citizen");
$txtpassport =  new TextBox("txtpassport", "txtpassport", "Passport");
$txtdriver =  new TextBox("txtdriver", "txtdriver", "Driver");
$txtsss =  new TextBox("txtsss", "txtsss", "SSS");
$txtothers =  new TextBox("txtothers", "txtothers", "Others");
$txtbankbranch = new TextBox("txtbankbranch", "txtbankbranch", "Bank Branch");
$txtbankacctname = new TextBox("txtbankacctname", "txtbankacctname", "Bank Account Name");
$txtbankacctnum = new TextBox("txtbankacctnum", "txtbankacctnum", "Bank Account Number");
$txtpayeebankbranch = new TextBox("txtpayeebankbranch", "txtpayeebankbranch", "Payee Bank Branch");
$txtpayeebankacctname = new TextBox("txtpayeebankacctname", "txtpayeebankacctname", "Payee Bank Account Name");
$txtpayeebankacctnum = new TextBox("txtpayeebankacctnum", "txtpayeebankacctnum", "Payee Bank Account Number");
$txtbday = new TextBox("txtbday", "txtbday", "Birthday");

$ddlcountry = new ComboBox("ddlcountry", "ddlcountry", "Country");
$opt = null;
$opt[] = new ListItem("Select One", "Select One", true);
$opt[] = new ListItem("Guam", "Guam");
$ddlcountry->Items = $opt;

$ddlbank = new ComboBox("ddlbank", "ddlbank", "Bank");
$opt1 = null;
$opt1[] = new ListItem("Select One", "Select One", true);
$opt1[] = new ListItem("Banco Filipino", "Banco Filipino");
$ddlbank->Items = $opt1;

$ddlbanktype = new ComboBox("ddlbanktype", "ddlbanktype", "Bank Type");
$opt2 = null;
$opt2[] = new ListItem("Select One", "Select One", true);
$opt2[] = new ListItem("", "");
$ddlbanktype->Items = $opt2;

$ddlpayeebank = new ComboBox("ddlpayeebank", "ddlpayeebank", "Payee Bank");
$opt3 = null;
$opt3[] = new ListItem("Select One", "Select One", true);
$opt3[] = new ListItem("Banco Filipino", "Banco Filipino");
$ddlpayeebank->Items = $opt3;

$ddlpayeebanktype = new ComboBox("ddlpayeebanktype", "ddlpayeebanktype", "Payee Bank Type");
$opt4 = null;
$opt4[] = new ListItem("Select One", "Select One", true);
$opt4[] = new ListItem("", "");
$ddlpayeebanktype->Items = $opt4;

$chkpass = new CheckBox("chkpass", "chkpass", "Passport");
$chkdriver = new CheckBox("chkdriver", "chkdriver", "Driver");
$chksss = new CheckBox("chksss", "chksss", "SSS");
$chkothers = new CheckBox("chkothers", "chkothers", "Others");

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Submit");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Args = "onclick='javascript: return checkinput_create_pos_acct(this);'";

$hidden = new Hidden("hidden", "hidden");

$fproc->AddControl($txtuname);
$fproc->AddControl($txtfname);
$fproc->AddControl($txtmname);
$fproc->AddControl($txtlname);
$fproc->AddControl($txtscode);
$fproc->AddControl($txthadd);
$fproc->AddControl($txtzip);
$fproc->AddControl($txttelno);
$fproc->AddControl($txtmobno);
$fproc->AddControl($txtfax);
$fproc->AddControl($txtemail);
$fproc->AddControl($txtwebadd);
$fproc->AddControl($ddlcountry);
$fproc->AddControl($txtcitizen);
$fproc->AddControl($txtbday);
$fproc->AddControl($chkpass);
$fproc->AddControl($txtpassport);
$fproc->AddControl($chkdriver);
$fproc->AddControl($txtdriver);
$fproc->AddControl($chksss);
$fproc->AddControl($txtsss);
$fproc->AddControl($chkothers);
$fproc->AddControl($txtothers);
$fproc->AddControl($ddlbank);
$fproc->AddControl($ddlbanktype);
$fproc->AddControl($txtbankbranch);
$fproc->AddControl($txtbankacctname);
$fproc->AddControl($txtbankacctnum);
$fproc->AddControl($ddlpayeebank);
$fproc->AddControl($ddlpayeebanktype);
$fproc->AddControl($txtpayeebankbranch);
$fproc->AddControl($txtpayeebankacctname);
$fproc->AddControl($txtpayeebankacctnum);
$fproc->AddControl($hidden);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($hidden->Text != '')
    {
        $csites->StartTransaction();
        $caccts->StartTransaction();
        $cacctdtls->StartTransaction();
        $cacctbankdtls->StartTransaction();
        
        $chksiteavailable = $csites->CheckSiteName($txtscode->SubmittedValue);
        if ($chksiteavailable[0][0] == 0)
        {
            $swccodeexist = $cacctdtls->CheckExistSWCCode($txtscode->SubmittedValue);
            if (count($swccodeexist) == 0)
            {
                $emailexist = $caccts->CheckEmailExist($txtemail->SubmittedValue);
                if (count($emailexist) == 0) 
                {
                    $userexist = $caccts->CheckUsernameExist($txtuname->SubmittedValue);
                    if (count($userexist) == 0)
                    {
                        // insert to sites
                        $registersite["Name"] = $txtscode->SubmittedValue;
                        $csites->Insert($registersite);
                        if ($csites->HasError)
                        {
                            $errormsgtitle = "POS Account Creation";
                            $errormsg = $csites->getErrors();
                            $csites->RollBackTransaction();
                        }
                        else 
                        {
                            $csites->CommitTransaction();
                            $last_siteid = $csites->LastInsertID;
                            $pass = substr(uniqid(),5);
                            
                            // insert to accounts
                            $arracct["AccountType"] = $_SESSION['accttype'];
                            $arracct["SiteID"] = $last_siteid;
                            $arracct["Username"] = $txtuname->SubmittedValue;
                            $arracct["Password"] = md5($pass);
                            $arracct["Email"] = $txtemail->SubmittedValue;
                            $arracct["Status"] = 1;
                            $arracct["DateCreated"] = 'now_usec()';
                            $caccts->Insert($arracct);
                            if ($caccts->HasError)
                            {
                                $errormsgtitle = "POS Account Creation";
                                $errormsg = $caccts->getErrors();
                                $caccts->RollBackTransaction();
                            }
                            else
                            {
                                $caccts->CommitTransaction();
                                // insert to accountdetails
                                $last_acctid = $caccts->LastInsertID;
                                $updacctdtls["AccountID"] = $last_acctid;
                                $updacctdtls["DateCreated"] = 'now_usec()';   
                                $updacctdtls["FirstName"] = $txtfname->SubmittedValue;
                                $updacctdtls["MiddleName"] = $txtmname->SubmittedValue;
                                $updacctdtls["LastName"] = $txtlname->SubmittedValue;
                                $updacctdtls["SWCCode"] = $txtscode->SubmittedValue;
                                $updacctdtls["HomeAddress"] = $txthadd->SubmittedValue;
                                $updacctdtls["ZipCode"] = $txtzip->SubmittedValue;
                                $updacctdtls["TelephoneNumber"] = $txttelno->SubmittedValue;
                                $updacctdtls["MobileNumber"] = $txtmobno->SubmittedValue;
                                $updacctdtls["Fax"] = $txtfax->SubmittedValue;
                                $updacctdtls["Email"] = $txtemail->SubmittedValue;
                                $updacctdtls["Website"] = $txtwebadd->SubmittedValue;
                                $updacctdtls["CountryID"] = $ddlcountry->SubmittedValue;
                                $updacctdtls["Citizenship"] = $txtcitizen->SubmittedValue;
                                $updacctdtls["BirthDate"] = $txtbday->SubmittedValue;
                                $updacctdtls["Passport"] = $txtpassport->SubmittedValue;
                                $updacctdtls["DriversLicense"] = $txtdriver->SubmittedValue;
                                $updacctdtls["Others"] = $txtothers->SubmittedValue;
                                $updacctdtls["DateUpdated"] = 'now_usec()';
                                $cacctdtls->Insert($updacctdtls);
                                if ($cacctdtls->HasError)
                                {
                                    $errormsgtitle = "POS Account Creation";
                                    $errormsg = $cacctdtls->getErrors();
                                    $cacctdtls->RollBackTransaction();
                                }
                                else
                                {
                                    $cacctdtls->CommitTransaction();
                                    // insert to accountbankdetails
                                    $updacctbankdtls["AccountID"] = $last_acctid;
                                    $updacctbankdtls["DateCreated"] = 'now_usec()';
                                    $updacctbankdtls["Bank"] = $ddlbank->SubmittedValue;
                                    $updacctbankdtls["BankType"] = $ddlbanktype->SubmittedValue;
                                    $updacctbankdtls["BankBranch"] = $txtbankbranch->SubmittedValue;
                                    $updacctbankdtls["BankAccountName"] = $txtbankacctname->SubmittedValue;
                                    $updacctbankdtls["BankAccountNumber"] = $txtbankacctnum->SubmittedValue;
                                    $updacctbankdtls["PayeeBank"] = $ddlpayeebank->SubmittedValue;
                                    $updacctbankdtls["PayeeBankType"] = $ddlpayeebanktype->SubmittedValue;
                                    $updacctbankdtls["PayeeBankBranch"] = $txtpayeebankbranch->SubmittedValue;
                                    $updacctbankdtls["PayeeBankAccountName"] = $txtpayeebankacctname->SubmittedValue;
                                    $updacctbankdtls["PayeeBankAccountNumber"] = $txtpayeebankacctnum->SubmittedValue;
                                    $updacctbankdtls["DateUpdated"] = 'now_usec()';
                                    $cacctbankdtls->Insert($updacctbankdtls);
                                    if ($cacctbankdtls->HasError)
                                    {
                                        $errormsgtitle = "POS Account Creation";
                                        $errormsg = $cacctbankdtls->getErrors();
                                        $cacctbankdtls->RollBackTransaction();
                                    }
                                    else
                                    {
                                        $cacctbankdtls->CommitTransaction();

                                        // send email
                                        $pm = new PHPMailer();
                                        $pm->AddAddress($txtemail->SubmittedValue);

                                        $pageURL = 'http';
                                        if (!empty($_SERVER['HTTPS'])) {$pageURL .= "s";}
                                        $pageURL .= "://";
                                        $folder = $_SERVER["REQUEST_URI"];
                                        $folder = substr($folder,0,strrpos($folder,'/') + 1);
                                        if ($_SERVER["SERVER_PORT"] != "80") 
                                        {
                                            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
                                        } 
                                        else
                                        {
                                            $pageURL .= $_SERVER["SERVER_NAME"].$folder;
                                        }

                                        $pm->IsHTML(true);

                                        $pm->Body = "Dear ".$txtfname->SubmittedValue."<br/><br/>
                                            This is to inform you that your POS Account has been successfully created on this date ".date("m/d/Y")." and time ".date("H:i:s").". Here are your new Account Details:<br/><br/>
                                            Username: <b>".$txtuname->SubmittedValue."</b><br/>
                                            Password: <b>".$pass."</b><br/><br/>".
                                        "<b>For security purposes, please change this assigned password upon log in at <a href=\"http://192.168.20.8:8105/GuamCashier\">http://www.thesweepscenter.com/Cashier</a>.<br/><br/>".
                                        "For inquiries on your account, please contact our Customer Service email at support.gu@philwebasiapacific.com ."."<br/><br/>
                                        Regards,<br/><br/>Customer Support<br/>The Sweeps Center";

                                        $pm->From = "operations@thesweepscenter.com";
                                        $pm->FromName = "The Sweeps Center";
                                        $pm->Host = "localhost";
                                        $pm->Subject = "NOTIFICATION FOR NEWLY CREATED POS ACCOUNT";
                                        $email_sent = $pm->Send();
                                        if($email_sent)
                                        {
                                            $successmsg = "New POS account has been successfully created.";
                                            $successmsgtitle = "POS Account Creation";
                                        }
                                        else
                                        {
                                            $errormsg = "An error occurred while sending the email to your email address";
                                            $errormsgtitle = "POS Account Creation";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        $errormsg = "Username already exists";
                        $errormsgtitle = "POS Account Creation";
                    }
                }
                else
                {
                    $errormsg = 'Email Address has already been used';
                    $errormsgtitle = "POS Account Creation";
                }
            }
            else
            {
                $errormsg = 'SWC entered has already been used';
                $errormsgtitle = "POS Account Creation";
            }
        } 
        else 
        {
            $errormsg = "Site name is already in use";
            $errormsgtitle = "POS Account Creation";
        }
    }
}
?>