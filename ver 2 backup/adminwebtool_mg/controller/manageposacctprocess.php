<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 19, 2012
 * Purpose          :       Manage POS Account: Managing of POS Account
 */

require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_AccountDetails");
App::LoadModuleClass($modulename, "SCC_AccountUpdateLog");
App::LoadModuleClass($modulename, "SCC_Accounts");

App::LoadCore("PHPMailer.class.php");
App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$fproc = new FormsProcessor();
$cacctdtls = new SCC_AccountDetails();
$cacctupdlog = new SCC_AccountUpdateLog();
$caccts = new SCC_Accounts();

$ddlacctname = new ComboBox("ddlacctname", "ddlacctname", "Account Name");
$ddlacctname->Args = "onchange='javascript: return SelectPOSAccountNumber()'";
$opt1 = null;
$opt1[] = new ListItem("Select One", "Select One", true);
$ddlacctname->Items = $opt1;
$posaccts = $cacctdtls->LoadPOSAccounts("Select One");
$list_posaccts = new ArrayList();
$list_posaccts->AddArray($posaccts);
$ddlacctname->DataSource = $list_posaccts;
$ddlacctname->DataSourceValue = "AccountID";
$ddlacctname->DataSourceText = "FullName";
$ddlacctname->DataBind();

$ddlacctno = new ComboBox("ddlacctno", "ddlacctno", "Account Number");
$ddlacctno->Args = "onchange='javascript: return SelectPOSAccountName()'";
$opt2 = null;
$opt2[] = new ListItem("Select One", "Select One", true);
$ddlacctno->Items = $opt2;
$posacctsid = $cacctdtls->LoadPOSAccountID();
$list_posacctsid = new ArrayList();
$list_posacctsid->AddArray($posacctsid);
$ddlacctno->DataSource = $list_posacctsid;
$ddlacctno->DataSourceValue = "AccountID";
$ddlacctno->DataSourceText = "AccountID";
$ddlacctno->DataBind();

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Search");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->IsSubmit = true;

$display = false;

$fproc->AddControl($ddlacctname);
$fproc->AddControl($ddlacctno);
$fproc->AddControl($btnSubmit);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSubmit->SubmittedValue == "Search")
    {
        $display = true;
    }
    
    if ($fproc->GetPostVar('btnSubmitOkay') != '')
    {   
        // Update POS Account Status
        $caccts->StartTransaction();
        $updacct["ID"] = $fproc->GetPostVar('acctid');
        $updacct["DateUpdated"] = 'now_usec()';
        $updacct["UpdatedBy"] = $_SESSION['accttype'];
        $updacct["Status"] = $fproc->GetPostVar('ddlacctstatus');
        $caccts->UpdateByArray($updacct);
        if ($caccts->HasError)
        {
            $errormsg = $caccts->getErrors();
            $errormsgtitle = "ERROR!";
            $caccts->RollBackTransaction();
        }
        else
        {
            $caccts->CommitTransaction();
            
            // Update Account Log
            $cacctupdlog->StartTransaction();
            $updlog["AccountID"] = $fproc->GetPostVar('acctid');
            $updlog["TransactionDescription"] = "Change Account Status";
            $updlog["Remarks"] = $fproc->GetPostVar('remarks');
            $updlog["DateCreated"] = 'now_usec()';
            $cacctupdlog->Insert($updlog);
            if ($cacctupdlog->HasError)
            {
                $errormsg = $cacctupdlog->getErrors();
                $errormsgtitle = "ERROR!";
                $cacctupdlog->RollBackTransaction();
            }
            else
            {
                $cacctupdlog->CommitTransaction();
                
                // Sending of Email
                $pm = new PHPMailer();
                $pm->AddAddress($fproc->GetPostVar('txtEmail'), $fproc->GetPostVar('fname'));

                $pageURL = 'http';
                if (!empty($_SERVER['HTTPS'])) {$pageURL .= "s";}
                $pageURL .= "://";
                $folder = $_SERVER["REQUEST_URI"];
                $folder = substr($folder,0,strrpos($folder,'/') + 1);
                if ($_SERVER["SERVER_PORT"] != "80") 
                {
                $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
                } 
                else 
                {
                $pageURL .= $_SERVER["SERVER_NAME"].$folder;
                }

                $pm->IsHTML(true);

                $pm->Body = "Dear " . $fproc->GetPostVar('fname') . "<br/><br/>
                    This is to inform you that your account status has been changed on this date ".date("m/d/Y")." and time ".date("H:i:s").". 
                    from " . $fproc->GetPostVar('oldstat') . " to " . $fproc->GetPostVar('newstat') . ".<br /><br />".
                "For further inquiries on the status of your account, please contact our Customer Service email at support.gu@philwebasiapacific.com.<br /><br />".
                "Regards,<br/>The Sweeps Center";

                $pm->From = "support.gu@philwebasiapacific.com";
                $pm->FromName = "The Sweeps Center";
                $pm->Host = "localhost";
                $pm->Subject = "NOTIFICATION FOR POS ACCOUNT CHANGE OF STATUS";
                $email_sent = $pm->Send();
                if(!$email_sent)
                {               
                    $errormsg = "An error occurred while sending the email to your email address";
                    $errormsgtitle = "ERROR!";
                }
                else
                {
                    $display = true;
                }
            }
        }
    }
}

if ($display)
{
    $arrposdtls = $cacctdtls->GetManagePOSAccountDetails($ddlacctname->SubmittedValue);
    $list_posaccts = new ArrayList();
    $list_posaccts->AddArray($arrposdtls);

    if (count($list_posaccts) > 0)
    {
        $acct_name = $list_posaccts[0]["LastName"] . ", " . $list_posaccts[0]["FirstName"] . " " . $list_posaccts[0]["MiddleName"];
        $acct_id = $list_posaccts[0]["ID"];
        $status = $list_posaccts[0]["Status"];
        $email = $list_posaccts[0]['Email'];
        $convert_status = array(1=>'Active',2=>'Suspended',3=>'Terminated');
        $wordstat = $convert_status[$status];
    }

    $posremarks = $cacctupdlog->GetPOSAccountRemarks($ddlacctname->SubmittedValue);
    if (count($posremarks) > 0)
        $remarks = $posremarks[0]["Remarks"];
    else
        $remarks = "Not yet edited";
}
?>
