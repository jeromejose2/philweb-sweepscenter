<?php

/*
 * Added On : June 25, 2012
 * Purpose : 
 */
require_once("include/core/init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename,"SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_TopUpLogs");
App::LoadModuleClass($modulename, "SCC_TerminalSessionDetails");

APP::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("PagingControl2");


$itemsperpage = 20;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
//$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;
$pgcon->CssClass = "paging";

$fproc = new FormsProcessor();
$accounts = new SCC_Accounts();
$topup = new SCC_TopUpLogs();
$terminalsessiondetails = new SCC_TerminalSessionDetails();


$txtDateFr = new TextBox("txtDateFr","txtDateFr");
$txtDateFr->Length = "10";
$txtDateFr->Style = "text-align:center";
$txtDateFr->ReadOnly = true;
$txtDateFr->Text = date("m/d/Y");

$txtDateTo = new TextBox("txtDateTo", "txtDateTo");
$txtDateTo->Length = "10";
$txtDateTo->Style = "text-align:center";
$txtDateTo->ReadOnly = true;
$txtDateTo->Text = date("m/d/Y");

$btnSearch = new Button("btnSearch", "btnSearch", "Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checkinput();' ";


$getlist = $accounts->SelectSiteIDbylist();
$ddlPos = new ComboBox("ddlPos","ddlPos");
$Poslist = new ArrayList();
$Poslist->AddArray($getlist);
$ddlPos->ClearItems();
$litem = null;
$litem[] = new ListItem("---","",true);
$litem[] = new ListItem("ALL","0");
$ddlPos->Items = $litem;
$ddlPos->DataSource = $Poslist;
$ddlPos->DataSourceText = "Name";
$ddlPos->DataSourceValue = "SiteID";
$ddlPos->DataBind();

$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSearch);
$fproc->AddControl($ddlPos);


$fproc->ProcessForms();

if($fproc->IsPostBack)
{
            $dDateFr = $txtDateFr->SubmittedValue;
            $dDateTo = $txtDateTo->SubmittedValue;
            $sPosAcct = $ddlPos->SubmittedValue;
            $_SESSION['start'] = $dDateFr;
            $_SESSION['end'] = $dDateTo;	
            $_SESSION['posacct'] = $sPosAcct;
            $ds = new DateSelector($dDateTo);
            $date2 = $ds->NextDate;
            $dDateTo = $date2;
            $dDateTo = date_create($dDateTo);
            $dDateTo = date_format($dDateTo, "Y-m-d 10:00:00.00000");
            $dDateFr = date_create($dDateFr);
            $dDateFr = date_format($dDateFr, "Y-m-d 09:59:59.99999");
                
            $getgrosshold1 = $terminalsessiondetails->GetGrossHoldHist($dDateFr, $dDateTo);
            $pgcon->Initialize($itemsperpage,  count($getgrosshold1));
            $pgHist = $pgcon->PreRender();
            $wherelimit = " LIMIT ".($pgcon->SelectedItemFrom-1).",".$itemsperpage." ";
            $getgrosshold = $terminalsessiondetails->GetGrossHoldHistbywithLIMIT($sPosAcct, $dDateFr, $dDateTo, $wherelimit);
           
            $totalsales = 0;
            for($x=0;$x<count($getgrosshold1);$x++)
            {
            $row5 = $getgrosshold1[$x][5];
            $totalsales = $totalsales + $row5;
            }
            
      if($btnSearch->SubmittedValue == "Search")
    {
        $pgcon->SelectedPage = 1;
    }
}

?>
