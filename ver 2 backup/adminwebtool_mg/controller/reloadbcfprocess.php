<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 20, 2012
 * Purpose          :       Reload BCF
 */

require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_AccountDetails");
App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_Sites");
App::LoadModuleClass($modulename, "SCC_TopUpLogs");
App::LoadModuleClass($modulename, "SCC_AccountUpdateLog");

App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$fproc = new FormsProcessor();
$cacctdtls = new SCC_AccountDetails();
$caccts = new SCC_Accounts();
$csites = new SCC_Sites();
$ctopuplogs = new SCC_TopUpLogs();
$cacctupdlog = new SCC_AccountUpdateLog();

$ddlacctname = new ComboBox("ddlacctname", "ddlacctname", "Account Name");
$ddlacctname->Args = "onchange='javascript: return SelectPOSAccountNumber()'";
$opt1 = null;
$opt1[] = new ListItem("Select One", "Select One", true);
$ddlacctname->Items = $opt1;
$posaccts = $cacctdtls->LoadPOSAccounts("Select One");
$list_posaccts = new ArrayList();
$list_posaccts->AddArray($posaccts);
$ddlacctname->DataSource = $list_posaccts;
$ddlacctname->DataSourceValue = "AccountID";
$ddlacctname->DataSourceText = "FullName";
$ddlacctname->DataBind();

$ddlacctno = new ComboBox("ddlacctno", "ddlacctno", "Account Number");
$ddlacctno->Args = "onchange='javascript: return SelectPOSAccountName()'";
$opt2 = null;
$opt2[] = new ListItem("Select One", "Select One", true);
$ddlacctno->Items = $opt2;
$posacctsid = $cacctdtls->LoadPOSAccountID();
$list_posacctsid = new ArrayList();
$list_posacctsid->AddArray($posacctsid);
$ddlacctno->DataSource = $list_posacctsid;
$ddlacctno->DataSourceValue = "AccountID";
$ddlacctno->DataSourceText = "AccountID";
$ddlacctno->DataBind();

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Search");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->IsSubmit = true;

$display = false;

$fproc->AddControl($ddlacctname);
$fproc->AddControl($ddlacctno);
$fproc->AddControl($btnSubmit);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSubmit->SubmittedValue == "Search")
    {
        $display = true;
    }
    
    if ($fproc->GetPostVar('btnSubmitOkay') == 'OKAY')
    {
        $dec_amount = 1500;
        
        $getsiteid = $caccts->GetSiteID($ddlacctname->SubmittedValue);
        $siteid = $getsiteid[0]["SiteID"];
        $getbalance = $csites->CheckBCFBalance($siteid);
        $startbalance = $getbalance[0]["Balance"];
        $endbalance = $getbalance[0]["Balance"] + $dec_amount;

        // update site balance
        $csites->StartTransaction();
        $updsitebal["ID"] = $siteid;
        $updsitebal["DateUpdated"] = 'now_usec()';
        $updsitebal["UpdatedBy"] = $_SESSION['aid'];
        $updsitebal["Balance"] = $endbalance;
        $csites->UpdateByArray($updsitebal);
        if ($csites->HasError)
        {
            $errormsg = $csites->getErrors();
            $errormsgtitle = "ERROR!";
            $csites->RollBackTransaction();
        }
        else
        {
            $csites->CommitTransaction();
            
            // insert to topup logs
            $ctopuplogs->StartTransaction();
            $instoplog["SiteID"] = $siteid;
            $instoplog["RecCreOn"] = 'now_usec()';
            $instoplog["AccountID"] = $_SESSION['aid'];
            $instoplog["Amount"] = number_format($dec_amount, 2);
            $instoplog["StartBalance"] = number_format($startbalance, 2);
            $instoplog["EndBalance"] = number_format($endbalance, 2);
            $ctopuplogs->Insert($instoplog);
            if ($ctopuplogs->HasError)
            {
                $errormsg = $ctopuplogs->getErrors();
                $errormsgtitle = "ERROR!";
                $ctopuplogs->RollBackTransaction();
            }
            else
            {
                $ctopuplogs->CommitTransaction();
                
                // update account logs
                $cacctupdlog->StartTransaction();
                $updlog["AccountID"] = $siteid; // Site ID for Account ID field? -- According to SP
                $updlog["TransactionDescription"] = "Update Balance";
                $updlog["Remarks"] = "";
                $updlog["DateCreated"] = 'now_usec()';
                $cacctupdlog->Insert($updlog);
                if ($cacctupdlog->HasError)
                {
                    $errormsg = $cacctupdlog->getErrors();
                    $errormsgtitle = "ERROR!";
                    $cacctupdlog->RollBackTransaction();
                }
                else
                {
                    $cacctupdlog->CommitTransaction();
                    $display = true;
                }
            }
        }
    }
}

if ($display)
{
    $bcfdtls = $cacctdtls->GetBCFDetails($ddlacctname->SubmittedValue);
    $list_bcfdtls = new ArrayList();
    $list_bcfdtls->AddArray($bcfdtls);

    if (count($list_bcfdtls) > 0)
    {
        $acct_name = $list_bcfdtls[0]["LastName"] . ", " . $list_bcfdtls[0]["FirstName"] . " " . $list_bcfdtls[0]["MiddleName"];
        $acct_id = $list_bcfdtls[0]["ID"];
        $bcf = $list_bcfdtls[0]["Balance"];
        $status = $list_bcfdtls[0]['Status'];
    }
}
?>
