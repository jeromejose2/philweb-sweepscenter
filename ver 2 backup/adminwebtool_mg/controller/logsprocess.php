<?php
/*-->
 * Date : July 02,2012
 * Created by : Jerome F. Jose
   <--*/
require_once("include/core/init.inc.php");



APP::LoadControl("Button");
$fproc = new FormsProcessor;
$btnApiLogs = new Button("btnApiLogs","btnApiLogs","API LOGS");
$btnApiLogs->Style = "margin-right: 10px;";
$btnApiLogs->IsSubmit = true;

$btnDBLogs = new Button("btnDBLogs","btnDBLogs","DB LOGS");
$btnDBLogs->IsSubmit = true;
$btnDBLogs->Style = "margin-left:10px;";

$btnBrowseLogs = new Button("btnBrowseLogs","btnBrowseLogs","BROWSE INTERNET LOGS");
$btnBrowseLogs->Style = "margin-left : 10px; ";
$btnBrowseLogs->IsSubmit = true;

$fproc->AddControl($btnApiLogs);
$fproc->AddControl($btnDBLogs);
$fproc->AddControl($btnBrowseLogs);
       
 $fproc->ProcessForms();
 
 
if($fproc->IsPostBack)
{
    if($btnBrowseLogs->SubmittedValue == "BROWSE INTERNET LOGS")
    {
        $path = "BrowseInternetLogs/";
    }elseif($btnDBLogs->SubmittedValue == "DB LOGS")
    {
        $path = "DBLogs/";
    }else
    {
        if (($_SESSION['accttype'] == 1) || ($_SESSION['accttype'] == 3))
    {
        $path = "APILogs/";
    }
    else
    {
        $path = "BrowseInternetLogs/";
    }
    }
    
    
}


?>
