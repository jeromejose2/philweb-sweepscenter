<?php

/*
 * Added On : June 29, 2012
 * Created by : Jerome F. Jose
 * Modified By: Noel Antonio
 * Date Modified: September 19, 2012
 * Reason: To cover all deposit, reload and withdraw in the manual redeem.
 */

require_once("include/core/init.inc.php");
require_once($librarydir . 'MicrogamingAPI.class.php');
//require_once('../views/include/MicrogamingAPI.class.php');
$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCC_Terminals");
App::LoadModuleClass($modulename, "SCC_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCC_AdminAccounts");
App::LoadModuleClass($modulename, "SCC_AuditTrail");
App::LoadModuleClass($modulename, "SCCSM_AgentSessions");
App::LoadModuleClass($modulename, "SCC_TerminalSessions");
App::LoadModuleClass($modulename, "SCC_TransactionLogs");
App::LoadModuleClass($modulename, "SCC_ManualRedemption");

APP::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("Hidden");

$fproc = new FormsProcessor();
$accounts = new SCC_Accounts();
$terminals = new SCC_Terminals();
$servicemapping = new SCCSM_AgentSessions();
$terminalsession = new SCC_TerminalSessions();
$audittrail = new SCC_AuditTrail();
$transactionlogs = new SCC_TransactionLogs();
$manualredemption = new SCC_ManualRedemption();

$listddl = $accounts->SelectSiteIDbylist();
$ddlPosName = new ComboBox("ddlPosName","ddlPosName");
$arrlist = new ArrayList();
$arrlist->AddArray($listddl);
$ddlPosName->ClearItems();
$list = null;
$list[] = new ListItem("Select One","0",true);
$ddlPosName->Items = $list;
$ddlPosName->DataSource = $arrlist;
$ddlPosName->DataSourceText = "Name";
$ddlPosName->DataSourceValue = "SiteID";
$ddlPosName->DataBind();
$ddlPosName->Args = "onchange='javascript: return get_terminals();'";

/*$stransrefid1 = new TextBox("txtTransRefID","txtTransRefID");
$stransrefid1->Length = "8";
$stransrefid1->Args = "onkeypress='javascript: return isNumberKey(event);'";*/

$ddlTerminalName = new ComboBox("ddlTerminalName","ddlTerminalName");
$list = null;
$list[] = new ListItem("---","",true);
$ddlTerminalName->Items = $list;

$txtamount = new TextBox("txtAmount","txtAmount");
$txtamount->Args = "onkeypress='javascript: return disablealphakeys(event);' autocomplete='off' maxlength='10'";

$btnSearch = new Button("btn","btn","SEARCH");
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checkmanualredemption();'";
$btnSearch->IsSubmit = true;

$hidlist = new Hidden("hidlist","hidlist");
       
$btnProcess = new Button("btnProcess","btnProcess","OKAY");
$btnProcess->IsSubmit = true;
$btnProcess->CssClass = "labelbold2";
$btnRedeem = new Button("btnRedeem","btnRedeem","Redeem");
$btnRedeem->CssClass = "labelbutton2";
$btnRedeem->Args = "onclick = 'javascript: return displayconfirmation(); ' ";

$fproc->AddControl($ddlPosName);
$fproc->AddControl($ddlTerminalName);
$fproc->AddControl($txtamount);
$fproc->AddControl($btnSearch);
$fproc->AddControl($hidlist);
$fproc->AddControl($btnProcess);
$fproc->AddControl($btnRedeem);

$fproc->ProcessForms();

if($fproc->IsPostBack)
{
    $display = false;
    $iamnt = $txtamount->SubmittedValue;
    
    $_SESSION['mr_siteid'] = $ddlPosName->SubmittedValue;
    $_SESSION['mr_terminalid'] = $ddlTerminalName->SubmittedValue;
    $_SESSION['mr_transrefid'] = "";
    $_SESSION['mr_DateCreated'] = "";
    
    //$listManualRedemption = $transactionlogs->getListManualRedemption($stransrefid, $iTerminalID, $iamnt);
    $listManualRedemption = $terminals->SelectTerminalInfoforManualRedemption($_SESSION['mr_terminalid']);
    $posaccountname = $listManualRedemption[0]['PosAccountName'];
    $terminalname = $listManualRedemption[0]['Name'];
    
    $getall = $servicemapping->selectbywhere();
    if(count($getall) == 1)
    {
        $sessionGUID_MG = $getall[0]["SessionGUID"];
    }
    
    $ip_add_MG = $_SERVER['REMOTE_ADDR'];
    $player = $terminalname;
            
    $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
            "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
            "<IPAddress>".$ip_add_MG."</IPAddress>".
            "<ErrorCode>0</ErrorCode>".
            "<IsLengthenSession>true</IsLengthenSession>".
            "</AgentSession>";
    
    $client = new nusoap_client('https://entservices.totalegame.net/EntServices.asmx?WSDL', 'wsdl');
    $client->setHeaders($headers);
    $param = array('delimitedAccountNumbers' => $player);
    $result = $client->call('GetAccountBalance', $param);
     
    if($btnSearch->SubmittedValue == "SEARCH")
    {
        $getlist = $terminals->getIDList($_SESSION['mr_siteid']);
        $list_terminals = new ArrayList();
        $list_terminals->AddArray($getlist);
        $ddlTerminalName->DataSource = $list_terminals;
        $ddlTerminalName->DataSourceText = "Name";
        $ddlTerminalName->DataSourceValue = "ID";
        $ddlTerminalName->DataBind();
        $ddlTerminalName->SetSelectedValue($_SESSION['mr_terminalid']);
        
        $amount = $amount = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];
        
        if ($amount != $iamnt)
        {
            $status = 1;
            $returnmsg = "Amount not equal!";
        }
        else
        {
            $display = true;
            $_SESSION['mr_TerminalName'] = $terminalname;
            $_SESSION['mr_Amount'] = $amount;
        }
    }
    
    if($btnProcess->SubmittedValue == "OKAY")
    {
        if($result['GetAccountBalanceResult']['BalanceResult']['Balance'] >= (int)$amount) 
        { 
            $getid = $terminalsession->getID($_SESSION['mr_terminalid']);
            if(count($getid) > 0)
            {
                if(count($getid) == 1)
                {
                    $terminalsessionid = $getid[0]["ID"];
                }
                else
                {
                    $returnmsg = "No active terminal session.";
                    $status = 1;
                }   
            }
            else
            {
                $returnmsg = "An error while retrieving terminal session id.";
                $status = 1;
            }
            
            if(isset($terminalsessionid))
            {
                $getdetail = $terminals->getAllbyID($_SESSION['mr_terminalid']);
                if(count($getdetail) > 0)
                {
                    $chr_IsFreeEntry = $getdetail[0]["IsFreeEntry"];
                    $terminalsession->StartTransaction();
                    $update = $terminalsession->updateterminalSesSion($_SESSION['mr_terminalid'], $terminalsessionid);
                    if($terminalsession->HasError)
                    {
                        $returnmsg = " ".$terminalsession->getError();
                        $status = 1;
                    }  
                    else 
                    {
                        $terminalsession->CommitTransaction();
                        if(!$update)
                        {
                            $returnmsg = "Error updating terminal sessions.";
                            $status = 1;
                        }
                        if($chr_IsFreeEntry == "0")
                        {
                            $terminals->StartTransaction();
                            $update2 =  $terminals->updateTerminalsID($_SESSION['mr_terminalid']);
                            if($terminals->HasError)
                            {
                                $returnmsg = " ".$terminals->getError();
                                $status = 1;

                            }
                            else
                            {
                                $terminals->CommitTransaction();
                                if(!$update2)
                                {
                                    $returnmsg = "Error updating terminal status.";
                                    $status = 1;
                                }
                            }
                        }
                                
                        $audittrail->StartTransaction();
                        $arr_audit['SessionID'] = $_SESSION['sid'];
                        $arr_audit['AccountID'] = $_SESSION['acctid'];
                        $arr_audit['TransDetails']= "Session end for terminalid: " . $_SESSION['mr_terminalid'] . " ";
                        $arr_audit['RemoteIP']= $_SERVER['REMOTE_ADDR'];
                        $arr_audit['TransDateTime'] = "now_usec()";
                        $insert = $audittrail->Insert($arr_audit);
                        if($audittrail->HasError)
                        {
                            $returnmsg = " ".$audittrail->getError();
                            $status = 1;
                            $audittrail->RollBackTransaction();
                        }
                        else
                        {
                            $audittrail->CommitTransaction();
                            if(!$insert)
                            {
                                $returnmsg = "Error inserting in audit trail.";
                                $status = 1;
                            }
                            $param2 = array('accountNumber' => $player, 'amount' => $amount, 'currency' => 1);
                            $result2 = $client->call('Withdrawal', $param2);
                            if($result2['WithdrawalResult']['IsSucceed'] == "true")
                            {
                                //insert to logs
                                $filename = "../APILogs/logs.txt";
                                $fp = fopen($filename , "a");

                                if($fp)
                                {
                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(MANUAL REDEMPTION) || ".$terminalname." || ".serialize($result2)."\r\n");
                                        fclose($fp);
                                }
                                else
                                {
                                        $returnmsg = "Cannot log Withdrawal API call.";
                                        $status = 0;
                                }
                                $transactionlogs->StartTransaction();
                                $update3 = $transactionlogs->updateStatusManualRedemp($_SESSION['mr_transrefid']);
                                if($transactionlogs->HasError)
                                {
                                    $returnmsg = " ".$transactionlogs->getError();
                                    $status = 1;
                                    $transactionlogs->RollBackTransaction();
                                }
                                else
                                {
                                    $transactionlogs->CommitTransaction();
                                    if(!$update3)
                                    {
                                            $returnmsg = "Updating Transaction Reference ID status unsuccessful.";
                                            $status = 1;
                                    }
                                                
                                    //insert into tblmanual redemp
                                    $manualredemption->StartTransaction();
                                    $arr_manual['SiteID'] = $_SESSION['mr_siteid'];
                                    $arr_manual['TransactionDate'] = $_SESSION['mr_DateCreated'];
                                    $arr_manual['DateProcessed'] = "now_usec()";
                                    $arr_manual['TerminalID'] = $_SESSION['mr_terminalid'];
                                    $arr_manual['Amount'] = $_SESSION['mr_Amount'];
                                    $arr_manual['TransactionReferenceID'] = $_SESSION['mr_transrefid'];
                                    $arr_manual['ProcessedBy'] = $_SESSION['acctid'];
                                    $arr_manual['Status'] = '1';
                                    $insertmanualredemp = $manualredemption->Insert($arr_manual);
                                    if($manualredemption->HasError)
                                    {
                                        $returnmsg = " ".$manualredemption->getError();
                                        $status = 1;
                                        $manualredemption->RollBackTransaction();
                                    }  
                                    else 
                                    {
                                        $manualredemption->CommitTransaction();
                                        if($insertmanualredemp)
                                        {
                                                $returnmsg = "You have successfully redeemed amount of " . $_SESSION['mr_Amount'] . " on " . $_SESSION['mr_TerminalName'] . ".";
                                                $status = 0;
                                                $txtamount->Text = "";
                                                $ddlPosName->SetSelectedValue(0);
                                                unset($_SESSION['mr_siteid']);
                                                unset($_SESSION['mr_terminalid']);
                                                unset($_SESSION['mr_TerminalName']);
                                                unset($_SESSION['mr_Amount']);
                                        }
                                        else
                                        {
                                            $returnmsg = "Manual Redemption log unsuccessful.";
                                            $status = 1;
                                        }
                                        $arrY_audit['SessionID'] = $_SESSION['sid'];
                                        $arrY_audit['AccountID'] = $_SESSION['acctid'];
                                        $arrY_audit['TransDetails'] = "Manual Redemption: " . $_SESSION['mr_TerminalName'] . ""; //$_SESSION['mr_transrefid']
                                        $arrY_audit['TransDateTime'] = "now_usec()";
                                        $arrY_audit['RemoteIP'] = $_SERVER['REMOTE_ADDR'];
                                        $insertaudit = $audittrail->Insert($arrY_audit);
                                        if(!$insertaudit)
                                        {
                                                $returnmsg = "Audit trail log unsuccessful.";
                                                $status = 1;
                                        }
                                    }       
                                }
                            }
                            else
                            {
                                $returnmsg = "Manual Redemption failed.";
                                $status = 1;
                            }            
                        }      
                    }      
                }
                else
                {
                        $returnmsg = "Terminal id does not exist.";
                        $status = 1;
                }
            }
        }
        else
        {
            $returnmsg= "You cannot proceed because there is no enough balance to perform the withdrawal.";
            $status = 1;
        }
    }  
}
?>