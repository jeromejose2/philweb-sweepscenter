<?php

/*
 * Added On : June 29, 2012
 * Created by : Jerome F. Jose
 */
require_once("include/core/init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCCV_VoucherInfo");
App::LoadModuleClass($modulename, "SCCV_AuditLog");
App::LoadModuleClass($modulename, "SCCV_BatchInfo");

$voucherinfo = new SCCV_VoucherInfo();
$auditlog = new SCCV_AuditLog();
$scbatchinfo = new SCCV_BatchInfo();

APP::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("Label");

$fproc = new FormsProcessor();

$txtVchrCode = new TextBox("vouchercode","vouchercde");
$txtVchrCode->Length = "20";
$txtVchrCode->Args = "onkeypress='javascript: return isAlphaNumericKey(event);' ";


$btnSearch = new Button("btn","btn","Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checkinputvoucherCD();'";

$linkbtn = new Button('linkbtn','linkbtn','OKAY');
$linkbtn->IsSubmit = true;
$linkbtn->CssClass = "labelbutton2";

$btnvoid = new Button("btnvoid","btnvoid","Cancel Voucher");
$btnvoid->CssClass = "labelbutton2";
$btnvoid->IsSubmit = true;
$btnvoid->Args = "onclick='javascript: return checkinputvoucherCC();'";
    
$ddlChangeStatus = new ComboBox('ddlChangeStatus','ddlChangeStatus');
$litem = null;
$litem[] = new ListItem('Please Select', "0");
$litem[] = new ListItem('By Code', "1");
$litem[] = new ListItem('By Batch', "2");
$ddlChangeStatus->Items = $litem;
$ddlChangeStatus->Args = "onchange = 'javascript: update_selection();'";

$getbatcher = $scbatchinfo->SelectperBatch();
$batchlist = new ArrayList();
$batchlist->AddArray($getbatcher);

$ddlvoucherbatch = new ComboBox('ddlvoucherbatch','ddlvoucherbatch');
$litems = null;
$litems[] = new ListItem ('Please Select',"0");
$ddlvoucherbatch->Items = $litems;
$ddlvoucherbatch->DataSource = $batchlist;
$ddlvoucherbatch->DataSourceText = "BatchNo";
$ddlvoucherbatch->DataSourceValue = "BatchNo";
$ddlvoucherbatch->DataBind();

$rtnmsg = new Label('rtnmsg','rtnmsg');
$rtnmsg->Style = "font-size: 20px; color: red; font-weight: bold;";

$remarks1 = new TextBox('rmklbl','rmklbl');
$remarks1->Length = 50;
$remarks1->Style = "width: 400px; height: 100px; border-color: #FF9C42; border-style: solid;";
$remarks1->Multiline = true;

$btnyes = new Button('btnyes','btnyes',"YES");
$btnyes->IsSubmit = true;
$btnyes->CssClass = "labelbutton2";
         
$fproc->AddControl($txtVchrCode);
$fproc->AddControl($btnSearch);
$fproc->AddControl($btnvoid);
$fproc->AddControl($ddlChangeStatus);
$fproc->AddControl($ddlvoucherbatch);
$fproc->AddControl($rtnmsg);
$fproc->AddControl($linkbtn);
$fproc->AddControl($remarks1);
$fproc->AddControl($btnyes);

$fproc->ProcessForms();

if($fproc->IsPostBack)
{
        $viewtable = '';
        $sVchrCode = $txtVchrCode->SubmittedValue;
        $batchno = $ddlvoucherbatch->SubmittedValue;
        $_SESSION['vouchercde'] = $sVchrCode;
        $_SESSION['batchnumber'] = $batchno;
        
        
        if($linkbtn->SubmittedValue == "OKAY")
        {
            $viewtable = '0';
            if($ddlChangeStatus->SubmittedValue == 1)
                        {
                            $getvoucher = $voucherinfo->getVoucherWhereVoucherno($_SESSION['vouchercde']);
                            $txtVchrCode->Enabled = true;
                        }
                            if($ddlChangeStatus->SubmittedValue == 2)
                        {
                            $getvoucher = $voucherinfo->getVoucherbyBatch($_SESSION['batchnumber']);
                        }
//             App::Pr("<script> window.location = 'cancelvoucher.php'; </script>"); 
        }
        
        
        if($btnSearch->SubmittedValue == "Search")
            {
            $viewtable = '0';
                if($ddlChangeStatus->SubmittedValue == 1)
                {
                    $getvoucher = $voucherinfo->getVoucherWhereVoucherno($sVchrCode);
                    $txtVchrCode->Enabled = true;
                }
                if($ddlChangeStatus->SubmittedValue == 2)
                {
                    $getvoucher = $voucherinfo->getVoucherbyBatch($batchno);
                }


            }
      
    if($btnvoid->SubmittedValue == "Cancel Voucher")
	{
        $confirm_msg = "Are you sure you want to cancel this voucher(s)?";
        $_SESSION['checkbox'] = $fproc->GetPostVar("checkbox");
        $viewtable = '0';
                    if($ddlChangeStatus->SubmittedValue == 1)
                    {
                        $getvoucher = $voucherinfo->getVoucherWhereVoucherno($sVchrCode);
                        $txtVchrCode->Enabled = true;
                    }
                     if($ddlChangeStatus->SubmittedValue == 2)
                    {
                        $getvoucher = $voucherinfo->getVoucherbyBatch($batchno);
                    }
        
        }
        
if($btnyes->SubmittedValue == "YES")
{
           
        $checkbox = $_SESSION['checkbox'];
             if(count($checkbox) > 0)
             {
                $voucherinfo->StartTransaction();
                $auditlog->StartTransaction();
                
                $getdetails = $voucherinfo->getVoucherByid($checkbox);
                $remarks = $remarks1->SubmittedValue;
                $acctid = $_SESSION['acctid'];
                $ip = $_SERVER['REMOTE_ADDR'];
                  
            for($x=0;$x<count($getdetails);$x++)
            {
                      $voucher = $getdetails[$x]['VoucherNo'];
                       $selectstatus = $voucherinfo->selectVoucherstatus($voucher);
                       
                $boolrollback = false;       
                for($a=0;$a<count($selectstatus);$a++)
                { 
                       $status = $selectstatus[$a]['Status'];
                    if($status == 1 || $status == 4 )
                    {
                                $voucherinfo->UpdateVoucherStats($remarks, $voucher);
                                if($voucherinfo->HasError)
                                {
                                    $boolrollback = true;
                                }else
                                {
                                        //insert into auditlog
                                        $arr_audit['TransDetails'] = "Cancel voucher:".$voucher;
                                        $arr_audit['IPAddress'] = $ip;
                                        $arr_audit['DateCreated'] = "now_usec()";
                                        $arr_audit['CreatedBy'] = $acctid;
                                        $auditlog->Insert($arr_audit);
                                        if($auditlog->HasError)
                                        {
                                           $boolrollback = true;
                                        }  else 
                                        {
                                                 if($ddlChangeStatus->SubmittedValue == 1)
                                                {
                                                 $getvoucher = $voucherinfo->getVoucherWhereVoucherno($_SESSION['vouchercde']);
                                                 $txtVchrCode->Enabled = true;
                                                }
                                                 if($ddlChangeStatus->SubmittedValue == 2)
                                                {
                                                    $getvoucher = $voucherinfo->getVoucherbyBatch($_SESSION['batchnumber']);
                                                }
                                                $return_msg = "Voucher(s)  successfully cancelled. ";
                                        }
                                    }
                    }
                }  
         }
                  
                    if($boolrollback == TRUE)
                    {
                          $voucherinfo->RollBackTransaction();
                          $auditlog->RollBackTransaction();
                    }else
                    {
                         $voucherinfo->CommitTransaction();
                         $auditlog->CommitTransaction();
                    }                                                

             }
                
                
	}
      
        
}
?>