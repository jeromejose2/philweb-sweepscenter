<?php

/*
 * Added By : Jerome F. Jose
 * Added On : June 21, 2012
 * Purpose : View/Manage User Account
 */
require_once("include/core/init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_AdminAccounts");
App::LoadModuleClass($modulename, "SCC_AdminAccountSessions");
APP::LoadModuleClass($modulename, "SCC_AccountStatusLogs");
App::LoadModuleClass($modulename, "SCC_AuditTrail");
App::LoadCore("PHPMailer.class.php");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$accountstatuslog = new SCC_AccountStatusLogs();
$adminaccount = new SCC_AdminAccounts();
$audittrail = new SCC_AuditTrail();
$adminaccountsession = new SCC_AdminAccountSessions();
$fproc = new FormsProcessor();
$managegroup = $adminaccount->ManageAccountGroup();

$uname1 = new ComboBox("uname","uname");
$ulist = null;
$ulist[] = new ListItem("Please Select", "0",true);
$uname1->Items = $ulist;
$ulist = new ArrayList();
$ulist->AddArray($managegroup);
$uname1->DataSource = $ulist;
$uname1->DataSourceText = "Username";
$uname1->DataSourceValue = "AccountID";
$uname1->DataBind();

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "labelbutton2";

$btnEdit = new Button("btnEdit","btnEdit","Edit");

$btnApply = new Button("btnApply","btnApply","Apply");
$btnApply->IsSubmit = true;
$btnApply->CssClass ="labelbutton2";

$btnback = new Button("back","back","Back");
$btnback->IsSubmit = true;
$btnback->CssClass = "labelbutton2";

$btnSubmitOkay = new Button("btnSubmit2","btnSubmitOkay","Okay");
$btnSubmitOkay->CssClass = "labelbutton2";
$btnSubmitOkay->IsSubmit = true;

$acctstatus  = new ComboBox("acctstatus","acctstatus");
 $litem = null;
$litem[] = new ListItem("Select One", "selectone");
       

$fproc->AddControl($uname1);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnEdit);
$fproc->AddControl($btnApply);
$fproc->AddControl($btnback);
$fproc->AddControl($btnSubmitOkay);
$fproc->AddControl($acctstatus);

$fproc->ProcessForms();


if($fproc->IsPostBack)
{
     $sessionID =  $_SESSION['sid'];
     $username = $uname1->SubmittedValue; // accountID
     $_SESSION['useraccountID'] = $username;
     $getdetails = $adminaccount->getdetailsmanageuser($username);
     $selectremarks = $accountstatuslog->SelectRemarks($username);
     if(count($selectremarks) > 0)
            {
                $remarks = $selectremarks[0]['Remarks'];
            }
            else
            {
                $remarks = "";
            }

     if(count($getdetails) > 0)
            {
                $uname = $getdetails[0]["Username"];
                $acctid = $getdetails[0]["AccountID"];
                $status = $getdetails[0]["Status"];
                $email = $getdetails[0]["Email"];
                if($status == 1)
                {
                    $status2 = "Active";
                }
                else if($status == 2)
                {
                    $status2 = "Inactive";
                }
                else if($status == 3)
                {
                    $status2 = "Suspended";
                }
                else if($status == 4)
                {
                    $status2 = "Terminated";
                }
            }
    if($btnback->SubmittedValue == "Back")
                    {
                        unset($displayupdate);
                        $displayinfo = "ok";
                    }

    if($status2 == "Terminated")
            {
                    $btnEdit->CssClass = "labelbutton2";
                    $btnEdit->Args = "style='cursor: default;'";
                    $btnEdit->Style = "cursor: default;";
            }else 
                {

                    $btnEdit->CssClass = "labelbutton2";
                    $btnEdit->IsSubmit = true;
                }

                
    if ($btnSubmit->SubmittedValue == "Submit")
       {
        $displayinfo = "ok";
        $getdetails = $adminaccount->getdetailsmanageuser($username);
        $selectremarks = $accountstatuslog->SelectRemarks($username);
        if(count($selectremarks) > 0)
                {
                    $remarks = $selectremarks[0]['Remarks'];
                }
                else
                {
                    $remarks = "";
                }

         if(count($getdetails) > 0)
                        {
                        $uname = $getdetails[0]["Username"];
                        $acctid = $getdetails[0]["AccountID"];
                        $status = $getdetails[0]["Status"];
                        $email = $getdetails[0]["Email"];
                        if($status == 1)
                        {
                            $status2 = "Active";
                        }
                        else if($status == 2)
                        {
                            $status2 = "Inactive";
                        }
                        else if($status == 3)
                        {
                            $status2 = "Suspended";
                        }
                        else if($status == 4)
                        {
                            $status2 = "Terminated";
                        }
                        }

     if (isset($_POST["changestatus"])) 
                {
                    $acctstatus = $_POST['acctstatus'];
                    $remarks = $_POST['remarks'];
                    $_SESSION['acctstatus'] = $acctstatus;
                    $_SESSION['acct_id_manageuseracct'] = $acctid;
                    $_SESSION['old_status'] = $status;
                    $_SESSION['remarks'] = $remarks;

                }
           
   
    }

    
 if($btnEdit->SubmittedValue == "Edit")
    {
                       
                    if($status != '1')
                    {
                        $litem[] = new ListItem("Active", "1");
                    }
                    if($status != '2')
                    {
                        $litem[] = new ListItem("Inactive", "2");
                    }
                    if($status != '3')
                    {
                        $litem[] = new ListItem("Suspended", "3");
                    }
                    if($status != '4')
                    {
                        $litem[] = new ListItem("Terminated", "4");
                    }
                    $acctstatus->Items = $litem;
                    
        $displayupdate = "ok";
        $getdetails = $adminaccount->getdetailsmanageuser($username);
        $selectremarks = $accountstatuslog->SelectRemarks($username);
    
     if(count($selectremarks) > 0)
            {
                $remarks = $selectremarks[0]['Remarks'];
            }
            else
            {
                $remarks = "";
            }

      if(count($getdetails) > 0)
                {
                    $uname = $getdetails[0]["Username"];
                    $acctid = $getdetails[0]["AccountID"];
                    $status = $getdetails[0]["Status"];
                    $email = $getdetails[0]["Email"];
                    if($status == 1)
                    {
                        $status2 = "Active";
                    }
                    else if($status == 2)
                    {
                        $status2 = "Inactive";
                    }
                    else if($status == 3)
                    {
                        $status2 = "Suspended";
                    }
                    else if($status == 4)
                    {
                        $status2 = "Terminated";
                    }
                
                     if($status == 1)
                    {
                        $status = "Active";
                    }
                    else if($status == 2)
                    {
                        $status = "Inactive";
                    }
                    else if($status == 3)
                    {
                        $status = "Suspended";
                    }
                    else if($status == 4)
                    {
                        $status = "Terminated";
                    }
                }
       
     if (isset($_POST["changestatus"])) 
            {
                $acctstatus = $_POST['acctstatus'];
                $remarks = $_POST['remarks'];
                $_SESSION['acctstatus'] = $acctstatus;
                $_SESSION['acct_id_manageuseracct'] = $acctid;
                $_SESSION['old_status'] = $status;
                $_SESSION['remarks'] = $remarks;

            }
          
    }
 
    
if(isset($_POST['btnSubmit3'])) // okay button
    { 

        $remarks =  $_POST['remarks1'];
        $username = $_SESSION['useraccountID'];
        $getdetails = $adminaccount->getdetailsmanageuser($username);
 
    if (count($getdetails) > 0 )
                {
                    $uname = $getdetails[0]["Username"];
                    $acctid = $getdetails[0]["AccountID"];
                    $status = $getdetails[0]["Status"];
                    $email = $getdetails[0]["Email"];
                }else
                {
                    $uname = "";
                    $acctid = "";
                    $status = "";
                    $email = "";
                }
    
       // if select account 
if(isset($_POST['acctstatus'])) 
    {
            $_SESSION['acct_id_manageuseracct'] = $_POST['hidid']; // account id
            $_SESSION['acctstatus'] = $_POST['acctstatus']; // new set account status
            $_SESSION['remarks'] = $_POST['remarks'];

   //user status update
                $date_now = date('m/d/Y');
                $time_now = date('h:i:s A');

                if($_SESSION['acctstatus'] == 1)
                {
                    $acct_status1 = "Active";
                }
                else if($_SESSION['acctstatus'] == 2)
                {
                    $acct_status1 = "Inactive";
                }
                else if($_SESSION['acctstatus'] == 3)
                {
                    $acct_status1 = "Suspended";
                }
                else if($_SESSION['acctstatus'] == 4)
                {
                    $acct_status1 = "Terminated";
                }
    
        //updateuserstatus
   
  
            $new_status =  $_SESSION['acctstatus'];
            $old_status = $status;
            $getsessdetail = $adminaccountsession->GetSessionID($sessionID);
  
   if(count($getsessdetail) > 0)
                {
            $getUname = $adminaccount->GetIdUnameRemoteIp($sessionID);

                $Uname = $getUname[0]['Username'];
                $accountid = $getUname[0]['ID'];
                $remoteip = $getUname[0]['RemoteIP'];
                }
                $getdetails = $adminaccount->GetAccountID($username);
 
        if(count($getdetails) > 0)
            {
                    $acctid = $username;
                $adminaccount->StartTransaction();

                $updatestatus = $adminaccount->UpdateStatus($new_status, $Uname, $acctid);
                if($adminaccount->HasError)
                {
                    $adminaccount->RollBackTransaction();

                }else
                {
              
                $adminaccount->CommitTransaction();

                    //log change of status
                $accountstatuslog->StartTransaction();
                $arrAccountstatuslog["AccountID"] = $acctid;
                $arrAccountstatuslog["Status"] = $new_status;
                $arrAccountstatuslog["Remarks"] = $remarks;
                $arrAccountstatuslog["TransDateTime"] = "now_usec()" ;
                $accountstatuslog->Insert($arrAccountstatuslog);
        
         if($accountstatuslog->HasError)
                {
                    $accountstatuslog->RollBackTransaction();
                }  else 
                    {
                    $accountstatuslog->CommitTransaction();


                        // -- insert in audit trail
                    $audittrail->StartTransaction();
                    $arrAudittrail["SessionID"] = $sessionID;
                    $arrAudittrail["AccountID"] = $accountid;
                    $arrAudittrail["TransDetails"] = "Update status of AccountID: $acctid " ;
                    $arrAudittrail["RemoteIP"] = $remoteip;
                    $arrAudittrail["TransDateTime"] = "now_usec()";
                    $audittrail->Insert($arrAudittrail);
                if($audittrail->HasError)
                {
                    $audittrail->RollBackTransaction();
                }else
                {
                    $audittrail->CommitTransaction();
  
                            // send email
                    $pm = new PHPMailer();
                    // $pm->AddAddress($_POST['txtEmail'], $_POST['fname']);
                    $pm->AddAddress("jfjose@philweb.com.ph", "jerome jose");
                    $pageURL = 'http';
                    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
                    $pageURL .= "://";
                    $folder = $_SERVER["REQUEST_URI"];
                    $folder = substr($folder,0,strrpos($folder,'/') + 1);
                    if ($_SERVER["SERVER_PORT"] != "80") 
                    {
                    $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
                    } 
                    else 
                    {
                    $pageURL .= $_SERVER["SERVER_NAME"].$folder;
                    }

                $pm->IsHTML(true);
                $pm->Body = "<HTML><BODY>
		                  Dear ".$_POST['fname'].",
		                  <br /><br />
		                  This is to inform you that your account status has been changed on this date $date_now and time $time_now 
		                  from ".$old_status." to $acct_status1.
		                  <br /><br />
		                  
		                      <!-- For inquiries on your account, please contact our toll free Customer Service hotline at XXXXXXX
		                      or email us at support@thesweepscenter.com. -->
							For further inquiries on the status of your account, please contact our Customer Service email at support.gu@philwebasiapacific.com.
		                      <br /><br />
		                      Regards,
		                      <!-- <br /><br />
		                      Customer Support -->
		                      <br />
		                      The Sweeps Center
		                    </BODY></HTML>";

		  
                    
                      
        $pm->From = "support.gu@philwebasiapacific.com";
        $pm->FromName = "The Sweeps Center";
        $pm->Host = "localhost";
        $pm->Subject = "NOTIFICATION FOR USER ACCOUNT CHANGE OF STATUS";
        $email_sent = $pm->Send();
                
       
              }
               }
           
       }
       
   }
  }
  }
    

    
 
}

    
?>
