<?php
/*
 * Added By : Jerome F. Jose
 * Added On : June 15, 2012
 * Purpose : Adding User Account
 */
require_once("include/core/init.inc.php");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadCore("PHPMailer.class.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_ref_AdminAccountTypes");
App::LoadModuleClass($modulename, "SCC_AdminAccounts");
APp::LoadModuleClass($modulename,"SCC_AdminAccountSessions");
App::LoadModuleClass($modulename,"SCC_AuditTrail");
$audittrail = new SCC_AuditTrail();
$adminaccountsession = new SCC_AdminAccountSessions();
$adminaccount = new SCC_AdminAccounts();
$ref_adminaccounttypes = new SCC_ref_AdminAccountTypes();

$fproc = new FormsProcessor();

$txtUName = new TextBox("txtUName","txtUName","Username");
$txtUName->Length = 12;
//$txtUName->ShowCaption = true;
//$txtUName->Args = "";

$txtPWord = new TextBox("txtPWord","txtPWord","Password");
$txtPWord->Length = 12;
$txtPWord->Password = true;
//$txtPWord->Args = "";

$txtCPWord = new TextBox("txtCPWord", "txtCPWord","Confirm Password");
$txtCPWord->Length = 12;
$txtCPWord->Password = true;
//$txtCPWord->Args = "onkeypress = ";
       
$txtFName = new TextBox("txtFName", "txtFName");
$txtFName->Length = 50;
$txtFName->Args = "onkeypress ='javascript : return numeric(event);'";
$txtFName->Style = "width:200px;";

$txtMName = new TextBox("txtMName","txtMName");
$txtMName->Length = 50;
$txtMName->Args = "onkeypress ='javascript : return numeric(event);'";
$txtMName->Style = "width:200px;";

$txtLName = new TextBox("txtLName","txtLName");
$txtLName->Length = 50;
$txtLName->Args = "onkeypress ='javascript : return numeric(event);'";
$txtLName->Style = "width:200px;";

$txtEmail = new TextBox("txtEmail", "txtEmail");
$txtEmail->Length = 50;
$txtEmail->Style = "width:200px;";
//$txtEmail->Args = "onkeypress = '' ";

$txtPosition = new TextBox("txtPosition", "txtPosition");
$txtPosition->Length = 50;
$txtPosition->Args = "onkeypress ='javascript : return numeric(event);'";
$txtPosition->Style = "width:200px;";

$txtCompany = new TextBox("txtCompany", "txtCompany");
$txtCompany->Length = 50;
$txtCompany->Args = "onkeypress = ''";
$txtCompany->Style = "width:200px;";

$txtDepartment = new TextBox("txtDepartment", "txtDepartment");
$txtDepartment->Length = 50;
$txtDepartment->Args = "onkeypress = ''";
$txtDepartment->Style = "width:200px;";


$ddlGroup = new ComboBox("ddlGroup", "ddlGroup");
$grouplist = Null;
$grouplist[] = new ListItem("Select Group", "0",true);
$ddlGroup->Items = $grouplist; 
$getall = $ref_adminaccounttypes->GetAccoutTypes();
$grouplist = new ArrayList();
$grouplist->AddArray($getall);
$ddlGroup->DataSource = $grouplist;
$ddlGroup->DataSourceText = "Name";
$ddlGroup->DataSourceValue = "ID";
$ddlGroup->DataBind();

$btnCancel = new Button("btnCancel", "btnCancel","Cancel");
$btnCancel->IsSubmit = true;
$btnCancel->CssClass = "labelbutton2";
$btnCancel->Args = "onclick='javascript: return resetadduseracct();'";

$btnSubmit = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Args ="onclick='javascript: return checkadduseracct();'";



$hidGroup = new Hidden("hidGroup","hidGroup");


$fproc->AddControl($txtUName);
$fproc->AddControl($txtPWord);
$fproc->AddControl($txtCPWord);
$fproc->AddControl($txtFName);
$fproc->AddControl($txtMName);
$fproc->AddControl($txtLName);
$fproc->AddControl($txtEmail);
$fproc->AddControl($txtPosition);
$fproc->AddControl($txtCompany);
$fproc->AddControl($txtDepartment);
$fproc->AddControl($ddlGroup);
$fproc->AddControl($hidGroup);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnCancel);


$fproc->ProcessForms();


   

if($fproc->IsPostBack)
{
            $returnid2 = 99;

                if($returnid2 == 0){
                    $_SESSION['title'] = "SUCCESSFUL ACCOUNT CREATION";
                }else
                    {
                    $_SESSION['title'] = "ERROR";
                    } 
            $sessionID = $_SESSION['sid'];

            $UName = $txtUName->SubmittedValue;
            $PWord = $txtPWord->SubmittedValue;
            $CPWord = $txtCPWord->SubmittedValue;
            $FName = $txtFName->SubmittedValue;
            $MName = $txtMName->SubmittedValue;
            $LName = $txtLName->SubmittedValue;
            $Email = $txtEmail->SubmittedValue;
            $Position = $txtPosition->SubmittedValue;
            $Company = $txtCompany->SubmittedValue;
            $Department = $txtDepartment->SubmittedValue;
            $Group = $ddlGroup->SubmittedValue;

    if(isset($_POST["btnSubmit"]))
        {
           
            $getsessionId = $adminaccountsession->GetSessionID($sessionID);
            if(count($getsessionId) > 0)
            {
                $getuname = $adminaccount->SelectUname($sessionID);
                $username = $getuname[0]['Username'];
                $remoteip = $getuname[0]['RemoteIP'];
            }else
            {
                $returnmsg = "User Session does not Exist;";
            }
    
       //check username exist
         
            $checkusername = $adminaccount->getUName($UName);

            if (count($checkusername) == '1')
                {
                    $returnmsg = "Username Already Exist;";

                }else
                {

                    $maxaccount = $adminaccount->MaxAccountID();
                    $lastID = $maxaccount[0]['LastID'];  
                    $lastID = ($lastID == null) ? 0 : $lastID. " " +2 ;

                    $accountname ="".$FName." ".$MName." ".$LName." ";
                    $adminaccount->StartTransaction();
                    $arradminacc['AccountId'] = $lastID;
                    $arradminacc['Username'] = $UName;
                    $arradminacc['Password'] = MD5($PWord);
                    $arradminacc['Status'] = '1';
                    $arradminacc['FirstName'] = $FName;
                    $arradminacc['MiddleName'] = $MName;
                    $arradminacc['LastName'] = $LName;
                    $arradminacc['Email'] = $Email;
                    $arradminacc['Position'] = $Position;
                    $arradminacc['Department'] = $Department;
                    $arradminacc['Company'] = $Company;
                    $arradminacc['AccountType'] = $Group;
                    $arradminacc['RecCreOn'] = "now_usec()";
                    $arradminacc['RecCreBy'] = $username;
                    $adminaccount->Insert($arradminacc);     
                    if($adminaccount->HasError)
                    {
                        $returnmsg = $adminaccount->getError();
                        $adminaccount->RollBackTransaction();

                    }else
                    {
                    $adminaccount->CommitTransaction();
                    $audittrail->StartTransaction();
                    $auditarr['SessionID'] = $sessionID;
                    $auditarr['AccountID'] = $lastID;
                    $auditarr['TransDetails'] = "New Account : ".$UName." ";
                    $auditarr['RemoteIP'] =  $remoteip;
                    $auditarr['TransDateTime'] =  "now_usec()";
                    $audittrail->Insert($auditarr);
                    if($audittrail->HasError)
                    {
                        $returnmsg = $audittrail->getError();
                        $audittrail->RollBackTransaction();
                    }else
                    {
                        $audittrail->CommitTransaction();
                        $date = date('m/d/Y');
                        $time = date('h:i:s');
                    // add emailing
                        $pm = new PHPMailer();
                        $pm->AddAddress($Email,$FName);
                        $pageURL = 'http';
                        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
                        $pageURL .= "://";
                        $folder = $_SERVER["REQUEST_URI"];
                        $folder = substr($folder,0,strrpos($folder,'/') + 1);
                        if ($_SERVER["SERVER_PORT"] != "80") 
                        {
                        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
                        } 
                        else 
                        {
                        $pageURL .= $_SERVER["SERVER_NAME"].$folder;
                        }
	
                    $pm->Body = "<HTML><BODY>						
                                        Dear $accountname,
				  <br /><br />
				  This is to inform you that your User Account has been successfully created on this date $date and time $time.
				  Here are your Account Details:
					<br /><br />
					Username: <b>$username</b><br />
					Password: <b>password</b>	
			      <br /><br />
			      For security purposes, please change this assigned password upon log in at $pageURL.																		
				  <br /><br />
				  For inquiries on your account, please contact our toll free Customer Service hotline at XXXXXXX
				  or email us at emailaddress@email.com.
				  <br /><br />
				  Regards,
				  <br /><br />
				  The Sweeps Center Team
				  <br />
				  Philweb Guam
				</BODY></HTML>";
                                $pm->From = "no-reply@ticketmanagementsystem.com";
                                $pm->FromName = "test_guam_email_address";
                                $pm->Host = "localhost";
                                $pm->Subject = "Test New User Account For Guam  Management Tool";
                                $email_sent = $pm->Send();
                                if($email_sent)
                                {
                                    $returnmsg = "New user account has been successfully created. Log in credentials have been sent to the registered e-mail address.";
                                    $returnid2 = 0;
                                }
                                else
                                {
                                    $returnmsg = "An error occurred while sending the email to your email address";
                                }
        
                    }
                }
                
            }
            
            
     }
}
?>
