<?php
/*
 * Added By : Jerome F. Jose
 * Added On : June 21, 2012
 * Purpose : Change Password Admin
 */
require_once("include/core/init.inc.php");

App::LoadModuleClass("SweepsCenter","SCC_AdminAccounts");
App::LoadModuleClass("SweepsCenter","SCC_AuditTrail");
App::LoadModuleClass("SweepsCenter","SCC_AdminAccountSessions");

APP::LoadControl("TextBox");
App::LoadControl("Button");

$fproc = new FormsProcessor();
$adminaccounts = new SCC_AdminAccounts();
$audittrail = new SCC_AuditTrail();
$adminaccountsession = new SCC_AdminAccountSessions();
$txtPassword = new TextBox("txtPassword","txtPassword");
$txtPassword->Style = "width:200px;";
$txtPassword->Password = true;
$txtPassword->Length = 12;

$txtNewPassword = new TextBox("txtNewPassword", "txtNewPassword");
$txtNewPassword->Style = "width:200px;";
$txtNewPassword->Password = true;
$txtNewPassword->Length = 12;

$txtConfPassword = new TextBox("txtConfPassword", "txtConfPassword");
$txtConfPassword->Style = "width:200px;";
$txtConfPassword->Password = true;
$txtConfPassword->Length = 12;

$btnOk = new Button("btnOK","btnOK","SUBMIT");
$btnOk->CssClass = "labelbutton2";
$btnOk->IsSubmit = true;
$btnOk->Args = "onclick='javascript: return checkchangepassword();' ";

$passSubmit = new Button("passSubmit","passSubmit", "OK");
$passSubmit->CssClass = "labelbutton2";
$passSubmit->Args = "onclick=document.getElementById('light11').style.display='none';document.getElementById('fade').style.display='none';";
$passSubmit->IsSubmit = true;

$fproc->AddControl($txtPassword);
$fproc->AddControl($txtNewPassword);
$fproc->AddControl($txtConfPassword);
$fproc->AddControl($btnOk);
$fproc->AddControl($passSubmit);


$fproc->ProcessForms();
    $isprocessed = -1;
 
if($fproc->IsPostBack)
{
$sessionID = $_SESSION['sid'];

  
   if($passSubmit->SubmittedValue == "OK")
   {
   
      
           $_SESSION['acctpword'] = $txtNewPassword->SubmittedValue;
           $_SESSION['newpw'] = $txtNewPassword->SubmittedValue;
           $_SESSION['oldpw'] = $txtPassword->SubmittedValue;
           $oldpass = $txtPassword->SubmittedValue;
           $newpass = $txtNewPassword->SubmittedValue;
           
           //change password
        
            $getsessionId = $adminaccountsession->GetSessionID($sessionID);
            if(count($getsessionId) == 0)
            {
                 $returnmsg = "User Session does not Exist;";
               
            }else
            {
                 
               $getdetail = $adminaccounts->getCashierDetails($sessionID);
              
                $AcctID = $getdetail[0]['AccountID'];
                $Email = $getdetail[0]['Email'];
                $password = $getdetail[0]['Password'];// current pass
                $username = $getdetail[0]['Username'];
                $remoteip = $getdetail[0]['RemoteIP'];
                    
   
//                    -- update accounts table
                  $adminaccounts->StartTransaction();
                    $updatePass = $adminaccounts->UpdateChangePassword($newpass, $username, $AcctID);
                    if($adminaccounts->HasError)
                    {
                        
                    }else
                    {
                        $adminaccounts->CommitTransaction();
                        
//                        -- insert in audit trail
                        $audittrail->StartTransaction();
                        $arrAudittrail['SessionID'] = $sessionID;
                        $arrAudittrail['AccountID'] = $AcctID;
                        $arrAudittrail['TransDetails'] = "Change password for Admin AcctID :".$AcctID." ";
                        $arrAudittrail['RemoteIP'] = $remoteip;
                        $arrAudittrail['TransDateTime'] = "now_usec()";
                        $audittrail->Insert($arrAudittrail);
                        if($audittrail->HasError)
                        {
                            $returnmsg = " Error inserting in audit trail";
                        }  else {
                            $audittrail->CommitTransaction();
                            $returnmsg = "You have successfully changed your password. An email notification was sent to the user account. ";
                                }

                       }
                  }
           }
}

?>
