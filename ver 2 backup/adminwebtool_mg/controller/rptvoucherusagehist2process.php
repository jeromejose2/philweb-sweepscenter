<?php

/*
 * Added On : June 25, 2012
 * Created by : JFJ
 */
require_once("include/core/init.inc.php");

$modulename = "SweepsCenter";

App::LoadModuleClass($modulename,"SCC_Accounts");
App::LoadModuleClass($modulename, "SCCV_VoucherInfo");

APP::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("PagingControl2");



$fproc = new FormsProcessor();
$accounts = new SCC_Accounts();
$voucherinfo = new SCCV_VoucherInfo();

$txtDateFr = new TextBox("txtDateFr","txtDateFr");
$txtDateFr->Length = "10";
$txtDateFr->Style = "text-align:center";
$txtDateFr->ReadOnly = true;
$txtDateFr->Text = date("m/d/Y");

$txtDateTo = new TextBox("txtDateTo", "txtDateTo");
$txtDateTo->Length = "10";
$txtDateTo->Style = "text-align:center";
$txtDateTo->ReadOnly = true;
$txtDateTo->Text = date("m/d/Y");

$btnSearch = new Button("btnSearch", "btnSearch", "Search");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checkvoucherinput();'  ";

$getlist = $accounts->SelectSiteIDbylist();
$ddlPos = new ComboBox("ddlPos","ddlPos");
$Poslist = new ArrayList();
$Poslist->AddArray($getlist);
$ddlPos->ClearItems();
$litem = null;
$litem[] = new ListItem("---","",true);
$litem[] = new ListItem("ALL","0",true);
$ddlPos->Items = $litem;
$ddlPos->DataSource = $Poslist;
$ddlPos->DataSourceText = "Name";
$ddlPos->DataSourceValue = "SiteID";
$ddlPos->DataBind();

$vchrusage = new ComboBox("vchrusage", "vchrusage");
$litems = null;
$litems[] = new ListItem("ALL","0",TRUE);
$litems[] = new ListItem("Unused","4");
$litems[] = new ListItem("Used","2");
$litems[] = new ListItem("Expired","3");
$litems[] = new ListItem("Cancelled","5");
$vchrusage->Items = $litems;

$vchrcode = new TextBox("vchrcode","vchrcode");

$itemsperpage = 20;
$pgcon = new PagingControl2($itemsperpage,1);
$pgcon->URL ="javascript:ChangePage(%currentpage)";
$pgcon->PageGroup = 5;


$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSearch);
$fproc->AddControl($ddlPos);
$fproc->AddControl($vchrusage);
$fproc->AddControl($vchrcode);


$fproc->ProcessForms();

if($fproc->IsPostBack)
{
  if($btnSearch->SubmittedValue == "Search")
  {
      $pgcon->SelectedPage = 1;
  }
      
	$dDateFr = $txtDateFr->SubmittedValue;
	$dDateTo = $txtDateTo->SubmittedValue;
	$sPosAcct = $ddlPos->SubmittedValue;
	$sVchrCode = $vchrcode->SubmittedValue;
	$sVchrUsage = $vchrusage->SubmittedValue;
        
	$_SESSION['start'] = $dDateFr;
	$_SESSION['end'] = $dDateTo;	
	$_SESSION['posacct'] = $sPosAcct;
	$_SESSION['vchrcode'] = $sVchrCode;
	$_SESSION['vchrusage'] = $sVchrUsage;
        
        $ds = new DateSelector($dDateTo);
        $date2 = $ds->NextDate;
        $dDateTo = $date2;
        $dDateTo = date_create($dDateTo);
        $dDateTo = date_format($dDateTo, "Y-m-d 10:00:00.00000");
        $dDateFr = date_create($dDateFr);
        $dDateFr = date_format($dDateFr, "Y-m-d 09:59:59.99999");
                      
        $wherelimit = " LIMIT ".($pgcon->SelectedItemFrom-1).",".$itemsperpage." ;";
  if($sVchrCode != "")
        {
            $getvoucher1 = $voucherinfo->getvouchernumber($sVchrCode);
            $pgcon->Initialize($itemsperpage,count($getvoucher1));
            $pgHist = $pgcon->PreRender();
            $getvoucher = $voucherinfo->getvouchernumberwithlimit($sVchrCode, $wherelimit);
        }elseif($sVchrUsage == "5")
        {
            $getvoucher1 = $voucherinfo->getcancelledvoucher($dDateFr, $dDateTo);
            $pgcon->Initialize($itemsperpage,count($getvoucher1));
            $pgHist = $pgcon->PreRender();
            $getvoucher = $voucherinfo->getcancelledvoucherwithlimit($dDateFr, $dDateTo, $wherelimit);
        }elseif ($sVchrUsage == "4") 
        {
            $getvoucher1 = $voucherinfo->getUnusedvoucher($dDateFr, $dDateTo);
            $pgcon->Initialize($itemsperpage,count($getvoucher1));
            $pgHist = $pgcon->PreRender();
            $getvoucher = $voucherinfo->getUnusedvoucherwithlimit($dDateFr, $dDateTo, $wherelimit);

        }elseif($sVchrUsage == "3") 
        {
            $getvoucher1 = $voucherinfo->getexpiredvoucher($dDateFr, $dDateTo);
            $pgcon->Initialize($itemsperpage,count($getvoucher1));
            $pgHist = $pgcon->PreRender();
            $getvoucher = $voucherinfo->getexpiredvoucherwithlimit($dDateFr, $dDateTo, $wherelimit);

        }elseif($sVchrUsage == "2")
        {
                $getvoucher1 = $voucherinfo->getusedvoucher($dDateFr, $dDateTo);
                $pgcon->Initialize($itemsperpage,count($getvoucher1));
                $pgHist = $pgcon->PreRender();
                $getvoucher = $voucherinfo->getusedvoucherwithlimit($dDateFr, $dDateTo, $wherelimit);
        }elseif($sVchrUsage == "0") 
        {

                $getvoucher1 = $voucherinfo->getalldetailsvoucher($dDateFr, $dDateTo);
                $pgcon->Initialize($itemsperpage,count($getvoucher1));
                $pgHist = $pgcon->PreRender();
                $getvoucher = $voucherinfo->getalldetailsvoucherwithlimit($dDateFr, $dDateTo, $wherelimit);
        }
                       
  
  
}



?>
