<?php
/*
 * Created By   :   Noel Antonio
 * Date Created :   June 22, 2012
 * Purpose      :   Update Terminal Password
 */

include("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_Terminals");
App::LoadModuleClass($modulename, "SCCSM_AgentSessions");
App::LoadModuleClass($modulename, "SCC_AuditTrail");

App::LoadLibrary("MicrogamingAPI.class.php");
App::LoadCore("PHPMailer.class.php");

$cterminals = new SCC_Terminals();
$csmagentsessions = new SCCSM_AgentSessions();
$caudittrail = new SCC_AuditTrail();

$terminal_account = $_POST['tacct'];
$fname = $_POST['fname'];

$arrTerminal = $cterminals->GetDetailsByTerminalID($terminal_account);
if (count($arrTerminal) > 0)
{
    $email = $arrTerminal[0][1];
    $un = $arrTerminal[0][2];
    $isFE = $arrTerminal[0][3];
}
$pw = substr(rand(),0,7);

if($isFE != 1) 
{
    $arrSessionGUID_MG = $csmagentsessions->SelectSessionGUID();
    $sessionGUID_MG = $arrSessionGUID_MG[0]['SessionGUID'];
    $ip_add_MG = $_SERVER['REMOTE_ADDR'];
    $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
               "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
               "<IPAddress>".$ip_add_MG."</IPAddress>".
               "<ErrorCode>0</ErrorCode>".
               "<IsLengthenSession>true</IsLengthenSession>".
               "</AgentSession>";
    
    $client = new nusoap_client("https://entservices.totalegame.net/EntServices.asmx?WSDL", 'wsdl');
    $client->setHeaders($headers);
    $param = array('accountNumber' => $un, 'firstName' => '', 'lastName' => '','password' => $pw,'isMobile' => false, 'mobileNumber' => '', 'isSendGame' => false);
    $result = $client->call('EditAccount',$param);
    $err_result = $result['EditAccountResult']['IsSucceed'];
    if($err_result == 'false')
    {
        echo "MG APi Error";
    }
}

// Update Terminal Password
$cterminals->StartTransaction();
$updterminal["ID"] = MD5($terminal_account);
$updterminal["Password"] = MD5($pw);
$cterminals->UpdateByArray($updterminal);
if ($cterminals->HasError)
{
    echo "Error updating terminal password.";
    $cterminals->RollBackTransaction();
}
else
{
    $cterminals->CommitTransaction();

    // log to audit trail
    $caudittrail->StartTransaction();
    $scauditlogparam["SessionID"] = $_SESSION['sid'];
    $scauditlogparam["AccountID"] = $_SESSION['aid'];
    $scauditlogparam["TransDetails"] = "Change Terminal Account Password : Terminal ID - " . $terminal_account;
    $scauditlogparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
    $scauditlogparam["TransDateTime"] = "now_usec()";
    $caudittrail->Insert($scauditlogparam);
    if ($caudittrail->HasError)
    {
        echo "Error inserting to audit trail";
        $caudittrail->RollBackTransaction();
    }
    else
    {
        $caudittrail->CommitTransaction();
        
        // Sending of Email
        $pm = new PHPMailer();
        $pm->AddAddress($email, $fname);

        $pageURL = 'http';
        if (!empty($_SERVER['HTTPS'])) {$pageURL .= "s";}
        $pageURL .= "://";
        $folder = $_SERVER["REQUEST_URI"];
        $folder = substr($folder,0,strrpos($folder,'/') + 1);
        if ($_SERVER["SERVER_PORT"] != "80") 
        {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
        } 
        else 
        {
        $pageURL .= $_SERVER["SERVER_NAME"].$folder;
        }

        $pm->IsHTML(true);

        $pm->Body = "Dear " . $fname . "<br/><br/>
            This is to inform you that your password has been changed on this date ".date("m/d/Y")." and time ".date("H:i:s").". 
            Here are your new Account Details:<br /><br />".
            "Username: <b>$un</b><br />
            Password: <b>$pw</b><br /><br />".
        "If you didn't perform this procedure, please report this to our Customer Service email at support.gu@philwebasiapacific.com.<br /><br />".
        "Regards,<br/><br/>Customer Support<br/><br/>The Sweeps Center";

        $pm->From = "support.gu@philwebasiapacific.com";
        $pm->FromName = "The Sweeps Center";
        $pm->Host = "localhost";
        $pm->Subject = "NOTIFICATION FOR NEW PASSWORD OF TERMINAL ACCOUNT";
        $email_sent = $pm->Send();
        if(!$email_sent)
        {               
            echo "An error occurred while sending the email to your email address";
        }
        else
        {
            echo "Terminal password successfully changed!";
        }
    }
}
?>