<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 21, 2012
 * Purpose          :       Add Terminal Account Controller Page.
 */

require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_AccountDetails");
App::LoadModuleClass($modulename, "SCC_Accounts");
App::LoadModuleClass($modulename, "SCCSM_AgentSessions");
App::LoadModuleClass($modulename, "SCC_Terminals");

App::LoadLibrary("MicrogamingAPI.class.php");

App::LoadCore("PHPMailer.class.php");
App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("CheckBox");

$fproc = new FormsProcessor();
$caccts = new SCC_Accounts();
$cacctdtls = new SCC_AccountDetails();
$csmagentsessions = new SCCSM_AgentSessions();
$cterminals = new SCC_Terminals();

$ddlacctname = new ComboBox("ddlacctname", "ddlacctname", "Account Name");
$ddlacctname->Args = "onchange='javascript: return SelectPOSAccountNumber()'";
$opt1 = null;
$opt1[] = new ListItem("Select One", "Select One", true);
$ddlacctname->Items = $opt1;
$posaccts = $cacctdtls->LoadPOSAccounts("Select One");
$list_posaccts = new ArrayList();
$list_posaccts->AddArray($posaccts);
$ddlacctname->DataSource = $list_posaccts;
$ddlacctname->DataSourceValue = "AccountID";
$ddlacctname->DataSourceText = "FullName";
$ddlacctname->DataBind();

$ddlacctno = new ComboBox("ddlacctno", "ddlacctno", "Account Number");
$ddlacctno->Args = "onchange='javascript: return SelectPOSAccountName()'";
$opt2 = null;
$opt2[] = new ListItem("Select One", "Select One", true);
$ddlacctno->Items = $opt2;
$posacctsid = $cacctdtls->LoadPOSAccountID();
$list_posacctsid = new ArrayList();
$list_posacctsid->AddArray($posacctsid);
$ddlacctno->DataSource = $list_posacctsid;
$ddlacctno->DataSourceValue = "AccountID";
$ddlacctno->DataSourceText = "AccountID";
$ddlacctno->DataBind();

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Search");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->IsSubmit = true;

$txtnumofterminals = new TextBox("txtnumofterminals", "txtnumofterminals", "Number of Terminals");
$txtnumofterminals->Args = "size='1px'";
$txtnumofterminals->Text = 1;
$txtnumofterminals->ReadOnly = true;

$ddlterminaltype = new ComboBox("ddlterminaltype", "ddlterminaltype", "Terminal Type");
$opt = null;
$opt[] = new ListItem("Free Entry Terminal", "free_entry", true);
$opt[] = new ListItem("Sweeps Gaming Terminal", "sgt");
$ddlterminaltype->Items = $opt;

$fproc->AddControl($ddlacctname);
$fproc->AddControl($ddlacctno);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($txtnumofterminals);
$fproc->AddControl($ddlterminaltype);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSubmit->SubmittedValue == "Search")
    {
        $terminalacct = $caccts->GetTerminalAccountDetails($ddlacctname->SubmittedValue);
        $list_terminals = new ArrayList();
        $list_terminals->AddArray($terminalacct);
        
        if (count($list_terminals) > 0)
        {
            $acct_name = $list_terminals[0]["LastName"] . ", " . $list_terminals[0]["FirstName"] . " " . $list_terminals[0]["MiddleName"];
            $acct_id = $list_terminals[0]["AccountID"];
            $swccode = $list_terminals[0]["SWCCode"];
            $email = $list_terminals[0]['Email'];
            $terminal_count = $list_terminals[0]["TerminalCount"];
            $status = $list_terminals[0]['Status'];
        }
    }

    if ($fproc->GetPostVar('btnSubmitOkay') == 'OKAY')
    { 
        $arrsessionguid = $csmagentsessions->SelectSessionGUID();
        $sessionGUID_MG = $arrsessionguid[0]["SessionGUID"];
        
        $termtypedtls = $caccts->GetTerminalType($ddlacctname->SubmittedValue, $ddlterminaltype->SubmittedValue);
        if (count($termtypedtls) == 1)
        {
            $swc_code = $termtypedtls[0]['SWCCode'];
            $curr_count = $termtypedtls[0]['Count'];
            $site_id = $termtypedtls[0]['SiteID'];
        }
        
        $i = 1;
        $tname = $curr_count + $i;
        
        $defaultPass = '1234567';
        $encrpytedPass = md5($defaultPass);
        $ip_add_MG = $_SERVER['REMOTE_ADDR'];
        $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
                   "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
                   "<IPAddress>".$ip_add_MG."</IPAddress>".
                   "<ErrorCode>0</ErrorCode>".
                   "<IsLengthenSession>true</IsLengthenSession>".
                   "</AgentSession>";

        $client = new nusoap_client("https://entservices.totalegame.net/EntServices.asmx?WSDL", 'wsdl');
        $client->setHeaders($headers);
        
        if ($ddlterminaltype->SubmittedValue == "sgt")
        {
            $terminal = "SWC".$swc_code.$tname;
            $param = array('accountNumber' => $terminal, 'isGeneratePassword' => false, 'password' => $defaultPass,'nickName' => $terminal,'currency' => 1, 'bettingProfileId' => -1);
            $result = $client->call('AddStationAccount',$param);
            $err_result = $result['AddStationAccountResult']['IsSucceed'];
            if($err_result == 'true')
            {
                $isFreeEntry = 0;
                $wtermtype = "Sweeps Gaming Terminal";
            }
            else
            {
                $errormsg = "MG API error exist!";
                $errormsgtitle = "ERROR!";
            }
        }
        else
        {
            $terminal = "SWC".$swc_code."FE".$tname;
            $isFreeEntry = 1;
            $wtermtype = "Free Entry Terminal";
        }
        
        // insert to terminals
        $cterminals->StartTransaction();
        $insterminal["Name"] = $terminal;
        $insterminal["Password"] = $encrpytedPass;
        $insterminal["SiteID"] = $site_id;
        $insterminal["IsFreeEntry"] = $isFreeEntry;
        $insterminal["Status"] = 1;
        $cterminals->Insert($insterminal);
        if ($cterminals->HasError)
        {
            $errormsg = $cterminals->getErrors();
            $errormsgtitle = "ERROR!";
            $cterminals->RollBackTransaction();
        }
        else
        {
            $cterminals->CommitTransaction();
            
            // Sending of Email
            $pm = new PHPMailer();
            $pm->AddAddress($fproc->GetPostVar('txtEmail'), $fproc->GetPostVar('fname'));

            $pageURL = 'http';
            if (!empty($_SERVER['HTTPS'])) {$pageURL .= "s";}
            $pageURL .= "://";
            $folder = $_SERVER["REQUEST_URI"];
            $folder = substr($folder,0,strrpos($folder,'/') + 1);
            if ($_SERVER["SERVER_PORT"] != "80") 
            {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
            } 
            else 
            {
            $pageURL .= $_SERVER["SERVER_NAME"].$folder;
            }

            $pm->IsHTML(true);

            $pm->Body = "Dear " . $fproc->GetPostVar('fname') . "<br/><br/>
                This is to inform you that a " . $wtermtype . " have been created under your account on this date ".date("m/d/Y")." and time ".date("H:i:s").".<br /><br />
                Below are the terminal account log in credentials: <br /><br /> " ."
                Username: <b>".$terminal."</b><br/>
                Password: <b>".$defaultPass."</b><br/><br/>".
            "For further inquiries on your account, please contact our Customer Service email at support.gu@philwebasiapacific.com.<br /><br />".
            "Regards,<br /><br />The Sweeps Center Management Team";

            $pm->From = "operations@thesweepscenter.com";
            $pm->FromName = "The Sweeps Center";
            $pm->Host = "localhost";
            $pm->Subject = "NOTIFICATION FOR NEW TERMINAL ACCOUNTS";
            $email_sent = $pm->Send();
            if(!$email_sent)
            {               
                $errormsg = "An error occurred while sending the email to your email address";
                $errormsgtitle = "ERROR!";
            }
            else
            {
                $errormsg = "A " . $wtermtype . " account was successfully created. An e-mail notification is sent to the sweeps center pos account for terminal details.";
                $errormsgtitle = "Notification!";
            }
        }
    }
}
?>
