<?php
/* 
 * Created by       :       Noel Antonio
 * Date Created     :       June 21, 2012
 * Purpose          :       Get Terminals
 */

$acctid = $_POST['ddlacctname'];

include("../init.inc.php");
App::LoadModuleClass("SweepsCenter", "SCC_Accounts");
$caccts = new SCC_Accounts();

$arrTerminals = $caccts->LoadPOSTerminals($acctid);
$data = array();

for ($i = 0; $i < count($arrTerminals); $i++)
{
    $data[$arrTerminals[$i]["ID"]] = $arrTerminals[$i]["Name"];
}

echo json_encode($data);
?>


