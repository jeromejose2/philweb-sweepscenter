<?php
/*
 * Created By       :       Noel D. Antonio
 * Date Created     :       June 18, 2012
 * Purpose          :       POS Account Update Profile Controller: Updating of POS Account
 */
require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCC_AccountDetails");
App::LoadModuleClass($modulename, "SCC_AccountBankDetails");

App::LoadControl("TextBox");
App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("CheckBox");

$fproc = new FormsProcessor();
$cacctdtls = new SCC_AccountDetails();
$cacctbankdtls = new SCC_AccountBankDetails();

/* -- LOAD POS ACCOUNT RECORDS -------------------------------------------------*/
$_SESSION['pos_acctid'] = $_POST['hiddenid'];
$arrposdtls = $cacctdtls->GetPOSAccountDetails($_SESSION['pos_acctid']);
$list_posdtls = new ArrayList();
$list_posdtls->AddArray($arrposdtls);

if (count($list_posdtls) > 0)
{
    $acct_name = $list_posdtls[0]["LastName"] . ", " . $list_posdtls[0]["FirstName"] . " " . $list_posdtls[0]["MiddleName"];
    $acct_id = $list_posdtls[0]["AccountID"];
    $uname = $list_posdtls[0]["Username"];
    $lname = $list_posdtls[0]["LastName"];
    $fname = $list_posdtls[0]["FirstName"];
    $mname = $list_posdtls[0]["MiddleName"];
    $swc_code = $list_posdtls[0]["SWCCode"];
    $home_addr = $list_posdtls[0]["HomeAddress"];
    $zip = $list_posdtls[0]["ZipCode"];
    $tel_no = $list_posdtls[0]["TelephoneNumber"];
    $mob_no = $list_posdtls[0]["MobileNumber"];
    $fax = $list_posdtls[0]["Fax"];
    $email = $list_posdtls[0]["Email"];
    $website = $list_posdtls[0]["Website"];
    $country = $list_posdtls[0]["CountryID"];
    $citizen = $list_posdtls[0]["Citizenship"];
    $bday = $list_posdtls[0]["BirthDate"];
    $pass = $list_posdtls[0]["Passport"];
    $d_lic = $list_posdtls[0]["DriversLicense"];
    $sss = $list_posdtls[0]["SocialSecurity"];
    $others = $list_posdtls[0]["Others"];
    $bank = $list_posdtls[0]["Bank"];
    $bank_type = $list_posdtls[0]["BankType"];
    $bank_bnch = $list_posdtls[0]["BankBranch"];
    $bank_acct_nme = $list_posdtls[0]["BankAccountName"];
    $bank_acct_num = $list_posdtls[0]["BankAccountNumber"];
    $payee_bank = $list_posdtls[0]["PayeeBank"];
    $payee_bank_type = $list_posdtls[0]["PayeeBankType"];
    $payee_bank_branch = $list_posdtls[0]["PayeeBankBranch"];
    $payee_bank_acct_nme = $list_posdtls[0]["PayeeBankAccountName"];
    $payee_bank_acct_num = $list_posdtls[0]["PayeeBankAccountNumber"];
    $status = $list_posdtls[0]['Stat'];
    $siteID = $list_posdtls[0]['SI'];
}
else
{
    $acct_name = "";$acct_id = "";$uname = "";$lname = "";$fname = "";$mname = "";$swc_code = "";
    $home_addr = "";$zip = "";$tel_no = "";$mob_no = "";$fax = "";$email = "";$website = "";$country = "";
    $citizen = "";$bday = "";$pass = "";$d_lic = "";$sss = "";$others = "";$bank = "";$bank_type = "";
    $bank_bnch = "";$bank_acct_nme = "";$bank_acct_num = "";$payee_bank = "";$payee_bank_type = "";
    $payee_bank_branch = "";$payee_bank_acct_nme = "";$payee_bank_acct_num = "";$status = "";$siteID = "";
}
/* -- LOAD POS ACCOUNT RECORDS -------------------------------------------------*/

$ddlacctname = new ComboBox("ddlacctname", "ddlacctname", "Account Name");
$ddlacctname->Args = "onchange='javascript: return SelectPOSAccountNumber()'";
$opt1 = null;
$opt1[] = new ListItem("Select One", "Select One", true);
$ddlacctname->Items = $opt1;
$posaccts = $cacctdtls->LoadPOSAccounts("Select One");
$list_posaccts = new ArrayList();
$list_posaccts->AddArray($posaccts);
$ddlacctname->DataSource = $list_posaccts;
$ddlacctname->DataSourceValue = "AccountID";
$ddlacctname->DataSourceText = "FullName";
$ddlacctname->DataBind();
$ddlacctname->SetSelectedValue($_SESSION['pos_acctid']);

$ddlacctno = new ComboBox("ddlacctno", "ddlacctno", "Account Number");
$ddlacctno->Args = "onchange='javascript: return SelectPOSAccountName()'";
$opt2 = null;
$opt2[] = new ListItem("Select One", "Select One", true);
$ddlacctno->Items = $opt2;
$posacctsid = $cacctdtls->LoadPOSAccountID();
$list_posacctsid = new ArrayList();
$list_posacctsid->AddArray($posacctsid);
$ddlacctno->DataSource = $list_posacctsid;
$ddlacctno->DataSourceValue = "AccountID";
$ddlacctno->DataSourceText = "AccountID";
$ddlacctno->DataBind();
$ddlacctno->SetSelectedValue($_SESSION['pos_acctid']);

$btnSearch = new Button("btnSearch", "btnSearch", "Search");
$btnSearch->CssClass = "labelbutton2";
$btnSearch->IsSubmit = true;

$btnSave = new Button("btnSave", "btnSave", "Save");
$btnSave->CssClass = "labelbutton2";
$btnSave->IsSubmit  = true;

$btnBack = new Button("btnBack", "btnBack", "Back");
$btnBack->CssClass = "labelbutton2";
$btnBack->Style = "margin-left: 20px;";

$hiddenid = new Hidden("hiddenid", "hiddenid", "Hidden Account ID");
$hiddenid->Text = $acct_id;

$hidden = new Hidden("hidden", "hidden");

// --
$txtuname = new TextBox("txtuname", "txtuname", "Cashier Username");
$txtuname->Length = 15;
$txtuname->Text = $uname;
$txtuname->ReadOnly = true;

$txtscode = new TextBox("txtscode", "txtscode", "SWC Code");
$txtscode->Length = 3;
$txtscode->Text = $swc_code;
$txtscode->ReadOnly = true;

$txthadd = new TextBox("txthadd", "txthadd", "Home Address");
$txthadd->Multiline = true;
$txthadd->Style = "margin-left: 30px; width: 400px;";
$txthadd->Text = $home_addr;

$txtfname = new TextBox("txtfname", "txtfname", "First Name");
$txtfname->Text = $fname;
$txtfname->ReadOnly = true;

$txtmname = new TextBox("txtmname", "txtmname", "Middle Name");
$txtfname->Text = $mname;
$txtmname->ReadOnly = true;

$txtlname = new TextBox("txtlname", "txtlname", "Last Name");
$txtlname->Text = $lname;
$txtlname->ReadOnly = true;

$txtzip = new TextBox("txtzip", "txtzip", "Zip Code");
$txtzip->Text = $zip;

$txttelno = new TextBox("txttelno", "txttelno", "Telephone Number");
$txttelno->Text = $tel_no;

$txtmobno = new TextBox("txtmobno", "txtmobno", "Mobile Number");
$txtmobno->Text = $mob_no;

$txtfax = new TextBox("txtfax", "txtfax", "Fax");
$txtfax->Text = $fax;

$txtemail = new TextBox("txtemail", "txtemail", "Email Address");
$txtemail->Text = $email;

$txtwebadd = new TextBox("txtwebadd", "txtwebadd", "Website Address");
$txtwebadd->Text = $website;

$txtcitizen = new TextBox("txtcitizen", "txtcitizen", "Citizen");
$txtcitizen->Text = $citizen;

$txtpassport =  new TextBox("txtpassport", "txtpassport", "Passport");
$txtpassport->Text = $pass;

$txtdriver =  new TextBox("txtdriver", "txtdriver", "Driver");
$txtdriver->Text = $d_lic;

$txtsss =  new TextBox("txtsss", "txtsss", "SSS");
$txtsss->Text = $sss;

$txtothers =  new TextBox("txtothers", "txtothers", "Others");
$txtothers->Text = $others;

$txtbankbranch = new TextBox("txtbankbranch", "txtbankbranch", "Bank Branch");
$txtbankbranch->Text = $bank_bnch;

$txtbankacctname = new TextBox("txtbankacctname", "txtbankacctname", "Bank Account Name");
$txtbankacctname->Text = $bank_acct_nme;

$txtbankacctnum = new TextBox("txtbankacctnum", "txtbankacctnum", "Bank Account Number");
$txtbankacctnum->Text = $bank_acct_num;

$txtpayeebankbranch = new TextBox("txtpayeebankbranch", "txtpayeebankbranch", "Payee Bank Branch");
$txtpayeebankbranch->Text = $payee_bank_branch;

$txtpayeebankacctname = new TextBox("txtpayeebankacctname", "txtpayeebankacctname", "Payee Bank Account Name");
$txtpayeebankacctname->Text = $payee_bank_acct_nme;

$txtpayeebankacctnum = new TextBox("txtpayeebankacctnum", "txtpayeebankacctnum", "Payee Bank Account Number");
$txtpayeebankacctnum->Text = $payee_bank_acct_num;

$txtbday = new TextBox("txtbday", "txtbday", "Birthday");
$txtbday->Text = $bday;

$ddlcountry = new ComboBox("ddlcountry", "ddlcountry", "Country");
$opt = null;
$opt[] = new ListItem($country, $country, true);
$opt[] = new ListItem("Guam", "Guam");
$ddlcountry->Items = $opt;

$ddlbank = new ComboBox("ddlbank", "ddlbank", "Bank");
$opt1 = null;
$opt1[] = new ListItem($bank, $bank, true);
$opt1[] = new ListItem("Banco Filipino", "Banco Filipino");
$ddlbank->Items = $opt1;

$ddlbanktype = new ComboBox("ddlbanktype", "ddlbanktype", "Bank Type");
$opt2 = null;
$opt2[] = new ListItem($bank_type, $bank_type, true);
$opt2[] = new ListItem("", "");
$ddlbanktype->Items = $opt2;

$ddlpayeebank = new ComboBox("ddlpayeebank", "ddlpayeebank", "Payee Bank");
$opt3 = null;
$opt3[] = new ListItem($payee_bank, $payee_bank, true);
$opt3[] = new ListItem("Banco Filipino", "Banco Filipino");
$ddlpayeebank->Items = $opt3;

$ddlpayeebanktype = new ComboBox("ddlpayeebanktype", "ddlpayeebanktype", "Payee Bank Type");
$opt4 = null;
$opt4[] = new ListItem($payee_bank_type, $payee_bank_type, true);
$opt4[] = new ListItem("", "");
$ddlpayeebanktype->Items = $opt4;

$chkpass = new CheckBox("chkpass", "chkpass", "Passport");
$chkdriver = new CheckBox("chkdriver", "chkdriver", "Driver");
$chksss = new CheckBox("chksss", "chksss", "SSS");
$chkothers = new CheckBox("chkothers", "chkothers", "Others");

$fproc->AddControl($ddlacctname);
$fproc->AddControl($ddlacctno);
$fproc->AddControl($btnSearch);
$fproc->AddControl($btnBack);
$fproc->AddControl($hiddenid);
$fproc->AddControl($hidden);
// --
$fproc->AddControl($txtuname);
$fproc->AddControl($txtfname);
$fproc->AddControl($txtmname);
$fproc->AddControl($txtlname);
$fproc->AddControl($txtscode);
$fproc->AddControl($txthadd);
$fproc->AddControl($txtzip);
$fproc->AddControl($txttelno);
$fproc->AddControl($txtmobno);
$fproc->AddControl($txtfax);
$fproc->AddControl($txtemail);
$fproc->AddControl($txtwebadd);
$fproc->AddControl($ddlcountry);
$fproc->AddControl($txtcitizen);
$fproc->AddControl($txtbday);
$fproc->AddControl($chkpass);
$fproc->AddControl($txtpassport);
$fproc->AddControl($chkdriver);
$fproc->AddControl($txtdriver);
$fproc->AddControl($chksss);
$fproc->AddControl($txtsss);
$fproc->AddControl($chkothers);
$fproc->AddControl($txtothers);
$fproc->AddControl($ddlbank);
$fproc->AddControl($ddlbanktype);
$fproc->AddControl($txtbankbranch);
$fproc->AddControl($txtbankacctname);
$fproc->AddControl($txtbankacctnum);
$fproc->AddControl($ddlpayeebank);
$fproc->AddControl($ddlpayeebanktype);
$fproc->AddControl($txtpayeebankbranch);
$fproc->AddControl($txtpayeebankacctname);
$fproc->AddControl($txtpayeebankacctnum);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($hidden->SubmittedValue != '')
    {
        $cacctdtls->StartTransaction();
        // Update Account Details
        $updacctdtls["AccountID"] = $hiddenid->SubmittedValue;
        $updacctdtls["SWCCode"] = $txtscode->SubmittedValue;
        $updacctdtls["HomeAddress"] = $txthadd->SubmittedValue;
        $updacctdtls["ZipCode"] = $txtzip->SubmittedValue;
        $updacctdtls["TelephoneNumber"] = $txttelno->SubmittedValue;
        $updacctdtls["MobileNumber"] = $txtmobno->SubmittedValue;
        $updacctdtls["Fax"] = $txtfax->SubmittedValue;
        $updacctdtls["Email"] = $txtemail->SubmittedValue;
        $updacctdtls["Website"] = $txtwebadd->SubmittedValue;
        $updacctdtls["CountryID"] = $ddlcountry->SubmittedValue;
        $updacctdtls["Citizenship"] = $txtcitizen->SubmittedValue;
        $updacctdtls["BirthDate"] = $txtbday->SubmittedValue;
        $updacctdtls["Passport"] = $txtpassport->SubmittedValue;
        $updacctdtls["DriversLicense"] = $txtdriver->SubmittedValue;
        $updacctdtls["Others"] = $txtothers->SubmittedValue;
        $updacctdtls["DateUpdated"] = 'now_usec()';
        $cacctdtls->UpdateByArray($updacctdtls);
        if ($cacctdtls->HasError)
        {
            $errormsg = $cacctdtls->getErrors();
            $errormsgtitle = "POS Account Update";
            $cacctdtls->RollBackTransaction();
        }
        else
        {
            $cacctdtls->CommitTransaction();

            $cacctbankdtls->StartTransaction();
            // Update Bank Details
            $updacctbankdtls["AccountID"] = $hiddenid->SubmittedValue;
            $updacctbankdtls["Bank"] = $ddlbank->SubmittedValue;
            $updacctbankdtls["BankType"] = $ddlbanktype->SubmittedValue;
            $updacctbankdtls["BankBranch"] = $txtbankbranch->SubmittedValue;
            $updacctbankdtls["BankAccountName"] = $txtbankacctname->SubmittedValue;
            $updacctbankdtls["BankAccountNumber"] = $txtbankacctnum->SubmittedValue;
            $updacctbankdtls["PayeeBank"] = $ddlpayeebank->SubmittedValue;
            $updacctbankdtls["PayeeBankType"] = $ddlpayeebanktype->SubmittedValue;
            $updacctbankdtls["PayeeBankBranch"] = $txtpayeebankbranch->SubmittedValue;
            $updacctbankdtls["PayeeBankAccountName"] = $txtpayeebankacctname->SubmittedValue;
            $updacctbankdtls["PayeeBankAccountNumber"] = $txtpayeebankacctnum->SubmittedValue;
            $updacctbankdtls["DateUpdated"] = 'now_usec()';
            $cacctbankdtls->UpdateByArray($updacctbankdtls);
            if ($cacctbankdtls->HasError)
            {
                $errormsg = $cacctbankdtls->getErrors();
                $errormsgtitle = "POS Account Update";
                $cacctbankdtls->RollBackTransaction();
            }
            else
            {
                $cacctbankdtls->CommitTransaction();
                unset($_SESSION['pos_acctid']);
                $successmsg = "The POS account status has been updated.";
                $successmsgtitle = "POS Account Update";
            }
        }
    }
}
?>
