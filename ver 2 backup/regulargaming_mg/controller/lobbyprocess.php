<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 19, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");

$cterminalsessions = new SCC_TerminalSessions();
$id = $_SESSION['id'];

$terminaldtls = $cterminalsessions->SelectTerminalSessionDetails($id);

if(count($terminaldtls) > 0)
{
    $conv_btn = "<div class=\"convertPoints\" onclick=\"popup('popUpDivLPConvert');\"></div>";
}
else
{
    $conv_btn = "<div class=\"convertPoints2\"></div>";
}
?>