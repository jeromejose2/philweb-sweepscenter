<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 14, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCCC_TransactionSummary");
App::LoadModuleClass("SweepsCenter", "SCCC_DeckInfo");
App::LoadModuleClass("SweepsCenter", "SCCC_TemporaryDeckTable");
App::LoadModuleClass("SweepsCenter", "SCCC_Winners");
App::LoadModuleClass("SweepsCenter", "SCCC_TempPrizeList");
App::LoadModuleClass("SweepsCenter", "SCCC_TempPrizeSummary");
App::LoadModuleClass("SweepsCenter", "SCCC_TransactionDetails");
App::LoadModuleClass("SweepsCenter", "SCCV_VoucherInfo");
App::LoadModuleClass("SweepsCenter", "SCCC_PointsConversionInfo");
App::LoadModuleClass("SweepsCenter", "SCC_FreeEntryRegistrants");
App::LoadModuleClass("SweepsCenter", "SCC_FreeEntry");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessionDetails");
App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");

$cctranssumm = new SCCC_TransactionSummary();
$ccdeckinfo = new SCCC_DeckInfo();
$cctempdeck = new SCCC_TemporaryDeckTable();
$ccwinners = new SCCC_Winners();
$cctempprizelist = new SCCC_TempPrizeList();
$cctempprizesumm = new SCCC_TempPrizeSummary();
$cctransdetails = new SCCC_TransactionDetails();
$cvvoucherinfo = new SCCV_VoucherInfo();
$ccpointsconversion = new SCCC_PointsConversionInfo();
$cfreeentryregistrants = new SCC_FreeEntryRegistrants();
$cfreeentry = new SCC_FreeEntry();
$cterminalsessiondtls = new SCC_TerminalSessionDetails();
$cwinningsdump = new SCC_WinningsDump();

$login = $_SESSION['user'];
$id = $_SESSION['id'];
$siteid = $_SESSION['siteid'];
$ip = $_SESSION['ip'];
$convertedpts = $_SESSION['equivalentpoints'];
$convertedbalance = $_SESSION['convertedbalance'];
$projectid = 2;
$alldeck_array = array();
$remainingcardcnt2 = 0;
//$ctr3 = 1;

if(isset ($_SESSION['sweeps_code']))
{
    $sweeps_code = $_SESSION['sweeps_code'];
}
else
{
    $sweeps_code = '';
}

if(isset ($_SESSION['IsFreeEntry']))
{
    $swps_cde = $_SESSION['sweeps_code'];
    $convertedpts = '1';
    $convertedbalance = 0.10;
    $option2 = 2;
}
else
{
    $swps_cde = "";
    $option2 = 1;
}

$cctranssumm->StartTransaction();
$arrTransactionSummary['RequestedBy'] = $id;
$arrTransactionSummary['TransactionDate'] = 'now_usec()';
$arrTransactionSummary['CardCount'] = $convertedpts;
$arrTransactionSummary['Status'] = 0;
$arrTransactionSummary['Option1'] = $id;
$cctranssumm->Insert($arrTransactionSummary);
if($cctranssumm->HasError)
{
    $cctranssumm->RollBackTransaction();
    echo "Error has occured: " . $cctranssumm->getError();
}
else
{
    $cctranssumm->CommitTransaction();

    $transsummaryid = $cctranssumm->LastInsertID;

    $deckdtls = $ccdeckinfo->SelectCurrentDeck($projectid);
    $deckid = $deckdtls[0]['DeckID'];
    $decksize = $deckdtls[0]['CardCount'];
    $usedcardcount = ($deckdtls[0]['UsedCardCount'] != null) ? $deckdtls[0]['UsedCardCount'] : 0;
    $winningcardcount = ($deckdtls[0]['WinningCardCount'] != null) ? $deckdtls[0]['WinningCardCount'] : 0;
    $remainingcardcount = ($decksize - $usedcardcount);

    if($convertedpts >= $remainingcardcount)
    {
        $ccdeckinfo->StartTransaction();
        $ccdeckinfo->UpdateDeckNStatus($deckid,$id,$option2,$transsummaryid,$remainingcardcount);
        if($ccdeckinfo->HasError)
        {
            $ccdeckinfo->RollBackTransaction();
            echo "Error has occured: " . $ccdeckinfo->getError();
        }
        else
        {
            $ccdeckinfo->CommitTransaction();

            $cctempdeck->StartTransaction();
            $cctempdeck->InsertSelectedCards($deckid,$transsummaryid);
            if($cctempdeck->HasError)
            {
                $cctempdeck->RollBackTransaction();
                echo "Error has occured: " . $cctempdeck->getError();
            }
            else
            {
                $cctempdeck->CommitTransaction();

                $usedcardcnt = ($usedcardcount + $remainingcardcount);
                $ccdeckinfo->StartTransaction();
                $ccdeckinfo->DeactivateDeck(2,$usedcardcnt, $deckid);
                if($ccdeckinfo->HasError)
                {
                    $ccdeckinfo->RollBackTransaction();
                    echo "Error has occured: " . $ccdeckinfo->getError();
                }
                else
                {
                    $ccdeckinfo->CommitTransaction();

                    $deckdtls = $ccdeckinfo->SelectCurrentDeck($projectid);
                    $deckid = $deckdtls[0]['DeckID'];
                    $decksize = $deckdtls[0]['CardCount'];
                    $usedcardcount = ($deckdtls[0]['UsedCardCount'] != null) ? $deckdtls[0]['UsedCardCount'] : 0;
                    $winningcardcount = ($deckdtls[0]['WinningCardCount'] != null) ? $deckdtls[0]['WinningCardCount'] : 0;
                    $remainingcardcount = ($decksize - $usedcardcount);

                    $remainingcardcount = ($convertedpts - $remainingcardcount);
                    $remainingcardcnt2 = $remainingcardcount;

                    $ccdeckinfo->StartTransaction();
                    $ccdeckinfo->DeactivateDeck(1,$remainingcardcnt2,$deckid);
                    if($ccdeckinfo->HasError)
                    {
                        $ccdeckinfo->RollBackTransaction();
                        echo "Error has occured: " . $ccdeckinfo->getError();
                    }
                    else
                    {
                        $ccdeckinfo->CommitTransaction();

                        $ccdeckinfo->StartTransaction();
                        $ccdeckinfo->UpdateDeckNStatus($deckid,$id,$option2,$transsummaryid,$remainingcardcount);
                        if($ccdeckinfo->HasError)
                        {
                            $ccdeckinfo->RollBackTransaction();
                            echo "Error has occured: " . $ccdeckinfo->getError();
                        }
                        else
                        {
                            $ccdeckinfo->CommitTransaction();

                            $cctempdeck->StartTransaction();
                            $cctempdeck->InsertSelectedCards($deckid,$transsummaryid);
                            if($cctempdeck->HasError)
                            {
                                $cctempdeck->RollBackTransaction();
                                echo "Error has occured: " . $cctempdeck->getError();
                            }
                            else
                            {
                                $cctempdeck->CommitTransaction();
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        $ccdeckinfo->StartTransaction();
        $ccdeckinfo->UpdateDeckNStatus($deckid,$id,$option2,$transsummaryid,$remainingcardcount);
        if($ccdeckinfo->HasError)
        {
            $ccdeckinfo->RollBackTransaction();
            echo "Error has occured: " . $ccdeckinfo->getError();
        }
        else
        {
            $ccdeckinfo->CommitTransaction();

            $cctempdeck->StartTransaction();
            $cctempdeck->InsertSelectedCards($deckid,$transsummaryid);
            if($cctempdeck->HasError)
            {
                $cctempdeck->RollBackTransaction();
                echo "Error has occured: " . $cctempdeck->getError();
            }
            else
            {
                $cctempdeck->CommitTransaction();
            }
        }
    }

    $cctempdeck->StartTransaction();
    $cctempdeck->UpdatePrizeDescription($transsummaryid);
    if($cctempdeck->HasError)
    {
        $cctempdeck->RollBackTransaction();
        echo "Error has occured: " . $cctempdeck->getError();
    }
    else
    {
        $cctempdeck->CommitTransaction();

        $ccwinners->StartTransaction();
        $ccwinners->InsertDataFromTemporaryDeck($projectid,$deckid,$transsummaryid);
        if($ccwinners->HasError)
        {
            $ccwinners->RollBackTransaction();
            echo "Error has occured: " . $ccwinners->getError();
        }
        else
        {
            $ccwinners->CommitTransaction();

            $totalcashwinningdtls = $cctempdeck->SelectTotalCashWinning($transsummaryid);
            $totalcashwinning = $totalcashwinningdtls[0]['TotalCash'];

            $winningscountdtls = $cctempdeck->SelectWinningCardCount($transsummaryid);
            $winningscount = $winningscountdtls[0]['cardcount'];

            if($winningscount > 0)
            {
                $cctempprizelist->StartTransaction();
                $cctempprizelist->InsertDetailsFromTemporaryDeck($transsummaryid);
                if($cctempprizelist->HasError)
                {
                    $cctempprizelist->RollBackTransaction();
                    echo "Error has occured: " . $cctempprizelist->getError();
                }
                else
                {
                    $cctempprizelist->CommitTransaction();
                }

                $cctempprizesumm->StartTransaction();
                $cctempprizesumm->InsertRecordsFromTemporaryDeck($transsummaryid);
                if($cctempprizesumm->HasError)
                {
                    $cctempprizesumm->RollBackTransaction();
                    echo "Error has occured: " . $cctempprizelist->getError();
                }
                else
                {
                    $cctempprizesumm->CommitTransaction();
                }
            }

            $cctransdetails->StartTransaction();
            $arrTransactionDetails['TransactionSummaryID'] = $transsummaryid;
            $arrTransactionDetails['DeckID'] = $deckid;
            $arrTransactionDetails['CardCount'] = $convertedpts;
            $arrTransactionDetails['TransactionDate'] = 'now_usec()';
            $cctransdetails->Insert($arrTransactionDetails);
            if($cctransdetails->HasError)
            {
                $cctransdetails->RollBackTransaction();
                echo "Error has occured: " . $cctransdetails->getError();
            }
            else
            {
                $cctransdetails->CommitTransaction();

                $cctranssumm->StartTransaction();
                $cctranssumm->UpdateStatus(1,$transsummaryid);
                if($cctranssumm->HasError)
                {
                    $cctranssumm->RollBackTransaction();
                }
                else
                {
                    $cctranssumm->CommitTransaction();

                    $usedcardcount = ($usedcardcount + $convertedpts);
                    $winningcardcount = ($winningcardcount + $winningscount);

                    $ccdeckinfo->StartTransaction();
                    if($remainingcardcnt2 > 0)
                    {
                        $ccdeckinfo->UpdateWinningCardCount($winningcardcount,'',$deckid);
                    }
                    else
                    {
                        $ccdeckinfo->UpdateWinningCardCount($winningcardcount,$usedcardcount,$deckid);
                    }
                    if($ccdeckinfo->HasError)
                    {
                        $ccdeckinfo->RollBackTransaction();
                        echo "Error has occured: " . $cctransdetails->getError();
                    }
                    else
                    {
                        $ccdeckinfo->CommitTransaction();

                        if($option2 == 1)
                        {
                            if($winningscount > 0)
                            {
                                $totalcash = "$" . $totalcashwinning . " Cash";

                                if($totalcashwinning > 0)
                                {
                                    $allPrizes = $totalcash;
                                }
                                else
                                {
                                    $allPrizes = "";
                                }

                                $prizecountdtls = $cctempprizesumm->SelectPrizeDescCount($transsummid);
                                $prizecount = $prizecountdtls[0]['prizedesccount'];

                                for($ctr3 = 1 ; $ctr3 <= $prizecount ; $ctr3++)
                                {
                                    $tempprizesummdtls = $cctempprizesumm->SelectTempPrizeSummaryDtls($transsummid);
                                    $vchr_Temp = $tempprizesummdtls[0]['PrizeDescription'];
                                    $int_TempCnt = $tempprizesummdtls[0]['PrizeCount'];
                                    $int_TempID = $tempprizesummdtls[0]['ID'];
                                    $int_Denomination = $tempprizesummdtls[0]['PrizeDenomination'];
                                    $int_TempPrizeType = $tempprizesummdtls[0]['PrizeType'];

                                    $noncash = "(" . $int_TempCnt  . ") " . $vchr_Temp;
                                    $allPrizes .= "," . $noncash;

                                    $cctempprizesumm->StartTransaction();
                                    $cctempprizesumm->DeleteRecord($int_TempID);
                                    if($cctempprizesumm->HasError)
                                    {
                                        $cctempprizesumm->RollBackTransaction();
                                        echo "Error has occured: " . $cctransdetails->getError();
                                    }
                                    else
                                    {
                                        $cctempprizesumm->CommitTransaction();
                                    }
                                }
                                $respondmessage = "You've won the following prize/s: " . $allPrizes . ". Your Transaction Reference ID is " .  str_pad($transsummaryid,14,0,STR_PAD_LEFT) . ".  Please proceed to the cashier to claim your winnings.";
                            }
                            else
                            {
                                $respondmessage = "You did not get any winning card. Your Transaction Reference ID is " .  str_pad($transsummaryid,14,0,STR_PAD_LEFT) . ".  Thank you for playing.";
                            }
                        }
                        else
                        {
                            $tempdeckdtls = $cctempdeck->SelectTempDeckDetailsByTransSummID($transsummid);
                            $allPrizes = $tempdeckdtls[0]['PrizeDescription'];
                            $vchr_ECN = $tempdeckdtls[0]['ECN'];
                            $int_TempPrizeType = $tempdeckdtls[0]['PrizeType'];
                            $int_TempPrizeValue = $tempdeckdtls[0]['PrizeValue'];

                            if($projectid == 2)
                            {
                                $vchr_ECN = str_pad($transsummid,14,0,STR_PAD_LEFT);
                            }

                            if($winningscount > 0)
                            {
                                $respondmessage = "You've won " .  $allPrizes . ". Your Transaction Reference ID is " . $vchr_ECN . ".<br />Please proceed to the cashier to claim your winnings.";
                            }
                            else
                            {
                                $respondmessage = "This is not a winning card. Your Transaction Reference ID is " . $vchr_ECN . ".<br /> Try our sweepstakes games and increase your chance of winning more and big prizes!<br />Thank you for playing.";
                            }
                        }

                        $cctranssumm->StartTransaction();
                        $cctranssumm->UpdateWinnings($winningcardcount,$allPrizes,$transsummid);
                        if($cctranssumm->HasError)
                        {
                            $cctranssumm->RollBackTransaction();
                            echo "Error has occured: " . $cctranssumm->getError();
                        }
                        else
                        {
                            $cctranssumm->CommitTransaction();

                            if($projectid == 2)
                            {
                                if($swps_cde != "")
                                {
                                    $denominationdtls = $cvvoucherinfo->SelectDenomination($swps_cde);
                                    $denomination = $denominationdtls[0]['Denomination'];

                                    $ccpointsconversion->StartTransaction();
                                    $arrPointsConversion['TransactionSummaryID'] = $transsummid;
                                    $arrPointsConversion['ProjectID'] = $projectid;
                                    $arrPointsConversion['TerminalID'] = $id;
                                    $arrPointsConversion['DateTimeTrans'] = 'now_usec()';
                                    $arrPointsConversion['EquivalentCards'] = $convertedpts;
                                    $arrPointsConversion['NoOfWinningCards'] = $winningscount;
                                    $arrPointsConversion['Winnings'] = $allPrizes;
                                    if($denomination == 1)
                                    {
                                        $arrPointsConversion['ConvertedPoints'] = '0.10';
                                    }
                                    else
                                    {
                                        $arrPointsConversion['ConvertedPoints'] = $convertedbalance;
                                    }
                                    $ccpointsconversion->Insert($arrPointsConversion);
                                    if($ccpointsconversion->HasError)
                                    {
                                        $ccpointsconversion->RollBackTransaction();
                                        echo "Error has occured: " . $ccpointsconversion->getError();
                                    }
                                    else
                                    {
                                        $ccpointsconversion->CommitTransaction();
                                    }
                                }
                                else
                                {
                                    $ccpointsconversion->StartTransaction();
                                    $arrPointsConversion['TransactionSummaryID'] = $transsummid;
                                    $arrPointsConversion['ProjectID'] = $projectid;
                                    $arrPointsConversion['TerminalID'] = $id;
                                    $arrPointsConversion['DateTimeTrans'] = 'now_usec()';
                                    $arrPointsConversion['EquivalentCards'] = $convertedpts;
                                    $arrPointsConversion['NoOfWinningCards'] = $winningscount;
                                    $arrPointsConversion['Winnings'] = $allPrizes;
                                    $arrPointsConversion['ConvertedPoints'] = $convertedbalance;
                                    $ccpointsconversion->Insert($arrPointsConversion);
                                    if($ccpointsconversion->HasError)
                                    {
                                        $ccpointsconversion->RollBackTransaction();
                                        echo "Error has occured: " . $ccpointsconversion->getError();
                                    }
                                    else
                                    {
                                        $ccpointsconversion->CommitTransaction();
                                    }
                                }

                                if($swps_cde != "")
                                {
                                    $cvvoucherinfo->StartTransaction();
                                    $cvvoucherinfo->UpdateVoucherInfoByVoucherNumAndStatus(2,$id,$allPrizes,$swps_cde,4);
                                    if($cvvoucherinfo->HasError)
                                    {
                                        $cvvoucherinfo->RollBackTransaction();
                                        echo "Error has occured: " . $ccpointsconversion->getError();
                                    }
                                    else
                                    {
                                        $cvvoucherinfo->CommitTransaction();
                                    }

                                    $freeentrydtls = $cfreeentry->SelectByVoucherCode($swps_cde);
                                    if(count($freeentrydtls) > 0)
                                    {
                                        $cfreeentry->StartTransaction();
                                        $cfreeentry->UpdateDatePlayed($swps_cde);
                                        if($cterminals->HasError)
                                        {
                                            $cterminals->RollBackTransaction();
                                            echo "Error has occured: " . $ccpointsconversion->getError();
                                        }
                                        else
                                        {
                                            $cterminals->CommitTransaction();
                                        }
                                    }
                                    else
                                    {
                                        $freeentryregistrants = $cfreeentryregistrants->SelectVoucherCode($swps_cde);
                                        if(count($freeentryregistrants) > 0)
                                        {
                                            $cfreeentryregistrants->StartTransaction();
                                            $cfreeentryregistrants->UpdateDatePlayedByVoucherCode($swps_cde);
                                            if($cfreeentryregistrants->HasError)
                                            {
                                                $cfreeentryregistrants->RollBackTransaction();
                                                echo "Error has occured: " . $cfreeentryregistrants->getError();
                                            }
                                            else
                                            {
                                                $cfreeentryregistrants->CommitTransaction();
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $tempdeckdata = $cctempdeck->SelectTempDeckDetailsByTransSummID($transsummid);
                        $cctempdeck->StartTransaction();
                        $cctempdeck->DeleteByTransSummID($transsummid);
                        if($cctempdeck->HasError)
                        {
                            $cctempdeck->RollBackTransaction();
                            echo "Error has occured: " . $cctempdeck->getError();
                        }
                        else
                        {
                            $cctempdeck->CommitTransaction();
                        }
                        $cctempprizelist->StartTransaction();
                        $cctempprizelist->DeleteByTransSummaryID($transsummid);
                        if($cctempprizelist->HasError)
                        {
                            $cctempprizelist->RollBackTransaction();
                            echo "Error has occured: " . $cctempprizelist->getError();
                        }
                        else
                        {
                            $cctempprizelist->CommitTransaction();
                        }
                    }
                }
            }
        }
    }
}

$terminalsessionid = $_SESSION['tsi'];
$maxiddtls = $cterminalsessiondtls->SelectMaxId();
$lastTransId = $maxiddtls[0]['max'];
$lastTransId = ($lastTransId != null) ? $lastTransId : 0;
$lastTransId = ($lastTransId + 1);
$transid = 'W' . str_pad($terminalsessionid,6,0,STR_PAD_LEFT) . str_pad($lastTransId,10,0,STR_PAD_LEFT);

$cterminalsessiondtls->StartTransaction();
$arrTerminalSessionDetails['TerminalSessionID'] = $terminalsessionid;
$arrTerminalSessionDetails['TransactionType'] = 'W';
$arrTerminalSessionDetails['Amount'] = $convertedbalance;
$arrTerminalSessionDetails['AcctID'] = $_SESSION['id'];
$arrTerminalSessionDetails['ServiceID'] = 1;
$arrTerminalSessionDetails['TransID'] = $transid;
$arrTerminalSessionDetails['ServiceTransactionID'] = $servicetransid;
$arrTerminalSessionDetails['TransDate'] ='now_usec()' ;
$arrTerminalSessionDetails['DateCreated'] = 'now_usec()';
$cterminalsessiondtls->Insert($arrTerminalSessionDetails);
if($cterminalsessiondtls->HasError)
{
    $cterminalsessiondtls->RollBackTransaction();
    echo "Error has occured: " . $cterminalsessiondtls->getError();
}
else
{
    $cterminalsessiondtls->CommitTransaction();
}

if(isset($_SESSION['IsFreeEntry']))
{
    $cvvoucherinfo->StartTransaction();
    $cvvoucherinfo->UpdateWinnings($wintype,$sweeps_code);
    if($cvvoucherinfo->HasError)
    {
        $cvvoucherinfo->RollBackTransaction();
        echo "Error updating voucher info.: " . $cvvoucherinfo->getError();
    }
    else
    {
        $cvvoucherinfo->CommitTransaction();
    }

    unset($_SESSION['IsFreeEntry']);
}

$cwinningsdump->StartTransaction();
$cwinningsdump->DeleteByTerminalID($id);
if($cwinningsdump->HasError)
{
    $cwinningsdump->RollBackTransaction();
    echo "Error has occured: " . $cwinningsdump->getError();
}
else
{
    $cwinningsdump->CommitTransaction();
}

for($i = 0 ; $i < count($tempdeckdata) ; $i++)
{
    if($tempdeckdata[$i]['PrizeDescription'] == "$0 Cash")
    {
        $prizedesc = "Try Again";
    }
    else
    {
        $prizedesc = $tempdeckdata[$i]['PrizeDescription'];
    }

    $cwinningsdump->StartTransaction();
    $arrWinningsDump['FK_TerminalID'] = $id;
    $arrWinningsDump['WinType'] = $prizedesc;
    $arrWinningsDump['ECN'] = $tempdeckdata[$i]['ECN'];
    $arrWinningsDump['IsOpened'] = 'N';
    $cwinningsdump->Insert($arrWinningsDump);
    if($cwinningsdump->HasError)
    {
        $cwinningsdump->RollBackTransaction();
        $error = "Error has occured: " . $cwinningsdump->getError();
    }
    else
    {
        $cwinningsdump->CommitTransaction();
    }
}

$_SESSION['resultmsg'] = $respondmessage;
if(!isset($error))
{
    echo "<script>window.location = '../displaycards.php';</script>";
}
?>
