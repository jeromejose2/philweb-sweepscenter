<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 08, 2012
 */
include('../init.inc.php');
include('xajax_toolkit/xajax_core/xajax.inc.php');

$xajax = new xajax();

$xajax->registerFunction("GetBalance");
$xajax->registerFunction("GetBalanceConv");
$xajax->registerFunction("HeartBeat");
$xajax->registerFunction("ConvertPoints");
$xajax->registerFunction("BrowseInternet");
$xajax->registerFunction("GetRegularGamingCards");
$xajax->registerFunction("RevealCards");
$xajax->registerFunction("ViewOpenedCards");
$xajax->registerFunction("CheckSession");
$xajax->registerFunction("CheckSessionSweepsCode");
$xajax->registerFunction("EnterSweepsCode");
$xajax->registerFunction("InterBrowsingLogs");
$xajax->registerFunction("checkPendingReloadTransactions");

function GetBalance()
{
    $objResponse = new xajaxResponse();

    $login = $_SESSION['user'];
    $ip_add_MG = $_SESSION['ip'];

    App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
    App::LoadLibrary("MicrogamingAPI.class.php");
    include("../config.php");

    $csmagentsessions = new SCCSM_AgentSessions();
    
    if(isset ($_SESSION['IsFreeEntry']))
    {
        $balance = '0.10';

        $balance = "Point Balance: ".$balance;

        $objResponse->assign("balance", "innerHTML", $balance);
        $objResponse->script("toggleBalanceLoading('balance');");
        $objResponse->script("hide_loading();");
    }
    else
    {
        $sessionguid = $csmagentsessions->SelectSessionGUID();
        $sessionGUID_MG = $sessionguid[0]["SessionGUID"];

        $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
                   "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
                   "<IPAddress>".$ip_add_MG."</IPAddress>".
                   "<ErrorCode>0</ErrorCode>".
                   "<IsLengthenSession>true</IsLengthenSession>".
                   "</AgentSession>";


        $client = new nusoap_client($mg_url, 'wsdl');
        $client->setHeaders($headers);
        $param = array('delimitedAccountNumbers' => $login);
        $filename = "../../APILogs/logs.txt";
        $fp = fopen($filename , "a");
        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".serialize($param)."\r\n");
        fclose($fp);
        $a = 1;
        while($a < 4)
        {
            $result = $client->call('GetAccountBalance', $param);
            if(is_array($result))
            {
                if(array_key_exists('faultcode', $result))
                {
                    $convert = $result['faultstring'];
                    $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                    $objResponse->assign("msg","innerHTML",$convert);
                    $objResponse->script("hide_loading();");
                    $a++;
                }
                else
                {
                    if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
                    {
                        $a++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                $convert = "Error in getting balance.";
                $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                $objResponse->assign("msg","innerHTML",$convert);
                $objResponse->script("hide_loading();");
                $a++;
            }
        }

        $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];

        $balance = number_format($balance, 2, '.', '');
        $balance = "Point Balance: ".$balance;

        $objResponse->assign("balance", "innerHTML", $balance);
        $objResponse->script("hide_loading();");
        $objResponse->script("toggleBalanceLoading('balance');");
    }

    return $objResponse;
}

function GetBalanceConv()
{
    $objResponse = new xajaxResponse();

    $login = $_SESSION['user'];
    $ip_add_MG = $_SESSION['ip'];

    App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
    App::LoadLibrary("MicrogamingAPI.class.php");
    include("../config.php");

    $csmagentsessions = new SCCSM_AgentSessions();

    if(isset ($_SESSION['IsFreeEntry']))
    {
        $balance = '0.10';

        $balance = "Point Balance: ".$balance;

        $objResponse->assign("balance", "innerHTML", $balance);
        $objResponse->script("toggleBalanceLoading('balance');");
        $objResponse->script("hide_loading();");
    }
    else
    {
        $sessionguid = $csmagentsessions->SelectSessionGUID();
        $sessionGUID_MG = $sessionguid[0]["SessionGUID"];

        $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
                   "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
                   "<IPAddress>".$ip_add_MG."</IPAddress>".
                   "<ErrorCode>0</ErrorCode>".
                   "<IsLengthenSession>true</IsLengthenSession>".
                   "</AgentSession>";


        $client = new nusoap_client($mg_url, 'wsdl');
        $client->setHeaders($headers);
        $param = array('delimitedAccountNumbers' => $login);
        $filename = "../../APILogs/logs.txt";
        $fp = fopen($filename , "a");
        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".serialize($param)."\r\n");
        fclose($fp);
        $a = 1;
        while($a < 4)
        {
            $result = $client->call('GetAccountBalance', $param);
            if(array_key_exists('faultcode', $result))
            {
                $convert = $result['faultstring'];
                $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                $objResponse->assign("msg","innerHTML",$convert);
                $objResponse->script("hide_loading();");
                $a++;
            }
            else
            {
                if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
                {
                    $a++;
                }
                else
                {
                    break;
                }
            }
        }

        $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];

        $balance = number_format($balance, 2, '.', '');

        $objResponse->assign("convbal", "innerHTML", $balance);
        $objResponse->script("hide_loading();");
        $objResponse->script("popup('popUpDivLPConvert');");
    }

    return $objResponse;
}

function HeartBeat()
{
    $objResponse = new xajaxResponse();

    $login = $_SESSION['user'];
    $ip_add_MG = $_SESSION['ip'];

    App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
    App::LoadLibrary("MicrogamingAPI.class.php");
    include("../config.php");

    $csmagentsessions = new SCCSM_AgentSessions();

    $sessionguid = $csmagentsessions->SelectSessionGUID();
    $sessionGUID_MG = $sessionguid[0]["SessionGUID"];

    $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
               "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
               "<IPAddress>".$ip_add_MG."</IPAddress>".
               "<ErrorCode>0</ErrorCode>".
               "<IsLengthenSession>true</IsLengthenSession>".
               "</AgentSession>";

    $client = new nusoap_client($mg_url, 'wsdl');
    $client->setHeaders($headers);
    $param = array('delimitedAccountNumbers' => $login);
    $filename = "../../APILogs/logs.txt";
    $fp = fopen($filename , "a");
    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".serialize($param)."\r\n");
    fclose($fp);
    $a = 1;
    while($a < 4)
    {
        $result = $client->call('GetAccountBalance', $param);
        if(array_key_exists('faultcode', $result))
        {
            $convert = $result['faultstring'];
            $objResponse->script("popup('popUpDivLPCheckActiveSession');");
            $objResponse->assign("msg","innerHTML",$convert);
            $objResponse->script("hide_loading();");
            $a++;
        }
        else
        {
            if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
            {
                $a++;
            }
            else
            {
                break;
            }
        }
    }

    return $objResponse;
}

function CheckSession($game_id)
{
    $objResponse = new xajaxResponse();

    $id = $_SESSION['id'];
    $login = $_SESSION['user'];
    $passwrd = $_SESSION['pass'];

    App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
    App::LoadModuleClass("SweepsCenter", "SCC_Terminals");

    $cterminalsessions = new SCC_TerminalSessions();
    $cterminals = new SCC_Terminals();

    $sessiondtls = $cterminalsessions->SelectTerminalSessionDetails($id);

    if(count($sessiondtls) > 0)
    {
        if(isset ($_SESSION['IsFreeEntry']))
        {
            $convert = "No Active Session.";
            $objResponse->script("popup('popUpDivLPCheckActiveSession');");
            $objResponse->assign("msg","innerHTML",$convert);
        }
        else
        {
            if($game_id == 'keno')
            {
                $objResponse->script("window.location.href='launcher.php?gid=Keno&user=".$login."&pass=".$passwrd."'");
            }
            else if($game_id == 'qhorse1')
            {
                $objResponse->script("window.location.href='launcher.php?gid=PremierRacing&user=".$login."&pass=".$passwrd."'");
                //$objResponse->alert("Game is undergoing system enhancement. Sorry for the inconvenience.");
            }
            else if($game_id == 'qhorse2')
            {
                $objResponse->script("window.location.href='launcher.php?gid=RoyalDerby&user=".$login."&pass=".$passwrd."'");
            }
	     	else if($game_id == 'dino')
            {
                $objResponse->script("window.location.href='launcher.php?gid=DinoMight&user=".$login."&pass=".$passwrd."'");
            }
	     	else if($game_id == 'tomb')
            {
                $objResponse->script("window.location.href='launcher.php?gid=TombRaider&user=".$login."&pass=".$passwrd."'");
            }
	     	else if($game_id == 'bj')
            {
                $objResponse->script("window.location.href='launcher.php?gid=bj&user=".$login."&pass=".$passwrd."'");
            }
                else if($game_id == 'tomb2')
            {
                $objResponse->script("window.location.href='launcher.php?gid=TombRaiderII&user=".$login."&pass=".$passwrd."'");
            }
                else if($game_id == 'wheel')
            {
                $objResponse->script("window.location.href='launcher.php?gid=WheelofWealth&user=".$login."&pass=".$passwrd."'");
            }
        }

        $cterminals->StartTransaction();
        $cterminals->UpdateServiceID($id,3);
        if($cterminals->HasError)
        {
            $cterminals->RollBackTransaction();
        }
        else
        {
            $cterminals->CommitTransaction();
        }
    }
    else
    {
        $convert = "No Active Session.";
        $objResponse->script("popup('popUpDivLPCheckActiveSession');");
        $objResponse->assign("msg","innerHTML",$convert);
    }
    
    return $objResponse;
}

function CheckSessionSweepsCode()
{
    $objResponse = new xajaxResponse();

    $id = $_SESSION['id'];

    if(isset ($_SESSION['IsFreeEntry']))
    {
        $convert = "<p>You have an existing free entry sweeps code.</p></br/> <p>Please convert this point first.</p>";
        $objResponse->script("popup('popUpDivLPCheckActiveSession');");
        $objResponse->assign("msg","innerHTML",$convert);
    }
    else
    {
        $objResponse->script("location.href='entrycode-page.php';");

        /*
        $query = "SELECT * FROM tbl_terminalsessions where TerminalID = '$id' AND DateEnd = 0;";
        $result = mysqli_query($dbConn,$query);
        $row = mysqli_fetch_array($result);
        $sql_row_num = mysqli_num_rows($result);

        mysqli_next_result($dbConn);
        mysqli_free_result($result);

        mysqli_close($dbConn);

        if ($sql_row_num > 0)
        {
            $convert = "<p>You are not allowed to use this feature if you</p><br/> <p>have an active session.</p>";

            $objResponse->script("popup('popUpDivLPCheckActiveSession');");
            $objResponse->assign("msg","innerHTML",$convert);
        }
        else
        {

        }
         */
    }

    return $objResponse;
}

function ConvertPoints()
{
    $objResponse = new xajaxResponse();

    $arrStr = explode("/", $_SERVER['SCRIPT_NAME'] );
    $arrStr = array_reverse($arrStr );

    $login = $_SESSION['user'];
    $id = $_SESSION['id'];
    $siteid = $_SESSION['siteid'];
    $ip = $_SESSION['ip'];
    $page = $arrStr[0];
    $terminal_session_id = $_SESSION['tsi'];
    $terminalbalance = 0;

    App::LoadLibrary("MicrogamingAPI.class.php");
    App::LoadLibrary("microseconds.php");
    include("../config.php");

    App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
    App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
    App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
    App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
    App::LoadModuleClass("SweepsCenter", "SCC_TransactionLogs");
    App::LoadModuleClass("SweepsCenter", "SCC_PointsConversion");

    $cterminals = new SCC_Terminals();
    $csmagentsessions = new SCCSM_AgentSessions();
    $cterminalsession = new SCC_TerminalSessions();
    $caudittrail = new SCC_AuditTrail();
    $ctranslog = new SCC_TransactionLogs();
    $cpointsconversion = new SCC_PointsConversion();
    
    if(isset ($_SESSION['IsFreeEntry']))
    {
        $terminaldtls = $cterminals->SelectTerminalName($id);
        if(count($terminaldtls) > 0)
        {
            $isFreeEntry = $terminaldtls[0]['IsFreeEntry'];
            $cterminalsession->StartTransaction();
            $cterminalsession->UpdateDateEnd($id,$terminal_session_id);
            if($cterminalsession->HasError)
            {
                $cterminalsession->RollBackTransaction();
                $convert = "An Error Occured. Please try again. ".$return_msg;
                $okbutton = "<img src='images/ProceedButton.png' onclick=\"popup('popUpDivConvert');\" style='cursor: pointer;' />";
            }
            else
            {
                $cterminalsession->CommitTransaction();

                if($isFreeEntry == 0)
                {
                    $cterminals->StartTransaction();
                    $cterminals->UpdateTerminalStatus($id);
                    if($cterminals->HasError)
                    {
                        $cterminals->RollBackTransaction();
                        $convert = "An Error Occured. Please try again. ".$return_msg;
                        $okbutton = "<img src='images/ProceedButton.png' onclick=\"popup('popUpDivConvert');\" style='cursor: pointer;' />";
                    }
                    else
                    {
                        $cterminals->CommitTransaction();
                    }
                }

                $caudittrail->StartTransaction();
                $arrAuditTrail["SessionID"] = "";
                $arrAuditTrail["AccountID"] = 0;
                $arrAuditTrail["TransDetails"] = "Session end for terminalid: " . $id;
                $arrAuditTrail["RemoteIP"] = "";
                $arrAuditTrail["TransDateTime"] = 'now_usec()';
                $caudittrail->Insert($arrAuditTrail);
                if($caudittrail->HasError)
                {
                    $caudittrail->RollBackTransaction();
                    $convert = "An Error Occured. Please try again. ".$return_msg;
                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"popup('popUpDivConvert');\" style='cursor: pointer;' />";
                }
                else
                {
                    $caudittrail->CommitTransaction();
                    $convert = "You have converted 0.10 point/s for 1 number of sweepstakes card/s. Click PROCEED to reveal your cards.";
                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"display_loading_img(); location.href='controller/get_regular_gaming_cards.php';\" style='cursor: pointer;' />";
                }
            }
        }
        else
        {
            $convert = "An Error Occured. Please try again. ".$return_msg;
            $okbutton = "<img src='images/ProceedButton.png' onclick=\"popup('popUpDivConvert');\" style='cursor: pointer;' />";
        }

        $objResponse->script("popup('popUpDivConvert');");
        $objResponse->assign("convert","innerHTML",$convert);
        $objResponse->assign("okbtn","innerHTML",$okbutton);
    }
    else
    {
        $sessiondtls = $cterminalsessions->SelectTerminalSessionDetails($id);

        if(count($sessiondtls) > 0)
        {
            $balancedtls = $cterminals->SelectBalance($id);
            if(count($balancedtls) > 0)
            {
                $terminalbalance = $balancedtls[0]['Balance'];
            }

            $login = $_SESSION['user'];
            $ip_add_MG = $_SESSION['ip'];

            $sessionguid = $csmagentsessions->SelectSessionGUID();
            $sessionGUID_MG = $sessionguid[0]["SessionGUID"];

            $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
                       "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
                       "<IPAddress>".$ip_add_MG."</IPAddress>".
                       "<ErrorCode>0</ErrorCode>".
                       "<IsLengthenSession>true</IsLengthenSession>".
                       "</AgentSession>";

            $client = new nusoap_client($mg_url, 'wsdl');
            $client->setHeaders($headers);
            $param = array('delimitedAccountNumbers' => $login);
            if(serialize($param) != "b:0") 
            {
                $filename = "../APILogs/logs.txt";
                $fp = fopen($filename , "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GetAccountBalance || ".$login." || ".serialize($param)."\r\n");
                fclose($fp);
                $a = 1;
                while($a < 4)
                {
                    $result = $client->call('GetAccountBalance', $param);
                    if(array_key_exists('faultcode', $result))
                    {
                        $convert = $result['faultstring'];
                        $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                        $objResponse->assign("msg","innerHTML",$convert);
                        $objResponse->script("hide_loading();");
                        $a++;
                    }
                    else
                    {
                        if($result['GetAccountBalanceResult']['BalanceResult']['IsSucceed'] == 'false')
                        {
                            $a++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                if(($string == "b:0") || (count($result) == 0) || (count($result["GetAccountBalanceResult"]["BalanceResult"]) != 8)) // Added by Arlene R. Salazar 04-29-2012
                {
                    $convert = "Error in serializing Get Balance.";
                    $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                    $objResponse->assign("msg","innerHTML",$convert);
                    $objResponse->script("hide_loading();");
                }
                else
                {
                    $balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];

                    $filename = "../../APILogs/logs.txt";
                    $fp = fopen($filename , "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: GET TERMINAL BALANCE(CONVERT POINTS) || ".$login." || ".$string."\r\n");
                    fclose($fp);

                    if ($balance >= 1)
                    {
                        //INSERT TO LOGS
                        $datestamp = udate("YmdHisu");

                        $ctranslog->StartTransaction();
                        $arrTransactionLog["TerminalSessionID"] = $datestamp;
                        $arrTransactionLog["TerminalID"] = $id;
                        $arrTransactionLog["ServiceID"] = 3;
                        $arrTransactionLog["DateCreated"] = 'now_usec()';
                        $arrTransactionLog["DateUpdated"] = 'now_usec()';
                        $arrTransactionLog["TransactionType"] = 'W';
                        $arrTransactionLog["Amount"] = $balance;
                        $arrTransactionLog["LaunchPad"] = 1;
                        $ctranslog->Insert($arrTransactionLog);
                        if($ctranslog->HasError)
                        {
                            $ctranslog->RollBackTransaction();

                            $convert = "An Error Occured. Please try again.(Error proc_inserttotransactionlog)".$returnmsg;
                            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                            $objResponse->script("popup('popUpDivConvert');");
                            $objResponse->assign("convert","innerHTML",$convert);
                            $objResponse->assign("okbtn","innerHTML",$okbutton);
                        }
                        else
                        {
                            $ctranslog->CommitTransaction();
                            //insert to logs
                            $filename = "../../DBLogs/logs.txt";
                            $fp = fopen($filename , "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_inserttotransactionlog('$datestamp','$id',3,1,'W','$balance',1,@retval) || ".$return." || ".$returnmsg."\r\n");
                            fclose($fp);

                            $param_withdraw = array('accountNumber' => $login, 'amount' => $balance, 'currency' => 1);

                            $filename = "../../APILogs/logs.txt";
                            $fp = fopen($filename , "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Withdrawal || ".$login." || ".serialize($param_withdraw)."\r\n");
                            fclose($fp);

                            $result_withdraw = $client->call('Withdrawal', $param_withdraw);
                            $string = serialize($result_withdraw);
                            if(($string == "b:0") || (count($result_withdraw) == 0) || (count($result_withdraw["WithdrawalResult"]) != 8)) 
                            {
                                $convert = "Error in serializing Withdrawal.";
                                $objResponse->script("popup('popUpDivConvert');");
                                $objResponse->assign("convert","innerHTML",$convert);
                                $objResponse->assign("okbtn","innerHTML",$okbutton);
                            }
                            else
                            {
                                $errormsg = $result_withdraw["WithdrawalResult"]["IsSucceed"];
                                $externaltransid = $result_withdraw["WithdrawalResult"]["TransactionId"];
                                $transaction_amt = $result_withdraw["WithdrawalResult"]["TransactionAmount"];
                                $transaction_credit_amt = $result_withdraw["WithdrawalResult"]["TransactionCreditAmount"];
                                $credit_bal = $result_withdraw["WithdrawalResult"]["CreditBalance"];
                                $balance_mg = $result_withdraw["WithdrawalResult"]["Balance"];

                                if($errormsg == 'true')
                                {
                                    //UPDATE LOGS
                                    $ctranslog->StartTransaction();
                                    $ctranslog->UpdateTransactionLog($externaltransid, $errormsg, 1, $datestamp);
                                    if($ctranslog->HasError)
                                    {
                                        $ctranslog->RollBackTransaction();

                                        $convert = "An Error Occured. Please try again.(Error proc_updatetransactionlog)".$returnmsg;
                                        $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                        $objResponse->script("popup('popUpDivConvert');");
                                        $objResponse->assign("convert","innerHTML",$convert);
                                        $objResponse->assign("okbtn","innerHTML",$okbutton);
                                    }
                                    else
                                    {
                                        $ctranslog->CommitTransaction();

                                        //insert to logs
                                        $filename = "../../DBLogs/logs.txt";
                                        $fp = fopen($filename , "a");
                                        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',1,1,@retval) || ".$return." || ".$returnmsg."\r\n");
                                        fclose($fp);

                                        $terminaldtls = $cterminals->SelectTerminalName($id);
                                        if(count($terminaldtls) > 0)
                                        {
                                            $processedby = $terminaldtls[0]['Name'];
                                            $maxid_dtls = $cpointsconversion->SelectMaxID();
                                            $lasttransid = ($maxid_dtls[0]['maxid'] != null) ? $maxid_dtls[0]['maxid'] : 0;
                                            $lasttransid = ($lasttransid + 1);
                                            $transid = 'C' . str_pad($id,6,0,STR_PAD_LEFT) . str_pad($lasttransid,10,0,STR_PAD_LEFT);
                                            $balance = ($balance < 1) ? 1 : $balance;
                                            
                                            $cpointsconversion->StartTransaction();
                                            $arrPointsConversion['SiteID'] = $siteid;
                                            $arrPointsConversion['TerminalID'] = $id;
                                            $arrPointsConversion['TransID'] = $transid;
                                            $arrPointsConversion['Balance'] = $balance;
                                            $arrPointsConversion['EquivalentPoints'] = floor($balance);
                                            $arrPointsConversion['TransDate'] = 'now_usec()';
                                            $arrPointsConversion['ProcessedBy'] = $processedby;
                                            $cpointsconversion->Insert($arrPointsConversion);
                                            if($cpointsconversion->HasError)
                                            {
                                                $cpointsconversion->RollBackTransaction();

                                                $convert = "An Error Occured. Please try again.(Error proc_convert_points)".$returnmsg;
                                                $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                                $objResponse->script("popup('popUpDivConvert');");
                                                $objResponse->assign("convert","innerHTML",$convert);
                                                $objResponse->assign("okbtn","innerHTML",$okbutton);
                                            }
                                            else
                                            {
                                                $cpointsconversion->CommitTransaction();

                                                $cterminalsession->StartTransaction();
                                                $cterminalsession->UpdateDateEndPerSiteID($id,$siteid);
                                                if($cterminalsession->HasError)
                                                {
                                                    $cterminalsession->RollBackTransaction();

                                                    $convert = "An Error Occured. Please try again.(Error proc_convert_points)".$returnmsg;
                                                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                                    $objResponse->script("popup('popUpDivConvert');");
                                                    $objResponse->assign("convert","innerHTML",$convert);
                                                    $objResponse->assign("okbtn","innerHTML",$okbutton);
                                                }
                                                else
                                                {
                                                    $cterminalsession->CommitTransaction();

                                                    $cterminals->StartTransaction();
                                                    $cterminals->UpdateTerminalStatus($id);
                                                    if($cterminals->HasError)
                                                    {
                                                        $cterminals->RollBackTransaction();

                                                        $convert = "An Error Occured. Please try again.(Error proc_convert_points)".$returnmsg;
                                                        $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                                        $objResponse->script("popup('popUpDivConvert');");
                                                        $objResponse->assign("convert","innerHTML",$convert);
                                                        $objResponse->assign("okbtn","innerHTML",$okbutton);
                                                    }
                                                    else
                                                    {
                                                        $cterminals->CommitTransaction();
                                                        
                                                        $caudittrail->StartTransaction();
                                                        $arrAuditTrail["SessionID"] = "";
                                                        $arrAuditTrail["AccountID"] = "";
                                                        $arrAuditTrail["TransDetails"] = 'Convert points for : ' . $processedby;
                                                        $arrAuditTrail["RemoteIP"] ="";
                                                        $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                                        $caudittrail->Insert($arrAuditTrail);
                                                        if($caudittrail->HasError)
                                                        {
                                                            $caudittrail->RollBackTransaction();
                                                            
                                                            $convert = "An Error Occured. Please try again.(Error proc_convert_points)".$returnmsg;
                                                            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                                            $objResponse->script("popup('popUpDivConvert');");
                                                            $objResponse->assign("convert","innerHTML",$convert);
                                                            $objResponse->assign("okbtn","innerHTML",$okbutton);
                                                        }
                                                        else
                                                        {
                                                            $caudittrail->CommitTransaction();

                                                            $cterminals->StartTransaction();
                                                            $cterminals->UpdateServiceID($id, 0);
                                                            if($cterminals->HasError)
                                                            {
                                                                $cterminals->RollBackTransaction();
                                                            }
                                                            else
                                                            {
                                                                $cterminals->CommitTransaction();
                                                            }

                                                            $_SESSION['equivalentpoints'] = $equivalentpoints;

                                                            $balance = number_format($balance,2);

                                                            $_SESSION['convertedbalance'] = $balance; 

                                                            $convert = "<p>You have converted ".$balance." point/s for ".$equivalentpoints."</p> <p style=\"margin-top: 10px;\">number of sweepstakes card/s. Click PROCEED</p> <p style=\"margin-top: 10px;\">to reveal your cards.</p>";
                                                            $okbutton = "<img src='images/ProceedButton.png' onclick=\"display_loading_img(); location.href='controller/get_regular_gaming_cards.php';\" style='cursor: pointer;' />";

                                                            $objResponse->script("popup('popUpDivConvert');");
                                                            $objResponse->script("hide_loading();");
                                                            $objResponse->assign("convert","innerHTML",$convert);
                                                            $objResponse->assign("okbtn","innerHTML",$okbutton);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //rollback withdraw API (deposit API amt=$balance)
                                            $convert = "An Error Occured. Please try again.(Error proc_convert_points)".$returnmsg;
                                            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                            $objResponse->script("popup('popUpDivConvert');");
                                            $objResponse->assign("convert","innerHTML",$convert);
                                            $objResponse->assign("okbtn","innerHTML",$okbutton);
                                        }
                                    }
                                }
                                else
                                {
                                    $string = serialize($result_withdraw);

                                    //insert to logs
                                    $filename = "../../APILogs/logs.txt";
                                    $fp = fopen($filename , "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(CONVERT POINTS) || ".$login." || ".$string."\r\n");
                                    fclose($fp);

                                    $ctranslog->StartTransaction();
                                    $ctranslog->UpdateTransactionLog($externaltransid,$errormsg,2,$datestamp);
                                    if($ctranslog->HasError)
                                    {
                                        $ctranslog->RollBackTransaction();
                                    }
                                    else
                                    {
                                        $ctranslog->CommitTransaction();
                                    }
                                    //insert to logs
                                    $filename = "../../DBLogs/logs.txt";
                                    $fp = fopen($filename , "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',2,1,@retval) || ".$return." || ".$returnmsg."\r\n");
                                    fclose($fp);

                                    $convert = "An Error Occured. Please try again.(Withdrawal API error)";
                                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                    $objResponse->script("popup('popUpDivConvert');");
                                    $objResponse->assign("convert","innerHTML",$convert);
                                    $objResponse->assign("okbtn","innerHTML",$okbutton);
                                }
                            }
                        }
                    }
                    else
                    {
                        //INSERT TO LOGS
                        $datestamp = udate("YmdHisu");

                        $ctranslog->StartTransaction();
                        $arrTransactionLog["TerminalSessionID"] = $datestamp;
                        $arrTransactionLog["TerminalID"] = $id;
                        $arrTransactionLog["ServiceID"] = 3;
                        $arrTransactionLog["DateCreated"] = 'now_usec()';
                        $arrTransactionLog["DateUpdated"] = 'now_usec()';
                        $arrTransactionLog["TransactionType"] = 'W';
                        $arrTransactionLog["Amount"] = $balance;
                        $arrTransactionLog["LaunchPad"] = 1;
                        $ctranslog->Insert($arrTransactionLog);
                        if($ctranslog->HasError)
                        {
                            $ctranslog->RollBackTransaction();

                            $convert = "An Error Occured. Please try again.(Error proc_inserttotransactionlog)".$returnmsg;
                            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                            $objResponse->script("popup('popUpDivConvert');");
                            $objResponse->assign("convert","innerHTML",$convert);
                            $objResponse->assign("okbtn","innerHTML",$okbutton);
                        }
                        else
                        {
                            $ctranslog->CommitTransaction();
                            
                            //insert to logs
                            $filename = "../../DBLogs/logs.txt";
                            $fp = fopen($filename , "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_inserttotransactionlog('$datestamp','$id',3,1,'W','$balance',1,@retval) || ".$return." || ".$returnmsg."\r\n");
                            fclose($fp);

                            $param_withdraw = array('accountNumber' => $login, 'amount' => $balance, 'currency' => 1);

                            $filename = "../../APILogs/logs.txt";
                            $fp = fopen($filename , "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Withdrawal || ".$login." || ".serialize($param_withdraw)."\r\n");
                            fclose($fp);

                            $result_withdraw = $client->call('Withdrawal', $param_withdraw);
                            $string = serialize($result_withdraw);
                            if(($string == "b:0") || (count($result_withdraw) == 0) || (count($result_withdraw["WithdrawalResult"]) != 8)) // Added by Arlene R. Salazar 04-29-2012
                            {
                                $convert = "Error in serializing Withdrawal.";
                                $objResponse->script("popup('popUpDivConvert');");
                                $objResponse->assign("convert","innerHTML",$convert);
                                $objResponse->assign("okbtn","innerHTML",$okbutton);
                            }
                            else
                            {
                                $errormsg = $result_withdraw["WithdrawalResult"]["IsSucceed"];
                                $externaltransid = $result_withdraw["WithdrawalResult"]["TransactionId"];
                                $transaction_amt = $result_withdraw["WithdrawalResult"]["TransactionAmount"];
                                $transaction_credit_amt = $result_withdraw["WithdrawalResult"]["TransactionCreditAmount"];
                                $credit_bal = $result_withdraw["WithdrawalResult"]["CreditBalance"];
                                $balance_mg = $result_withdraw["WithdrawalResult"]["Balance"];

                                $balance_orig = $balance;
                                $balance = 1;

                                if($errormsg == 'true')
                                {
                                    //insert to logs
                                    $filename = "../../APILogs/logs.txt";
                                    $fp = fopen($filename , "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(CONVERT POINTS) || ".$login." || ".$string."\r\n");
                                    fclose($fp);

                                    //UPDATE LOGS
                                    $ctranslog->StartTransaction();
                                    $ctranslog->UpdateTransactionLog($externaltransid,$errormsg,1,$datestamp);
                                    if($ctranslog->HasError)
                                    {
                                        $ctranslog->RollBackTransaction();

                                        $convert = "An Error Occured. Please try again.(Error proc_updatetransactionlog)".$returnmsg;
                                        $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                        $objResponse->script("popup('popUpDivConvert');");
                                        $objResponse->assign("convert","innerHTML",$convert);
                                        $objResponse->assign("okbtn","innerHTML",$okbutton);
                                    }
                                    else
                                    {
                                        $ctranslog->CommitTransaction();
                                        
                                        $terminaldtls = $cterminals->SelectTerminalName($id);
                                        if(count($terminaldtls) > 0)
                                        {
                                            $processedby = $terminaldtls[0]['Name'];
                                            $maxid_dtls = $cpointsconversion->SelectMaxID();
                                            $lasttransid = ($maxid_dtls[0]['maxid'] != null) ? $maxid_dtls[0]['maxid'] : 0;
                                            $lasttransid = ($lasttransid + 1);
                                            $transid = 'C' . str_pad($id,6,0,STR_PAD_LEFT) . str_pad($lasttransid,10,0,STR_PAD_LEFT);
                                            $balance = ($balance < 1) ? 1 : $balance;

                                            $cpointsconversion->StartTransaction();
                                            $arrPointsConversion['SiteID'] = $siteid;
                                            $arrPointsConversion['TerminalID'] = $id;
                                            $arrPointsConversion['TransID'] = $transid;
                                            $arrPointsConversion['Balance'] = $balance;
                                            $arrPointsConversion['EquivalentPoints'] = floor($balance);
                                            $arrPointsConversion['TransDate'] = 'now_usec()';
                                            $arrPointsConversion['ProcessedBy'] = $processedby;
                                            $cpointsconversion->Insert($arrPointsConversion);
                                            if($cpointsconversion->HasError)
                                            {
                                                $cpointsconversion->RollBackTransaction();

                                                $convert = "An Error Occured. Please try again.(Error proc_convert_points)".$returnmsg;
                                                $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                                $objResponse->script("popup('popUpDivConvert');");
                                                $objResponse->assign("convert","innerHTML",$convert);
                                                $objResponse->assign("okbtn","innerHTML",$okbutton);
                                            }
                                            else
                                            {
                                                $cpointsconversion->CommitTransaction();

                                                $cterminalsession->StartTransaction();
                                                $cterminalsession->UpdateDateEndPerSiteID($id,$siteid);
                                                if($cterminalsession->HasError)
                                                {
                                                    $cterminalsession->RollBackTransaction();

                                                    $convert = "An Error Occured. Please try again.(Error proc_convert_points)".$returnmsg;
                                                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                                    $objResponse->script("popup('popUpDivConvert');");
                                                    $objResponse->assign("convert","innerHTML",$convert);
                                                    $objResponse->assign("okbtn","innerHTML",$okbutton);
                                                }
                                                else
                                                {
                                                    $cterminalsession->CommitTransaction();

                                                    $cterminals->StartTransaction();
                                                    $cterminals->UpdateTerminalStatus($id);
                                                    if($cterminals->HasError)
                                                    {
                                                        $cterminals->RollBackTransaction();

                                                        $convert = "An Error Occured. Please try again.(Error proc_convert_points)".$returnmsg;
                                                        $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                                        $objResponse->script("popup('popUpDivConvert');");
                                                        $objResponse->assign("convert","innerHTML",$convert);
                                                        $objResponse->assign("okbtn","innerHTML",$okbutton);
                                                    }
                                                    else
                                                    {
                                                        $cterminals->CommitTransaction();

                                                        $caudittrail->StartTransaction();
                                                        $arrAuditTrail["SessionID"] = "";
                                                        $arrAuditTrail["AccountID"] = "";
                                                        $arrAuditTrail["TransDetails"] = 'Convert points for : ' . $processedby;
                                                        $arrAuditTrail["RemoteIP"] ="";
                                                        $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                                        $caudittrail->Insert($arrAuditTrail);
                                                        if($caudittrail->HasError)
                                                        {
                                                            $caudittrail->RollBackTransaction();

                                                            $convert = "An Error Occured. Please try again.(Error proc_convert_points)".$returnmsg;
                                                            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                                            $objResponse->script("popup('popUpDivConvert');");
                                                            $objResponse->assign("convert","innerHTML",$convert);
                                                            $objResponse->assign("okbtn","innerHTML",$okbutton);
                                                        }
                                                        else
                                                        {
                                                            $caudittrail->CommitTransaction();

                                                            $cterminals->StartTransaction();
                                                            $cterminals->UpdateServiceID($id, 3);
                                                            if($cterminals->HasError)
                                                            {
                                                                $cterminals->RollBackTransaction();
                                                            }
                                                            else
                                                            {
                                                                $cterminals->CommitTransaction();
                                                            }

                                                            $_SESSION['equivalentpoints'] = $equivalentpoints;

                                                            $balance = number_format($balance,2);

                                                            $_SESSION['convertedbalance'] = $balance_orig;

                                                            $convert = "<p>You have converted ".$balance_orig." point/s for ".$equivalentpoints."</p> <p style=\"margin-top: 10px;\">number of sweepstakes card/s. Click PROCEED</p> <p style=\"margin-top: 10px;\">to reveal your cards.</p>";
                                                            $okbutton = "<img src='images/ProceedButton.png' onclick=\"display_loading_img(); location.href='controller/get_regular_gaming_cards.php';\" style='cursor: pointer;' />";

                                                            $objResponse->script("popup('popUpDivConvert');");
                                                            $objResponse->script("hide_loading();");
                                                            $objResponse->assign("convert","innerHTML",$convert);
                                                            $objResponse->assign("okbtn","innerHTML",$okbutton);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //rollback withdraw API (deposit API amt=$balance)
                                            $convert = "An Error Occured. Please try again.(Error proc_convert_points)".$returnmsg;
                                            $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                            $objResponse->script("popup('popUpDivConvert');");
                                            $objResponse->assign("convert","innerHTML",$convert);
                                            $objResponse->assign("okbtn","innerHTML",$okbutton);
                                        }
                                    }
                                }
                                else
                                {
                                    $string = serialize($result_withdraw);

                                    //insert to logs
                                    $filename = "../../APILogs/logs.txt";
                                    $fp = fopen($filename , "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: WITHDRAWAL(CONVERT POINTS) || ".$login." || ".$string."\r\n");
                                    fclose($fp);

                                    //UPDATE LOGS
                                    $ctranslog->StartTransaction();
                                    $ctranslog->UpdateTransactionLog($externaltransid,$errormsg,2,$datestamp);
                                    if($ctranslog->HasError)
                                    {
                                        $ctranslog->RollBackTransaction();
                                    }
                                    else
                                    {
                                        $ctranslog->CommitTransaction();
                                    }

                                    //insert to logs
                                    $filename = "../../DBLogs/logs.txt";
                                    $fp = fopen($filename , "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: CONVERT POINTS || ".$login." || proc_updatetransactionlog('$datestamp','$externaltransid','$errormsg',2,1,@retval) || ".$return." || ".$returnmsg."\r\n");
                                    fclose($fp);

                                    $convert = "An Error Occured. Please try again.(Withdrawal API error)";
                                    $okbutton = "<img src='images/ProceedButton.png' onclick=\"window.location.href='$page';\" style='cursor: pointer;' />";

                                    $objResponse->script("popup('popUpDivConvert');");
                                    $objResponse->assign("convert","innerHTML",$convert);
                                    $objResponse->assign("okbtn","innerHTML",$okbutton);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                $convert = "Error in serializing Get Balance.";
                $objResponse->script("popup('popUpDivLPCheckActiveSession');");
                $objResponse->assign("msg","innerHTML",$convert);
                $objResponse->script("hide_loading();");
            }
        }
        else
        {
            $convert = "No Active Session.";
            $objResponse->script("popup('popUpDivLPCheckActiveSession');");
            $objResponse->assign("msg","innerHTML",$convert);
            $objResponse->script("hide_loading();");
        }
    }

    return $objResponse;
}

function BrowseInternet()
{
    $objResponse = new xajaxResponse();

    $login = $_SESSION['user'];
    $ip = $_SESSION['ip'];
    $id = $_SESSION['id'];
    $ip_add_MG = $_SESSION['ip'];
    $time_now = date('Y-m-d g:i:s');

    $objResponse->script("openurl('http://192.168.20.8:8105/WebBrowserProxy/upload/index.php?id=".$_SESSION['id']."&user=".$_SESSION['user']."'); location.href='launchpad.php';");

    return $objResponse;
}

function RevealCards($i)
{
    $objResponse = new xajaxResponse();

    $login = $_SESSION['user'];
    $id = $_SESSION['id'];
    $siteid = $_SESSION['siteid'];
    $ip = $_SESSION['ip'];
    $resultmsg2 = $_SESSION['$resultmsg[0]'];

    App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");

    $cwinningsdump = new SCC_WinningsDump();

    $winnings_count = $cwinningsdump->SelectAllWinnings($terminalid);
    $count = count($winnings_count);

    $avail_cards = $count - 1;
    $avail_cards = "<div class=\"countBox_body\"><p>".$avail_cards."</p></div>";

    if($count > 0)
    {
        if($count == 1)
        {
            $resultmsg = $resultmsg2;
            $okbtn = "<div class=\"sweeps_quick7\" onclick=\"window.location.href='logout.php'\" ></div>";
            $quick_pick_btn = "<div class=\"sweeps_quick2\" ></div>";

            $objResponse->script("document.getElementById(\"view_next_set\").style.visibility = \"hidden\";");
            $objResponse->assign("quick_pick_btn", "innerHTML", $quick_pick_btn);
            $objResponse->assign("resultmsg", "innerHTML", $resultmsg);
            $objResponse->assign("okbtn", "innerHTML", $okbtn);
        }

        $winningsdtls = $cwinningsdump->SelectAllWinnings($terminalid);
        $win_type = $winningsdtls[0]['WinType'];
        $ecn = $winningsdtls[0]['ECN'];
        $id = $winningsdtls[0]['ID'];

        $cwinningsdump->StartTransaction();
        $cwinningsdump->UpdateIsOpenedStatus($terminalid,'Y');
        if($cwinningsdump->HasError)
        {
            $cwinningsdump->RollBackTransaction();
        }
        else
        {
            $cwinningsdump->CommitTransaction();
        }

        $balance = "<div class=\"win_container2\"><p>".$win_type."</p><span>".$ecn."</span></div>";
        $objResponse->assign(".$i.", "innerHTML", $balance);
        $objResponse->assign("avail_cards", "innerHTML", $avail_cards);
    }
    else
    {
        $balance = "<div class=\"win_container2\"><div class=\"flipCard\"></div></div>";
        $objResponse->assign(".$i.", "innerHTML", $balance);
        $objResponse->alert("You do not have available scratch card(s)");
    }

    return $objResponse;
}

function ViewOpenedCards()
{
    $objResponse = new xajaxResponse();

    $id = $_SESSION['id'];

    App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");

    $cwinningsdump = new SCC_WinningsDump();

    $winningsdtls = $cwinningsdump->SelectAllWinnings($terminalid);

    $opened_cards = "<div style='margin-left: 35px;'>";

    for($i = 0 ; $i < count($winningsdtls) ; $i++)
    {
        $win_type[$i] = $winningsdtls[$i]['WinType'];
        $ecn[$i] = $winningsdtls[$i]['ECN'];

        $opened_cards .= "<div class=\"sweepsContainer\"><div class=\"win_container\"><p>".$winningsdtls[$i]['WinType']."</p><span>".$winningsdtls[$i]['ECN']."</span></div></div>";
    }
    $opened_cards .= "</div>";

    $objResponse->assign("opened_cards", "innerHTML", $opened_cards);

    return $objResponse;
}

function EnterSweepsCode($sweeps_code)
{
    $objResponse = new xajaxResponse();

    $id = $_SESSION['id'];
    $ip = $_SESSION['ip'];
    $_SESSION['sweeps_code'] = $sweeps_code;
    $login = $_SESSION['user'];

    App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");
    App::LoadModuleClass("SweepsCenter", "SCCV_VoucherInfo");
    App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
    App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
    App::LoadModuleClass("SweepsCenter", "SCC_Accounts");
    App::LoadModuleClass("SweepsCenter", "SCC_Sites");
    App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessionDetails");
    App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
    App::LoadLibrary("MicrogamingAPI.class.php");
    App::LoadLibrary("microseconds.php");
    include("../config.php");

    $csmagentsessions = new SCCSM_AgentSessions();
    $cvvoucherinfo = new SCCV_VoucherInfo();
    $ccterminalsessions = new SCC_TerminalSessions();
    $cterminals = new SCC_Terminals();
    $caccounts = new SCC_Accounts();
    $csites = new SCC_Sites();
    $cterminalsessiondtls = new SCC_TerminalSessionDetails();
    $caudittrail = new SCC_AuditTrail();

    $voucherdtls = $cvvoucherinfo->SelectVoucherDtls($sweeps_code);
    $status = $voucherdtls[0]['Status'];
    $entrytype = $voucherdtls[0]['EntryType'];
    $denomination = $voucherdtls[0]['Description'];

    if($status == 2)
    {
        $title = "USED CODE";
        $msg = "The Sweeps Code you have entered has already been used.";
        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
    }
    else if($status == 3)
    {
        $title = "EXPIRED CODE";
        $msg = "The Sweeps Code you have entered has already expired.";
        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
    }
    else if($status == 4)
    {
        if($entrytype == 3)
        {
            //deposit to MG
            $sessionguid = $csmagentsessions->SelectSessionGUID();
            $sessionGUID_MG = $sessionguid[0]["SessionGUID"];

            $headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
                       "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
                       "<IPAddress>".$ip."</IPAddress>".
                       "<ErrorCode>0</ErrorCode>".
                       "<IsLengthenSession>true</IsLengthenSession>".
                       "</AgentSession>";

            $client = new nusoap_client($mg_url, 'wsdl');
            $client->setHeaders($headers);
            $param = array('accountNumber' => $login, 'amount' => $denomination, 'currency' => 1);
			$filename = "../APILogs/logs.txt";
			$fp = fopen($filename , "a");
			fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: Deposit || ".$login." || ".serialize($param)."\r\n");
			fclose($fp);
    	    $result = $client->call('Deposit', $param);

            $errormsg = $result['DepositResult']['IsSucceed'];
            $transid_mg = $result["DepositResult"]["TransactionId"];
            $acctno_mg = $result["DepositResult"]["AccountNumber"];
            $transamt_mg = $result["DepositResult"]["TransactionAmount"];

            $datestamp = udate("Y-m-d H:i:s.u");

            if($errormsg == 'true')
            {
                $string = serialize($result);

                //insert to logs
                $filename = "../../APILogs/logs.txt";
                $fp = fopen($filename , "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: DEPOSIT || ".$string."\r\n");
                fclose($fp);

                $cvvoucherinfo->StartTransaction();
                $cvvoucherinfo->UpdateVoucherInfo($datestamp, $id, $sweeps_code);
                if($cvvoucherinfo->HasError)
                {
                    $cvvoucherinfo->RollBackTransaction();
                }
                else
                {
                    $cvvoucherinfo->CommitTransaction();
                }

                $session_dtls = $ccterminalsessions->SelectTerminalSessionDetails($id);

                if (count($session_dtls) > 0)
                {
                    $sitedtls = $cterminals->SelectTerminalName($id);
                    if(count($sitedtls) > 0)
                    {
                        $siteid = $sitedtls[0]['SiteID'];

                        $acctdtls = $caccounts->SelectAcctIDReloadNonCashier($siteid);
                        $acctid = $acctdtls[0]['ID'];

                        $balancedtls = $csites->SelectSiteDetails($siteid);
                        $balance = $balancedtls[0]['Balance'];

                        if($balance < $denomination)
                        {
                            $title = "ERROR";
                            $msg = "An Error Occured. Please try again. ".'Account balance is less than the deposit amount.';
                            $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                        }
                        else
                        {
                            $new_balance = ($balance - $denomination);
                            $csites->StartTransaction();
                            $csites->UpdateSiteBalance($new_balance,$siteid);
                            if($csites->HasError)
                            {
                                $csites->RollBackTransaction();

                                $title = "ERROR";
                                $msg = "An Error Occured. Please try again. ".'Error updating site balance.';
                                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                            }
                            else
                            {
                                $csites->CommitTransaction();

                                $cterminals->StartTransaction();
                                $cterminals->UpdateTerminalBalance($balance,$id);
                                if($cterminals->HasError)
                                {
                                    $cterminals->RollBackTransaction();

                                    $title = "ERROR";
                                    $msg = "An Error Occured. Please try again. ".'Error updating terminal balance.';
                                    $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                }
                                else
                                {
                                    $cterminals->CommitTransaction();

                                    $terminalsessiondtls = $ccterminalsessions->SelectTerminalSessionDetails($id);
                                    $terminalsessionid = $terminalsessiondtls[0]['ID'];

                                    $lasttransiddtls = $cterminalsessiondtls->SelectMaxId();
                                    $lasttransid = ($lasttransiddtls[0]['max'] == null) ? 0 : $lasttransiddtls[0]['max'];
                                    $lasttransid = ($lasttransid + 1);

                                    $transrefid = 'R' . str_pad($terminalsessionid,6,0,STR_PAD_LEFT) . str_pad($lasttransid,10,0,STR_PAD_LEFT);
                                    
                                    $cterminalsessiondtls->StartTransaction();
                                    $arrTerminalSessionDetails['AcctID'] = $acctid;
                                    $arrTerminalSessionDetails['TerminalSessionID'] = $terminalsessionid;
                                    $arrTerminalSessionDetails['TransactionType'] = 'R';
                                    $arrTerminalSessionDetails['Amount'] = $denomination;
                                    $arrTerminalSessionDetails['ServiceID'] = 1;
                                    $arrTerminalSessionDetails['TransID'] = $transrefid;
                                    $arrTerminalSessionDetails['ServiceTransactionID'] = $transid_mg;
                                    $arrTerminalSessionDetails['TransDate'] = 'now_usec()';
                                    $arrTerminalSessionDetails['DateCreated'] = 'now_usec()';
                                    $cterminalsessiondtls->Insert($arrTerminalSessionDetails);
                                    if($cterminalsessiondtls->HasError)
                                    {
                                        $cterminalsessiondtls->RollBackTransaction();

                                        $title = "ERROR";
                                        $msg = "An Error Occured. Please try again. ".'Error inserting new terminal session details.';
                                        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                    }
                                    else
                                    {
                                        $cterminalsessiondtls->CommitTransaction();

                                        $caudittrail->StartTransaction();
                                        $arrAuditTrail["SessionID"] = "";
                                        $arrAuditTrail["AccountID"] = $acctid;
                                        $arrAuditTrail["TransDetails"] = 'Non-cashier reload for terminalid: ' . $id;
                                        $arrAuditTrail["RemoteIP"] = $ip;
                                        $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                        $caudittrail->Insert($arrAuditTrail);
                                        if($caudittrail->HasError)
                                        {
                                            $caudittrail->RollBackTransaction();

                                            $title = "ERROR";
                                            $msg = "An Error Occured. Please try again. ".'Error inserting in audit trail..';
                                            $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                        }
                                        else
                                        {
                                            $caudittrail->CommitTransaction();

                                            $title = "SUCCESSFUL RELOAD";
                                            $msg = "You have successfully reloaded ".$denomination." points using your voucher";
                                            $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"window.location.reload( true ); location.href='launchpad.php'\" style=\"cursor:pointer;\"/>";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        $title = "ERROR";
                        $msg = "An Error Occured. Please try again. ".'Terminal ID does not exist.';
                        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                    }
                }
                else
                {
                    //INITIAL DEPOSIT

                    //start session
                    $terminaldtls = $cterminals->SelectTerminalDetails($id);
                    if(count($terminaldtls) > 0)
                    {
                        $title = "ERROR";
                        $msg = "An Error Occured. Please try again. ".'Terminal is already in use.';
                        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                    }
                    else
                    {
                        $terminalsessiondtls = $ccterminalsessions->SelectTerminalSessionDetails($id);
                        if(count($terminalsessiondtls) > 0)
                        {
                            $title = "ERROR";
                            $msg = "An Error Occured. Please try again. ".'Terminal session already exists.';
                            $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                        }
                        else
                        {
                            $terminalnamedtls = $cterminals->SelectTerminalName($id);
                            $isFreeEntry = $terminalnamedtls[0]['IsFreeEntry'];
                            $siteid = $terminalnamedtls[0]['SiteID'];

                            $accountdtls = $caccounts->SelectAcctIDStartSessionNonCashier($id);
                            $acctid = $accountdtls[0]['ID'];

                            if(($denomination == 0) && ($isFreeEntry != 1))
                            {
                                $title = "ERROR";
                                $msg = "An Error Occured. Please try again. ".'Initial deposit amount must be greater than 0.';
                                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                            }
                            else
                            {
                                $balancedtls = $csites->SelectSiteDetails($siteid);
                                $balance = $balancedtls[0]['Balance'];
                                $new_balance = ($balance - $denomination);

                                if($balance < $denomination)
                                {
                                    $title = "ERROR";
                                    $msg = "An Error Occured. Please try again. ".'Account balance is less than the deposit amount.';
                                    $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                }
                                else
                                {
                                    if($isFreeEntry != 1)
                                    {
                                        $csites->StartTransaction();
                                        $csites->UpdateSiteBalance($new_balance,$siteid);
                                        if($csites->HasError)
                                        {
                                            $csites->RollBackTransaction();

                                            $title = "ERROR";
                                            $msg = "An Error Occured. Please try again. ".'Error updating site balance.';
                                            $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                        }
                                        else
                                        {
                                            $csites->CommitTransaction();
                                        }
                                    }

                                    $cterminals->StartTransaction();
                                    $cterminals->UpdateTerminalBalanceServiceIDEquivalentPoints($denomination,$id);
                                    if($cterminals->HasError)
                                    {
                                        $cterminals->RollBackTransaction();

                                        $title = "ERROR";
                                        $msg = "An Error Occured. Please try again. ".'Error updating terminal balance.';
                                        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                    }
                                    else
                                    {
                                        $cterminals->CommitTransaction();

                                        $ccterminalsessions->StartTransaction();
                                        $arrTerminalSession['TerminalID'] = $id;
                                        $arrTerminalSession['SiteID'] = $siteid;
                                        $arrTerminalSession['RemoteIP'] = $ip;
                                        $arrTerminalSession['DateStart'] = 'now_usec()';
                                        $arrTerminalSession['DateCreated'] = 'now_usec()';
                                        $ccterminalsessions->Insert($arrTerminalSession);
                                        if($ccterminalsessions->HasError)
                                        {
                                            $ccterminalsessions->RollBackTransaction();

                                            $title = "ERROR";
                                            $msg = "An Error Occured. Please try again. ".'Error inserting new terminal session.';
                                            $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                        }
                                        else
                                        {
                                            $ccterminalsessions->CommitTransaction();

                                            $lastinsertid = $ccterminalsessions->LastInsertID;
                                            $maxiddtls = $cterminalsessiondtls->SelectMaxId();
                                            $lasttransid = $maxiddtls[0]['max'];

                                            $transrefid = 'D' . str_pad($lastinsertid,6,0,STR_PAD_LEFT) . str_pad($lasttransid,10,0,STR_PAD_LEFT);

                                            $cterminalsessiondtls->StartTransaction();
                                            $arrTerminalSessionDtls['TerminalSessionID'] = $lastinsertid;
                                            $arrTerminalSessionDtls['TransactionType'] = 'D';
                                            $arrTerminalSessionDtls['Amount'] = $denomination;
                                            $arrTerminalSessionDtls['ServiceID'] = 1;
                                            $arrTerminalSessionDtls['TransID'] = $transrefid;
                                            $arrTerminalSessionDtls['ServiceTransactionID'] = $transid_mg;
                                            $arrTerminalSessionDtls['AcctID'] = $acctid;
                                            $arrTerminalSessionDtls['TransDate'] = 'now_usec()';
                                            $arrTerminalSessionDtls['DateCreated'] = 'now_usec()';
                                            $cterminalsessiondtls->Insert($arrTerminalSessionDtls);
                                            if($cterminalsessiondtls->HasError)
                                            {
                                                $cterminalsessiondtls->RollBackTransaction();

                                                $title = "ERROR";
                                                $msg = "An Error Occured. Please try again. ".'Error inserting new terminal session details.';
                                                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                            }
                                            else
                                            {
                                                $cterminalsessiondtls->CommitTransaction();

                                                $caudittrail->StartTransaction();
                                                $arrAuditTrail["SessionID"] = "";
                                                $arrAuditTrail["AccountID"] = $acctid;
                                                $arrAuditTrail["TransDetails"] = 'Non-cashier session start for terminalid: ' . $id;
                                                $arrAuditTrail["RemoteIP"] = $ip;
                                                $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                                $caudittrail->Insert($arrAuditTrail);
                                                if($caudittrail->HasError)
                                                {
                                                    $caudittrail->RollBackTransaction();

                                                    $title = "ERROR";
                                                    $msg = "An Error Occured. Please try again. ".'Error inserting in audit trail.';
                                                    $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
                                                }
                                                else
                                                {
                                                    $caudittrail->CommitTransaction();

                                                    $title = "SUCCESSFUL INITIAL DEPOSIT";
                                                    $msg = "You have successfully loaded ".$denomination." points using your voucher. You may use these points to Browse the Internet or Play Games";
                                                    $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"window.location.reload( true ); location.href='launchpad.php'\" style=\"cursor:pointer;\"/>";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                $string = serialize($result);

                //insert to logs
                $filename = "../../APILogs/logs.txt";
                $fp = fopen($filename , "a");
                fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: DEPOSIT || ".$string."\r\n");
                fclose($fp);

                $title = "ERROR";
                $msg = "An Error Occured. Please try again. API Deposit Error.";
                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
            }
        }
        else
        {
            //check if there is an existing session
            $sessiondtls = $ccterminalsessions->SelectTerminalSessionDetails($id);
            
            if (count($sessiondtls) > 0)
            {
                $title = "FREE ENTRY NOT ALLOWED";
                $msg = "Sorry, but you are not allowed to enter a Free Entry code with an ongoing session.";
                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
            }
            else
            {
                $title = "ERROR";
                $msg = "Please enter your Free Entry Sweeps Code on the designated Free Entry terminal.";
                $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"location.href='launchpad.php'\" style=\"cursor:pointer;\"/>";
            }
        }
    }
    else
    {
        $title = "INVALID CODE";
        $msg = "You have entered an invalid sweeps code. Please check if you entered the code correctly.";
        $okbtn = "<img src=\"images/OK Button.png\" alt=\"\" onclick=\"popup('popUpDivEntryCode');\" style=\"cursor:pointer;\"/>";
    }

    $objResponse->script("hide_loading();");
    $objResponse->assign("title", "innerHTML", $title);
    $objResponse->assign("msg", "innerHTML", $msg);
    $objResponse->assign("okbtn", "innerHTML", $okbtn);
    $objResponse->script("popup('popUpDivEntryCode');");

    return $objResponse;
}

/*TUMIGIL KA DITO!!!!*/
function InterBrowsingLogs($btn_name)
{
    $objResponse = new xajaxResponse();

    $login = $_SESSION['user'];

    if($btn_name == 'browse_btn')
    {
        //insert to logs
        $filename = "../BrowseInternetLogs/logs.txt";
        $fp = fopen($filename , "a");

        fwrite($fp, date("Y-m-d H:i:s") . " || ".$login." || BUTTON CLICKED: INTERNET BROWSING BUTTON || \r\n");

        fclose($fp);
    }
    else if($btn_name == 'cancel_btn')
    {
        //insert to logs
        $filename = "../BrowseInternetLogs/logs.txt";
        $fp = fopen($filename , "a");

        fwrite($fp, date("Y-m-d H:i:s") . " || ".$login." || BUTTON CLICKED: INTERNET BROWSING CANCEL BUTTON || \r\n");

        fclose($fp);
    }
    else
    {
        //insert to logs
        $filename = "../BrowseInternetLogs/logs.txt";
        $fp = fopen($filename , "a");

        fwrite($fp, date("Y-m-d H:i:s") . " || ".$login." || BUTTON CLICKED: INTERNET BROWSING CONFIRMATION BUTTON || \r\n");

        fclose($fp);
    }

    return $objResponse;
}

/*Created By: Arlene R. Salazar 03/20/2012*/
function checkPendingReloadTransactions()
{
    $objResponse = new xajaxResponse();
    
    App::LoadModuleClass("SweepsCenter", "SCC_TransactionLogs");

    $ctranslog = new SCC_TransactionLogs();

    $id = $_SESSION['id'];
	
    $pendingdtls = $ctranslog->SelectPendingReloadTransaction($id);
    if(count($pendingdtls) > 0)
    {
        $message = '<div class="convertPoints" style="cursor: default;" onclick="javascript: alert(\'You cannot convert your points because you still have a pending reload transaction.\');">
	                    <img src="images/convertPoints.png" height="33px" width="150px" />
	                </div>';
    }
    else
    {
        $message = '<div class="convertPoints" onclick="show_loading(); xajax_GetBalanceConv();">
	                    <img src="images/convertPoints.png" height="33px" width="150px" />
	                </div>';
    }
    $objResponse->assign("convertPointsContainer", "innerHTML", $message);

    return $objResponse;
}
/*Created By: Arlene R. Salazar 03/20/2012*/

$xajax->processRequest();
$xajax_js = $xajax->getJavascript("xajax_toolkit");
?>
?>