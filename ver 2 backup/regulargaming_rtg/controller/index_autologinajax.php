<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 19, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
App::LoadLibrary("class.rc4crypt.php");

$cterminals = new SCC_Terminals();
$caudittrail = new SCC_AuditTrail();
$cterminalsessions = new SCC_TerminalSessions();
$mac_address = $_GET['macadd'];
$ip = $_SERVER['REMOTE_ADDR'];

$macadddtls = $cterminals->SelectByMacAddress($mcadd);

$name = $macadddtls[0]['Name'];
$pass = $macadddtls[0]['Password'];
$hshedpass = $macadddtls[0]['HashedPassword'];

//decrypt password
$pwd = '23780984';
$hshedpass = @pack('H*', $hshedpass);
$dcrptpass = rc4crypt::decrypt($pwd, $hshedpass);
//end decrypt password

if($name == '')
{
    echo json_encode(array("msg1" => "Incorrect login for this specific terminal."));
}
else
{
    $terminaldtls = $cterminals->SelectTerminalUserDtlsWithHashedPassword($name,$pass);
    if(count($terminaldtls) > 0)
    {
        $terminalid = $terminaldtls[0]['ID'];
        $siteid = $terminaldtls[0]['SiteID'];

        $terminalsessionsdtls = $cterminalsessions->SelectTerminalSessionDetails($terminalid);
        
        $caudittrail->StartTransaction();
        $arrAuditTrail['SessionID'] = "";
        $arrAuditTrail['AccountID'] = 0;
        $arrAuditTrail['TransDetails'] = 'Auto Login: ' . $name;
        $arrAuditTrail['RemoteIP'] = $ip;
        $arrAuditTrail['TransDateTime'] = 'now_usec()';
        $caudittrail->Insert($arrAuditTrail);
        if($caudittrail->HasError)
        {
            $caudittrail->RollBackTransaction();
            echo json_encode(array("msg1" => "Error inserting in audit trail."));
        }
        else
        {
            $cterminals->CommitTransaction();
            
            $_SESSION['user'] = $name;
            $_SESSION['id'] = $terminalid;
            $_SESSION['siteid'] = $siteid;
            $_SESSION['ip'] = $ip;
            $_SESSION['pass'] = $dcrptpass;
            $_SESSION['terminal_session_id'] = $terminalsessionsdtls[0]['ID'];

            echo json_encode(array("msg1" => "Terminal login successful."));
        }

        //insert to logs
        $filename = "../../DBLogs/logs.txt";
        $fp = fopen($filename , "a");
        fwrite($fp, date("Y-m-d H:i:s") . " || LAUNCHPAD || TRANSACTION TYPE: AUTO LOGIN || ".$name." || INSERT INTO tbl_audittrail \r\n");
    }
    else
    {
        echo json_encode(array("msg1" => "You have entered an invalid account information."));
    }
}
?>