<?php
/* 
 * Created By: Arlene R. Salazar
 * Created On: June 08, 2012
 */
include('../controller/loginprocess.php');
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Login : Launch Pad</title>
        <link rel="stylesheet" type="text/css" href="css/login.css" />
        <style>
            .txtName2{
                font: bold 20px/25px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
                color: #FFF;
                text-transform: uppercase;
                width:150px;
                height:auto;
                margin:10px 0 0 15px;
                float:left;
                width: 100%;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div id="mainContainer">
            <div id="banner"></div>
            <div id="contentContainer">
                <div id="Login_Cont">
                    <div class="login_logo"></div>
                    <div class="login_body">
                        <div class="loginCont_body">
                            <div class="txtName2">CHOOSE LOGIN TYPE:</div>
                             <div style="width: 100%; text-align: center;">
                                   <img src="images/autologin.png" width="150" height="40" style="margin-top: 5px; cursor: pointer;" onclick="window.location.href='index_autologin.php'" />
                                   <img src="images/manuallogin.png" width="150" height="40" style="margin-top: 5px; cursor: pointer;" onclick="window.location.href='index_manuallogin.php'" />
                            </div>
                              <div style="width: 100%; text-align: center;">
                                  <img src="images/registerred.png" width="180" height="40" style="margin-top: 10px; cursor: pointer;" onclick="window.location.href='index_register.php'" />
                              </div>
                        </div>
                        <div class="login_bottom">&nbsp;</div>
                    </div>
                </div>
                <div id="footer"></div>
            </div>
        </div>
    </body>
</html>