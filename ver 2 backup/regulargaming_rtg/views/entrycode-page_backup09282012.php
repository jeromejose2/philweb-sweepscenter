<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 20, 2012
 */
include('../controller/launchpadprocess.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Sweeps Entry Code </title>
        <link rel="stylesheet" type="text/css" href="css/launchpad.css" />
        <link href="css/CSSPopUp.css" rel="stylesheet" type="text/css" />
        <link href="css/lightbox.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript" src="jscripts/CSSPopUp.js"></script>
        <script src="jscripts/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="jscripts/jquery.background.image.scale-0.1.js" type="text/javascript"></script>
        <script type="text/javascript" language="javascript" src="jscripts/jquery-fieldselection.js"></script>
        <script type="text/javascript" language="javascript" src="jscripts/jquery-ui-personalized-1.5.2.min.js"></script>
        <script type="text/javascript" src="jscripts/cskeyboard.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/trans.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/lightbox.js"></script>
        <script type="text/javascript">
                //Using document.ready causes issues with Safari when the page loads
                jQuery(window).load(function(){
                        $("#contentContainer").backgroundScale({
                                imageSelector: "#gaBG",
                                centerAlign: true,
                                containerPadding: 0
                        });
                });
        </script>
        <?php $xajax->printJavascript(); ?>
    </head>
    <body>
        <div id="blanket" style="display:none;"></div>
        <div id="popUpDivEntryCode" style="display:none; font-family:Helvetica; font-size: 20px;">
            <div align="center" style=" border-bottom-style: solid; border-color:#139E9E; background-color: #77A6A0; color: white; height: 40px;"><b><div id="title" style="margin-top: 8px; position: absolute; margin-left: 180px;"></div></b></div>
            <div style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
                <div align="center"></div><div id="msg" align="center"></div>
                <div id="okbtn" align="center" style="margin-top: 20px;"></div>
            </div>
        </div>

        <div id="light" class="white_content"><?php include('mechanics.php') ?></div>
        <div id="light2" class="white_content"><?php include('terms.php') ?></div>
        <div id="light3" class="white_content2"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>
        <div id="fade" class="black_overlay"></div>

        <div id="mainContainer">
            <div id="banner">
                <div id="logo_landing"> <img src="images/theSweepsLogo.png" /></div>
            </div>
            <div id="contentContainer">
                <img id="gaBG" src="images/contentbg.jpg" height="458px" />
                <div id="codeContainer">
                    <div class="codeP"> Please enter your Sweeps Code in the box below.</div>
                    <div class="inBox">
                        <div class="inputBox_left"></div>
                        <div class="inputBox_body"><input type="text" id="txtVoucher" name="txtVoucher" class="inputbox_code" maxlength="20" /></div>
                        <div class="inputBox_right"></div>
                    </div>
                </div>

                <div id="codeContainer2">
                    <div id="keyboard" >
                        <div id="row">
                            <input class="button" name="1" type="button" value="1" size="" />
                            <input class="button2" name="2" type="button" value="2" />
                            <input class="button2" name="3" type="button" value="3" />
                            <input class="button2" name="4" type="button" value="4" />
                            <input class="button2" name="5" type="button" value="5" />
                            <input class="button2" name="6" type="button" value="6" />
                            <input class="button2" name="7" type="button" value="7" />
                            <input class="button2" name="8" type="button" value="8" />
                            <input class="button2" name="9" type="button" value="9" />
                            <input class="button2" name="0" type="button" value="0" />
                            <input class="button2" name="backspace" type="button" value="Backspace" style="width: 100px" />
                        </div>
                        <div id="row1">
                            <input class="button" name="q" type="button" value="q" />
                            <input class="button2" name="w" type="button" value="w" />
                            <input class="button2" name="e" type="button" value="e" />
                            <input class="button2" name="r" type="button" value="r" />
                            <input class="button2" name="t" type="button" value="t" />
                            <input class="button2" name="y" type="button" value="y" />
                            <input class="button2" name="u" type="button" value="u" />
                            <input class="button2" name="i" type="button" value="i" />
                            <input class="button2" name="o" type="button" value="o" />
                            <input class="button2" name="p" type="button" value="p" />
                          <input class="button2" name="shift" type="button" value="Shift" id="shift2" style="width: 100px" />
                        </div>
                        <div id="row1_shift">
                            <input class="button" name="Q" type="button" value="Q" />
                            <input class="button2" name="W" type="button" value="W" />
                            <input class="button2" name="E" type="button" value="E" />
                            <input class="button2" name="R" type="button" value="R" />
                            <input class="button2" name="T" type="button" value="T" />
                            <input class="button2" name="Y" type="button" value="Y" />
                            <input class="button2" name="U" type="button" value="U" />
                            <input class="button2" name="I" type="button" value="I" />
                            <input class="button2" name="O" type="button" value="O" />
                            <input class="button2" name="P" type="button" value="P" />
                            <input class="button2" name="shift" type="button" value="Shift" id="shift2" style="width: 100px" />

                        </div>
                        <div id="row2">
                            <input class="button" name="a" type="button" value="a" />
                            <input class="button2" name="s" type="button" value="s" />
                            <input class="button2" name="d" type="button" value="d" />
                            <input class="button2" name="f" type="button" value="f" />
                            <input class="button2" name="g" type="button" value="g" />
                            <input class="button2" name="h" type="button" value="h" />
                            <input class="button2" name="j" type="button" value="j" />
                            <input class="button2" name="k" type="button" value="k" />
                            <input class="button2" name="l" type="button" value="l" />
                            <input class="button2"  type="button" style="visibility: hidden" />
                            <input id="clearall" class="button2" name="clear" style="width: 100px;" type="button" value="Clear All" />
                        </div>
                       <div id="row2_shift">
                            <input class="button" name="a" type="button" value="A" />
                            <input class="button2" name="s" type="button" value="S" />
                            <input class="button2" name="d" type="button" value="D" />
                            <input class="button2" name="f" type="button" value="F" />
                            <input class="button2" name="g" type="button" value="G" />
                            <input class="button2" name="h" type="button" value="H" />
                            <input class="button2" name="j" type="button" value="J" />
                            <input class="button2" name="k" type="button" value="K" />
                            <input class="button2" name="l" type="button" value="L" />
                            <input class="button2"  type="button" style="visibility: hidden" />
                            <input id="clearall" class="button2" name="clear" style="width: 100px;" type="button" value="Clear All" />
                        </div>
                        <div id="row3">
                            <input class="button" name="z" type="button" value="z" />
                            <input class="button2" name="x" type="button" value="x" />
                            <input class="button2" name="c" type="button" value="c" />
                            <input class="button2" name="v" type="button" value="v" />
                            <input class="button2" name="b" type="button" value="b" />
                            <input class="button2" name="n" type="button" value="n" />
                            <input class="button2" name="m" type="button" value="m" />
                            <input class="button2" id="leftarrow" name="leftarrow" type="button" value="<-" />
                            <input class="button2" id="rightarrow" name="rightarrow" type="button" value="->" />
                        </div>
                       <div id="row3_shift">
                            <input class="button" name="Z" type="button" value="Z" />
                            <input class="button2" name="X" type="button" value="X" />
                            <input class="button2" name="C" type="button" value="C" />
                            <input class="button2" name="V" type="button" value="V" />
                            <input class="button2" name="B" type="button" value="B" />
                            <input class="button2" name="N" type="button" value="N" />
                            <input class="button2" name="M" type="button" value="M" />
                            <input class="button2" id="leftarrow" name="leftarrow" type="button" value="<-" />
                            <input class="button2" id="rightarrow" name="rightarrow" type="button" value="->" />
                        </div>
                    </div>
                    <div class="code_btn">
                        <div class="code_submit" onclick="enter_sweeps_code();"></div>
                        <div class="code_back" onclick="window.location.href='launchpad.php'"></div>
                    </div>
                </div>
            </div>
            <div id="footer">
                    <div id="footerBox">
                    <div class="footerBox_left"></div>
                    <div class="footerBox_body">
                        <div class="under18"></div>
                        <div class="rules" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</div>
                        <div class="terms" onclick="document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</div>
                    </div>
                    <div class="footerBox_right"></div>
                </div>
             </div>
        </div>
    </body>
</html>