<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 22, 2012
 */
include('../controller/redemptionprocess.php');
?>
<?php include('header.php');?>
<script type="text/javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
    function process(terminal,transid,winnings,cardcnt,sid,transref)
    {
        document.getElementById("hidTransID").value = transid;
        document.getElementById("hidTransRef").value = transref;
        document.getElementById("hidRowID").value = cardcnt;
        document.getElementById("hidWinnings").value = winnings;
        document.getElementById("hidTerminal").value = terminal;
        var title = "REDEMPTION CONFIRMATION";
        
        if (winnings.indexOf(',') == 0)
        {
            winnings = winnings.substr(1);
        }
        var msg = "Are you sure you want to process redemption of " + winnings + " for " + terminal + "?";
        document.getElementById('title1').innerHTML = title;
        document.getElementById('msg1').innerHTML = msg;
        document.getElementById('light1').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }
</script>
<div id="page">
    <p>
        Page: <?php echo $pgTransactionHistory?>
    </p>
    <table style="border-color:#000000; width:1000px;">
        <tr width="100px;">
            <td colspan="4" align="right">
                <a href="redemption.php" class="labelbutton2" style="text-decoration:none; color:#000; font-weight:bold;">REFRESH</a>
            </td>
        </tr>
        <tr><td colspan="4"></td></tr>
        <tr><td colspan="4"></td></tr>
        <tr><td colspan="4"></td></tr>
        <tr>
            <th class="th">Terminal Name</th>
            <th class="th">Transaction Reference ID</th>
            <th class="th">Redeemable Winnings</th>
            <th class="th">Action</th>
        </tr>
    <?php if(count($redeemablewinnings_list) > 0):?>
        <?php for($i = 0 ; $i < count($redeemablewinnings_list) ; $i++):?>
        <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
        <tr class="<?php echo $class;?>">
            <td class="td"><?php echo $redeemablewinnings_list[$i]["Name"]?></td>
            <td class="td"><?php echo $redeemablewinnings_list[$i]["TransactionSummID"]?></td>
            <td class="td"><?php if (strpos($redeemablewinnings_list[$i]["Winnings"],',') == 0){ echo substr($redeemablewinnings_list[$i]["Winnings"],1); } else {echo $redeemablewinnings_list[$i]["Winnings"];} ?></td>
            <td class="td">
                <a href="#" onclick="javascript:return process('<?php echo $redeemablewinnings_list[$i]['Name']?>','<?php echo $redeemablewinnings_list[$i]['TransactionSummaryID']?>','<?php echo $redeemablewinnings_list[$i]['Winnings']?>','<?php echo $redeemablewinnings_list[$i]['CardCount']?>','<?php echo $_SESSION['sid']?>','<?php echo $redeemablewinnings_list[$i]['TransactionSummID']?>')">Process</a>
            </td>
        </tr>
        <?php endfor;?>
    <?php else:?>
        <tr>
            <td colspan="7" style="text-align: center;">No Records Found.</td>
        </tr>
    <?php endif;?>
    </table>
    <?php echo $hidTransID;?>
    <?php echo $hidTransRef;?>
    <?php echo $hidRowID;?>
    <?php echo $hidWinnings;?>
    <?php echo $hidTerminal;?>
    <div id="fade" class="black_overlay"></div>
    <div id="light1" class="white_content">
        <div class="light-title" id="title1"></div>
        <div class="light-message" id="msg1"></div>
        <?php echo $btnOkay;?>
        <input id="btnCancel" type="submit" value="" class="inputBoxEffectPopupCancel"/>
    </div>
    <div id="light2" class="white_content">
        <div class="light-title" id="title2"></div>
        <div class="light-message" id="msg2"></div>
        <div type="button" class="inputBoxEffectPopup" id="button2" style="margin-left:auto;margin-right:auto;"  onclick = "window.location = 'redemption.php'"></div>
    </div>
</div>
<?php if(isset($error_title)):?>
<script>
    document.getElementById('title2').innerHTML = "<?php echo $error_title;?>";
    document.getElementById('msg2').innerHTML = "<?php echo $error_msg;?>";
    document.getElementById('light2').style.display='block';
    document.getElementById('fade').style.display='block';
</script>
<?php endif;?>
<?php include('footer.php');?>