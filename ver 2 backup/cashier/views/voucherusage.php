<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 05, 2012
 */
include('../controller/voucherusageprocess.php');
?>
<script language="javascript" src="jscripts/checking.js"></script>
<?php include('header.php');?>
<script type="text/javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
</script>
<div id="page">
    <table width="1200px">
        <tr>
            <td align="center">
                <table cellspacing="3">
                    <tr>
                        <td class="labelbold2">From: &nbsp;
                            <?php echo $txtDateFr;?>
                            <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
                        </td>
                        <td class="labelbold2">To: &nbsp;
                            <?php echo $txtDateTo;?>
                            <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
                        </td>
                        <td><?php echo $btnSearch;?></td>
                    </tr>
                    <tr>
                        <td class="labelbold2">Voucher Code: &nbsp;</td>
                        <td colspan="2" ><?php echo $txtVoucherCode;?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <?php if(isset($displayrecords)):?>
                <table style="overflow-x:scroll; width:100%;">
                    <tr>
                        <th colspan ="4" style="height:30px;background-color: #5C5858;color:#FFFFFF;">VOUCHER USAGE HISTORY</th>
                    </tr>
                    <tr>
                        <th class="th">Date & Time Generated</th>
                        <th class="th">Voucher Code</th>
                        <th class="th">Voucher Points</th>
                        <th class="th">Usage Details</th>
                    </tr>
                    <?php if(count($voucherusagesdtls) > 0):?>
                    <p class="paging"><b>Displaying <?php echo $start;?>-<?php echo ($end > $transhistcount) ? $transhistcount : $end;?> of <?php echo $transhistcount;?></b></p>
                    <?php for($i = 0 ; $i < count($voucherusagesdtls) ; $i++):?>
                    <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
                    <tr class="<?php echo $class;?>">
                        <td class="td"><?php echo date_format(date_create($voucherusagesdtls[$i]['DateGenerated']), "m/d/Y H:i:s");?></td>
                        <td class="td"><?php echo $voucherusagesdtls[$i]['VoucherNo']?></td>
                        <td class="td"><?php echo $voucherusagesdtls[$i]['Winnings']?></td>
                        <td class="td">
                            <table>
                                <tr>
                                    <td><b>Status:</b></td>
                                    <?php
                                    if($voucherusagesdtls[$i]['Status'] == 4)
                                    {
                                        $status = "Unused";
                                    }
                                    else if($voucherusagesdtls[$i]['Status'] == 3)
                                    {
                                        $status = "Expired";
                                    }
                                    else if($voucherusagesdtls[$i]['Status'] == 2)
                                    {
                                        $status = "Used";
                                    }
                                    else
                                    {
                                        $status = "Unused ***";
                                    }
                                    ?>
                                    <td><?php echo $status?></td>
                                </tr>
                                <tr>
                                    <td><b>Date & Time Used:</b></td>
                                    <td><?php echo date_format(date_create($voucherusagesdtls[$i]['DateUsed']), "m/d/Y h:i:s A");?></td>
                                </tr>
                                <tr>
                                    <td><b>Terminal:</b></td>
                                    <td><?php echo $voucherusagesdtls[$i]['Name']?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php endfor;?>
                    <?php else:?>
                    <tr>
                        <td colspan="4" align="center"><b>No Records Found.</b></td>
                    </tr>
                    <?php endif;?>
                    <tr>
                        <td colspan="4" align="center"><?php echo $pgTransactionHistory;?></td>
                    </tr>
                </table>
                <?php endif;?>
            </td>
        </tr>
    </table>
</div>
<div id="light" class="white_content">
    <div class="light-title" id="title"></div>
    <div class="light-msg" id="msg"></div>
    <div class="inputBoxEffectPopup" style="margin-left:auto;margin-right:auto;" onclick ="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';"></div>
</div>
<div id="fade" class="black_overlay"></div>
<?php include('footer.php');?>
