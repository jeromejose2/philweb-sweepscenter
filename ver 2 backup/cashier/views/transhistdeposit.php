<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 31, 2012
 */
include('../controller/transhistdepositprocess.php');
?>
<?php include('header.php');?>
<script type="text/javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
</script>
<div id="page">
    <table width="1200px">
        <tr>
            <td align="right"><?php include_once('menutrans.php'); ?>
        </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="labelbold2">From:</td>
                        <td>
                            <?php echo $txtDateFr;?>
                            <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
                        </td>
                        <td class="labelbold2">To:</td>
                        <td>
                            <?php echo $txtDateTo;?>
                            <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
                        </td>
                        <td class="labelbold2">Terminal:&nbsp;<?php echo $ddlTerminal;?></td>
                        <td width="100px"><?php echo $btnSearch;?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <?php if(isset($displayrecords)):?>
                <table style="overflow-x:scroll; width:100%;" >
                    <tr>
                        <th colspan ="4" style="height:30px;background-color: #5C5858;color:#FFFFFF;">SALES HISTORY</th>
                    </tr>
                    <tr>
                        <th class="th">Transaction Reference ID</th>
                        <th class="th">Gaming Terminal</th>
                        <th class="th">Date & Time of Transaction</th>
                        <th class="th">Points</th>
                    </tr>
                    <?php if(count($transhistdtls) > 0):?>
                    <p class="paging"><b>Displaying <?php echo $start;?>-<?php echo ($end > $transhistcount) ? $transhistcount : $end;?> of <?php echo $transhistcount;?></b></p>
                    <?php for($i = 0 ; $i < count($transhistdtls) ; $i++):?>
                    <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
                    <tr class="<?php echo $class;?>">
                        <td class="td"><?php echo $transhistdtls[$i]["TransID"];?></td>
                        <td class="td"><?php echo $transhistdtls[$i][0];?></td>
                        <td class="td"><?php echo date_format(date_create($transhistdtls[$i]["TransDate"]), "m/d/Y H:i:s");?></td>
                        <td class="td"><?php echo $transhistdtls[$i]["Amount"];?></td>
                    </tr>
                    <?php endfor;?>
                    <?php else: ?>
                    <tr>
                        <td colspan="4" align="center"><b>No Records Found.</b></td>
                    </tr>
                    <?php endif;?>
                    <?php if($selectedpage == $totalpages):?>
                    <tr class="evenrow">
                        <td class="labelbold">Total Sales</td>
                        <td class="labelbold"><?php echo $totaldtls[0]['total']?></td>
                    </tr>
                    <?php endif;?>
                    <tr>
                        <td colspan="7" align="center"><?php echo $pgTransactionHistory;?></td>
                    </tr>
                </table>
                <?php endif;?>
            </td>
        </tr>
    </table>
</div>
<div id="light" class="white_content">
    <div class="light-title" id="title"></div>
    <div class="light-msg" id="msg"></div>
    <div class="inputBoxEffectPopup" style="margin-left:auto;margin-right:auto;" onclick ="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';"></div>
</div>
<div id="fade" class="black_overlay"></div>
<?php include('footer.php');?>