<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 21, 2012
 */
include('../controller/printvoucherprocess.php');
?>
<?php include('header.php');?>
<script type="text/javascript">
    function disableText()
    {
        var amt = document.getElementById('ddlAmount').options[document.getElementById('ddlAmount').selectedIndex].value;
        if (amt == 0)
        {
            document.getElementById('txtAmount').disabled = false;
        }
        else
        {
            document.getElementById('txtAmount').disabled = true;
            document.getElementById('txtAmount').focus();
        }
    }
    function checkinput()
    {
        var bcf = "<?php echo $_SESSION['Bal']; ?>";
        var title = "INVALID DEPOSIT";
        var msg = "Please indicate the gaming terminal to be deposited.";
        var terminal = document.getElementById('ddlTerminal').options[document.getElementById('ddlTerminal').selectedIndex].value;
        var terminalname = document.getElementById("ddlTerminal").options[document.getElementById("ddlTerminal").selectedIndex].text;
        var ddlamount = document.getElementById('ddlAmount').options[document.getElementById('ddlAmount').selectedIndex].value;
        var txtamount = document.getElementById('txtAmount').value;
        if(ddlamount != 0)
        {
            var amount = ddlamount;
        }
        else
        {
            var amount = txtamount;
        }

        if (terminal == 0 && ddlamount == 0)
        {
            document.getElementById('title1').innerHTML = title;
            document.getElementById('msg1').innerHTML = msg;
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
        if (terminal == 0)
        {
            document.getElementById('title1').innerHTML = title;
            document.getElementById('msg1').innerHTML = msg;
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
        
        if (ddlamount == 0 && txtamount.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
        {
            msg = "Please indicate the amount of points to be deposited.";
            document.getElementById('title1').innerHTML = title;
            document.getElementById('msg1').innerHTML = msg;
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }

        bcf = bcf.replace(/,/g,'');
        if (eval(amount) > bcf)
        {
            title = "INSUFFICIENT SCF";
            msg = "You do not have sufficient SCF on your account. Please contact Customer Service to replenish SCF.";
            document.getElementById('title1').innerHTML = title;
            document.getElementById('msg1').innerHTML = msg;
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }

        var title = "DEPOSIT CONFIRMATION";
        var msg = "Are you sure you want to load " + amount + " points to " + terminalname + "?";
        document.getElementById('title2').innerHTML = title;
        document.getElementById('msg2').innerHTML = msg;
        document.getElementById('light2').style.display='block';
        document.getElementById('fade').style.display='block';
        return true;
    }

    function printpage(url)
    {
        child = window.open(url, "", "height=1, width=1");
        window.focus();
        window.location = 'activesession.php';
    }
</script>
<div id="page">
    <div id="innerpage">
        <table style="margin-left:auto;margin-right:auto;text-align:left;">
            <tr>
                <td class="labelbold2" align="right">Gaming Terminal:&nbsp;</td>
                <td>
                    <?php echo $ddlTerminal;?>
                </td>
            </tr>
            <tr>
                <td class="labelbold2" align="right">Points:&nbsp;</td>
                <td><?php echo $ddlAmount;?>&nbsp;&nbsp;<?php echo $txtAmount;?>
                </td>
            </tr>
            <tr>
            <td colspan="2" style="text-align:center;">
            <?php echo $btnStartSession;?>
            </td>
            </tr>
        </table>
    </div>
</div>
<div id="light1" class="white_content">
    <div class="light-title" id="title1"></div>
    <div class="light-msg" id="msg1"></div>
    <div type="button" class="inputBoxEffectPopup" id="button1" style="margin-left:auto;margin-right:auto;"  onclick ="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';"></div>
</div>
<div id="light2" class="white_content">
    <div class="light-title" id="title2"></div>
    <div class="light-msg" id="msg2"></div>
    <?php echo $btnOkay;?>
    <input id="btnCancel" type="button" value="" class="inputBoxEffectPopupCancel" onclick ="document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';javascript:location.reload(true)"/>
</div>
<div id="fade" class="black_overlay"></div>
<?php if(isset($error_msg)):?>
<script>
    document.getElementById('title1').innerHTML = "<?php echo $error_title?>";
    document.getElementById('msg1').innerHTML = "<?php echo $error_msg?>";
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
</script>
<?php endif;?>
<?php if(isset($url)):?>
<script>
    printpage(<?php echo $url?>);
</script>
<?php endif;?>
<?php include('footer.php');?>