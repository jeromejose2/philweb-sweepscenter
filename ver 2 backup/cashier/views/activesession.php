<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 21, 2012
 */
include("../controller/activesessionprocess.php");
?>
<?php include("header.php");?>
<div id="page">
    <table width="800">
        <tr>
        <th class="th">Terminal Number/Name</th>
        <th class="th">Time In</th>
        <th class="th">Point Balance</th>
        <th class="th">Activity</th>
        </tr>
        <?php for($i = 0 ; $i < count($activesessions) ; $i++):?>
        <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
        <tr class="<?php echo $class;?>">
            <td class="td"><?php echo $activesessions[$i]["Name"];?></td>
            <td class="td"><?php echo $activesessions[$i]["DateStart"];?></td>
            <td class="td"><?php echo $activesessions[$i]["PtBalance"];?></td>
            <td class="td"><?php echo $activesessions[$i]["Activity"];?></td>
        </tr>
        <?php endfor;?>
        <tr>
        <td colspan="6">&nbsp;</td>
        </tr>
        <tr>
        <td colspan="6" align="right">
        <a href="activesession.php" class="labelbutton2" style="text-decoration:none; color:#000; font-weight:bold; padding:5px;">REFRESH</a>&nbsp;&nbsp;
        </td>
        </tr>
    </table>
</div>
<?php include("footer.php");?>
