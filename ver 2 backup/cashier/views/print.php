<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 07, 2012
 */
?>
<html>
    <head>
        <link rel="stylesheet" href="css/print.css" type="text/css" media="print"/>
        <script type="text/javascript">
        function print_page()
        {
           window.print();
           window.close();
        }
        </script>
    <?php
        $amt = $_GET['points'];
        $terminal = $_GET['terminal'];
        $refnum = $_GET['refnum'];
        $transdate = $_GET['transdate'];
    ?>
    <body onload="javascript: return print_page()">
        <img src="images/receipt_logo.bmp" height="100px" width="150px" style="padding-left: 9%;"/><br/>
        <div style="text-align: justify;"><strong>Thank you for purchasing The Sweeps
                                            Center Internet browsing time
                                            credits equivalent to <?php echo $amt;?> Points! Log
                                            on to your designated terminal to
                                            start your browsing session.</strong></div><br/><br/>

            Your Purchase Details are:<br/><br/>
            Internet Time Credits: <b><?php echo $amt;?> Points</b><br/><br/>
            Terminal Number: <b><?php echo $terminal;?></b><br/><br/>
            Receipt Number: <b><?php echo $refnum;?></b><br/><br/>
            Date & Time of Purchase: <br /><b><?php echo $transdate;?></b><br/><br/>

        <div style="text-align: justify"><strong>Terms & Conditions:</strong><br/><br/>
                <b>1)</b> The Sweeps Center is open for individuals 18 years and over.<br/><br/>
                <b>2)</b> Credits are loaded to chosen terminals as points. These points shall be for the sole purpose of availing services inside the Sweeps Center and not in any other services.<br/><br/>
                <b>3)</b> The participant agrees that points loaded to the terminals are non-refundable and non-replaceable.<br/><br/>
                <b>4)</b> The Sweeps Center can only guarantee one electronic sweepstakes from the purchase of Internet time credits. <br/><br/>
                <b>5)</b> The participant acknowledges that playing the Sweeps games does not always guarantee increase in number of electronic sweepstakes entries he/she will get at the end of his session.<br/><br/>
        </div>
            <div style="margin-left: 6.8%;"><strong>Guam Sweepstakes Corporation</strong></div>
    </body>
</html>