<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 21, 2012
 */
include("../controller/managesessionprocess.php");
$headerinfo = "";
for ($i = 0; $i < count($javascripts); $i++)
{
    $js = $javascripts[$i];
    $headerinfo .= "<script language='javascript' type='text/javascript' src='$js'></script>";
}
for ($i = 0; $i < count($stylesheets); $i++)
{
    $css = $stylesheets[$i];
    $headerinfo .= "<link rel='stylesheet' type='text/css' media='screen' href='$css' />";
}
?>
<html>
    <head>
        <script type="text/javascript">
            function preventBackandForward()
            {
                window.history.forward();
            }

            preventBackandForward();
            window.onload=preventBackandForward();
            window.inhibited_load=preventBackandForward;
            window.onpageshow=function(evt){if(evt.persisted)preventBackandForward();};
            window.inhibited_unload=function(){void(0);};

            function check_page()
            {
                $.ajax({
                url: 'heartbeat.php',
                type : 'post',
                success : function(data)
                {

                },
                error: function(e)
                {
                alert("Error");
                }
                });
            }
        </script>
        <title><?php echo $project_title;?></title>
        <?php echo $headerinfo; ?>
    </head>
    <body onload="<?php if ($pagename == 'activesession') {?>timedRefresh(30000);<?php } else {?> setInterval ('check_page()', 600000); <?php } ?>">
        <form id="frmTemplate" name="frmTemplate" method="post">
            <div id="header"  style="background-image: url(images/HeaderStrip.png); height: 216px; background-repeat:repeat;">
                <div style="background-image: url(images/SweepsLogo.png); height:139px; width:293px; float:left; margin-top:10px;"></div>
                <div style="background-image: url(images/WhiteBox.png); height:61px; width:387px; float:left; margin-left:-293px; margin-top:150px;">
                    <div class="labelboldbig" style="margin-top:20px;">
                    SC Fund:
                    <?php if(isset($_SESSION["Bal"])): ?>
                        <?php echo $_SESSION["Bal"];?>
                    <?php else: ?>
                        <script type="text/javascript">
                        <!--
                        window.location = "index.php";
                        //-->
                        </script>
                    <?php endif; ?>
                    </div>
                </div>
                <div style="width:300px; height:100px; float:right; margin-top:70px;">
                <a href="changepassword.php" class="labelboldredlink">Change Password</a>&nbsp;&nbsp;<a href="logout.php" style="font-size:1.2em; font-weight:bold; cursor:pointer; color:#660033;">Log Out</a>
                <br /><br /><br />
                <div class="labelboldred">
                Current Date: <span id="servdate" name="servdate">&nbsp;</span>
                <br />
                Current Time: <span id="clock">&nbsp;</span>
                </div>
                </div>
            </div>
            <script type="text/javascript">
                <!--
                var tempDate = "<?php echo $serverdatetime; ?>";
                var tempTime = "<?php echo $serverdatetime; ?>";
                tempDate = tempDate.substring(5,7) + "/" + tempDate.substring(8,10) + "/" + tempDate.substring(0,4);
                document.getElementById("servdate").firstChild.nodeValue = tempDate;


                <?php $startdate = date("F d, Y H:i:s"); ?>
                var timesetter = new Date('<?php echo $startdate ?>');
                var TimeNow = '';
                function MakeTime() {
                timesetter.setTime(timesetter.getTime()+1000);
                var hhN  = timesetter.getHours();
                if(hhN > 12){
                var hh = String(hhN - 12);
                var AP = 'PM';
                } else if(hhN == 12) {
                var hh = '12';
                var AP = 'PM';
                }else if(hhN == 0){
                var hh = '12';
                var AP = 'AM';
                }else{
                var hh = String(hhN);
                var AP = 'AM';
                }
                var mm  = String(timesetter.getMinutes());
                var ss  = String(timesetter.getSeconds());
                TimeNow = ((hh < 10) ? ' ' : '') + hh + ((mm < 10) ? ':0' : ':') + mm + ((ss < 10) ? ':0' : ':') + ss + ' ' + AP;
                document.getElementById("clock").firstChild.nodeValue = TimeNow;
                setTimeout(function(){
                MakeTime();},1000);
                }
                MakeTime();

                function timedRefresh(timeoutPeriod)
                {
                setTimeout("location.reload(true);",timeoutPeriod);
                }
            </script>
            <div id="containertemplate">
                <div id="menu">
                    <?php
                    include_once('menu.php');
                    ?>
                </div>
                <div id="maincontent">