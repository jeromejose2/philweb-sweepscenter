<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 01, 2012
 */
include('../controller/transhistpayoutprocess.php');
?>
<?php include('header.php');?>
<script type="text/javascript">
    function SelectedIndexChange(index)
    {
        selectedindex = document.getElementById("hdnSelectedProviderID");
        selectedindex.value = index;
        document.forms[0].submit();
    }
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
</script>
<div id="page">
    <table width="1200px">
        <tr>
            <td align="right"><?php include_once('menutrans.php'); ?>
        </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="labelbold2">From:
                            <?php echo $txtDateFr;?>
                            <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" />
                        </td>
                        <td class="labelbold2">To:
                            <?php echo $txtDateTo;?>
                            <img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" />
                        </td>
                    </tr>
                    <tr>
                        <td class="labelbold2">Entry Type:&nbsp;<?php echo $ddlEntryType;?></td>
                        <td class="labelbold2">Terminal:&nbsp;<?php echo $ddlTerminal;?></td>
                        <td width="100px"><?php echo $btnSearch;?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <?php if(isset($displayrecords)): ?>
                <table style="overflow-x:scroll; width:100%;">
                    <tr>
                        <th colspan ="5" style="height:30px;background-color: #5C5858;color:#FFFFFF;">REDEMPTION HISTORY</th>
                    </tr>
                    <tr>
                        <th class="th">Transaction Reference ID</th>
                        <th class="th">Terminal Name</th>
                        <th class="th">Redeemed Winnings</th>
                        <th class="th">Entry Type</th>
                        <th class="th">Date & Time Claimed</th>
                    </tr>
                    <?php if(count($transhistpayoutdtls) > 0):?>
                    <p class="paging"><b>Displaying <?php echo $start;?>-<?php echo ($end > $transhistcount) ? $transhistcount : $end;?> of <?php echo $transhistcount;?></b></p>
                    <?php for($i = 0 ; $i < count($transhistpayoutdtls) ; $i++):?>
                    <?php ($i % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
                    <tr class="<?php echo $class;?>">
                        <td class="td"><?php echo $transhistpayoutdtls[$i]['TransSumm'];?></td>
                        <td class="td"><?php echo $transhistpayoutdtls[$i]['Name'];?></td>
                        <td class="td"><?php if (strpos($transhistpayoutdtls[$i]["Winnings"],',') == 0){ echo substr($transhistpayoutdtls[$i]["Winnings"],0); } else {echo $transhistpayoutdtls[$i]["Winnings"];} ?></td>
                        <td class="td"><?php echo $transhistpayoutdtls[$i]['EntryType'];?></td>
                        <td class="td"><?php echo date_format(date_create($transhistpayoutdtls[$i]['DateClaimed']), "m/d/Y H:i:s");?></td>
                    </tr>
                    <?php endfor;?>
                    <?php else: ?>
                    <tr>
                        <td colspan="5" align="center"><b>No Records Found.</b></td>
                    </tr>
                    <?php endif;?>
                    <?php if($selectedpage == $totalpages):?>
                    <tr class="evenrow">
                        <td class="labelbold" colspan="2" style="text-align:right;">Total Cash Redemptions:</td>
                        <td class="labelbold" colspan="3" style="text-align:left;">$<?php echo number_format($cashdtls[0]['TotalCash'] , 2 , '.' , ',')?></td>
                    </tr>
                    <tr class="evenrow">
                        <td class="labelbold" colspan="2" style="text-align:right;">Total Value of Gas Coupon Redeemed:</td>
                        <td class="labelbold" colspan="3" style="text-align:left;">$<?php echo number_format($noncashdtls[0]['TotalNonCash'] , 2 , '.' , ',')?></td>
                    </tr>
                    <?php endif;?>
                    <tr>
                        <td colspan="5" align="center"><?php echo $pgTransactionHistory;?></td>
                    </tr>
                </table>
                <?php endif;?>
            </td>
        </tr>
    </table>
</div>
<div id="light" class="white_content">
    <div class="light-title" id="title"></div>
    <div class="light-msg" id="msg"></div>
    <div class="inputBoxEffectPopup" style="margin-left:auto;margin-right:auto;" onclick ="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';"></div>
</div>
<div id="fade" class="black_overlay"></div>
<?php include('footer.php');?>