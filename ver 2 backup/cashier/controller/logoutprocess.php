<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 06, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_AccountSessions");

$cacctsessions = new SCC_AccountSessions();

$cacctsessions->StartTransaction();
$cacctsessions->Logout($_SESSION['sid']);
if($cacctsessions->HasError)
{
    $cacctsessions->RollBackTransaction();
}
else
{
    $cacctsessions->CommitTransaction();
	session_destroy();
    header("location: index.php");  
}
?>
