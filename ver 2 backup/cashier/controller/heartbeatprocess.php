<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 07, 2012
 */
include('../init.inc.php');
include("../config.php");

App::LoadLibrary("MicrogamingAPI.class.php");
App::LoadLibrary("microseconds.php");

App::LoadModuleClass("SweepsCenter", "SCCSM_AgentSessions");

$sessionguid = $csmagentsessions->SelectSessionGUID();
$sessionGUID_MG = $sessionguid[0]["SessionGUID"];
$ip_add_MG = $_SERVER['REMOTE_ADDR'];

$headers = "<AgentSession xmlns='https://entservices.totalegame.net'>".
       "<SessionGUID>".$sessionGUID_MG."</SessionGUID>".
       "<IPAddress>".$ip_add_MG."</IPAddress>".
       "<ErrorCode>0</ErrorCode>".
       "<IsLengthenSession>true</IsLengthenSession>".
       "</AgentSession>";

$client = new nusoap_client($mg_url, 'wsdl');
$client->setHeaders($headers);
$param = array('delimitedAccountNumbers' => 'SWCJBP1');
/*$filename = "../APILogs/logs.txt";
$fp = fopen($filename , "a");
fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GetAccountBalance || ".'SWCJBP1'." || ".serialize($param)."\r\n");
fclose($fp);*/
$result = $client->call('GetAccountBalance', $param);

$balance = $result['GetAccountBalanceResult']['BalanceResult']['Balance'];
?>