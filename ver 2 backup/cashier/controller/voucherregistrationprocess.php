<?php

/*
 * Author :             Noel Antonio
 * Date Created:        September 20, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCCV_PromotionalCoupons");
App::LoadModuleClass("SweepsCenter", "SCCV_VoucherInfo");
App::LoadModuleClass("SweepsCenter", "SCCV_AuditLog");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$cvpromotionalcoupons = new SCCV_PromotionalCoupons();
$cvvoucherinfo = new SCCV_VoucherInfo();
$cvauditlog = new SCCV_AuditLog();

$txtFullName = new TextBox("txtFullName", "txtFullName");
$txtFullName->Length = 75;
$txtBday = new TextBox("txtBday", "txtBday");
$txtBday->Style = "display: none";
$txtAddress = new TextBox("txtAddress", "txtAddress");
$txtAddress->Args = "size='34px'";
$txtAddress->Length = 300;
$txtContact = new TextBox("txtContact", "txtContact");
$txtContact->Args = "onkeypress='javascript: return isNumberKey(event);'";
$txtContact->Length = 20;
$txtEmail = new TextBox("txtEmail", "txtEmail");
$txtEmail->Length = 100;
$txtCN = new TextBox("txtCN", "txtCN");
$txtCN->Length = 20;

$btnSubmit = new Button("btnSubmit", "btnSubmit", "SUBMIT");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Args = "onclick='javascript: return checkvoucherinput();'";

$btnOkay = new Button("btnOkay", "btnOkay", " ");
$btnOkay->CssClass = "inputBoxEffectPopup";
$btnOkay->IsSubmit = true;

$fproc = new FormsProcessor();
$fproc->AddControl($txtFullName);
$fproc->AddControl($txtBday);
$fproc->AddControl($txtAddress);
$fproc->AddControl($txtContact);
$fproc->AddControl($txtEmail);
$fproc->AddControl($txtCN);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnOkay);
$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    $ds = new DateSelector("now");
    $currentdate = $ds->CurrentDate;
    $txtBday->SubmittedValue = $_POST['txtBday_year'] . '-' . $_POST['txtBday_month'] . '-' . $_POST['txtBday_day'];
    
    $age = getAge($txtBday->SubmittedValue);
    if ($age >= 18)
    {
            $fullname = $txtFullName->SubmittedValue;
            $gender = $_POST['rdogen'];
            $bday = $txtBday->SubmittedValue;
            $address = $txtAddress->SubmittedValue;
            $email = $txtEmail->SubmittedValue;
            $contact = $txtContact->SubmittedValue;
            $cn = $txtCN->SubmittedValue;

            $playerexist = $cvpromotionalcoupons->SelectExistingPlayer($fullname, $email, $currentdate);
            if (count($playerexist) == 0)
            {
                $cvpromotionalcoupons->StartTransaction();
                $cvvoucherinfo->StartTransaction();

                $voucherinfo = $cvvoucherinfo->SelectAvailableVoucher();
                if (count($voucherinfo) > 0)
                {
                    $id = $voucherinfo[0]["ID"];
                    $voucherno = $voucherinfo[0]["VoucherNo"];

                    $cvvoucherinfo->UpdateAvailableVoucher($id);
                    if ($cvvoucherinfo->HasError)
                    {
                        $cvvoucherinfo->RollBackTransaction();
                        $error_title = "ERROR!";
                        $error_msg = $cvvoucherinfo->getErrors();
                    }
                    else
                    {
                        $cvvoucherinfo->CommitTransaction();

                        $pcinfo["FullName"] = $fullname;
                        $pcinfo["Gender"] = $gender;
                        $pcinfo["Birthday"] = $bday;
                        $pcinfo["MailingAddress"] = $address;
                        $pcinfo["ContactNumber"] = $contact;
                        $pcinfo["EmailAddress"] = $email;
                        $pcinfo["CouponNo"] = $cn;
                        $pcinfo["VoucherNo"] = $voucherno;
                        $pcinfo["DateRegistered"] = 'now_usec()';
                        $pcinfo["Status"] = 0;
                        $cvpromotionalcoupons->Insert($pcinfo);
                        if ($cvpromotionalcoupons->HasError)
                        {
                            $cvpromotionalcoupons->RollBackTransaction();
                            $error_title = "ERROR!";
                            $error_msg = $cvpromotionalcoupons->getErrors();
                        }
                        else
                        {
                            $cvpromotionalcoupons->CommitTransaction();

                            $error_title = "SUCCESSFUL!";
                            $error_msg = 'Coupon Number: ' . $cn . '<br/>' . ' Voucher Code: ' . $voucherno . '';
                        }
                    }
                }
                else
                {
                    $error_title = "ERROR!";
                    $error_msg = "No voucher available.";
                }
            }
            else
            {
                $error_title = "ERROR!";
                $error_msg = "The player already played today.";
            }
    }
    else
    {
                $error_title = "AGE LIMIT";
                $error_msg = "The Sweeps Center prohibits participation of individuals below 18 years of age.";
    }
}
?>
