<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 21, 2012
 * Updated By: Tere Calderon
 * Updated On: September 26, 2012
 * Purpose:    Update for RTG API
 */
require_once("../init.inc.php");
$pagename = "printvoucher";

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCC_TransactionLogs");
App::LoadModuleClass("SweepsCenter", "SCC_AccountSessions");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
App::LoadModuleClass("SweepsCenter", "SCC_Sites");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessionDetails");
App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
App::LoadLibrary("microseconds.php");

App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
App::LoadSettings("swcsettings.inc.php");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$cterminals = new SCC_Terminals();
$ctranslog = new SCC_TransactionLogs();
$cacctsessions = new SCC_AccountSessions();
$cterminalsessions = new SCC_TerminalSessions();
$csites = new SCC_Sites();
$cterminalsessiondtls = new SCC_TerminalSessionDetails();
$caudittrail = new SCC_AuditTrail();

$capiwrapper = new RealtimeGamingAPIWrapper($_CONFIG["rtg_url"],  RealtimeGamingAPIWrapper::CASHIER_API, $_CONFIG["certFilePath"], $_CONFIG["keyFilePath"]);
$capicashier = new RealtimeGamingCashierAPI();

$startsession_form = new FormsProcessor();

$terminals = $cterminals->SelectTerminalsForStartSession($_SESSION['siteid']);
$terminal_list = new ArrayList();
$terminal_list->AddArray($terminals);
$ddlTerminal = new ComboBox("ddlTerminal","ddlTerminal","Gaming Terminal: ");
$options = null;
$options[] = new ListItem("----","0",true);
$ddlTerminal->Items = $options;
$ddlTerminal->DataSource = $terminal_list;
$ddlTerminal->DataSourceText = "Name";
$ddlTerminal->DataSourceValue = "ID";
$ddlTerminal->DataBind();

$ddlAmount = new ComboBox("ddlAmount","ddlAmount","Points:");
$options = null;
$options[] = new ListItem("----","0",true);
$options[] = new ListItem("1","1");
$options[] = new ListItem("5","5");
$options[] = new ListItem("10","10");
$options[] = new ListItem("20","20");
$options[] = new ListItem("50","50");
$options[] = new ListItem("100","100");
$ddlAmount->Items = $options;
$ddlAmount->Args = "onchange='javascript: return disableText();'";

$txtAmount = new TextBox("txtAmount","txtAmount","Amount");
$txtAmount->Length = 10;
$txtAmount->Args = "maxlength=\"10\"  style=\"width:100px;\" onkeypress='javascript: return isNumberKey(event);'";
//onblur='javascript: return onchange_amounttxtbox();'

$btnStartSession = new Button("btnStartSession","btnStartSession","START SESSION");
$btnStartSession->CssClass = "labelbutton2";
$btnStartSession->Args = 'onclick="javascript: return checkinput();"';

$btnOkay = new Button("btnOkay","btnOkay"," ");
$btnOkay->IsSubmit = true;
$btnOkay->CssClass = "inputBoxEffectPopup";

$startsession_form->AddControl($ddlTerminal);
$startsession_form->AddControl($ddlAmount);
$startsession_form->AddControl($txtAmount);
$startsession_form->AddControl($btnStartSession);
$startsession_form->AddControl($btnOkay);

$startsession_form->ProcessForms();

if ($startsession_form->IsPostBack)
{
    if($btnOkay->SubmittedValue == " ")
    { 
        //check if cashier session exists
        $sessionsdtls = $cacctsessions->SelectSessionDetails($_SESSION['sid']);
        if(count($sessionsdtls) > 0)
        {
            $terminalsessionid = udate('YmdHisu');
            $terminal = $ddlTerminal->SubmittedValue;
            if($ddlAmount->SubmittedValue != 0)
            {
                $amount = $ddlAmount->SubmittedValue;
            }
            else
            {
                $amount = $txtAmount->SubmittedValue;
            }            
            //check the terminal's session
            $terminalsessiondtls = $cterminalsessions->SelectTerminalSessionDetails($terminal);
            if(count($terminalsessiondtls) > 0)
            {
                $error_title = "ERROR";
                $error_msg = "Terminal session already exists.";
            }
            else
            {     
                //check the terminal's balance
                $terminaldtls = $cterminals->SelectTerminalDetails($terminal);
                if(count($terminaldtls) > 0)
                {
                    $error_title = "ERROR";
                    $error_msg = "Terminal is already in use.";
                }
                else
                {
                    $isFreeEntrydtls = $cterminals->SelectTerminalName($terminal);
                    $isFreeEntry = $isFreeEntrydtls[0]['IsFreeEntry'];
                    $siteid = $isFreeEntrydtls[0]['SiteID'];
                    $acctid = $isFreeEntrydtls[0]['ID'];
                    //check if the deposit amount exceeds the site's SCF
                    $sitedtls = $csites->SelectSiteDetails($siteid);
                    $sitebalance = $sitedtls[0]['Balance'];
                    if($sitebalance < $amount)
                    {
                        $error_title = "ERROR";
                        $error_msg = "Insufficient SCF.";
                    }
                    else
                    {
                        //check the deposit amount
                        if(($amount == 0) && ($isFreeEntry != 1))
                        {
                            $error_title = "ERROR";
                            $error_msg = "Initial deposit amount must be greater than 0.";
                        }
                        else
                        {
                            //get the terminal's name
                            $arrterminalname = $cterminals->SelectTerminalName($terminal);
                            $terminalname = $arrterminalname[0]["Name"];
                            $param = array('delimitedAccountNumbers' => $terminalname);
                            //insert to logs before the get balance api call
                            $filename = "../../APILogs/logs.txt";
                            $fp = fopen($filename , "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GetAccountBalance || ".$terminalname." || ".serialize($param)."\r\n");
                            fclose($fp);

                            $a = 1;
                            while($a < 4)
                            {
                                //call the get balance api
                                $rtgbalance = $capiwrapper->GetBalance($terminalname);  
                                if ($rtgbalance['IsSucceed'] == 1)
                                {
                                    $balance = $rtgbalance['BalanceInfo']['Balance'];
                                    break;
                                }
                                else
                                {
                                    $a++;
                                }
                                break;
                            }
                            //insert to logs after the get balance api call
                            $filename = "../../APILogs/logs.txt";
                            $fp = fopen($filename , "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GET TERMINAL BALANCE(DEPOSIT) || ".$terminalname." || ".serialize($rtgbalance)."\r\n");
                            fclose($fp);
                            //check if balance is greater than zero (otherwise, ther terminal must not be allowed to deposit)
                            if ($balance > 0)
                            {
                                $error_title = "INVALID DEPOSIT";
                                $error_msg = "Casino balance must be zero.";
                            }
                            else
                            {
                                //insert to tbl_transactionlogs table
                                $ctranslog->StartTransaction();
                                $arrTransactionLog["TerminalSessionID"] = $terminalsessionid;
                                $arrTransactionLog["TerminalID"] = $terminal;
                                $arrTransactionLog["ServiceID"] = 4;
                                $arrTransactionLog["DateCreated"] = 'now_usec()';
                                $arrTransactionLog["DateUpdated"] = 'now_usec()';
                                $arrTransactionLog["TransactionType"] = 'D';
                                $arrTransactionLog["Amount"] = $amount;
                                $arrTransactionLog["LaunchPad"] = 0;
                                $ctranslog->Insert($arrTransactionLog);

                                if($ctranslog->HasError)
                                {
                                    $ctranslog->RollBackTransaction();
                                    $error_title = "ERROR";
                                    $error_msg = "INSERT Failed - Failed to insert in tbl_transactionlogs";
                                }
                                else
                                {      
                                    $ctranslog->CommitTransaction();
                                    //insert to logs
                                    $param = array('delimitedAccountNumbers' => $terminalname);                    
                                    $filename = "../../DBLogs/logs.txt";
                                    $fp = fopen($filename , "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || INSERT INTO tbl_transactionlogs: " . serialize($arrTransactionLog) . "\r\n");
                                    fclose($fp);

                                    //insert to logs before the deposit api call
                                    $param = array('accountNumber' => $terminalname, 'amount' => $amount, 'currency' => 1);                
                                    $filename = "../../APILogs/logs.txt";
                                    $fp = fopen($filename , "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: Deposit || ".$terminalname." || ".serialize($param)."\r\n");
                                    fclose($fp);

                                    //call the deposit api
                                    $rtgdeposit = $capiwrapper->Deposit($terminalname,$amount,$tracking1 = '',$tracking2 = '',$tracking3 = '',$tracking4 = ''); 
                                    $rtgtransstatus = $rtgdeposit['TransactionInfo']['DepositGenericResult']['transactionStatus'];
                                    $rtgtransid = $rtgdeposit['TransactionInfo']['DepositGenericResult']['transactionID'];
                                    if ($rtgdeposit['TransactionInfo']['DepositGenericResult']['transactionStatus'] == 'TRANSACTIONSTATUS_APPROVED')
                                    {
                                        $rtgtransstat = '1';  
                                        //insert to logs after the reload api call
                                        $filename = "../../APILogs/logs.txt";
                                        $fp = fopen($filename , "a");
                                        fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || ".serialize($rtgdeposit)."\r\n");
                                        fclose($fp);                        
                                        //update the tbl_transactionlogs table
                                        $ctranslog->StartTransaction();                        
                                        $ctranslog->UpdateTransactionLog($rtgtransid, $rtgtransstatus, $rtgtransstat, $terminalsessionid);
                                        if($ctranslog->HasError)
                                        {
                                            $ctranslog->RollBackTransaction();
                                            $error_title = "ERROR";
                                            $error_msg = "Update Failed - Failed to update tbl_transactionlogs";
                                        }
                                        else
                                        {
                                            $ctranslog->CommitTransaction();
                                            //insert to db logs the updating of tbl_transactionlogs
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp = fopen($filename , "a");
                                            fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || UPDATE tbl_transactionlogs: " . $rtgtransid .';'. $rtgtransstatus .';'. $rtgtransstat .';'. $terminalsessionid . "\r\n");
                                            fclose($fp);
                                            if($isFreeEntry != 1)
                                            {
                                                //update the site's balance
                                                $newbalance = ($sitebalance - $amount);
                                                $csites->StartTransaction();
                                                $csites->UpdateSiteBalance($newbalance, $siteid);
                                                if($csites->HasError)
                                                {
                                                    $csites->RollBackTransaction();
                                                    $error_title = "ERROR";
                                                    $error_msg = "Error updating site balance.";
                                                }
                                                else
                                                {
                                                    $csites->CommitTransaction();
                                                    //insert to db logs
                                                    $filename = "../../DBLogs/logs.txt";
                                                    $fp = fopen($filename , "a");
                                                    fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || UPDATE tbl_sites: " . $siteid .';'. $newbalance . " \r\n");
                                                    fclose($fp);
                                                    //update the terminal's balance
                                                    $cterminals->StartTransaction();
                                                    $cterminals->UpdateTerminalBalanceServiceIDEquivalentPoints($amount,$terminal);
                                                    if($cterminals->HasError)
                                                    {
                                                        $cterminals->RollBackTransaction();
                                                        $error_title = "ERROR";
                                                        $error_msg = "Error updating terminal balance.";
                                                    }
                                                    else
                                                    {
                                                        $cterminals->CommitTransaction();
                                                        //insert to db logs
                                                        $filename = "../../DBLogs/logs.txt";
                                                        $fp = fopen($filename , "a");
                                                        fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || UPDATE tbl_terminals: " . $terminal .';'. $newbalance . " \r\n");
                                                        fclose($fp);
                                                        //insert into the terminal sessions table
                                                        $cterminalsessions->StartTransaction();
                                                        $arrTerminalSessions["TerminalID"] = $terminal;
                                                        $arrTerminalSessions["SiteID"] = $siteid;
                                                        $arrTerminalSessions["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                                                        $arrTerminalSessions["DateStart"] = 'now_usec()';
                                                        $arrTerminalSessions["DateCreated"] = 'now_usec()';
                                                        $cterminalsessions->Insert($arrTerminalSessions);
                                                        if($cterminalsessions->HasError)
                                                        {
                                                            $cterminalsessions->RollBackTransaction();
                                                            $error_title = "INVALID START SESSION";
                                                            $error_msg = "Error inserting new terminal session.";
                                                        }
                                                        else
                                                        {
                                                            $cterminalsessions->CommitTransaction();
                                                            $terminalsessionlastinsertid = $cterminalsessions->LastInsertID;
                                                            //insert to db logs
                                                            $filename = "../../DBLogs/logs.txt";
                                                            $fp = fopen($filename , "a");
                                                            fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || INSERT TO tbl_terminalsessions: " . serialize($arrTerminalSessions) . " \r\n");
                                                            fclose($fp);
                                                            //formulate SWC deposit reference id
                                                            $terminalsessiondetails = $cterminalsessiondtls->SelectMaxId();
                                                            $lastTransId = ($terminalsessiondetails[0]["max"] != null) ? $terminalsessiondetails[0]["max"] : 0;
                                                            $lastTransId = ($lastTransId + 1);
                                                            $transRefId = 'D' . str_pad($terminalsessionlastinsertid,6,0,STR_PAD_LEFT) . str_pad($lastTransId,10,0,STR_PAD_LEFT);
                                                            //insert the terminal session details
                                                            $cterminalsessiondtls->StartTransaction();
                                                            $arrTerminalSessionDetails["TerminalSessionID"] = $terminalsessionlastinsertid;
                                                            $arrTerminalSessionDetails["TransactionType"] = 'D';
                                                            $arrTerminalSessionDetails["Amount"] = $amount;
                                                            $arrTerminalSessionDetails["ServiceID"] = '1';
                                                            $arrTerminalSessionDetails["ServiceTransactionID"] = $mgtransid;
                                                            $arrTerminalSessionDetails["TransID"] = $transRefId;
                                                            $arrTerminalSessionDetails["AcctID"] = $acctid;
                                                            $arrTerminalSessionDetails["TransDate"] = 'now_usec()';
                                                            $arrTerminalSessionDetails["DateCreated"] = 'now_usec()';
                                                            $cterminalsessiondtls->Insert($arrTerminalSessionDetails);
                                                            if($cterminalsessiondtls->HasError)
                                                            {
                                                                $cterminalsessiondtls->RollBackTransaction();
                                                                $error_title = "ERROR";
                                                                $error_msg = "Error inserting new terminal session details.";
                                                            }
                                                            else
                                                            {
                                                                $cterminalsessiondtls->CommitTransaction();
                                                                //insert to db logs
                                                                $filename = "../../DBLogs/logs.txt";
                                                                $fp = fopen($filename , "a");
                                                                fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || INSERT TO tbl_terminalsessiondetails: " . serialize($arrTerminalSessionDetails) . " \r\n");
                                                                fclose($fp);
                                                                //insert in the audit trail table
                                                                $caudittrail->StartTransaction();
                                                                $arrAuditTrail["SessionID"] = $_SESSION['sid'];
                                                                $arrAuditTrail["AccountID"] = $acctid;
                                                                $arrAuditTrail["TransDetails"] = 'Session start for terminalid: ' . $terminal;
                                                                $arrAuditTrail["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                                                                $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                                                $caudittrail->Insert($arrAuditTrail);
                                                                if($caudittrail->HasError)
                                                                {
                                                                    $caudittrail->RollBackTransaction();
                                                                    $error_title = "ERROR";
                                                                    $error_msg = "Error inserting in audit trail.";
                                                                }
                                                                else
                                                                {
                                                                    $caudittrail->CommitTransaction();
                                                                    //insert to db logs
                                                                    $filename = "../../DBLogs/logs.txt";
                                                                    $fp = fopen($filename , "a");
                                                                    fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || INSERT TO tbl_audittrail: " . serialize($arrAuditTrail) . " \r\n");
                                                                    fclose($fp);
                                                                    $transdate = date("Y-m-d h:i:s A");
                                                                    $url = "\"print.php?points='".$amount."'&terminal='".$terminalname."'&refnum='".$transRefId."'&transdate='".$transdate . "'\"";
                                                                }
                                                            }
                                                        }
                                                    }                                                    
                                                }
                                            }
                                            else
                                            {
                                                $error_title = "ERROR";
                                                $error_msg = "Terminal is not valid for loading.";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $rtgtransstat = '2';
                                        $ctranslog->StartTransaction();
                                        $ctranslog->UpdateTransactionLog($rtgtransid, $rtgtransstatus, $rtgtransstat, $terminalsessionid);
                                        if($ctranslog->HasError)
                                        {
                                            $ctranslog->RollBackTransaction();
                                            $error_title = "ERROR";
                                            $error_msg = "Update Failed - Failed to update tbl_transactionlogs";
                                        }
                                        else
                                        {
                                            $ctranslog->CommitTransaction();
                                            //insert to logs
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp = fopen($filename , "a");
                                            fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: DEPOSIT || ".$terminalname." || UPDATE tbl_transactionlogs : " . $rtgtransid .';'. $rtgtransstatus .';'. $rtgtransstat .';'. $terminalsessionid . " \r\n");
                                            fclose($fp);
                                            $error_title = "ERROR";
                                            $error_msg = "Transaction denied. Please try again later.";
                                        }
                                    }
                                }      
                            }
                        }
                    }    
                }    
            }
        }
        else
        { 
            $error_title = "ERROR";
            $error_msg = "Cashier account session does not exist or has expired. Kindly log-in again.";
        }   
    }
}
?>
