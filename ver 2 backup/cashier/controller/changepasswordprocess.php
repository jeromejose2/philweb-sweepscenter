<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 06, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_Accounts");
App::LoadModuleClass("SweepsCenter", "SCC_AccountSessions");
App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");

App::LoadControl("TextBox");
App::LoadControl("Button");

$caccounts = new SCC_Accounts();
$cacctsessions = new SCC_AccountSessions();
$caudittrail = new SCC_AuditTrail();
$pagename = "changepassword";

$changepassword_form = new FormsProcessor();

$txtPassword = new TextBox("txtPassword","txtPassword","Password");
$txtPassword->Password = true;
$txtPassword->Length = 12;
$txtPassword->Args = 'style="width:200px;" onkeypress="javascript: return isAlphaNumericKey(event);"';

$txtNewPassword = new TextBox("txtNewPassword","txtNewPassword","New Password");
$txtNewPassword->Password = true;
$txtNewPassword->Length = 12;
$txtNewPassword->Args = 'style="width:200px;" onkeypress="javascript: return isAlphaNumericKey(event);"';

$txtConfPassword = new TextBox("txtConfPassword","txtConfPassword","Confirm Password");
$txtConfPassword->Password = true;
$txtConfPassword->Length = 12;
$txtConfPassword->Args = 'style="width:200px;" onkeypress="javascript: return isAlphaNumericKey(event);"';

$btnSubmit = new Button("btnSubmit","btnSubmit","SUBMIT");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Args = "onclick='javascript: return checkinput();'";
$btnSubmit->Style = "width: 10%; color:#000; font-weight: bold;";

$btnOkay = new Button("btnOkay","btnOkay","OKAY");
$btnOkay->IsSubmit = true;
$btnOkay->CssClass = "labelbold2";

$changepassword_form->AddControl($txtPassword);
$changepassword_form->AddControl($txtNewPassword);
$changepassword_form->AddControl($txtConfPassword);
$changepassword_form->AddControl($btnSubmit);
$changepassword_form->AddControl($btnOkay);

$changepassword_form->ProcessForms();

if($changepassword_form->IsPostBack)
{
    if($btnOkay->SubmittedValue == "OKAY")
    {
        $session = $_SESSION['sid'];
        $password = $txtPassword->SubmittedValue;
        $newpassword = $txtNewPassword->SubmittedValue;

        $sessiondtls = $cacctsessions->SelectSessionDetails($session);
        if(count($sessiondtls) > 0)
        {
            $acctid = $sessiondtls[0]['ID'];
            $siteid = $sessiondtls[0]['SiteID'];
            $email = $sessiondtls[0]['Email'];
            $pword = $sessiondtls[0]['Password'];
            $uname = $sessiondtls[0]['Username'];
            $acctname = $sessiondtls[0]['FirstName'] . " " . $sessiondtls[0]['MiddleName'] . " " . $sessiondtls[0]['LastName'];

            if($pword != MD5($password))
            {
                $error_title = "ERROR";
                $error_msg = "Your passwords information do not match. Please try again.";
            }
            else
            {
                if($pword == MD5($newpassword))
                {
                    $error_title = "ERROR";
                    $error_msg = "You have entered an invalid password. Please try again.";
                }
                else
                {
                    $caccounts->StartTransaction();
                    $caccounts->UpdatePassword($newpassword, $acctid);
                    if($caccounts->HasError)
                    {
                        $caccounts->RollBackTransaction();
                        $error_title = "ERROR";
                        $error_msg = "Error updating the accounts table.";
                    }
                    else
                    {
                        $caccounts->CommitTransaction();

                        $caudittrail->StartTransaction();
                        $audittrail_arr["SessionID"] = $session;
                        $audittrail_arr["AccountID"] = $acctid;
                        $audittrail_arr["TransDetails"] = 'Change password for AcctID: '. $acctid;
                        $audittrail_arr["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                        $audittrail_arr["TransDateTime"] = 'now_usec()';
                        $caudittrail->Insert($audittrail_arr);
                        if($caudittrail->HasError)
                        {
                            $caudittrail->RollBackTransaction();
                            $error_title = "ERROR";
                            $error_msg = "Error inserting in audit trail.";
                        }
                        else
                        {
                            $caudittrail->CommitTransaction();
                            $error_title = "SUCCESSFUL PASSWORD CHANGE";
                            $error_msg = "You have successfully changed your password. Please check your e-mail for your new log in credentials.";

                            $subject='THE SWEEPS CENTER NOTIFICATION FOR CHANGE PASSWORD';
                            $date = date("m/d/Y");
                            $time = date("H:i:s");
                            $to = $email;
                            $message ="<HTML><BODY>
                                    <div>Dear $acctname,</div>
                                    <br />
                                    This is to inform you that your password has been changed on this date $date and time $time. Here are your new Account Details:
                                                      <br /><br />
                                                      <div>Username: <b>$uname</b></div>
                                                      <div>Password: <b>$newpassword</b></div>
                                    <br /><br />
                                    If you didn't perform this procedure, please report this to our toll free Customer Service hotline at XXXXXXX or email us at support@thesweepscenter.com.
                                    <br /><br />
                                    Regards,
                                    <br /><br />
                                    The Sweeps Center Team
                                    <br />
                                    Philweb Guam
                                    </BODY></HTML>";

                            $header="From: The Sweeps Center <support@thesweepscenter.com>\r\nReply-To: '$to'\r\nBCC: '$to'\r\nContent-type:text/html";

                            $mail_sent = mail($to,$subject,$message,$header);
                        }
                    }
                }
            }
        }
        else
        {
            $error_title = "ERROR";
            $error_msg = "Account session does not exist.";
        }
    }
}
?>