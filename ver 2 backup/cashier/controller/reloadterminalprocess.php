<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 29, 2012
 * Updated By: Tere Calderon
 * Updated On: September 26, 2012
 * Purpose:    Update for RTG API
 */
require_once("../init.inc.php");
$pagename = "reloadterminal";

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessions");
App::LoadModuleClass("SweepsCenter", "SCC_TransactionLogs");
App::LoadModuleClass("SweepsCenter", "SCC_AccountSessions");
App::LoadModuleClass("SweepsCenter", "SCC_Sites");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessionDetails");
App::LoadModuleClass("SweepsCenter", "SCC_AuditTrail");
App::LoadLibrary("microseconds.php");

App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
App::LoadSettings("swcsettings.inc.php");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$cterminals = new SCC_Terminals();
$cterminalsessions = new SCC_TerminalSessions();
$ctranslog = new SCC_TransactionLogs();
$cacctsessions = new SCC_AccountSessions();
$csites = new SCC_Sites();
$cterminalsessiondtls = new SCC_TerminalSessionDetails();
$caudittrail = new SCC_AuditTrail();

$capiwrapper = new RealtimeGamingAPIWrapper($_CONFIG["rtg_url"],  RealtimeGamingAPIWrapper::CASHIER_API, $_CONFIG["certFilePath"], $_CONFIG["keyFilePath"]);
$capicashier = new RealtimeGamingCashierAPI();

$reloadsession_form = new FormsProcessor();

$terminals = $cterminals->SelectTerminalsForReloadSession($_SESSION['siteid']);
$terminal_list = new ArrayList();
$terminal_list->AddArray($terminals);
$ddlTerminal = new ComboBox("ddlTerminal","ddlTerminal","Terminal");
$options = null;
$options[] = new ListItem("----","0",true);
$ddlTerminal->Items = $options;
$ddlTerminal->DataSource = $terminal_list;
$ddlTerminal->DataSourceText = "Name";
$ddlTerminal->DataSourceValue = "ID";
$ddlTerminal->DataBind();
$ddlTerminal->CssClass = "options";

$ddlAmount = new ComboBox("ddlAmount","ddlAmount","Points:");
$options = null;
$options[] = new ListItem("----","0",true);
$options[] = new ListItem("1","1");
$options[] = new ListItem("5","5");
$options[] = new ListItem("10","10");
$options[] = new ListItem("20","20");
$options[] = new ListItem("50","50");
$options[] = new ListItem("100","100");
$ddlAmount->Items = $options;
$ddlAmount->CssClass = "options";
$ddlAmount->Args = 'onchange="javascript: return onchange_amount();"';

$txtAmount = new TextBox("txtAmount","txtAmount","Amount");
$txtAmount->Length = 10;
$txtAmount->Args = 'style="width:100px;" onkeypress="javascript: return isNumberKey(event);"';

$btnReloadSession = new Button("btnReloadSession","btnReloadSession","RELOAD SESSION");
$btnReloadSession->Args = 'onclick="javascript: return onSubmit();"';
$btnReloadSession->CssClass = "labelbutton2";

$btnOkay = new Button("btnOkay","btnOkay"," ");
$btnOkay->IsSubmit = true;
$btnOkay->CssClass = "inputBoxEffectPopup";

$reloadsession_form->AddControl($ddlTerminal);
$reloadsession_form->AddControl($ddlAmount);
$reloadsession_form->AddControl($txtAmount);
$reloadsession_form->AddControl($btnReloadSession);
$reloadsession_form->AddControl($btnOkay);

$reloadsession_form->ProcessForms();
if ($reloadsession_form->IsPostBack)
{
    if($btnOkay->SubmittedValue == " ")
    {
        //check if cashier session exists
        $sessionsdtls = $cacctsessions->SelectSessionDetails($_SESSION['sid']);
        if(count($sessionsdtls) > 0)
        {        
            $acctid = $sessionsdtls[0]['ID'];
            $siteid = $sessionsdtls[0]['SiteID'];
            $terminalid = $ddlTerminal->SubmittedValue;
            if($ddlAmount->SubmittedValue != 0)
            {
                $amount = $ddlAmount->SubmittedValue;
            }
            else
            {
                $amount = $txtAmount->SubmittedValue;
            }
            //get the terminal's name
            $arrterminalname = $cterminals->SelectTerminalName($terminalid);
            $terminalname = $arrterminalname[0]["Name"];
            $terminalsessionid = udate('YmdHisu');
            //get the terminal's session details if it is still active 
            $sessiondtls = $cterminalsessions->SelectTerminalSessionDetails($terminalid);
            if(count($sessiondtls) > 0)
            {
                $terminalsessid = $sessiondtls[0]['ID'];
                //insert to tbl_transactionlogs table
                $ctranslog->StartTransaction();
                $arrTransactionLog["TerminalSessionID"] = $terminalsessionid;
                $arrTransactionLog["TerminalID"] = $terminalid;
                $arrTransactionLog["ServiceID"] = 4;
                $arrTransactionLog["DateCreated"] = 'now_usec()';
                $arrTransactionLog["DateUpdated"] = 'now_usec()';
                $arrTransactionLog["TransactionType"] = 'R';
                $arrTransactionLog["Amount"] = $amount;
                $arrTransactionLog["LaunchPad"] = 0;
                $ctranslog->Insert($arrTransactionLog);
                if($ctranslog->HasError)
                {
                    $ctranslog->RollBackTransaction();
                    $error_title = "ERROR";
                    $error_msg = "INSERT Failed - Failed to insert in tbl_transactionlogs";
                }
                else
                {
                    $ctranslog->CommitTransaction();
                    $param = array('delimitedAccountNumbers' => $terminalname);
                    //insert to logs
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename , "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: RELOAD || ".$terminalname." || INSERT INTO tbl_transactionlogs: " . serialize($arrTransactionLog) . "\r\n");
                    fclose($fp);
                    //insert to logs before the get balance api call
                    $filename = "../../APILogs/logs.txt";
                    $fp = fopen($filename , "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GetAccountBalance || ".$terminalname." || ".serialize($param)."\r\n");
                    fclose($fp);
                    $a = 1;
                    while($a < 4)
                    {
                        //call the get balance api
                        $rtgbalance = $capiwrapper->GetBalance($terminalname);                    
                        if ($rtgbalance['IsSucceed'] == 1)
                        {
                            $balance = $rtgbalance['BalanceInfo']['Balance'];
                            break;
                        }
                        else
                        {
                            $a++;
                        }
                        break;
                    }
                    //insert to logs after the get balance api call
                    $filename = "../../APILogs/logs.txt";
                    $fp = fopen($filename , "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: GET TERMINAL BALANCE(RELOAD) || ".$terminalname." || ".serialize($rtgbalance)."\r\n");
                    fclose($fp);
                    //insert to logs before the deposit api call
                    $param = array('accountNumber' => $terminalname, 'amount' => $amount, 'currency' => 1);
                    $filename = "../../APILogs/logs.txt";
                    $fp = fopen($filename , "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: Reload || ".$terminalname." || ".serialize($param)."\r\n");
                    fclose($fp);
                    //call the deposit api
                    $rtgreload = $capiwrapper->Deposit($terminalname,$amount,$tracking1 = '',$tracking2 = '',$tracking3 = '',$tracking4 = ''); 
                    $rtgtransstatus = $rtgreload['TransactionInfo']['DepositGenericResult']['transactionStatus'];
                    $rtgtransid = $rtgreload['TransactionInfo']['DepositGenericResult']['transactionID'];
                    if ($rtgreload['TransactionInfo']['DepositGenericResult']['transactionStatus'] == 'TRANSACTIONSTATUS_APPROVED')
                    {
                        $rtgtransstat = '1';  
                        //insert to logs after the reload api call
                        $filename = "../../APILogs/logs.txt";
                        $fp = fopen($filename , "a");
                        fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: RELOAD || ".$terminalname." || ".serialize($rtgreload)."\r\n");
                        fclose($fp);
                        //update the tbl_transactionlogs table
                        $ctranslog->StartTransaction();                        
                        $ctranslog->UpdateTransactionLog($rtgtransid, $rtgtransstatus, $rtgtransstat, $terminalsessionid);
                        if($ctranslog->HasError)
                        {
                            $ctranslog->RollBackTransaction();
                            $error_title = "ERROR";
                            $error_msg = "Update Failed - Failed to update tbl_transactionlogs";
                        }
                        else
                        {
                            $ctranslog->CommitTransaction();
                            //insert to db logs the updating of tbl_transactionlogs
                            $filename = "../../DBLogs/logs.txt";
                            $fp = fopen($filename , "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: RELOAD || ".$terminalname." || UPDATE tbl_transactionlogs: " . $rtgtransid .';'. $rtgtransstatus .';'. $rtgtransstat .';'. $terminalsessionid . "\r\n");
                            fclose($fp);

                            //get site details
                            $sitedtls = $csites->SelectSiteDetails($siteid);
                            $balance = $sitedtls[0]['Balance'];
                            //check if SCF still enough
                            if($balance < $amount)
                            {
                                $error_title = "ERROR";
                                $error_msg = "Insufficient SCF.";
                            }
                            else
                            {
                                $newbalance = ($balance - $amount);
                                //update the site's balance
                                $csites->StartTransaction();                                
                                $csites->UpdateSiteBalance($newbalance, $siteid);
                                if($csites->HasError)
                                {
                                    $csites->RollBackTransaction();
                                    $error_title = "ERROR";
                                    $error_msg = "Error updating site balance.";
                                }
                                else
                                {
                                    $csites->CommitTransaction();
                                    //insert to db logs the updating of tbl_sites                                  
                                    $filename = "../../DBLogs/logs.txt";
                                    $fp = fopen($filename , "a");
                                    fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: RELOAD || ".$terminalname." || UPDATE tbl_sites: " . $siteid .';'. $newbalance . " \r\n");
                                    fclose($fp);
                                    //update the terminal's balance
                                    $cterminals->StartTransaction();                                    
                                    $cterminals->UpdateTerminalBalance($newbalance, $terminalid);
                                    if($cterminals->HasError)
                                    {
                                        $cterminals->RollBackTransaction();
                                        $error_title = "ERROR";
                                        $error_msg = "Error updating terminal balance.";
                                    }
                                    else
                                    {
                                        $cterminals->CommitTransaction();
                                        //formulate SWC reload reference id
                                        $terminalsessiondetails = $cterminalsessiondtls->SelectMaxId();
                                        $lastTransId = ($terminalsessiondetails[0]["max"] != null) ? $terminalsessiondetails[0]["max"] : 0;
                                        $lastTransId = ($lastTransId + 1);
                                        $transRefId = 'R' . str_pad($terminalsessid,6,0,STR_PAD_LEFT) . str_pad($lastTransId,10,0,STR_PAD_LEFT);
                                        //insert the terminal session details
                                        $cterminalsessiondtls->StartTransaction();
                                        $arrTerminalSessionDetails["TerminalSessionID"] = $terminalsessid;
                                        $arrTerminalSessionDetails["TransactionType"] = 'R';
                                        $arrTerminalSessionDetails["Amount"] = $amount;
                                        $arrTerminalSessionDetails["ServiceID"] = '1';
                                        $arrTerminalSessionDetails["ServiceTransactionID"] = $rtgtransid;
                                        $arrTerminalSessionDetails["TransID"] = $transRefId;
                                        $arrTerminalSessionDetails["AcctID"] = $acctid;
                                        $arrTerminalSessionDetails["TransDate"] = 'now_usec()';
                                        $arrTerminalSessionDetails["DateCreated"] = 'now_usec()';
                                        $cterminalsessiondtls->Insert($arrTerminalSessionDetails);
                                        if($cterminalsessiondtls->HasError)
                                        {
                                            $cterminalsessiondtls->RollBackTransaction();
                                            $error_title = "ERROR";
                                            $error_msg = "Error inserting new terminal session details.";
                                        }
                                        else
                                        {
                                            $cterminalsessiondtls->CommitTransaction();
                                            //insert to db logs
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp = fopen($filename , "a");
                                            fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: RELOAD || ".$terminalname." || INSERT TO tbl_terminalsessiondetails: " . serialize($arrTerminalSessionDetails) . " \r\n");
                                            fclose($fp);
                                            //insert in the audit trail table
                                            $caudittrail->StartTransaction();
                                            $arrAuditTrail["SessionID"] = $_SESSION['sid'];
                                            $arrAuditTrail["AccountID"] = $acctid;
                                            $arrAuditTrail["TransDetails"] = 'Reload for terminalid: ' . $terminalid;
                                            $arrAuditTrail["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                                            $arrAuditTrail["TransDateTime"] = 'now_usec()';
                                            $caudittrail->Insert($arrAuditTrail);
                                            if($caudittrail->HasError)
                                            {
                                                $caudittrail->RollBackTransaction();
                                                $error_title = "ERROR";
                                                $error_msg = "Error inserting in audit trail.";
                                            }
                                            else
                                            {
                                                $caudittrail->CommitTransaction();
                                                //insert to db logs
                                                $filename = "../../DBLogs/logs.txt";
                                                $fp = fopen($filename , "a");
                                                fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: RELOAD || ".$terminalname." || INSERT TO tbl_audittrail: " . serialize($arrAuditTrail) . " \r\n");
                                                fclose($fp);
                                                $transdate = date("Y-m-d h:i:s A");
                                                $url = "\"print.php?points='".$amount."'&terminal='".$terminalname."'&refnum='".$transRefId."'&transdate='".$transdate . "'\"";
                                            }
                                        }
                                    }
                                }
                            }
                        }                    
                    }
                    else
                    {
                        $rtgtransstat = '2';
                        $ctranslog->StartTransaction();
                        $ctranslog->UpdateTransactionLog($rtgtransid, $rtgtransstatus, $rtgtransstat, $terminalsessionid);
                        if($ctranslog->HasError)
                        {
                            $ctranslog->RollBackTransaction();
                            $error_title = "ERROR";
                            $error_msg = "Update Failed - Failed to update tbl_transactionlogs";
                        }
                        else
                        {
                            $ctranslog->CommitTransaction();
                            //insert to logs
                            $filename = "../../DBLogs/logs.txt";
                            $fp = fopen($filename , "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || CASHIER || TRANSACTION TYPE: RELOAD || ".$terminalname." || UPDATE tbl_transactionlogs : " . $rtgtransid .';'. $rtgtransstatus .';'. $rtgtransstat .';'. $terminalsessionid . " \r\n");
                            fclose($fp);
                            $error_title = "ERROR";
                            $error_msg = "Transaction denied. Please try again later.";
                        }
                    }          
                }
            }
            else
            {
                $error_title = "INVALID RELOAD";
                $error_msg = "Terminal does not have an active session.";
            }
        }
        else
        {
            $error_title = "ERROR";
           $error_msg = "Cashier account session does not exist or has expired. Kindly log-in again.";
        }            
    }
}
?>
