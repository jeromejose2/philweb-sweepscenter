<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: May 21, 2012
 */
require_once("../init.inc.php");

$stylesheets[] = "css/default.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$pagename = "activesession";

App::LoadModuleClass("SweepsCenter", "SCC_Terminals");
App::LoadModuleClass("CasinoAPI", "RealtimeGamingAPIWrapper");
App::LoadModuleClass("CasinoAPI", "RealtimeGamingCashierAPI");
App::LoadSettings("swcsettings.inc.php");

$cterminals = new SCC_Terminals();
$capiwrapper = new RealtimeGamingAPIWrapper($_CONFIG["rtg_url"],  RealtimeGamingAPIWrapper::CASHIER_API, $_CONFIG["certFilePath"], $_CONFIG["keyFilePath"]);
$capicashier = new RealtimeGamingCashierAPI();

$activesessions = $cterminals->SelectActiveSessions($_SESSION['siteid']);
if(count($activesessions) > 0)
{
    for ($i = 0 ; $i < count($activesessions) ; $i++ )
    {
        $param = array('delimitedAccountNumbers' => $activesessions[$i]["Name"]);
        $rtgbalance = $capiwrapper->GetBalance($activesessions[$i]["Name"]);
        if ($rtgbalance['IsSucceed'] == 1)
        {
            $balance = $rtgbalance['BalanceInfo']['Balance'];
        }

        $cterminals->StartTransaction();
        $cterminals->UpdateTerminalBalance($balance, $activesessions[$i]["ID"]);
        if($cterminals->HasError)
        {
            $cterminals->RollBackTransaction();
        }
        else
        {
            $cterminals->CommitTransaction();
        }
    }
}
$activesessions = $cterminals->SelectActiveSessions($_SESSION['siteid']);

?>
