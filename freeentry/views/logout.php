<?php

/*
 * @author JFJ 05-18-2012
 * Purpose   : view for logout
 */

require_once("../init.inc.php");
//$modulename = "SweepsCenter";
//App::LoadModuleClass($modulename, "SCC_Terminals");
//APP::LoadModuleClass($modulename, "SCC_TerminalSessions");
//App::LoadModuleClass($modulename,"SCC_AuditTrail");
//
//$tbl_terminal = new SCC_Terminals();
//$terminalSessions = new SCC_TerminalSessions();
//$audittrail = new SCC_AuditTrail();
//$terminalid =$_SESSION["terminalid"];
//$terminalsessionid = $_SESSION['terminalsid'];
if (isset($_SESSION["login"]))
{
    unset($_SESSION['login']);
    unset($_SESSION['changestatus']);
    App::Pr("<script> window.location = 'index.php'; </script>");
}
else
{
    unset($_SESSION['changestatus']);
    App::Pr("<script> window.location = 'index.php'; </script>");
}
//check if id exist
//$checkid = $tbl_terminal->CheckTerminalID($terminalid);
//if(count($checkid) > 0)
//{
//    $checkisfreeentry = $tbl_terminal->CheckIsFreeEntry($terminalid); //$checkisfreeentry[0] 
//    if(count($checkisfreeentry) > 0)
//    {
//        $terminalSessions->StartTransaction();
//        $updateterminalsession = $terminalSessions->UpdateTerminalSessions($terminalid, $terminalsessionid);
//        $terminalSessions->CommitTransaction();
//       if ($terminalSessions->HasError)
//        {
//            $terminalSessions->RollBackTransaction();
//        }else
//        {
//        
//           if($checkisfreeentry[0][0] == "0") 
//           {
//                               
//               $updateserviceid = $tbl_terminal->UpdateServiceID($terminalid);
//               $tbl_terminal->CommitTransaction();
//               if($tbl_terminal->HasError)
//               {
//                   $tbl_terminal->RollBackTransaction();
//               }else
//               {
//           //insert in audittrail
//           $audittrail->StartTransaction();
//           //$audittrailparam["SessionID"] = " ";
//           $audittrailparam["AccountID"] = "0";
//           $audittrailparam["TransDetails"] = "Session end for terminalid:".$terminalid.".";
//           $audittrailparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
//           $audittrailparam["TransDateTime"] = "now_usec()";
//           $audittrail->Insert($audittrailparam);
//           $audittrail->CommitTransaction();
//              
//           if($audittrail->HasError)
//           {
//           $audittrail->RollBackTransaction();
//           }else
//           {
//            session_destroy();
//             App::Pr("<script> window.location = 'index.php'; </script>");
//           }
//               }
//           }else
//           {
//           session_destroy();
//           App::Pr("<script> window.location = 'index.php'; </script>");
//           }
//        }
//        
//    }
//  }
?>
