<?php
/*
 * @author JFJ 05-15-2012
 * Purpose   : view for registration
 */
include ("tc_calendar.php");
include("../controller/registrationprocess.php");
include_once("../controller/managesession.php");
$id = -1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Guam Sweepstakes Caf&#233; Free Entry Registration</title>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/calendar.css" />
        <script type="text/javascript" src="jscript/jquery-1.4.min.js" ></script>
        <script type="text/javascript" src="jscript/jquery-ui-1.7.1.custom.min.js" ></script>
        <script language="javascript" src="jscript/calendar.js"></script>
        <script type="text/javascript" src="jscript/validations.js"></script>
        <script type="text/javascript" src="jscript/disable_copy_paste.js"></script>
    </head>
    <style>
        body{ overflow: hidden;}        
    </style> 
    <script language="javascript">

        function showData()
        {
            var sa = document.getElementById("txtAddress").value;
            document.getElementById('hiddenadd').value = sa;
            
        }

    </script>    
    <body>
        <?php
        ?>
        <div id="mainContainer">
            <div id="banner">
                <div id="logo_landing">
                    <img src="images/theSweepsLogo.png" />
                </div>
            </div>
            <div id="contentContainer" style="margin-top: -70px;">
                <div id="contentContainer4" style ="">
                    <div id=""  style="
                         width:630px;
                         height:500px;
                         left: 50%;
                         top: 50%;
                         margin-left: -290px;
                         margin-top: -205px;
                         position: absolute;

                         ">
                        <div class="regBox" >
                            <div class="regBoxLeft"></div>
                            <div class="regContainer">

                                <div class="regForm">
                                    <br>
                                        <form id="reg_form" name="reg_form" method="post" action="" >
                                            <?php echo $hiddenadd; ?>
                                            <div id="lblLName" class="txtName">*Last Name</div>
                                            <div class="inputbox"><?php echo $txtLName; ?></div>
                                            <div id="lblFName" class="txtName">*Given Name</div>
                                            <div class="inputbox"><?php echo $txtFname; ?></div>
                                            <div id="lblBday" class="txtName">*Birth Date</div>
                                            <div class="inputbox">
                                                <?php
                                                echo $hidbday;
                                                $myCalendar->writeScript();
                                                ?>
                                            </div>
                                            <div id="lblHomePhone" class="txtName">Home Phone</div>
                                            <div class="inputbox"><?php echo $txtHomePhone; ?></div>
                                            <div id="lblMobilePhone" class="txtName">*Mobile Phone</div>
                                            <div class="inputbox"><?php echo $txtMobilePhone; ?></div>
                                            <div id="lblAddress" class="txtName">*Mailing Address </div>
                                            <div class="messagebox">
                                                <?php // echo $txtAddress; ?>
                                                       <textarea maxlength="300" rows='5' cols='30' name='txtAddress' id='txtAddress' onpaste="return false;"  class='messageBoxEffect' onkeypress='javascript: return chkChar(event);' style='resize: none;' ><?php echo $hiddenadd->Text; ?></textarea>
                                      
                                            </div>
                                            <div id="lblEmail" class="txtName">*Email Address</div>
                                            <div class="inputbox"><?php echo $txtEmail; ?></div>
                                            <div class="regNote">(*) Required fields
                                            </div>
                                            <div class="regNote1">
                                                <?php echo $btnSubmit; ?>
                                                <a href="home.php" class="submitBack" style="width: 100px; height: 40px; font-weight: bold; margin-top:-40px; margin-left:300px;"></a>
                                            </div>


                                        </form>


                                </div>   

                            </div>

                            <div class="regBoxRight"></div>
                        </div>
                    </div>
                </div>
                <div id="footer">
                    <div id="footerBox">
                        <div class="footerBox_left"></div>
                        <div class="footerBox_body">
                            <div class="under18"></div>
                            <div class="rules">
                                <a href="#" onclick="document.getElementById('lightMechanics').style.display = 'block';
                                        document.getElementById('fade').style.display = 'block'">Rules &amp; Mechanics</a>
                            </div>
                            <div class="terms"> 
                                <a href="#" onclick="document.getElementById('lightTerms').style.display = 'block';
                                        document.getElementById('fade').style.display = 'block'">Terms &amp; Conditions</a>
                            </div>
                        </div>
                        <div class="footerBox_right"></div>
                    </div>
                </div>


                <div id="lightMechanics" class="white_content2">
                    <div style="background-image:  url(images/close_btn.gif); cursor:pointer; width:14px; height:14px; float:right; margin-top:15px; margin-right:15px; display:inline;" 
                         onclick="document.getElementById('lightMechanics').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';"></div>
                         <?php include('mechanics.php') ?>
                </div>
                <div id="lightTerms" class="white_content2">
                    <div style="background-image:  url(images/close_btn.gif); _background-image: none; _filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/close_btn.gif',sizingMethod='image'); cursor:pointer; width:14px; height:14px; float:right; margin-top:15px; margin-right:15px; display:inline;" 
                         onclick="document.getElementById('lightTerms').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';"></div>
                         <?php include('terms.php') ?>
                </div>
                <!--<div id="fade" class="black_overlay"></div>-->
            </div>

            <div id="light90" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px">
                    <b id="a"><div id="title90"></div></b></div>
                <p id="b"><br/><div class="sansserif" id="msg90"></div></p>
                <br/>
                <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light90').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';"/>
            </div>

            <div id="light91" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px">
                    <b id="a"><div id="title91"></div></b></div>
                <p id="b"><br/><div class="sansserif" id="msg91"></div></p>
                <br/><input type="button" class="inputBoxEffectPopup" onclick ="javascript: return redirect();"/>
            </div>

            <div id="light1" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px"><b id="a">INSUFFICIENT DATA</b></div>
                <br/><br/>
                <p id="b" class="sansserif"><br/>Please enter your last name.</p>
                <br/><br/>
                <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light1').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';
                                        document.getElementById('txtLName').focus();"/>
            </div>

            <div id="light2" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px"><b id="a">INSUFFICIENT DATA</b></div>
                <br/><br/>
                <p id="b" class="sansserif"><br/>Please enter your given name.</p>
                <br/><br/>
                <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light2').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';
                                        document.getElementById('txtFName').focus();"/>

            </div>

            <div id="light3" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px"><b id="a">INSUFFICIENT DATA</b></div>
                <br/><br/>
                <p id="b" class="sansserif"><br/>Please enter at least one contact number.</p>
                <br/><br/>
                <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light3').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';
                                        document.getElementById('txtHomePhone').focus();"/>
            </div>

            <div id="light4" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px"><b id="a">INSUFFICIENT DATA</b></div>
                <br/><br/>
                <p id="b" class="sansserif"><br/>Please enter your mailing address.</p>
                <br/><br/>
                <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light4').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';
                                        document.getElementById('txtAddress').focus();"/>
            </div>
            <div id="light40" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px"><b id="a">INSUFFICIENT DATA</b></div>
                <br/><br/>
                <p id="b" class="sansserif"><br/>Please enter correct mailing address</p>
                <br/><br/>
                <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light40').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';
                                        document.getElementById('txtAddress').focus();"/>
            </div>

            <div id="light5" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px"><b id="a">INSUFFICIENT DATA</b></div>
                <br/><br/>
                <div id="b" class="sansserif"><br/>Please enter your birth date.</div>
                <br/><br/>
                <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light5').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';"/>
            </div>

            <div id="light6" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px"><b id="a">INSUFFICIENT DATA</b></div>
                <br/><br/>
                <div id="b" class="sansserif"><br/>Please enter your e-mail address.</div>
                <br/><br/>
                <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light6').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';
                                        document.getElementById('txtEmail').focus();"/>
            </div>

            <div id="light8" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px"><b id="a">INSUFFICIENT DATA</b></div>
                <br/><br/>
                <div id="b" class="sansserif"><br/>Invalid e-mail address format.</div>
                <br/><br/>
                <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light8').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';
                                        document.getElementById('txtEmail').focus();"/>
            </div>

            <div id="light7" style="text-align: center;font-size: 16pt;height: 270px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px;"><b id="a"><?php echo $title; ?></b></div>
                <br/>
                <p id="b" class="sansserif"><br/><?php echo $msg; ?></p>
                <br/><br/>
                <input type="button" class="inputBoxEffectPopup" onclick ="javascript: return redirect();"/>
            </div>
            <div id="light9" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px"><b id="a">AGE LIMIT</b></div>
                <br/><br/>
                <div id="b" class="sansserif"><br/>The Sweeps Center prohibits participation of individuals below 18 years of age.</div>
                <br/><br/>
                <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light9').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';"/>
            </div>
            <div id="light10" style="text-align: center;font-size: 16pt;height: 220px" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px"><b id="a">ERROR</b></div>
                <br/><br/>
                <div id="b" class="sansserif"><br/>Address must not be greater than 300 characters.</div>
                <br/><br/>
                <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light10').style.display = 'none';
                                        document.getElementById('fade').style.display = 'none';"/>
            </div>
            <div id="fade" class="black_overlay"></div>
        </div>

    </body>
    <script type="text/javascript">


                                    function allowedKeysName(evt)
                                    {
                                        var charCode = (evt.which) ? evt.which : event.keyCode
                                        if ((charCode < 65 && charCode != 32 && charCode != 8) || ((charCode > 90) && (charCode < 97)) || charCode > 122)
                                            return false;

                                        return true;
                                    }

                                    $('#txtLName').live("cut copy paste", function(e) {
                                        e.preventDefault();
                                    });

                                    function chkChar(evt)
                                    {
                                        var charCode = (evt.which) ? evt.which : event.keyCode
                                        if ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || e.keyCode == 9 || k == 32 || (e.keyCode > 36 && e.keyCode <= 40) || (k >= 48 && k <= 57))
//            (charCode == 39)
                                            return false;

                                        return true;
                                    }
                                    function alphanumeric(e)
                                    {
                                        var k;
                                        document.all ? k = e.keyCode : k = e.which;
                                        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || e.keyCode == 9 || k == 32 || (e.keyCode > 36 && e.keyCode <= 40) || (k >= 48 && k <= 57));
                                    }

                                    function error()
                                    {
                                        document.getElementById('light7').style.display = 'block';
                                        document.getElementById('fade').style.display = 'block';
                                        return false;
                                    }

<?php
if ($id >= 0)
{
    ?>
                                        error();
    <?php
}
?>

                                    function redirect()
                                    {
                                        var redir = "<?php echo $id; ?>";
                                        if (redir > 0)
                                        {
                                            document.getElementById('light7').style.display = 'none';
                                            document.getElementById('fade').style.display = 'none';
                                            return false;
                                        }
                                        else
                                        {
                                            window.location = 'home.php';
                                        }
                                    }


    </script>
    <?php if (isset($error_msg)): ?>
        <script>
            document.getElementById('title90').innerHTML = "<?php echo $error_msgtitle; ?>";
            document.getElementById('msg90').innerHTML = "<?php echo $error_msg; ?>";
            document.getElementById('light90').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>
    <?php endif; ?>
    <?php // $msg = "You have successfully sent your registration for the Sweeps Center Free Entry. A Free Entry Code was sent to your e-mail address. Please visit any Sweeps Center branch and enter this code at allocated terminals to avail your one Free Entry Sweepstakes.";  ?>
    <?php if (isset($msg)): ?>
        <script>
            document.getElementById('title91').innerHTML = "CONGRATULATIONS!";
            document.getElementById('msg91').innerHTML = "<?php echo $msg; ?>";
            document.getElementById('light91').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>
    <?php endif; ?> 
    <style>

        div.sansserif{font-family:Arial,Helvetica,sans-serif; font-size:16px;}
        p.sansserif{font-family:Arial,Helvetica,sans-serif; font-size:16px;}
    </style>
</html>
