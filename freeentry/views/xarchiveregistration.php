<?php
/*
 * @author JFJ 05-15-2012
 * Purpose   : view for xarchiveregistration
 */
include("/home/webadmin/www/sweepscenter/freeentry/init.inc.php");
$modulename = "SweepsCenter";

App::LoadModuleClass($modulename, "SCC_FreeEntry");
App::LoadModuleClass($modulename, "SCC_FreeEntryRegistrants");
App::LoadModuleClass($modulename, "SCC_AuditTrail");

$scfreeentry = new SCC_FreeEntry();
$scfreeentryregistrants = new SCC_FreeEntryRegistrants();
$scaudittrail = new SCC_AuditTrail();

$boolrollback = false;
$currdate = date('Y-m-d');
$date = strtotime ( '-1 day' , strtotime ( $currdate ) ) ;
$date = date ( 'Y-m-d' , $date );

$scfreeentry->StartTransaction();
$scfreeentryregistrants->StartTransaction();
$scaudittrail->StartTransaction();

//Insert into free entry registrants table
$arrRegistrants = $scfreeentry->GetFreeEntryRegistrants($date);
$regdtls = $arrRegistrants[0];

if (count($arrRegistrants) > 0)
{
    for ($i = 0; $i < count($arrRegistrants); $i++)
    {    
        $regDetails["LName"] = $arrRegistrants[$i]["LName"];
        $regDetails["FName"] = $arrRegistrants[$i]["FName"];
        $regDetails["BirthDate"] = $arrRegistrants[$i]["BirthDate"];
        $regDetails["HomePhone"] = $arrRegistrants[$i]["HomePhone"];
        $regDetails["MobilePhone"] = $arrRegistrants[$i]["MobilePhone"];
        $regDetails["Address"] = $arrRegistrants[$i]["Address"];
        $regDetails["Email"] = $arrRegistrants[$i]["Email"];
        $regDetails["VoucherCode"] = $arrRegistrants[$i]["VoucherCode"];
        $regDetails["DateRegistered"] = $arrRegistrants[$i]["DateRegistered"];
        $regDetails["DatePlayed"] = $arrRegistrants[$i]["DatePlayed"];
        $regDetails["IsSent"] = $arrRegistrants[$i]["IsSent"];
        $array[] = $regDetails;  
    }

    $scfreeentryregistrants->InsertMultiple($array);
    if ($scfreeentryregistrants->HasError) 
    {
        $errormsg = $scfreeentryregistrants->getError();
        $boolrollback = true;
    } 
    else
    {
        //Truncate free entry table
        $scfreeentry->ExecuteQuery("TRUNCATE TABLE tbl_freeentry");
        if ($scfreeentry->HasError)
        {
            $errormsg = $scfreeentry->getError();
            $boolrollback = true;
        }
        else
        {
            // Log to Audit Trail
            $scaudittrailparam["SessionID"] = "";
            $scaudittrailparam["AccountID"] = "0";
            $scaudittrailparam["TransDetails"] = "Registration archiving cron";
            $scaudittrailparam["TransDateTime"] = "now_usec()";
            $scaudittrailparam["RemoteIP"] = "";
            
            $scaudittrail->Insert($scaudittrailparam);
            if ($scaudittrailparam->HasError) 
            {
                $errormsg = $scaudittrail->getError();
                $boolrollback = true;
            }        
        }
    }
    
    // Commit and Rollback Transactions
    if ($boolrollback)
    {
        $scfreeentryregistrants->RollBackTransaction();
        $scfreeentry->RollBackTransaction();
        $scaudittrail->RollBackTransaction();
    } else {
        $scfreeentryregistrants->CommitTransaction();
        $scfreeentry->CommitTransaction();
        $scaudittrail->CommitTransaction();
    }
}





?>
