<?php
/*
 * @author JFJ 05-16-2012
 * Purpose   : view for home
 */
//include("../controller/homeprocess.php");
include_once("../controller/managesession.php");
APP::LoadModuleClass("SweepsCenter", "SCCV_VoucherInfo");
$cafinovoucher = new SCCV_VoucherInfo();
$vcode = $_SESSION["vcode"];
$checkstatus = $cafinovoucher->CheckStatus($vcode);
$status = $checkstatus[0][0];
$bal = "0.00";
if ($status == 4)
{
    if (isset($_SESSION['bal']) == '0.10')
    {
        $bal = '0.10';
    }
    else
    {
        $bal = '0.00';
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="css/main.css" media="all" rel="stylesheet" type="text/css" />
        <title>Guam Sweepstakes Caf&#233; Free Entry</title>
        <script type="text/javascript" src="jscript/jquery-1.4.min.js" ></script>
        <script type="text/javascript" language="javascript">
            function checkcardsStatus()
            {
                //    alert('checkcard status');
                $.ajax({
                    url: "../controller/homeprocess.php",
                    type: "POST",
                    success: function(data) {
                        //                    alert(data);
                        if (data == "1")
                        {
                            window.location = 'getcards.php';
                        }
                    },
                    error: function() {
                        alert('error');
                    }
                });
            }

        </script>
    </head>
    <body onload ="checkcardsStatus();">
        <div id="popUpDivLPConvert" style="display:none;font-family:Helvetica; font-size: 20px; left: 36.5%;top : 40%;">
            <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 100px;">CONVERT POINTS CONFIRMATION</div></b></div>
            <div id="popup_container_home" style="margin-left:30px; margin-top:30px; width:460px; height:60px; font-weight:bold;">
                <div id="convert1" align="center"></div><div id="convert_img" align="center"><p>Are you sure you want to convert your 1</p><br/> <p> point/s and end your gaming session now?</p></div>
                <div id="okbtn1" align="center" style="margin-top: 38px; margin-left: 80px; float: left;"><img src="images/OK Button.png" alt="" onclick="document.getElementById('popUpDivLPConvert').style.display = 'none';
                document.getElementById('popUpDivConvert').style.display = 'block';" style="cursor:pointer;"/></div><div style="margin-top: 35px;">
                    <img src="images/cancelbutton.png" alt="" onclick="document.getElementById('convertdisabled').style.display = 'none';
                document.getElementById('convert').style.display = 'block';
                document.getElementById('fade').style.display = 'none';
                document.getElementById('popUpDivLPConvert').style.display = 'none';" style="cursor:pointer;"/></div>
            </div>
        </div>  
        <div id="popUpDivConvert" style="display:none;font-family:Helvetica; font-size: 20px;left: 36.5%;top : 40%;">
            <div align="center" style=" border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b><div style="margin-top: 8px; position: absolute; margin-left: 180px;">CONVERT POINTS</div></b></div>
            <div id="popup_container_home">
                <div id="convert2" align="center" style="font-weight:bold;"><br></br>
                    <p>   You have converted  1 point/s for 1 </p> <p style="margin-top: 10px;">number of sweepstakes card/s. Click PROCEED</p> <p style="margin-top: 10px;">to reveal your cards.</p>
                </div><div id="convert_img" align="center" style="visibility:hidden;"><img src="images/load_bal.gif" height="20px" /></div>
                <div id="okbtn" align="center" style="margin-top: 0px;">
                    <img src='images/ProceedButton.png' onclick="document.getElementById('popUpDivConvert').style.display = 'none';
                convertpoints2();" style='cursor: pointer;' /></div>
                <div id="okbtn_img" align="center" style="margin-top: -20px; visibility:hidden;">PROCESSING</div>
            </div>
        </div>

        <div id="mainContainer"> 




            <div id="banner">
                <div id="logo_landing">
                    <img src="images/theSweepsLogo.png" />
                </div>
                <div id="logout">
                    <a href="logout.php">
                        <div class="logout"></div>
                    </a>
                </div>
            </div>
            <div id="contentContainer" style="margin-top: -70px;">

                <div >
                    <div id="launch_Btn" style="margin-left: auto; margin-right: 55%;">
                        <a href="registration.php">
                            <div class="launch_game"></div>
                        </a>
                        <br />
                        <div class="launch_title"> Submit Registration</div>
                    </div>
                    <div id="launch_Btn2" style="margin-top:-350px; margin-right: auto; margin-left: 55%;">
                        <!--<a href="voucher.php"><div class="launch_game"></div></a>-->
                        <div id="convertptsbtn"  class="launch_game" onclick="javascript: return gotovoucherpage();"  style="display:none"></div>
                        <div id="convertptsbtndisabled" class="launch_game_disabled" style="display:none"></div>
                        <br />
                        <div class="launch_title"> Enter <br /> Sweeps Code
                        </div>
                    </div>
                </div>
            </div>
            <div style="width:363px; height:47px; margin-top:-90px; margin-left:auto; margin-right: 42%;">
                <div id="txtBoxContainer_point">
                    <div class="txtBox_left"></div>
                    <div class="txtBox_body"><p align="center">Point Balance: <?php echo $bal; ?> POINTS</p></div>
                    <div class="txtBox_right"></div>
                </div>
            </div>
            <div style="height:33px; width:164px; margin-left:60%; margin-top:-50px; margin-right: auto;">
                <div id="btnContainer">

                    <div id="convert"  class="convertPoints" onclick="javascript: return disableconvertbutton();" style="display:none;"></div>

                    <div id="convertdisabled" class="convertPointsDisabled" style="display:none"></div>
                </div>
            </div>
            <div id="footer">
                <div id="footerBox">
                    <div class="footerBox_left"></div>
                    <div class="footerBox_body">
                        <div class="under18"></div>
                        <div class="rules">
                            <a href="#" onclick="document.getElementById('light').style.display = 'block';
                document.getElementById('fade').style.display = 'block'">Rules &amp; Mechanics</a>
                        </div>
                        <div class="terms"> 
                            <a href="#" onclick="document.getElementById('light2').style.display = 'block';
                document.getElementById('fade').style.display = 'block'">Terms &amp; Conditions</a>
                        </div>
                    </div>
                    <div class="footerBox_right"></div>
                </div>
            </div>
            <div id="light" class="white_content">
                <div style="background-image:  url(images/close_btn.gif); cursor:pointer; width:14px; height:14px; float:right; margin-top:15px; margin-right:15px; display:inline;" 
                     onclick="document.getElementById('light').style.display = 'none';
                document.getElementById('fade').style.display = 'none';"></div>
<?php include('mechanics.php') ?>
            </div>
            <div id="light2" class="white_content">
                <div style="background-image:  url(images/close_btn.gif); _background-image: none; _filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/close_btn.gif',sizingMethod='image'); cursor:pointer; width:14px; height:14px; float:right; margin-top:15px; margin-right:15px; display:inline;" 
                     onclick="document.getElementById('light2').style.display = 'none';
                document.getElementById('fade').style.display = 'none';"></div>
<?php include('terms.php') ?>
            </div>
            <div id="light3" class="white_contentforloading"><div align="center"><br/><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div></div>

            <div id="fade" class="black_overlay"></div>
        </div>
    </body>
</html>
<script type="text/javascript">
            function gotovoucherpage()
            {
                window.location = 'voucher.php';
            }
            function convertpoints2()
            {
                show_loading();
                window.location = 'result.php';
//    setTimeout("gotoresultpage()",2000);
            }
            function gotoresultpage()
            {
                window.location = 'result.php';
            }
            function disableconvertbutton()
            {
                document.getElementById('popUpDivLPConvert').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
                document.getElementById('convertdisabled').style.display = 'block';
                document.getElementById('convert').style.display = 'none';
            }

<?php
if ($bal == '0.10')
{
    ?>
                document.getElementById('convert').style.display = 'block';
                document.getElementById('convertptsbtndisabled').style.display = 'block';
    <?php
}
else
{
    ?>
                document.getElementById('convertdisabled').style.display = 'block';
                document.getElementById('convertptsbtn').style.display = 'block';
    <?php
}
?>
            function show_loading()
            {
                document.getElementById('light3').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
            }

            function hide_loading()
            {
                document.getElementById('light3').style.display = 'none';
                document.getElementById('fade').style.display = 'none';
            }
</script>
