<?php 
/*
 * @author JFJ 05-15-2012
 * Purpose   : view for voucher
 */
include("../controller/voucherprocess.php");
include_once("../controller/managesession.php");
if($_SESSION['login'] == NULL || !isset($_SESSION['login']))
{
     App::Pr("<script> window.location = 'index.php'; </script>");
}
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Guam Sweepstakes Caf&#233; Sweeps Entry Code</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <script type="text/javascript" language="javascript" src="jscript/jquery-1.2.6.min.js"></script>
    <script type="text/javascript" language="javascript" src="jscript/jquery-fieldselection.js"></script>
    <script type="text/javascript" language="javascript" src="jscript/jquery-ui-personalized-1.5.2.min.js"></script>
    <script type="text/javascript" src="jscript/cskeyboard.js"></script>
    
    
    <script type="text/javascript">
        var clickCtr = 0;
        function preventBackandForward()
        {
            window.history.forward(0);
        }
        preventBackandForward();
        window.inhibited_load=preventBackandForward;
        window.onpageshow=function(evt){if(evt.persisted)preventBackandForward();};
        window.inhibited_unload=function(){void(0);};
          function ClearTextboxes()
        {
            document.getElementById('txtVoucher').value = '';
        }

        function keycount()
	 {
	     var txt = document.getElementById('txtVoucher').value;
            
             clickCtr = clickCtr + 1;
            if(clickCtr > 12 && txt.length > 11)
                { 
                    if(txt.length >= 12)
                   {
                   document.getElementById('fade').style.display='block';
                   document.getElementById('light30').style.display='block';
                   document.getElementById('hiddenvoucher').value = txt;
                   return false;
                   }else
                       {
                           return true;
                       }
                }
                else{
                    return true;
                }
        }
       function backvoucher()
        {
            
            var txt1 = document.getElementById('txtVoucher').value;
            var oldtxt = document.getElementById('hiddenvoucher').value;
            if(txt1.length > 12 || txt1.length == 12)
                    {
                       if(document.getElementById('txtVoucher').value.length >= 12)      
                        {    
                            document.getElementById('txtVoucher').value = oldtxt;
                        }
                    }
        
        }  
       
        </script>

  </head>

  <body>
    <form id="frmVoucher" name="frmVoucher" method="post" >
 <!-- POP UP FOR MESSAGES-->   
<div id="light" style="text-align: center;font-size: 16pt;height: 260px" class="white_content2">
   <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px;">
   <b id="a"><div id="title"></div></b></div>
   <p id="b"><br/><div id="msg"></div></p><BR />
   <br/><input type="button" class="inputBoxEffectPopup" onclick ="ClearTextboxes();;document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';document.getElementById('txtVoucher').focus();"/>

</div> <div id="fade" class="black_overlay"></div>
 
<div id="light1" style="text-align: center;font-size: 16pt;height: 260px" class="white_content2">
   <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px;">
   <b id="a"><div id="title1"></div></b></div>
   <p id="b"><br/><div id="msg1"></div></p><BR />
      <br/>  <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';window.location='home.php';"/>
</div> <div id="fade" class="black_overlay"></div>

<div id="light30" style="text-align: center;font-size: 16pt;height: 260px" class="white_content2">
   <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px;">
   <b id="a">Error</b></div>
   <p id="b"><br/>Voucher number must not exceed 12 characters </p><BR />
      <br/>  <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light30').style.display='none';document.getElementById('fade').style.display='none';return backvoucher();"/>
</div> <div id="fade" class="black_overlay"></div>


<!-- POP UP FOR MESSAGES-->
    <div id="mainContainer">
      <div id="banner">
        <div id="logo_landing">
          <img src="images/theSweepsLogo.png" />
        </div>
      </div>
      <div id="contentContainer">
        <div id="codeContainer">
          <div class="codeP"> Please enter your Sweeps Code in the box below.</div>
          <div class="inBox">
            <div class="inputBox_left"></div>
            <div class="inputBox_body">
                <?php  echo $txtVoucher; ?> 
<!--              <input type="text" id="txtVoucher" name="txtVoucher" maxlength="14" class="inputbox_code" />-->
            </div>
            <div class="inputBox_right"></div>
          </div>
        </div>
          <?php echo $hiddenvoucher;?>
        <div id="codeContainer2" style="margin-top: 80px;">
          <div id="keyboard" >
                    <div id="row">
                        <input class="button" name="1" type="button" value="1" size="" onclick="javascript: return keycount();" />
                        <input class="button2" name="2" type="button" value="2" size="" onclick="javascript: return keycount();" />
                        <input class="button2" name="3" type="button" value="3" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="4" type="button" value="4" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="5" type="button" value="5" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="6" type="button" value="6" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="7" type="button" value="7" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="8" type="button" value="8" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="9" type="button" value="9" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="0" type="button" value="0" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="backspace" type="button" value="Backspace" style="width: 100px" />
                    </div>
                    <div id="row1">
                        <input class="button" name="q" type="button" value="q" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="w" type="button" value="w" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="e" type="button" value="e" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="r" type="button" value="r" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="t" type="button" value="t" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="y" type="button" value="y" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="u" type="button" value="u" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="i" type="button" value="i" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="o" type="button" value="o" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="p" type="button" value="p" size="" onclick="javascript: return keycount();"/>
                      <input class="button2" name="shift" type="button" value="Shift" id="shift2" style="width: 100px" />
                    </div>
                    <div id="row1_shift">
                        <input class="button" name="Q" type="button" value="Q" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="W" type="button" value="W" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="E" type="button" value="E" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="R" type="button" value="R" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="T" type="button" value="T" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="Y" type="button" value="Y" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="U" type="button" value="U" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="I" type="button" value="I" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="O" type="button" value="O" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="P" type="button" value="P" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="shift" type="button" value="Shift" id="shift2" style="width: 100px" />

                    </div>
                    <div id="row2">
                        <input class="button" name="a" type="button" value="a" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="s" type="button" value="s" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="d" type="button" value="d" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="f" type="button" value="f" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="g" type="button" value="g" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="h" type="button" value="h" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="j" type="button" value="j" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="k" type="button" value="k" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="l" type="button" value="l" size="" onclick="javascript: return keycount();"/>
                        <input class="button2"  type="button" style="visibility: hidden" />
                        <input id="clearall" class="button2" name="clear" style="width: 100px;" type="button" value="Clear All" />
                    </div>
                   <div id="row2_shift">
                        <input class="button" name="a" type="button" value="A" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="s" type="button" value="S" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="d" type="button" value="D" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="f" type="button" value="F" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="g" type="button" value="G" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="h" type="button" value="H" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="j" type="button" value="J" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="k" type="button" value="K" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="l" type="button" value="L" size="" onclick="javascript: return keycount();"/>
                        <input class="button2"  type="button" style="visibility: hidden" />
                        <input id="clearall" class="button2" name="clear" style="width: 100px;" type="button" value="Clear All" />
                    </div>
                    <div id="row3">
                        <input class="button" name="z" type="button" value="z" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="x" type="button" value="x" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="c" type="button" value="c" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="v" type="button" value="v" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="b" type="button" value="b" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="n" type="button" value="n" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="m" type="button" value="m" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" id="leftarrow" name="leftarrow" type="button" value="<-" />
                        <input class="button2" id="rightarrow" name="rightarrow" type="button" value="->" />
                    </div>
                   <div id="row3_shift">
                        <input class="button" name="Z" type="button" value="Z" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="X" type="button" value="X" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="C" type="button" value="C" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="V" type="button" value="V" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="B" type="button" value="B" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="N" type="button" value="N" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" name="M" type="button" value="M" size="" onclick="javascript: return keycount();"/>
                        <input class="button2" id="leftarrow" name="leftarrow" type="button" value="<-" />
                        <input class="button2" id="rightarrow" name="rightarrow" type="button" value="->" />
                    </div>
          </div>
          <div  class="code_btn"><?php echo $btnSubmit;?>
            <a href="home.php">
              <div class="code_back"></div>
            </a>
          </div>
        </div>
      </div>
      <div id="footer">
        <div id="footerBox">
          <div class="footerBox_left"></div>
          <div class="footerBox_body">
            <div class="under18"></div>
             <div class="rules">
               <a href="#" onclick="document.getElementById('lightMechanics').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</a>
             </div>
             <div class="terms"> 
                 <a href="#" onclick="document.getElementById('lightTerms').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</a>
             </div>
          </div>
          <div class="footerBox_right"></div>
        </div>
      </div>
        <div id="lightMechanics" class="white_content">
         <div style="background-image:  url(images/close_btn.gif); cursor:pointer; width:14px; height:14px; float:right; margin-top:15px; margin-right:15px; display:inline;" 
           onclick="document.getElementById('lightMechanics').style.display='none';document.getElementById('fade').style.display='none';"></div>
           <?php include('mechanics.php') ?>
        </div>
        <div id="lightTerms" class="white_content">
          <div style="background-image:  url(images/close_btn.gif); _background-image: none; _filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/close_btn.gif',sizingMethod='image'); cursor:pointer; width:14px; height:14px; float:right; margin-top:15px; margin-right:15px; display:inline;" 
            onclick="document.getElementById('lightTerms').style.display='none';document.getElementById('fade').style.display='none';"></div>
          <?php include('terms.php') ?>
        </div>
        <div id="fade" class="black_overlay"></div>
      </div>
         <?php if(isset($error_msg)):?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $error_msgtitle;?>";
    document.getElementById('msg').innerHTML = "<?php echo $error_msg;?>";
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif; ?>
 <?php if(isset($msg)):?>
<script>
    document.getElementById('title1').innerHTML = "<?php echo $msg_title;?>";
    document.getElementById('msg1').innerHTML = "<?php echo $msg;?>";
    document.getElementById('light1').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif; ?>
  </form>
  </body>
  <script type="text/javascript">
  <!--
  <?php
	$stat = (isset($stat)) ? $stat : 1;
    if ($stat <> 1 && $post > 0)
    {
  ?>
    function error()
    {
      <?php
        if ($stat == 2)
        {
        ?>
          document.getElementById('light3').style.display='block';
          document.getElementById('fade').style.display='block';
        <?php
        }
        else if ($stat == 3)
        {
        ?>
          document.getElementById('light2').style.display='block';
          document.getElementById('fade').style.display='block';
        <?php
        }
        else if ($stat == 4)
        {
        ?>
          document.getElementById('light4').style.display='block';
          document.getElementById('fade').style.display='block';        
        <?php
        }
        else
        {
        ?>
          document.getElementById('light1').style.display='block';
          document.getElementById('fade').style.display='block'; 
        <?php
        }
        
      ?>
    }
    error();
  <?php
    }
  ?>
        function chkChar(evt)
  {
       var charCode = (evt.which) ? evt.which : event.keyCode;
    if((charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;

    return true;
  }
  </script>
</html>
