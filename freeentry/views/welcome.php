<?php
/*
 * @author JFJ 05-16-2012
 * Purpose   : view for welcome
 */
//require_once("../init.inc.php");
include("../controller/homeprocess.php");
//include_once("../controller/managesession.php");
?> 
<?php error_reporting(E_ALL); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="refresh" content="600" />
        <title>Guam Sweepstakes Caf&#233; Free Entry</title>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <script src="jscript/jquery-1.4.min.js" type="text/javascript"></script>
        <!-- <script src="jscript/jquery-1.4.1.js" type="text/javascript"></script> 
        <script src="jscript/jquery.background.image.scale-0.1.js" type="text/javascript"></script> -->
        <script src="jscript/ypSimpleScrollC.js" type="text/javascript"></script>
        <script type="text/javascript">
            var test = new ypSimpleScroll("myScroll", 20, 0, 750, 330, 40, 700)
        </script>
        <script type="text/javascript">
            //Using document.ready causes issues with Safari when the page loads
            jQuery(window).load(function() {
                $("#contentContainer").backgroundScale({
                    imageSelector: "#gaBG",
                    centerAlign: true,
                    containerPadding: 0
                });
            });
        </script>
        <script type="text/javascript">
            function checkform() {
                if (document.getElementById("terms").checked)
                {
                    document.getElementById('light3').style.display = 'none';
                    document.getElementById('fade').style.display = 'none';
                }
                else {
                    document.getElementById('lightterms').style.display = 'block';
                    document.getElementById('fade').style.display = 'block';
                    //return false;
                }

            }
        </script>
        <style>
            body{ overflow: hidden;}        
        </style>
    </head>

    <body onload="test.load();
                document.getElementById('light3').style.display = 'block';
                document.getElementById('fade').style.display = 'block';">
        <div id="mainContainer">
            <div id="banner">
                <div id="logo_landing"> <img src="images/theSweepsLogo.png" /></div>
                <div id="logout"><a href="logout.php"><div class="logout"></div></a></div>
            </div>
            <div id="contentContainer3" style="margin-top: -50px;">
                <div id="about_container">
                    <div class="aboutTitle" style="">WELCOME TO THE SWEEPS CENTER!</div>
                    <div id="myScrollContainer" style="height: 320px;">
                        <div  id="myScrollContent">
                            <br />
                            The Sweeps Center is a one-stop managed sweepstakes shop that sells a wide variety of products and offers internet browsing services.  For every purchase you make, you get a chance to win up to US$ 10,000 cash instantly!<br/><br/>
                            We also offer a Free Entry service where you can avail an e – Sweeps Card for free.  All you need to do is visit any of our Sweeps Center branch.  Approach our Gaming Assistants so they can direct you to our Free Entry terminal and register your information. After completing the registration form and successfully submitting it, an auto-generated code will be sent to your registered email.  Print your code and go to any of the Sweeps Center branches near you to redeem your free e – Sweeps Card.  You can either choose your own card or let the computer choose it for you.<br/><br/>
                            If you pick a winning card, take note of your Transaction Reference ID.  Most prizes won can be instantly claimed at the Sweeps Center branch.<br/><br/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/FreeEntry.png" alt=""/>
                            <br/><br/>
                            Visit the Sweeps Center at (location address) for a higher quality of Sweepstakes service that is more fun and rewarding!<br/><br />
                        </div>
                    </div>
                </div>
                <div class="goToLaunchPad_quickContainer">
                    <form action="home.php" method="post" onsubmit="">
                        <div style="width:100%; text-align:center; margin-top: 50px;">
                            <br/>
                            <input type="image" name="btnLogin" id="btnLogin" accesskey="L" tabindex="3" title="Proceed" src="images/ProceedButton.png" alt="Login" style="width:150px; height:50px;" align="center" />
                        </div>
                    </form>
                </div>
                <div id="msg" style="float:right;margin-right:70px;margin-top: -400px;">
                    <a href="#" onmouseover="test.scrollNorth()" onmouseout="test.endScroll()">
                        <div style="color:#BDB76B;">
                            <img src="images/scroll_up.png" alt="^"border="2" style="border-color:#BDB76B;" />
                        </div>
                    </a>
                    <br/>

                    <a href="#" onmouseover="test.scrollSouth()" onmouseout="test.endScroll()">
                        <div style="color:#BDB76B; margin-top:5px">
                            <img src="images/scroll_down.png" alt="v"/>
                        </div>
                    </a>
                </div>

            </div>
            <div id="footer">
                <div id="footerBox">
                    <div class="footerBox_left"></div>
                    <div class="footerBox_body">
                        <div class="under18"></div>
                        <div class="rules">
                            <a href="#" onclick="document.getElementById('light').style.display = 'block';
                document.getElementById('fade').style.display = 'block'">Rules &amp; Mechanics</a>
                        </div>
                        <div class="terms">
                            <a href="#" onclick="document.getElementById('light2').style.display = 'block';
                document.getElementById('fade').style.display = 'block'">Terms &amp; Conditions</a>
                        </div>
                    </div>
                    <div class="footerBox_right"></div>
                </div>
            </div>

            <div id="light" class="white_content2">
                <div style="background-image:  url(images/close_btn.gif); cursor:pointer; width:14px; height:14px; float:right; margin-top:15px; margin-right:15px; display:inline;"
                     onclick="document.getElementById('light').style.display = 'none';
                document.getElementById('fade').style.display = 'none';"></div>
                     <?php include('mechanics.php') ?>
            </div>
            <div id="light2" class="white_content2">
                <div style="background-image:  url(images/close_btn.gif); _background-image: none; _filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/close_btn.gif',sizingMethod='image'); cursor:pointer; width:14px; height:14px; float:right; margin-top:15px; margin-right:15px; display:inline;"
                     onclick="document.getElementById('light2').style.display = 'none';
                document.getElementById('fade').style.display = 'none';"></div>
                     <?php include('terms.php') ?>
            </div>
            <div id="light3" class="white_content2">
                <?php include('terms2.php') ?>
            </div>
            <div id="fade" class="black_overlay"></div>
        </div>
        <div id="lightterms" style="text-align: center;font-size: 16pt; height: 25%;" class="white_content">
            <div style="width: 100%;height: 45px;background-color:#006E6E;top: 0px;color: white;padding-top: 5px"><b>TERMS AND CONDITION CONFIRMATION</b></div>
            <br/>
            Please confirm that you have read<br/><br/>the Terms &#38; Conditions.
            <br/><br/>
            <img src="images/okbutton.png" onclick="document.getElementById('lightterms').style.display = 'none';"/>
        </div>

        <div id="light_msg" style="text-align: center;font-size: 14pt;height: auto;" class="white_content">
            <div style="width: 100%; height: 27px; background-color: #006E6E; color: white; padding-top: 15px"><b id="a"></b></div>
            <br/><br/>
            <div id="b" style=" text-align: center; padding: 5px;"><br/></div>
            <br/><br/>
            <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light_msg').style.display = 'none';
                document.getElementById('fade').style.display = 'none';"/>
        </div>

        <div id="fade" class="black_overlay"></div>
    </body>
</html>








