<?php
/*
 * @author JFJ 05-16-2012
 * Purpose   : view for login
 */
include("../controller/indexprocess.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="css/login.css" />
    <!--<link rel="stylesheet" type="text/css" href="css/main.css" />-->
  <title>Guam Sweepstakes Caf&#233; Free Entry</title>
</head>
<body>
  <form id="frmLogin" name="frmLogin" method="post" >
      <!-- POP UP FOR MESSAGES-->
   <div id="light" style="text-align: center;font-size: 16pt;height: 260px" class="white_content">
   <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px;">
   <b id="a"><div id="title"></div></b></div><br/><br/>
   <p id="b"><br/><div id="msg"></div></p>
  <br/><br/>
    <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';"/>
</div> <div id="fade" class="black_overlay"></div>
<div id="light1" style="text-align: center;font-size: 16pt;height: auto;margin-left: auto;" class="white_content">
   <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px">
   <b id="a"><div id="title1"></div></b></div>
   <p id="b"><br/><div id="msg1"></div></p>
   <br/> <?php echo $btnwelcome;?>
<!--             <input type="button" class="inputBoxEffectPopup" onclick ="URL::Redirect('welcome.php')"/>-->
</div> <div id="fade" class="black_overlay"></div>
<!-- POP UP FOR MESSAGES-->
	<div id="mainContainer">
    	<div id="banner"></div>
        <div id="contentContainer" style="margin-top: -60px;">
        	<div id="Login_Cont">
            	<div class="login_logo"></div>
                <div class="login_body">
                	<div class="loginCont_body">
                        <div class="txtName">USERNAME</div>
                        <div class="inputbox">
                            <?php echo $txtUsername;?>
                        </div>
                         <div class="txtName">PASSWORD</div>
                        <div class="inputbox">
                            <?php echo $txtPassword;?>
                         </div>
                         <div class="loginCont_btn">
                             <?php echo $btnSubmit;?>
<!--                           <input type="submit" class="login" value=""  onclick="javascript: return checklogin();"></input>-->
                        </div>
                         </div>
                <div class="login_bottom">&nbsp;</div>
                
                </div>
            </div>
        </div>
        <div id="footer"></div>
    <?php if(isset($error_msg)):?>
<script>
    document.getElementById('title').innerHTML = "<?php echo $error_msgtitle;?>";
    document.getElementById('msg').innerHTML = "<?php echo $error_msg;?>";
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif; ?>     
      
        <?php if(isset($msg)):?>
<script>
    document.getElementById('title1').innerHTML = "Error";
    document.getElementById('msg1').innerHTML = "<?php echo $msg;?>";
    document.getElementById('light1').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
</script>
<?php endif; ?>   
        
 </div>
</form>
</body>                
          <script type="text/javascript">
   
    function checklogin()
    {
    var oUsername = document.getElementById("txtUsername");
    var oPassword = document.getElementById("txtPassword");

    if (oUsername.value.length == 0)
    {
        document.getElementById('title').innerHTML = "INVALID LOG IN";
    document.getElementById('msg').innerHTML = "Please enter your username.";
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
      return false;
    }
    
    else if (oPassword.value.length == 0)
    {
        document.getElementById('title').innerHTML = "INVALID LOG IN";
    document.getElementById('msg').innerHTML = "Please enter your password.";
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
      return false;
    }
    
    return true;
    }
    
    function chkChar(evt)
    {
         var charCode = (evt.which) ? evt.which : event.keyCode;
       if((charCode == 32 || charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;

        return true;
    }
    
    <?php
	$id = (isset($id)) ? $id : '';
    if ($id > 0)
    {
    ?>
    function error()
    {
      document.getElementById('light3').style.display='block';
      document.getElementById('fade').style.display='block';
      return false;
    }
    error();
    <?php
    }
    ?>
  </script>
</html>                  
                        
                        
                        
                        
                        
                        
                        