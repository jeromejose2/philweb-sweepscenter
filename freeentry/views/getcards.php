<?php

/*
 * @author Jerome Jose March 13-2013
 * 
 */
include("../controller/getcardsprocess.php");
include_once("../controller/managesession.php");
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Guam Sweepstakes Caf&#233;</title>
    <link rel="stylesheet" type="text/css" href="css/style_1.css" />
      <script type="text/javascript" src="jscript/jquery-1.4.min.js" ></script>

      <script type="text/javascript" language="javascript">
  $(document).ready(function(){
                    
     function browserTester(browserString)
    {
        return navigator.userAgent.toLowerCase().indexOf(browserString) > -1;
    }

    if(browserTester('chrome'))       {   cssfunctionChrome();     } 
    else if(browserTester('safari'))  {   cssfunctionSafari();     }
    else if(browserTester('firefox')) {   cssfunctionFirefox();    }
    
  });
  
  function cssfunctionFirefox()
  {
//       alert('cssfunctionFirefox');
        $("#divmsg").css("top","50%"); 
        $("#divmsg").css("left","50%"); 
        $("#divmsg").css("margin-left","-500px"); 
        $("#divmsg").css("margin-top","200px"); 
        $("#divmsg").css("width","985px"); 
        $("#divmsg").css("height","auto"); 
        $("#divmsg").css("text-align","center"); 
        $("#divmsg").css("display","none"); 
        $("#divmsg").css("position","absolute");
        $("#resmsg").css("margin-top","0px");
        $("#btnOkay").css("margin-top","10px");
  }
   
  function cssfunctionSafari()
  {
//      alert('safari');
      $("#divmsg").css("top","50%"); 
        $("#divmsg").css("left","50%"); 
        $("#divmsg").css("margin-left","-500px"); 
        $("#divmsg").css("margin-top","100px"); 
        $("#divmsg").css("width","985px"); 
        $("#divmsg").css("height","auto"); 
        $("#divmsg").css("text-align","center"); 
        $("#divmsg").css("display","none"); 
        $("#divmsg").css("position","absolute"); 
        $("#resmsg").css("margin-top","80px");
        $("#btnOkay").css("margin-top","50px");
  }
 function preventBackandForward()
        {
            window.history.forward(0);
        }
        preventBackandForward();
        window.inhibited_load=preventBackandForward;
        window.onpageshow=function(evt){if(evt.persisted)preventBackandForward();};
        window.inhibited_unload=function(){void(0);};

    var ctr = 1;
   
    function boxclicked(id)
    {
      if (ctr == 1)
      {
        document.getElementById("divmsg").style.display = "block";
        document.getElementById("winnings"+id).style.display = "block";
        document.getElementById("divecn"+id).style.display = "block";
        document.getElementById("box"+id).style.display = "none";
        /*document.getElementById("btnQuickPick").style.display = "none";*/
        document.getElementById('quickpickdisabled').style.display = 'block';
        document.getElementById('quickpick').style.display = 'none';
        document.getElementById("lblCtr").value = 0;
        ctr = ctr + 1;
                 /*change status Created by JFJ 09-28-2012*/
                $.ajax({
                url     : "../controller/statuschangeproccess.php",
                type    : "POST",
                success : function(data){
//                    alert(data);
                },
                error   : function(){
                    alert('error');
                } 
                });
                 /*change status Created by JFJ 09-28-2012*/
      }
      
      for (var i = 1; i < 11; i++)
      {
        if (i != id)
        {
          document.getElementById("boxdisabled"+i).style.display = "block";
          document.getElementById("box"+i).style.display = "none";
        }
      }
      
    }
    
    function quickpick()
    {
      var qp = Math.random();
      qp = qp * 10;
      qp = Math.ceil(qp);
      
      if (ctr == 1)
      {
         
        document.getElementById("box" + qp).style.display = "none";
        document.getElementById("divmsg").style.display = "block";
        document.getElementById("winnings"+qp).style.display = "block";
        document.getElementById("divecn"+qp).style.display = "block";
        /*document.getElementById("btnQuickPick").style.display = "none";*/
        document.getElementById('quickpickdisabled').style.display = 'block';
        document.getElementById('quickpick').style.display = 'none';
        document.getElementById("lblCtr").value = 0;
        ctr = ctr + 1;
                         /*change status Created by JFJ 09-28-2012*/
                $.ajax({
                url     : "../controller/statuschangeproccess.php",
                type    : "POST",
                success : function(data){
//                    alert(data);
                   
                },
                error   : function(){
                    alert('error');
                } 
                });
                 /*change status Created by JFJ 09-28-2012*/
       
        
      }

      for (var i = 1; i < 11; i++)
      {
        if (i != qp)
        {
          document.getElementById("boxdisabled"+i).style.display = "block";
        }
      }
      
    }
  </script>
<style>
body{
	/*background: url(../images/launchpad_bg2.png) #FEF6EB repeat-x;*/
	font:normal 12px/18px  "Lucida Sans Unicode", "Lucida Grande", sans-serif;
	margin:0;
	padding:0;
	background-color: #FEF6EB;
    }
</style>
      </head>
<body>
 <form id="frmResult" name="frmResult" action="result.php"  method="post" onsubmit="">
    <div id="mainContainer">
      <div id="banner2">
        <div id="logo_landing2">
          <img src="images/theSweepsLogo.png" alt="" height="130" width="230"/>
        </div>
      </div>
      <div id="contentContainer" style="height: 75%; top: 500px;">
        <div id="sweepsMainContainer">
          <div class="sweeps_quickContainer">
            <div class="cardCount">CARD COUNTER:</div>
            <div class="countBox_left"></div>
            <div class="countBox_body"><p>
              <input id="lblCtr" name="lblCtr" value="1" class="cardCount" style="width:20px; height:30px; margin-left:15px;margin-top:5px;"></input>
            </p></div>
            <div class="countBox_right"></div>
            <div class="quick" >
              <div id="quickpick"  class="sweeps_quick" onclick="javascript: return quickpick();" style="display:block"></div>
              <div id="quickpickdisabled" class="sweeps_quick_disabled" style="display:none"></div>
            </div>
          </div>
          <div class="sweepsContainer">
            <div class="win_container">
              <a href="#" onclick="javascript: return boxclicked('1');">
                <div id="box1" class="flipCard"></div>
              </a>
              <p id="winnings1" class="winnings">
                <?php echo $wintype; ?>
              </p>      
              <span id="divecn1">
                <?php echo $ecn; ?>
              </span> 
            </div>
            <div id="boxdisabled1" class="win_container_disabled"></div>
          </div>
          <div class="sweepsContainer">
            <div class="win_container">
              <a href="#" onclick="javascript: return boxclicked('2');">
                <div id="box2" class="flipCard"></div>
              </a>
              <p id="winnings2" class="winnings">
                <?php echo $wintype; ?>
              </p> 
              <span id="divecn2">
                <?php echo $ecn; ?>
              </span>
            </div>
            <div id="boxdisabled2" class="win_container_disabled"></div>
          </div>
          <div class="sweepsContainer">
            <div class="win_container">
              <a href="#" onclick="javascript: return boxclicked('3');">
                <div id="box3"  class="flipCard"></div>
              </a>
              <p id="winnings3" class="winnings">
                <?php echo $wintype; ?>
              </p> 
              <span id="divecn3">
                <?php echo $ecn; ?>
              </span> 
            </div>
            <div id="boxdisabled3" class="win_container_disabled"></div>
          </div>
          <div class="sweepsContainer">
            <div class="win_container">
              <a href="#" onclick="javascript: return boxclicked('4');">
                <div id="box4"  class="flipCard"></div>
              </a>
              <p id="winnings4" class="winnings">
                <?php echo $wintype; ?>
              </p> 
              <span id="divecn4">
                <?php echo $ecn; ?>
              </span> 
            </div>
            <div id="boxdisabled4" class="win_container_disabled"></div>
          </div>
          <div class="sweepsContainer2">
            <div class="win_container">
              <a href="#" onclick="javascript: return boxclicked('5');">
                <div id="box5"  class="flipCard"></div>
              </a>
              <p id="winnings5" class="winnings">
                <?php echo $wintype; ?>
              </p> 
              <span id="divecn5">
                <?php echo $ecn; ?>
              </span> 
            </div>
            <div id="boxdisabled5" class="win_container_disabled"></div>
          </div>
          <div class="sweepsContainer">
            <div class="win_container">
              <a href="#" onclick="javscript: return boxclicked('6');">
                <div id="box6"  class="flipCard"></div>
              </a>
              <p id="winnings6" class="winnings">
                <?php echo $wintype; ?>
              </p> 
              <span id="divecn6">
                <?php echo $ecn; ?>
              </span> 
            </div>
            <div id="boxdisabled6" class="win_container_disabled"></div>
          </div>
          <div class="sweepsContainer">
            <div class="win_container">
              <a href="#" onclick="javscript: return boxclicked('7');">
                <div id="box7"  class="flipCard"></div>
              </a>
              <p id="winnings7" class="winnings">
                <?php echo $wintype; ?>
              </p>                 
              <span id="divecn7">
                <?php echo $ecn; ?>
              </span> 
            </div>
            <div id="boxdisabled7" class="win_container_disabled"></div>
          </div>
          <div class="sweepsContainer">
            <div class="win_container">
              <a href="#" onclick="javascript: return boxclicked('8');" >
                <div id="box8"  class="flipCard"></div>
              </a>
              <p id="winnings8" class="winnings">
                <?php echo $wintype; ?>
              </p>           
              <span id="divecn8">
                <?php echo $ecn; ?>
              </span> 
            </div>
            <div id="boxdisabled8" class="win_container_disabled"></div>
          </div>
          <div class="sweepsContainer">
            <div class="win_container">
              <a href="#" onclick="javascript: return boxclicked('9');">
                <div id="box9"  class="flipCard"></div>
              </a>
              <p id="winnings9" class="winnings">
                <?php echo $wintype; ?>
              </p>             
              <span id="divecn9">
                <?php echo $ecn; ?>
              </span> 
            </div>
            <div id="boxdisabled9" class="win_container_disabled"></div>
          </div>
          <div class="sweepsContainer2">
            <div class="win_container">
              <a href="#" onclick="javascript: return boxclicked('10');">
                <div id="box10"  class="flipCard"></div>
              </a>
              <p id="winnings10" class="winnings">
                <?php echo $wintype; ?>
              </p>         
              <span id="divecn10">
                <?php echo $ecn; ?>
              </span> 
            </div>
            <div id="boxdisabled10" class="win_container_disabled"></div>
          </div>
        </div>
          <?php if(isset($msg))
          {?>
        <div id="divmsg" style="">
            <div id="resmsg" style="color:#FF0000;font-size:1.2em;font-weight:bold;left: 50%;top: 50%;height: auto;margin-left: 0px;"><?php echo $msg; ?></div>
            <br />
        <input id="btnOkay"  class="proceedbutton" type="button" value="" onclick="javascript: return window.location='result2.php';"
               style='top: 50%;left: 50%;position: absolute;margin-left: -70px;'></input>
        </div>
           <?php  }?>
        <div id="fade" class="black_overlay"></div>
        
      
      <div id="footer">
        <div id="footerBox">
          <div class="footerBox_left"></div>
          <div class="footerBox_body">
            <div class="under18"></div>
             <div class="rules">
               <a href="#" onclick="document.getElementById('lightMechanics').style.display='block';document.getElementById('fade').style.display='block'">Rules &amp; Mechanics</a>
             </div>
             <div class="terms"> 
                 <a href="#" onclick="document.getElementById('lightTerms').style.display='block';document.getElementById('fade').style.display='block'">Terms &amp; Conditions</a>
             </div>
          </div>
          <div class="footerBox_right"></div>
        </div>
      </div>
</div> 
        <!--mainfooter-->
  </div> 
        <div id="lightMechanics" class="white_content2">
         <div style="background-image:  url(images/close_btn.gif); cursor:pointer; width:14px; height:14px; float:right; margin-top:15px; margin-right:15px; display:inline;" 
           onclick="document.getElementById('lightMechanics').style.display='none';document.getElementById('fade').style.display='none';"></div>
           <?php include('mechanics.php') ?>
        </div>
        <div id="lightTerms" class="white_content2">
          <div style="background-image:  url(images/close_btn.gif); _background-image: none; _filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/close_btn.gif',sizingMethod='image'); cursor:pointer; width:14px; height:14px; float:right; margin-top:15px; margin-right:15px; display:inline;" 
            onclick="document.getElementById('lightTerms').style.display='none';document.getElementById('fade').style.display='none';"></div>
          <?php include('terms.php') ?>
        </div>

    </form>
        <div id="light" style="text-align: center;font-size: 16pt;height: 260px" class="white_content">
          <div style="width: 100%;height: 27px;background-color: #006E6E;top: 0px;color: white;padding-top: 5px;">
            <b id="a">CONFIRMATION</b>
          </div>
          <br/>
          <p id="b">
            <br/>
            Kindly select one card from the display to avail your free entry for the day.
          </p>
          <br/>
          <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';"/>
        </div>
        <?php
        if ($_SESSION['vcode'] <> '')
        {
              echo "<script type=\"text/javascript\">";
              echo "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block';";
              echo "</script>";
        }
        ?>
        <div id="fade" class="black_overlay"></div>

    </div>
  </body>
</html>


