<?php

/*
 * @author Jerome Jose March 13-2013
 */

require_once("../init.inc.php");
App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");
App::LoadModuleClass("SweepsCenter", "SCCC_TransactionSummary");
$cwinningsdump = new SCC_WinningsDump();
$transactionsummary = new SCCC_TransactionSummary();

$terminalid = $_SESSION["terminalid"];


$getid = $transactionsummary->SelectTransactionSummaryPerTerminal($terminalid);
$transid = str_pad($getid[0][0], 14, "0", STR_PAD_LEFT);


     $isFinalized = $cwinningsdump->SelectAllWinnings($terminalid);
    if(count($isFinalized) == 1)
    {
        $wintype = $isFinalized[0]["WinType"];
        if ($wintype == "$0 Cash")
        {
            $wintype = "Try Again";
        }
       $ecn =  $isFinalized[0]["ECN"];
       
       if($wintype != "Try Again")
       {
            $Wintypecash =  strpos($wintype, "Cash");
                if($Wintypecash)
                {
                     $allprizes = "(1)" . $wintype . "";
                }  
                else 
                {
                    $allprizes = "" . $wintype . "";
                }
            $msg = "You've won " . $allprizes . ". Your Transaction Reference ID is " . $transid . ".<br/>Please proceed to the cashier to claim your winning. ";
                                          
           
       }
       else 
       {
           $msg = "This is not a winning card. Your Transaction Reference ID is " . $transid . ". <br /> Try our sweeptakes games and increase your chance of winning more and big prizes!<br/> Thank you for playing.";
                    
            }
    }
    else
    {
              $msg = "Error";
    }
   
?>
