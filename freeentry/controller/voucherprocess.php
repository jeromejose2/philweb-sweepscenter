<?php
/*
 * @author JFJ 05-29-2012
 * Purpose   : controller for voucher
 */
require_once("../init.inc.php");
$modulename = "SweepsCenter";
APP::LoadModuleClass($modulename, "SCC_FreeEntry");
APP::LoadModuleClass($modulename, "SCCV_VoucherInfo");
App::LoadModuleClass($modulename, "SCC_AuditTrail");
app::LoadModuleClass($modulename, "SCC_Terminals");
App::LoadModuleClass($modulename, "SCC_TerminalSessions");

App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("Hidden");

$freeentry = new SCC_FreeEntry();
$cafinovoucher = new SCCV_VoucherInfo();
$audittrail = new SCC_AuditTrail();
$terminal = new SCC_Terminals();
$terminalSessions = new SCC_TerminalSessions();
$voucherform = new FormsProcessor();

$btnSubmit = new Button("btnSubmit", "btnSubmit");
$btnSubmit->CssClass = "code_submit";
$btnSubmit->Text = " ";

//$btnSubmit->Args = "onclick='javascript: return checkinput();'";
$btnSubmit->Style = "width: 100px; height: 40px; font-weight: bold;";
$btnSubmit->IsSubmit = true;

$txtVoucher = new TextBox("txtVoucher", "txtVoucher", "Voucher");
$txtVoucher->Length = 12;
$txtVoucher->CssClass = "inputbox_code";
$txtVoucher->Args = "onkeypress ='javascript: return chkChar(event)'; ";

$hiddenvoucher = new Hidden('hiddenvoucher', 'hiddenvoucher');

$voucherform->AddControl($btnSubmit);
$voucherform->AddControl($txtVoucher);
$voucherform->AddControl($hiddenvoucher);

$voucherform->ProcessForms();



if ($voucherform->IsPostBack)
{
    if (isset($_POST["btnSubmit"]))
    {
        $aid = $_SESSION["terminalid"];
        $siteID = $_SESSION["siteid2"];
        $username = $_SESSION['username'];

        $voucherNo = $_POST["txtVoucher"];
        $getV = $cafinovoucher->getOnlyVoucher($voucherNo);
        if (count($cafinovoucher) == 0 || $voucherNo == "")
        {
            $error_msgtitle = "INVALID CODE";
            $error_msg = "You have entered an invalid free entry code. Please check if you have entered the correct code.";
        }
        else
        {
            $arVoucher = $getV[0];
            $status = $arVoucher["Status"];
            $entryType = $arVoucher["EntryType"];
            if ($entryType != 1 && $entryType != 2 && $entryType != 4)
            {
                $error_msgtitle = "INVALID CODE";
                $error_msg = "You have entered an invalid free entry code. Please check if you have entered the correct code.";
            }
            else
            {

                if ($status == 2)
                {
                    $error_msgtitle = "USED CODE";
                    $error_msg = "The Free Entry Code you have entered has already been used.";
                }
                else if ($status == 3)
                {
                    $error_msgtitle = "EXPIRED CODE";
                    $error_msg = "The code you have entered has already expired.";
                }
                else if ($status == 4)
                {
                    //start ther terminal's session
                    //end add mtcc on 01/17/2013
                    $terminalSessions->StartTransaction();
                    //check if terminal still has an existing session
                    $arrexisting = $terminalSessions->SelectTerminalSessionDetails();
                    if (count($arrexisting) > 0)
                    {
                        //end existing session              
                        $arrendsession = $terminalSessions->UpdateUnendedFreeEntrySession();
                        if ($terminalSessions->HasError)
                        {
                            $error_msgtitle = " Error";
                            $error_msg = "Error in Terminal Sessions" . $terminalSessions->getError();
                            $terminalSessions->RollBackTransaction();
                        }
                    }

                    //insert into terminal Session
                    $arrTerminalsess["TerminalID"] = $aid;
                    $arrTerminalsess["SiteID"] = $siteID;
                    $arrTerminalsess["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                    $arrTerminalsess["DateStart"] = "now_usec()";
                    $arrTerminalsess["DateCreated"] = "now_usec()";
                    $terminalSessions->Insert($arrTerminalsess);
                    $terminalSessions->CommitTransaction();

                    if ($terminalSessions->HasError)
                    {
                        $error_msgtitle = " Error";
                        $error_msg = "Error in Terminal Sessions" . $terminalSessions->getError();
                        $terminalSessions->RollBackTransaction();
                    }
                    else
                    {

                        $terminalsessionid = $terminalSessions->LastInsertID;
                        $_SESSION['terminalsid'] = $terminalsessionid;

                        //insert in audittrail
                        $audittrail->StartTransaction();
                        $audittrailparam["SessionID"] = $_SESSION['sid'];
                        $audittrailparam["AccountID"] = "0";
                        $audittrailparam["TransDetails"] = "Convert Voucher: " . $voucherNo . " of " . $username;
                        $audittrailparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                        $audittrailparam["TransDateTime"] = "now_usec()";
                        $audittrail->Insert($audittrailparam);
                        $audittrail->CommitTransaction();

                        if ($audittrail->HasError)
                        {
                            $error_msgtitle = " Error";
                            $error_msg = "Error in Audittrail" . $audittrail->getError();
                            $audittrail->RollBackTransaction();
                        }
                        else
                        {
                            $_SESSION["vcode"] = $_POST['txtVoucher'];
                            $_SESSION["bal"] = '0.10';
                            $msg_title = "SUCCESSFUL!";
                            $msg = "You have " . $_SESSION['bal'] . " points. Please convert this point to avail your free entry card for the day.";
                            $error_msg = NULL;
                            $error_msgtitle = NULL;
                        }
                    }
                    //end add mtcc on 01/17/2013
                }
                else
                {
                    $error_msgtitle = "INVALID CODE";
                    $error_msg = "You have entered an invalid free entry code. Please check if you have entered the correct code.";
                }
            }
        }
    }
}