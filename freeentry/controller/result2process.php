<?php
/* * Date Created 05-21-2012
 * JFJ */
require_once("../init.inc.php");
App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");
$cwinningsdump = new SCC_WinningsDump();

$terminalid = $_SESSION["terminalid"];

$cwinningsdump->StartTransaction();

$cwinningsdump->UpdateIsFinalizedStatus($terminalid);
if($cwinningsdump->HasError)
{
    $cwinningsdump->RollBackTransaction();
    echo "Error";
}
else 
{
    $cwinningsdump->CommitTransaction();
    App::Pr("<script> window.location = 'welcome.php'; </script>");

}
?>
