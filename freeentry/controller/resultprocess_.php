<?php

/* * Date Created 05-21-2012
 * JFJ */
require_once("include/core/init.inc.php");
$modulename = "SweepsCenter";
APP::LoadModuleClass($modulename, "SCC_FreeEntry");
APP::LoadModuleClass($modulename, "SCCV_VoucherInfo");
APP::LoadModuleClass($modulename, "SCCC_TransactionSummary");
APP::LoadModuleClass($modulename, "SCCC_DeckInfo");
App::LoadModuleClass($modulename, "SCCC_TemporaryDeckTable");
App::LoadModuleClass($modulename, "SCCC_TempPrizeList");
App::LoadModuleClass($modulename, "SCCC_TransactionDetails");
App::LoadModuleClass($modulename, "SCCC_TempPrizesSummary");
App::LoadModuleClass($modulename, "SCCC_PointsConversionInfo");
App::LoadModuleClass($modulename, "SCC_FreeEntryRegistrants");
App::LoadModuleClass($modulename, "SCC_TerminalSessions");
App::LoadControl("Button");
App::LoadControl("TextBox");
$freeentry = new SCC_FreeEntry();
$cafinovoucher = new SCCV_VoucherInfo();
$transactionsummary = new SCCC_TransactionSummary();
$deckinfo = new SCCC_DeckInfo();
$tempdeck = new SCCC_TemporaryDeckTable();
$tempPrizeList = new SCCC_TempPrizeList();
$transactiondetails = new SCCC_TransactionDetails();
$tempprizesummary = new SCCC_TempPrizesSummary();
$pointconversioninfo = new SCCC_PointsConversionInfo();
$freeentryregistrants = new SCC_FreeEntryRegistrants();
$terminalsession = new SCC_TerminalSessions();
$fproc = new FormsProcessor();

$btnOkay = new Button("btnOkay", "btnOkay", "proceed");
//$btnOkay->CssClass = "proceedbutton";
$btnOkay->IsSubmit = true;
$btnOkay->Args = "onclick='javascript: location.href='result2.php';'";

$fproc->AddControl($btnOkay);
$fproc->ProcessForms();

//legend
$vcode = $_SESSION["vcode"];
$bal = $_SESSION['bal'];
$boolrollback = false;
$terminalID = $_SESSION['terminalid'];
$siteID = $_SESSION["siteid2"];
$projID = 2;
$option2 = 2; //vchr_Option2
$CardCount = 1;
$aid = $_SESSION["terminalid"];

$getV = $cafinovoucher->getOnlyVoucher($_SESSION["vcode"]);

if ($getV[0][0] == "4")
{
//step 1
    $transactionsummary->StartTransaction();
    $transactionsummparam["RequestedBy"] = $aid;
    $transactionsummparam["TransactionDate"] = "now_usec()";
    $transactionsummparam["CardCount"] = "1";
    $transactionsummparam["Status"] = "0";
    $transactionsummparam["Option1"] = $aid;
    $transactionsummary->Insert($transactionsummparam);

    if ($transactionsummary->HasError)
    {
        echo $transactionsummary->getError();
        $transactionsummary->RollBackTransaction();
    }
    else
    {
        $transactionsummary->CommitTransaction();
        $transID = $transactionsummary->LastInsertID;
        $transSummaryID = $transID;
        //get current deck
        $selectCurrentDeckID = $deckinfo->SelectCurrentDeckID($projID);
        $DeckSize = $selectCurrentDeckID[0]["CardCount"];
        $UsedCardCount = $selectCurrentDeckID[0]["UsedCardCount"];
        $DeckID = $selectCurrentDeckID[0]["DeckID"];
        $WinningCardCount = $selectCurrentDeckID[0]["WinningCardCount"];
        //check if deck still has enough cards
        $remainingCardCount = $DeckSize - $UsedCardCount;

        if ($CardCount >= $remainingCardCount)
        {
            //get remaining cards from deck
            $updatedeck = $deckinfo->UpdateDeck($DeckID, $terminalID, '2', $transSummaryID, $remainingCardCount);
            //get records from the deck
            $tempdeck->StartTransaction();

            $getrecord = $tempdeck->InsertDeck($DeckID, $transSummaryID);
            if ($deckinfo->HasError)
            {
                echo $deckinfo->getError();
                $deckinfo->RollBackTransaction();
            }
            else
            {
                if ($tempdeck->HasError)
                {
                    $tempdeck->RollBackTransaction();
                    echo $tempdeck->getError();
                }
                else
                {
                    //deactivate current deck
                    $deactivatedeck = $deckinfo->DeactivateDeck($DeckID, $UsedCardCount, $remainingCardCount);
                    $selectID = $deckinfo->SelectDeckIDbyStatus($projID);
                    $DeckSize = $selectID[0]["CardCount"];
                    $UsedCardCount = $selectID[0]["UsedCardCount"];
                    $DeckID = $selectID[0]["DeckID"];
                    $remainingCardCount2 = 1 - $remainingCardCount;
                    //active queueddeck
                    $activedeck = $deckinfo->ActiveDeck($DeckID, $remainingCardCount2);
                    //get from the newly activated deck the remaining cards
                    // update deck
                    $updatedeck1 = $deckinfo->UpdateDeck($DeckID, $terminalID, '2', $transSummaryID, $remainingCardCount);
                    //get records from the deck
                    $getrecord = $tempdeck->InsertDeck($DeckID, $transSummaryID);
                    $deckinfo->CommitTransaction();
                    $tempdeck->CommitTransaction();
                }
            }
        }
        else
        {
            $deckinfo->StartTransaction();
            //update deck   
            $deckinfo->UpdateDeck($DeckID, $terminalID, '2', $transSummaryID, $CardCount);
            if ($deckinfo->HasError)
            {
                $deckinfo->RollBackTransaction();
                echo $deckinfo->getError();
            }
            else
            {
                $deckinfo->CommitTransaction();
                $tempdeck->StartTransaction();
                //get records from the deck
                $tempdeck->InsertDeck($DeckID, $transSummaryID);
                if ($tempdeck->HasError)
                {
                    $tempdeck->RollBackTransaction();
                    echo $tempdeck->getError();
                }
                else
                {
                    $tempdeck->CommitTransaction();
                }
            }
        }
        //update prize description
        $updateprize = $tempdeck->UpdatePrizeDescription($transSummaryID); // prize description =  $1 cash
        $insertintowinners = $tempdeck->InsertWinnersTable($projID, $DeckID, $transSummaryID, $terminalID, "2");
        if ($tempdeck->HasError)
        {
            $tempdeck->RollBackTransaction();
            echo $tempdeck->getError();
        }
        else
        {
            $selecttotalcash = $tempdeck->SelectTotalCashWinning($transSummaryID);
            $totalcash = $selecttotalcash[0]["TotalCash"];
            //select winning card
            $winningcardscount = $tempdeck->SelectWinningCards($transSummaryID);
            $WinningCard = $winningcardscount[0]["WinningCardCounts"]; // value =1
            //insert into temp prize list
            if ($WinningCard > 0)
            {
                //insert into prizelist
                $tempdeck->InsertPrizeList($transSummaryID);
                if ($tempdeck->HasError)
                {
                    $tempdeck->RollBackTransaction();
                    echo $tempdeck->getError();
                }
                else
                {
                    //$tempdeck->CommitTransaction();
                    $tempPrizeList->StartTransaction();
                    $insertintoprizesummary = $tempPrizeList->InsertintoTempPrizeSummary($transSummaryID);
                    if ($tempPrizeList->HasError)
                    {
                        $tempPrizeList->RollBackTransaction();
                        echo $tempPrizeList->getError();
                    }
                    else
                    {
                        $tempPrizeList->CommitTransaction();
                    }
                }
            }
            $transactiondetails->StartTransaction();
            $transactiondetailsparam["TransactionSummaryID"] = $transSummaryID;
            $transactiondetailsparam["DeckID"] = $DeckID;
            $transactiondetailsparam["CardCount"] = 1;
            $transactiondetailsparam["TransactionDate"] = "now_usec()";
            $transactiondetails->Insert($transactiondetailsparam);
            if ($transactiondetails->HasError)
            {
                $transactiondetails->RollBackTransaction();
                echo $transactiondetails->getError();
            }
            else
            {
                $transactiondetails->CommitTransaction();
                //update transaction Summary table
                $updatesummary = $transactionsummary->UpdateTransaction($transSummaryID);
                // $transactionsummary->CommitTransaction();
                if (!isset($UsedCardCount))
                {
                    $UsedCardCount = 0;
                }
                if (!isset($WinningCardCount))
                {
                    $WinningCardCount = 0;
                }
                $UsedCardCnt = $UsedCardCount + $CardCount;
                $WinningCardCnt = $WinningCardCount + $WinningCard;
                if ($remainingCardCount2 > 0)
                {
                    $updatedeckbywinning = $deckinfo->UpdateDeckbyWinningCardCount($DeckID, $WinningCardCnt);
                }
                else
                {
                    $updatedeckbyused = $deckinfo->UpdateDeckbyUsedCardCount($DeckID, $WinningCardCnt, $UsedCardCnt);
                }
                // $deckinfo->CommitTransaction();
                if ($option2 == 1) //for regular gaming
                {
                    if ($WinningCard > 0)
                    {
                        $ChrTotalCash = "$" . number_format($totalcash, 2) . " Cash";
                        if ($totalcash > 0)
                            $allprizes = $ChrTotalCash;
                        else
                            $allprizes = "";

                        //select count prize description
                        $tempprizesummary->StartTransaction();
                        $selectprizedesc = $tempprizesummary->SelectPrizeDescription($transSummaryID);
                        $prizeTypeCnt = $selectprizedesc[0]["PrizeTypeCnt"];

                        for ($x = 0; $x < count($prizeTypeCnt); $x++)
                        {
                            $getTempPrizes = $tempprizesummary->SelectTempPrizes($transSummaryID);
                            $tempCnt = $getTempPrizes[0]["PrizeCount"];
                            $prizeDes = $getTempPrizes[0]["PrizeDescription"];
                            $id = $getTempPrizes[0]["ID"];
                            $NonCash = "" . $tempCnt . "" . $prizeDes . " ";
                            $allprizes = "" . $allprizes . "" . $NonCash . " ";
                            $tempprizesummary->DeleteByID($id);
                        }
                        if ($tempprizesummary->HasError)
                        {
                            $tempprizesummary->RollBackTransaction();
                            echo $tempprizesummary->getError();
                        }
                        else
                        {
                            $tempprizesummary->CommitTransaction();
                            $msg = "You've won the following prize/s:" . $allprizes . ". Your Transaction Reference ID is " . str_pad($transSummaryID, 14, "0", STR_PAD_LEFT) . ". Please proceed to the cashier to claim your winning.";
                            $_SESSION["refreshmode"] = 1;
                        }
                    }
                    else
                    {
                        $_SESSION["refreshmode"] = 1;
                        $msg = "You did not get any winning card.Your Transaction Reference ID is " . str_pad($transSummaryID, 14, "0", STR_PAD_LEFT) . ".Thank you for playing. ";
                    }
                }
                else //for gold kick and free entry
                {
                    $selectallprize = $tempdeck->SelectAllPrizes($transSummaryID);
                    $allprizes = $selectallprize[0]["PrizeDescription"];
                    $ecn = $selectallprize[0]["ECN"];
                    $tempprizetype = $selectallprize[0]["PrizeType"];
                    $tempprizevalue = $selectallprize[0]["PrizeValue"];
                    if ($tempprizetype == 6)
                    {
                        $allprizes = "" . $selectallprize[0]["PrizeDescription"] . "";
                    }
                    else
                    {
                        $allprizes = "(1)" . $selectallprize[0]["PrizeDescription"] . "";
                    }

                    if ($projID == 2)
                    {
                        $ecn = str_pad($transSummaryID, 14, "0", STR_PAD_LEFT);
                    }
                    if ($WinningCard > 0)
                    {
                        $_SESSION["refreshmode"] = 1;
                        $msg = "You've won " . $allprizes . ". Your Transaction Reference ID is " . $ecn . ".<br/>Please proceed to the cashier to claim your winning. ";
                    }
                    else
                    {
                        $_SESSION["refreshmode"] = 1;
                        $msg = "This is not a winning card. Your Transaction Reference ID is " . $ecn . ". <br /> Try our sweeptakes games and increase your chance of winning more and big prizes!<br/> Thank you for playing.";
                    }
                }

                $changestatus = array();
                $changestatus["winningcardcount"] = $WinningCardCnt;
                $changestatus['allprizes'] = $allprizes;
                $changestatus['transsummaryid'] = $transSummaryID;
                $changestatus['projid'] = $projID;
                $changestatus['vcode'] = $vcode;
                $changestatus['terminalid'] = $terminalID;
                $changestatus['cardcount'] = $CardCount;
                $changestatus['winningcard'] = $WinningCard;
                $changestatus['bal'] = $bal;

                $_SESSION['changestatus'] = $changestatus;
                $summary = $tempdeck->SelectAllPrizes($transSummaryID);
                $wintype = $summary[0]["PrizeDescription"];
                $ecn = $summary[0]["ECN"];
                if ($wintype == "$0 Cash")
                {
                    $wintype = "Try Again";
                }
                $_SESSION['ecn'] = $ecn;
                $_SESSION['wintype'] = $wintype;
                //delete from temporarydecktable
                if ($tempdeck->HasError)
                {
                    $tempdeck->RollBackTransaction();
                    echo $tempdeck->getError();
                }
                else
                {
                    $tempdeck->DeleteTransactionSummaryID($transSummaryID);
                    $tempPrizeList->DeleteTransactionSummaryID($transSummaryID);

                    //start add mtcc on 01/17/2013
                    $terminalsession->StartTransaction();

                    $terminalsession->UpdateDateEndPerSiteID($terminalID, $siteID);

                    if ($terminalsession->HasError)
                    {
                        $terminalsession->RollBackTransaction();
                    }
                    else
                    {
                        $terminalsession->CommitTransaction();
                    }
                    //end add mtcc on 01/17/2013
                }
            }
        }
    }
}
else
{
    unset($_SESSION["vcode"]);
    unset($_SESSION["changestatus"]);
    unset($_SESSION["refreshmode"]);
//    App::Pr("<script> window.location = 'welcome.php'; </script>");
}


if ($fproc->IsPostBack)
{
    if (isset($_SESSION['vcode']))
    {
        $checkstatus = $cafinovoucher->CheckStatus($_SESSION['vcode']);
        $status = $checkstatus[0][0];
        if ($status == "")
        {
            // URL::Redirect('welcome1.php');
            unset($_SESSION['bal']);
        }
    }
}
?>
