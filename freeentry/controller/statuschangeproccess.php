<?php 
/**Date Created September 28-2012
 * Jerome Jose */
require_once("../init.inc.php");
App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");
$cwinningsdump = new SCC_WinningsDump();

$terminalid = $_SESSION["terminalid"];

$cwinningsdump->StartTransaction();

$cwinningsdump->UpdateCardsStatus($terminalid);
if($cwinningsdump->HasError)
{
    $cwinningsdump->RollBackTransaction();
    echo "Error";
}
else 
{
    $cwinningsdump->CommitTransaction();
    echo true;
}


                                
?>
