<?php

/** Date Created JFJ 05-16-2012 */
require_once("include/core/init.inc.php");
$modulename = "SweepsCenter";
app::LoadModuleClass($modulename, "SCC_Terminals");
App::LoadModuleClass($modulename, "SCC_TerminalSessions");
App::LoadModuleClass($modulename, "SCC_AuditTrail");

App::LoadControl("Button");
App::LoadControl("TextBox");

$loginterminal = new SCC_Terminals();
$terminalSessions = new SCC_TerminalSessions();
$audittrail = new SCC_AuditTrail();

$fproc = new FormsProcessor();

$txtUsername = new TextBox("txtUsername", "txtUsername");
$txtUsername->CssClass = "inputBoxEffect";
$txtUsername->Args = "onkeypress='javascript: return chkChar(event);'";
$txtUsername->Length = 20;

$txtPassword = new TextBox("txtPassword", "txtPassword");
$txtPassword->CssClass = "inputBoxEffect";
$txtPassword->Password = true;
$txtPassword->Length = 20;
$txtPassword->Args = "autocomplete='off'";
$txtPassword->Args = "onkeypress='javascript: return chkChar(event);'";

$btnSubmit = new Button("btnSubmit", "btnSubmit", " ");
$btnSubmit->CssClass = "login";
$btnSubmit->Args = "onclick='javascript: return checklogin();'";
$btnSubmit->IsSubmit = true;


$btnwelcome = new Button("btnWelcome", "btnWelcome");
$btnwelcome->Text = " ";
$btnwelcome->IsSubmit = true;
$btnwelcome->Args = "onclick='URL::Redirect(welcome.php);'";
$btnwelcome->CssClass = "inputBoxEffectPopup";

$fproc->AddControl($txtUsername);
$fproc->AddControl($txtPassword);
$fproc->AddControl($btnSubmit);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{

    $username = mysql_escape_string(trim($txtUsername->Text));
    $password = mysql_escape_string(trim($txtPassword->Text));

    if (count($btnSubmit->SubmittedValue) == "1")
    {

        $checking = $loginterminal->CheckUsernamePassword($username, $password);

        if (count($checking) == 1)
        {

            $arrDetails = $checking[0];
            $aid = $arrDetails["ID"];
            $name = $arrDetails["Name"];
            //$password = $arrDetails["Password"];
            $siteID = $arrDetails["SiteID"];
            $balance = $arrDetails["Balance"];
            $eqpoint = $arrDetails["EquivalentPoints"];
            $serviceID = $arrDetails["ServiceID"];
            $playable = $arrDetails["IsPlayable"];
            $freentry = $arrDetails["IsFreeEntry"];
            $status = $arrDetails["Status"];
//            $_SESSION["siteid"] = $status;
            $_SESSION["terminalid"] = $aid;
            $_SESSION["siteid2"] = $siteID;
            session_regenerate_id();
            $_SESSION['sid'] = session_id();
            $_SESSION['username'] = $username;
            //check if status is active
            if ($status == 1)
            {
                $getTerminal = $loginterminal->getTerminalID($username, $password);
                $arrTerminal = $getTerminal[0];
                $TerminalID = $arrTerminal["ID"];
                $siteID = $arrTerminal["SiteID"];

//                $terminalSessions->StartTransaction();
//
//                //check if terminal still has an existing session
//                $arrexisting = $terminalSessions->SelectTerminalSessionDetails($aid);
//                if (count($arrexisting) > 0)
//                {
//                    //end existing session              
//                    $arrendsession = $terminalSessions->UpdateUnendedFreeEntrySession();
//                    if ($terminalSessions->HasError)
//                    {
//                        $error_msgtitle = " Error";
//                        $error_msg = "Error in Terminal Sessions" . $terminalSessions->getError();
//                        $terminalSessions->RollBackTransaction();
//                    }
//                }


//                //insert into terminal Session
//                $arrTerminalsess["TerminalID"] = $aid;
//                $arrTerminalsess["SiteID"] = $siteID;
//                $arrTerminalsess["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
//                $arrTerminalsess["DateStart"] = "now_usec()";
//                $arrTerminalsess["DateCreated"] = "now_usec()";
//                $terminalSessions->Insert($arrTerminalsess);
//                $terminalSessions->CommitTransaction();
//
//                if ($terminalSessions->HasError)
//                {
//                    $error_msgtitle = " Error";
//                    $error_msg = "Error in Terminal Sessions" . $terminalSessions->getError();
//                    $terminalSessions->RollBackTransaction();
//                }
//                else
//                {
//
//                    $terminalsessionid = $terminalSessions->LastInsertID;
//                    $_SESSION['terminalsid'] = $terminalsessionid;

                    //insert in audittrail
                    $audittrail->StartTransaction();
                    $audittrailparam["SessionID"] = $_SESSION['sid'];
                    $audittrailparam["AccountID"] = "0";
                    $audittrailparam["TransDetails"] = "Login Free Entry Terminal: " . $username;
                    $audittrailparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                    $audittrailparam["TransDateTime"] = "now_usec()";
                    $audittrail->Insert($audittrailparam);
                    $audittrail->CommitTransaction();

                    if ($audittrail->HasError)
                    {
                        $error_msgtitle = " Error";
                        $error_msg = "Error in Audittrail" . $audittrail->getError();
                        $audittrail->RollBackTransaction();
                    }
                    else
                    {
                        App::Pr("<script> window.location = 'welcome.php'; </script>");
                    }
 //               }
            }
            else
            {
                $error_msgtitle = " Error";
                $error_msg = "Your account is not allowed to login.";
            }
        }
        else
        {
            $error_msgtitle = "INVALID LOG IN";
            $error_msg = "You have entered an invalid account information.";
        }
    }
}
?>



