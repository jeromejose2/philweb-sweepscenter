<?php //

/*
 * Created by JFJ 06-19-2012
 * Purpose : to manage single login and send to default page if not login
 */
require_once("../init.inc.php");
if($_SESSION['login'] == NULL || !isset($_SESSION['login']))
{
     App::Pr("<script> window.location = 'index.php'; </script>");
}
    $timenow = strtotime('now');
    $logintime = $_SESSION['login'];
    $timediff = $timenow - $logintime;
    
if($timediff >= 57600) // SESSION TIMEOUT 16 hours
{
 unset($_SESSION['login']);
}
/*$modulename = "SweepsCenter";
app::LoadModuleClass($modulename, "SCC_Terminals");
App::LoadModuleClass($modulename, "SCC_TerminalSessions");
App::LoadModuleClass($modulename, "SCC_AuditTrail");

$audittrail = new SCC_AuditTrail();
$terminalSessions = new SCC_TerminalSessions();


$terminalSessions->checkdateend();

if (isset($_SESSION["terminalid"]) && $_SESSION["terminalid"] != '')
{
    //var_dump($_SESSION['terminalsid']);exit();
    $terminalid = $_SESSION["terminalid"];
    $terminalsessionid = $_SESSION['terminalsid'];
    $terminalSessions->StartTransaction();

    $existterminal = $terminalSessions->ifexistsession($terminalsessionid + 1);

    if (count($existterminal) == 1)
    {
        $checkterminal = $terminalSessions->getSessionDtls($terminalid, $terminalsessionid);
        if (count($checkterminal) == 1)
        {
            $checkdateEnd = $terminalSessions->checkdateend();

            if (count($checkdateEnd) > 0)
            {
                $updateterminalsessionDATEEND = $terminalSessions->UpdateTerminalSessions($terminalid, $terminalsessionid);
                if ($terminalSessions->HasError)
                {
                    echo $terminalSessions->getError();
                    $terminalSessions->RollBackTransaction();
                }
                else
                {
                    $terminalSessions->CommitTransaction();
                    session_destroy();
                    App::Pr("<script> window.location = 'index.php'; </script>");
                }
            }
        }
        else
        {
            App::Pr("<script> window.location = 'index.php'; </script>");
        }
    }
}
else
{
    App::Pr("<script> window.location = 'index.php'; </script>");
}
 */
?>
