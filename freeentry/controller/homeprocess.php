<?php

/**
 * Date Created 05-23-2012
 * JFJ
 */
require_once("../init.inc.php");
App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");
$cwinningsdump = new SCC_WinningsDump();

$terminalid = $_SESSION["terminalid"];
$isFinalized = $cwinningsdump->SelectAllWinningsIsOpened($terminalid);
if (count($isFinalized) > 0)
{
    echo json_encode(1);
}
else
{
    $checkisfinal = $cwinningsdump->SelectAllWinnings($terminalid);
    if ($checkisfinal)
    {
        echo json_encode(1);
    }
    else
    {
        $cwinningsdump->StartTransaction();
        $cwinningsdump->DeleteByTerminalID($terminalid);
        if ($cwinningsdump->HasError)
        {
            
        }
        else
        {
            $cwinningsdump->CommitTransaction();
            echo json_encode(2);
        }
    }
}
?>
