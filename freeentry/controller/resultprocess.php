<?php
/*
 * @author JFJ 05-21-2012
 * Purpose   : controller for result
 */
require_once("../init.inc.php");
$modulename = "SweepsCenter";
APP::LoadModuleClass($modulename, "SCC_FreeEntry");
APP::LoadModuleClass($modulename, "SCCV_VoucherInfo");
APP::LoadModuleClass($modulename, "SCCC_TransactionSummary");
APP::LoadModuleClass($modulename, "SCCC_DeckInfo");
App::LoadModuleClass($modulename, "SCCC_TemporaryDeckTable");
App::LoadModuleClass($modulename, "SCCC_TempPrizeList");
App::LoadModuleClass($modulename, "SCCC_TransactionDetails");
App::LoadModuleClass($modulename, "SCCC_TempPrizesSummary");
App::LoadModuleClass($modulename, "SCCC_PointsConversionInfo");
App::LoadModuleClass($modulename, "SCC_FreeEntryRegistrants");
App::LoadModuleClass($modulename, "SCC_TerminalSessions");
App::LoadModuleClass($modulename, "SCC_TerminalSessionDetails");
App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");


App::LoadControl("Button");
App::LoadControl("TextBox");
$freeentry = new SCC_FreeEntry();
$cafinovoucher = new SCCV_VoucherInfo();
$transactionsummary = new SCCC_TransactionSummary();
$deckinfo = new SCCC_DeckInfo();
$tempdeck = new SCCC_TemporaryDeckTable();
$tempPrizeList = new SCCC_TempPrizeList();
$transactiondetails = new SCCC_TransactionDetails();
$tempprizesummary = new SCCC_TempPrizesSummary();
$pointconversioninfo = new SCCC_PointsConversionInfo();
$freeentryregistrants = new SCC_FreeEntryRegistrants();
$terminalsession = new SCC_TerminalSessions();
$terminalsessiondtls = new SCC_TerminalSessionDetails();
$cwinningsdump = new SCC_WinningsDump();
$fproc = new FormsProcessor();

$btnOkay = new Button("btnOkay", "btnOkay", "proceed");
//$btnOkay->CssClass = "proceedbutton";
$btnOkay->IsSubmit = true;
$btnOkay->Args = "onclick='javascript: location.href='result2.php';'";

$fproc->AddControl($btnOkay);
$fproc->ProcessForms();

//legend
$vcode = $_SESSION["vcode"];
$bal = $_SESSION['bal'];
$boolrollback = false;
$terminalID = $_SESSION['terminalid'];
$siteID = $_SESSION["siteid2"];
$projID = 2;
$option2 = 2; //vchr_Option2
$CardCount = 1;
$aid = $_SESSION["terminalid"];

$login = $_SESSION["terminalname"];

//if ($getV[0][0] == "4")
//{
//step 1
    $transactionsummary->StartTransaction();
    $transactionsummparam["RequestedBy"] = $aid;
    $transactionsummparam["TransactionDate"] = "now_usec()";
    $transactionsummparam["CardCount"] = "1";
    $transactionsummparam["Status"] = "0";
    $transactionsummparam["Option1"] = $aid;
    $transactionsummary->Insert($transactionsummparam);

    if ($transactionsummary->HasError)
    {
        echo $transactionsummary->getError();
        $transactionsummary->RollBackTransaction();
    }
    else
    {
        $transactionsummary->CommitTransaction();
                    
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO transactionsummary: " . serialize($transactionsummparam) . "; \r\n");
                    fclose($fp);
                    
        $transID = $transactionsummary->LastInsertID;
        $transSummaryID = $transID;
        //get current deck
        $selectCurrentDeckID = $deckinfo->SelectCurrentDeckID($projID);
        $DeckSize = $selectCurrentDeckID[0]["CardCount"];
        $UsedCardCount = $selectCurrentDeckID[0]["UsedCardCount"];
        $DeckID = $selectCurrentDeckID[0]["DeckID"];
        $WinningCardCount = $selectCurrentDeckID[0]["WinningCardCount"];
        //check if deck still has enough cards
        $remainingCardCount = $DeckSize - $UsedCardCount;

        if ($CardCount >= $remainingCardCount)
        {
            //get remaining cards from deck
            $updatedeck = $deckinfo->UpdateDeck($DeckID, $terminalID, '2', $transSummaryID, $remainingCardCount);
            
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: " .$DeckID. ";".$terminalID.";2;".$transSummaryID.";".$remainingCardCount."; \r\n");
                    fclose($fp);
            //get records from the deck
            $tempdeck->StartTransaction();

            $getrecord = $tempdeck->InsertDeck($DeckID, $transSummaryID);
                                
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO temporarydecktable: ".$DeckID.";".$transSummaryID."; \r\n");
                    fclose($fp);
                    
            if ($deckinfo->HasError)
            {
                echo $deckinfo->getError();
                $deckinfo->RollBackTransaction();
            }
            else
            {
                if ($tempdeck->HasError)
                {
                    $tempdeck->RollBackTransaction();
                    echo $tempdeck->getError();
                }
                else
                {
                    //deactivate current deck
                    $deactivatedeck = $deckinfo->DeactivateDeck($DeckID, $UsedCardCount, $remainingCardCount);
                                                    
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: ".$DeckID.";".$UsedCardCount.";".$remainingCardCount."\r\n");
                    fclose($fp);
                    
                    $selectID = $deckinfo->SelectDeckIDbyStatus($projID);
                    $DeckSize = $selectID[0]["CardCount"];
                    $UsedCardCount = $selectID[0]["UsedCardCount"];
                    $DeckID = $selectID[0]["DeckID"];
                    $remainingCardCount2 = 1 - $remainingCardCount;
                    //active queueddeck
                    $activedeck = $deckinfo->ActiveDeck($DeckID, $remainingCardCount2);
                    //get from the newly activated deck the remaining cards
                                                                        
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: ".$DeckID.";".$remainingCardCount2.";\r\n");
                    fclose($fp);
                    // update deck
                    $updatedeck1 = $deckinfo->UpdateDeck($DeckID, $terminalID, '2', $transSummaryID, $remainingCardCount);
                                
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: " .$DeckID. ";".$terminalID.";2;".$transSummaryID.";".$remainingCardCount."; \r\n");
                    fclose($fp);
                    //get records from the deck
                    $getrecord = $tempdeck->InsertDeck($DeckID, $transSummaryID);
                                                    
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO temporarydecktable: ".$DeckID.";".$transSummaryID."; \r\n");
                    fclose($fp);
                    
                    $deckinfo->CommitTransaction();
                    $tempdeck->CommitTransaction();
                }
            }
        }
        else
        {
            $deckinfo->StartTransaction();
            //update deck   
            $deckinfo->UpdateDeck($DeckID, $terminalID, '2', $transSummaryID, $CardCount);
            if ($deckinfo->HasError)
            {
                $deckinfo->RollBackTransaction();
                echo $deckinfo->getError();
            }
            else
            {
                $deckinfo->CommitTransaction();
                
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deck_".$DeckID.": " .$DeckID  . ";".$terminalID.";2;".$transSummaryID.";".$CardCount."; \r\n");
                    fclose($fp);
                    
                $tempdeck->StartTransaction();
                //get records from the deck
                $tempdeck->InsertDeck($DeckID, $transSummaryID);
                if ($tempdeck->HasError)
                {
                    $tempdeck->RollBackTransaction();
                    echo $tempdeck->getError();
                }
                else
                {
                    $tempdeck->CommitTransaction();
                                
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO temporarydecktable: " .$DeckID. ";".$transSummaryID."; \r\n");
                    fclose($fp);
                }
            }
        }
        //update prize description
        $updateprize = $tempdeck->UpdatePrizeDescription($transSummaryID); 
                      
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE temporarydecktable: ". $transSummaryID."; \r\n");
                    fclose($fp);
                    
                    $insertintowinners =  $tempdeck->SelectIsWinningCard($projID, $DeckID, $transSummaryID, $terminalID, "2");
                     if(count($insertintowinners) == 1)
                     {
                            $tempdeck->InsertWinnersTable($projID, $DeckID, $transSummaryID, $terminalID, "2");
                            $filename = "../../DBLogs/logs.txt";
                            $fp = fopen($filename, "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO winners: ". $transSummaryID."; \r\n");
                            fclose($fp);
                     }
        if ($tempdeck->HasError)
        {
            $tempdeck->RollBackTransaction();
            echo $tempdeck->getError();
        }
        else
        {
            $selecttotalcash = $tempdeck->SelectTotalCashWinning($transSummaryID);
            $totalcash = ($selecttotalcash[0]["TotalCash"] == NULL ) ? '0' : $selecttotalcash[0]["TotalCash"];
            //select winning card
            $winningcardscount = $tempdeck->SelectWinningCards($transSummaryID);
            $WinningCard = $winningcardscount[0]["WinningCardCounts"]; // value =1
            //insert into temp prize list
//            if ($WinningCard > 0)
//            {
                //insert into prizelist
                $tempdeck->InsertPrizeList($transSummaryID);
                                              
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO temporarydecktable: ". $transSummaryID."; \r\n");
                    fclose($fp);
                    
                if ($tempdeck->HasError)
                {
                    $tempdeck->RollBackTransaction();
                    echo $tempdeck->getError();
                }
                else
                {
                    //$tempdeck->CommitTransaction();
                    $tempPrizeList->StartTransaction();
                    $insertintoprizesummary = $tempPrizeList->InsertintoTempPrizeSummary($transSummaryID);
                    if ($tempPrizeList->HasError)
                    {
                        $tempPrizeList->RollBackTransaction();
                        echo $tempPrizeList->getError();
                    }
                    else
                    {
                        $tempPrizeList->CommitTransaction();
                           $filename = "../../DBLogs/logs.txt";
                        $fp = fopen($filename, "a");
                        fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tempprizelist: " .$transSummaryID . "; \r\n");
                        fclose($fp);
                    }
                }
//            }
            $transactiondetails->StartTransaction();
            $transactiondetailsparam["TransactionSummaryID"] = $transSummaryID;
            $transactiondetailsparam["DeckID"] = $DeckID;
            $transactiondetailsparam["CardCount"] = 1;
            $transactiondetailsparam["TransactionDate"] = "now_usec()";
            $transactiondetails->Insert($transactiondetailsparam);
            if ($transactiondetails->HasError)
            {
                $transactiondetails->RollBackTransaction();
                echo $transactiondetails->getError();
            }
            else
            {
                $transactiondetails->CommitTransaction();
                                               
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO transactiondetails: ". serialize($transactiondetailsparam)."; \r\n");
                    fclose($fp);
                    
                //update transaction Summary table
                $updatesummary = $transactionsummary->UpdateTransaction($transSummaryID);
                                             
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE transactionsummary: ". $transSummaryID."; \r\n");
                    fclose($fp);
                    
                // $transactionsummary->CommitTransaction();
                if (!isset($UsedCardCount))
                {
                    $UsedCardCount = 0;
                }
                if (!isset($WinningCardCount))
                {
                    $WinningCardCount = 0;
                }
                $UsedCardCnt = $UsedCardCount + $CardCount;
                $WinningCardCnt = $WinningCardCount + $WinningCard;
                if ($remainingCardCount2 > 0)
                {
                    $updatedeckbywinning = $deckinfo->UpdateDeckbyWinningCardCount($DeckID, $WinningCardCnt);
                                                                 
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: ". $DeckID."; \r\n");
                    fclose($fp);
                }
                else
                {
                    $updatedeckbyused = $deckinfo->UpdateDeckbyUsedCardCount($DeckID, $WinningCardCnt, $UsedCardCnt);
                                                                   
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE deckinfo: ". $DeckID."; \r\n");
                    fclose($fp);
                }
                // $deckinfo->CommitTransaction();
                if ($option2 == 1) //for regular gaming
                {
                    if ($WinningCard > 0)
                    {
                        $ChrTotalCash = "$" . number_format($totalcash, 2) . " Cash";
                        if ($totalcash > 0)
                            $allprizes = $ChrTotalCash;
                        else
                            $allprizes = "";

                        //select count prize description
                        $tempprizesummary->StartTransaction();
                        $selectprizedesc = $tempprizesummary->SelectPrizeDescription($transSummaryID);
                        $prizeTypeCnt = $selectprizedesc[0]["PrizeTypeCnt"];

                            $getTempPrizes = $tempprizesummary->SelectTempPrizes($transSummaryID);
                            $tempCnt = $getTempPrizes[0]["PrizeCount"];
                            $prizeDes = $getTempPrizes[0]["PrizeDescription"];
                            $id = $getTempPrizes[0]["ID"];
                            $NonCash = "" . $tempCnt . "" . $prizeDes . " ";
                            $allprizes = "" . $allprizes . "" . $NonCash . " ";
                            $tempprizesummary->DeleteByID($id);
                        
                        if ($tempprizesummary->HasError)
                        {
                            $tempprizesummary->RollBackTransaction();
                            echo $tempprizesummary->getError();
                        }
                        else
                        {
                            $tempprizesummary->CommitTransaction();
                                                                                               
                                $filename = "../../DBLogs/logs.txt";
                                $fp = fopen($filename, "a");
                                fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tempprizesummary: ". $id."; \r\n");
                                fclose($fp);
                    
                            $msg = "You've won the following prize/s:" . $allprizes . ". Your Transaction Reference ID is " . str_pad($transSummaryID, 14, "0", STR_PAD_LEFT) . ". Please proceed to the cashier to claim your winning.";
                            $_SESSION["refreshmode"] = 1;
                        }
                    }
                    else
                    {
                        $_SESSION["refreshmode"] = 1;
                        $msg = "You did not get any winning card.Your Transaction Reference ID is " . str_pad($transSummaryID, 14, "0", STR_PAD_LEFT) . ".Thank you for playing. ";
                    }
                }
                else //for gold kick and free entry
                {
                    $selectallprize = $tempdeck->SelectAllPrizes($transSummaryID);
                    $allprizes = $selectallprize[0]["PrizeDescription"];
                    $ecn = $selectallprize[0]["ECN"];
                    $tempprizetype = $selectallprize[0]["PrizeType"];
                    $tempprizevalue = $selectallprize[0]["PrizeValue"];
                    if ($tempprizetype == 6)
                    {
                        $allprizes = "" . $selectallprize[0]["PrizeDescription"] . "";
                    }
                    else
                    {
                        $allprizes = "(1)" . $selectallprize[0]["PrizeDescription"] . "";
                    }

                    if ($projID == 2)
                    {
                        $ecn = str_pad($transSummaryID, 14, "0", STR_PAD_LEFT);
                    }
                    if ($WinningCard > 0)
                    {
                        $_SESSION["refreshmode"] = 1;
                        $msg = "You've won " . $allprizes . ". Your Transaction Reference ID is " . $ecn . ".<br/>Please proceed to the cashier to claim your winning. ";
                    }
                    else
                    {
                        $_SESSION["refreshmode"] = 1;
                        $msg = "This is not a winning card. Your Transaction Reference ID is " . $ecn . ". <br /> Try our sweeptakes games and increase your chance of winning more and big prizes!<br/> Thank you for playing.";
                    }
                }

//start of statuschangeprocess.php
                                   //update transaction summary table
                                   $updatewinnings = $transactionsummary->UpdateWinnings($WinningCardCnt, $allprizes, $transSummaryID);
                                                                                  
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE transactionsummary: ". $WinningCardCnt.";". $allprizes.";". $transSummaryID."; \r\n");
                    fclose($fp);
                                                       //insert into point conversion history
                                               if($projID == 2)
                                               {
                                                   if($vcode != "")
                                                   {
                                                        $selectDenomination = $cafinovoucher->SelectDenomination($vcode);
                                                        $denomination = $selectDenomination[0]["Denomination"];
                                                            if($denomination == 1)
                                                            {
                                                                $pointsparam["TransactionSummaryID"] = $transSummaryID;
                                                                $pointsparam["ProjectID"] = $projID;
                                                                $pointsparam["TerminalID"] = $terminalID;
                                                                $pointsparam["DateTimeTrans"] = "now_usec()";
                                                                $pointsparam["ConvertedPoints"] = "0.10";
                                                                $pointsparam["EquivalentCards"] = $CardCount;
                                                                $pointsparam["NoOfWinningCards"] = $WinningCard;
                                                                $pointsparam["Winnings"] = $allprizes;
                                                            }
                                                            else
                                                            {
                                                                $pointsparam["TransactionSummaryID"] = $transSummaryID;
                                                                $pointsparam["ProjectID"] = $projID;
                                                                $pointsparam["TerminalID"] = $terminalID;
                                                                $pointsparam["DateTimeTrans"] = "now_usec()";
                                                                $pointsparam["ConvertedPoints"] = $bal;
                                                                $pointsparam["EquivalentCards"] = $CardCount;
                                                                $pointsparam["NoOfWinningCards"] = $WinningCard;
                                                                $pointsparam["Winnings"] = $allprizes;
                                                            }
                                                   }else
                                                   {            
                                                       $pointsparam["TransactionSummaryID"] = $transSummaryID;
                                                       $pointsparam["ProjectID"] = $projID;
                                                       $pointsparam["TerminalID"] = $terminalID;
                                                       $pointsparam["DateTimeTrans"] = "now_usec()";
                                                       $pointsparam["ConvertedPoints"] = $bal;
                                                       $pointsparam["EquivalentCards"] = $CardCount;
                                                       $pointsparam["NoOfWinningCards"] = $WinningCard;
                                                       $pointsparam["Winnings"] = $allprizes;
                                                    }
                                                    $pointconversioninfo->StartTransaction();
                                                    $pointconversioninfo->Insert($pointsparam);
                                                    if($pointconversioninfo->HasError)
                                                    {
                                                        $pointconversioninfo->RollBackTransaction();
                                                        echo $pointconversioninfo->getError();
                                                    }else
                                                    {
                                                        $pointconversioninfo->CommitTransaction();
                                                                                                                                          
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp = fopen($filename, "a");
                                            fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO pointsconversioninfo: ". serialize($pointsparam)."; \r\n");
                                            fclose($fp);
                                                    }
                                             }
                                                       $cafinovoucher->StartTransaction();
                                                       //update voucher status
                                                       $cafinovoucher->UpdateVoucher($terminalID, $allprizes, $vcode);
                                                       if($cafinovoucher->HasError)
                                                       {
                                                           $cafinovoucher->RollBackTransaction();
                                                           echo $cafinovoucher->getError();
                                                       }else
                                                        {
                                                            $cafinovoucher->CommitTransaction();
                                                            
                                                                                                                                                           
                                $filename = "../../DBLogs/logs.txt";
                                $fp = fopen($filename, "a");
                                fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_voucherinfo: ". $terminalID.";".$allprizes.";".$vcode."; \r\n");
                                fclose($fp);
                                
                                                            $freeentry->StartTransaction();
                                                                //update voucher date played
                                                                $ifexist = $freeentry->SelectVoucherCode($vcode);

                                                                if ($ifexist[0][0] != NULL)
                                                                {   
                                                                    $freeentry->UpdateDatePlayed($vcode);
                                                                    if($freeentry->HasError)
                                                                    {
                                                                        $freeentry->RollBackTransaction();
                                                                        echo $freeentry->getError();
                                                                    }else
                                                                    {
                                                                        $freeentry->CommitTransaction();
                                                                                                                                                                                                                    
                                                                        $filename = "../../DBLogs/logs.txt";
                                                                        $fp = fopen($filename, "a");
                                                                        fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_freeentry: ". $vcode."; \r\n");
                                                                        fclose($fp);
                                                                    }
                                                                }else
                                                                {   
                                                                    $freeentryregistrants->StartTransaction();
                                                                    $freeentryregistrants->UpdateDatePlayed($vcode);
                                                 
                                                                    if($freeentryregistrants->HasError)
                                                                    {
                                                                        $freeentryregistrants->RollBackTransaction();
                                                                        echo $freeentryregistrants->getError();
                                                                    }else
                                                                    {
                                                                        $freeentryregistrants->CommitTransaction();
                                                                            $filename = "../../DBLogs/logs.txt";
                                                                            $fp = fopen($filename, "a");
                                                                            fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_freeentryregistrants: ". $vcode."; \r\n");
                                                                            fclose($fp);
                                                                    }
                                                                }
                                                       }
//end of statuschangeprocess.php
                                                  
                $summary = $tempdeck->SelectAllPrizes($transSummaryID);
                $wintype = $summary[0]["PrizeDescription"];
                $ecn = $summary[0]["ECN"];
                if ($wintype == "$0 Cash")
                {
                    $wintype = "Try Again";
                }
                      
                //start of result2process.php
                    $cafinovoucher->StartTransaction();
                    $cafinovoucher->UpdateWinnings($wintype, $_SESSION['vcode']);
                    if ($cafinovoucher->HasError)
                    {
                        echo $cafinovoucher->getError();
                        $cafinovoucher->RollBackTransaction();
                    }
                    else
                    {
                        $cafinovoucher->CommitTransaction();
                            $filename = "../../DBLogs/logs.txt";
                            $fp = fopen($filename, "a");
                            fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_voucherinfo: ". $wintype.";".$_SESSION['vcode']."; \r\n");
                            fclose($fp);
                    }
                //end of result2process.php
                                                   
                $cwinningsdump->StartTransaction();                        
                $winning_array["ECN"] = $ecn;
                $winning_array["WinType"] = $wintype;
                $winning_array["FK_TerminalID"] = $terminalID;
                $winning_array["IsOpened"] = "N";
                $winning_array["isFinalized"] = "0";
               
                $cwinningsdump->Insert($winning_array);
                if($winning_array->HasError)
                {
                    $cwinningsdump->RollBackTransaction();
                    echo $cwinningsdump->getError();
                }
                else 
                {
                    $cwinningsdump->CommitTransaction();
                    
                    $filename = "../../DBLogs/logs.txt";
                    $fp = fopen($filename, "a");
                    fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || INSERT INTO tbl_winnings_dump: " . serialize($winning_array) . "; \r\n");
                    fclose($fp);
                
                                //delete from temporarydecktable
                                if ($tempdeck->HasError)
                                {
                                    $tempdeck->RollBackTransaction();
                                    echo $tempdeck->getError();
                                }
                                else
                                {
                                    $tempdeck->DeleteTransactionSummaryID($transSummaryID);
                                    $tempPrizeList->DeleteTransactionSummaryID($transSummaryID);
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp = fopen($filename, "a");
                                            fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || DELETE From tbl_terminalsessions: " .$transSummaryID . "; \r\n");
                                            fclose($fp);
                                            
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp = fopen($filename, "a");
                                            fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || DELETE From tempprizelist: " .$transSummaryID . "; \r\n");
                                            fclose($fp);
                                    //start add by mtcc 02/05/2013
                                    $terminalsessionid = $_SESSION['terminalsid'];
                                    $maxiddtls = $terminalsessiondtls->SelectMaxId();
                                    $lastTransId = $maxiddtls[0]['max'];
                                    $lastTransId = ($lastTransId != null) ? $lastTransId : 0;
                                    $lastTransId = ($lastTransId + 1);
                                    $transid = 'W' . str_pad($terminalsessionid, 6, 0, STR_PAD_LEFT) . str_pad($lastTransId, 10, 0, STR_PAD_LEFT);

                                    $terminalsessiondtls->StartTransaction();
                                    $arrTerminalSessionDetails['TerminalSessionID'] = $terminalsessionid;
                                    $arrTerminalSessionDetails['TransactionType'] = 'W';
                                    $arrTerminalSessionDetails['Amount'] = '0.00';
                                    $arrTerminalSessionDetails['AcctID'] = $terminalID;
                                    $arrTerminalSessionDetails['ServiceID'] = 1;
                                    $arrTerminalSessionDetails['TransID'] = $transid;
                                    $arrTerminalSessionDetails['ServiceTransactionID'] = $transSummaryID;
                                    $arrTerminalSessionDetails['TransDate'] = 'now_usec()';
                                    $arrTerminalSessionDetails['DateCreated'] = 'now_usec()';

                                    $terminalsessiondtls->Insert($arrTerminalSessionDetails);

                                    if ($terminalsessiondtls->HasError)
                                    {
                                        $terminalsessiondtls->RollBackTransaction();
                                        echo "Error has occured: " . $terminalsessiondtls->getError();
                                    }
                                    else
                                    {
                                        $terminalsessiondtls->CommitTransaction();

                                        //start add mtcc on 01/17/2013
                                        $terminalsession->StartTransaction();

                                        $terminalsession->UpdateDateEndPerSiteID($terminalID, $siteID);
                                        

                                        if ($terminalsession->HasError)
                                        {
                                            $terminalsession->RollBackTransaction();
                                        }
                                        else
                                        {
                                            $terminalsession->CommitTransaction();
                                               
                                            $filename = "../../DBLogs/logs.txt";
                                            $fp = fopen($filename, "a");
                                            fwrite($fp, date("Y-m-d H:i:s") . " || FREEENTRY || TRANSACTION TYPE: CONVERT POINTS || " . $login . " || UPDATE tbl_terminalsessions: " .$terminalID . ";".$siteID."; \r\n");
                                            fclose($fp);

                                        }
                                        //end add mtcc on 01/17/2013
                                    }
                                    //end add by mtcc 02/05/2013
                                }
                }
            }
        }
    }
//}
//else
//{
//    unset($_SESSION["vcode"]);
//    unset($_SESSION["changestatus"]);
//    unset($_SESSION["refreshmode"]);
////    App::Pr("<script> window.location = 'welcome.php'; </script>");
//}


if ($fproc->IsPostBack)
{
    if (isset($_SESSION['vcode']))
    {
        $checkstatus = $cafinovoucher->CheckStatus($_SESSION['vcode']);
        $status = $checkstatus[0][0];
        if ($status == "")
        {
            // URL::Redirect('welcome1.php');
            unset($_SESSION['bal']);
        }
    }
}
?>
