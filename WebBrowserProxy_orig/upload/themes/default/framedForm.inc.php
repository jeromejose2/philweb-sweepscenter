<?php
$user_agent = $_SERVER['HTTP_USER_AGENT'];
$div = "";
$div2 = "";
if (preg_match('/MSIE/i', $user_agent))
{//"Internet Explorer";
    $div = '<div id="light" class="white_content" style="width:50%;height: 35%;left: 25%;">';
    $div2 = '<div id="light3" class="white_content" style="width:40%;top: 35%;left: 30%;height: 30%;">';
}
else if (preg_match('/Epiphany/i', $user_agent))
{//"Epiphany";
    $div = '<div id="light" class="white_content" style="width:66%;height: 45%;">';
    $div2 = '<div id="light3" class="white_content" style="width:55%;top: 35%;left: 22%;height: 30%;">';
}
else
{//"Non-IE Browser";
    $div = '<div id="light" class="white_content" style="width:66%;height: 35%;left: 18%;">';
    $div2 = '<div id="light3" class="white_content" style="width:40%;top: 35%;left: 30%;height: 30%;">';
}
?>
<style type="text/css">
    /* Make room for the mini-form */
    html body { margin-top: 60px; }

    /* Reset all styles */
    #include * {
        text-align: left;
        border: 0; padding: 0; margin: 0;
        font: 12px Verdana,Arial,Tahoma;
        color: #eee;
        font-weight: normal;
        background: transparent;
        text-decoration: none;
        display: inline;
    }
    #include p {
        margin: 4px 0 0 10px;
        display: block;
    }
    #include b {
        font-weight: bold;
    }
    #include script {
        display:none;
    }

    /* Style the mini-form div */
    #include {
        border-top: 3px solid #ce6c1c;
        border-bottom: 3px solid #ce6c1c;
        background: #0b1933;
        position: absolute;
        top:0; left:0;
        width: 100%;
        height: 50px;
        z-index: 2000;
    }

    /* Mini-form elements */
    #include a {
        color: #ce6c1c;
    }
    #include a:hover {
        color: #ccc;
    }
    #include .url-input {
        padding: 2px;
        background: #eee;
        color: #111;
        border: 1px solid #ccc;
    }
    #include .url-input:focus {
        background: #fff;
        border: 1px solid #ce6c1c;
    }
    #include .url-button {
        font-weight: bold;
        border-style: outset;
        font-size: 11px;
        line-height: 10px;
    }
</style>
<link href="includes/lightbox.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="includes/jquery-1.5.2.min.js"></script>
<script language="javascript" type="text/javascript" src="includes/trans.js"></script>
<script>
    function continueInternetBrowsing()
    {
        document.getElementById('light').style.display = 'none';
        document.getElementById('light3').style.display = 'none';
        document.getElementById('light2').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        $.ajax({
            url: 'includes/proceedinternet.php',
            type: 'post',
            success: function(data)
            {
                document.getElementById('light2').style.display = 'none';
                document.getElementById('light3').style.display = 'block';
                if (data == 6)
                {
                    document.getElementById('browsingmessage').innerHTML = 'You have insufficient points in your balance. Please reload to enjoy continuous Internet browsing time.';
                    document.getElementById('light-button1').style.display = 'block';
                    document.getElementById('fade').style.display = 'block';
                }
                else if (data == 5)
                {
                    document.getElementById('browsingmessage').innerHTML = 'An Error Occured. Please Try Again.(Error proc_inserttotransactionlog)';
                    document.getElementById('light-button2').style.display = 'block';
                    document.getElementById('fade').style.display = 'block';
                }
                else if (data == 4)
                {
                    document.getElementById('browsingmessage').innerHTML = 'An Error Occured. Please Try Again.(Withdrawal API error)';
                    document.getElementById('light-button2').style.display = 'block';
                    document.getElementById('fade').style.display = 'block';
                }
                else if (data == 3)
                {
                    document.getElementById('browsingmessage').innerHTML = 'An Error Occured. Please Try Again.(Error proc_updatetransactionlog)';
                    document.getElementById('light-button2').style.display = 'block';
                    document.getElementById('fade').style.display = 'block';
                }
                else if (data == 2)
                {
                    document.getElementById('browsingmessage').innerHTML = 'An Error Occured. Please Try Again.(Update SeviceID=2 error)';
                    document.getElementById('light-button2').style.display = 'block';
                    document.getElementById('fade').style.display = 'block';
                }
                else
                {
                    document.getElementById('light3').style.display = 'none';
                    document.getElementById('fade').style.display = 'none';
                }

            },
            error: function(e)
            {
                alert("Error");
            }
        });
        $(document).ready(function() {

            function browserTester(browserString)
            {
                return navigator.userAgent.toLowerCase().indexOf(browserString) > -1;
            }

            if (browserTester('chrome')) {
                cssfunctionChrome();
            }
            else if (browserTester('safari')) {
                cssfunctionSafari();
            }
            else if (browserTester('firefox')) {
                cssfunctionFirefox();
            }

        });
        function cssfunctionSafari()
        {
            //alert('s');
            $(".light").css("width", "67%");
        }
        function cssfunctionFirefox()
        {
            //     alert('c');
        }
        function cssfunctionChrome()
        {//     alert('c');
        }
    }
</script>
<?php echo $div; ?>
    <div id="popUpDivLPBrowse" style="font-family:Helvetica; font-size: 20px;">
        <div align="center" style=" padding-top: 2%;border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;width: auto;"><b>
                INTERNET BROWSING CONFIRMATION</b>
        </div>
        <div id="popup_container_home" style="margin-left:30px; margin-top:15px; width:600px; height:60px; font-weight:bold;">
            <div id="convert" align="center"></div>
            <div id="convert_img" align="justify"></div>
            <br/>
            <div id="okbtn" align="center" style="margin-top: 10px; margin-left: 130px; float: left;">
                <input type="button" name="btnContinueBrowsing" id="btnContinueBrowsing" value="" style="background: url('images/ProceedButton.png');background-position:center;background-repeat:no-repeat;width:139px;height:38px;border:none;margin-top: 5px; margin-left: 30px; float: left;cursor:pointer;" onclick="javascript: return continueInternetBrowsing();"/>
            </div>
            <div style="margin-top: 7px;">
                <img src="images/cancelbutton.png" alt="" onclick="back_to_lp();" style="cursor:pointer;"/>
            </div>
        </div>
    </div>
</div>

<div id="light2" class="white_content" style="width:auto;top: 35%;left: 30%;">
    <div id="popUpDivLPBrowse" style="font-family:Helvetica; font-size: 20px;">
        <div id="popup_container_home" style="margin-left:30px; margin-top:15px; width:480px; height:60px; font-weight:bold;">
            <div id="convert" align="center"></div><div id="convert_img2" align="justify">
                <div align="center"><img src="images/dice.gif" alt="" height="120px" width="200px" style="margin-top: 30px;" /></div>
            </div>
        </div>
    </div>
</div>

<?php echo $div2; ?>
    <div id="popUpDivLPBrowse" style="font-family:Helvetica; font-size: 20px;">
        <div align="center" style=" padding-top: 2%;border-bottom-style: solid; border-color:#1FC4A9; background-color: #139E9E; color: white; height: 40px;"><b>INTERNET BROWSING MESSAGE</b></div>
        <div id="popup_container_home" style="margin-left:30px; margin-top:15px; width:480px; height:60px; font-weight:bold;">
            <div id="browsingmessage" align="center"></div><div id="convert_img2" align="justify">

            </div>
            <div id="light-button1" style="display:none;top: 50%;left: 40%;">
                <div id="okbtn" align="center" style="margin-top: 10px; margin-left: 80px; float: left;">
                    <input type="button" name="btnContinueBrowsing" id="btnContinueBrowsing" value="" style="background: url('images/ProceedButton.png');background-position:center;background-repeat:no-repeat;width:139px;height:38px;border:none;margin-top: -2px; margin-left: 30px; float: left;cursor:pointer;" onclick="javascript: return continueInternetBrowsing();"/>
                </div>
                <div style="margin-top: 7px;">
                    <img src="images/cancelbutton.png" alt="" onclick="back_to_lp();" style="cursor:pointer;"/>
                </div>
            </div>
            <div id="light-button2" style="display:none;top: 50%;left: 40%;">
                <div id="okbtn" align="center" style="margin-top: 10px; margin-left: 80px; float: left;">
                    <input type="button" name="btnContinueBrowsing" id="btnContinueBrowsing" value="" style="background: url('images/ProceedButton.png');background-position:center;background-repeat:no-repeat;width:139px;height:38px;border:none;margin-top: 5px; margin-left: 30px; float: left;cursor:pointer;" onclick="javascript: return continueInternetBrowsing();"/>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="fade" class="black_overlay" style="z-index:3000;height:1850px;"></div>

<div id="include">
    <?php
// Print form using variables (saves repeatedly opening/closing PHP tags)
// Edit as if normal HTML but escape any dollar signs

    echo <<<OUT
  	<form action="{$proxy}/includes/process.php?action=update" target="_top" method="post" onsubmit="return updateLocation(this);">

      <p>
         
         <b>URL:</b>
         <input type="text" name="u" size="40" value="{$url}" class="url-input" style="width:50%;" />
         <input type="submit" value="Go" class="url-input url-button" name="a"/>
         
                  
      </p>
<img src="images/backLaunchPad.png" title="Go Back to Launch Pad" style="height: 30px; margin-left: 80%; margin-top: -50px; cursor: pointer;" onclick="back_to_lp();" />

OUT;




// Loop through the options and print with appropriate checkedness
//foreach($toShow as $details) {
//   echo <<<OUT
//         <input type="checkbox" name="{$details['name']}" id="{$details['name']}"{$details['checked']} />
//         <label for="{$details['name']}">{$details['title']}</label>
//
//OUT;
//}
    ?>
</p>

</form>

</div>

<!--[proxified]-->
