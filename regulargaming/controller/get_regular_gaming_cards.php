<?php
/*
 * Created By:      Arlene R. Salazar
 * Created On:      June 14, 2012
 * 
 * Modified By:     Noel Antonio
 * Date Modified:   October 9, 2012 (1st)
 *                  November 23, 2012 (2nd)
 */

if (!isset($_SESSION))
{
    session_start();
}

$id = $_SESSION['id'];
$convertedpts = $_SESSION['equivalentpoints'];
$convertedbalance = $_SESSION['convertedbalance'];
$projectid = 2;
$remainingcardcnt2 = 0;

include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCCC_TransactionSummary");
App::LoadModuleClass("SweepsCenter", "SCCC_DeckInfo");
App::LoadModuleClass("SweepsCenter", "SCCC_TemporaryDeckTable");
App::LoadModuleClass("SweepsCenter", "SCCC_Winners");
App::LoadModuleClass("SweepsCenter", "SCCC_TempPrizeList");
App::LoadModuleClass("SweepsCenter", "SCCC_TempPrizesSummary");
App::LoadModuleClass("SweepsCenter", "SCCC_TransactionDetails");
App::LoadModuleClass("SweepsCenter", "SCCV_VoucherInfo");
App::LoadModuleClass("SweepsCenter", "SCCC_PointsConversionInfo");
App::LoadModuleClass("SweepsCenter", "SCC_FreeEntryRegistrants");
App::LoadModuleClass("SweepsCenter", "SCC_FreeEntry");
App::LoadModuleClass("SweepsCenter", "SCC_TerminalSessionDetails");
App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");
App::LoadModuleClass("SweepsCenter", "SCC_Terminals");

$cctranssumm = new SCCC_TransactionSummary();
$ccdeckinfo = new SCCC_DeckInfo();
$cctempdeck = new SCCC_TemporaryDeckTable();
$ccwinners = new SCCC_Winners();
$cctempprizelist = new SCCC_TempPrizeList();
$cctempprizesumm = new SCCC_TempPrizesSummary();
$cctransdetails = new SCCC_TransactionDetails();
$cvvoucherinfo = new SCCV_VoucherInfo();
$ccpointsconversion = new SCCC_PointsConversionInfo();
$cfreeentryregistrants = new SCC_FreeEntryRegistrants();
$cfreeentry = new SCC_FreeEntry();
$cterminalsessiondtls = new SCC_TerminalSessionDetails();
$cwinningsdump = new SCC_WinningsDump();
$cterminals = new SCC_Terminals();

if(isset ($_SESSION['sweeps_code']))
{
    $sweeps_code = $_SESSION['sweeps_code'];
}
else
{
    $sweeps_code = '';
}

if(isset ($_SESSION['IsFreeEntry']))
{
    $swps_cde = $_SESSION['sweeps_code'];
    $convertedpts = '1';
    $convertedbalance = 0.10;
    $option2 = 2;
}
else
{
    $swps_cde = "";
    $option2 = 1;
}

$cctranssumm->StartTransaction();
$arrTransactionSummary['RequestedBy'] = $id;
$arrTransactionSummary['TransactionDate'] = 'now_usec()';
$arrTransactionSummary['CardCount'] = $convertedpts;
$arrTransactionSummary['Status'] = 0;
$arrTransactionSummary['Option1'] = $id;
$cctranssumm->Insert($arrTransactionSummary);
if($cctranssumm->HasError)
{
    $cctranssumm->RollBackTransaction();
    echo "Error has occured: " . $cctranssumm->getError();
}
else
{
    $cctranssumm->CommitTransaction();

    $transsummaryid = $cctranssumm->LastInsertID;

    $deckdtls = $ccdeckinfo->SelectCurrentDeck($projectid, 1);
    $deckid = $deckdtls[0]['DeckID'];
    $decksize = $deckdtls[0]['CardCount'];
    $usedcardcount = ($deckdtls[0]['UsedCardCount'] != null) ? $deckdtls[0]['UsedCardCount'] : 0;
    $winningcardcount = ($deckdtls[0]['WinningCardCount'] != null) ? $deckdtls[0]['WinningCardCount'] : 0;
    $remainingcardcount = ($decksize - $usedcardcount);
    
    if($convertedpts >= $remainingcardcount)
    {
        $ccdeckinfo->StartTransaction();
        $ccdeckinfo->UpdateDeckNStatus($deckid,$id,$option2,$transsummaryid,floor($remainingcardcount));
        if($ccdeckinfo->HasError)
        {
            $ccdeckinfo->RollBackTransaction();
            echo "Error has occured: " . $ccdeckinfo->getError();
        }
        else
        {
            $ccdeckinfo->CommitTransaction();

            $cctempdeck->StartTransaction();
            $cctempdeck->InsertSelectedCards($deckid,$transsummaryid);
            if($cctempdeck->HasError)
            {
                $cctempdeck->RollBackTransaction();
                echo "Error has occured: " . $cctempdeck->getError();
            }
            else
            {
                $cctempdeck->CommitTransaction();

                $usedcardcnt = ($usedcardcount + $remainingcardcount);
                $ccdeckinfo->StartTransaction();
                $ccdeckinfo->DeactivateDeck(2,$usedcardcnt, $deckid);
                if($ccdeckinfo->HasError)
                {
                    $ccdeckinfo->RollBackTransaction();
                    echo "Error has occured: " . $ccdeckinfo->getError();
                }
                else
                {
                    $ccdeckinfo->CommitTransaction();

                    $deckdtls = $ccdeckinfo->SelectCurrentDeck($projectid, 0);
                    $deckid = $deckdtls[0]['DeckID'];
                    $decksize = $deckdtls[0]['CardCount'];
                    $usedcardcount = ($deckdtls[0]['UsedCardCount'] != null) ? $deckdtls[0]['UsedCardCount'] : 0;
                    $winningcardcount = ($deckdtls[0]['WinningCardCount'] != null) ? $deckdtls[0]['WinningCardCount'] : 0;
                    
                    $remainingcardcnt2 = ($convertedpts - $remainingcardcount);

                    $ccdeckinfo->StartTransaction();
                    $ccdeckinfo->ActivateQueuedDeck(1,$remainingcardcnt2,$deckid);
                    if($ccdeckinfo->HasError)
                    {
                        $ccdeckinfo->RollBackTransaction();
                        echo "Error has occured: " . $ccdeckinfo->getError();
                    }
                    else
                    {
                        $ccdeckinfo->CommitTransaction();

                        $ccdeckinfo->StartTransaction();
                        $ccdeckinfo->UpdateDeckNStatus($deckid,$id,$option2,$transsummaryid,$remainingcardcnt2);
                        if($ccdeckinfo->HasError)
                        {
                            $ccdeckinfo->RollBackTransaction();
                            echo "Error has occured: " . $ccdeckinfo->getError();
                        }
                        else
                        {
                            $ccdeckinfo->CommitTransaction();

                            $cctempdeck->StartTransaction();
                            $cctempdeck->InsertSelectedCards($deckid,$transsummaryid);
                            if($cctempdeck->HasError)
                            {
                                $cctempdeck->RollBackTransaction();
                                echo "Error has occured: " . $cctempdeck->getError();
                            }
                            else
                            {
                                $cctempdeck->CommitTransaction();
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        $remainingcardcount = $convertedpts;
        
        $ccdeckinfo->StartTransaction();
        $ccdeckinfo->UpdateDeckNStatus($deckid,$id,$option2,$transsummaryid,floor($remainingcardcount));
        if($ccdeckinfo->HasError)
        {
            $ccdeckinfo->RollBackTransaction();
            echo "Error has occured: " . $ccdeckinfo->getError();
        }
        else
        {
            $ccdeckinfo->CommitTransaction();
            
            $cctempdeck->StartTransaction();
            $cctempdeck->InsertSelectedCards($deckid,$transsummaryid);
            if($cctempdeck->HasError)
            {
                $cctempdeck->RollBackTransaction();
                echo "Error has occured: " . $cctempdeck->getError();
            }
            else
            {
                $cctempdeck->CommitTransaction();
            }
        }
    }
    
    $cctempdeck->StartTransaction();
    $cctempdeck->UpdatePrizeDescription($transsummaryid);
    if($cctempdeck->HasError)
    {
        $cctempdeck->RollBackTransaction();
        echo "Error has occured: " . $cctempdeck->getError();
    }
    else
    {
        $cctempdeck->CommitTransaction();
        
        $ccwinners->StartTransaction();
        $ccwinners->InsertDataFromTemporaryDeck($projectid,$deckid,$transsummaryid,$id);
        if($ccwinners->HasError)
        {
            $ccwinners->RollBackTransaction();
            echo "Error has occured: " . $ccwinners->getError();
        }
        else
        {
            $ccwinners->CommitTransaction();

            $totalcashwinningdtls = $cctempdeck->SelectTotalCashWinning($transsummaryid);
            $totalcashwinning = $totalcashwinningdtls[0]['TotalCash'];

            $winningscountdtls = $cctempdeck->SelectWinningCardCount($transsummaryid);
            $winningscount = $winningscountdtls[0]['cardcount'];

            if($winningscount > 0)
            {
                $cctempprizelist->StartTransaction();
                $cctempprizelist->InsertDetailsFromTemporaryDeck($transsummaryid);
                if($cctempprizelist->HasError)
                {
                    $cctempprizelist->RollBackTransaction();
                    echo "Error has occured: " . $cctempprizelist->getError();
                }
                else
                {
                    $cctempprizelist->CommitTransaction();
                }

                $cctempprizesumm->StartTransaction();
                $cctempprizesumm->InsertRecordsFromTemporaryDeck($transsummaryid);
                if($cctempprizesumm->HasError)
                {
                    $cctempprizesumm->RollBackTransaction();
                    echo "Error has occured: " . $cctempprizelist->getError();
                }
                else
                {
                    $cctempprizesumm->CommitTransaction();
                }
            }

            $cctransdetails->StartTransaction();
            $arrTransactionDetails['TransactionSummaryID'] = $transsummaryid;
            $arrTransactionDetails['DeckID'] = $deckid;
            $arrTransactionDetails['CardCount'] = $convertedpts;
            $arrTransactionDetails['TransactionDate'] = 'now_usec()';
            $cctransdetails->Insert($arrTransactionDetails);
            if($cctransdetails->HasError)
            {
                $cctransdetails->RollBackTransaction();
                echo "Error has occured: " . $cctransdetails->getError();
            }
            else
            {
                $cctransdetails->CommitTransaction();

                $cctranssumm->StartTransaction();
                $cctranssumm->UpdateStatus(1,$transsummaryid);
                if($cctranssumm->HasError)
                {
                    $cctranssumm->RollBackTransaction();
                }
                else
                {
                    $cctranssumm->CommitTransaction();

                    $usedcardcount = ($usedcardcount + $convertedpts);
                    $winningcardcount = ($winningcardcount + $winningscount);

                    $ccdeckinfo->StartTransaction();
                    if($remainingcardcnt2 > 0)
                    {
                        $ccdeckinfo->UpdateWinningCardCount($winningcardcount,'',$deckid);
                    }
                    else
                    {
                        $ccdeckinfo->UpdateWinningCardCount($winningcardcount,$usedcardcount,$deckid);
                    }
                    if($ccdeckinfo->HasError)
                    {
                        $ccdeckinfo->RollBackTransaction();
                        echo "Error has occured: " . $cctransdetails->getError();
                    }
                    else
                    {
                        $ccdeckinfo->CommitTransaction();

                        if($option2 == 1)
                        {
                            if($winningscount > 0)
                            {
                                $totalcashwinning = number_format($totalcashwinning,2);
                                $totalcash = "$" . $totalcashwinning . " Cash";

                                if($totalcashwinning > 0)
                                {
                                    $allPrizes = $totalcash;
                                }
                                else
                                {
                                    $allPrizes = "";
                                }

                                $prizecountdtls = $cctempprizesumm->SelectPrizeDescCount($transsummaryid);
                                $prizecount = $prizecountdtls[0]['prizedesccount'];

                                for($ctr3 = 1 ; $ctr3 <= $prizecount ; $ctr3++)
                                {
                                    $tempprizesummdtls = $cctempprizesumm->SelectTempPrizeSummaryDtls($transsummaryid);
                                    $vchr_Temp = $tempprizesummdtls[0]['PrizeDescription'];
                                    $int_TempCnt = $tempprizesummdtls[0]['PrizeCount'];
                                    $int_TempID = $tempprizesummdtls[0]['ID'];
                                    //$int_Denomination = $tempprizesummdtls[0]['PrizeDenomination'];
                                    $int_TempPrizeType = $tempprizesummdtls[0]['PrizeType'];

                                    $noncash = "(" . $int_TempCnt  . ") " . $vchr_Temp;
                                    $allPrizes .= "," . $noncash;

                                    $cctempprizesumm->StartTransaction();
                                    $cctempprizesumm->DeleteRecord($int_TempID);
                                    if($cctempprizesumm->HasError)
                                    {
                                        $cctempprizesumm->RollBackTransaction();
                                        echo "Error has occured: " . $cctransdetails->getError();
                                    }
                                    else
                                    {
                                        $cctempprizesumm->CommitTransaction();
                                    }
                                }
                                $respondmessage = "You've won the following prize/s: " . $allPrizes . ". Your Transaction Reference ID is " .  str_pad($transsummaryid,14,0,STR_PAD_LEFT) . ".  Please proceed to the cashier to claim your winnings.";
                            }
                            else
                            {
                                $respondmessage = "You did not get any winning card. Your Transaction Reference ID is " .  str_pad($transsummaryid,14,0,STR_PAD_LEFT) . ".  Thank you for playing.";
                            }
                        }
                        else
                        {
                            $tempdeckdtls = $cctempdeck->SelectTempDeckDetailsByTransSummID($transsummaryid);
                            $allPrizes = $tempdeckdtls[0]['PrizeDescription'];
                            $vchr_ECN = $tempdeckdtls[0]['ECN'];
                            $int_TempPrizeType = $tempdeckdtls[0]['PrizeType'];
                            //$int_TempPrizeValue = $tempdeckdtls[0]['PrizeValue'];

                            if($projectid == 2)
                            {
                                $vchr_ECN = str_pad($transsummaryid,14,0,STR_PAD_LEFT);
                            }

                            if($winningscount > 0)
                            {
                                $respondmessage = "You've won " .  $allPrizes . ". Your Transaction Reference ID is " . $vchr_ECN . ".<br />Please proceed to the cashier to claim your winnings.";
                            }
                            else
                            {
                                $respondmessage = "This is not a winning card. Your Transaction Reference ID is " . $vchr_ECN . ".<br /> Try our sweepstakes games and increase your chance of winning more and big prizes!<br />Thank you for playing.";
                            }
                        }

                        $cctranssumm->StartTransaction();
                        $cctranssumm->UpdateWinnings($winningcardcount,$allPrizes,$transsummaryid);
                        if($cctranssumm->HasError)
                        {
                            $cctranssumm->RollBackTransaction();
                            echo "Error has occured: " . $cctranssumm->getError();
                        }
                        else
                        {
                            $cctranssumm->CommitTransaction();

                            if($projectid == 2)
                            {
                                if($swps_cde != "")
                                {
                                    $denominationdtls = $cvvoucherinfo->SelectDenomination($swps_cde);
                                    $denomination = $denominationdtls[0]['Denomination'];

                                    $ccpointsconversion->StartTransaction();
                                    $arrPointsConversion['TransactionSummaryID'] = $transsummaryid;
                                    $arrPointsConversion['ProjectID'] = $projectid;
                                    $arrPointsConversion['TerminalID'] = $id;
                                    $arrPointsConversion['DateTimeTrans'] = 'now_usec()';
                                    $arrPointsConversion['EquivalentCards'] = $convertedpts;
                                    $arrPointsConversion['NoOfWinningCards'] = $winningscount;
                                    $arrPointsConversion['Winnings'] = $allPrizes;
                                    if($denomination == 1)
                                    {
                                        $arrPointsConversion['ConvertedPoints'] = '0.10';
                                    }
                                    else
                                    {
                                        $arrPointsConversion['ConvertedPoints'] = $convertedbalance;
                                    }
                                    $ccpointsconversion->Insert($arrPointsConversion);
                                    if($ccpointsconversion->HasError)
                                    {
                                        $ccpointsconversion->RollBackTransaction();
                                        echo "Error has occured: " . $ccpointsconversion->getError();
                                    }
                                    else
                                    {
                                        $ccpointsconversion->CommitTransaction();
                                    }
                                }
                                else
                                {
                                    $ccpointsconversion->StartTransaction();
                                    $arrPointsConversion['TransactionSummaryID'] = $transsummaryid;
                                    $arrPointsConversion['ProjectID'] = $projectid;
                                    $arrPointsConversion['TerminalID'] = $id;
                                    $arrPointsConversion['DateTimeTrans'] = 'now_usec()';
                                    $arrPointsConversion['EquivalentCards'] = $convertedpts;
                                    $arrPointsConversion['NoOfWinningCards'] = $winningscount;
                                    $arrPointsConversion['Winnings'] = $allPrizes;
                                    $arrPointsConversion['ConvertedPoints'] = $convertedbalance;
                                    $ccpointsconversion->Insert($arrPointsConversion);
                                    if($ccpointsconversion->HasError)
                                    {
                                        $ccpointsconversion->RollBackTransaction();
                                        echo "Error has occured: " . $ccpointsconversion->getError();
                                    }
                                    else
                                    {
                                        $ccpointsconversion->CommitTransaction();
                                    }
                                }

                                if($swps_cde != "")
                                {
                                    $cvvoucherinfo->StartTransaction();
                                    $cvvoucherinfo->UpdateVoucherInfoByVoucherNumAndStatus(2,$id,$allPrizes,$swps_cde,4);
                                    if($cvvoucherinfo->HasError)
                                    {
                                        $cvvoucherinfo->RollBackTransaction();
                                        echo "Error has occured: " . $ccpointsconversion->getError();
                                    }
                                    else
                                    {
                                        $cvvoucherinfo->CommitTransaction();
                                    }

                                    $freeentrydtls = $cfreeentry->SelectByVoucherCode($swps_cde);
                                    if(count($freeentrydtls) > 0)
                                    {
                                        $cfreeentry->StartTransaction();
                                        $cfreeentry->UpdateDatePlayed($swps_cde);
                                        if($cfreeentry->HasError)
                                        {
                                            $cfreeentry->RollBackTransaction();
                                            echo "Error has occured: " . $ccpointsconversion->getError();
                                        }
                                        else
                                        {
                                            $cfreeentry->CommitTransaction();
                                        }
                                    }
                                    else
                                    {
                                        $freeentryregistrants = $cfreeentryregistrants->SelectVoucherCode($swps_cde);
                                        if(count($freeentryregistrants) > 0)
                                        {
                                            $cfreeentryregistrants->StartTransaction();
                                            $cfreeentryregistrants->UpdateDatePlayedByVoucherCode($swps_cde);
                                            if($cfreeentryregistrants->HasError)
                                            {
                                                $cfreeentryregistrants->RollBackTransaction();
                                                echo "Error has occured: " . $cfreeentryregistrants->getError();
                                            }
                                            else
                                            {
                                                $cfreeentryregistrants->CommitTransaction();
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $tempdeckdata = $cctempdeck->SelectTempDeckDetailsByTransSummID($transsummaryid);
                        $cctempdeck->StartTransaction();
                        $cctempdeck->DeleteByTransSummID($transsummaryid);
                        if($cctempdeck->HasError)
                        {
                            $cctempdeck->RollBackTransaction();
                            echo "Error has occured: " . $cctempdeck->getError();
                        }
                        else
                        {
                            $cctempdeck->CommitTransaction();
                        }
                        
                        $cctempprizelist->StartTransaction();
                        $cctempprizelist->DeleteByTransSummaryID($transsummaryid);
                        if($cctempprizelist->HasError)
                        {
                            $cctempprizelist->RollBackTransaction();
                            echo "Error has occured: " . $cctempprizelist->getError();
                        }
                        else
                        {
                            $cctempprizelist->CommitTransaction();
                        }
                    }
                }
            }
        }
    }
}

$terminalsessionid = $_SESSION['tsi'];
$maxiddtls = $cterminalsessiondtls->SelectMaxId();
$lastTransId = $maxiddtls[0]['max'];
$lastTransId = ($lastTransId != null) ? $lastTransId : 0;
$lastTransId = ($lastTransId + 1);
$transid = 'W' . str_pad($terminalsessionid,6,0,STR_PAD_LEFT) . str_pad($lastTransId,10,0,STR_PAD_LEFT);

$cterminalsessiondtls->StartTransaction();
$arrTerminalSessionDetails['TerminalSessionID'] = $terminalsessionid;
$arrTerminalSessionDetails['TransactionType'] = 'W';
$arrTerminalSessionDetails['Amount'] = $convertedbalance;
$arrTerminalSessionDetails['AcctID'] = $_SESSION['id'];
$arrTerminalSessionDetails['ServiceID'] = 1;
$arrTerminalSessionDetails['TransID'] = $transid;
$arrTerminalSessionDetails['ServiceTransactionID'] = $transsummaryid;
$arrTerminalSessionDetails['TransDate'] ='now_usec()' ;
$arrTerminalSessionDetails['DateCreated'] = 'now_usec()';
$cterminalsessiondtls->Insert($arrTerminalSessionDetails);
if($cterminalsessiondtls->HasError)
{
    $cterminalsessiondtls->RollBackTransaction();
    echo "Error has occured: " . $cterminalsessiondtls->getError();
}
else
{
    $cterminalsessiondtls->CommitTransaction();
}

if(isset($_SESSION['IsFreeEntry']))
{
    $cvvoucherinfo->StartTransaction();
    $cvvoucherinfo->UpdateWinnings($wintype,$sweeps_code);
    if($cvvoucherinfo->HasError)
    {
        $cvvoucherinfo->RollBackTransaction();
        echo "Error updating voucher info.: " . $cvvoucherinfo->getError();
    }
    else
    {
        $cvvoucherinfo->CommitTransaction();
    }

    unset($_SESSION['IsFreeEntry']);
}

$cwinningsdump->StartTransaction();
$cwinningsdump->DeleteByTerminalID($id);
if($cwinningsdump->HasError)
{
    $cwinningsdump->RollBackTransaction();
    echo "Error has occured: " . $cwinningsdump->getError();
}
else
{
    $cwinningsdump->CommitTransaction();
}

$cwinningsdump->StartTransaction();
$bool = false;
for($i = 0 ; $i < count($tempdeckdata) ; $i++)
{
    if($tempdeckdata[$i]['PrizeDescription'] == "$0 Cash")
    {
        $prizedesc = "Try Again";
    }
    else
    {
        $prizedesc = $tempdeckdata[$i]['PrizeDescription'];
    }

    $arrWinningsDump['FK_TerminalID'] = $id;
    $arrWinningsDump['WinType'] = $prizedesc;
    $arrWinningsDump['ECN'] = $tempdeckdata[$i]['ECN'];
    $arrWinningsDump['IsOpened'] = 'N';
    $cwinningsdump->Insert($arrWinningsDump);
    if($cwinningsdump->HasError)
    {
        $bool = true;
    }
}

if ($bool)
{
    $cwinningsdump->RollBackTransaction();
    $error = "Error has occured: " . $cwinningsdump->getError();
}
else
{
    $cwinningsdump->CommitTransaction();
}

//added 01/03/2012 mtcc
$cterminals->StartTransaction();
$cterminals->UpdateIsPlayableStatus($terminalid,1);
if($cterminals->HasError)
{
    $cterminals->RollBackTransaction();
    $error_title = "ERROR";
    $error_msg = "Error updating terminal\'s Isplayable status.";
}
else
{
    $cterminals->CommitTransaction();
}
//added 01/03/2012 mtcc

$_SESSION['resultmsg'] = $respondmessage;

if(!isset($error))
{
    echo "<script>window.location = '../views/displaycards.php';</script>";
}
?>