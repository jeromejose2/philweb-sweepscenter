<?php
/*
 * Created By: Arlene R. Salazar
 * Created On: June 20, 2012
 */
include('../init.inc.php');

App::LoadModuleClass("SweepsCenter", "SCC_WinningsDump");

$cwinningsdump = new SCC_WinningsDump();
$id = $_SESSION['id'];

$cwinningsdump->StartTransaction();
$cwinningsdump->DeleteByTerminalID($id);
if($cwinningsdump->HasError)
{
    $cwinningsdump->RollBackTransaction();
}
else
{
    $cwinningsdump->CommitTransaction();
}

header("location: launchpad.php");
?>