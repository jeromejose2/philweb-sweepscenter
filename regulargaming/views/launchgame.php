<?php
/*
 * Added By: Noel Antonio
 * Date Added: October 18, 2012
 * Purpose: Added in SWC v.3 to launch casino games using flash game wrapper 
 *          with flash sizes/dimension that corresponds on browser.
 */

$gid = $_GET["gid"];
$mid = $_GET["mid"];
$user = $_GET["user"];
$pass = $_GET["pass"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>MM_FlashGame_Wrapper Sample Usage</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1.0, height=device-height, width=device-width, user-scalable=no" />
<style type="text/css" media="all">

body {
	background-color: #000;
	margin: 0;
	padding: 0;
}

#container {
	height: 100%;
	width: 100%;
       text-align: center;
}

</style>
<script charset="utf-8" type="text/javascript" src="jscripts/jquery-1.6.2.min.js"></script>
<script charset="utf-8" type="text/javascript" src="jscripts/jquery.swfobject.1-1-1.min.js"></script>
<script charset="utf-8" type="text/javascript" src="jscripts/MM_FlashGame_Wrapper.js"></script>
<script charset="utf-8" type="text/javascript" src="jscripts/browser.js"></script>
<script charset="utf-8" type="text/javascript">

$(document).ready(function() {

if (BrowserDetect.browser == 'Firefox' || BrowserDetect.browser == 'Safari' || BrowserDetect.browser == 'Chrome')
{
	var flashOptions = {
                height: $(window).height() - 20,
 		width: $(window).width() - 150,
		swf: 'media/mini_flash_client.swf',
		base: 'media'
                //swf: 'http://freeplay.pagcoregames.com/media/mini_flash_client.swf',
		//base: 'http://freeplay.pagcoregames.com/media/'
	};
}
else
{
	var flashOptions = {
		swf: 'media/mini_flash_client.swf',
		base: 'media'
                //swf: 'http://freeplay.pagcoregames.com/media/mini_flash_client.swf',
		//base: 'http://freeplay.pagcoregames.com/media/'
	};
}
	var flashVars = {
                user: '<?php echo $user ?>',
		sPassword: '<?php echo $pass ?>',
		IP: '202.44.102.31',
                portBase: 500,
		casinoName: 'ECF Demo',
		gameid: '<?php echo $gid ?>',
		machid: '<?php echo $mid ?>'
	};

	$('#container').MM_FlashGame_Wrapper(flashOptions, flashVars);
});

</script>
</head>
<body lang="en">
<div id="container"></div>
</body>
</html>
