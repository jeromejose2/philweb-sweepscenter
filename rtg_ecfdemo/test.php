<?php

/* * ***************** 
 * Author: Roger Sanchez
 * Date Created: 2012-09-23
 * Company: Philweb
 * ***************** */
require_once("include/core/init.inc.php");

require_once("RealtimeGamingCashierAPI.class.php");
require_once("RealtimeGamingAPIWrapper.class.php");
require_once("RealtimeGamingPlayerAPI.class.php");
require_once("RealtimeGamingRemoteAuthAPI.class.php");

App::LoadLibrary("nusoap/nusoap.php");


//$apiurl = "https://202.44.102.31/ECFTESTFDGUFTUGEHVHF/processor/processorapi/cashier.asmx";
//$rtg_url_auth = "https://202.44.102.31/ECFTESTFDGUFTUGEHVHF/CasinoAPI/remoteAuth.asmx";
//$playerurl = 'https://202.44.102.31/ECFTESTFDGUFTUGEHVHF/CasinoAPI/player.asmx';

$apiurl = "https://202.44.102.31/ECFDEMOFDGEFNPGEMFOQ/processor/processorapi/cashier.asmx";
$rtg_url_auth = "https://202.44.102.31/ECFDEMOFDGEFNPGEMFOQ/CasinoAPI/remoteAuth.asmx";
$playerurl = "https://202.44.102.31/ECFDEMOFDGEFNPGEMFOQ/casinoapi/player.asmx";


$certFilePath = dirname(__FILE__) . "/ecfdemopem/cert.pem";
$keyFilePath = dirname(__FILE__) . "/ecfdemopem/key.pem";

$login = "SWCTRE1"; //"ICSA-TSTID06";//"SWCRTY1";// //"SWCABC1";//"ICSA-TST02";//"SWCTRE11-01";
$amount = 5;
//$amount = 564.50;
//$amount = 805;


$rtgwrapper = new RealtimeGamingAPIWrapper($apiurl, RealtimeGamingAPIWrapper::CASHIER_API, $certFilePath, $keyFilePath);
$rtgwrapperplayer = new RealtimeGamingAPIWrapper($playerurl, RealtimeGamingAPIWrapper::PLAYER_API, $certFilePath, $keyFilePath);
$rtgwrapperremoteAuth = new RealtimeGamingAPIWrapper($rtg_url_auth, RealtimeGamingAPIWrapper::REMOTEAUTH_API, $certFilePath, $keyFilePath);
App::Pr($rtgwrapper);


//--------------------------------Get Balance
$rtgbalance = $rtgwrapper->GetBalance($login);
echo '<br>balance:';
App::Pr($rtgbalance);


//--------------------------------get account info
$accountinfo = $rtgwrapper->GetAccountInfoByLogin($login);
App::Pr($accountinfo);


//--------------------------------Deposit
$rtgdeposit = $rtgwrapper->Deposit($login, $amount, $tracking1 = '', $tracking2 = '', $tracking3 = '', $tracking4 = '');
echo '<br>Deposit:';
App::Pr($rtgdeposit);

//--------------------------------Get Balance
$rtgbalance = $rtgwrapper->GetBalance($login);
echo '<br>balance:';
App::Pr($rtgbalance);


//--------------------------------Withdraw
$rtgwithdraw = $rtgwrapper->Withdraw($login, 1, $tracking1 = '', $tracking2 = '', $tracking3 = '', $tracking4 = '');
echo '<br>Withdraw:';
App::Pr($rtgwithdraw);




//--------------------------------Get Balance
$rtgbalance = $rtgwrapper->GetBalance($login);
echo '<br>balance:';
App::Pr($rtgbalance);




//--------------------------------Create Terminal

$terminal = "SWCRTG01";
$login = $terminal;
$pw = '1234567';
$aid = 0;
$country = 'PH';
$casinoID = 1;
$fname = 'SWC';
$lname = 'SWC' . $login;
$email = 'no@mail.com';
$dayphone = '3385599';
$evephone = '3385599';
$addr1 = 'Makati';
$addr2 = '';
$city = 'Makati';
$state = '';
$zip = '';
$ip = $_SERVER['REMOTE_ADDR'];
$mac = '';
$userID = 0;
$downloadID = 0;
$birthdate = '1981-01-01';
$clientID = 1;
$putInAffPID = 0;
$calledFromCasino = 0;
$hashedPassword = sha1('1234567');
$agentID = '';
$currentPosition = 0;
$thirdPartyPID = '';

$newplayer = $rtgwrapperplayer->AddUser($login, $pw, $aid, $country, $casinoID, $fname, $lname, $email, $dayphone, $evephone, $addr1, $addr2, $city, $state, $zip, $ip, $mac, $userID, $downloadID, $birthdate, $clientID, $putInAffPID, $calledFromCasino, $hashedPassword, $agentID, $currentPosition, $thirdPartyPID);

echo '<br>Add Terminal:';App::Pr($newplayer);
?>
