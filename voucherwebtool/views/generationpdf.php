<?php
/*
 * @author
 * Purpose   : view for generationpdf
 */
/*register_shutdown_function('foo');

function foo() {
  flush();
  $c = "headers_list: \n  " . join("\n  ", headers_list());

  if ( function_exists('apache_response_headers') ) {
    $c .= "\napache_response_headers:";
    foreach( apache_response_headers() as $k=>$v) {
      $c.= "\n  $k=$v";
    }
  }
  $c .= "\n\n";
  echo '<pre>', $c, '</pre>';
}
if (headers_sent())
{
    var_dump(headers_list());
}*/
    $modulename = "SweepsCenter";
    App::LoadModuleClass($modulename, "SCCV_VoucherInfo");
    App::LoadModuleClass($modulename, "Pdf");
    $svvoucherinfo = new SCCV_VoucherInfo();

    $max_batchid = $_GET["batchid"];
    $file = $_GET["filename"];
    
    $pdfdata = $svvoucherinfo->SelectByBatchNo($max_batchid);
    $tbldata_list = new ArrayList();
    $tbldata_list->AddArray($pdfdata);

    $pdf = new PDF("P","mm","Legal");
    $pdf->AliasNbPages();
    $title = 'Voucher Information';
    $pdf->SetTitle($title);
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(0,5,"Voucher Information",0,0,'C');
    $pdf->Ln(4);
    $lengths = array(35, 35, 35, 35, 50);

    $header = array('Batch ID', 'Voucher Number', 'Value', 'Validity', 'Date Generated');
    $cols = array('BatchID', 'VoucherNo', 'Denomination', 'Validity', 'DateGenerated');

    $pdf->SetFont('Arial','',9);   
    $pdf->AddPage();
    $pdf->FancyTable($header,$tbldata_list,$lengths,$cols,$title);
    $pdf->Output($file.'.pdf','D');
    header('Content-Type: application/pdf');
?>