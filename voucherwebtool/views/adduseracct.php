<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 25, 2012
 * ADD USER ACCOUNT VIEW PAGE
 * Purpose: For adding new user account.
 */
include("../controller/adduseracctprocess.php");
?>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<div id="fade" class="black_overlay"></div>
<div id="light1" class="white_content">
<div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
</div><br />
<div id="msg"></div>
</div>

<div id="page">
  <table>
    <tr>
      <td colspan="2">
        <div id="header">Create User Account</div>
      </td>
    </tr>
    <tr style="background-color:#FFF1E6; height:30px;">
      <td class="fontboldblack" style="width:400px;">&nbsp;&nbsp;Username</td>
      <td style="width:400px;"><?php echo $txtUName; ?></td>
    </tr>
<!--    <tr style="background-color:#FFF1E6; height:30px;">
      <td class="fontboldblack">&nbsp;&nbsp;Password</td>
      <td><?php echo $txtPWord; ?></td>
    </tr>-->
<!--    <tr style="background-color:#FFF1E6; height:30px;">
      <td class="fontboldblack">&nbsp;&nbsp;Confirm Password:</td>
      <td><?php echo $txtCPWord; ?></td>
    </tr>-->
  </table>
  <br />
  <div id="header2">Account Information</div>
  <div style="text-align:center;">
    <table>
      <tr>
        <td class="fontboldblackcenter">First Name</td>
        <td class="fontboldblackcenter">Middle Name</td>
        <td class="fontboldblackcenter">Last Name</td>
      </tr>
      <tr>
        <td><?php echo $txtFName; ?></td>
        <td><?php echo $txtMName; ?></td>
        <td><?php echo $txtLName; ?></td>
      </tr>
      <tr>
        <td class="fontboldblackcenter">E-mail Address</td>
        <td colspan="2"><?php echo $txtEmail; ?></td>
      </tr>
      <tr>
        <td class="fontboldblackcenter">Position</td>
        <td colspan="2"><?php echo $txtPosition; ?></td>
      </tr>
      <tr>
        <td class="fontboldblackcenter">Company</td>
        <td colspan="2"><?php echo $txtCompany; ?></td>
      </tr>
      <tr>
        <td class="fontboldblackcenter">Department</td>
        <td colspan="2"><?php echo $txtDept; ?></td>
      </tr>
      <tr>
        <td class="fontboldblackcenter">Group</td>
        <td colspan="2"><?php echo $ddlGroup; ?>
          <input type="hidden" id="hidGroup" name="hidGroup" value =""></input>
        </td>
      </tr>
      <tr>
        <td class="fontboldred"  colspan="3">All fields are required.<br/><br/></td>
      </tr>      
      <tr><td colspan="3" align="center"><?php echo $btnSubmit; ?></td></tr>
    </table>
  </div>
</div>
<script language="Javascript" type="text/javascript">
<?php if (isset($errormsg)):?>
        document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
        document.getElementById('msg').innerHTML = "<?php echo $errormsg; ?>";
        document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
<?php endif; ?>
    
<?php if (isset($confirmmsg)): ?>
        document.getElementById('title').innerHTML = "Confirmation";
        document.getElementById('msg').innerHTML = "<?php echo $confirmmsg; ?>";
        document.getElementById('msg').innerHTML += "<br/><br/><br/>"+"<?php echo $btnConfirm; ?>"+"&nbsp;&nbsp; <input id=\"btnCancel\" type=\"button\" value=\"NO\" class=\"labelbutton2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
<?php endif; ?>
    <?php if (isset($errormsg1)):?>
        document.getElementById('title').innerHTML = "<?php echo $errormsgtitle1; ?>";
        document.getElementById('msg').innerHTML = "<?php echo $errormsg1; ?>";
        document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" onclick=\"window.location = 'template.php?page=adduseracct'; \" />";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
<?php endif; ?>
    
</script>
