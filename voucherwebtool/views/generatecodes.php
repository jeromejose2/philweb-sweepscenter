<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 21, 2012
 * GENERATE CODE VIEW PAGE
 * Purpose: Landing page. View for generating voucher codes.
 */
include("../controller/generatecodesprocess.php");
?>
  <script language="javascript" src="jscripts/checkinputs.js"></script>

<form name="generatecode" method="POST" action="generatecodes.php">
    
<script type="text/javascript">
    function submitform()
    {
        var batchid = document.getElementById('hiddenbatchid').value;
        var filename = document.getElementById('hiddenfile').value;
        document.getElementById('light1').style.display = 'none';
        document.getElementById('fade').style.display = 'none';
        window.location.href = "template.php?page=rptgenerationhist&generatepdf=1&batchid=" + batchid + "&filename=" + filename;      
    }
</script>
<div id="page">
  <table width="100%">
    <tr>
      <td colspan="2">
        <div id="header">Generate Voucher Codes</div>
      </td>
    </tr>
    <tr style="height:30px;">
      <td class="fontboldblack" align="right">Denomination:&nbsp;&nbsp;&nbsp;</td>
      <td><?php echo $ddlDenomination; ?></td>
    </tr>
    <tr style="height:30px;">
      <td class="fontboldblack" align="right">Quantity:&nbsp;&nbsp;&nbsp;</td>
      <td><?php echo $txtQty; ?></td>
    </tr>
    <tr style="height:30px;">
      <td class="fontboldblack" align="right">Validity Period:&nbsp;&nbsp;&nbsp;</td>
      <td><?php echo $ddlValidity; ?></td>
    </tr>
    <tr style="height:30px;">
      <td colspan="2" align="center"><?php echo $btnGenerate; ?></td>
    </tr>
  </table>
    <?php echo $hiddenbatchid; ?>
    <?php echo $hiddenfile; ?>
</div>

<div id="fade" class="black_overlay"></div>
<div id="light1" class="white_content">
<div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
</div><br />
<div id="msg"></div>
</div>

<div id="light11" class="white_content">
<div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
</div><br />
<div id="msg"></div>
</div>

 <?php if (isset($success)): ?>
    <script>
        document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
        document.getElementById('msg').innerHTML = "<?php echo $errormsg; ?>";
        document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" onclick=\"javascript: return submitform();\" /> &nbsp&nbsp";
        document.getElementById('msg').innerHTML += "<a href=\"template.php?page=rptgenerationhist\" id=\"btnClose\" class=\"labelbutton2\">CLOSE</a>";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
    </script>
  <?php endif; ?>    

</form>

<script type="text/javascript">
  $(document).ready(function(){
     $('#btnSubmit').click(function(){	
        if($('#ddlDenomination').val() == 0) {
            document.getElementById('title').innerHTML = "NOTIFICATION";
            document.getElementById('msg').innerHTML = "Please select a denomination to be generated.";
            document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }        
        if($('#txtQty').val() == "") {
            document.getElementById('title').innerHTML = "NOTIFICATION";
            document.getElementById('msg').innerHTML = "Please specify quantity to be generated.";
            document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
        if($('#txtQty').val() < 50 || $('#txtQty').val() > 5000) {
            document.getElementById('title').innerHTML = "NOTIFICATION";
            document.getElementById('msg').innerHTML = "Quantity of voucher codes to be generated must be between 50 to 5000.";
            document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }

        document.getElementById('title').innerHTML = "CONFIRMATION";
        document.getElementById('msg').innerHTML = "You are about to generate " + $('#txtQty').val() + " number of voucher codes with " + $('#ddlDenomination option:selected').text() + " denomination. Do you wish to continue?";
        document.getElementById('msg').innerHTML += "<br/><br/><br/>" + "<?php echo $btnConfirm; ?> &nbsp&nbsp";
        document.getElementById('msg').innerHTML += "<input id=\"btnCancel\" type=\"button\" value=\"CANCEL\" class=\"labelbutton2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
     });
     
    
         
  });
  </script>
</div>
