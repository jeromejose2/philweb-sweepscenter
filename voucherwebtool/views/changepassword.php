<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 24, 2012
 * CHANGE PASSWORD VIEW PAGE
 * Purpose: View page for changing user password.
 */
include("../controller/changepasswordprocess.php");
?>
<div id="page">
    <div id="light1" class="white_content">
        <div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
        </div><br />
        <div id="msg"></div>
    </div>
    <script type="text/javascript">
<?php if ( isset($errormsg) ): ?>

                  document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
                  document.getElementById('msg').innerHTML = "<?php echo $errormsg; ?>";
                  document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
                  document.getElementById('light1').style.display = 'block';
                  document.getElementById('fade').style.display = 'block';
<?php endif; ?>

    

    
<?php if ( isset($confirmmsg) ): ?>
            document.getElementById('title').innerHTML = "Confirmation";
            document.getElementById('msg').innerHTML = "<?php echo $confirmmsg; ?>";
            document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" onclick=\"submitform();\" />&nbsp;&nbsp; <input id=\"btnCancel\" type=\"button\" value=\"CANCEL\" class=\"labelbutton2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
<?php endif; ?>

<?php if ( isset($checkpasswordinput) ) : ?>

                   html = "Please change the default password before proceeding.<br/>";
           

                   document.getElementById('title').innerHTML = "NOTIFICATION";
                   document.getElementById('msg').innerHTML = html;
                   document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk1\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
                   document.getElementById('light1').style.display = 'block';
                   document.getElementById('fade').style.display = 'block';
            
<?php endif; ?>
        function checkpasswordinput()
        {
            var pword = document.getElementById('txtPassword').value;
            var npword = document.getElementById('txtNewPassword').value;
            var cnpword = document.getElementById('txtConfPassword').value;
            var hasError = 0;

            if(pword.trim() == "")
            {
                html = "Please enter your current password to continue.<br/>";
                hasError = 1;
            }
            else if(pword.length < 8)
            {
                html = "Password should be more than 7 characters.<br/>";
                hasError = 1;
            }
            else if(npword.trim() == "")
            {
                html = "Please enter a new password to continue.<br/>";
                hasError = 1;
            }
            else if(npword.length < 8)
            {
                html = "Password should be more than 7 characters.<br/>";
                hasError = 1;
            }
            else if(cnpword == "")
            {
                html = "Please confirm password.<br/>";
                hasError = 1;
            }
            else if(cnpword.length < 8)
            {
                html = "Password should be more than 7 characters.<br/>";
                hasError = 1;
            }
            else if(npword != cnpword)
            {
                html = "The new passwords you entered do not match. Please try again.<br/>";
                hasError = 1;
            }

            if(hasError == 1) {
                document.getElementById('title').innerHTML = "NOTIFICATION";
                document.getElementById('msg').innerHTML = html;
                document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
                document.getElementById('light1').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
                return false;
            }
            else 
            {
                return true;
            }
        }
    </script>

    <table>
        <tr>
            <td colspan="2"><div id="header">Fill out this form to change your password.</div>
            </td>
        </tr>
        <tr style="background-color:#FFF1E6; height:30px;">
            <td class="fontboldblack" style="width:400px;">&nbsp;&nbsp;Current Password:</td>
            <td style="width:400px;">&nbsp;&nbsp;<?php echo $txtPassword; ?>
            </td>
        </tr>
        <tr style="background-color:#FFF1E6; height:30px;">
            <td class="fontboldblack">&nbsp;&nbsp;New Password:</td>
            <td>&nbsp;&nbsp;<?php echo $txtNewPassword; ?>
            </td>
        </tr>
        <tr style="background-color:#FFF1E6; height:30px;">
            <td class="fontboldblack">&nbsp;&nbsp;Confirm New Password:</td>
            <td>&nbsp;&nbsp;<?php echo $txtConfPassword; ?></td>
        </tr>
        <tr><td colspan="2" align="center"><br/><?php echo $btnSubmit; ?><?php echo $hidden; ?></td></tr>
    </table>
    <script type="text/javascript">
        function submitform()
        {
            document.getElementById('hidden').value = "<?php echo $hash_newpass; ?>";
            document.forms[0].submit();
        }
    </script>
</div>