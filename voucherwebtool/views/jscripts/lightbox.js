
function showLightBox(html)
{
   var e = document.getElementById('frmTemplate');
   var conheight = e.offsetHeight;
   document.getElementById('fade').style.height = conheight;
   $('#frmTemplate').append('<div id="light" class="white_content"></div>');
   $('.white_content').html(html);

   $('#fade').show();
   $('.white_content').show();
}

function hideLightBox()
{
   document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';
}

function hideLightBox2()
{
   $('#light').fadeOut('slow',function(){
      $('#light').remove();
   });
   $('#fade').fadeOut('slow');
}

$('#btnOkay').live('click',function(){
   $('#light').fadeOut('slow',function(){
      $('#light').remove();
   });
   $('#fade').fadeOut('slow');
   return false;
});

function trim(str) {
   return jQuery.trim(str);
}
