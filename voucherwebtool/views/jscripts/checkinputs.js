function checkunamepword()
{
    var uname = document.getElementById('txtUsername').value;
    var pword = document.getElementById('txtPassword').value;
    var trimmed_uname = uname.trim();
    if(trimmed_uname == "")
    {
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            document.getElementById('title').innerHTML = "Error!";
            document.getElementById('msg').innerHTML = "Please enter your username.";
            return false;
    }
    if(pword == "")
    {
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            document.getElementById('title').innerHTML = "Error!";
            document.getElementById('msg').innerHTML = "Please enter your password.";
            return false;
    }
    return true;
}

function openforgotpw()
{
    document.getElementById('title2').innerHTML= 'Reset Password';
    document.getElementById('msg2').innerHTML = 'You are about to reset your current password. Please enter your registered e-mail address.';
    document.getElementById('light2').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    return false;
}

function checkinputemail()
{
    var email = document.getElementById('txtEmail');
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (email.value.length == 0)
    {
        document.getElementById('title2').innerHTML = "Reset Password";
        document.getElementById('msg2').innerHTML = "Please enter e-mail address.";
        document.getElementById('txtEmail').value = "";
        document.getElementById('txtEmail').focus();
        document.getElementById('light2').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }
    else if (!filter.test(email.value)) 
    {
        document.getElementById('title2').innerHTML = "Reset Password";
        document.getElementById('msg2').innerHTML = "You have entered a non-existing e-mail address. Please try again.";
        document.getElementById('txtEmail').value = "";
        document.getElementById('txtEmail').focus();
        document.getElementById('light2').style.display='block';
        document.getElementById('fade').style.display='block';
        return false;
    }
    return true;
}

function specialkeys(e)
{
    var k;
    document.all ? k = e.keyCode : k = e.which;
    return ((k >= 64 && k < 91) || (k > 96 && k < 123) || k == 8 || e.keyCode == 9 || k == 32 || k == 46 || (e.keyCode > 36 && e.keyCode <= 40) || (k >= 48 && k <= 57));
}

/*Added by JFJ 10-04-2012*/
function disableSpace(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 32)
        return false;

    return true;
}

function DisableSpecialCharacters(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if((charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;

    return true;
}
function DisableSpaceandSpCharacters(evt)
{
      var charCode = (evt.which) ? evt.which : event.keyCode;
    if(charCode == 32 || (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;

    return true;
}
/*Added by JFJ 10-04-2012*/

function alphanumeric(e)
{
    var k;
    document.all ? k = e.keyCode : k = e.which;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || e.keyCode == 9 || k == 32 || (e.keyCode > 36 && e.keyCode <= 40) || (k >= 48 && k <= 57));
}

function isnumberkey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function isNumericAndPeriod(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 46 || (charCode < 58 && charCode > 47) || charCode == 8)
        return true;
    else
        return false;
}

function isSpecialKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 32 || (charCode <= 47 && charCode >= 33) || (charCode <= 64 && charCode >= 58) || (charCode <= 126 && charCode >= 123) || (charCode <= 96 && charCode >= 91))
        return false;
    else
        return true;
}

function numeric(evt)
{
    keyHit = evt.which;
    NumericCode = "61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122";
    if (NumericCode.indexOf(keyHit)<0)

    {
            return false;
    }
    return true;
}

function checkadduseracct()
{
 
    var sFName = document.getElementById("txtFName");
    var sMName = document.getElementById("txtMName");
    var sLName = document.getElementById("txtLName");  
    var sEmail =  document.getElementById("txtEmail");
    var sPosition = document.getElementById("txtPosition");
    var sCompany =  document.getElementById("txtCompany");
    var sDepartment = document.getElementById("txtDepartment");
    var sGroup =  document.getElementById("ddlGroup").options[document.getElementById("ddlGroup").selectedIndex].value;
    var sUName = document.getElementById("txtUName");
//    var sPWord = document.getElementById("txtPWord");
//    var sCPWord = document.getElementById("txtCPWord");
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if(sUName.value.length == 0)
    {
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
    document.getElementById('msg').innerHTML = "Please enter your Username.";
    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
    sUName.focus();
    return false;	
    }	
    if(sUName.value.length < 8)
    {
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
    document.getElementById('msg').innerHTML = "Username should be more than 7 characters.";
    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
    sUName.focus();
    return false;	
    }	
//    if(sPWord.value.length == 0)
//    {
//    document.getElementById('light1').style.display='block';
//    document.getElementById('fade').style.display='block';
//    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
//    document.getElementById('msg').innerHTML = "Please enter your Password.";
//    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
//    sPWord.focus();
//    return false;
//    }
//    if(sPWord.value.length < 6)
//    {
//    document.getElementById('light1').style.display='block';
//    document.getElementById('fade').style.display='block';
//    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
//    document.getElementById('msg').innerHTML = "Invalid Password.";
//    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
//    sPWord.focus();
//    return false;
//    }
//    if(sCPWord.value.length == 0)
//    {
//    document.getElementById('light1').style.display='block';
//    document.getElementById('fade').style.display='block';
//    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
//    document.getElementById('msg').innerHTML = "Please enter your Confirm Password.";
//    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
//    sCPWord.focus();
//    return false;
//    }
//    if(sPWord.value != sCPWord.value)
//    {
//    document.getElementById('light1').style.display='block';
//    document.getElementById('fade').style.display='block';
//    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
//    document.getElementById('msg').innerHTML = "The passwords you have entered do not match. Please try again.";
//    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
//    sCPWord.focus();
//    return false;
//    }
    if (sFName.value.length == 0)
    {
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
    document.getElementById('msg').innerHTML = "Please enter your First Name.";
    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
    sFName.focus();
    return false;
    }
    if (sMName.value.length == 0)
    {
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
    document.getElementById('msg').innerHTML = "Please enter your Middle Name.";
    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
    sMName.focus();
    return false;
    }
    if (sLName.value.length == 0)
    {
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
    document.getElementById('msg').innerHTML = "Please enter your Last Name.";
    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
    sLName.focus();
    return false;
    }
    if (sEmail.value.length == 0)
    {
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
    document.getElementById('msg').innerHTML = "Please enter your Email Address.";
    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
    sLName.focus();
    return false;
    }
    if (!filter.test(sEmail.value)) 
    {
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
    document.getElementById('msg').innerHTML = "Please supply valid email address.";
    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
    sLName.focus();
    return false;
    }
    if (sPosition.value.length == 0)
    {
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
    document.getElementById('msg').innerHTML = "Please enter your Position.";
    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
    sPosition.focus();
    return false;
    }
    if (sCompany.value.length == 0)
    {
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
    document.getElementById('msg').innerHTML = "Please enter your Company.";
    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
    sCompany.focus();
    return false;
    }
    if (sDepartment.value.length == 0)
    {
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
    document.getElementById('msg').innerHTML = "Please enter your Department.";
    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
    sDepartment.focus();
    return false;
    }
    if (sGroup == 0)
    {
    document.getElementById('light1').style.display='block';
    document.getElementById('fade').style.display='block';
    document.getElementById('title').innerHTML = "INCOMPLETE DATA";
    document.getElementById('msg').innerHTML = "Please enter your Group.";
    document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOk\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
    document.getElementById("ddlGroup").focus();
    return false;
    }    

    return true;
}
function AlphaOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;  
    if(/*DisableWhiteSpaces*/(charCode == 32) ||/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))      
    return false;
        var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
        // returns false if a numeric character has been entered
        return (chCode < 48 /* '0' */ || chCode > 57 /* '9' */ );
        return (chCode==32);

    return true;
}
function verifyEmail(event){
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode == 64)||(charCode == 95)||(charCode == 46))/* @=64, _=95, .=46 characters are allowed*/
  {  
    return true;
    return true;
  }else{
  if(/*DisableWhiteSpaces*/(charCode == 32) ||/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128)) 
    return false;
    return true;
  }
}
