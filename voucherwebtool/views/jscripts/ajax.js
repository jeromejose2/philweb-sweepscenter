function callAjax(url,data,type) {
   showLoginLightbox2('<h1>Loading...</h1>');
   $.ajax({
      url     : url,
      data    : data,
      type    : type,
      success : function(html){
         $('#light').html(html);
      },
      error   : function(){
         html = '<h1>Oops! Something went wrong</h1>';
         html += '<input id="btnOkay" type="submit" value="OKAY" class="labelbutton2" ></input>';
         $('#light').html(html);
      }
   });
}

$('#forgotPassword').live('click',function(){
   var data = $('#frmLogin').serialize();
   $('.white_content').remove();
   callAjax('popupforgotpass.php',data,'post');
   return false;
});

function showLoginLightbox(html) {
   var e = $('#frmTemplate');
   var conheight = e.offsetHeight;
   $('#fade').height(conheight);
   $('#frmTemplate').append('<div id="light" class="white_content"></div>');
   $('.white_content').html(html);
   $('#fade').show();
   $('.white_content').show();
}

function showLoginLightbox2(html) {
   var e = $('#frmLogin');
   var conheight = e.offsetHeight;
   $('#fade').height(conheight);
   $('#frmLogin').append('<div id="light" class="white_content"></div>');
   $('.white_content').html(html);
   $('#fade').show();
   $('.white_content').show();
}

function customAjax(url,data,type) {
   $('.white_content').remove();
   showLoginLightbox('<h1>Loading...</h1>');
   $.ajax({
      url     : url,
      data    : data,
      type    : type,
      success : function(html){
         $('#light').html(html);
      },
      error   : function(){
         html = '<h1>Oops! Something went wrong</h1>';
         html += '<input id="btnOkay" type="submit" value="OKAY" class="labelbutton2" ></input>';
         $('#light').html(html);
      }
   });
}
