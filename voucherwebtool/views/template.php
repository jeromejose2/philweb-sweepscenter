<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 21, 2012
 * MANAGE SESSION CONTROLLER
 * Purpose: Managing Sessions for Voucher Webtool Template and provides template for all the pages.
 */

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCV_AdminAccounts");
App::LoadModuleClass($modulename, "SCCV_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCCV_AuditLog");

$svadminaccts = new SCCV_AdminAccounts();
$svadminacctsessions = new SCCV_AdminAccountSessions();
$svauditlog = new SCCV_AuditLog();

if (isset($_SESSION['sid']))
{
    $currtime = '';
    $timestart = '';
    $id = '';
    $aid = '';
    
    $svadminacctsessions->StartTransaction();
    $svadminaccts->StartTransaction();
    
    // Get the Session Details
    $sessiondtls = $svadminacctsessions->GetSessionDetails($_SESSION['sid']);
    if (count($sessiondtls) > 0)
    {
        $currtime = $sessiondtls[0]["CurrTime"];
        $timestart = $sessiondtls[0]["TransDate"];
        $id = $sessiondtls[0]["ID"];
        $aid = $sessiondtls[0]["AccountID"];
    }
    
    // Get the User Details
    $userdetails = $svadminaccts->SelectByID($aid);
    $_SESSION["username"] = $userdetails[0]["Username"];
    
    // Check for SESSION TIMEOUT
    $timediff = (strtotime($currtime) - strtotime($timestart)) / 60;
    if($timediff >= 20) 
    {
        $updateSession["ID"] = $id;
        $updateSession["DateEnded"] = 'now_usec()';
        $svadminacctsessions->UpdateByArray($updateSession);
        if ($svadminacctsessions->HasError)
        {
            $errormsg = $svadminacctsessions->getError();
            $errormsgtitle = "ERROR!";
            $svadminacctsessions->RollBackTransaction();
        }

        //log to audit trail
        $svauditlog->StartTransaction();
        $scauditlogparam["TransDetails"] = "Login Account ID: ".$_SESSION['aid'];
        $scauditlogparam["IPAddress"] = $_SERVER['REMOTE_ADDR'];
        $scauditlogparam["DateCreated"] = "now_usec()";
        $scauditlogparam["CreatedBy"] = $_SESSION["username"];
        $svauditlog->Insert($scauditlogparam);
        if ($svauditlog->HasError)
        {
            $errormsg = $svauditlog->getError();
            $errormsgtitle = "ERROR!";
            $svauditlog->RollBackTransaction();
        }

        $svauditlog->CommitTransaction();
        $svadminacctsessions->CommitTransaction(); 
        
        session_destroy();
        URL::Redirect('login.php');
    }
    else 
    {
        $updateSession["ID"] = $id;
        $updateSession["TransDate"] = 'now_usec()';
        $svadminacctsessions->UpdateByArray($updateSession);
        if ($svadminacctsessions->HasError)
        {
            $errormsg = $svadminacctsessions->getError();
            $errormsgtitle = "ERROR!";
            $svadminacctsessions->RollBackTransaction();
        }
        $svadminacctsessions->CommitTransaction(); 
    }
    
    if( $_SESSION['allowedpage'] == 1)
    {
       $allowedPages = array('changepassword',);
             
    }else
    {
      $allowedPages = array('generatecodes','adddenomination','rptgenerationhist','changepassword','changepassword_1','adduseracct','vouchercounter','generationpdf');
    }
} else {
    /* Logout Transaction Here? */
    
    URL::Redirect('login.php');
}

$serverdtls = $svadminacctsessions->GetServerDateTime();
$servertime = substr($serverdtls[0][0],1,19);
$serverdate = date("M d, Y", strtotime($serverdtls[0][0]));

if ($_GET['page'] == 'generationpdf')
{
    include_once 'generationpdf.php';
    exit();
}
?>
<html>
<!--  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>-->
  <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="css/datepicker.css" />
  <script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
  <script language="javascript" src="jscripts/datepicker.js"></script>
  <script language="javascript" src="jscripts/checkinputs.js"></script>
  <script language="javascript" src="jscripts/lightbox.js"></script>  
  <script language="javascript" src="jscripts/ajax.js"></script>
  <title>Guam Sweepstakes Caf&#233; Voucher Generation Tool</title>
  <body style="background-color: #FFFFFF;">
      <form id="frmTemplate" name="frmTemplate" method="post">
        <div id="containertemplate">
          <div id="fade" class="black_overlay"></div>
          <div id="logo" style="background-image: url(images/sweepslogo.png); height:139px; width:293px;"></div>
          <div id="datetime" style ="height: 100px; width: 200px; position: relative; margin-top: -120px; margin-left: 680px;">
            <div class="labelbold" style="text-align:left;">
              Welcome <?php echo $_SESSION['username']; ?>!
            </div>
            <div>
              <b>DATE:</b>&nbsp;<span id="servdate" name="servdate">&nbsp;</span>
            </div>
            <div>
              <b>TIME:</b>&nbsp;<span id="clock">&nbsp;</span>
            </div>
          </div>
          <table>
            <tr>
              <td>
                <div id="menu" style="vertical-align:top; width:255px;">
                  <?php
                        include_once('menu.php');
                  ?>
                </div>
              </td>
              <td>
                <div id="maincontent" >
                  <?php
                  // create an array of allowed pages
//                  $allowedPages = array('generatecodes','adddenomination','rptgenerationhist','changepassword','changepassword_1','adduseracct','vouchercounter','generationpdf');
//                  
                  // check if the page variable is set and check if it is in the array of allowed pages
                  if(isset($_GET['page']) && in_array($_GET['page'], $allowedPages))
                    {
                    // check that the file exists
                    if(file_exists($_GET['page'].'.php'))
                        {
                        // we include the file
                        include_once($_GET['page'].'.php');
                        }
                    else
                        {
                        // if the file does not exist
                        echo 'Requested page does not exist.';
                        }

                    }
                  // if incorrect url
                  else
                    {
                    // if things are not as they should be, we included the default page
                    if(file_exists('index.php'))
                        {
                        //include_once("index.php");
                        //echo "<script language=\"javascript\">window.location(\"url here");
                        }
                    else
                        {
                        // if the default page is missing
                        echo 'index.php is missing.';
                        }
                    }
                ?>
                </div>

              </td>
            </tr>
          </table>

      </div>
      <script type="text/javascript">
        var tempDate = "<?php echo $serverdate; ?>";
        var tempTime = "<?php echo $servertime; ?>";
        document.getElementById("servdate").firstChild.nodeValue = tempDate;

        <?php $startdate = date("F d, Y H:i:s"); ?>
        var timesetter = new Date('<?php echo $startdate ?>');
        var TimeNow = '';
        function MakeTime() {
           timesetter.setTime(timesetter.getTime()+1000);
           var hhN  = timesetter.getHours();
           if(hhN > 12){
              var hh = String(hhN - 12);
              var AP = 'PM';
           } else if(hhN == 12) {
              var hh = '12';
              var AP = 'PM';
           }else if(hhN == 0){
              var hh = '12';
              var AP = 'AM';
           }else{
              var hh = String(hhN);
              var AP = 'AM';
           }
           var mm  = String(timesetter.getMinutes());
           var ss  = String(timesetter.getSeconds());
           TimeNow = ((hh < 10) ? ' ' : '') + hh + ((mm < 10) ? ':0' : ':') + mm + ((ss < 10) ? ':0' : ':') + ss + ' ' + AP;
           document.getElementById("clock").firstChild.nodeValue = TimeNow;
           setTimeout(function(){
              MakeTime();},1000);
         }
         MakeTime();

        /*function updateClock ( )
        {
          var currentTime = new Date ();
          //var currentHours = currentTime.getHours ( );
          //var currentMinutes = currentTime.getMinutes ( );
          var currentSeconds = currentTime.getSeconds ( );
          
          var currentHours = tempTime.substring(11,13);
          var currentMinutes = tempTime.substring(14,16);
          //var currentSeconds = tempTime.substring(17,19);

          // Pad the minutes and seconds with leading zeros, if required
          currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
          currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

          // Choose either "AM" or "PM" as appropriate
          var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

          // Convert the hours component to 12-hour format if needed
          currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

          // Convert an hours component of "0" to "12"
          currentHours = ( currentHours == 0 ) ? 12 : currentHours;

          // Compose the string for display
          var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;

          // Update the time display
          document.getElementById("clock").firstChild.nodeValue = currentTimeString;
          
        }*/

        function timedRefresh(timeoutPeriod)
        {
          setTimeout("location.reload(true);",timeoutPeriod);
        }    
        // -->
      </script>
    </form>
  </body>
</html>
<?php







?>
