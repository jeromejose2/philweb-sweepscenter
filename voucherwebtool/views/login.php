<?php
/*
 * Created By: Noel Antonio
 * Date Modified: May 16, 2012
 * LOGIN PAGE
 * Purpose: Entry point for Guam Management Voucher Webtool
 */
include("../controller/loginprocess.php");
?>
<html>
<!--  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>-->
  <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
  <script language="Javascript" src="jscripts/jquery-1.5.2.min.js" ></script>
  <script language="javascript" src="jscripts/checkinputs.js"></script>
  
  <title>Guam Sweepstakes Caf&#233; Voucher Generation Tool</title>
  <script>
      function preventBackandForward()
        {
            window.history.forward(0);
        }
        preventBackandForward();
        window.inhibited_load=preventBackandForward;
        window.onpageshow=function(evt){if(evt.persisted)preventBackandForward();};
        window.inhibited_unload=function(){void(0);};
        
        $(document).ready(function(){
            $('#txtUsername').focus();
        }); 
        $('#txtUsername').live('keypress',function(e){
            if(e.keyCode == 13) {
                $('#txtPassword').focus();
                return false;
            }
        });        
        $('#txtPassword').live('keypress',function(e){
            if(e.keyCode == 13) {
                $('#btnSubmit').focus();
                $('#btnSubmit').click();
                return false;
            }
        });
        $('a').live('click', function(){
            $('#txtEmail').focus();
            return false;
        });
  </script>
  <body>
    <form id="frmLogin" name="frmLogin" action="login.php" method="post">
        <div id="fade" class="black_overlay"></div>
        <!-- ERROR MESSAGE -->
        <div id="light1" style="margin-left: 33%;" class="white_content">
        <div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
        </div><br />
        <div id="msg"></div>
        <br/><br/>
        <input id="btnOk" type="button" value="OK" class="labelbold2" onclick="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';" />
        </div>
        
        <div id="light3" style="margin-left: 33%;" class="white_content">
        <div id="title3" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
        </div><br />
        <div id="msg3"></div>
        <br/><br/>
        <input id="btnOk" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light3').style.display='none';document.getElementById('fade').style.display='none';" />
        </div>

        <div id="light2" style="margin-left: 33%;" class="white_content">
        <div id="title2" style="width: 100%;height: 27px;background-color: #FF9C42; font-size: 16pt; font-weight: bold; padding-top: 5px;color:black;border-color:#000000;border-width:thin; border-style:solid;"></div><br/>
        <div id="msg2"></div><br/>
            <?php echo $txtEmail; ?><br/><br/>
            <?php echo $btnReset; ?>
            <input id="btnCancel" type="button" value="CANCEL" class="labelbutton2" onclick="document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none';" />
            <br/><br/>
        </div>
        
        <div id="light4" style="margin-left: 33%;" class="white_content">
        <div id="title4" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
        </div><br />
        <div id="msg4"></div>
        <br/><br/>
        <input id="btnOk" type="button" value="OK" class="labelbold2" onclick="location.href='template.php?page=changepassword'" />
        </div>
        <!-- END OF ERROR MESSAGE -->
        
      <div id="container">
          <table cellspacing="6">
            <tr>
              <td colspan="2" align="center">
                <div style="background-image: url(images/sweepslogo.png); height:139px; width:293px;"></div>
                <br /><h2>Guam Sweepstakes Caf&#233; <br />Voucher Generation Tool</h2><br />
              </td>
            </tr>
            <tr>
              <td class="fontboldblack" width="100px" align="right">Username:</td>
              <td><?php echo $txtUsername; ?></td>
            </tr>
            <tr>
              <td class="fontboldblack" width="100px" align="right">Password:</td>
              <td><?php echo $txtPassword; ?></td>
            </tr>
            <tr><td colspan="2" align="center"><?php echo $btnSubmit; ?></td></tr>
            <tr>
              <td colspan="2" align="center">
                <div><a onclick="javascript: return openforgotpw();" style="font-size:1.0em; font-weight:bold; cursor:pointer; color:#000000;" href="">Forgot Password</a></div>
              </td>
            </tr>
            <tr><td colspan="2" align="center"><div id="errmsg"></div></td></tr>
          </table>
      </div>
        <script>
        <?php if (isset($errormsg)): ?>
            document.getElementById('title').innerHTML = "<?php echo $errormsgtitle;?>";
            document.getElementById('msg').innerHTML = "<?php echo $errormsg;?>";
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        <?php endif; ?>
        <?php if (isset($successmsg)): ?>
            document.getElementById('title3').innerHTML = "<?php echo $successmsgtitle;?>";
            document.getElementById('msg3').innerHTML = "<?php echo $successmsg;?>";
            document.getElementById('light3').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        <?php endif; ?>
        <?php if (isset($cpmsg)): ?>
            document.getElementById('title4').innerHTML = "<?php echo $cpmsgtitle;?>";
            document.getElementById('msg4').innerHTML = "<?php echo $cpmsg;?>";
            document.getElementById('light4').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        <?php endif; ?>
        </script>
    </form>
  </body>
</html>
