<?php 
/*
 * Added By: Noel Antonio
 * Date: May 21, 2012
 * Purpose: For logout
 */

require_once("../init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCV_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCCV_AuditLog");

$aid = $_SESSION['aid'];
$svadminacctsessions = new SCCV_AdminAccountSessions();
$svauditlog = new SCCV_AuditLog();

$boolrollback = false;

$arrAcctSessions = $svadminacctsessions->IfExists($aid);
if (count($arrAcctSessions) > 0)
{
    //end current active session
    $svadminacctsessions->StartTransaction();
//    $scupdateparam["DateEnded"] = "now_usec()";
//    $scupdateparam["AccountID"] = $aid;
//    $scupdateacctsession = $svadminacctsessions->UpdateByArray($scupdateparam);
    $scupdateacctsession = $svadminacctsessions->updateSess($aid);
    if ($svadminacctsessions->HasError)
    {
        $errormsg = $svadminacctsessions->getError();
        $errormsgtitle = "ERROR!";
        $boolrollback = true;
    }
    
    // Log to Audit Trail
    $svauditlog->StartTransaction();
    $scauditlogparam["TransDetails"] = "Logout Account ID: ".$_SESSION['aid'];
    $scauditlogparam["IPAddress"] = $_SERVER['REMOTE_ADDR'];
    $scauditlogparam["DateCreated"] = "now_usec()";
    $scauditlogparam["CreatedBy"] = $_SESSION["username"];
    $svauditlog->Insert($scauditlogparam);
    if ($svauditlog->HasError)
    {
        $errormsg = $svauditlog->getError();
        $errormsgtitle = "ERROR!";
        $boolrollback = true;
    }
    
    if ($boolrollback)
    {
        $svadminacctsessions->RollBackTransaction();
        $svauditlog->RollBackTransaction();
    } else {
        $svadminacctsessions->CommitTransaction();
        $svauditlog->CommitTransaction();
    }
}

session_destroy();
URL::Redirect("login.php");
?>
