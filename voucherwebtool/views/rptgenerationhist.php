<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 22, 2012
 * GENERATION HISTORY VIEW PAGE
 * Purpose: Report of all the generated voucher codes.
 */
include("../controller/rptgenerationhistprocess.php");
?>
<script type="text/javascript">
function ChangePage(pagenum)
{
    selectedindex = document.getElementById("pgSelectedPage");
    selectedindex.value = pagenum;
    document.forms[0].submit();
}
function datevalidation()
{
    var SDate = document.getElementById("txtDateFr").value;
    var EDate = document.getElementById("txtDateTo").value;

    var endDate = new Date(EDate);
    var startDate = new Date(SDate);
    var currDate = new Date();

    if(SDate != '' && EDate != '' && startDate > endDate)
    {
    	document.getElementById('title5').innerHTML = "ERROR";
        document.getElementById('msg5').innerHTML = "Please enter a valid date range.";
        document.getElementById('light5').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
  
    else{ return true;}
    } 
</script>
<div style="width:100%; text-align:center;">
  <table>
      <?php echo $hiddenbatch; ?>
      <?php echo $hiddenfile; ?>
    <tr>
      <td class="labelbold2">From:</td>
      <td><?php echo $txtDateFr; ?><img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateFr', this, 'mdy', '/');" style="cursor: pointer;" /></td>
      <td class="labelbold2">To:</td>
      <td><?php echo $txtDateTo; ?><img src="images/close.gif" align="absMiddle" onclick="displayDatePicker('txtDateTo', this, 'mdy', '/');" style="cursor: pointer;" /></td>
      <td width="100px"><?php echo $btnSearch; ?></td>
    </tr>
    <tr>
      <td colspan="5" class="labelbold2">Denomination:&nbsp;&nbsp;
        <?php echo $ddlDenomination; ?>
      </td>
    </tr>
    <tr>
      <td colspan="5">
         <table style="overflow:scroll;">
            <tr><th colspan ="5" style="height:30px; background-color: #FF9C42; color:#000000;">GENERATION HISTORY</th></tr>
            <tr>
                <th class="th">Date of Generation</th>
                <th class="th">Period of Validity</th>
                <th class="th">Denomination</th>
                <th class="th">Batch ID</th>
                <th class="th">Generated By</th>
            </tr>
            <?php if (count($list) > 0): ?>
            <?php for ($i = 0; $i < count($list); $i++){ ?>
            <tr style="background-color:#FFF1E6; height:30px;">
                <td class="td"><?php echo date("m/d/Y h:i:s A", strtotime($list[$i]["DateGenerated"])); ?></td>
                <td class="td"><?php if ($list[$i]["Validity"] <> '') { echo date("m/d/Y", strtotime($list[$i]["Validity"]));} ?></td>
                <td class="td"><?php echo $list[$i]["Description"]; ?></td>
                <td class="td"><a style="text-decoration: none" href="template.php?page=generationpdf&batchid=<?php echo $list[$i]["BatchNo"]; ?>&filename=<?php echo "SWC_Voucher" . $list[$i]["Description"] . "_" . date("mdY", strtotime($list[$i]["DateGenerated"])) . "_" . $list[$i]["BatchNo"]; ?>"><?php echo $list[$i]["BatchNo"]; ?></a></td>
                <td class="td"><?php echo $list[$i]["GeneratedBy"]; ?></td>
            </tr>
            <?php } ?>
            <?php else: ?>
                <td class="td" colspan="5"><br/>No Records Found.</td>
            <?php endif; ?>
         </table>
          <?php if (count($list) > 0): ?>
            <br/><div style="text-align: center;">Page: <?php echo $pgTransactionHistory; ?></div>
          <?php endif; ?>
      </td>
    </tr>
  </table>
    <!-- POP UP FOR MESSAGES -->
<div id="light5" class="white_content">
<div id="title5" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
</div><br />
<div id="msg5"></div>
<input id="btnOk" type="button" value="OKAY" class="labelbold2" onclick="document.getElementById('light5').style.display='none';document.getElementById('fade').style.display='none';"/>
</div>
 </div>

<script type="text/javascript" lang="Javascript">
<?php if(isset($_GET['generatepdf']) && $_GET['generatepdf'] == 1): ?>
      window.open("template.php?page=generationpdf&batchid=<?php echo $_GET['batchid']; ?>&filename=<?php echo $_GET['filename'];?>");
<?php endif; ?>
</script>
