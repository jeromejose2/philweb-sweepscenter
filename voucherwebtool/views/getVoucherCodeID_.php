<?php 
/* Created By: Noel Antonio May 24, 2012
 * Purpose: For ajax loading of voucher code id
 */
require_once("include/core/init.inc.php");

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCV_BatchInfo");
App::LoadModuleClass($modulename, "SCCV_VoucherInfo");
$svbatchinfo = new SCCV_BatchInfo();
$svvoucherinfo = new SCCV_VoucherInfo();

$denomination = $_POST['denomination'];

$currentusedvoucher = '0';
$where = " WHERE Denomination = '$denomination'";
$voucherdtls = $svbatchinfo->SelectByWhere($where);

if($denomination == 1)
{
    $where = " WHERE Status = '1' AND Denomination = '1' LIMIT 1";
    $batchdtls = $svvoucherinfo->SelectByWhere($where);
    $currentusedvoucher = $batchdtls[0]["BatchID"];
}
else {
    $option = "<option value = '0'>Select Batch ID</option>";
}

if(count($voucherdtls) > 0)
{
    for ($i = 0; $i < count($voucherdtls); $i++)
    {
        $countdtls = $svvoucherinfo->SelectCount($denomination, $voucherdtls[$i]["BatchNo"]);
        if($countdtls[0][0] > 0)
        {
            if($currentusedvoucher == $voucherdtls[$i]["BatchNo"])
            {
                $option .= "<option value = '" . $voucherdtls[$i]["BatchNo"] . "' selected>" . $voucherdtls[$i]["BatchNo"] . "</option>";
            }
            else
            {
                $option .= "<option value = '" . $voucherdtls[$i]["BatchNo"] . "'>" . $voucherdtls[$i]["BatchNo"] . "</option>";
            }
        }
    }     
}

echo $option;
?>
