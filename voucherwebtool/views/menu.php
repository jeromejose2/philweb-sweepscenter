<?php 
/*
 * @author
 * Purpose   : view for menu
 */
if(!isset($_SESSION))
{
    session_start(); 
}
$pagename = $_GET["page"];
$accttype = $_SESSION['accttype'];
$willchangepassword = $_SESSION['willchangepassword'];
?>
<script type="text/javascript" src="jscripts/leftmenu.js"></script>
<link type="text/css" href="css/default.css" media="projection, screen" rel="stylesheet" />
<div id="masterdiv">
   <?php if ($willchangepassword == 1){ ?>
    <div class="menutitle" style="cursor: default;">
        Generate Voucher
    </div>
   <?php } else { ?>
    <div class="menutitle" onclick="SwitchMenu('sub1')">
        Generate Voucher
    </div>
  <span class="submenu" id="sub1">
    <?php
    if ($accttype == 1 || $accttype == 2)
    {
      if ($pagename == "generatecodes"){
      ?>
    <a href="" class="linkmenuactive">* Generate Codes</a>
    <br />
    <?php
      }
      else
      {
      ?>
    <a href="template.php?page=generatecodes" class="linkmenu">* Generate Codes</a>
    <br />
    <?php
      }
      
      if ($pagename == "adddenomination"){
      ?>
    <a href="" class="linkmenuactive">* Add Denomination</a>
    <br />
    <?php
      }
      else
      {
      ?>
    <a href="template.php?page=adddenomination" class="linkmenu">* Add Denomination</a>
    <br />
    <?php
      }
    }
   }
    ?>
    
  </span>
  <div class="menutitle">
    <?php if ($willchangepassword == 1){ ?>
        <a href="" style="cursor: default;text-decoration:none; color:#000;">Voucher Counter</a>
    <?php } else { ?>
        <a href="template.php?page=vouchercounter" style="text-decoration:none; color:#000;">Voucher Counter</a>
    <?php } ?>
  </div>
    
  <div class="menutitle">
    <?php if ($willchangepassword == 1){ ?>
        <a href="" style="cursor: default;text-decoration:none; color:#000;">Generation History</a>
    <?php } else { ?>
        <a href="template.php?page=rptgenerationhist" style="text-decoration:none; color:#000;">Generation History</a>
    <?php } ?>
  </div>
    
  <div class="menutitle">
        <a href="template.php?page=changepassword" style="text-decoration:none; color:#000;">Change Password</a>
  </div>
    
  <div class="menutitle">
    <?php if ($willchangepassword == 1){ ?>
        <a href="" style="cursor: default;text-decoration:none; color:#000;">Create User Account</a>
    <?php } else { ?>
        <a href="template.php?page=adduseracct" style="text-decoration:none; color:#000;">Create User Account</a>
    <?php } ?>
  </div>
    
  <div class="menutitle">
        <a href="logout.php" style="text-decoration:none; color:#000;">Log Out</a>
  </div>
    
</div>

