<?php 
/*
 * Created By: Noel Antonio
 * Date Created: May 22, 2012
 * ADD DENOMINATION VIEW PAGE
 * Purpose: For adding new denomination.
 */
include("../controller/adddenominationprocess.php");
?>
<div id="page">
  <table width="100%">
    <tr>
      <td colspan="2">
        <div id="header">Add Voucher Denomination</div>
      </td>
    </tr>
    <tr style="height:30px;">
      <td class="fontboldblackcenter">Enter New Denomination:</td>
      <td><?php echo $txtDenomination; ?>
	<span id="blank_msg"></span>    
    </tr>
    <tr style="height:30px;">
      <td colspan="2" align="center"><?php echo $btnSubmit; ?></td>
    </tr>
  </table>
</div>

<div id="fade" class="black_overlay"></div>
<div id="light1" class="white_content">
<div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;text-color:black">
</div><br />
<div id="msg"></div>
</div>

<script language="Javascript" type="text/javascript">   
$(document).ready(function(){
    $('#btnSubmit').live('click',function(){
        if($('#txtDenomination').val()== "") {
            document.getElementById('title').innerHTML = "NOTIFICATION";
            document.getElementById('msg').innerHTML = "Please enter a new denomination to be added.";
            document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbold2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            document.getElementById('blank_msg').innerHTML = "<font color='#ff0000'>*</font>";
            document.getElementById('txtDenomination').focus();
            return false;
        }
        return true;
   });
});

<?php if (isset($errormsg)):?>
        document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
        document.getElementById('msg').innerHTML = "<?php echo $errormsg; ?>";
        document.getElementById('msg').innerHTML += "<br/><br/><br/><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
<?php endif; ?>
    
<?php if (isset($errormsg) && isset($confirm)):?>
        document.getElementById('title').innerHTML = "<?php echo $errormsgtitle; ?>";
        document.getElementById('msg').innerHTML = "<?php echo $errormsg; ?>";
        document.getElementById('msg').innerHTML += "<br/><br/><br/>" + "<?php echo $btnConfirm; ?> &nbsp&nbsp";
        document.getElementById('msg').innerHTML += "<input id=\"btnCancel\" type=\"button\" value=\"CANCEL\" class=\"labelbutton2\" onclick=\"document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';\" />";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
<?php endif; ?>    
    
<?php if (isset($successmsg)):?>
        document.getElementById('title').innerHTML = "<?php echo $successmsgtitle; ?>";
        document.getElementById('msg').innerHTML = "<?php echo $successmsg; ?>";      
        document.getElementById('msg').innerHTML += "<input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\" onclick=\"window.location = 'template.php?page=generatecodes';\" />";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
<?php endif; ?>   
</script>
