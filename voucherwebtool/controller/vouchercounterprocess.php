<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 24, 2012
 * VOUCHER COUNTER CONTROLLER
 * Purpose: Counter page for generated vouchers.
 */
$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCV_Denominationref");
App::LoadModuleClass($modulename, "SCCV_BatchInfo");
App::LoadModuleClass($modulename, "SCCV_VoucherInfo");

$svdenominationref = new SCCV_Denominationref();
$svbatchinfo = new SCCV_BatchInfo();
$svvoucherinfo = new SCCV_VoucherInfo();

App::LoadControl("Hidden");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$fproc = new FormsProcessor();

$ddlDenomination = new ComboBox("ddlDenomination", "ddlDenomination", "Select Denomination");
$option1 = null;
$option1[] = new ListItem("Select Denomination", "0", true);
$ddlDenomination->Items = $option1;
$denopt = $svdenominationref->SelectDenomination();
$denlist = new ArrayList();
$denlist->AddArray($denopt);
$ddlDenomination->DataSource = $denlist;
$ddlDenomination->DataSourceValue = "ID";
$ddlDenomination->DataSourceText = "Description";
$ddlDenomination->DataBind();
$ddlDenomination->Args = "onchange='javascript: return loadVoucherCodeID();'";

$ddlVoucherCodeID = new ComboBox("ddlVoucherCodeID", "ddlVoucherCodeID", "Batch ID;");
$option2 = null;
$option2[] = new ListItem("Select Batch ID", "0", true);
$ddlVoucherCodeID->Items = $option2;
$ddlVoucherCodeID->Args = "onchange='javascript: return getSelectedBatchID();'";

$btnSearch = new Button("btnSearch", "btnSearch", "SEARCH");
$btnSearch->CssClass = "labelbutton2";
$btnSearch->Args = "onclick='javascript: return checkVoucherCounterInfo();'";
$btnSearch->IsSubmit = true;

$btnRefresh = new Button("btnRefresh", "btnRefresh", "REFRESH");
$btnRefresh->CssClass = "labelbutton2";
$btnRefresh->Args = "onclick='javascript: return checkVoucherCounterInfo();'";
$btnRefresh->IsSubmit = true;

$hidden = new Hidden("hidden", "hidden");

$vouchercodeid_dtls = "";
$denomination_dtls = "";
$startdate_dtls = "";
$enddate_dtls = "";
$generatedby_dtls = "";
$totalvoucher_dtls = "";
$eadd_dtls = "";
$usedvoucher_dtls = "";
$unusedvoucher_dtls = "";

$fproc->AddControl($ddlDenomination);
$fproc->AddControl($ddlVoucherCodeID);
$fproc->AddControl($btnSearch);
$fproc->AddControl($hidden);
$fproc->AddControl($btnRefresh);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSearch->SubmittedValue == "SEARCH" || $btnRefresh->SubmittedValue == "REFRESH")
    {
        if ($ddlVoucherCodeID->SubmittedValue == 0)
        {
            $ddlVoucherCodeID->Args = "disabled = true";
        }
        $where = " WHERE Denomination = '$ddlDenomination->SubmittedValue'";
        $voucherdtls = $svbatchinfo->SelectByWhere($where);
        $list = new ArrayList();
        $list->AddArray($voucherdtls);
        $ddlVoucherCodeID->DataSource = $list;
        $ddlVoucherCodeID->DataSourceText = "BatchNo";
        $ddlVoucherCodeID->DataSourceValue = "BatchNo";
        $ddlVoucherCodeID->DataBind();
        $ddlVoucherCodeID->SetSelectedValue($hidden->SubmittedValue);
        
        // Get Voucher Counter Details
        $ctrvoucherdtls = $svbatchinfo->SelectVoucherCounterDetails($ddlDenomination->SubmittedValue, $ddlVoucherCodeID->SubmittedValue);
        if (count($ctrvoucherdtls) > 0)
        {
            $vouchercodeid_dtls = $ctrvoucherdtls[0][0];
            $denomination_dtls = $ctrvoucherdtls[0][1];
            $startdate_dtls = date("m/d/Y h:i:s A", strtotime($ctrvoucherdtls[0][2]));
            if ($ctrvoucherdtls[0][3] <> '')
            {
                $enddate_dtls = date("m/d/Y h:i:s A", strtotime($ctrvoucherdtls[0][3]));
            }
            else
            {
                $enddate_dtls = '';
            }
            $generatedby_dtls = $ctrvoucherdtls[0][4];
            $totalvoucher_dtls = $ctrvoucherdtls[0][5];
            $eadd_dtls = $ctrvoucherdtls[0][6];
        } else {
            $vouchercodeid_dtls = "No record found";
            $denomination_dtls = "No record found";
            $startdate_dtls = "No record found";
            $enddate_dtls = "No record found";
            $generatedby_dtls = "No record found";
            $totalvoucher_dtls = "No record found";
            $eadd_dtls = "";
        }
        $usedvoucher = $svvoucherinfo->SelectCountVoucher($ddlDenomination->SubmittedValue, $ddlVoucherCodeID->SubmittedValue, 2);
        $usedvoucher_dtls = $usedvoucher[0][0];
        if ($ddlDenomination->SelectedText == "0.10")
        {
            $unusedstatus = 1; // free entry denomination
            $assignedvoucher = $svvoucherinfo->SelectCountVoucher($ddlDenomination->SubmittedValue, $ddlVoucherCodeID->SubmittedValue, 4);
            $assignedvoucher_dtls = $assignedvoucher[0][0];
        } else {
            $unusedstatus = 4; // regular denomination
            $assignedvoucher_dtls = "---";
        }
        $unusedvoucher = $svvoucherinfo->SelectCountVoucher($ddlDenomination->SubmittedValue, $ddlVoucherCodeID->SubmittedValue, $unusedstatus);
        $unusedvoucher_dtls = $unusedvoucher[0][0];
        $expiredvoucher = $svvoucherinfo->SelectCountVoucher($ddlDenomination->SubmittedValue, $ddlVoucherCodeID->SubmittedValue, 3);
        $expiredvoucher_dtls = $expiredvoucher[0][0];
        $voidvoucher = $svvoucherinfo->SelectCountVoucher($ddlDenomination->SubmittedValue, $ddlVoucherCodeID->SubmittedValue, 5);
        $voidvoucher_dtls = $voidvoucher[0][0];
        if ($ddlDenomination->SelectedText == "0.10")
        {
            $percentage_dtls = round(((($assignedvoucher_dtls + $usedvoucher_dtls + $expiredvoucher_dtls + $voidvoucher_dtls) / $totalvoucher_dtls) * 100) , 4);
        } else {
            $percentage_dtls = round(((($usedvoucher_dtls + $expiredvoucher_dtls + $voidvoucher_dtls) / $totalvoucher_dtls) * 100) , 4);
        }
    }
}
else
{
    $ddlVoucherCodeID->Args = "disabled = true";
}
?>
