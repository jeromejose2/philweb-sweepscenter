<?php
/*
 * Created By: Noel Antonio
 * Date Modified: May 16, 2012
 * LOGIN CONTROLLER
 * Purpose: Entry point for Guam Management Voucher Webtool
 */

require_once("../init.inc.php");

session_regenerate_id();

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCV_AdminAccounts");
App::LoadModuleClass($modulename, "SCCV_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCCV_AuditLog");

App::LoadCore("PHPMailer.class.php");
App::LoadControl("TextBox");
App::LoadControl("Button");

$fproc = new FormsProcessor();
$svadminaccts = new SCCV_AdminAccounts();
$svadminacctsessions = new SCCV_AdminAccountSessions();
$svauditlog = new SCCV_AuditLog();

$txtUsername = new TextBox("txtUsername", "txtUsername", "Username: ");
$txtUsername->Length = 20;
$txtUsername->Args="autocomplete='off' onkeypress='javascript: return DisableSpaceandSpCharacters(event);'";

$txtPassword = new TextBox("txtPassword", "txtPassword", "Password: ");
$txtPassword->Password = true;
$txtPassword->Length = 20;
$txtPassword->Args="autocomplete='off' onkeypress='javascript: return DisableSpaceandSpCharacters(event);'";

$txtEmail = new TextBox("txtEmail","txtEmail", "Email");
$txtEmail->Style = "width: 300px";
$txtEmail->Args="onkeypress='javascript: return specialkeys(event);'";

$btnSubmit = new Button("btnSubmit", "btnSubmit", "Log In");
$btnSubmit->CssClass = "labelbold2";
$btnSubmit->Args="onclick='javascript: return checkunamepword();'";
$btnSubmit->IsSubmit = true;

$btnReset = new Button("btnReset", "btnReset", "RESET");
$btnReset->Args="onclick='javascript: return checkinputemail();'";
$btnReset->CssClass = "labelbutton2";
$btnReset->IsSubmit = true;

$fproc->AddControl($txtUsername);
$fproc->AddControl($txtPassword);
$fproc->AddControl($txtEmail);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnReset);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{    
    if ($btnSubmit->SubmittedValue == "Log In")
    {
        $username = mysql_escape_string(trim($txtUsername->Text));
        $password = mysql_escape_string(trim($txtPassword->Text));
        
        $_SESSION['sid'] = session_id();
        
        // check if account exist
        $arrAccts = $svadminaccts->CheckAccount($username, $password);
        if (count($arrAccts) == 1)
        {
            $arrDtls = $arrAccts[0];
            $aid = $arrDtls["ID"];
            $accttype = $arrDtls["AccountType"];
            $status = $arrDtls["Status"];
            $sessionpassword = $arrDtls["Password"];
            $_SESSION['aid'] = $aid;
            $_SESSION['accttype'] = $accttype;
            $_SESSION['password'] = $sessionpassword;
            $_SESSION['actualpassword'] = $password;
            $_SESSION['actualusername'] = $username;
            $boolrollback = false;
            // Check for Existing Session
            $svadminacctsessions->StartTransaction();
            $svauditlog->StartTransaction();
            
            $arrAcctSessions = $svadminacctsessions->IfExists($aid);
            if (count($arrAcctSessions) > 0)
            {
                // End Current Active Session
                $scupdateparam["DateEnded"] = "now_usec()";
                $scupdateparam["AccountID"] = $aid;
                $svadminacctsessions->UpdateByArray($scupdateparam);
                if ($svadminacctsessions->HasError)
                {
                    $errormsg = $svadminacctsessions->getError();
                    $errormsgtitle = "ERROR!";
                    $boolrollback = true;
                }                       
            }
            
            // Insert into Account Session
            $scacctsessionsparam["SessionID"] = $_SESSION['sid'];
            $scacctsessionsparam["AccountID"] = $_SESSION['aid'];
            $scacctsessionsparam["TransDate"] = "now_usec()";
            $scacctsessionsparam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $scacctsessionsparam["RecCreOn"] = "now_usec()";
            $scacctsessionsparam["RecCreBy"] = $username;
            $scacctsessionsparam["DateEnded"] = "0";
            $svadminacctsessions->Insert($scacctsessionsparam);
            if ($svadminacctsessions->HasError)
            {
                $errormsg = $svadminacctsessions->getError();
                $errormsgtitle = "ERROR!";
                $boolrollback = true;
            }

            // Log to Audit Trail
            $scauditlogparam["TransDetails"] = "Login Account ID: ".$_SESSION['aid'];
            $scauditlogparam["IPAddress"] = $_SERVER['REMOTE_ADDR'];
            $scauditlogparam["DateCreated"] = "now_usec()";
            $scauditlogparam["CreatedBy"] = $username;
            $svauditlog->Insert($scauditlogparam);
            if ($svauditlog->HasError)
            {
                $errormsg = $svauditlog->getError();
                $errormsgtitle = "ERROR!";
                $svauditlog->RollBackTransaction();
            } else {
                $svauditlog->CommitTransaction();
            }
                
            if ($boolrollback)
            {
                    $svadminacctsessions->RollBackTransaction();
            } 
            else 
            {
                    $svadminacctsessions->CommitTransaction();  

                    $acctchangepass = $svadminaccts->SelectAccounttoChangePassword($username, $password);
                    $_SESSION['willchangepassword'] = $acctchangepass[0][0];
                    if ($acctchangepass[0][0] == 1)
                    {
                            $cpmsg = "This is a newly created account. You must change your password first.";
                            $cpmsgtitle = "Change Password!";
                    }
                    else
                    {
                            $checksess = $svadminacctsessions->checkSession($aid);
                            if(count($checksess) == 1)
                            {
                                if($_SESSION['actualpassword'] == "password")
                                {
                                        URL::Redirect('template.php?page=changepassword');
                                        $_SESSION['allowedpage'] = 1;
                                } 
                                else
                                {
                                        URL::Redirect('template.php?page=generatecodes');
                                        $_SESSION['allowedpage'] = 0;
                                }
                            }
                            else 
                            {
                                    URL::Redirect('template.php?page=generatecodes');
                                    $_SESSION['allowedpage'] = 0;
                            }
                    }
            }
        }
        else
        {
            $errormsg = "You have entered an invalid account information.";
            $errormsgtitle = "Error!";
        }
    }
    
    if ($btnReset->SubmittedValue == "RESET")
    {
        $emailaddr = $txtEmail->SubmittedValue;
        
        $svadminaccts->StartTransaction();
        $where = " WHERE Email = '$emailaddr' AND Status = '1'";
        $userdetails = $svadminaccts->SelectByWhere($where);
        $id = $userdetails[0]["ID"];
        $name = $userdetails[0]["Username"];
        $acctname = $userdetails[0]["FirstName"] . " " . $userdetails[0]["MiddleName"] . " " . $userdetails[0]["LastName"];
        $newpass = substr(session_id(),0,8);
        
        // Update password
        $updateAccount["ID"] = $id;
        $updateAccount["Password"] = MD5($newpass);
        $svadminaccts->UpdateByArray($updateAccount);
        if ($svadminaccts->HasError)
        {
            $errormsg = $svadminaccts->getErrors();
            $errormsgtitle = "ERROR!";
            $svadminaccts->RollBackTransaction();
        }
        $svadminaccts->CommitTransaction();
        
        // Sending of Email
        $pm = new PHPMailer();
        $pm->AddAddress($emailaddr, $name);
        
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        $folder = $_SERVER["REQUEST_URI"];
        $folder = substr($folder,0,strrpos($folder,'/') + 1);
        if ($_SERVER["SERVER_PORT"] != "80") 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
        } 
        else 
        {
          $pageURL .= $_SERVER["SERVER_NAME"].$folder;
        }
        
        $pm->IsHTML(true);

        $pm->Body = "Dear ".$acctname."<br/><br/>
            This is to inform you that your password has been reset on this date ".date("m/d/Y")." and time ".date("H:i:s").". Here are your new Account Details:<br/><br/>
            Username: <b>".$name."</b><br/>
            Password: <b>".$newpass."</b><br/><br/>".
        "<b>For security purposes, please change this system-generated password upon log in at </b><a href=" . $pageURL .">http://www.thesweepscenter.com/VoucherWebtool</a><br/><br/>".
        "If you didn't perform this procedure, please report this to our Customer Service email at support.gu@philwebasiapacific.com or support@philwebasiapacific.com."."<br/><br/>
         Regards,<br/><br/>Customer Support<br/>The Sweeps Center";

        $pm->From = "support@thesweepscenter.com";
        $pm->FromName = "The Sweeps Center"; 
        $pm->Host = "localhost";
        $pm->Subject = "NOTIFICATION FOR NEW PASSWORD";
        $email_sent = $pm->Send();
        if($email_sent)
        {
            $successmsg = "Your password has been sent to your e-mail account. For security purpose, please change this system-generated password upon log in.";
            $successmsgtitle = "Reset Password";
        }
        else
        {
            $errormsg = "An error occurred while sending the email to your email address";
            $errormsgtitle = "ERROR!";
        }
    }
}
?>
