<?php
/*
 * Created By: Noel Antonio
 * Date Modified: May 23, 2012
 * ADD DENOMINATION CONTROLLER
 * Purpose: For adding new denomination.
 */
$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCV_AdminAccounts");
App::LoadModuleClass($modulename, "SCCV_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCCV_AuditLog");
App::LoadModuleClass($modulename, "SCCV_Denominationref");

App::LoadControl("TextBox");
App::LoadControl("Button");

$fproc = new FormsProcessor();
$svadminaccts = new SCCV_AdminAccounts();
$svadminacctsessions = new SCCV_AdminAccountSessions();
$svauditlog = new SCCV_AuditLog();
$svdenominationref = new SCCV_Denominationref();

$txtDenomination = new TextBox("txtDenomination", "txtDenomination");
$txtDenomination->Length = 10;
$txtDenomination->Style = "width: 220px";
$txtDenomination->Args = "onkeypress='javascript: return isNumericAndPeriod(event);'";

$btnSubmit = new Button("btnSubmit", "btnSubmit", "SUBMIT");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->IsSubmit = true;

$btnConfirm = new Button("btnConfirm", "btnConfirm", "OKAY");
$btnConfirm->CssClass = "labelbutton2";
$btnConfirm->IsSubmit = true;

$fproc->AddControl($txtDenomination);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnConfirm);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSubmit->SubmittedValue == "SUBMIT")
    {
        $boolrollback = false;   
        
        $sessiondtls = $svadminacctsessions->GetSessionDetails($_SESSION['sid']);
        if (count($sessiondtls) > 0)
        {   
            $where = " WHERE Description = '$txtDenomination->SubmittedValue';";
            $denodtls = $svdenominationref->SelectByWhere($where);
            if (count($denodtls) == 0)
            {
                $errormsg = "You are about to add new voucher code denomination $txtDenomination->SubmittedValue for the Generate Codes tool. Do you wish to continue?";
                $errormsgtitle = "Confirmation";
                $confirm = 'true';
            } else {
                $errormsg = "The denomination you've entered already exists. Please enter another denomination.";
                $errormsgtitle = "NOTIFICATION";
            }
        } else {
            URL::Redirect('login.php');
        }
    }
    
    if ($btnConfirm->SubmittedValue == "OKAY")
    {
        $svdenominationref->StartTransaction();
        $insertdeno["Description"] = $txtDenomination->SubmittedValue;
        $svdenominationref->Insert($insertdeno);
        if ($svdenominationref->HasError)
        {
            $errormsg = $svdenominationref->getErrors();
            $svdenominationref->RollBackTransaction();
        } else {
            $svdenominationref->CommitTransaction();
            $successmsg = "Denomination successfully added.";
            $successmsgtitle = "Confirmation";
        }
    }
}
?>
