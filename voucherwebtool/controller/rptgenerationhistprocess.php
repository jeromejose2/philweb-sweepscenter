<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 22, 2012
 * GENERATION HISTORY CONTROLLER
 * Purpose: Report controller for all the generated voucher codes.
 */

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCV_Denominationref");
App::LoadModuleClass($modulename, "SCCV_VoucherInfo");
App::LoadModuleClass($modulename, "Pdf");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");
App::LoadControl("PagingControl2");

$fproc = new FormsProcessor();
$svdenominationref = new SCCV_Denominationref();
$svvoucherinfo = new SCCV_VoucherInfo();

$ddlDenomination = new ComboBox("ddlDenomination", "ddlDenomination", "Select Denomination");
$option1 = null;
$option1[] = new ListItem("All", "0", true);
$ddlDenomination->Items = $option1;
$denopt = $svdenominationref->SelectDenomination();
$denlist = new ArrayList();
$denlist->AddArray($denopt);
$ddlDenomination->DataSource = $denlist;
$ddlDenomination->DataSourceValue = "ID";
$ddlDenomination->DataSourceText = "Description";
$ddlDenomination->DataBind();

$txtDateFr = new TextBox("txtDateFr", "txtDateFr");
$txtDateFr->ReadOnly = true;
$txtDateFr->Length = 10;
$txtDateFr->Args = "onclick='javascript: return isNumberKey(event);'";
$txtDateFr->Style = "text-align: center; width: 130px;";
$txtDateFr->Text = date("m/d/Y");

$txtDateTo = new TextBox("txtDateTo", "txtDateTo");
$txtDateTo->ReadOnly = true;
$txtDateTo->Length = 10;
$txtDateTo->Args = "onclick='javascript: return isNumberKey(event);'";
$txtDateTo->Style = "text-align: center; width: 130px;";
$txtDateTo->Text = date("m/d/Y");

$btnSearch = new Button("btnSearch", "btnSearch", "SEARCH");
$btnSearch->CssClass = "labelbutton2";
$btnSearch->IsSubmit = true;
$btnSearch->Args = "onclick = 'javascript: return datevalidation();'";

$hiddenbatch = new Hidden("hiddenbatch","hiddenbatch");
$hiddenfile = new Hidden("hiddenfile","hiddenfile");

$fproc->AddControl($txtDateFr);
$fproc->AddControl($txtDateTo);
$fproc->AddControl($btnSearch);
$fproc->AddControl($ddlDenomination);
$fproc->AddControl($hiddenbatch);
$fproc->AddControl($hiddenfile);

$itemsperpage = 10;
$pgcon = new PagingControl2($itemsperpage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;
$pgcon->PageGroup = 5;   
$fproc->ProcessForms();

if ($ddlDenomination->SelectedValue == 0)
{
    $datefr = date("Y-m-d", strtotime($txtDateFr->Text));
    $dateto = date("Y-m-d", strtotime($txtDateTo->Text));
    $count = $svvoucherinfo->SelectGeneratedVoucherHistory($datefr, $dateto, 0);
    $_SESSION["count"] = count($count);
    $voucherdata = $svvoucherinfo->SelectGeneratedVoucherHistoryWithLimit($datefr, $dateto, 0, $pgcon->SelectedItemFrom-1, $itemsperpage);
}

if ($fproc->IsPostBack)
{
    if ($btnSearch->SubmittedValue == "SEARCH" || $fproc->GetPostVar('pgSelectedPage') != '')
    {
        $datefr = date("Y-m-d", strtotime($txtDateFr->SubmittedValue));
        $dateto = date("Y-m-d", strtotime($txtDateTo->SubmittedValue));
        $count = $svvoucherinfo->SelectGeneratedVoucherHistory($datefr, $dateto, $ddlDenomination->SubmittedValue);
        $_SESSION["count"] = count($count);
        $voucherdata = $svvoucherinfo->SelectGeneratedVoucherHistoryWithLimit($datefr, $dateto, $ddlDenomination->SubmittedValue, $pgcon->SelectedItemFrom-1, $itemsperpage);
        if ($btnSearch->SubmittedValue == "SEARCH") { $pgcon->SelectedPage = 1; }
    }
}
$list = new ArrayList();
$list->AddArray($voucherdata);
$pgcon->Initialize($itemsperpage, $_SESSION["count"]);
$pgTransactionHistory = $pgcon->PreRender();
unset($_SESSION["count"]);
unset($voucherdata);
?>