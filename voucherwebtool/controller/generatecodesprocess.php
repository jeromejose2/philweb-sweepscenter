<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 21, 2012
 * GENERATE CODE CONTROLLER
 * Purpose: Landing page. Controller for generating voucher codes.
 */

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCV_Denominationref");
App::LoadModuleClass($modulename, "SCCV_BatchInfo");
App::LoadModuleClass($modulename, "SCCV_VoucherInfo");
App::LoadModuleClass($modulename, "SCCV_AuditLog");
App::LoadModuleClass($modulename, "Pdf");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");

$fproc = new FormsProcessor();
$svdenominationref = new SCCV_Denominationref();
$svbatchinfo = new SCCV_BatchInfo();
$svvoucherinfo = new SCCV_VoucherInfo();
$svauditlog = new SCCV_AuditLog();

$ds = new DateSelector("now");

$ddlDenomination = new ComboBox("ddlDenomination", "ddlDenomination", "Select Denomination");
$option1 = null;
$option1[] = new ListItem("Select Denomination", "0", true);
$ddlDenomination->Items = $option1;
$denopt = $svdenominationref->SelectDenomination();
$denlist = new ArrayList();
$denlist->AddArray($denopt);
$ddlDenomination->DataSource = $denlist;
$ddlDenomination->DataSourceValue = "ID";
$ddlDenomination->DataSourceText = "Description";
$ddlDenomination->DataBind();

$txtQty = new TextBox("txtQty","txtQty","Quantity");
$txtQty->Length = 10;
$txtQty->Text = 50;
$txtQty->Style = "width: 100px;";
$txtQty->Args = "onkeypress='javascript: return isnumberkey(event);'";

$ddlValidity = new ComboBox("ddlValidity","ddlValidity","Validity Period");
$option2 = null;
$option2[] = new ListItem("No Expiration", "0", true);
$option2[] = new ListItem("1 Week", "1w");
$option2[] = new ListItem("2 Weeks", "2w");
$option2[] = new ListItem("1 Month", "1m");
$option2[] = new ListItem("3 Months", "3m");
$option2[] = new ListItem("6 Months", "6m");
$option2[] = new ListItem("1 Year", "12m");
$ddlValidity->Items = $option2;

$btnGenerate = new Button("btnSubmit", "btnSubmit", "GENERATE");
$btnGenerate->CssClass = "labelbutton2";

$btnConfirm = new Button("btnConfirm", "btnConfirm", "OKAY");
$btnConfirm->IsSubmit = true;
$btnConfirm->CssClass = "labelbutton2";

$hiddenbatchid = new Hidden("hiddenbatchid", "hiddenbatchid");
$hiddenfile = new Hidden("hiddenfile", "hiddenfile");

$fproc->AddControl($ddlDenomination);
$fproc->AddControl($txtQty);
$fproc->AddControl($ddlValidity);
$fproc->AddControl($btnGenerate);
$fproc->AddControl($btnConfirm);
$fproc->AddControl($hiddenbatchid);
$fproc->AddControl($hiddenfile);

$fproc->ProcessForms();



if ($fproc->IsPostBack)
{
    
  
    if ($btnConfirm->SubmittedValue == "OKAY")
    {     
        $boolrollback = false;               
        switch($ddlValidity->SubmittedValue)
        {
            case "0": $validity = ""; break;
            case "1w": $ds->AddWeeks(1); $validity = $ds->CurrentDate; break;
            case "2w": $ds->AddWeeks(2); $validity = $ds->CurrentDate; break;
            case "1m": $ds->AddMonths(1); $validity = $ds->CurrentDate; break;
            case "3m": $ds->AddMonths(2); $validity = $ds->CurrentDate; break;
            case "6m": $ds->AddMonths(6); $validity = $ds->CurrentDate; break;  
            case "12m": $ds->AddYears(1); $validity = $ds->CurrentDate; break;
        }
        
        // Get Batch Max ID
        $batchdata = $svbatchinfo->SelectMaxID();
        $max_batchid = $batchdata[0]["MaxID"];
        
        // Get Description of Selected Denomination
        $denodata = $svdenominationref->SelectByID($ddlDenomination->SubmittedValue);
        $deno_desc = $denodata[0]["Description"];
        
        // Insert to tbl_batchinfo
        $svbatchinfo->StartTransaction();
        $arrbatch["BatchNo"] = STR_PAD($max_batchid, 3, "000", STR_PAD_LEFT);
        $arrbatch["Quantity"] = $txtQty->SubmittedValue;
        $arrbatch["Validity"] = $validity;
        $arrbatch["Denomination"] = $ddlDenomination->SubmittedValue;
        $arrbatch["DateGenerated"] = 'now_usec()';
        $arrbatch["GeneratedBy"] = $_SESSION["username"];
        $svbatchinfo->Insert($arrbatch);
        if ($svbatchinfo->HasError){
            $errormsg = $svbatchinfo->getErrors();
            $boolrollback = true;
        }
        
        // Insert to tbl_voucherinfo
        $svvoucherinfo->StartTransaction();
        $charToUse = "ABCDEFGHJKMNPQRSTUVWXYZ";
        $randAlphaLength = strlen($charToUse);
        $var_randAlpha = "";
        $var_VoucherCode = "";
        
        for ($ctr = 1; $ctr <= $txtQty->SubmittedValue; $ctr++)
        {
            $var_randAlpha = substr($charToUse, mt_rand(0, ($randAlphaLength - 1)), 1) . substr($charToUse, mt_rand(0, ($randAlphaLength - 1)), 1) . substr($charToUse, mt_rand(0, ($randAlphaLength - 1)), 1) . substr($charToUse, mt_rand(0, ($randAlphaLength - 1)), 1);
            $var_VoucherCode = "G" . STR_PAD($max_batchid, 3, "000", STR_PAD_LEFT) . $var_randAlpha . STR_PAD($ctr, 4, "0000", STR_PAD_LEFT); 
            
            if ($ddlDenomination->SubmittedValue != 1) {
                $arrvoucher["Status"] = 4;
                $arrvoucher["EntryType"] = 3;
            } else {
                $arrvoucher["Status"] = 1;
            }
            $arrvoucher["BatchID"] = STR_PAD($max_batchid, 3, "000", STR_PAD_LEFT);
            $arrvoucher["VoucherNo"] = $var_VoucherCode;
            $arrvoucher["Denomination"] = $ddlDenomination->SubmittedValue;
            $arrvoucher["DateGenerated"] = 'now_usec()';
            $voucherdata[] = $arrvoucher;
        }
        $svvoucherinfo->InsertMultiple($voucherdata);
        if ($svvoucherinfo->HasError) {
            $errormsg = $svvoucherinfo->getErrors();
            $boolrollback = true;
        }
        
        // Log to Audit Trail
        $svauditlog->StartTransaction();
        $scauditlogparam["TransDetails"] = "Generated Voucher Batch No. : " . STR_PAD($max_batchid, 3, "000", STR_PAD_LEFT);
        $scauditlogparam["IPAddress"] = $_SERVER['REMOTE_ADDR'];
        $scauditlogparam["DateCreated"] = "now_usec()";
        $scauditlogparam["CreatedBy"] = $_SESSION["username"];
        $svauditlog->Insert($scauditlogparam);
        if ($svauditlog->HasError) {
            $errormsg = $svauditlog->getError();
            $boolrollback = true;
        }        
        
        // Commit and Rollback Transactions
        if ($boolrollback)
        {
            $svbatchinfo->RollBackTransaction();
            $svvoucherinfo->RollBackTransaction();
            $svauditlog->RollBackTransaction();
        } else {
            // Get Date Generated
            $svbatchinfo->CommitTransaction();
            $svvoucherinfo->CommitTransaction();
            $svauditlog->CommitTransaction();
        
            $pdfdata = $svvoucherinfo->SelectByBatchNo(STR_PAD($max_batchid, 3, "000", STR_PAD_LEFT));  
            $dategen = date("mdY", strtotime($pdfdata[0]["DateGenerated"]));
            $hiddenbatchid->Text = STR_PAD($max_batchid, 3, "000", STR_PAD_LEFT);
            $hiddenfile->Text = "SWC_Voucher" . $deno_desc . "_" . $dategen;
            
            $errormsg = "You have successfully generated " . $txtQty->SubmittedValue . " number of voucher codes with " . $deno_desc . " denomination and Batch ID " . STR_PAD($max_batchid, 3, "000", STR_PAD_LEFT) . ". Click OKAY to download the newly generated codes into pdf file.";
            $errormsgtitle = "Successful Generation";
            $success = 1;
        }
    }
}
?>
