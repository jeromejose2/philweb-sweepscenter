<?php
/*
 * Created By: Noel Antonio
 * Date Created: May 24, 2012
 * CHANGE PASSWORD CONTROLLER
 * Purpose: Controller for changing user password.
 */
$modulename = "SweepsCenter";
App::LoadModuleClass($modulename,"SCCV_AdminAccounts");
App::LoadModuleClass($modulename,"SCCV_AdminAccountSessions");

$svadminaccts        = new SCCV_AdminAccounts;
$svadminacctsessions = new SCCV_AdminAccountSessions();

App::LoadCore("PHPMailer.class.php");
App::LoadControl("Hidden");
App::LoadControl("TextBox");
App::LoadControl("Button");

$fproc = new FormsProcessor();

$txtPassword           = new TextBox("txtPassword","txtPassword");
$txtPassword->Password = true;
$txtPassword->Length   = 12;
$txtPassword->Style    = "width: 200px";
$txtPassword->Args     = "autocomplete='off' onkeypress='javascript: return DisableSpaceandSpCharacters(event);'";

$txtNewPassword           = new TextBox("txtNewPassword","txtNewPassword");
$txtNewPassword->Password = true;
$txtNewPassword->Length   = 12;
$txtNewPassword->Style    = "width: 200px";
$txtNewPassword->Args     = "autocomplete='off' onkeypress='javascript: return DisableSpaceandSpCharacters(event);'";

$txtConfPassword           = new TextBox("txtConfPassword","txtConfPassword");
$txtConfPassword->Password = true;
$txtConfPassword->Length   = 12;
$txtConfPassword->Style    = "width: 200px";
$txtConfPassword->Args     = "autocomplete='off' onkeypress='javascript: return DisableSpaceandSpCharacters(event);'";

$btnSubmit           = new Button("btnSubmit","btnSubmit","SUBMIT");
$btnSubmit->Args     = "onclick='javascript: return checkpasswordinput();'";
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->IsSubmit = true;

$hidden = new Hidden("hidden","hidden");

$fproc->AddControl($txtPassword);
$fproc->AddControl($txtNewPassword);
$fproc->AddControl($txtConfPassword);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($hidden);

$fproc->ProcessForms();



if ( !$fproc->IsPostBack )
{
    $checksess = $svadminacctsessions->checkSession($_SESSION['aid']);

    if ( count($checksess) == 1 )
    {
        if ( $_SESSION['actualpassword'] == "password" )
        {
            $checkpasswordinput = "is set";
        }
        else
        {
            $_SESSION['allowedpage'] = 0;
        }
    }
}

if ( $fproc->IsPostBack )
{
    if ( $btnSubmit->SubmittedValue == "SUBMIT" )
    {
        $hash_oldpass = MD5($txtPassword->SubmittedValue);
        $hash_newpass = MD5($txtNewPassword->SubmittedValue);
        if ( $hash_oldpass != $_SESSION['password'] )
        {
            $errormsg      = "The current password you entered is invalid. Please try again.";
            $errormsgtitle = "NOTIFICATION";
        }
        else if ( $hash_oldpass == $hash_newpass )
        {
            $errormsg      = "The current password and the new password is the same. Please try again.";
            $errormsgtitle = "NOTIFICATION";
        }
        else
        {
            $confirmmsg = "You are about to change your current password. Do you wish to continue?";
        }
    }

    if ( $hidden->SubmittedValue != '' )
    {
        $acctdtls = $svadminaccts->SelectAccountDetails($_SESSION['sid']);
        if ( count($acctdtls) > 0 )
        {

            $id        = $acctdtls[0]["ID"];
            $emailaddr = $acctdtls[0]["Email"];
            $name      = $acctdtls[0]["Username"];

            $svadminaccts->StartTransaction();
            $updateacct["ID"]                 = $id;
            $updateacct["Password"]           = $hidden->SubmittedValue;
            $updateacct["RecUpdOn"]           = 'now_usec()';
            $updateacct["RecUpdBy"]           = $name;
            $updateacct["WillChangepassword"] = 0;
            $svadminaccts->UpdateByArray($updateacct);
            if ( $svadminaccts->HasError )
            {
                $svadminaccts->RollBackTransaction();
            }
            else
            {
                $svadminaccts->CommitTransaction();

                //added by jfj 07-16-2012
                $_SESSION['allowedpage'] = 0;
                $check1                  = $svadminaccts->checkExistingUsername($name);
                $checkpass               = $check1[0]['Password'];



                // Sending of Email
                $pm = new PHPMailer();
                $pm->AddAddress($emailaddr,$name);

                $pageURL = "http";
                if ( isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) == "on" )
                {
                    $pageURL .= "s";
                }
                $pageURL .= "://";
                $folder = $_SERVER["REQUEST_URI"];
                $folder = substr($folder,0,strrpos($folder,'/') + 1);
                if ( $_SERVER["SERVER_PORT"] != "80" )
                {
                    $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $folder;
                }
                else
                {
                    $pageURL .= $_SERVER["SERVER_NAME"] . $folder;
                }

                $pm->IsHTML(true);

                $pm->Body = "Dear $name,
                          <br /><br />
                          This is to inform you that your password has been changed on this date " . date("m/d/Y") . " and time " . date("H:i:s") . ".
                          Here are your new Account Details:
                                <br /><br />
                                Username: <b>$name</b><br />
                                Password: <b>$txtNewPassword->SubmittedValue</b>
                          <br /><br />
                          If you didn't perform this procedure, please report this to our Customer Service email at support.gu@philwebasiapacific.com or support@philwebasiapacific.com.
                          <br /><br />
                          Regards,
                          <br /><br />
                          Customer Support
                          <br />
                          The Sweeps Center";

                $pm->From     = "operations@thesweepscenter.com";
                $pm->FromName = "The Sweeps Center";
                $pm->Host     = "localhost";
                $pm->Subject  = "EMAIL NOTIFICATION FOR NEW PASSWORD";
                $email_sent   = $pm->Send();
                if ( $email_sent )
                {
                    $txtPassword->Text     = "";
                    $txtNewPassword->Text  = "";
                    $txtConfPassword->Text = "";
                    $hidden->Text          = "";
                    $errormsg              = "You have successfully changed your password. An e-mail notification was sent to the user account.";
                    $errormsgtitle         = "Password Changed!";
                    if ( $_SESSION['actualpassword'] == "password" )
                    {
                        echo "<script>  window.location = 'login.php'</script>";
                    }
                }
                else
                {
                    $emailmsg      = "An error occurred while sending the email to your email address";
                    $errormsgtitle = "ERROR!";
                }
            }
        }
        else
        {
            $emailmsg      = "No Session exists.";
            $errormsgtitle = "NOTIFICATION!";
        }
    }
}
?>
