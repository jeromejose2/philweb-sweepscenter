<?php
/*
 * Created By: Noel Antonio
 * Date Modified: May 25, 2012
 * ADD USER ACCOUNT CONTROLLER
 * Purpose: For adding new user account.
 */

$modulename = "SweepsCenter";
App::LoadModuleClass($modulename, "SCCV_AdminAccounts");
App::LoadModuleClass($modulename, "SCCV_AdminAccountSessions");
App::LoadModuleClass($modulename, "SCCV_AuditLog");
App::LoadModuleClass($modulename, "SCCV_Denominationref");

App::LoadCore("PHPMailer.class.php");
App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");

$fproc = new FormsProcessor();
$svadminaccts = new SCCV_AdminAccounts();
$svadminacctsessions = new SCCV_AdminAccountSessions();
$svauditlog = new SCCV_AuditLog();
$svdenominationref = new SCCV_Denominationref();

$txtUName = new TextBox("txtUName", "txtUName", "Username");
$txtUName->Length = 12;
$txtUName->Args = "onkeypress='javascript: return isSpecialKey(event);'";




$txtFName = new TextBox("txtFName", "txtFName", "First Name");
$txtFName->Length = 50;
$txtFName->Style = "width: 200px;";
$txtFName->Args = "onkeypress='javascript: return AlphaOnly(event);'";

$txtMName = new TextBox("txtMName", "txtMName", "Middle Name");
$txtMName->Length = 5;
$txtMName->Style = "width: 200px;";
$txtMName->Args = "onkeypress='javascript: return AlphaOnly(event);'";

$txtLName = new TextBox("txtLName", "txtLName", "Last Name");
$txtLName->Length = 50;
$txtLName->Style = "width: 200px;";
$txtLName->Args = "onkeypress='javascript: return AlphaOnly(event);'";

$txtEmail = new TextBox("txtEmail", "txtEmail", "Email");
$txtEmail->Length = 50;
$txtEmail->Style = "width: 200px;";
$txtEmail->Args = "onkeypress='javascript: return verifyEmail(event);'";

$txtPosition = new TextBox("txtPosition", "txtPosition", "Position");
$txtPosition->Length = 20;
$txtPosition->Style = "width: 200px;";
$txtPosition->Args = "onkeypress='javascript: return numeric(event);'";

$txtCompany = new TextBox("txtCompany", "txtCompany", "Company");
$txtCompany->Length = 20;
$txtCompany->Style = "width: 200px;";

$txtDept = new TextBox("txtDepartment", "txtDepartment", "Department");
$txtDept->Length = 20;
$txtDept->Style = "width: 200px;";

$ddlGroup = new ComboBox("ddlGroup", "ddlGroup", "Group");
$opt = null;
$opt[] = new ListItem("---", "0", true);
$opt[] = new ListItem("Admin", "1");
$opt[] = new ListItem("Management", "2");
$ddlGroup->Items = $opt;

$btnSubmit = new Button("btnSubmit", "btnSubmit", "SUBMIT");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Args = "onclick='javascript: return checkadduseracct();'";
$btnSubmit->IsSubmit = true;

$btnConfirm = new Button("btnConfirm", "btnConfirm", "YES");
$btnConfirm->CssClass = "labelbutton2";
$btnConfirm->IsSubmit = true;

$fproc->AddControl($txtUName);
$fproc->AddControl($txtFName);
$fproc->AddControl($txtMName);
$fproc->AddControl($txtLName);
$fproc->AddControl($txtEmail);
$fproc->AddControl($txtPosition);
$fproc->AddControl($txtCompany);
$fproc->AddControl($txtDept);
$fproc->AddControl($ddlGroup);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnConfirm);

$fproc->ProcessForms();

if ($fproc->IsPostBack)
{
    if ($btnSubmit->SubmittedValue == "SUBMIT")
    {
        $acctdtls = $svadminaccts->SelectAccountDetails($_SESSION['sid']);
        if (count($acctdtls) > 0)
        {
            // Check if Email exist
            $where = " WHERE Email = '$txtEmail->SubmittedValue'";
            $emailexist = $svadminaccts->SelectByWhere($where);
            if (count($emailexist) == 0)
            {
                // Check if Username exist
                $where = " WHERE Username = '$txtUName->SubmittedValue'";
                $unameexist = $svadminaccts->SelectByWhere($where);
                if (count($unameexist) == 0)
                {
                    $confirmmsg = "You are about to create a new user account for the voucher management tool. Do you wish to continue?";
                } else {
                    $errormsg = "Username already exist.";
                    $errormsgtitle = "NOTIFICATION!";
                }
            } else {
                $errormsg = "Email already exist.";
                $errormsgtitle = "NOTIFICATION!";
            }
        } else {
            $errormsg = "No Session exist.";
            $errormsgtitle = "NOTIFICATION!";
        }
    }
    
    if ($btnConfirm->SubmittedValue == "YES")
    {
        $svadminaccts->StartTransaction();
        $insert["Username"] = $txtUName->SubmittedValue;
        $insert["Password"] = MD5("password");
        $insert["Status"] = 1;
        $insert["FirstName"] = $txtFName->SubmittedValue;
        $insert["MiddleName"] = $txtMName->SubmittedValue;
        $insert["LastName"] = $txtLName->SubmittedValue;
        $insert["Email"] = $txtEmail->SubmittedValue;
        $insert["WillChangePassword"] = 1;
        $insert["Position"] = $txtPosition->SubmittedValue;
        $insert["Department"] = $txtDept->SubmittedValue;
        $insert["Company"] = $txtCompany->SubmittedValue;
        $insert["AccountType"] = $_SESSION['accttype'];
        $insert["RecCreOn"] = 'now_usec()';
        $insert["RecCreBy"] = $_SESSION["username"];
        $svadminaccts->Insert($insert);
        if ($svadminaccts->HasError)
        {
            $svadminaccts->RollBackTransaction();
        } else {
            $svadminaccts->CommitTransaction();

            // Sending of Email
            $username = $txtUName->SubmittedValue;
            $acctname = $txtFName->SubmittedValue . " " . $txtMName->SubmittedValue . " " . $txtLName->SubmittedValue;
            $password = "password";
      
            $pm = new PHPMailer();
            $pm->AddAddress($txtEmail->SubmittedValue, $username);

            $pageURL = "http";
            if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
            $pageURL .= "://";
            $folder = $_SERVER["REQUEST_URI"];
            $folder = substr($folder,0,strrpos($folder,'/') + 1);
            if ($_SERVER["SERVER_PORT"] != "80") 
            {
              $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
            } 
            else 
            {
              $pageURL .= $_SERVER["SERVER_NAME"].$folder;
            }

            $pm->IsHTML(true);

            $pm->Body = "Dear $acctname,
                        <br /><br />
                        This is to inform you that your User Account has been successfully created on this date " . date("m/d/Y") . " and time " . date("H:i:s") . ".
                        Here are your Account Details:
                        <br /><br />
                        Username: <b>$username</b><br />
                        Password: <b>$password</b>	
                        <br /><br />
                        For security purposes, please change this assigned password upon log in at <a href='$pageURL'>http://www.thesweepscenter.com/VoucherWebtool</a>.																		
                        <br /><br />
                        For inquiries on your account, please contact our Customer Service email at support.gu@philwebasiapacific.com or support@philwebasiapacific.com.
                        <br /><br />
                        Regards,
                        <br /><br />
                        Customer Support
                        <br />
                        The Sweeps Center Team";

            $pm->From = "operations@thesweepscenter.com";
            $pm->FromName = "The Sweeps Center";  
            $pm->Host = "localhost";
            $pm->Subject = "SUCCESSFUL ACCOUNT CREATION FOR VOUCHER MANAGEMENT TOOL";
            $email_sent = $pm->Send();
            if($email_sent)
            {
                $errormsg1 = "You have successfully created a new account with username " . $username . ". Please check the registered email address for the account details.";
                $errormsgtitle1 = "CONFIRMATION";
                
            }
            else
            {
                $errormsg = "An error occurred while sending the email to your email address";
                $errormsgtitle = "NOTIFICATION!";
            }  
        }
    }
}
?>
